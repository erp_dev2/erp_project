﻿namespace RunSystem
{
    partial class FrmMultiPayrun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.LueSystemType = new DevExpress.XtraEditors.LookUpEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.LuePayrunPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkSiteCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(492, 0);
            this.panel1.Size = new System.Drawing.Size(82, 154);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(82, 33);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkSiteCode);
            this.panel2.Controls.Add(this.LblSiteCode);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LuePGCode);
            this.panel2.Controls.Add(this.label60);
            this.panel2.Controls.Add(this.LueSystemType);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.LuePayrunPeriod);
            this.panel2.Controls.Add(this.DteEndDt);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteStartDt);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Size = new System.Drawing.Size(492, 154);
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(73, 9);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 3;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(105, 6);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 400;
            this.LueSiteCode.Size = new System.Drawing.Size(348, 20);
            this.LueSiteCode.TabIndex = 4;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(23, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 14);
            this.label8.TabIndex = 10;
            this.label8.Text = "Payroll Group";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(105, 72);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.MaxLength = 1;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 400;
            this.LuePGCode.Size = new System.Drawing.Size(376, 20);
            this.LuePGCode.TabIndex = 11;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Red;
            this.label60.Location = new System.Drawing.Point(22, 31);
            this.label60.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(79, 14);
            this.label60.TabIndex = 6;
            this.label60.Text = "System Type";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSystemType
            // 
            this.LueSystemType.EnterMoveNextControl = true;
            this.LueSystemType.Location = new System.Drawing.Point(105, 28);
            this.LueSystemType.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.LueSystemType.Name = "LueSystemType";
            this.LueSystemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.Appearance.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSystemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSystemType.Properties.DropDownRows = 30;
            this.LueSystemType.Properties.NullText = "[Empty]";
            this.LueSystemType.Properties.PopupWidth = 400;
            this.LueSystemType.Size = new System.Drawing.Size(376, 20);
            this.LueSystemType.TabIndex = 7;
            this.LueSystemType.ToolTip = "F4 : Show/hide list";
            this.LueSystemType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSystemType.EditValueChanged += new System.EventHandler(this.LueSystemType_EditValueChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Red;
            this.label51.Location = new System.Drawing.Point(19, 53);
            this.label51.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(82, 14);
            this.label51.TabIndex = 8;
            this.label51.Text = "Payrun Period";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrunPeriod
            // 
            this.LuePayrunPeriod.EnterMoveNextControl = true;
            this.LuePayrunPeriod.Location = new System.Drawing.Point(105, 50);
            this.LuePayrunPeriod.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.LuePayrunPeriod.Name = "LuePayrunPeriod";
            this.LuePayrunPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrunPeriod.Properties.DropDownRows = 30;
            this.LuePayrunPeriod.Properties.MaxLength = 1;
            this.LuePayrunPeriod.Properties.NullText = "[Empty]";
            this.LuePayrunPeriod.Properties.PopupWidth = 400;
            this.LuePayrunPeriod.Size = new System.Drawing.Size(376, 20);
            this.LuePayrunPeriod.TabIndex = 9;
            this.LuePayrunPeriod.ToolTip = "F4 : Show/hide list";
            this.LuePayrunPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePayrunPeriod.EditValueChanged += new System.EventHandler(this.LuePayrunPeriod_EditValueChanged);
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(105, 116);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(119, 20);
            this.DteEndDt.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(43, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "End Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(105, 94);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(119, 20);
            this.DteStartDt.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(37, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 14);
            this.label3.TabIndex = 12;
            this.label3.Text = "Start Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkSiteCode
            // 
            this.ChkSiteCode.Location = new System.Drawing.Point(457, 5);
            this.ChkSiteCode.Name = "ChkSiteCode";
            this.ChkSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCode.Properties.Caption = " ";
            this.ChkSiteCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCode.Size = new System.Drawing.Size(23, 22);
            this.ChkSiteCode.TabIndex = 5;
            this.ChkSiteCode.ToolTip = "Remove filter";
            this.ChkSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCode.ToolTipTitle = "Run System";
            this.ChkSiteCode.CheckedChanged += new System.EventHandler(this.ChkSiteCode_CheckedChanged);
            // 
            // FrmMultiPayrun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 154);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmMultiPayrun";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Generate Multi Payrun";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LuePGCode;
        private System.Windows.Forms.Label label60;
        private DevExpress.XtraEditors.LookUpEdit LueSystemType;
        private System.Windows.Forms.Label label51;
        private DevExpress.XtraEditors.LookUpEdit LuePayrunPeriod;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCode;
    }
}