﻿#region update
    // 20/11/2020 [VIN/IMS] New App
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUpdateVoucherCashType : RunSystem.FrmBase12
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmUpdateVoucherCashType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.SetControlReadOnly(new List<BaseEdit> { TxtDocNo, TxtOldCashTypeCode }, true);
                SetLueCashTypeCode(ref LueCashTypeCode, "");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<BaseEdit> 
            {
               TxtDocNo, TxtOldCashTypeCode, LueCashTypeCode
            });
        }

        #region Button Methods

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.StdMsgYN("Process", "") == DialogResult.No)
                return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ProcessData();
                ClearData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        private void ProcessData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("UPDATE TblVoucherHdr ");
            SQL.AppendLine("SET CashTypeCode = @CashTypeCode ");
            SQL.AppendLine("WHERE DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam(ref cm, "@CashTypeCode", Sm.GetLue(LueCashTypeCode));

            cm.CommandText = SQL.ToString();

            Sm.ExecCommand(cm);
            Sm.StdMsg(mMsgType.Info, "Cash Type has been Updated.");
        }
        #endregion

        internal void SetLueCashTypeCode(ref LookUpEdit Lue, string CashTypeCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CashTypeCode As Col1, CashTypeName As Col2 From TblCashType Order By CashTypeName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CashTypeCode.Length != 0) Sm.SetLue(Lue, CashTypeCode);
        }

        private void BtnVoucher_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmUpdateVoucherCashTypeDlg (this));

        }

       
    }
}
