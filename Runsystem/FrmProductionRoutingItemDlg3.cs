﻿#region Update
/*
    12/12/2019 [WED/MAI] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionRoutingItemDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        private FrmProductionRoutingItem mFrmParent;

        #endregion

        #region Constructor

        public FrmProductionRoutingItemDlg3(FrmProductionRoutingItem FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            SetGrd();

        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Work Center "+Environment.NewLine+"Document Number",
                    "",
                    "Work Center "+Environment.NewLine+"Document Date",
                    "Work Center "+Environment.NewLine+"Document Name",
                    //6-8
                    "Location",
                    "Capacity",
                    "UoM"
                }
            );
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8 });

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo,A.DocDt,A.DocName,A.Location,A.Capacity,B.UomName ");
            SQL.AppendLine("From tblworkCenterhdr A ");
            SQL.AppendLine("Inner Join TblUom B On A.UomCode=B.UomCode ");
            SQL.AppendLine("Where A.ActiveInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And A.DocNo Not In (" + mFrmParent.GetSelectedItem() + ") ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "A.DocNo", "A.DocName" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                    new string[] 
                    { 
                    
                        //0
                        "DocNo",
                        //1-5
                        "DocDt","DocName","Location","Capacity","UomName"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 8].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 workcenter.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 4),
                    Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 5)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
