﻿#region Update
/*
    20/04/2020 [TKG/IMS] quantity material bisa 0
    22/03/2021 [WED/IMS] urutan tampil berdasarkan seqno
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBom3Dlg2 : RunSystem.FrmBase15
    {
        #region Field

        private int mRow = -1;
        private FrmBom3 mFrmParent;
        private bool mIsInsert;

        #endregion

        #region Constructor

        public FrmBom3Dlg2(FrmBom3 FrmParent, int r, bool IsInsert)
        {
            InitializeComponent();
            mRow = r;
            mFrmParent = FrmParent;
            mIsInsert = IsInsert;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetGrd();
                ShowData();
                if (!mIsInsert)
                {
                    Grd1.ReadOnly = true;
                    BtnSave.Enabled = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Material's Code",
                    "Material's Name",
                    "Local Code",
                    "Specification",
                    "Quantity",

                    //6-8
                    "UoM",
                    "Remark",
                    "FileName"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    130, 200, 100, 200, 80,
                    
                    //6-8
                    100, 200, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 8 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 6, 8 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowData()
        {
            TxtItCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 2);
            TxtItName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 3);
            TxtItCodeInternal.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 4);
            MeeSpecification.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 5);
            TxtQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 6), 0);
            TxtUomCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7);

            if (mFrmParent.ml.Count > 0)
            {
                int r = 0;
                Grd1.BeginUpdate();
                foreach (var x in mFrmParent.ml.Where(w =>Sm.CompareStr(w.ItCode, TxtItCode.Text)).OrderBy(o => o.SeqNo))
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[r, 1].Value = x.ItCode2;
                    Grd1.Cells[r, 2].Value = x.ItName;
                    Grd1.Cells[r, 3].Value = x.ItCodeInternal;
                    Grd1.Cells[r, 4].Value = x.Specification;
                    Grd1.Cells[r, 5].Value = Sm.FormatNum(x.Qty, 0);
                    Grd1.Cells[r, 6].Value = x.UomCode;
                    Grd1.Cells[r, 7].Value = x.Remark;
                    Grd1.Cells[r, 8].Value = x.FileName;
                    r++;
                }
            }
            Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Button Methods

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (IsGrdValueNotValid()) return;
            try
            {
                if (mFrmParent.ml.Count > 0) mFrmParent.ml.RemoveAll(x =>Sm.CompareStr(x.ItCode, TxtItCode.Text));
                
                var MaxDt = string.Empty;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length>0)
                    {
                        mFrmParent.ml.Add(new FrmBom3.Bom2Dtl2()
                        {
                            ItCode = TxtItCode.Text,
                            ItCode2 = Sm.GetGrdStr(Grd1, r, 1),
                            ItName = Sm.GetGrdStr(Grd1, r, 2),
                            ItCodeInternal = Sm.GetGrdStr(Grd1, r, 3),
                            Specification = Sm.GetGrdStr(Grd1, r, 4),
                            Qty = Sm.GetGrdDec(Grd1, r, 5),
                            UomCode= Sm.GetGrdStr(Grd1, r, 6),
                            Remark = Sm.GetGrdStr(Grd1, r, 7),
                            FileName = Sm.GetGrdStr(Grd1, r, 8)
                        });
                    }
                }
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsGrdValueNotValid()
        {
            //for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            //{
            //    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            //    {
            //        if (Sm.IsGrdValueEmpty(Grd1, r, 5, true, "Quantity is empty."))
            //            return true;
            //    }
            //}
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBom3Dlg3(this));
            }

            if (Sm.IsGrdColSelected(new int[] { 0, 5, 7 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmBom3Dlg3(this));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7 }, e);
        }

        #endregion

        #endregion
    }
}
