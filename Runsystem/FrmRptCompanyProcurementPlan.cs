﻿#region Update
/*
  08/11/2021 [MYA/PHT] New Apps
  15/11/2021 [MYA/PHT] Menambahkan filter Site
  15/11/2021 [MYA/PHT] Bug : Company Plan's Report ketika search dengan filter month muncul warning
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCompanyProcurementPlan : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptCompanyProcurementPlan(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetGrd();
                SetSQL();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueSiteCode(ref LueSite, string.Empty);
                Sl.SetLueMth(LueMth);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL()
        {
            string Mth = Sm.GetLue(LueMth);
            Mth = Mth.TrimStart('0');
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.ItCode, T.ItName, T.SiteCode, T.SiteName, T.ProcurementType, T.Method, T.ProcurementPlan, T.Realization, T.Group");
            SQL.AppendLine("FROM (");
            SQL.AppendLine("SELECT B.ItCode, C.ItName, A.SiteCode, E.SiteName, B.ProcurementType, D.OptDesc AS Method, ");
            if (Sm.GetLue(LueMth).Length > 0)
            {
                SQL.AppendLine("T2.ProcurementPlan, ");
            }
            else
            {
                SQL.AppendLine("(B.Qty * B.UPrice) AS ProcurementPlan, ");
            }
            SQL.AppendLine("T1.Realization, 'CPP' AS `Group` ");
            SQL.AppendLine("FROM TblCompanyProcurementPlanHdr A ");
            SQL.AppendLine("INNER JOIN tblcompanyprocurementplandtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("AND A.Yr = @Yr ");
            SQL.AppendLine("AND A.CancelInd = 'N'"); 
            if (Sm.GetLue(LueSite).Length > 0)
                SQL.AppendLine("AND A.SiteCode = @Site");
            SQL.AppendLine("LEFT JOIN TblMaterialRequestDtl B1 On B.ItCode = B1.ItCode ");
            SQL.AppendLine("AND B1.Status = 'A' ");
            SQL.AppendLine("AND B1.CancelInd = 'N'");
            SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN tbloption D ON B.ProcurementType = D.OptCode AND D.OptCat = 'ProcurementType'  ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("	SELECT C.ItCode, SUM(F.Qty * F.EstPrice) AS Realization, E.ProcurementType, E.SiteCode ");
            SQL.AppendLine("	FROM tblmaterialrequesthdr E ");
            SQL.AppendLine("	INNER JOIN tblmaterialrequestdtl F ON E.DocNo = F.DocNo  ");
            SQL.AppendLine("	AND E.Status = 'A'  ");
            SQL.AppendLine("    AND E.CancelInd = 'N' ");
            SQL.AppendLine("	AND LEFT(E.DocDt,4) = @Yr  ");
            if (Sm.GetLue(LueMth).Length > 0)
                SQL.AppendLine("AND MID(E.DocDt,5,2) = @Mth ");
            SQL.AppendLine("    AND F.CancelInd = 'N' ");
            SQL.AppendLine("	INNER JOIN tblitem C ON F.ItCode = C.ItCode AND F.ItCode IN  ");
            SQL.AppendLine("	(  ");
            SQL.AppendLine("		SELECT B.ItCode  ");
            SQL.AppendLine("		FROM tblcompanyprocurementplanhdr A ");
            SQL.AppendLine("		INNER JOIN tblcompanyprocurementplandtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("		WHERE A.Yr = @Yr   ");
            SQL.AppendLine("		GROUP BY ItCode  ");
            SQL.AppendLine("	)  ");
            SQL.AppendLine("	GROUP BY C.ItCode, E.ProcurementType, E.SiteCode ");
            SQL.AppendLine(") T1 ON C.ItCode = T1.ItCode AND B.ProcurementType = T1.ProcurementType AND A.SiteCode = T1.SiteCode And B1.Status = 'A' AND B1.CancelInd = 'N' ");
            if (Sm.GetLue(LueMth).Length > 0)
            {
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("SELECT B.DocNo, B.DNo, B."+ Mth +" AS ProcurementPlan ");
                SQL.AppendLine("FROM tblcompanyprocurementplandtl B ");
                SQL.AppendLine("GROUP BY B.DocNo, B.DNo ");
                SQL.AppendLine(")T2 ON B.DocNo = T2.DocNo AND B.DNo = T2.DNo ");
            }
            SQL.AppendLine("INNER JOIN tblsite E on A.SiteCode = E.SiteCode");
            SQL.AppendLine("GROUP BY B.ItCode, B.ProcurementType, A.SiteCode ");

            SQL.AppendLine("UNION ALL  ");

            SQL.AppendLine("SELECT C.ItCode, C.ItName, A.SiteCode, E.SiteName, A.ProcurementType, D.OptDesc AS Method, 0 AS ProcurementPlan, SUM(B.Qty * B.EstPrice) AS Realization, 'Miscellaneous' AS `Group` ");
            SQL.AppendLine("FROM tblmaterialrequesthdr A  ");
            SQL.AppendLine("INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
            SQL.AppendLine("AND A.Status = 'A'  ");
            SQL.AppendLine("AND B.CancelInd = 'N' ");
            SQL.AppendLine("AND LEFT(A.DocDt,4) = @Yr  ");
            if (Sm.GetLue(LueMth).Length > 0)
                SQL.AppendLine("AND MID(A.DocDt,5,2) = @Mth ");
            if (Sm.GetLue(LueSite).Length > 0)
                SQL.AppendLine("AND A.SiteCode = @Site");
            SQL.AppendLine("INNER JOIN tblitem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.ProcurementType = D.OptCode AND D.OptCat = 'ProcurementType'  ");
            SQL.AppendLine("INNER JOIN tblsite E on A.SiteCode = E.SiteCode");
            SQL.AppendLine("WHERE CONCAT(C.ItCode,D.OptCode) NOT IN  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("	SELECT CONCAT(F.ItCode,F.ProcurementType) ");
            SQL.AppendLine("	FROM tblcompanyprocurementplanhdr E  ");
            SQL.AppendLine("	INNER JOIN tblcompanyprocurementplandtl F  ");
            SQL.AppendLine("	WHERE E.Yr = @Yr  ");
            SQL.AppendLine("	GROUP BY ItCode  ");
            SQL.AppendLine(")  ");
            SQL.AppendLine("GROUP BY C.ItCode, A.ProcurementType, A.SiteCode ");
            SQL.AppendLine(")T");
            SQL.AppendLine("WHERE 1 = 1  ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item's Code",
                    "Item Name",
                    "Site Code",
                    "Site Name",
                    "Procurement Type",
                     
                    
                    
                    //6-10
                    "Method",
                    "Procurement's Plan",
                    "Realization",
                    "",
                    "Group",


                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 0, 200, 0, 
                    
                    //6-10
                    200, 180, 180, 20, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName"});
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Site", Sm.GetLue(LueSite));


                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SetSQL() + Filter + ";",
                new string[]
                    {
                        //0
                        "ItCode", 

                        //1-5
                        "ItName", "SiteCode", "SiteName", "ProcurementType", "Method", 
                        
                        //6-8
                         "ProcurementPlan", "Realization", "Group"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                }, true, false, false, true
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmRptCompanyProcurementPlanDlg(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ProcurementType = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.SiteCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.Yr = Sm.GetLue(LueYr);
                f.Mth = Sm.GetLue(LueMth);
                f.Text = "Company Procurement Plan";
                f.ShowDialog();
            }
        }




        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmRptCompanyProcurementPlanDlg(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ProcurementType = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.SiteCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.Yr = Sm.GetLue(LueYr);
                f.Mth = Sm.GetLue(LueMth);
                f.Text = "Company Procurement Plan";
                f.ShowDialog();
            }

        }

       

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMth_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Month");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSite, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSite_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

    }
}
