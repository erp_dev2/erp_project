﻿#region Update
/* 
    29/06/2018 [ARI] tambah warning jika budget lebih kecil dari sebelumnya
    14/08/2018 [TKG] budget yg diapprove tidak boleh kurang dari budget yg digunakan.
    15/08/2018 [TKG] mempercepat proses penyimpanan budget.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudget : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mBudgetBasedOn = string.Empty; // 1 = Department, 2 = Site
        internal FrmBudgetFind FrmFind;
        private bool mIsFilterBySite = false;
        private bool mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmBudget(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, "");
                Sl.SetLueMth(LueMth);
                SetGrd();
                base.FrmLoad(sender, e);
                
              
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 105;
            Grd1.FrozenArea.ColCount = 8;

            Grd1.Header.Cells[0, 0].Value = mBudgetBasedOn=="1"?"Department":"Site";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Cols[0].Width = 0;

            Grd1.Header.Cells[1, 1].Value = "Total";

            Grd1.Header.Cells[1, 8].Value = "January";
            Grd1.Header.Cells[1, 15].Value = "February";
            Grd1.Header.Cells[1, 22].Value = "March";
            Grd1.Header.Cells[1, 29].Value = "April";
            Grd1.Header.Cells[1, 36].Value = "May";
            Grd1.Header.Cells[1, 43].Value = "June";
            Grd1.Header.Cells[1, 50].Value = "July";
            Grd1.Header.Cells[1, 57].Value = "August";
            Grd1.Header.Cells[1, 64].Value = "September";
            Grd1.Header.Cells[1, 71].Value = "October";
            Grd1.Header.Cells[1, 78].Value = "November";
            Grd1.Header.Cells[1, 85].Value = "December";

            Grd1.Header.Cells[0, 93].Value = "January";
            Grd1.Header.Cells[0, 93].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 93].SpanRows = 2;
            Grd1.Cols[93].Width = 130;

            Grd1.Header.Cells[0, 94].Value = "February";
            Grd1.Header.Cells[0, 94].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 94].SpanRows = 2;
            Grd1.Cols[94].Width = 130;

            Grd1.Header.Cells[0, 95].Value = "March";
            Grd1.Header.Cells[0, 95].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 95].SpanRows = 2;
            Grd1.Cols[95].Width = 130;

            Grd1.Header.Cells[0, 96].Value = "April";
            Grd1.Header.Cells[0, 96].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 96].SpanRows = 2;
            Grd1.Cols[96].Width = 130;

            Grd1.Header.Cells[0, 97].Value = "May";
            Grd1.Header.Cells[0, 97].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 97].SpanRows = 2;
            Grd1.Cols[97].Width = 130;

            Grd1.Header.Cells[0, 98].Value = "June";
            Grd1.Header.Cells[0, 98].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 98].SpanRows = 2;
            Grd1.Cols[98].Width = 130;

            Grd1.Header.Cells[0, 99].Value = "July";
            Grd1.Header.Cells[0, 99].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 99].SpanRows = 2;
            Grd1.Cols[99].Width = 130;

            Grd1.Header.Cells[0, 100].Value = "August";
            Grd1.Header.Cells[0, 100].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 100].SpanRows = 2;
            Grd1.Cols[100].Width = 130;

            Grd1.Header.Cells[0, 101].Value = "September";
            Grd1.Header.Cells[0, 101].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 101].SpanRows = 2;
            Grd1.Cols[101].Width = 130;

            Grd1.Header.Cells[0, 102].Value = "October";
            Grd1.Header.Cells[0, 102].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 102].SpanRows = 2;
            Grd1.Cols[102].Width = 130;

            Grd1.Header.Cells[0, 103].Value = "November";
            Grd1.Header.Cells[0, 103].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 103].SpanRows = 2;
            Grd1.Cols[103].Width = 130;

            Grd1.Header.Cells[0, 104].Value = "December";
            Grd1.Header.Cells[0, 104].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 104].SpanRows = 2;
            Grd1.Cols[104].Width = 130;
            

            for (int Col = 0; Col <= 12; Col++)
            {
                Grd1.Header.Cells[1, Col*7+1].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[1, Col*7+1].SpanCols = 7;
            }

            for (int Col = 0; Col <= 12; Col++)
            {
                Grd1.Header.Cells[0, (Col*7)+1].Value = "Previous" + Environment.NewLine + "Year";
                Grd1.Cols[(Col * 7) + 1].Width = 130;
                Grd1.Cols[(Col * 7) + 1].Visible = false;

                Grd1.Header.Cells[0, (Col * 7) + 2].Value = "Previous Requested" + Environment.NewLine + "Amount";
                Grd1.Cols[(Col * 7) + 2].Width = 130;
                Grd1.Cols[(Col * 7) + 2].Visible = false;

                Grd1.Header.Cells[0, (Col * 7) + 3].Value = "Requested" + Environment.NewLine + "Amount";
                Grd1.Cols[(Col * 7) + 3].Width = 130;

                Grd1.Header.Cells[0, (Col * 7) + 4].Value = "Percentage";
                Grd1.Cols[(Col * 7) + 4].Width = 80;
                Grd1.Cols[(Col * 7) + 4].Visible = false;

                Grd1.Header.Cells[0, (Col * 7) + 5].Value = "Previous Approved" + Environment.NewLine + "Amount";
                Grd1.Cols[(Col * 7) + 5].Width = 130;
                //Grd1.Cols[(Col * 7) + 5].Visible = false;
                Grd1.Cols[(Col * 7) + 5].Visible = true;

                Grd1.Header.Cells[0, (Col * 7) + 6].Value = "Approved" + Environment.NewLine + "Amount";
                Grd1.Cols[(Col * 7) + 6].Width = 130;

                Grd1.Header.Cells[0, (Col * 7) + 7].Value = "Approved" + Environment.NewLine + "Date";
                Grd1.Cols[(Col * 7) + 7].Width = 80;
                Grd1.Cols[(Col * 7) + 7].Visible = false;
            }

            for (int Col = 0; Col <= 90; Col++)
                Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Header.Cells[0, 92].Value = mBudgetBasedOn == "1" ? "Department" : "Site";
            Grd1.Header.Cells[0, 92].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[0, 92].SpanRows = 2;
            Grd1.Cols[92].Width = 150;
            Grd1.Cols[92].Move(0);

            Sm.GrdColReadOnly(Grd1, 
                new int[] { 
                    0, 
                    1, 2, 3, 4, 5, 6, 7,
                    8, 9, 10, 11, 12, 14, 
                    15, 16, 17, 18, 19, 21,
                    22, 23, 24, 25, 26, 28, 
                    29, 30, 31, 32, 33, 35, 
                    36, 37, 38, 39, 40, 42,  
                    43, 44, 45, 46, 47, 49, 
                    50, 51, 52, 53, 54, 56, 
                    57, 58, 59, 60, 61, 63, 
                    64, 65, 66, 67, 68, 70,
                    71, 72, 73, 74, 75, 77, 
                    78, 79, 80, 81, 82, 84, 
                    85, 86, 87, 88, 89, 91,
                    92, 93, 94, 95, 96, 97, 
                    98, 99, 100, 101, 102, 
                    103, 104,
                    
                });
            Sm.GrdFormatDec(Grd1, 
                new int[] { 
                    1, 2, 3, 4, 5, 6, 
                    8, 9, 10, 11, 12, 13, 
                    15, 16, 17, 18, 19, 20,
                    22, 23, 24, 25, 26, 27, 
                    29, 30, 31, 32, 33, 34, 
                    36, 37, 38, 39, 40, 41,  
                    43, 44, 45, 46, 47, 48, 
                    50, 51, 52, 53, 54, 55, 
                    57, 58, 59, 60, 61, 62, 
                    64, 65, 66, 67, 68, 69,
                    71, 72, 73, 74, 75, 76, 
                    78, 79, 80, 81, 82, 83, 
                    85, 86, 87, 88, 89, 90,
                    93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104,
                }, 0);

            Sm.GrdColInvisible(Grd1, new int[] { 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104 }, false);

            Grd2.Cols.Count = 6;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        mBudgetBasedOn=="1"?"Department":"Site",

                        "Previous" + Environment.NewLine + "Year",
                        "Requested" + Environment.NewLine + "Amount",
                        "Percentage" + Environment.NewLine + "(%)",
                        "Approved" + Environment.NewLine + "Amount",
                        mBudgetBasedOn=="1"?"Department":"Site"
                    },
                    new int[] { 0, 130, 130, 80, 130, 150 }
                );
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 5 });
            Sm.GrdFormatDec(Grd2, new int[] { 1, 2, 3, 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 3 }, false);
            Grd2.Cols[5].Move(0);
        }

        private bool IsApprovedAmtLessThanUsedAmt()
        {
            decimal BudgetAmt = 0m, UsedAmt = 0m;
            var DeptCode = string.Empty;
            var YrMth = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
            SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo ");
            if (mBudgetBasedOn == "1")
                SQL.AppendLine("        And A.DeptCode=@DeptCode ");
            if (mBudgetBasedOn == "2")
                SQL.AppendLine("        And A.SiteCode=@DeptCode ");
            SQL.AppendLine("        And A.ReqType='1' ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status In ('O', 'A') ");
            SQL.AppendLine("        And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("    ),0.00)-");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
            SQL.AppendLine("        From TblPOQtyCancel A ");
            SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
            SQL.AppendLine("            On D.DocNo=E.DocNo  ");
            if (mBudgetBasedOn == "1")
                SQL.AppendLine("        And E.DeptCode=@DeptCode ");
            if (mBudgetBasedOn == "2")
                SQL.AppendLine("        And E.SiteCode=@DeptCode ");
            SQL.AppendLine("            And E.ReqType='1' ");
            SQL.AppendLine("            And Left(E.DocDt, 6)=@YrMth ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("    ),0.00) ");
            SQL.AppendLine(" As Amt; ");

            var Query = SQL.ToString();

            for (int c = 1; c <= 12; c++)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 0).Length > 0 &&
                        Sm.GetGrdDec(Grd1, r, (c * 7) + 5) > Sm.GetGrdDec(Grd1, r, (c * 7) + 6))
                    {
                        DeptCode = Sm.GetGrdStr(Grd1, r, 0);
                        YrMth = string.Concat(Sm.GetLue(LueYr), Sm.Right("0"+c.ToString(), 2));
                        BudgetAmt = Sm.GetGrdDec(Grd1, r, (c * 7) + 6);
                        UsedAmt = GetAvailableBudget(Query, DeptCode, YrMth);
                        if (BudgetAmt-UsedAmt < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, 
                                "Department : " + Sm.GetGrdStr(Grd1, r, 92) + Environment.NewLine +
                                "Budget amount : " + Sm.FormatNum(BudgetAmt, 0) + Environment.NewLine +
                                "Used amount : " + Sm.FormatNum(UsedAmt, 0) + Environment.NewLine + Environment.NewLine +
                                "Used amount is bigger than approved budget amount.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private decimal GetAvailableBudget(string SQL, string DeptCode, string YrMth)
        { 
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@YrMth", YrMth);

            var Value = Sm.GetValue(cm);
            if (Value.Length > 0)
                return decimal.Parse(Value);
            else
                return 0m;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, 
                new int[] {  
                    1, //4,
                    8, 11,  
                    15, 18, 
                    22, 25, 
                    29, 32, 
                    36, 39,  
                    43, 46, 
                    50, 53, 
                    57, 60, 
                    64, 67, 
                    71, 74,
                    78, 81,
                    85, 88, 
                }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueYr, LueMth }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    BtnApprove.Enabled = true;
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueYr, LueMth }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    BtnApprove.Enabled = false;
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(LueYr, true);
                    Sm.SetControlReadOnly(LueMth, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    BtnApprove.Enabled = false;
                    LueYr.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueYr, LueMth
            });
            Sm.ClearGrd(Grd1, true);

            for (int Row = 0; Row <= Grd1.Rows.Count-1; Row++)
            {
                for (int Col = 1; Col <= Grd1.Cols.Count - 1; Col++)
                    Grd1.Cells[Row, Col].ForeColor = Color.Black;
            }

            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetFind(this, mBudgetBasedOn, mIsFilterByDept, mIsFilterByDept);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (!LueYr.Properties.ReadOnly)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(
                BtnSave.Enabled && 
                Sm.IsGrdColSelected(new int[] { 13, 20, 27, 34, 41, 48, 55, 62, 69, 76, 83, 90 }, e.ColIndex) && 
                Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0 &&
                !Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 0), "Total")
                ))
                e.DoDefault = false;
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 13, 20, 27, 34, 41, 48, 55, 62, 69, 76, 83, 90 }, e.ColIndex))
                ComputeTotalApprovedAmt(e.RowIndex, e.ColIndex);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (BtnSave.Enabled && 
                Sm.IsGrdColSelected(new int[] { 8, 15, 22, 29, 36, 43, 50, 57, 64, 71, 78, 85 }, e.ColIndex))
            {
                Sm.SetLue(LueMth, Sm.Right("0" + (e.ColIndex / 7).ToString(), 2));
                ShowMonthlyBudget(e.ColIndex);
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            //IsBudgetSmaller();
            //bool mIsBudgetSmaller = IsBudgetSmaller();

            //for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            //{
            //    if (mIsBudgetSmaller)
            //    {
            //        if (Sm.StdMsgYN("Question",
            //            "Budget " + Sm.GetGrdStr(Grd2, Row, 5) + " is smaller than previous budget." + Environment.NewLine +
            //            "Do you still want to proceed ?") == DialogResult.No) return;
            //    }
            //}
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (!IsBudgetSmaller())
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    int Index = 1;
                   
                    while (Index <= 12)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, Index * 7 + 3).Length > 0)
                              cml.Add(InsertBudget(Index, Row, Index * 7 + 3));
                         Index++;
                    }

                }
                Sm.ExecCommands(cml);
                ShowData(Sm.GetLue(LueYr));

            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsYrExisted() ||
                IsGrdEmpty() ||
                IsBudgetRequestAmtChanged();
        }

        private bool IsYrExisted()
        {
            if (!LueYr.Properties.ReadOnly && 
                Sm.IsDataExist("Select Yr From TblBudget Where Yr='" + Sm.GetLue(LueYr) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Budget year ( " + Sm.GetLue(LueYr) + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <2)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 " + mBudgetBasedOn=="1"?"department":"site" + "'s budget.");
                return true;
            }
            return false;
        }

        private bool IsBudgetRequestAmtChanged()
        {
            ShowBudgetRequestAmt();

            for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
            {
                for (int Index = 1; Index <= 12; Index++)
                {
                    if (Grd1.Cells[Row, Index * 7 + 3].ForeColor == Color.Red)
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "Budget request amount changed already" + Environment.NewLine + 
                            "(Check budget request amount with red color)."
                            );
                        return true;
                    }
                }
            }
            return false;   
        }

        private bool IsBudgetSmaller()
        {
            decimal Amt = 0m;
            decimal Amt2 = 0m;
            decimal Mth = 0;
            decimal Mthdtl = 0;
            string Dept = string.Empty;
            string DeptDtl = string.Empty;

            if (Sm.GetLue(LueMth).Length != 0)
            {
                    for (var Row = 0; Row < Grd2.Rows.Count -1; Row++)
                    {
                        Dept = Sm.GetGrdStr(Grd2, Row, 0);
                        DeptDtl = Sm.GetGrdStr(Grd1, Row, 0);
                        Mth = decimal.Parse(Sm.GetLue(LueMth));
                        Mthdtl = 92;
                        
                        if (Dept == DeptDtl)
                        {
                            //for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
                            //{
                                Amt = Sm.GetGrdDec(Grd2, Row, 4);
                                if (Sm.GetGrdDec(Grd1, Row, Convert.ToInt32(Mthdtl + Mth)).ToString().Length <= 0) return false;
                                Amt2 = Sm.GetGrdDec(Grd1, Row, Convert.ToInt32(Mthdtl + Mth));
                                if (Amt < Amt2)
                                {
                                    if (Sm.StdMsgYN("Question",
                                       "Budget " + Sm.GetGrdStr(Grd2, Row, 5) + " is smaller than previous budget." + Environment.NewLine +
                                       "Do you still want to proceed ?") == DialogResult.No)
                                        return true;
                                }
                           // }
                        }
                }
            }
            return false;
        }

        private MySqlCommand InsertBudget(int Mth, int Row, int Col)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudget(Yr, DeptCode, Mth, Amt, UserCode, ApprovalDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Value(@Yr, @DeptCode, @Mth, @Amt, Null, Null, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.Right("0"+Mth.ToString(), 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, Col+3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBudget1());
            for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
            {
                int Index = 1;
                while (Index <= 12)
                {
                    if (Sm.GetGrdStr(Grd1, Row, Index * 7 + 3).Length > 0 &&
                        Sm.GetGrdDec(Grd1, Row, Index * 7 + 5) != Sm.GetGrdDec(Grd1, Row, Index * 7 + 6))

                        cml.Add(EditBudget2(Index, Row, Index * 7 + 3));
                    Index++;
                }
            }

            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueYr));
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsBudgetRequestAmtChanged() ||
                IsApprovedAmtLessThanUsedAmt();
        }

        private MySqlCommand EditBudget1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudget ");
            SQL.AppendLine("(Yr, Mth, DeptCode, Amt, UserCode, ApprovalDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.Yr, T.Mth, T.DeptCode, 0.00, Null, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblBudgetRequest T ");
            SQL.AppendLine("Where T.Yr=@Yr");
            SQL.AppendLine("And Concat(T.Yr, T.Mth, T.DeptCode) Not In ( ");
            SQL.AppendLine("    Select Concat(Yr, Mth, DeptCode) From TblBudget ");
            SQL.AppendLine("    Where Yr=@Yr ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditBudget2(int Mth, int Row, int Col)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudget Set ");
            SQL.AppendLine("    UserCode=Null, ");
            SQL.AppendLine("    ApprovalDt=Null, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime(), ");
            SQL.AppendLine("    Amt=@Amt ");
            SQL.AppendLine("Where Yr=@Yr And Mth=@Mth And DeptCode=@DeptCode ");
            SQL.AppendLine("And Not Exists(Select 1 From (Select 1 From TblBudget Where Yr=@Yr And Mth=@Mth And DeptCode=@DeptCode And Amt=@Amt) Tbl); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.Right("0" + Mth.ToString(), 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, Col + 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Approve Data
        
        private void ApproveData()
        {
            if (Sm.StdMsgYN("Approve", "") == DialogResult.No || IsApprovedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Update TblBudget Set " +
                    "   UserCode=@UserCode, ApprovalDt=Left(CurrentDateTime(), 8), LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where Yr=@Yr And UserCode Is Null And ApprovalDt Is Null; "
            };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            ShowData(Sm.GetLue(LueYr));
        }

        private bool IsApprovedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsBudgetAmtChanged();
        }

        private bool IsBudgetAmtChanged()
        {
            ShowBudgetAmt();

            for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
            {
                for (int Index = 1; Index <= 12; Index++)
                {
                    if (Grd1.Cells[Row, Index * 7 + 6].ForeColor == Color.Red)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Budget request changed already" + Environment.NewLine +
                            "(Check budget amount with red color)."
                            );
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region Show Data

        public void ShowData(string Yr)
        {
            try
            {
                ClearData();
                
                string DeptCode = string.Empty;
                int Row = -1;
                var Sql = new StringBuilder();

                Sql.AppendLine("Select A.DeptCode, ");
                if (mBudgetBasedOn == "1") Sql.AppendLine("E.DeptName, ");
                if (mBudgetBasedOn == "2") Sql.AppendLine("E.SiteName As DeptName, ");
                Sql.AppendLine("A.Mth, IfNull(B.Amt, 0) As Amt, B.ApprovalDt, IfNull(C.Amt, 0) As PrevAmt, IfNull(A.Amt, 0) As ReqAmt, ");
                Sql.AppendLine("Case When IfNull(D.TotalAmt, 0)=0 Then 0 Else IfNull(A.Amt, 0)/IfNull(D.TotalAmt, 0)*100 End As Percentage, ");
                Sql.AppendLine("Case When B.UserCode Is Not Null And B.ApprovalDt Is Not Null Then 'Approved' Else 'Outstanding' End As Status ");
                Sql.AppendLine("From TblBudgetRequest A ");
                Sql.AppendLine("Left join TblBudget B On A.Yr=B.Yr And A.DeptCode=B.DeptCode And A.Mth=B.Mth  ");
                Sql.AppendLine("Left Join TblBudget C On B.DeptCode=C.DeptCode And B.Mth=C.Mth And B.Yr=C.Yr-1 ");
                Sql.AppendLine("Left Join ( ");
                Sql.AppendLine("    Select Mth, Sum(Amt) TotalAmt ");
                Sql.AppendLine("    From TblBudgetRequest ");
                Sql.AppendLine("    Where Yr=@Yr Group By Mth ");
                Sql.AppendLine(") D On A.Mth=D.Mth ");
                if (mBudgetBasedOn=="1")
                    Sql.AppendLine("Inner Join TblDepartment E On A.DeptCode=E.DeptCode ");
                if (mBudgetBasedOn == "2")
                    Sql.AppendLine("Inner Join TblSite E On A.DeptCode=E.SiteCode ");
                Sql.AppendLine("Where A.Yr=@Yr ");
                if (mBudgetBasedOn == "1" && mIsFilterByDept)
                {
                    Sql.AppendLine("And Exists( ");
                    Sql.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    Sql.AppendLine("    Where DeptCode=A.DeptCode ");
                    Sql.AppendLine("    And GrpCode In ( ");
                    Sql.AppendLine("        Select GrpCode From TblUser ");
                    Sql.AppendLine("        Where UserCode=@UserCode ");
                    Sql.AppendLine("    ) ");
                    Sql.AppendLine(") ");
                }

                if (mBudgetBasedOn == "2" && mIsFilterBySite)
                {
                    Sql.AppendLine("And Exists( ");
                    Sql.AppendLine("    Select SiteCode From TblGroupSite ");
                    Sql.AppendLine("    Where SiteCode=A.DeptCode ");
                    Sql.AppendLine("    And GrpCode In ( ");
                    Sql.AppendLine("        Select GrpCode From TblUser ");
                    Sql.AppendLine("        Where UserCode=@UserCode ");
                    Sql.AppendLine("    ) ");
                    Sql.AppendLine(") ");
                }
                Sql.AppendLine("Order By ");
                if (mBudgetBasedOn == "1") Sql.AppendLine("E.DeptName, ");
                if (mBudgetBasedOn == "2") Sql.AppendLine("E.SiteName, ");
                Sql.AppendLine("A.Mth;");

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = Sql.ToString();

                    Sm.CmParam<String>(ref cm, "@Yr", Yr);
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        { 
                            "DeptCode", 
                            "Mth", "PrevAmt", "ReqAmt", "Percentage", "Amt", 
                            "Status", "DeptName" 
                        });
                    if (dr.HasRows)
                    {
                        BtnSave.Enabled = false;
                        Sm.SetLue(LueYr, Yr);
                        BtnSave.Enabled = true;

                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            if (!Sm.CompareStr(DeptCode, dr.GetString(c[0])))
                            {
                                DeptCode = dr.GetString(c[0]);
                                Grd1.Rows.Add();
                                Row++;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 92, 7);
                                for (int Col = 1; Col < 7; Col++)
                                {
                                    Grd1.Cells[Row, Col].Value = 0m;
                                }
                                Grd1.Cells[Row, 7].Value = null;
                            }
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 1].Value = dr.GetDecimal(c[2]);
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 2].Value =
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 3].Value = dr.GetDecimal(c[3]);
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 4].Value = dr.GetDecimal(c[4]);
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 5].Value =
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 6].Value = dr.GetDecimal(c[5]);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                        }
                        Row++;
                        Grd1.Cells[Row, 0].Value = "Total";
                        //for (int Col = 1; Col <= Grd1.Cols.Count - 1; Col++)
                        for (int Col = 1; Col <= 91; Col++)
                        {
                            if (Sm.IsGrdColSelected(new int[] { 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91 }, Col))
                                Grd1.Cells[Row, Col].Value = null;
                            else
                                Grd1.Cells[Row, Col].Value = 0m;
                        }
                        Grd1.Rows[Row].BackColor = Color.Coral;
                        ComputeTotal();
                        Grd1.EndUpdate();
                    }
                    dr.Close();

                    Sm.FocusGrd(Grd1, 0, 0);
                }

                if (mBudgetBasedOn == "1")
                    ShowDepartment();
                if (mBudgetBasedOn == "2")
                    ShowSite();

                ComputeTotal();
                ComputeSummaryTotalApproved();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsFilterByDept = Sm.GetParameter("IsFilterByDept") == "Y";
        }

        private void ShowDepartment()
        {
            try
            {
                int Row = 0;
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Distinct A.DeptCode, B.DeptName ");
                    SQL.AppendLine("From TblBudgetRequest A, TblDepartment B ");
                    SQL.AppendLine("Where A.DeptCode=B.DeptCode And A.Yr=@Yr ");
                    if(mIsFilterByDept)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ) ");
                    }
                    SQL.AppendLine("Order By B.DeptName");

                    cm.CommandText = SQL.ToString();

                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DeptCode", "DeptName" });
                    if (dr.HasRows)
                    {
                        Grd2.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd2.Rows.Add();
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 1);
                            Grd2.Cells[Row, 2].Value = 0m;
                            Grd2.Cells[Row, 3].Value = 0m;
                            Grd2.Cells[Row, 4].Value = 0m;
                            Row++;
                        }
                        Grd2.Cells[Row, 0].Value = "Total";
                        Grd2.Cells[Row, 2].Value = 0m;
                        Grd2.Cells[Row, 3].Value = 0m;
                        Grd2.Cells[Row, 4].Value = 0m;
                        Grd2.Rows[Row].BackColor = Color.Coral;
                        Grd2.EndUpdate();
                    }
                    dr.Close();

                    Sm.FocusGrd(Grd2, 0, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSite()
        {
            try
            {
                int Row = 0;
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Distinct A.DeptCode, B.SiteName ");
                    SQL.AppendLine("From TblBudgetRequest A, TblSite B ");
                    SQL.AppendLine("Where A.DeptCode=B.SiteCode And A.Yr=@Yr ");
                    if (mIsFilterBySite)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                        SQL.AppendLine("    Where SiteCode=A.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine("Order By B.SiteName ");

                    cm.CommandText = SQL.ToString();

                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DeptCode", "SiteName" });
                    if (dr.HasRows)
                    {
                        Grd2.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd2.Rows.Add();
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 1);
                            Grd2.Cells[Row, 2].Value = 0m;
                            Grd2.Cells[Row, 3].Value = 0m;
                            Grd2.Cells[Row, 4].Value = 0m;
                            Row++;
                        }
                        Grd2.Cells[Row, 0].Value = "Total";
                        Grd2.Cells[Row, 2].Value = 0m;
                        Grd2.Cells[Row, 3].Value = 0m;
                        Grd2.Cells[Row, 4].Value = 0m;
                        Grd2.Rows[Row].BackColor = Color.Coral;
                        Grd2.EndUpdate();
                    }
                    dr.Close();

                    Sm.FocusGrd(Grd2, 0, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetRequest()
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DeptCode, ");
                if (mBudgetBasedOn == "1") SQL.AppendLine("D.DeptName, ");
                if (mBudgetBasedOn == "2") SQL.AppendLine("D.SiteName As DeptName, ");
                SQL.AppendLine("A.Mth, A.Amt, IfNull(B.Amt, 0) As PrevAmt, ");
                SQL.AppendLine("Case When IfNull(C.TotalAmt, 0)=0 Then 0 Else A.Amt/IfNull(C.TotalAmt, 0)*100 End As Percentage ");
                SQL.AppendLine("From TblBudgetRequest A ");
                SQL.AppendLine("Left Join TblBudget B On A.DeptCode = B.DeptCode And A.Mth=B.Mth And A.Yr-1=B.Yr ");
                SQL.AppendLine("Left Join (Select Mth, Sum(Amt) TotalAmt From TblBudgetRequest Where Yr=@Yr Group By Mth) C On A.Mth=C.Mth ");
                
                if (mBudgetBasedOn=="1")
                    SQL.AppendLine("Inner Join TblDepartment D On A.DeptCode=D.DeptCode ");

                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("Inner Join TblSite D On A.DeptCode=D.SiteCode ");

                SQL.AppendLine("Where A.Yr=@Yr ");
                if (mBudgetBasedOn == "1" && mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }

                if (mBudgetBasedOn == "2" && mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                    SQL.AppendLine("Order By A.DeptCode, A.Mth;");
                }

                Grd1.BeginUpdate();
                string DeptCode = string.Empty;
                int Row = -1;
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { 
                        "DeptCode", 
                        "DeptName", "Mth", "PrevAmt", "Amt", "Percentage" 
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                            if (!Sm.CompareStr(DeptCode, dr.GetString(c[0])))
                            {
                                
                                Grd1.Rows.Add();
                                Row++;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 92, 1);
                                for (int Col = 1; Col < 7; Col++)
                                {
                                    Grd1.Cells[Row, Col].Value = 0m;
                                }
                                Grd1.Cells[Row, 7].Value = null;
                            }
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 1].Value = dr.GetDecimal(c[3]);
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 2].Value =
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 3].Value = dr.GetDecimal(c[4]);
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 4].Value = dr.GetDecimal(c[5]);
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 5].Value =
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 6].Value = 0m;
                            Grd1.Cells[Row, int.Parse(dr.GetString(c[2])) * 7 + 7].Value = null;

                            string a = dr.GetString(c[2]);
                            if (a.Length > 0 && dr.GetDecimal(c[3]) > 0)
                            {
                                if (a == "01")
                                {
                                    Grd1.Cells[Row, 93].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "02")
                                {
                                    Grd1.Cells[Row, 94].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "03")
                                {
                                    Grd1.Cells[Row, 95].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "04")
                                {
                                    Grd1.Cells[Row, 96].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "05")
                                {
                                    Grd1.Cells[Row, 97].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "06")
                                {
                                    Grd1.Cells[Row, 98].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "07")
                                {
                                    Grd1.Cells[Row, 99].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "08")
                                {
                                    Grd1.Cells[Row, 100].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "09")
                                {
                                    Grd1.Cells[Row, 101].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "10")
                                {
                                    Grd1.Cells[Row, 102].Value = dr.GetDecimal(c[3]);
                                }
                                else if (a == "11")
                                {
                                    Grd1.Cells[Row, 103].Value = dr.GetDecimal(c[3]);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 104].Value = dr.GetDecimal(c[3]);
                                }
                            }
                            DeptCode = dr.GetString(c[0]);

                    }
                        Row++;

                        Grd1.Cells[Row, 0].Value = "Total";
                        for (int Col = 1; Col <= 91; Col++)
                        {
                            if (Sm.IsGrdColSelected(new int[] { 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91 }, Col))
                                Grd1.Cells[Row, Col].Value = null;
                            else
                                Grd1.Cells[Row, Col].Value = 0m;
                        }

                        Grd1.Rows[Row].BackColor = Color.Coral;

                        ComputeTotal();
                    }
                    dr.Close();
                    Sm.FocusGrd(Grd1, 0, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd1.EndUpdate();
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetRequestAmt()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
            {
                for (int Index = 1; Index <= 12; Index++)
                {
                    Grd1.Cells[Row, Index * 7 + 2].Value = Grd1.Cells[Row, Index * 7 + 3].Value;
                    Grd1.Cells[Row, Index * 7 + 3].ForeColor = Color.Black;
                }
            }

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText =
                    "Select A.DeptCode, A.Mth, A.Amt, " +
                    "Case When IfNull(B.TotalAmt, 0)=0 Then 0 Else A.Amt/IfNull(B.TotalAmt, 0)*100 End As Percentage " +
                    "From TblBudgetRequest A " +
                    "Left Join (Select Mth, Sum(Amt) TotalAmt From TblBudgetRequest Where Yr=@Yr Group By Mth) B On A.Mth=B.Mth " +
                    "Where A.Yr=@Yr " +
                    "Order By A.DeptCode, A.Mth";
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DeptCode", "Mth", "Amt", "Percentage" });
                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                            Grd1.BeginUpdate();
                            while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd1.Rows.Count-2; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), dr.GetString(c[0])))
                            {
                                Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 3].Value = dr.GetDecimal(c[2]);
                                Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 4].Value = dr.GetDecimal(c[3]);
                                break;
                            }
                        }
                    }
                    ComputeTotal();

                    for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
                    {
                        for (int Index = 1; Index <= 12; Index++)
                            if (Sm.GetGrdDec(Grd1, Row, Index * 7 + 2) != Sm.GetGrdDec(Grd1, Row, Index * 7 + 3))
                                Grd1.Cells[Row, Index * 7 + 3].ForeColor = Color.Red;
                    }

                    Grd1.EndUpdate();
                }
                dr.Close();

                Sm.FocusGrd(Grd1, 0, 0);
            }
            
        }

        private void ShowBudgetAmt()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
            {
                for (int Index = 1; Index <= 12; Index++)
                {
                    Grd1.Cells[Row, Index * 7 + 5].Value = Grd1.Cells[Row, Index * 7 + 6].Value;
                    Grd1.Cells[Row, Index * 7 + 6].ForeColor = Color.Black;
                }
            }

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText =
                    "Select DeptCode, Mth, Amt From TblBudget " +
                    "Where Yr=@Yr Order By DeptCode, Mth;";
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DeptCode", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), dr.GetString(c[0])))
                            {
                                Grd1.Cells[Row, int.Parse(dr.GetString(c[1])) * 7 + 6].Value = dr.GetDecimal(c[2]);

                                break;
                            }
                        }
                    }
                    ComputeTotal();

                    for (int Row = 0; Row <= Grd1.Rows.Count - 2; Row++)
                    {
                        for (int Index = 1; Index <= 12; Index++)
                            if (Sm.GetGrdDec(Grd1, Row, Index * 7 + 5) != Sm.GetGrdDec(Grd1, Row, Index * 7 + 6))
                                Grd1.Cells[Row, Index * 7 + 6].ForeColor = Color.Red;
                    }

                    Grd1.EndUpdate();
                }
                dr.Close();

                Sm.FocusGrd(Grd1, 0, 0);
            }

        }

        private void ComputeTotal()
        {
            decimal Amt = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length!=0)
                {
                    for (int Col = 1; Col <= 6; Col++)
                    {
                        Amt = 0m;
                        for (int Index = 1; Index <= 12; Index++)
                            Amt += Sm.GetGrdDec(Grd1, Row, Index*7+Col);
                        Grd1.Cells[Row, Col].Value = Amt;
                    }
                }
            }

            if (Grd1.Rows.Count >= 2)
            {
                int LastRow = Grd1.Rows.Count - 1;
                for (int Col = 1; Col <= 91; Col++)
                {
                    if (Col != 7)
                    {
                        Amt = 0m;
                        for (int Row = 0; Row <= LastRow - 1; Row++)
                        {
                            Amt += Sm.GetGrdDec(Grd1, Row, Col);
                        }
                        Grd1.Cells[LastRow, Col].Value = Amt;
                    }
                }
            }
        }

        private void ComputeTotalApprovedAmt(int Row, int Col)
        {
            decimal Amt = 0m;
            int C = Col % 7;  
            for (int Index = 1; Index <= 12; Index++)
                Amt += Sm.GetGrdDec(Grd1, Row, Index * 7 + C);
            Grd1.Cells[Row, C].Value = Amt;

            if (Grd1.Rows.Count >= 2)
            {
                Amt = 0m;

                for (int Index = 0; Index <= Grd1.Rows.Count - 2; Index++)
                    Amt += Sm.GetGrdDec(Grd1, Index, C);
                Grd1.Cells[Grd1.Rows.Count - 1, C].Value = Amt;

                Amt = 0m;
                for (int Index = 0; Index <= Grd1.Rows.Count-2; Index++)
                    Amt += Sm.GetGrdDec(Grd1, Index, Col);
                Grd1.Cells[Grd1.Rows.Count-1, Col].Value = Amt;
            }

        }

        private void ComputeSummaryTotalApproved()
        {
            if (Grd2.Rows.Count > 0)
            {
                for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                {
                    for (int Row1 = 0; Row1 < Grd1.Rows.Count; Row1++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row1, 0) == Sm.GetGrdStr(Grd2, Row2, 0))
                        {
                            decimal x = Sm.GetGrdDec(Grd1, Row1, 5);
                            Grd2.Cells[Row2, 4].Value = x;
                            Grd2.Cells[Row2, 2].Value = Sm.GetGrdDec(Grd1, Row1, 2);
                        }
                    }
                }
            }
        }

        private void ShowMonthlyBudget(int Col)
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd2.Cells[Row, 0].Value = Sm.GetGrdStr(Grd1, Row, 0);
                Grd2.Cells[Row, 92].Value = Sm.GetGrdStr(Grd1, Row, 5);
                Grd2.Cells[Row, 1].Value = Sm.GetGrdDec(Grd1, Row, Col);
                Grd2.Cells[Row, 2].Value = Sm.GetGrdDec(Grd1, Row, Col+2);
                Grd2.Cells[Row, 3].Value = Sm.GetGrdDec(Grd1, Row, Col+3);
                Grd2.Cells[Row, 4].Value = Sm.GetGrdDec(Grd1, Row, Col+5);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            LueMth.EditValue = null;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);

            if (Sm.GetLue(LueYr).Length != 0 && 
                BtnSave.Enabled)
            {
                if (mBudgetBasedOn == "1") ShowDepartment();
                if (mBudgetBasedOn == "2") ShowSite();
                ShowBudgetRequest();
            }
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd2, true);

            if (Sm.GetLue(LueMth).Length != 0)
            {
                int Col = int.Parse(Sm.GetLue(LueMth)) * 7;
                Grd2.Rows.Count = Grd1.Rows.Count;
                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                {
                    Grd2.Cells[Row, 0].Value = Sm.GetGrdStr(Grd1, Row, 0);
                    Grd2.Cells[Row, 5].Value = Sm.GetGrdStr(Grd1, Row, 92);
                    Grd2.Cells[Row, 1].Value = Sm.GetGrdDec(Grd1, Row, Col + 1);
                    Grd2.Cells[Row, 2].Value = Sm.GetGrdDec(Grd1, Row, Col + 3);
                    Grd2.Cells[Row, 3].Value = Sm.GetGrdDec(Grd1, Row, Col + 4);
                    Grd2.Cells[Row, 4].Value = Sm.GetGrdDec(Grd1, Row, Col + 6);
                }
                Grd2.Rows[Grd2.Rows.Count - 1].BackColor = Color.Coral;
            }
            else
            {
                if (mBudgetBasedOn == "1") ShowDepartment();
                if (mBudgetBasedOn == "2") ShowSite();
            }
        }


        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(
               BtnSave.Enabled &&
               (Sm.GetLue(LueMth).Length!=0) &&
               Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex) &&
               Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0 &&
               !Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 0), "Total")
               ))
                e.DoDefault = false;
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (Grd2.Rows.Count >= 2)
                {
                    decimal Amt = 0;
                    for (int Row = 0; Row <= Grd2.Rows.Count - 2; Row++)
                        Amt += Sm.GetGrdDec(Grd2, Row, 4);
                    Grd2.Cells[Grd2.Rows.Count - 1, 4].Value = Amt;

                    int Col = int.Parse(Sm.GetLue(LueMth))*7+6;
                    Grd1.Cells[e.RowIndex, Col].Value = Sm.GetGrdDec(Grd2, e.RowIndex, 4);
                    ComputeTotalApprovedAmt(e.RowIndex, Col);
                }
            }               
        }

        #endregion

        #region Button Event

        private void BtnApprove_Click(object sender, EventArgs e)
        {
            ApproveData();
        }

        #endregion

        #endregion
    }
}
