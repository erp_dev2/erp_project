﻿#region Update
/* 
    08/07/2017 [TKG] bug muncul bin dengan beda lot.
    08/12/2017 [TKG] bug muncul ketika ada bin dengan lot yg berbeda.
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBinTransferDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBinTransfer mFrmParent;
        private string mSQL = string.Empty, mBinTransferRequestDocNo = string.Empty;
        bool mFirst = true;

        #endregion

        #region Constructor

        public FrmBinTransferDlg2(FrmBinTransfer FrmParent, string BinTransferRequestDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBinTransferRequestDocNo = BinTransferRequestDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, Group_Concat(Distinct B.Lot Order By B.Lot Separator ', ') As Lot ");
            SQL.AppendLine("From TblBinTransferRequestDtl A ");
            SQL.AppendLine("Inner Join TblBinTransferRequestDtl2 B On A.DocNo=B.DocNo And A.Bin=B.Bin ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.ProcessInd<>'F' ");
            SQL.AppendLine("And A.DocNo=@BinTransferRequestDocNo ");
            //SQL.AppendLine("And Locate(Concat('##', A.Bin, '##'), @SelectedBin)<1 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(Grd1, 
                new string[] { 
                    "No", 
                    "", "Bin", "Lot" 
                }, 
                new int[] { 
                    50, 
                    20, 130, 130 
                });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string Filter = string.Empty;
                var Bin = string.Empty;
                var cm = new MySqlCommand();

                if (mFrmParent.Grd2.Rows.Count >= 0)
                {
                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                    {
                        Bin = Sm.GetGrdStr(mFrmParent.Grd2, r, 1);
                        if (Bin.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(A.Bin=@Bin0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Bin0" + r.ToString(), Bin);
                        }
                    }
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@BinTransferRequestDocNo", mBinTransferRequestDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By A.Bin Order By A.Bin;",
                        new string[] { "Bin", "Lot" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            bool IsChoose = false;
            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                mFrmParent.Grd2.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !IsBinAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Sm.CopyGrdValue(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, 1, Grd1, Row, 2);
                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 2, 4, 6 });
                        mFrmParent.InsertItem(Sm.GetGrdStr(Grd1, Row, 2));
                    }
                }
                mFrmParent.Grd1.EndUpdate();
                mFrmParent.Grd2.EndUpdate();
                if (!IsChoose)
                    Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 bin.");
                else
                    if (mFirst) mFirst = false;
            }
        }

        private bool IsBinAlreadyChosen(int Row)
        {
            if (mFirst) return false;
            string Key = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 1), Key))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
