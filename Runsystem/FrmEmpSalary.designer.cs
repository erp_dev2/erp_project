﻿namespace RunSystem
{
    partial class FrmEmpSalary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmpSalary));
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPosCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TcEmpSalary = new DevExpress.XtraTab.XtraTabControl();
            this.TpSalary = new DevExpress.XtraTab.XtraTabPage();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpAllowance = new DevExpress.XtraTab.XtraTabPage();
            this.DteEndDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt2 = new DevExpress.XtraEditors.DateEdit();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpDeduction = new DevExpress.XtraTab.XtraTabPage();
            this.DteEndDtDed = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDtDed = new DevExpress.XtraEditors.DateEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpSalarySS = new DevExpress.XtraTab.XtraTabPage();
            this.DteEndDt3 = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt3 = new DevExpress.XtraEditors.DateEdit();
            this.LueSSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpClaim = new DevExpress.XtraTab.XtraTabPage();
            this.DteClaimEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteClaimStartDt = new DevExpress.XtraEditors.DateEdit();
            this.LueClaimCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpPerformance = new DevExpress.XtraTab.XtraTabPage();
            this.LueGradePerformance = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.LuePerformanceStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.TpApproval = new DevExpress.XtraTab.XtraTabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcEmpSalary)).BeginInit();
            this.TcEmpSalary.SuspendLayout();
            this.TpSalary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpAllowance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpDeduction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDtDed.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDtDed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDtDed.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDtDed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpSalarySS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpClaim.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClaimCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpPerformance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueGradePerformance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePerformanceStatus.Properties)).BeginInit();
            this.TpApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcEmpSalary);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.TxtSiteCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.BtnEmpCode);
            this.panel3.Controls.Add(this.DteJoinDt);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.TxtDeptCode);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TxtPosCode);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtEmpCodeOld);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.TxtEmpCode);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 244);
            this.panel3.TabIndex = 10;
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(131, 172);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 1000;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(500, 20);
            this.TxtSiteCode.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(100, 175);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 14);
            this.label1.TabIndex = 28;
            this.label1.Text = "Site";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(131, 214);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(500, 20);
            this.MeeRemark.TabIndex = 33;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(81, 217);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 32;
            this.label12.Text = "Remark";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(131, 25);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 20;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(104, 20);
            this.TxtStatus.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(86, 28);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 13;
            this.label14.Text = "Status";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(131, 46);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(104, 20);
            this.DteDocDt.TabIndex = 16;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(95, 49);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 15;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(131, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 1000;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(339, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(55, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 14);
            this.label18.TabIndex = 11;
            this.label18.Text = "Document#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(476, 66);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 19;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(131, 193);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.ReadOnly = true;
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(104, 20);
            this.DteJoinDt.TabIndex = 31;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(70, 196);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 14);
            this.label10.TabIndex = 30;
            this.label10.Text = "Join Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(131, 151);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 1000;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(500, 20);
            this.TxtDeptCode.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(55, 154);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 14);
            this.label9.TabIndex = 26;
            this.label9.Text = "Department";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosCode
            // 
            this.TxtPosCode.EnterMoveNextControl = true;
            this.TxtPosCode.Location = new System.Drawing.Point(131, 130);
            this.TxtPosCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosCode.Name = "TxtPosCode";
            this.TxtPosCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCode.Properties.MaxLength = 1000;
            this.TxtPosCode.Properties.ReadOnly = true;
            this.TxtPosCode.Size = new System.Drawing.Size(500, 20);
            this.TxtPosCode.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(79, 133);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Position";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(131, 109);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 1000;
            this.TxtEmpCodeOld.Properties.ReadOnly = true;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(500, 20);
            this.TxtEmpCodeOld.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 14);
            this.label6.TabIndex = 22;
            this.label6.Text = "Employee\'s Old Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(131, 88);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 1000;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(500, 20);
            this.TxtEmpName.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(25, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "Employee\'s Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(131, 67);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 1000;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(339, 20);
            this.TxtEmpCode.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(28, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Employee\'s Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcEmpSalary
            // 
            this.TcEmpSalary.AppearancePage.Header.Options.UseTextOptions = true;
            this.TcEmpSalary.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TcEmpSalary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcEmpSalary.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcEmpSalary.Location = new System.Drawing.Point(0, 244);
            this.TcEmpSalary.Name = "TcEmpSalary";
            this.TcEmpSalary.SelectedTabPage = this.TpSalary;
            this.TcEmpSalary.Size = new System.Drawing.Size(772, 229);
            this.TcEmpSalary.TabIndex = 34;
            this.TcEmpSalary.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpSalary,
            this.TpAllowance,
            this.TpDeduction,
            this.TpSalarySS,
            this.TpClaim,
            this.TpPerformance,
            this.TpApproval});
            // 
            // TpSalary
            // 
            this.TpSalary.Controls.Add(this.DteStartDt);
            this.TpSalary.Controls.Add(this.Grd1);
            this.TpSalary.Name = "TpSalary";
            this.TpSalary.Size = new System.Drawing.Size(766, 201);
            this.TpSalary.Text = "Salary";
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(87, 22);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(156, 20);
            this.DteStartDt.TabIndex = 36;
            this.DteStartDt.Visible = false;
            this.DteStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt_KeyDown);
            this.DteStartDt.Leave += new System.EventHandler(this.DteStartDt_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 201);
            this.Grd1.TabIndex = 35;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // TpAllowance
            // 
            this.TpAllowance.Controls.Add(this.DteEndDt2);
            this.TpAllowance.Controls.Add(this.DteStartDt2);
            this.TpAllowance.Controls.Add(this.LueSiteCode);
            this.TpAllowance.Controls.Add(this.Grd2);
            this.TpAllowance.Name = "TpAllowance";
            this.TpAllowance.Size = new System.Drawing.Size(766, 201);
            this.TpAllowance.Text = "Allowance";
            // 
            // DteEndDt2
            // 
            this.DteEndDt2.EditValue = null;
            this.DteEndDt2.EnterMoveNextControl = true;
            this.DteEndDt2.Location = new System.Drawing.Point(294, 22);
            this.DteEndDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt2.Name = "DteEndDt2";
            this.DteEndDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt2.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt2.Size = new System.Drawing.Size(156, 20);
            this.DteEndDt2.TabIndex = 37;
            this.DteEndDt2.Visible = false;
            this.DteEndDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt2_KeyDown);
            this.DteEndDt2.Leave += new System.EventHandler(this.DteEndDt2_Leave);
            // 
            // DteStartDt2
            // 
            this.DteStartDt2.EditValue = null;
            this.DteStartDt2.EnterMoveNextControl = true;
            this.DteStartDt2.Location = new System.Drawing.Point(106, 22);
            this.DteStartDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt2.Name = "DteStartDt2";
            this.DteStartDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt2.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt2.Size = new System.Drawing.Size(156, 20);
            this.DteStartDt2.TabIndex = 36;
            this.DteStartDt2.Visible = false;
            this.DteStartDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt2_KeyDown);
            this.DteStartDt2.Leave += new System.EventHandler(this.DteStartDt2_Leave);
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(488, 22);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 25;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 220;
            this.LueSiteCode.Size = new System.Drawing.Size(220, 20);
            this.LueSiteCode.TabIndex = 38;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            this.LueSiteCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueSiteCode_KeyDown);
            this.LueSiteCode.Leave += new System.EventHandler(this.LueSiteCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 201);
            this.Grd2.TabIndex = 35;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpDeduction
            // 
            this.TpDeduction.Controls.Add(this.DteEndDtDed);
            this.TpDeduction.Controls.Add(this.DteStartDtDed);
            this.TpDeduction.Controls.Add(this.Grd3);
            this.TpDeduction.Name = "TpDeduction";
            this.TpDeduction.Size = new System.Drawing.Size(766, 201);
            this.TpDeduction.Text = "Deduction";
            // 
            // DteEndDtDed
            // 
            this.DteEndDtDed.EditValue = null;
            this.DteEndDtDed.EnterMoveNextControl = true;
            this.DteEndDtDed.Location = new System.Drawing.Point(265, 24);
            this.DteEndDtDed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDtDed.Name = "DteEndDtDed";
            this.DteEndDtDed.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDtDed.Properties.Appearance.Options.UseFont = true;
            this.DteEndDtDed.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDtDed.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDtDed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDtDed.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDtDed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDtDed.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDtDed.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDtDed.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDtDed.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDtDed.Size = new System.Drawing.Size(156, 20);
            this.DteEndDtDed.TabIndex = 39;
            this.DteEndDtDed.Visible = false;
            this.DteEndDtDed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDtDed_KeyDown);
            this.DteEndDtDed.Leave += new System.EventHandler(this.DteEndDtDed_Leave);
            // 
            // DteStartDtDed
            // 
            this.DteStartDtDed.EditValue = null;
            this.DteStartDtDed.EnterMoveNextControl = true;
            this.DteStartDtDed.Location = new System.Drawing.Point(77, 24);
            this.DteStartDtDed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDtDed.Name = "DteStartDtDed";
            this.DteStartDtDed.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDtDed.Properties.Appearance.Options.UseFont = true;
            this.DteStartDtDed.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDtDed.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDtDed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDtDed.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDtDed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDtDed.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDtDed.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDtDed.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDtDed.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDtDed.Size = new System.Drawing.Size(156, 20);
            this.DteStartDtDed.TabIndex = 38;
            this.DteStartDtDed.Visible = false;
            this.DteStartDtDed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDtDed_KeyDown);
            this.DteStartDtDed.Leave += new System.EventHandler(this.DteStartDtDed_Leave);
            // 
            // Grd3
            // 
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 201);
            this.Grd3.TabIndex = 35;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpSalarySS
            // 
            this.TpSalarySS.Controls.Add(this.DteEndDt3);
            this.TpSalarySS.Controls.Add(this.DteStartDt3);
            this.TpSalarySS.Controls.Add(this.LueSSPCode);
            this.TpSalarySS.Controls.Add(this.Grd5);
            this.TpSalarySS.Name = "TpSalarySS";
            this.TpSalarySS.Size = new System.Drawing.Size(766, 0);
            this.TpSalarySS.Text = "Salary (Social Security)";
            // 
            // DteEndDt3
            // 
            this.DteEndDt3.EditValue = null;
            this.DteEndDt3.EnterMoveNextControl = true;
            this.DteEndDt3.Location = new System.Drawing.Point(192, 22);
            this.DteEndDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt3.Name = "DteEndDt3";
            this.DteEndDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt3.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt3.Size = new System.Drawing.Size(104, 20);
            this.DteEndDt3.TabIndex = 37;
            this.DteEndDt3.Visible = false;
            this.DteEndDt3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt3_KeyDown);
            this.DteEndDt3.Leave += new System.EventHandler(this.DteEndDt3_Leave);
            // 
            // DteStartDt3
            // 
            this.DteStartDt3.EditValue = null;
            this.DteStartDt3.EnterMoveNextControl = true;
            this.DteStartDt3.Location = new System.Drawing.Point(82, 22);
            this.DteStartDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt3.Name = "DteStartDt3";
            this.DteStartDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt3.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt3.Size = new System.Drawing.Size(104, 20);
            this.DteStartDt3.TabIndex = 36;
            this.DteStartDt3.Visible = false;
            this.DteStartDt3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt3_KeyDown);
            this.DteStartDt3.Leave += new System.EventHandler(this.DteStartDt3_Leave);
            // 
            // LueSSPCode
            // 
            this.LueSSPCode.EnterMoveNextControl = true;
            this.LueSSPCode.Location = new System.Drawing.Point(302, 22);
            this.LueSSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSSPCode.Name = "LueSSPCode";
            this.LueSSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSSPCode.Properties.DropDownRows = 25;
            this.LueSSPCode.Properties.NullText = "[Empty]";
            this.LueSSPCode.Properties.PopupWidth = 220;
            this.LueSSPCode.Size = new System.Drawing.Size(220, 20);
            this.LueSSPCode.TabIndex = 38;
            this.LueSSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSSPCode.EditValueChanged += new System.EventHandler(this.LueSSPCode_EditValueChanged);
            this.LueSSPCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueSSPCode_KeyDown);
            this.LueSSPCode.Leave += new System.EventHandler(this.LueSSPCode_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 0);
            this.Grd5.TabIndex = 35;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpClaim
            // 
            this.TpClaim.Controls.Add(this.DteClaimEndDt);
            this.TpClaim.Controls.Add(this.DteClaimStartDt);
            this.TpClaim.Controls.Add(this.LueClaimCode);
            this.TpClaim.Controls.Add(this.Grd6);
            this.TpClaim.Name = "TpClaim";
            this.TpClaim.Size = new System.Drawing.Size(766, 0);
            this.TpClaim.Text = "Claim";
            // 
            // DteClaimEndDt
            // 
            this.DteClaimEndDt.EditValue = null;
            this.DteClaimEndDt.EnterMoveNextControl = true;
            this.DteClaimEndDt.Location = new System.Drawing.Point(529, 20);
            this.DteClaimEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteClaimEndDt.Name = "DteClaimEndDt";
            this.DteClaimEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteClaimEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteClaimEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteClaimEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteClaimEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteClaimEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteClaimEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteClaimEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteClaimEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteClaimEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteClaimEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteClaimEndDt.Size = new System.Drawing.Size(138, 20);
            this.DteClaimEndDt.TabIndex = 59;
            this.DteClaimEndDt.Visible = false;
            this.DteClaimEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteClaimEndDt_KeyDown);
            this.DteClaimEndDt.Leave += new System.EventHandler(this.DteClaimEndDt_Leave);
            // 
            // DteClaimStartDt
            // 
            this.DteClaimStartDt.EditValue = null;
            this.DteClaimStartDt.EnterMoveNextControl = true;
            this.DteClaimStartDt.Location = new System.Drawing.Point(377, 21);
            this.DteClaimStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteClaimStartDt.Name = "DteClaimStartDt";
            this.DteClaimStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteClaimStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteClaimStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteClaimStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteClaimStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteClaimStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteClaimStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteClaimStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteClaimStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteClaimStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteClaimStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteClaimStartDt.Size = new System.Drawing.Size(138, 20);
            this.DteClaimStartDt.TabIndex = 60;
            this.DteClaimStartDt.Visible = false;
            this.DteClaimStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteClaimStartDt_KeyDown);
            this.DteClaimStartDt.Leave += new System.EventHandler(this.DteClaimStartDt_Leave);
            // 
            // LueClaimCode
            // 
            this.LueClaimCode.EnterMoveNextControl = true;
            this.LueClaimCode.Location = new System.Drawing.Point(138, 20);
            this.LueClaimCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueClaimCode.Name = "LueClaimCode";
            this.LueClaimCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClaimCode.Properties.Appearance.Options.UseFont = true;
            this.LueClaimCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClaimCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueClaimCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClaimCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueClaimCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClaimCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueClaimCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClaimCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueClaimCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueClaimCode.Properties.DropDownRows = 25;
            this.LueClaimCode.Properties.NullText = "[Empty]";
            this.LueClaimCode.Properties.PopupWidth = 220;
            this.LueClaimCode.Size = new System.Drawing.Size(220, 20);
            this.LueClaimCode.TabIndex = 36;
            this.LueClaimCode.ToolTip = "F4 : Show/hide list";
            this.LueClaimCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueClaimCode.EditValueChanged += new System.EventHandler(this.LueClaimCode_EditValueChanged);
            this.LueClaimCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueClaimCode_KeyDown);
            this.LueClaimCode.Leave += new System.EventHandler(this.LueClaimCode_Leave);
            // 
            // Grd6
            // 
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 0);
            this.Grd6.TabIndex = 35;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd6_ColHdrDoubleClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // TpPerformance
            // 
            this.TpPerformance.Controls.Add(this.LueGradePerformance);
            this.TpPerformance.Controls.Add(this.Grd7);
            this.TpPerformance.Controls.Add(this.panel4);
            this.TpPerformance.Name = "TpPerformance";
            this.TpPerformance.Size = new System.Drawing.Size(766, 0);
            this.TpPerformance.Text = "Performance";
            // 
            // LueGradePerformance
            // 
            this.LueGradePerformance.EnterMoveNextControl = true;
            this.LueGradePerformance.Location = new System.Drawing.Point(170, 52);
            this.LueGradePerformance.Margin = new System.Windows.Forms.Padding(5);
            this.LueGradePerformance.Name = "LueGradePerformance";
            this.LueGradePerformance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradePerformance.Properties.Appearance.Options.UseFont = true;
            this.LueGradePerformance.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradePerformance.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGradePerformance.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradePerformance.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGradePerformance.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradePerformance.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGradePerformance.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGradePerformance.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGradePerformance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGradePerformance.Properties.DropDownRows = 25;
            this.LueGradePerformance.Properties.NullText = "[Empty]";
            this.LueGradePerformance.Properties.PopupWidth = 220;
            this.LueGradePerformance.Size = new System.Drawing.Size(163, 20);
            this.LueGradePerformance.TabIndex = 39;
            this.LueGradePerformance.ToolTip = "F4 : Show/hide list";
            this.LueGradePerformance.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGradePerformance.EditValueChanged += new System.EventHandler(this.LueGradePerformance_EditValueChanged);
            this.LueGradePerformance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueGradePerformance_KeyDown);
            this.LueGradePerformance.Leave += new System.EventHandler(this.LueGradePerformance_Leave);
            // 
            // Grd7
            // 
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 32);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(766, 0);
            this.Grd7.TabIndex = 38;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd7_AfterCommitEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.LuePerformanceStatus);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 32);
            this.panel4.TabIndex = 35;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 14);
            this.label7.TabIndex = 36;
            this.label7.Text = "Performance Status";
            // 
            // LuePerformanceStatus
            // 
            this.LuePerformanceStatus.EnterMoveNextControl = true;
            this.LuePerformanceStatus.Location = new System.Drawing.Point(128, 6);
            this.LuePerformanceStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LuePerformanceStatus.Name = "LuePerformanceStatus";
            this.LuePerformanceStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePerformanceStatus.Properties.Appearance.Options.UseFont = true;
            this.LuePerformanceStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePerformanceStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePerformanceStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePerformanceStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePerformanceStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePerformanceStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePerformanceStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePerformanceStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePerformanceStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePerformanceStatus.Properties.DropDownRows = 10;
            this.LuePerformanceStatus.Properties.MaxLength = 16;
            this.LuePerformanceStatus.Properties.NullText = "[Empty]";
            this.LuePerformanceStatus.Properties.PopupWidth = 350;
            this.LuePerformanceStatus.Size = new System.Drawing.Size(249, 20);
            this.LuePerformanceStatus.TabIndex = 37;
            this.LuePerformanceStatus.ToolTip = "F4 : Show/hide list";
            this.LuePerformanceStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePerformanceStatus.EditValueChanged += new System.EventHandler(this.LuePerformanceStatus_EditValueChanged);
            // 
            // TpApproval
            // 
            this.TpApproval.Controls.Add(this.Grd4);
            this.TpApproval.Name = "TpApproval";
            this.TpApproval.Size = new System.Drawing.Size(766, 0);
            this.TpApproval.Text = "Approval Information";
            // 
            // Grd4
            // 
            this.Grd4.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd4.BackColorOddRows = System.Drawing.Color.White;
            this.Grd4.DefaultAutoGroupRow.Height = 21;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.FrozenArea.ColCount = 3;
            this.Grd4.FrozenArea.SortFrozenRows = true;
            this.Grd4.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd4.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd4.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd4.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.UseXPStyles = false;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd4.Name = "Grd4";
            this.Grd4.ProcessTab = false;
            this.Grd4.ReadOnly = true;
            this.Grd4.RowTextStartColNear = 3;
            this.Grd4.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd4.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd4.SearchAsType.SearchCol = null;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 0);
            this.Grd4.TabIndex = 35;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmEmpSalary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmEmpSalary";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcEmpSalary)).EndInit();
            this.TcEmpSalary.ResumeLayout(false);
            this.TpSalary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpAllowance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpDeduction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDtDed.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDtDed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDtDed.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDtDed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpSalarySS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpClaim.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteClaimStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClaimCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpPerformance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueGradePerformance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePerformanceStatus.Properties)).EndInit();
            this.TpApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        internal DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPosCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraTab.XtraTabControl TcEmpSalary;
        private DevExpress.XtraTab.XtraTabPage TpSalary;
        private DevExpress.XtraTab.XtraTabPage TpAllowance;
        private DevExpress.XtraTab.XtraTabPage TpDeduction;
        private DevExpress.XtraTab.XtraTabPage TpApproval;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        internal DevExpress.XtraEditors.DateEdit DteStartDt2;
        internal DevExpress.XtraEditors.DateEdit DteEndDt2;
        private DevExpress.XtraTab.XtraTabPage TpSalarySS;
        internal DevExpress.XtraEditors.DateEdit DteEndDt3;
        internal DevExpress.XtraEditors.DateEdit DteStartDt3;
        private DevExpress.XtraEditors.LookUpEdit LueSSPCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private DevExpress.XtraTab.XtraTabPage TpClaim;
        private DevExpress.XtraEditors.LookUpEdit LueClaimCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private DevExpress.XtraTab.XtraTabPage TpPerformance;
        private DevExpress.XtraEditors.LookUpEdit LueGradePerformance;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.LookUpEdit LuePerformanceStatus;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteEndDtDed;
        internal DevExpress.XtraEditors.DateEdit DteStartDtDed;
        internal DevExpress.XtraEditors.DateEdit DteClaimEndDt;
        internal DevExpress.XtraEditors.DateEdit DteClaimStartDt;
    }
}