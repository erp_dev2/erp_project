﻿#region Update
/*
    18/09/2019 [TKG/IMS] tambah division site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSiteDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSite mFrmParent;

        #endregion

        #region Constructor

        public FrmSiteDlg(FrmSite FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-4
                        "",
                        "Division's Code",
                        "Divison's Name"
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string DivisionCode = string.Empty, Filter = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DivisionCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (DivisionCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += string.Concat("(DivisionCode<>@DivisionCode0", r.ToString(), ") ");
                            Sm.CmParam<String>(ref cm, "@DivisionCode0" + r.ToString(), DivisionCode);
                        }
                    }
                }
                if (Filter.Length != 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.FilterStr(ref Filter, ref cm, TxtDivisionCode.Text, "DivisionName", false);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        string.Concat("Select DivisionCode, DivisionName From TblDivision Where ActInd='Y' ", Filter, " Order By DivisionName;"),
                        new string[] { "DivisionCode", "DivisionName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);

                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 division.");
        }

        private bool IsCodeAlreadyChosen(int r)
        {
            var DivCode = Sm.GetGrdStr(Grd1, r, 2);
            for (int i = 0; i< mFrmParent.Grd1.Rows.Count - 1; i++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, i, 1), DivCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDivisionCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkDivisionCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Division");
        }

        #endregion

        #endregion

    }
}
