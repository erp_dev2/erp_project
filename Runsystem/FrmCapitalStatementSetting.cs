﻿#region Update
/*
 * 10/08/2021 [ICA/ALL] New Apps
   19/08/2021 [DITA/ALL] ubah compute balance supaya dapat menghitung multiple coa per baris nya
   23/08/2021 [MYA/AMKA] Custom menu "Capital Statement"
   06/09/2021 [MYA/AMKA] Bug Muncul warning ketika akan save menu Capital Statement Setting
   15/11/2021 [RDA/AMKA] filter profit center terfilter berdasarkan group user based on parameter IsFilterByProfitCenter
   07/12/2021 [YOG/AMKA] Penyesuaian format input parameter dan reporting pada menu Capital Statement Setting 
   02/09/2022 [VIN/AMKA] OB yg masih aktif yg dihitung
   24/02/2022 [YOG/AMKA] Feedback : Ketika formula balance dicentang maka yang ditampilkan hanya Opening Balance, Jika tidak dicentang maka yang ditampilkan adalah total amount dari awal tahun sampai akhir bulan dari month dan year yang dipilih (tanpa include OB)
   14/03/2022 [DITA/AMKA] Penyesuaian calculate coa parent yang diambil dari loop coa di capital statement setting -> calculate sampai ke parent-parent nya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Collections;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmCapitalStatementSetting : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private bool
            IsInsert = false,
            mIsFilterByProfitCenter = false;
        internal FrmCapitalStatementSettingFind FrmFind;
        internal List<COARow> lCR = null;

        #endregion

        #region Constructor

        public FrmCapitalStatementSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueEntCode(ref LueEntCode);
                SetLueProfitCenterCode(ref LueProfitCenterCode);
                lCR = new List<COARow>();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                LueProfitCenterCode.Visible = false;
                label4.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Setting Code",

                        //1-5
                        "COA Formula",
                        "Formula"+Environment.NewLine+"Balance",
                        "",
                        "COA Account#",
                        "Description",

                        //6-10
                        "Year-to-Date", 
                        "Print", 
                        "",
                        "Budget Plan",
                        "Customized"+Environment.NewLine+"(Date Periode)"
                    },
                    new int[] 
                    {
                        //0
                        100, 

                        //1-5
                        200, 100, 20, 200, 250,  
                        
                        //6-9
                        200, 80, 20, 200, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdColCheck(Grd1, new int[] { 2, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 9, 10 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10 });
            Grd1.Cols[8].Move(5);
            Grd1.Cols[9].Move(7);
            Grd1.Cols[10].Move(9);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtDocNo, DteDocDt, LueMth, ChkCancelInd, ChkCompPeriode, LueYr, DteCompPeriode1, DteCompPeriode2, LueEntCode, LueProfitCenterCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueMth, LueYr, ChkCompPeriode, DteCompPeriode1, DteCompPeriode2, LueEntCode, LueProfitCenterCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3, 5, 7 });
                    TxtDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueMth, LueYr, ChkCancelInd, ChkCompPeriode, DteCompPeriode1, DteCompPeriode2, LueEntCode, LueProfitCenterCode
                    }, false);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, LueMth, LueYr, DteCompPeriode1, DteCompPeriode2, LueProfitCenterCode, LueEntCode });
            ChkCancelInd.Checked = false;
            ChkCompPeriode.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 9, 10 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCapitalStatementSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            IsInsert = true;
            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            Sm.SetDefaultPeriod(ref DteCompPeriode1, ref DteCompPeriode2, -1);
            Sm.SetLue(LueMth, Sm.ServerCurrentDateTime().Substring(4, 2));
            Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            IsInsert = true;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            IsInsert = false;
            ClearData();
            SetFormControl(mState.View);
        }

        protected override void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
                Sm.FormShowDialog(new FrmCapitalStatementSettingDlg(this, e.RowIndex));
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0) Sm.FormShowDialog(new FrmCapitalStatementSettingDlg2(this, e.RowIndex));

        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCapitalStatementSettingDlg(this, e.RowIndex));
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1 || e.ColIndex == 2 || e.ColIndex == 3)
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    ComputeBalance(row);
                    ComputeBudgetAmt(row);

                    if (Sm.GetDte(DteCompPeriode1).Length != 0 && Sm.GetDte(DteCompPeriode2).Length != 0)
                        ComputeCustomized(row);

                    if (Sm.GetDte(DteCompPeriode1).Length == 0 && Sm.GetDte(DteCompPeriode2).Length == 0 && !ChkCompPeriode.Checked)
                        Sm.SetGrdNumValueZero(ref Grd1, row, new int[] { 10 });
                }
            }
            if (e.ColIndex == 1)
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
                {
                    Grd1.Cells[e.RowIndex, 2].Value = false;
                    Grd1.Cells[e.RowIndex, 2].ReadOnly = iGBool.True;
                }
                else
                    Grd1.Cells[e.RowIndex, 2].ReadOnly = iGBool.False;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                IsInsert = false;
                ClearData();
                ShowCapitalStatementSettingHdr(DocNo);
                ShowCapitalStatementSettingDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCapitalStatementSettingHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select DocNo, DocDt, CancelInd, Yr, Mth, PeriodStartDt, PeriodEndDt, EntCode, ProfitCenterCode " +
                  "From TblCapitalStatementSettingHdr Where DocNo=@DocNo",
                 new string[] 
                   {
                      //0
                      "DocNo",

                      //1-5
                      "DocDt", "CancelInd", "PeriodStartDt", "PeriodEndDt", "EntCode",

                      //6-8
                      "ProfitCenterCode", "Yr", "Mth",
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     ChkCancelInd.Checked = (Sm.DrStr(dr, c[2])) == "Y";
                     Sm.SetDte(DteCompPeriode1, Sm.DrStr(dr, c[3]));
                     Sm.SetDte(DteCompPeriode2, Sm.DrStr(dr, c[4]));
                     Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LueYr, Sm.DrStr(dr, c[7]));
                     Sm.SetLue(LueMth, Sm.DrStr(dr, c[8]));
                 }, true
             );
        }

        private void ShowCapitalStatementSettingDtl(string DocNo)
        {
            lCR.Clear();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SettingCode, COAFormula, FormulaBalanceInd, AcNo, Description, BudgetAmt, Balance, Customized, PrintInd ");
            SQL.AppendLine("From TblCapitalStatementSettingDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "SettingCode",

                           //1-5
                           "COAFormula", "FormulaBalanceInd", "AcNo", "Description", "Balance",

                           //6
                            "PrintInd", "BudgetAmt", "Customized"

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 2);
                        //Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Grd.Cells[Row, 4].Value = Sm.DrStr(dr, c[3]).Length == 0 ? null : "[Collection]";
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);

                        if (Sm.DrStr(dr, c[3]).Length > 0) lCR.Add(new COARow() { Row = Row, AcNo = Sm.DrStr(dr, c[3]) });
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CapitalStatementSetting", "TblCapitalStatementSettingHdr");

            cml.Add(SaveCapitalStatementSettingHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    string AcNo = string.Empty;
                    if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    {
                        int mListIndex = 0;
                        for (int t = 0; t < lCR.Count; ++t)
                        {
                            if (lCR[t].Row == r)
                            {
                                mListIndex = t;
                                break;
                            }
                        }

                        AcNo = lCR[mListIndex].AcNo;
                    }

                    cml.Add(SaveCapitalStatementSettingDtl(DocNo, r, AcNo));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "COA Account# is empty.");
                    return true;
                }
                if (Sm.GetGrdStr(Grd1, Row, 1).Length == 0 && Sm.GetGrdStr(Grd1, Row, 4).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "COA Formula or COA Account# is empty.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 detail.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCapitalStatementSettingHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCapitalStatementSettingHdr ");
            SQL.AppendLine("(DocNo, DocDt, Yr, Mth, PeriodStartDt, PeriodEndDt, EntCode, ProfitCenterCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, @Mth, @PeriodStartDt, @PeriodEndDt, @EntCode, @ProfitCenterCode, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@PeriodStartDt", Sm.GetDte(DteCompPeriode1));
            Sm.CmParamDt(ref cm, "@PeriodEndDt", Sm.GetDte(DteCompPeriode2));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            return cm;
        }

        private MySqlCommand  SaveCapitalStatementSettingDtl(string DocNo, int Row, string AcNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCapitalStatementSettingDtl ");
            SQL.AppendLine("(DocNo, DNo, SettingCode, COAFormula, FormulaBalanceInd, AcNo, Description, BudgetAmt, Balance, Customized, PrintInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @SettingCode, @COAFormula, @FormulaBalanceInd, @AcNo, @Description, @BudgetAmt, @Balance, @Customized, @PrintInd, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SettingCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@COAFormula", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@FormulaBalanceInd", Sm.GetGrdBool(Grd1, Row, 2) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@BudgetAmt", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Balance", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PrintInd", Sm.GetGrdBool(Grd1, Row, 7) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Customized", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData(string DocNo)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(EditCapitalStatementSettingHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    string AcNo = string.Empty;
                    if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    {
                        int mListIndex = 0;
                        for (int t = 0; t < lCR.Count; ++t)
                        {
                            if (lCR[t].Row == r)
                            {
                                mListIndex = t;
                                break;
                            }
                        }

                        AcNo = lCR[mListIndex].AcNo;
                    }

                    cml.Add(SaveCapitalStatementSettingDtl(DocNo, r, AcNo));
                }
            }

           

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsDataAlreadyCancelled()
                ;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblCapitalStatementSettingHdr Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand EditCapitalStatementSettingHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCapitalStatementSettingHdr ");
            SQL.AppendLine("Set CancelInd=@CancelInd, Yr=@Yr, Mth=@Mth, PeriodStartDt=@PeriodStartDt, PeriodEndDt=@PeriodEndDt,  ");
            SQL.AppendLine("EntCode=@EntCode, ProfitCenterCode=@ProfitCenterCode, LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblCapitalStatementSettingDtl ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@PeriodStartDt", Sm.GetDte(DteCompPeriode1));
            Sm.CmParamDt(ref cm, "@PeriodEndDt", Sm.GetDte(DteCompPeriode2));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method 

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        private void SetLueProfitCenterCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("Select ProfitCenterCode As Col1, ProfitCenterName As Col2 ");
                SQL.AppendLine("From TblProfitCenter ");
                SQL.AppendLine("Where ProfitCenterCode In (");
                SQL.AppendLine("    Select ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Order By Col2;");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            }
            else
            {
                SQL.AppendLine("Select ProfitCenterCode As Col1, ProfitCenterName As Col2 ");
                SQL.AppendLine("From TblProfitCenter Order By ProfitCenterName;");
            }

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        internal void ComputeBalance(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            {
                string SQLFormula = Sm.GetGrdStr(Grd1, Row, 1);
                char[] delimiters = { '+', '-' };
                string[] ArraySQLFormula = SQLFormula.Split(delimiters);
                for (int i = 0; i < ArraySQLFormula.Count(); i++)
                {
                    string oldS = ArraySQLFormula[i];
                    string newS = "0";
                    for (int row = 0; row < Row; row++)
                    {
                        if (Sm.GetGrdStr(Grd1, row, 0) == oldS)
                        {
                            newS = Sm.GetGrdDec(Grd1, row, 6).ToString();
                            newS = newS.Replace(',', '.');
                        }
                    }
                    SQLFormula = SQLFormula.Replace(oldS, newS);
                }

                Grd1.Cells[Row, 6].Value = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
            }
            else if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            {
                int mListIndex = 0;
                for (int t = 0; t < lCR.Count; ++t)
                {
                    if (lCR[t].Row == Row)
                    {
                        mListIndex = t;
                        break;
                    }
                }
                string DocDt1 = Sm.GetLue(LueYr) + "0101";
                string DocDt2 = Sm.GetValue("SELECT DATE_FORMAT(LAST_DAY('" + (Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01") + "'), '%Y%m%d') ");
                string AcNo = lCR[mListIndex].AcNo;
                string[] AcNos = AcNo.Split(',');

                //if (Sm.GetGrdBool(Grd1, Row, 2))
                //{
                //    DocDt1 = Sm.GetValue("SELECT DATE_FORMAT(@Param, '%Y%m%d')",(Sm.GetLue(LueYr) + "0101"));
                //    //DocDt1 = DocDt2.Substring(0, 4) + "0101";
                //}

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT SUM(Tbl.Amt) ");
                if (Sm.GetGrdBool(Grd1, Row, 2))
                {
                    SQL.AppendLine("FROM ( ");
                    SQL.AppendLine("    SELECT B.AcNo, SUM(IFNULL(B.Amt, 0.00)) Amt ");
                    SQL.AppendLine("    FROM tblCOAOpeningBalanceHdr A ");
                    SQL.AppendLine("    INNER JOIN tblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    WHERE A.DocDt = '" + DocDt1 + "' ");
                    SQL.AppendLine("    And Find_In_Set(B.AcNo,'" + AcNo + "') ");
                    SQL.AppendLine("    And A.CancelInd ='N'  ");
                    SQL.AppendLine("    GROUP BY B.AcNo  ");
                    SQL.AppendLine(" )Tbl ");
                    //SQL.AppendLine("    HAVING B.AcNo = '"+AcNo+"' ");

                    // SQL.AppendLine("	UNION ALL ");
                }

                else
                {
                    SQL.AppendLine("FROM ( ");
                    SQL.AppendLine("    SELECT T.AcNo, SUM(T.Amt) Amt ");
                    SQL.AppendLine("    FROM( ");
                    SQL.AppendLine("        SELECT B.AcNo, ");
                    SQL.AppendLine("        Case ");
                    SQL.AppendLine("            when C.AcType = 'D' then (B.DAmt - B.CAmt) ");
                    SQL.AppendLine("            ELSE (B.CAmt - B.DAmt) ");
                    SQL.AppendLine("        END Amt ");
                    SQL.AppendLine("        FROM TblJournalHdr A ");
                    SQL.AppendLine("        INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                    SQL.AppendLine("        WHERE A.DocDt BETWEEN '" + DocDt1 + "' AND '" + DocDt2 + "' ");
                    //SQL.AppendLine("        And Find_In_Set(C.AcNo,'" + AcNo + "') ");
                    SQL.AppendLine("        And ( ");
                    int i = 0;
                    foreach (var x in AcNos)
                    {
                        if (i != 0) SQL.AppendLine("        Or ");
                        SQL.AppendLine("        B.AcNo Like '" + x + "%' ");
                        i++;
                    }
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    )T ");
                    SQL.AppendLine("    GROUP BY T.AcNo ");
                    //SQL.AppendLine("    HAVING T.AcNo = '"+AcNo+"' ");
                    SQL.AppendLine(")Tbl ");
                }

                Grd1.Cells[Row, 6].Value = Sm.GetValue(SQL.ToString()).Length > 0 ? Decimal.Parse(Sm.GetValue(SQL.ToString())) : 0m;
            }
        }

        internal void ComputeCustomized(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            {
                string SQLFormula = Sm.GetGrdStr(Grd1, Row, 1);
                char[] delimiters = { '+', '-' };
                string[] ArraySQLFormula = SQLFormula.Split(delimiters);
                for (int i = 0; i < ArraySQLFormula.Count(); i++)
                {
                    string oldS = ArraySQLFormula[i];
                    string newS = "0";
                    for (int row = 0; row < Row; row++)
                    {
                        if (Sm.GetGrdStr(Grd1, row, 0) == oldS)
                        {
                            newS = Sm.GetGrdDec(Grd1, row, 10).ToString();
                            newS = newS.Replace(',', '.');
                        }
                    }
                    SQLFormula = SQLFormula.Replace(oldS, newS);
                }

                Grd1.Cells[Row, 10].Value = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
            }
            else if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            {
                int mListIndex = 0;
                for (int t = 0; t < lCR.Count; ++t)
                {
                    if (lCR[t].Row == Row)
                    {
                        mListIndex = t;
                        break;
                    }
                }

                string DocDt1 = Sm.GetDte(DteCompPeriode1).Substring(0, 8);
                string DocDt2 = Sm.GetDte(DteCompPeriode2).Substring(0, 8);
                string AcNo = lCR[mListIndex].AcNo;

                if (Sm.GetGrdBool(Grd1, Row, 2))
                {
                    DocDt1 = Sm.GetValue("SELECT DATE_FORMAT(@Param, '%Y%m%d')", Sm.GetDte(DteCompPeriode1).Substring(0, 4) + "0101");
                }

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT SUM(Tbl.Amt) ");
               if (Sm.GetGrdBool(Grd1, Row, 2))
                {
                    SQL.AppendLine("FROM ( ");
                    SQL.AppendLine("    SELECT B.AcNo, SUM(IFNULL(B.Amt, 0.00)) Amt ");
                    SQL.AppendLine("    FROM tblCOAOpeningBalanceHdr A ");
                    SQL.AppendLine("    INNER JOIN tblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    WHERE A.DocDt = '" + DocDt1 + "' ");
                    SQL.AppendLine("    And Find_In_Set(B.AcNo,'" + AcNo + "') ");
                    SQL.AppendLine("    And A.CancelInd ='N'  ");
                    SQL.AppendLine("    GROUP BY B.AcNo  ");
                    SQL.AppendLine(" )Tbl ");
                    //SQL.AppendLine("    HAVING B.AcNo = '"+AcNo+"' ");

                    // SQL.AppendLine("	UNION ALL ");
                }

                else
                {
                    SQL.AppendLine("FROM ( ");
                    SQL.AppendLine("    SELECT T.AcNo, SUM(T.Amt) Amt ");
                    SQL.AppendLine("    FROM( ");
                    SQL.AppendLine("        SELECT B.AcNo, ");
                    SQL.AppendLine("        Case ");
                    SQL.AppendLine("            when C.AcType = 'D' then (B.DAmt - B.CAmt) ");
                    SQL.AppendLine("            ELSE (B.CAmt - B.DAmt) ");
                    SQL.AppendLine("        END Amt ");
                    SQL.AppendLine("        FROM TblJournalHdr A ");
                    SQL.AppendLine("        INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                    SQL.AppendLine("        WHERE A.DocDt BETWEEN '" + DocDt1 + "' AND '" + DocDt2 + "' ");
                    SQL.AppendLine("        And Find_In_Set(C.AcNo,'" + AcNo + "') ");
                    SQL.AppendLine("    )T ");
                    SQL.AppendLine("    GROUP BY T.AcNo ");
                    //SQL.AppendLine("    HAVING T.AcNo = '"+AcNo+"' ");
                    SQL.AppendLine(")Tbl ");
                }

                Grd1.Cells[Row, 10].Value = Sm.GetValue(SQL.ToString()).Length > 0 ? Decimal.Parse(Sm.GetValue(SQL.ToString())) : 0m;
            }
        }

        internal void ComputeBudgetAmt(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            {
                string SQLFormula = Sm.GetGrdStr(Grd1, Row, 1);
                char[] delimiters = { '+', '-' };
                string[] ArraySQLFormula = SQLFormula.Split(delimiters);
                for (int i = 0; i < ArraySQLFormula.Count(); i++)
                {
                    string oldS = ArraySQLFormula[i];
                    string newS = "0";
                    for (int row = 0; row < Row; row++)
                    {
                        if (Sm.GetGrdStr(Grd1, row, 0) == oldS)
                        {
                            newS = Sm.GetGrdDec(Grd1, row, 9).ToString();
                            newS = newS.Replace(',', '.');
                        }
                    }
                    SQLFormula = SQLFormula.Replace(oldS, newS);
                }

                Grd1.Cells[Row, 9].Value = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
            }
            else if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            {
                int mListIndex = 0;
                for (int t = 0; t < lCR.Count; ++t)
                {
                    if (lCR[t].Row == Row)
                    {
                        mListIndex = t;
                        break;
                    }
                }

                string DocDt1 = Sm.GetLue(LueYr);
                //string DocDt2 = Sm.GetDte(DtePeriode2).Substring(0, 8);
                string AcNo = lCR[mListIndex].AcNo;
                //if (Sm.GetGrdBool(Grd1, Row, 2))
                //{
                    //DocDt2 = Sm.GetValue("SELECT DATE_FORMAT(DATE_ADD(@Param, INTERVAL -1 DAY), '%Y%m%d')", Sm.GetDte(DtePeriode));
                    //DocDt1 = DocDt2.Substring(0, 4) + "0101";
                //}

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT SUM(D.Amt) ");
                SQL.AppendLine("FROM tblbudgetrequesthdr A ");
                SQL.AppendLine("INNER JOIN tblbudgetrequestdtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("INNER JOIN tblbudgetcategory C ON B.BCCode = C.BCCode ");
                SQL.AppendLine("INNER JOIN tblbudgetdtl D ON B.DocNo = D.BudgetRequestDocNo AND B.DNo = D.BudgetRequestDNo ");
                SQL.AppendLine("WHERE A.Yr = '" + DocDt1 + "' AND Find_In_Set(C.AcNo, '"+ AcNo +"') ");

                Grd1.Cells[Row, 9].Value = Sm.GetValue(SQL.ToString()).Length > 0 ? Decimal.Parse(Sm.GetValue(SQL.ToString())) : 0m;
            }
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
           
            var l = new List<CapitalStatementSettingHdr>();
            var ldtl = new List<CapitalStatementSettingDtl>();


            string[] TableName = { "CapitalStatementSettingHdr", "CapitalStatementSettingDtl" };

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select DocNo, Date_format(DocDt, '%d %M %Y') DocDt, Periode, ");
            SQL.AppendLine("Date_format(PeriodStartDt, '%d %M %Y') PeriodStartDt, Date_format(PeriodEndDt, '%d %M %Y') PeriodEndDt ");
            SQL.AppendLine("From TblCapitalStatementSettingHdr ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "DocNo",

                        //1-4
                        "DocDt", 
                        "Periode", 
                        "PeriodStartDt", 
                        "PeriodEndDt"

                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CapitalStatementSettingHdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            Periode = Sm.DrStr(dr, c[2]),
                            PeriodStartDt = Sm.DrStr(dr, c[3]),
                            PeriodEndDt = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select Description, Balance, BudgetAmt ");
                SQLDtl.AppendLine("From TblCapitalStatementSettingDtl ");
                SQLDtl.AppendLine("Where DocNo = @DocNo And PrintInd = 'Y' ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                {
                 //0
                 "Description" ,

                 //1-5
                 "BudgetAmt", "Balance" 
                });
                if (drDtl.HasRows)
                {
                    int No = 0;
                    while (drDtl.Read())
                    {
                        No = No + 1;
                        ldtl.Add(new CapitalStatementSettingDtl()
                        {
                            nomor = No,
                            Description = Sm.DrStr(drDtl, cDtl[0]),
                            BudgetAmt = Sm.DrDec(drDtl, cDtl[1]),
                            Balance = Sm.DrDec(drDtl, cDtl[2]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("CapitalStatementSetting", myLists, TableName, false);
        }

        internal void GetCOAList(int Row, string AcNo)
        {
            if (lCR.Count > 0) lCR.RemoveAll(t => t.Row == Row);
            lCR.Add(new COARow() { Row = Row, AcNo = AcNo });
            if (AcNo.Length == 0) Grd1.Cells[Row, 4].Value = null;
            else Grd1.Cells[Row, 4].Value = "[Collection]";

            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                ComputeBalance(row);
                ComputeBudgetAmt(row);

                if (Sm.GetDte(DteCompPeriode1).Length != 0 && Sm.GetDte(DteCompPeriode2).Length != 0)
                    ComputeCustomized(row);

                if (Sm.GetDte(DteCompPeriode1).Length == 0 && Sm.GetDte(DteCompPeriode2).Length == 0 && !ChkCompPeriode.Checked)
                    Sm.SetGrdNumValueZero(ref Grd1, row, new int[] { 10 });
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteCompPeriode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && IsInsert)
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetDte(DteCompPeriode1).Length != 0 && Sm.GetDte(DteCompPeriode2).Length != 0)
                        ComputeCustomized(row);

                    if (Sm.GetDte(DteCompPeriode1).Length == 0 && Sm.GetDte(DteCompPeriode2).Length == 0 && !Sm.GetGrdBool(Grd1, row, 2))
                        Sm.SetGrdNumValueZero(ref Grd1, row, new int[] { 10 });
                }
            }

            Sm.FilterDteSetCheckEdit(this, sender);

        }
        private void DteCompPeriode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && IsInsert)
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetDte(DteCompPeriode1).Length != 0 && Sm.GetDte(DteCompPeriode2).Length != 0)
                        ComputeCustomized(row);

                    if (Sm.GetDte(DteCompPeriode1).Length == 0 && Sm.GetDte(DteCompPeriode2).Length == 0 && !Sm.GetGrdBool(Grd1, row, 2))
                        Sm.SetGrdNumValueZero(ref Grd1, row, new int[] { 10 });
                }
            }

            Sm.FilterDteSetCheckEdit(this, sender);

        }
        private void ChkCompPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Comparison Period");
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                ComputeBalance(row);
            }
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                ComputeBalance(row);
                ComputeBudgetAmt(row);
            }
        }

        #endregion

        #region Class 

        private class CapitalStatementSettingHdr
        {
            public string DocNo { set; get; }
            public string DocDt { get; set; }
            public string PeriodStartDt { get; set; }
            public string PeriodEndDt { get; set; }
            public string Periode { get; set; }
        }

        

        private class CapitalStatementSettingDtl
        {
            public int nomor { set; get; }
            public string Description { set; get; }
            public decimal Balance { set; get; }
            public decimal BudgetAmt { set; get; }
        }

        internal class COARow
        {
            public int Row { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
        }

        #endregion
    }
}
