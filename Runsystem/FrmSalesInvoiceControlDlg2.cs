﻿#region Update
/*
    31/05/2021 [MYA/KIM] penambahan field collector yg ambil dr master employee di SLI Control
    14/06/2021 [MYA/KIM] menambah parameter untuk filter field collector yg ambil dr master employee di SLI Control 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoiceControlDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesInvoiceControl mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesInvoiceControlDlg2(FrmSalesInvoiceControl FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Employee";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            //Sl.SetLueDeptCode(ref LueDeptCode);
            //Sl.SetLueOption(ref LueFakultas, "Fakultas");
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 63;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's Name",
                        "Old Code",
                        "Division",
                        "Department",

                        //6-10
                        "Position",
                        "Position Status",
                        "Grade",
                        "Level",
                        "Site",
                        
                        //11-15
                        "Join",
                        "Resign",
                        "Identity#",
                        "Gender",
                        "Religion",
                        
                        //16-20
                        "Employment Status",
                        "Last"+Environment.NewLine+"Assessment",
                        "Birth"+Environment.NewLine+"Date",
                        "Age",
                        "Address",
                        
                        //21-25
                        "City",
                        "Postal"+Environment.NewLine+"Code",
                        "Phone",
                        "Mobile",
                        "Email",
                        
                        //26-30
                        "NPWP",
                        "System Type",
                        "Payroll Group",
                        "Payroll Type",
                        "Payrun Period",
                        
                        //31-35
                        "PTKP",
                        "Bank", 
                        "Bank Account#",
                        "Bank"+Environment.NewLine+"Branch",
                        "SS Employement",
                        
                        //36-40
                        "SS Health",
                        "Entity",
                        "Entity (SS)",
                        "Education Level",
                        "Education Major",
                        
                        //41-45
                        "Permanent Date",
                        "Section",
                        "User Code",
                        "Family",
                        "Clothes Size",

                        //46-50
                        "Shoe Size",
                        "Created By",  
                        "Created Date",
                        "Created Time", 
                        "Last Updated By",
                        
                        //51-55
                        "Last Updated Date",
                        "Last Updated Time",
                        "Training Graduation",
                        "Contract Date",
                        "Cost Group",

                        //56-60
                        "Marital Status",
                        "Acting Official",
                        "Last Seen Code"+Environment.NewLine+"of Conduct Date",
                        "Last Seen Code"+Environment.NewLine+"of Conduct Time",
                        "Years of Service",

                        //61-62
                        "Merit Increase"+Environment.NewLine+"Start Date",
                        "Birth Place"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 250, 100, 150, 150, 
                        
                        //6-10
                        200, 100, 130, 150, 150, 
                        
                        //11-15
                        80, 80, 130, 80, 80, 

                        //16-20
                        120, 100, 80, 80, 300, 

                        //21-25
                        150, 80, 80, 80, 80, 

                        //26-30
                        130, 80, 100, 80, 100, 

                        //31-35
                        80, 200, 200, 130, 80, 
                      
                        //36-40
                        80, 150, 150, 250, 250, 

                        //41-45
                        100, 180, 100, 200, 120, 
                        
                        //46-50
                        120, 130, 130, 130, 130, 
                        
                        //51-55
                        130, 130, 130, 100, 100,

                        //56-60
                        150, 150, 130, 130, 130,

                        //61-62
                        120, 100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62});
            Sm.GrdColCheck(Grd1, new int[] { 35, 36, 57 });
            Sm.GrdFormatDate(Grd1, new int[] { 11, 12, 17, 18, 41, 48, 51, 53, 54, 58, 61 });
            Sm.GrdFormatTime(Grd1, new int[] { 49, 52, 59 });
            Sm.GrdFormatDec(Grd1, new int[] { 19, 60 }, 11);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62 }, false);
            //Sm.GrdColInvisible(Grd1, new int[] { 44 }, mIsEmployeeFindShowFamily);
            Sm.SetGrdProperty(Grd1, false);

            Grd1.Cols[60].Move(12);
            Grd1.Cols[53].Move(14);
            Grd1.Cols[54].Move(11);
            Grd1.Cols[55].Move(12);
            Grd1.Cols[56].Move(23);
            Grd1.Cols[57].Move(7);
            Grd1.Cols[58].Move(52);
            Grd1.Cols[59].Move(53);
            Grd1.Cols[62].Move(23);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, B.UserName, A.EmpCodeOld, O.DivisionName, A.DeptCode,  C.DeptName, D.PosName, D.ActingOfficialInd, Z.PositionStatusName, ");
            SQL.AppendLine("N.GrdLvlName,  ");
            SQL.AppendLine("    V.LevelName, ");
            SQL.AppendLine("    A.SiteCode, M.SiteName, A.JoinDt, A.ResignDt, ");
            SQL.AppendLine("    A.IdNumber, E.OptDesc As Gender, F.OptDesc As Religion, A.ConductDtTm, ");
            SQL.AppendLine("    X.LastAssessment, A.BirthPlace, A.BirthDt, ");
            SQL.AppendLine("    Case When A.BirthDt Is Null Then 0.00 Else TimeStampDiff(Year, Str_To_Date(A.BirthDt, '%Y%m%d'), Str_To_Date(Replace(CurDate(), '-', ''), '%Y%m%d')) End As Age, ");
            SQL.AppendLine("    A.Address, G.CityName, A.PostalCode, A.Phone, A.Mobile, A.Email, A.NPWP, Q.PGName, H.OptDesc As PayrollType, R.OptDesc As PayrunPeriod, A.PTKP, ");
            SQL.AppendLine("    I.BankName As BankName, A.BankAcNo As BankAcNo, A.BankBranch, K.OptDesc As EmploymentStatusDesc, L.OptDesc As SystemTypeDesc, P.EntName, W.EntName As RegEntName, S.Level, S.Major, ");
            SQL.AppendLine("    if(length(T.EmpCode)>0, 'Y', 'N') As SSind, if(length(U.EmpCode)>0, 'Y', 'N') As SSind2, A.LeaveStartDt, Y.SectionName, A.UserCode, ");
            SQL.AppendLine("    Null As FamilyInfo, ");
            SQL.AppendLine("    AB.OptDesc As ClothesSizeDesc, AC.OptDesc As ShoeSizeDesc, A.CostGroup, AE.OptDesc As MaritalStatusDesc, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.TGDt, A.ContractDt, ");
            SQL.AppendLine("    TimeStampdiff(Year, A.JoinDt, left(Replace(CurDate(), '-', ''), 8)) As YearsOfService, ");
            SQL.AppendLine("    Null as EmpMeritIncreaseStartDt ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Left Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("    Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            if(mFrmParent.mDeptCodeForSLIPICCollector.Length != 0){
                SQL.AppendLine("AND Find_In_Set(A.DeptCode, ");
                SQL.AppendLine("(Select ParValue From tblparameter where parcode='DeptCodeForSLIPICCollector' and parcode is not null))");
            }
            SQL.AppendLine("    Left Join TblPosition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("    Left Join TblOption E On A.Gender=E.OptCode And E.OptCat='Gender' ");
            SQL.AppendLine("    Left Join TblOption F On A.Religion=F.OptCode And F.OptCat='Religion' ");
            SQL.AppendLine("    Left Join TblCity G On A.CityCode=G.CityCode ");
            SQL.AppendLine("    Left Join tblOption H On A.PayrollType=H.OptCode And H.OptCat='EmployeePayrollType' ");
            SQL.AppendLine("    Left Join TblBank I On A.BankCode=I.BankCode ");
            //SQL.AppendLine("    Left Join TblBankAccount J On A.BankAcNo=J.BankAcNo ");
            SQL.AppendLine("    Left Join TblOption K On A.EmploymentStatus=K.OptCode And K.OptCat='EmploymentStatus' ");
            SQL.AppendLine("    Left Join TblOption L On A.SystemType=L.OptCode And L.OptCat='EmpSystemType' ");
            SQL.AppendLine("    Left Join TblSite M On A.SiteCode=M.SiteCode ");
            SQL.AppendLine("    left Join TblGradeLevelHdr N On A.GrdLvlCode=N.GrdLvlCode ");
            SQL.AppendLine("    Left join TblDivision O On A.DivisionCode = O.DivisionCode ");
            SQL.AppendLine("    Left Join TblEntity P On A.EntCode = P.EntCode ");
            SQL.AppendLine("    Left Join TblPayrollGrpHdr Q On A.PGCode=Q.PGCode ");
            SQL.AppendLine("    Left Join TblOption R On A.PayrunPeriod=R.OptCode And R.OptCat='PayrunPeriod' ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct T1.EmpCode, group_concat(T2.OptDesc) As Level, group_concat(T3.MajorName) As major  ");
            SQL.AppendLine("        From TblEmployeeEducation T1  ");
            SQL.AppendLine("        Left Join TblOption T2 On T1.Level=T2.OptCode And T2.OptCat='EmployeeEducationLevel'  ");
            SQL.AppendLine("        Left Join Tblmajor T3 On T1.major = T3.MajorCode ");
            SQL.AppendLine("        Where T1.HighestInd = 'Y'  ");
            SQL.AppendLine("        Group by T1.EmpCode ");
            SQL.AppendLine("    ) S On A.EmpCode=S.EmpCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct A.EmpCode From TblEmployeeSS A ");
            SQL.AppendLine("        Inner Join TblSS B On A.SSCode = B.SSCode And B.SSPCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblSSProgram C On B.SSPCode = C.SSpCode ");
            SQL.AppendLine("        And Find_In_Set( ");
            SQL.AppendLine("            IfNull(C.SSpCode, ''), ");
            SQL.AppendLine("            IfNull((Select ParValue From TblParameter Where ParCode='SSProgramForEmployment' And ParValue Is Not Null), '') ");
            SQL.AppendLine("        ) ");
            //SQL.AppendLine("        And C.SSpCode = (Select Parvalue From TblParameter ");
            //SQL.AppendLine("        Where ParCode = 'SSProgramForEmployment')  ");

            SQL.AppendLine("        Where A.EmpCode=A.EmpCode ");
            SQL.AppendLine("        And A.SSCode Is Not Null ");
            SQL.AppendLine("        And (EndDt Is Null Or (EndDt is Not Null And EndDt>Left(CurrentDateTime(), 8))) ");

            SQL.AppendLine("    ) T On A.EmpCode = T.EmpCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Distinct A.EmpCode From TblEmployeeSS A ");
            SQL.AppendLine("        Inner Join Tblss B On A.SSCode = B.SSCode ");
            SQL.AppendLine("        Inner Join TblSSProgram C On B.SSpCode = C.SSpCode ");
            SQL.AppendLine("        Where A.EmpCode=A.EmpCode And (EndDt Is Null Or (EndDt is Not Null And EndDt>Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("        And C.SSpCode = (Select Parvalue From TblParameter ");
            SQL.AppendLine("        Where ParCode = 'SSProgramForHealth') ");
            SQL.AppendLine("    ) U On A.EmpCode=U.EmpCode ");
            SQL.AppendLine("    Left Join TblLevelHdr V On N.LevelCode=V.LevelCode ");
            SQL.AppendLine("    Left Join TblEntity W On A.RegEntCode = W.EntCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.DocDt As LastAssessment ");
            SQL.AppendLine("        From TblAssesmentProcessHdr T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select EmpCode, Max(Concat(DocDt, DocNo)) Key1 ");
            SQL.AppendLine("            From TblAssesmentProcessHdr ");
            SQL.AppendLine("            Where CancelInd='N' ");
            SQL.AppendLine("            And EmpCode In (Select EmpCode From TblEmployee Where 1=1 )");
            SQL.AppendLine("            Group By EmpCode ");
            SQL.AppendLine("        ) T2 On T1.EmpCode=T2.EmpCode And Concat(T1.DocDt, T1.DocNo)=key1 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.EmpCode In (Select EmpCode From TblEmployee Where 1=1 )");
            SQL.AppendLine("    ) X On A.EmpCode=X.EmpCode ");
            SQL.AppendLine("    Left Join TblSection Y On A.SectionCode = Y.SectionCode ");
            SQL.AppendLine("    Left Join TblPositionStatus Z On A.PositionStatusCode=Z.PositionStatusCode ");
            SQL.AppendLine("    Left Join TblOption AB On A.ClothesSize=AB.OptCode And AB.OptCat='EmployeeClothesSize' ");
            SQL.AppendLine("    Left Join TblOption AC On A.ShoeSize=AC.OptCode And AC.OptCat='EmployeeShoeSize' ");
            SQL.AppendLine("    Left Join TblOption AD On A.CostGroup=AD.OptCode And AD.OptCat='EmpCostGroup' ");
            SQL.AppendLine("    Left Join TblOption AE On A.MaritalStatus=AE.OptCode And AE.OptCat='MaritalStatus' ");
            SQL.AppendLine("    Left Join TblLevelHdr AF On A.LevelCode=AF.LevelCode ");
            SQL.AppendLine("Where A.EmpCode In (Select EmpCode From TblEmployee Where 1=1 ) ");
            SQL.AppendLine(") T ");
            //SQL.AppendLine(" Order By DeptName, EmpName;");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                //Sm.FilterStr(ref Filter, ref cm, TxtPICCollector.Text, "A.EmpName", false);
                Sm.FilterStr(ref Filter, ref cm, TxtEmployee.Text, new string[] { "EmpCode", "EmpName" });
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By EmpName ",
                        new string[] 
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName","EmpCodeOld", "DivisionName", "DeptName","PosName", 

                            //6-10
                            "PositionStatusName", "GrdLvlName", "LevelName", "SiteName", "JoinDt",
                            
                            //11-15
                            "ResignDt", "IdNumber", "Gender", "Religion", "EmploymentStatusDesc", 
                            
                            //16-20
                            "LastAssessment", "BirthDt", "Age", "Address", "CityName", 
                            
                            //21-25
                            "PostalCode", "Phone", "Mobile", "Email", "NPWP", 
                            
                            //26-30
                            "SystemTypeDesc", "PGName", "PayrollType", "PayrunPeriod", "PTKP", 
                            
                            //31-35
                            "BankName", "BankAcNo", "BankBranch", "SSInd", "SSInd2", 
                            
                            //36-40
                            "EntName", "RegEntName", "Level", "Major", "LeaveStartDt", 
                            
                            //41-45
                            "SectionName",  "UserCode", "FamilyInfo", "ClothesSizeDesc", "ShoeSizeDesc", 
                            
                            //46-50
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "TGDt",

                            //51-55
                            "ContractDt", "CostGroup", "MaritalStatusDesc", "ActingOfficialInd", "ConductDtTm",

                            //56-58
                            "YearsOfService", "EmpMeritIncreaseStartDt", "BirthPlace"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 33);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 35, 34);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 36, 35);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 36);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 38);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 39);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 41, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 41);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 42);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 43);
                            Grd.Cells[Row, 44].Value = Sm.GetGrdStr(Grd, Row, 44).Replace(",", "," + Environment.NewLine);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 45, 44);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 45);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 46);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 48, 47);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 49, 47);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 48);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 51, 49);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 52, 49);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 53, 50);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 54, 51);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 55, 52);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 56, 53);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 57, 54);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 58, 55);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 59, 55);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 60, 56);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 61, 57);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 62, 58);


                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtPICCollector.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                //mFrmParent.TxtSalesInvoiceDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                //mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                this.Close();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmployee_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmployee_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        //private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        //{
        //    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        //   Sm.FilterLueSetCheckEdit(this, sender);
        //}

        //private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        //{
        //    Sm.FilterSetLookUpEdit(this, sender, "Department");
        //}

        #endregion

        #endregion

    }
}
