﻿#region Update
/*
    07/09/2021 [BRI/AMKA] New reporting
    10/11/2021 [SET/AMKA] Membuat Combo Box Field Profit Center di reporting Monitoring Profit Center dapat terfilter berdasarkan Group User berdasar parameter IsFilterByProfitCenter
    19/11/2021 [SET/AMKA] SUPPORT : Muncul warning No Data ketika refresh menu Reporting Monitoring Profit Center.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonitoringProfitCenter : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<String> mlProfitCenter;
        private bool mIsAllProfitCenterSelected = false,
            mIsFilterByProfitCenter = false;

        #endregion

        #region Constructor

        public FrmRptMonitoringProfitCenter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                mlProfitCenter = new List<String>();
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -365);
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                ChkProfitCenterCode.Visible = false;
                //var CurrentDateTime = Sm.ServerCurrentDateTime();
                //Sl.SetLueYr(LueYr, string.Empty);
                //Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                //Sl.SetLueMth(LueMth);
                //Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Code",
                        "Periode", 
                        "Profit Center",
                        "LPP",
                        "Upah",
                        
                        //6-10
                        "Bahan",
                        "SubCont",
                        "ALAT",
                        "BTL",
                        "HPP",                       
                        
                        //11-14
                        "%2",
                        "PPh Final",
                        "%3",
                        "Laba/Rugi"
                    });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        private string GetSQL(string Alias, string subSQL1, string Filter, string subSQL2, string AcNo, bool IsNeedOpeningBalance)
        {
            var SQL = new StringBuilder();
            string AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param; ", AcNo);

            if (IsNeedOpeningBalance)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select DocDt, ProfitCenterCode, ");
                SQL.AppendLine("    Sum( ");
                SQL.AppendLine("        Case ");
                SQL.AppendLine("            When T.AcType = '" + AcType + "' Then T.Amt ");
                SQL.AppendLine("            Else T.Amt*-1 ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select Left(T1.DocDt,6) As DocDt, T3.ProfitCenterCode, T4.AcType, ");
                SQL.AppendLine("        Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("        From TblJournalHdr T1, TblJournalDtl T2, TblCostCenter T3, TblCOA T4 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo And T1.CCCode Is Not Null ");
                SQL.AppendLine("        And T1.CCCode=T3.CCCode And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And T2.AcNo=T4.AcNo ");
                SQL.AppendLine("        And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine(subSQL1 + Filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("        Group By Left(T1.DocDt,6), T3.ProfitCenterCode, T2.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Left(T1.DocDt,6) As DocDt, T1.ProfitCenterCode, T3.AcType, Sum(T2.Amt) As Amt ");
                SQL.AppendLine("        From TblCOAOpeningBalanceHdr T1, TblCOAOpeningBalanceDtl T2, TblCOA T3 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.Yr In(Left(@DocDt1,4),Left(@DocDt2,4)) ");
                //SQL.AppendLine("        And T1.Yr=@StartFrom ");
                SQL.AppendLine("        And T2.AcNo=T3.AcNo ");
                SQL.AppendLine("        And T3.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2.Replace("A.", "T1."));
                SQL.AppendLine("        Group By Left(T1.DocDt,6), T1.ProfitCenterCode, T2.AcNo ");
                SQL.AppendLine("    ) T Group By ProfitCenterCode, Left(DocDt,6) ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");
                SQL.AppendLine("And A.DocDt=" + Alias + ".DocDt ");

            }
            else
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select Left(T1.DocDt,6) As DocDt, T3.ProfitCenterCode, ");
                SQL.AppendLine("    Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("    From TblJournalHdr T1, TblJournalDtl T2, TblCostCenter T3, TblCOA T4 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.CCCode Is Not Null ");
                SQL.AppendLine("    And T1.CCCode=T3.CCCode And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And T2.AcNo=T4.AcNo ");
                SQL.AppendLine("    And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine(subSQL1 + Filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("    Group By T3.ProfitCenterCode, Left(T1.DocDt,6) ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");
                SQL.AppendLine("And A.DocDt = "+Alias+".DocDt");
            }

            return SQL.ToString();
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            var Filter1 = " And (Left(T1.DocDt,6) BETWEEN Left(@DocDt1,6) AND Left(@DocDt2,6)) ";
            //var Filter2 = " And T1.DocDt>=@Dt1 And T1.DocDt<@Dt2 "; // awal tahun sampai bulan ini
            //var Filter3 = " And T1.DocDt>=@Dt3 And T1.DocDt<@Dt2 "; // awal ada data sampai bulan ini
            var Filter = string.Empty;
            int i = 0;
            foreach (var x in mlProfitCenter.Distinct())
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                i++;
            }
            if (Filter.Length == 0)
                SQL2.AppendLine("    And 1=0 ");
            else
                SQL2.AppendLine("    And (" + Filter + ") ");

            SQL.AppendLine("Select Distinct Concat(A.DocDt,'01') As Periode, A.ProfitCenterCode, A.ProfitCenterName, ");
            SQL.AppendLine("IfNull(B.Amt, 0.00) As Value1, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) As Value2, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) As Value3, ");
            SQL.AppendLine("IfNull(E.Amt, 0.00) As Value4, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) As Value5, ");
            SQL.AppendLine("IfNull(G.Amt, 0.00) As Value6, ");
            SQL.AppendLine("IfNull(H.Amt, 0.00) As Value7 ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("Select Left(A.DocDt,6) As DocDt, B.ProfitCenterCode, C.ProfitCenterName ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblCostCenter B ON A.CCCode = B.CCCode ");
            SQL.AppendLine("Inner Join TblProfitCenter C ON B.ProfitCenterCode = C.ProfitCenterCode ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("Select Distinct Left(A.DocDt,6) As Periode, B.ProfitCenterCode, B.ProfitCenterName ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblProfitCenter B ON A.ProfitCenterCode = B.ProfitCenterCode ");
            SQL.AppendLine(")A ");
            SQL.AppendLine(GetSQL("B", " And T2.AcNo Like '4%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("C", " And T2.AcNo Like '5.3%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("D", " And T2.AcNo Like '5.1%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("E", " And T2.AcNo Like '5.2%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("F", " And T2.AcNo Like '5.4%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("G", " And T2.AcNo Like '5.5%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("H", " And T2.AcNo Like '8.3%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine(SQL2.ToString());
            SQL.AppendLine("And Left(A.DocDt,6) BETWEEN Left(@DocDt1,6) AND Left(@DocDt2,6) ");
            SQL.AppendLine("Order By Periode ");

            return SQL.ToString();
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (IsProfitCenterEmpty() || 
                Sm.IsDteEmpty(DteDocDt1, "Start date") || 
                Sm.IsDteEmpty(DteDocDt2, "End date") || 
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            try
            {
                SetProfitCenter();

                if (!mIsAllProfitCenterSelected)
                {
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "ProfitCenterCode",

                        //1-5
                        "Periode", "ProfitCenterName", "Value1", "Value2", "Value3", 

                        //6-9
                        "Value4", "Value5", "Value6", "Value7", /*"Value8", "Value9"*/
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Grd.Cells[Row, 10].Value = Sm.GetGrdDec(Grd, Row, 5) + Sm.GetGrdDec(Grd, Row, 6) + Sm.GetGrdDec(Grd, Row, 7) + Sm.GetGrdDec(Grd, Row, 8) + Sm.GetGrdDec(Grd, Row, 9);
                        Grd.Cells[Row, 11].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row, 4) == 0 ? 0m : (Sm.GetGrdDec(Grd1, Row, 10) / Sm.GetGrdDec(Grd1, Row, 4)) * 100),2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Grd.Cells[Row, 13].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row, 4) == 0 ? 0m : (Sm.GetGrdDec(Grd1, Row, 12) / Sm.GetGrdDec(Grd1, Row, 4)) * 100),2);
                        Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 4) - Sm.GetGrdDec(Grd, Row, 10) - Sm.GetGrdDec(Grd, Row, 12);

                    }, true, false, false, true
                );
                //ComputeGrd();
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 12, 14 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void ComputeGrd()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 10].Value = Sm.GetGrdDec(Grd1, Row, 5) + Sm.GetGrdDec(Grd1, Row, 6) + Sm.GetGrdDec(Grd1, Row, 7) + Sm.GetGrdDec(Grd1, Row, 8) + Sm.GetGrdDec(Grd1, Row, 9);
                Grd1.Cells[Row, 11].Value = (Sm.GetGrdDec(Grd1, Row, 10) / Sm.GetGrdDec(Grd1, Row, 4)) * 100;
                Grd1.Cells[Row, 13].Value = (Sm.GetGrdDec(Grd1, Row, 12) / Sm.GetGrdDec(Grd1, Row, 4)) * 100;
                Grd1.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 4) - Sm.GetGrdDec(Grd1, Row, 10) - Sm.GetGrdDec(Grd1, Row, 12);
            }
        }

        private bool IsProfitCenterEmpty()
        {
            if (!ChkProfitCenterCode.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit Center is Empty.");
                return true;
            }
            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param)) T; ",
                    Value.Replace(", ", ","));
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion
    }
}
