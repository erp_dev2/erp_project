﻿#region Update
/*
    09/02/2018 [TKG] tambah OT dan tax allowance     
    26/02/2018 [TKG] ubah perhitungan brutto (take home pay+tax)
    10/03/2018 [TKG] filter site
    17/04/2018 [HAR] Rounding Value berdasarkan parameter
    26/04/2018 [TKG] tambah employment status
    31/05/2018 [TKG] meal+transport dari fixed allowance 
    01/10/2018 [TKG]  ubah rumus brutto (Salary + semua allowance - (bpjs employee) + salary adjustment)
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary5 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal string mSalaryInd = "1", mRptPayrollProcessSummaryVersion ="1";
        private bool
             mIsNotFilterByAuthorization = false,
             mIsFilterBySiteHR = false,
             mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd(); 
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mRptPayrollProcessSummaryVersion = Sm.GetParameter("RptPayrollProcessSummaryVersion");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.EmpCode, C.EmpName, C.EmpCodeOld, E.PosName, D.DeptName, C.JoinDt, ");
            SQL.AppendLine("C.ResignDt, F.OptDesc As SystemTypeDesc, G.OptDesc As PayrunPeriodDesc, H.PGName, J.SiteName, A.NPWP, ");
            SQL.AppendLine("I.OptDesc As NonTaxableIncomeDesc, A.Salary, A.WorkingDay, K.EmploymentStatusDesc, ");
            SQL.AppendLine("A.PLDay, A.PLHr, A.PLAmt, A.ProcessPLAmt, ");
            SQL.AppendLine("A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, A.OT1Amt, A.OT2Amt, ");
            SQL.AppendLine("A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, A.FixAllowance, A.PerformanceValue, ");
            SQL.AppendLine("A.ADOT, A.Meal, A.Transport, A.TaxAllowance, A.SSEmployerPension, ");
            SQL.AppendLine("A.SSEmployerHealth, A.SSEmployeeHealth, A.SSEmployerEmployment, A.SSEmployeeEmployment, A.SSErPension, A.SSEePension, ");
            SQL.AppendLine("A.SSEmployeePension, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, A.DedEmployee, ");
            SQL.AppendLine("A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, A.SalaryAdjustment, ");
            SQL.AppendLine("(A.Salary+A.FixAllowance+A.TaxAllowance+ADOT-(A.SSEmployeeHealth+A.SSEmployeeEmployment+A.SSEePension)+A.SalaryAdjustment) As Brutto, ");
            SQL.AppendLine("A.Tax, A.EOYTax, A.Amt, A.VoucherRequestPayrollDocNo ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, ");
            SQL.AppendLine("    Group_Concat(Distinct D.OptDesc Order By A.Dt Separator ', ') As EmploymentStatusDesc ");
            SQL.AppendLine("    From TblPayrollProcess2 A ");
            SQL.AppendLine("    Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("    Inner Join TblOption D On A.EmploymentStatus=D.OptCode And D.OptCat='EmploymentStatus' ");
            SQL.AppendLine("    Where 1=1 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By A.PayrunCode, A.EmpCode ");
            SQL.AppendLine(") K On A.PayrunCode=K.PayrunCode And A.EmpCode=K.EmpCode ");

            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine(Filter);
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(" Order By A.PayrunCode, C.EmpName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 63;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun Name",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",

                        //6-10
                        "Old Code",
                        "Position",
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Type",
                        "Period",
                        "Group",
                        "Site",
                        "NPWP",

                        //16-20
                        "PTKP",
                        "Salary", 
                        "Working Day", 
                        "Paid Leave"+Environment.NewLine+"(Day)", 
                        "Paid Leave"+Environment.NewLine+"(Hour)",  
                        
                        //21-25
                        "Paid Leave"+Environment.NewLine+"(Amount)", 
                        "Processed Paid"+Environment.NewLine+"Leave (Amount)", 
                        "Unpaid Leave"+Environment.NewLine+"(Day)", 
                        "Unpaid Leave"+Environment.NewLine+"(Hour)",  
                        "Unpaid Leave"+Environment.NewLine+"(Amount)", 
                        
                        //26-30
                        "Processed Unpaid"+Environment.NewLine+"Leave (Amount)", 
                        "OT 1"+Environment.NewLine+"(Hour)", 
                        "OT 2"+Environment.NewLine+"(Hour)", 
                        "OT Holiday"+Environment.NewLine+"(Hour)", 
                        "OT 1"+Environment.NewLine+"(Amount)", 
                        
                        //31-35
                        "OT 2"+Environment.NewLine+"(Amount)", 
                        "OT Holiday"+Environment.NewLine+"(Amount)", 
                        "Taxable Fixed"+Environment.NewLine+"Allowance", 
                        "Non Taxable Fixed"+Environment.NewLine+"Allowance", 
                        "Fixed"+Environment.NewLine+"Allowance", 
                        
                        //36-40
                        "Performance"+Environment.NewLine+"Allowance", 
                        "OT"+Environment.NewLine+"Allowance",
                        "",
                        "Meal", 
                        "Transport", 

                        //41-45
                        "Tax"+Environment.NewLine+"Allowance",
                        "SS Employer"+Environment.NewLine+"Health", 
                        "SS Employee"+Environment.NewLine+"Health", 
                        "SS Employer"+Environment.NewLine+"Employment",
                        "SS Employee"+Environment.NewLine+"Employment", 

                        //46-50
                        "SS Employer"+Environment.NewLine+"Pension",
                        "SS Employee"+Environment.NewLine+"Pension",
                        "SS Employer"+Environment.NewLine+"Jiwasraya",
                        "SS Employee"+Environment.NewLine+"Jiwasraya",
                        "Non Taxable Employee's"+Environment.NewLine+"Deduction",

                        //51-55
                        "Taxable Employee's"+Environment.NewLine+"Deduction", 
                        "Fixed"+Environment.NewLine+"Deduction", 
                        "Employee's"+Environment.NewLine+"Advance Payment",
                        "Salary"+Environment.NewLine+"Adjustment", 
                        "Brutto",

                        //56-60
                        "Tax", 
                        "End of Year"+Environment.NewLine+"Tax", 
                        "Take Home Pay", 
                        "Voucher Request#"+Environment.NewLine+"(Payroll)",
                        "Employment"+Environment.NewLine+"Status",

                        //61-62
                        "",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 80, 200, 
                        
                        //6-10
                        80, 250, 150, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 150, 130, 
                        
                        //16-20
                        100, 100, 100, 100, 100,  
                        
                        //21-25
                        100, 100, 100, 100, 100, 
                        
                        //26-30
                        100, 100, 100, 100, 100, 

                        //31-35
                        100, 100, 130, 130, 100, 

                        //36-40
                        100, 100, 20, 0, 0,

                        //41-50
                        100, 100, 100, 100, 100,

                        //46-50
                        100, 100, 100, 100, 120,
                        
                        //51-55
                        120, 100, 130, 100, 150, 
                        
                        //56-60
                        130, 100, 100, 130, 180,

                        //61-62
                        20, 20
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 38, 61, 62 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 39, 40, 
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 
                51, 52, 53, 54, 55, 56, 57, 58
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] 
            { 
                0, 
                2, 3, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 39, 40, 
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52, 53, 54, 55, 56, 57, 58, 59, 60
            });
            Sm.GrdColInvisible(Grd1, new int[] { 11, 13 }, false);
            Grd1.Cols[60].Move(12);
            Grd1.Cols[61].Move(37);
            Grd1.Cols[62].Move(55);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "PayrunCode",
                        
                            //1-5
                            "PayrunName",
                            "EmpCode",
                            "EmpName",
                            "EmpCodeOld",
                            "PosName",
                            
                            //6-10
                            "DeptName",
                            "JoinDt",
                            "ResignDt",
                            "SystemTypeDesc", 
                            "PayrunPeriodDesc",
                            
                            //11-15
                            "PGName",
                            "SiteName",
                            "NPWP",
                            "NonTaxableIncomeDesc",
                            "Salary", 

                            //16-20
                            "WorkingDay", 
                            "PLDay", 
                            "PLHr", 
                            "PLAmt", 
                            "ProcessPLAmt", 
                            
                            //21-25
                            "UPLDay",
                            "UPLHr", 
                            "UPLAmt", 
                            "ProcessUPLAmt",
                            "OT1Hr", 
                            
                            //26-30
                            "OT2Hr", 
                            "OTHolidayHr", 
                            "OT1Amt", 
                            "OT2Amt", 
                            "OTHolidayAmt", 
                            
                            //31-35
                            "TaxableFixAllowance",
                            "NonTaxableFixAllowance",
                            "FixAllowance", 
                            "PerformanceValue", 
                            "ADOT",

                            //36-40
                            "Meal",
                            "Transport",
                            "TaxAllowance",
                            "SSEmployerHealth", 
                            "SSEmployeeHealth", 
                            
                            //41-45
                            "SSEmployerEmployment", 
                            "SSEmployeeEmployment",
                            "SSErPension", 
                            "SSEePension",
                            "SSEmployerPension", 
                            
                            //46-50
                            "SSEmployeePension", 
                            "NonTaxableFixDeduction",
                            "TaxableFixDeduction",
                            "FixDeduction", 
                            "EmpAdvancePayment",

                            //51-55
                            "SalaryAdjustment", 
                            "Brutto",
                            "Tax", 
                            "EOYTax",
                            "Amt",
                            
                            //56-57
                            "VoucherRequestPayrollDocNo",
                            "EmploymentStatusDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) => 
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 37);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 38);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 39);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 40);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 41);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 42);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 43);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 44);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 45);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 46);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 47);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 48);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 49);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 50);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 54, 51);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 55, 52);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 56, 53);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 57, 54);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 58, 55);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 59, 56);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 60, 57);
                            
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    { 
                        17, 18, 19, 20, 
                        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                        31, 32, 33, 34, 35, 36, 37, 39, 40, 
                        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 
                        51, 52, 53, 54, 55, 56, 57, 58
                    });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    Sm.FormShowDialog(
                        new FrmRptPayrollProcessSummary5Dlg(
                            this,
                            Sm.GetGrdStr(Grd1, r, 2),
                            Sm.GetGrdStr(Grd1, r, 4)
                            ));
                }
            }
            if (e.ColIndex == 38 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 37) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessADOT(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 61 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 35) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 62 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 52) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                Sm.FormShowDialog(
                    new FrmRptPayrollProcessSummary5Dlg(
                        this,
                        Sm.GetGrdStr(Grd1, r, 2),
                        Sm.GetGrdStr(Grd1, r, 4)
                        ));
            }
            if (e.ColIndex == 38 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 37) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessADOT(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 61 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 35) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 62 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 52) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (IsGrdEmpty() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<PayrollProcess>();
            var ldtl = new List<PayrollProcessDtl>();
            string[] TableName = { "PayrollProcess", "PayrollProcessDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where Menucode=@MenuCode) As MenuDesc ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                        "CompanyLogo",

                         //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "MenuDesc",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PayrollProcess()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MenuDesc = Sm.DrStr(dr, c[4]),
                            UserName = Gv.CurrentUserCode,
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.PayrunCode, B.PayrunName, C.EmpName, C.EmpCodeOld, D.DeptName, A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployeeHealth, ");
                SQLDtl.AppendLine("A.SSEmployeeEmployment, A.Amt, (A.SSEmployerHealth+A.SSEmployerEmployment+A.SSEmployeeHealth+A.SSEmployeeEmployment+A.Amt)As TotalTHPBPJS, ");
                SQLDtl.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment)As TotBPJSKet ");
                SQLDtl.AppendLine("From tblpayrollprocess1 A ");
                SQLDtl.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
                SQLDtl.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
                SQLDtl.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQLDtl.AppendLine("Where A.PayrunCode in ( ");

                int x = Grd1.Rows.Count-1;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (i == x)
                    {
                        SQLDtl.AppendLine("'" + Sm.GetGrdStr(Grd1, i, 2) + "'");

                    }
                    else
                    {
                        SQLDtl.AppendLine("'" + Sm.GetGrdStr(Grd1, i, 2) + "'");
                        SQLDtl.AppendLine(", ");
                    }
                }
                SQLDtl.AppendLine(" );");

                cmDtl.CommandText = SQLDtl.ToString();
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "PayrunCode",

                    //1-5
                    "PayrunName",
                    "EmpName",
                    "EmpCodeOld",
                    "DeptName",
                    "SSEmployerHealth",

                    //6-10
                    "SSEmployerEmployment",
                    "SSEmployeeHealth",
                    "SSEmployeeEmployment",
                    "Amt",
                    "TotalTHPBPJS",

                    //11
                    "TotBPJSKet",
                   
                });

                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PayrollProcessDtl()
                        {
                            nomor = nomor,
                            PayrunCode = Sm.DrStr(drDtl, cDtl[0]),

                            PayrunName = Sm.DrStr(drDtl, cDtl[1]),
                            EmpName = Sm.DrStr(drDtl, cDtl[2]),
                            EmpCodeOld = Sm.DrStr(drDtl, cDtl[3]),
                            DeptName = Sm.DrStr(drDtl, cDtl[4]),
                            SSEmployerHealth = Sm.DrDec(drDtl, cDtl[5]),

                            SSEmployerEmployment = Sm.DrDec(drDtl, cDtl[6]),
                            SSEmployeeHealth = Sm.DrDec(drDtl, cDtl[7]),
                            SSEmployeeEmployment = Sm.DrDec(drDtl, cDtl[8]),
                            Amt = Sm.DrDec(drDtl, cDtl[9]),
                            TotalTHPBPJS = Sm.DrDec(drDtl, cDtl[10]),

                            TotBPJSKet = Sm.DrDec(drDtl, cDtl[11]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("PayrollProcess", myLists, TableName, false);
        }

        #region Additional Method

        private void ShowPayrollProcessADOT(string PayrunCode, string EmpCode)
        {
            StringBuilder
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, Sum(A.Amt) As Amt ");
            SQL.AppendLine("From TblPayrollProcessADOT A ");
            SQL.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group By B.ADName ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowPayrollProcessAD(string ADType, string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, Sum(A.Amt) As Amt ");
            SQL.AppendLine("From TblPayrollProcessAD A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType=@ADType And B.AmtType='1' ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group By B.ADName ");
            SQL.AppendLine("Having Sum(A.Amt)<>0.00 ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParam<String>(ref cm, "@ADType", ADType);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayCod_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPayCod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        #endregion

        #endregion

        #region Report Class

        class PayrollProcess
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string UserName { get; set; }
            public string PrintBy { get; set; }

        }

        class PayrollProcessDtl
        {
            public int nomor { get; set; }
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalTHPBPJS { get; set; }
            public decimal TotBPJSKet { get; set; }
        }

        #endregion
    }
}
