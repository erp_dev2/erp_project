﻿#region Update
/*
    19/09/2017 [TKG] tambah informasi local document#, employment status, remark
    19/09/2017 [TKG] employment status tidak muncul
    04/11/2017 [TKG] tambah filter status
    14/11/2017 [HAR] Filter status diganti active terhadap employee buka document nya 
    13/03/2018 [TKG] tambah contract extension, validasi employment status, hilangkan gaji
    16/08/2019 [TKG] tambah filter site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmEmpContractDocFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmpContractDoc mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpContractDocFind(FrmEmpContractDoc FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, A.LocalDocNo, ");
            SQL.AppendLine("A.EmpCode, B.EmpCodeOld, B.EmpName, A.RenewalInd, ");
            SQL.AppendLine("G.SiteName, C.DeptName, D.PosName, E.GrdLvlName, F.OptDesc As EmploymentStatusDesc, A.StartDt, A.EndDt, ");
            SQL.AppendLine("if(DateDiff(EndDt, concat(replace(curdate(), '-', '')))<0, 0, DateDiff(EndDt, concat(replace(curdate(), '-', '')))) As Remaining, ");
            SQL.AppendLine("A.ContractExt, A.Remark, ");  
            SQL.AppendLine("A.CreateBy, A.CreateDt,A.LastUpBy,A.LastUpDt  ");
            SQL.AppendLine("From TblEmpContractDoc A ");
            SQL.AppendLine("Left Join TblEmployee B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=B.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (B.SiteCode Is Null Or ( ");
                SQL.AppendLine("    B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode = E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption F On A.EmploymentStatus=F.OptCode And F.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblSite G On B.SiteCode=G.SiteCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Active",
                        "Local"+Environment.NewLine+"Document#",
                        "Employee's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Old Code",                       
                        "Employee's"+Environment.NewLine+"Name",
                        "Renewal",
                        "Site",
                        "Department",
                        
                        //11-15
                        "Position",
                        "Grade",
                        "Employment"+Environment.NewLine+"Status",
                        "Contract"+Environment.NewLine+"Extension",
                        "Start Date",
                        
                        //16-20
                        "End Date",
                        "Remaining"+Environment.NewLine+"Day",
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        
                        //21-24
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 100, 100, 100,  
                        
                        //6-10
                        100, 200, 100, 200, 200, 

                        //11-15
                        200, 200, 150, 150, 120,

                        //16-20
                        120, 120, 200, 120, 120, 

                        //21-24
                        120, 120, 120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 16, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdFormatDec(Grd1, new int[] { 17 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 14 }, 11);
            Sm.GrdColCheck(Grd1, new int[] { 3, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 19, 20, 21, 22, 23, 24 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@DocDtNow", Sm.ServerCurrentDateTime());

                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        Filter = " And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@DocDtNow)) ";
                        break;
                    case "2":
                        Filter = " And (ResignDt Is Not Null And ResignDt<=@DocDtNow) ";
                        break;
                }


                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpCodeOld", "B.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "ActInd", "LocalDocNo", "EmpCode", "EmpCodeOld", 
                            
                            //6-10
                            "EmpName", "RenewalInd", "SiteName", "DeptName", "PosName", 
                            
                            //11-15
                            "GrdLvlName", "EmploymentStatusDesc", "ContractExt", "StartDt", "EndDt", 
                            
                            //16-20
                            "Remaining", "Remark", "CreateBy", "CreateDt","LastUpBy", 
                            
                            //21
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Additional Method

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status Employee");
        }

        #endregion

        #endregion
    }
}
