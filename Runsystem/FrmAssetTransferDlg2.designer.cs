﻿namespace RunSystem
{
    partial class FrmAssetTransferDlg2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.TxtCCCodeTo = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCCCodeFrom = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRequestDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 74);
            this.panel3.Size = new System.Drawing.Size(506, 399);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(506, 399);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit_1);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtCCCodeTo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtCCCodeFrom);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtRequestDocNo);
            this.panel2.Size = new System.Drawing.Size(506, 74);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "CostCenter (To)";
            // 
            // TxtCCCodeTo
            // 
            this.TxtCCCodeTo.EnterMoveNextControl = true;
            this.TxtCCCodeTo.Location = new System.Drawing.Point(132, 49);
            this.TxtCCCodeTo.Name = "TxtCCCodeTo";
            this.TxtCCCodeTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCodeTo.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCodeTo.Properties.MaxLength = 30;
            this.TxtCCCodeTo.Size = new System.Drawing.Size(208, 20);
            this.TxtCCCodeTo.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "CostCenter (From)";
            // 
            // TxtCCCodeFrom
            // 
            this.TxtCCCodeFrom.EnterMoveNextControl = true;
            this.TxtCCCodeFrom.Location = new System.Drawing.Point(132, 27);
            this.TxtCCCodeFrom.Name = "TxtCCCodeFrom";
            this.TxtCCCodeFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCodeFrom.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCodeFrom.Properties.MaxLength = 30;
            this.TxtCCCodeFrom.Size = new System.Drawing.Size(208, 20);
            this.TxtCCCodeFrom.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Request Document";
            // 
            // TxtRequestDocNo
            // 
            this.TxtRequestDocNo.EnterMoveNextControl = true;
            this.TxtRequestDocNo.Location = new System.Drawing.Point(132, 5);
            this.TxtRequestDocNo.Name = "TxtRequestDocNo";
            this.TxtRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRequestDocNo.Properties.MaxLength = 30;
            this.TxtRequestDocNo.Size = new System.Drawing.Size(208, 20);
            this.TxtRequestDocNo.TabIndex = 18;
            // 
            // FrmAssetTransferDlg2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 473);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmAssetTransferDlg2";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCodeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRequestDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.TextEdit TxtCCCodeTo;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.TextEdit TxtCCCodeFrom;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtRequestDocNo;
    }
}