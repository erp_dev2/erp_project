﻿namespace RunSystem
{
    partial class FrmDepreciationAsset3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDepreciationAsset3));
            this.panel4 = new System.Windows.Forms.Panel();
            this.EconomicLifeYearRule = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.TxtResidualValue = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDepreciationCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtEcoLifeYr = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtCC = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtPercentageAnnualDepreciation = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtEcoLife = new DevExpress.XtraEditors.TextEdit();
            this.TxtAssetValue = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAssetName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAssetCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDepreciationDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnAssetCode = new DevExpress.XtraEditors.SimpleButton();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.DepreciationDateRule = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResidualValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDepreciationCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLifeYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentageAnnualDepreciation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLife.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepreciationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepreciationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(889, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DepreciationDateRule);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.BtnAssetCode);
            this.panel2.Controls.Add(this.TxtDisplayName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtAssetName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtAssetCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.DteDepreciationDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Size = new System.Drawing.Size(889, 193);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 193);
            this.panel3.Size = new System.Drawing.Size(889, 280);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(889, 280);
            this.Grd1.TabIndex = 53;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.EconomicLifeYearRule);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.TxtStatus);
            this.panel4.Controls.Add(this.TxtResidualValue);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.LueDepreciationCode);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.TxtEcoLifeYr);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.TxtCC);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtPercentageAnnualDepreciation);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtEcoLife);
            this.panel4.Controls.Add(this.TxtAssetValue);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(462, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(427, 193);
            this.panel4.TabIndex = 31;
            // 
            // EconomicLifeYearRule
            // 
            this.EconomicLifeYearRule.AutoSize = true;
            this.EconomicLifeYearRule.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EconomicLifeYearRule.ForeColor = System.Drawing.Color.Red;
            this.EconomicLifeYearRule.Location = new System.Drawing.Point(45, 53);
            this.EconomicLifeYearRule.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.EconomicLifeYearRule.Name = "EconomicLifeYearRule";
            this.EconomicLifeYearRule.Size = new System.Drawing.Size(21, 14);
            this.EconomicLifeYearRule.TabIndex = 36;
            this.EconomicLifeYearRule.Text = "(!)";
            this.EconomicLifeYearRule.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.EconomicLifeYearRule.Click += new System.EventHandler(this.EconomicLifeYearRule_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(107, 141);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 14);
            this.label19.TabIndex = 49;
            this.label19.Text = "Status";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(153, 138);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 250;
            this.TxtStatus.Size = new System.Drawing.Size(263, 20);
            this.TxtStatus.TabIndex = 50;
            // 
            // TxtResidualValue
            // 
            this.TxtResidualValue.EnterMoveNextControl = true;
            this.TxtResidualValue.Location = new System.Drawing.Point(153, 28);
            this.TxtResidualValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtResidualValue.Name = "TxtResidualValue";
            this.TxtResidualValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtResidualValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtResidualValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtResidualValue.Properties.Appearance.Options.UseFont = true;
            this.TxtResidualValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtResidualValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtResidualValue.Size = new System.Drawing.Size(262, 20);
            this.TxtResidualValue.TabIndex = 35;
            this.TxtResidualValue.Validated += new System.EventHandler(this.TxtResidualValue_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(65, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 14);
            this.label4.TabIndex = 34;
            this.label4.Text = "Residual Value";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDepreciationCode
            // 
            this.LueDepreciationCode.EnterMoveNextControl = true;
            this.LueDepreciationCode.Location = new System.Drawing.Point(153, 72);
            this.LueDepreciationCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDepreciationCode.Name = "LueDepreciationCode";
            this.LueDepreciationCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.Appearance.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDepreciationCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDepreciationCode.Properties.DropDownRows = 5;
            this.LueDepreciationCode.Properties.NullText = "[Empty]";
            this.LueDepreciationCode.Properties.PopupWidth = 350;
            this.LueDepreciationCode.Size = new System.Drawing.Size(264, 20);
            this.LueDepreciationCode.TabIndex = 43;
            this.LueDepreciationCode.ToolTip = "F4 : Show/hide list";
            this.LueDepreciationCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDepreciationCode.EditValueChanged += new System.EventHandler(this.LueDepreciationCode_EditValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(243, 53);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 14);
            this.label17.TabIndex = 39;
            this.label17.Text = "Year";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEcoLifeYr
            // 
            this.TxtEcoLifeYr.EnterMoveNextControl = true;
            this.TxtEcoLifeYr.Location = new System.Drawing.Point(153, 50);
            this.TxtEcoLifeYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEcoLifeYr.Name = "TxtEcoLifeYr";
            this.TxtEcoLifeYr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEcoLifeYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEcoLifeYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEcoLifeYr.Properties.Appearance.Options.UseFont = true;
            this.TxtEcoLifeYr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEcoLifeYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEcoLifeYr.Size = new System.Drawing.Size(86, 20);
            this.TxtEcoLifeYr.TabIndex = 38;
            this.TxtEcoLifeYr.Validated += new System.EventHandler(this.TxtEcoLifeYr_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(67, 53);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 14);
            this.label18.TabIndex = 37;
            this.label18.Text = "Economic Life";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(153, 160);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(700, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(263, 20);
            this.MeeRemark.TabIndex = 52;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(102, 162);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 14);
            this.label15.TabIndex = 51;
            this.label15.Text = "Remark";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCC
            // 
            this.TxtCC.EnterMoveNextControl = true;
            this.TxtCC.Location = new System.Drawing.Point(153, 116);
            this.TxtCC.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCC.Name = "TxtCC";
            this.TxtCC.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCC.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCC.Properties.Appearance.Options.UseFont = true;
            this.TxtCC.Properties.MaxLength = 250;
            this.TxtCC.Size = new System.Drawing.Size(263, 20);
            this.TxtCC.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(243, 97);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 14);
            this.label11.TabIndex = 46;
            this.label11.Text = "%";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPercentageAnnualDepreciation
            // 
            this.TxtPercentageAnnualDepreciation.EnterMoveNextControl = true;
            this.TxtPercentageAnnualDepreciation.Location = new System.Drawing.Point(153, 94);
            this.TxtPercentageAnnualDepreciation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPercentageAnnualDepreciation.Name = "TxtPercentageAnnualDepreciation";
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Options.UseFont = true;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPercentageAnnualDepreciation.Size = new System.Drawing.Size(86, 20);
            this.TxtPercentageAnnualDepreciation.TabIndex = 45;
            this.TxtPercentageAnnualDepreciation.Validated += new System.EventHandler(this.TxtPercentageAnnualDepreciation_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(4, 97);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 14);
            this.label12.TabIndex = 44;
            this.label12.Text = "Annual Depreciation Rate";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(45, 119);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 14);
            this.label9.TabIndex = 47;
            this.label9.Text = "Initial Cost Center";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(28, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 14);
            this.label8.TabIndex = 42;
            this.label8.Text = "Depreciation Method";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(371, 53);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 41;
            this.label7.Text = "Months";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEcoLife
            // 
            this.TxtEcoLife.EnterMoveNextControl = true;
            this.TxtEcoLife.Location = new System.Drawing.Point(281, 50);
            this.TxtEcoLife.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEcoLife.Name = "TxtEcoLife";
            this.TxtEcoLife.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEcoLife.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEcoLife.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEcoLife.Properties.Appearance.Options.UseFont = true;
            this.TxtEcoLife.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEcoLife.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEcoLife.Size = new System.Drawing.Size(87, 20);
            this.TxtEcoLife.TabIndex = 40;
            this.TxtEcoLife.Validated += new System.EventHandler(this.TxtEcoLife_Validated);
            // 
            // TxtAssetValue
            // 
            this.TxtAssetValue.EnterMoveNextControl = true;
            this.TxtAssetValue.Location = new System.Drawing.Point(153, 6);
            this.TxtAssetValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetValue.Name = "TxtAssetValue";
            this.TxtAssetValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetValue.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAssetValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAssetValue.Size = new System.Drawing.Size(262, 20);
            this.TxtAssetValue.TabIndex = 33;
            this.TxtAssetValue.Validated += new System.EventHandler(this.TxtAssetValue_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(78, 8);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 14);
            this.label16.TabIndex = 32;
            this.label16.Text = "Asset Value";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(116, 93);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 200;
            this.TxtDisplayName.Size = new System.Drawing.Size(322, 20);
            this.TxtDisplayName.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(34, 96);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 14);
            this.label10.TabIndex = 22;
            this.label10.Text = "Display Name";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(116, 136);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(322, 20);
            this.TxtItName.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(44, 139);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 14);
            this.label3.TabIndex = 26;
            this.label3.Text = "Item Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(116, 115);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Size = new System.Drawing.Size(198, 20);
            this.TxtItCode.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(47, 118);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 24;
            this.label6.Text = "Item Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetName
            // 
            this.TxtAssetName.EnterMoveNextControl = true;
            this.TxtAssetName.Location = new System.Drawing.Point(116, 71);
            this.TxtAssetName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetName.Name = "TxtAssetName";
            this.TxtAssetName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetName.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetName.Properties.MaxLength = 200;
            this.TxtAssetName.Size = new System.Drawing.Size(322, 20);
            this.TxtAssetName.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(40, 74);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "Asset Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetCode
            // 
            this.TxtAssetCode.EnterMoveNextControl = true;
            this.TxtAssetCode.Location = new System.Drawing.Point(116, 49);
            this.TxtAssetCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetCode.Name = "TxtAssetCode";
            this.TxtAssetCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetCode.Properties.MaxLength = 30;
            this.TxtAssetCode.Size = new System.Drawing.Size(198, 20);
            this.TxtAssetCode.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(43, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Asset Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDepreciationDt
            // 
            this.DteDepreciationDt.EditValue = null;
            this.DteDepreciationDt.EnterMoveNextControl = true;
            this.DteDepreciationDt.Location = new System.Drawing.Point(116, 158);
            this.DteDepreciationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDepreciationDt.Name = "DteDepreciationDt";
            this.DteDepreciationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDepreciationDt.Properties.Appearance.Options.UseFont = true;
            this.DteDepreciationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDepreciationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDepreciationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDepreciationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDepreciationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDepreciationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDepreciationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDepreciationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDepreciationDt.Properties.MaxLength = 8;
            this.DteDepreciationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDepreciationDt.Size = new System.Drawing.Size(170, 20);
            this.DteDepreciationDt.TabIndex = 29;
            this.DteDepreciationDt.EditValueChanged += new System.EventHandler(this.DteDepreciationDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(7, 161);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 14);
            this.label2.TabIndex = 28;
            this.label2.Text = "Depreciation Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAssetCode
            // 
            this.BtnAssetCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAssetCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAssetCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAssetCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAssetCode.Appearance.Options.UseBackColor = true;
            this.BtnAssetCode.Appearance.Options.UseFont = true;
            this.BtnAssetCode.Appearance.Options.UseForeColor = true;
            this.BtnAssetCode.Appearance.Options.UseTextOptions = true;
            this.BtnAssetCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAssetCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAssetCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnAssetCode.Image")));
            this.BtnAssetCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAssetCode.Location = new System.Drawing.Point(322, 48);
            this.BtnAssetCode.Name = "BtnAssetCode";
            this.BtnAssetCode.Size = new System.Drawing.Size(24, 21);
            this.BtnAssetCode.TabIndex = 19;
            this.BtnAssetCode.ToolTip = "Find Asset";
            this.BtnAssetCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAssetCode.ToolTipTitle = "Run System";
            this.BtnAssetCode.Click += new System.EventHandler(this.BtnAssetCode_Click);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(320, 5);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 14;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(116, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(129, 20);
            this.DteDocDt.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(79, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 14);
            this.label13.TabIndex = 15;
            this.label13.Text = "Date";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(116, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(198, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(39, 8);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 14);
            this.label14.TabIndex = 12;
            this.label14.Text = "Document#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DepreciationDateRule
            // 
            this.DepreciationDateRule.AutoSize = true;
            this.DepreciationDateRule.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DepreciationDateRule.ForeColor = System.Drawing.Color.Red;
            this.DepreciationDateRule.Location = new System.Drawing.Point(295, 161);
            this.DepreciationDateRule.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.DepreciationDateRule.Name = "DepreciationDateRule";
            this.DepreciationDateRule.Size = new System.Drawing.Size(21, 14);
            this.DepreciationDateRule.TabIndex = 30;
            this.DepreciationDateRule.Text = "(!)";
            this.DepreciationDateRule.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.DepreciationDateRule.Click += new System.EventHandler(this.DepreciationDateRule_Click);
            // 
            // FrmDepreciationAsset3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 473);
            this.Name = "FrmDepreciationAsset3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResidualValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDepreciationCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLifeYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentageAnnualDepreciation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLife.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepreciationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDepreciationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtAssetName;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtAssetCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtPercentageAnnualDepreciation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtEcoLife;
        internal DevExpress.XtraEditors.TextEdit TxtAssetValue;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.DateEdit DteDepreciationDt;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnAssetCode;
        internal DevExpress.XtraEditors.TextEdit TxtCC;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtEcoLifeYr;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.LookUpEdit LueDepreciationCode;
        internal DevExpress.XtraEditors.TextEdit TxtResidualValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label DepreciationDateRule;
        private System.Windows.Forms.Label EconomicLifeYearRule;
    }
}