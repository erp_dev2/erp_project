﻿namespace RunSystem
{
    partial class FrmRptNewsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPhoto = new DevExpress.XtraEditors.TextEdit();
            this.PicBoxContent = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxContent)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(491, 0);
            this.panel1.Size = new System.Drawing.Size(70, 395);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAdd.Appearance.Options.UseBackColor = true;
            this.BtnAdd.Appearance.Options.UseFont = true;
            this.BtnAdd.Appearance.Options.UseForeColor = true;
            this.BtnAdd.Appearance.Options.UseTextOptions = true;
            this.BtnAdd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.PicBoxContent);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(491, 395);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.TxtPhoto);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(487, 29);
            this.panel3.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhoto
            // 
            this.TxtPhoto.EnterMoveNextControl = true;
            this.TxtPhoto.Location = new System.Drawing.Point(46, 4);
            this.TxtPhoto.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhoto.Name = "TxtPhoto";
            this.TxtPhoto.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhoto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhoto.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhoto.Properties.Appearance.Options.UseFont = true;
            this.TxtPhoto.Properties.MaxLength = 250;
            this.TxtPhoto.Properties.ReadOnly = true;
            this.TxtPhoto.Size = new System.Drawing.Size(436, 20);
            this.TxtPhoto.TabIndex = 6;
            // 
            // PicBoxContent
            // 
            this.PicBoxContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PicBoxContent.Location = new System.Drawing.Point(0, 29);
            this.PicBoxContent.Name = "PicBoxContent";
            this.PicBoxContent.Size = new System.Drawing.Size(487, 362);
            this.PicBoxContent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBoxContent.TabIndex = 15;
            this.PicBoxContent.TabStop = false;
            // 
            // FrmRptNewsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 395);
            this.Name = "FrmRptNewsDlg";
            this.Text = "Photo";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxContent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PicBoxContent;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtPhoto;
    }
}