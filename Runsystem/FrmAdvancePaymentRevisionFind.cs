﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAdvancePaymentRevisionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmAdvancePaymentRevision _frmParent;

        #endregion

        #region Constructor

        public FrmAdvancePaymentRevisionFind(FrmAdvancePaymentRevision FrmParent)
        {
            InitializeComponent();
            _frmParent = FrmParent;
        }

        #endregion

        #region Method

        private void FrmAdvancePaymentFind_Load(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = _frmParent.Text;
            Sm.ButtonVisible(_frmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
             Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document"+Environment.NewLine+"Number", 
                        "Document"+Environment.NewLine+"Date",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's"+Environment.NewLine+"Name", 
                        "Amount",
                        //6-10
                        "Term"+Environment.NewLine+"Of Payment",
                        "Start Month",
                        "Year",
                        "Remark",
                        "Revision",
                        //11-15
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        //16
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 90, 180, 100, 
                        
                        //6-10
                        90, 100, 70, 150, 80,  
                        
                        //11-15
                        80, 100, 100, 100, 100, 
                        
                        100 
                        
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColInvisible(Grd1, new int[] {3, 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {3, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where A.EmpCode=B.EmpCode ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.DocNo, A.DocDt, A.EmpCode, B.EmpName, A.Amt, A.Top, Case A.StartMth When '01' Then 'January' When '02' Then 'February' " +
                        "When '03' Then 'March' When '04' Then 'April' When '05' Then 'May' When '06' Then 'June' When '07' Then 'July' When '08' Then 'August' "+
                        "When '09' Then 'September' When '10' Then 'October' When '11' Then 'November' When '10' Then 'December' End As StartMth," +
                        "A.Yr, A.Remark, A.Revision, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt " +
                        "From TblAdvancePaymentRevisionHdr A, TblEmployee B " +
                        Filter + "",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "EmpCode", "EmpName", "Amt", "Top",   
                            
                            //6-10
                            "StartMth", "Yr", "Remark", "Revision", "CreateBy",  
                            
                            //11-13
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                _frmParent.ShowDataAdvancePayment(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), 1);
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event


        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document");
        }
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }
        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

    }
}
