﻿#region Update
/*
    21/04/2022 [HAR/GSS] tambah input COA
    12/05/2022 [RDA/PRODUCT] resize action bar
   
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmInvestmentCategoryFind FrmFind;
        private bool
            mIsInvestmentCtAcNoMandatory = false,
            mIsInvestmentCtAcNo2Mandatory = false,
            mIsInvestmentCtAcNo3Mandatory = false,
            mIsInvestmentCtAcNo4Mandatory = false,
            mIsInvestmentCtAcNo5Mandatory = false,
            mIsInvestmentCtAcNo6Mandatory = false,
            mIsInvestmentCtAcNo7Mandatory = false;
        internal string 
            mAcNoForStoreSales = string.Empty,
            mAcNoForSalesReturnInvoice = string.Empty,
            mAcNoForCOGS = string.Empty;
        internal bool
            mIsCOAUseAlias = false;
        #endregion

        #region Constructor

        public FrmInvestmentCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
                if (mIsInvestmentCtAcNoMandatory) LblCOAStock.ForeColor = Color.Red;
                if (mIsInvestmentCtAcNo4Mandatory) LblCOASales.ForeColor = Color.Red;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtInvestmentCtCode, TxtInvestmentCtName }, true);
                    
                    ChkActiveInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    BtnAcNo3.Enabled = false;
                    BtnAcNo4.Enabled = false;
                    BtnAcNo5.Enabled = false;
                    BtnAcNo6.Enabled = false;
                    BtnAcNo7.Enabled = false;
                    BtnAcNo8.Enabled = false;
                    BtnAcNo9.Enabled = false;

                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtInvestmentCtCode, TxtInvestmentCtName }, false);
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    BtnAcNo9.Enabled = true;

                    TxtInvestmentCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtInvestmentCtName }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    BtnAcNo9.Enabled = true;

                    TxtInvestmentCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtInvestmentCtCode, TxtInvestmentCtName, 
                TxtAcNo, TxtAcDesc, 
                TxtAcNo2, TxtAcDesc2,
                TxtAcNo3, TxtAcDesc3,
                TxtAcNo4, TxtAcDesc4,
                TxtAcNo5, TxtAcDesc5,
                TxtAcNo6, TxtAcDesc6,
                TxtAcNo7, TxtAcDesc7,
                TxtAcNo8, TxtAcDesc8,
                TxtAcNo9, TxtAcDesc9,

            });
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActiveInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInvestmentCtCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInvestmentCtCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblInvestmentCategory Where InvestmentCtCode=@InvestmentCtCode" };
                Sm.CmParam<String>(ref cm, "@InvestmentCtCode", TxtInvestmentCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

       

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblInvestmentCategory(InvestmentCtCode, InvestmentCtName, ActInd, AcNo, AcNo2,  ");
                SQL.AppendLine("AcNo3, AcNo4, AcNo5, AcNo6, AcNo7, Acno8, Acno9, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@InvestmentCtCode, @InvestmentCtName, @ActInd, @AcNo, @AcNo2, ");
                SQL.AppendLine("@AcNo3, @AcNo4, @AcNo5, @AcNo6, @AcNo7, @Acno8, @Acno9, ");
                SQL.AppendLine("@UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update InvestmentCtName=@InvestmentCtName, ActInd=@ActInd, ");
                SQL.AppendLine("   AcNo=@AcNo, AcNo2=@AcNo2, ");
                SQL.AppendLine("   AcNo3=@AcNo3, Acno4=@AcNo4, Acno5=@AcNo5, AcNo6=@AcNo6, Acno7=@AcNo7, Acno8=@Acno8, Acno9=@Acno9, ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@InvestmentCtCode", TxtInvestmentCtCode.Text);
                Sm.CmParam<String>(ref cm, "@InvestmentCtName", TxtInvestmentCtName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
                Sm.CmParam<String>(ref cm, "@AcNo3", TxtAcNo3.Text);
                Sm.CmParam<String>(ref cm, "@AcNo4", TxtAcNo4.Text); 
                Sm.CmParam<String>(ref cm, "@AcNo5", TxtAcNo5.Text);
                Sm.CmParam<String>(ref cm, "@AcNo6", TxtAcNo6.Text);
                Sm.CmParam<String>(ref cm, "@AcNo7", TxtAcNo7.Text);
                Sm.CmParam<String>(ref cm, "@AcNo8", TxtAcNo8.Text);
                Sm.CmParam<String>(ref cm, "@AcNo9", TxtAcNo9.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtInvestmentCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string InvestmentCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.InvestmentCtCode, A.InvestmentCtName, A.ActInd, ");
            SQL.AppendLine("A.AcNo, A.AcNo2, A.Acno3, A.AcNo4, A.Acno5, A.Acno6, A.Acno7, A.Acno8, A.AcNo9, ");
            SQL.AppendLine("B.AcDesc, C.AcDesc As AcDesc2, ");
            SQL.AppendLine("D.AcDesc As AcDesc3, E.AcDesc As AcDesc4, ");
            SQL.AppendLine("F.AcDesc As AcDesc5, G.AcDesc As AcDesc6, ");
            SQL.AppendLine("H.AcDesc As AcDesc7, I.AcDesc As AcDesc8, ");
            SQL.AppendLine("J.AcDesc As AcDesc9 ");
            SQL.AppendLine("From TblInvestmentCategory A ");
            SQL.AppendLine("Left Join TblCoa B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Left Join TblCoa C On A.AcNo2=C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo3=D.AcNo ");
            SQL.AppendLine("Left Join TblCoa E On A.AcNo4=E.AcNo ");
            SQL.AppendLine("Left Join TblCoa F On A.AcNo5=F.AcNo ");
            SQL.AppendLine("Left Join TblCoa G On A.AcNo6=G.AcNo ");
            SQL.AppendLine("Left Join TblCoa H On A.AcNo7=H.AcNo ");
            SQL.AppendLine("Left Join TblCoa I On A.AcNo8=I.AcNo ");
            SQL.AppendLine("Left Join TblCoa J On A.AcNo9=J.AcNo ");
            SQL.AppendLine("Where A.InvestmentCtCode=@InvestmentCtCode;");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@InvestmentCtCode", InvestmentCtCode);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "InvestmentCtCode",
                        //1-5
                        "InvestmentCtName", "ActInd", "AcNo", "AcNo2", "Acno3", 
                        //6-10
                        "AcNo4", "Acno5", "Acno6",  "Acno7", "Acno8", 
                        //11-15
                        "AcNo9", "AcDesc", "AcDesc2","AcDesc3", "AcDesc4",  
                        //16-20
                        "AcDesc5", "AcDesc6", "AcDesc7", "AcDesc8", "AcDesc9"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtInvestmentCtCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtInvestmentCtName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtAcNo2.EditValue = Sm.DrStr(dr, c[4]);
                        TxtAcNo3.EditValue = Sm.DrStr(dr, c[5]);
                        TxtAcNo4.EditValue = Sm.DrStr(dr, c[6]);
                        TxtAcNo5.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAcNo6.EditValue = Sm.DrStr(dr, c[8]);
                        TxtAcNo7.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAcNo8.EditValue = Sm.DrStr(dr, c[10]);
                        TxtAcNo9.EditValue = Sm.DrStr(dr, c[11]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[12]);
                        TxtAcDesc2.EditValue = Sm.DrStr(dr, c[13]);
                        TxtAcDesc3.EditValue = Sm.DrStr(dr, c[14]);
                        TxtAcDesc4.EditValue = Sm.DrStr(dr, c[15]);
                        TxtAcDesc5.EditValue = Sm.DrStr(dr, c[16]);
                        TxtAcDesc6.EditValue = Sm.DrStr(dr, c[17]);
                        TxtAcDesc7.EditValue = Sm.DrStr(dr, c[18]);
                        TxtAcDesc8.EditValue = Sm.DrStr(dr, c[19]);
                        TxtAcDesc9.EditValue = Sm.DrStr(dr, c[20]);

                    }, true
                );
            }
            catch (Exception Exc)

            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtInvestmentCtCode, "Investment's category code", false) ||
                Sm.IsTxtEmpty(TxtInvestmentCtName, "Investment's category name", false) ||
                Sm.IsTxtEmpty(TxtAcDesc, "COA account stock", false) ||
                Sm.IsTxtEmpty(TxtAcDesc2, "COA account sales", false) ||
                Sm.IsTxtEmpty(TxtAcDesc3, "COA account receivable", false) ||
                Sm.IsTxtEmpty(TxtAcDesc4, "COA account payable", false) ||
                Sm.IsTxtEmpty(TxtAcDesc5, "COA account FVPL", false) ||
                Sm.IsTxtEmpty(TxtAcDesc6, "COA account FVOCI", false) ||
                Sm.IsTxtEmpty(TxtAcDesc7, "COA account Gain/Loss", false) ||
                IsInvestmentCtCodeExisted()||
                IsCOAMandatoryInvalid();
        }

        private bool IsInvestmentCtCodeExisted()
        {
            if (!TxtInvestmentCtCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select InvestmentCtCode From TblInvestmentCategory Where InvestmentCtCode=@Param;", TxtInvestmentCtCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Investment's category code ( " + TxtInvestmentCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsCOAMandatoryInvalid()
        {
            if (!TxtInvestmentCtCode.Properties.ReadOnly) return false;
            if (mIsInvestmentCtAcNoMandatory && Sm.IsTxtEmpty(TxtAcNo, "COA account# for stock", false)) return true;
            if (mIsInvestmentCtAcNo2Mandatory && Sm.IsTxtEmpty(TxtAcNo2, "COA account# for sales", false)) return true;
            if (mIsInvestmentCtAcNo3Mandatory && Sm.IsTxtEmpty(TxtAcNo3, "COA account# for receivable", false)) return true;
            if (mIsInvestmentCtAcNo4Mandatory && Sm.IsTxtEmpty(TxtAcNo4, "COA account# for payable", false)) return true;
            if (mIsInvestmentCtAcNo5Mandatory && Sm.IsTxtEmpty(TxtAcNo5, "COA account# for Loss FVPL", false)) return true;
            if (mIsInvestmentCtAcNo6Mandatory && Sm.IsTxtEmpty(TxtAcNo6, "COA account# for Loss FVOCI", false)) return true;
            if (mIsInvestmentCtAcNo7Mandatory && Sm.IsTxtEmpty(TxtAcNo7, "COA account# for Realized Gain/Loss", false)) return true;

            return false;
        }

        #endregion

        #region Addditional Method
        private void GetParameter()
        {
            
            mAcNoForStoreSales = Sm.GetParameter("AcNoForStoreSales");
            mAcNoForSalesReturnInvoice = Sm.GetParameter("AcNoForSalesReturnInvoice");
            mAcNoForCOGS = Sm.GetParameter("AcNoForCOGS");
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
        }

        private void ClearCOA(DXE.TextEdit tAcCode, DXE.TextEdit tAcDesc)
        {
            tAcCode.Text = tAcDesc.Text = string.Empty;
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event


        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo, TxtAcDesc);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 1));
        }


        private void BtnAcNo4_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo2, TxtAcDesc2);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 2));
        }

        private void BtnAcNo3_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo3, TxtAcDesc3);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 3));
        }

        private void BtnAcNo4_Click_1(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo4, TxtAcDesc4);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 4));
        }

        private void BtnAcNo5_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo5, TxtAcDesc5);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 5));
        }

        private void BtnAcNo6_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo6, TxtAcDesc6);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 6));
        }

        private void BtnAcNo7_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo7, TxtAcDesc7);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 7));
        }

        private void BtnAcNo8_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo8, TxtAcDesc8);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 8));
        }

        private void BtnAcNo9_Click(object sender, EventArgs e)
        {
            ClearCOA(TxtAcNo9, TxtAcDesc9);
            Sm.FormShowDialog(new FrmInvestmentCategoryDlg(this, 9));
        }

        #endregion

        #endregion

    }
}
