﻿#region Update
/*
    30/05/2017 [TKG] tambah remark
    15/06/2017 [TKG] data dari TblCOA belum difilter berdasarkan status aktif atau tidak.
    06/07/2017 [ARI] tambah filter Entity, berdasarkan parameter IsEntityMandatory
    25/07/2017 [TKG] tambah remark dari hdr+dtl
    26/07/2017 [TKG] remark melihat kondisi journal voucher
    26/07/2017 [WED] tambah kolom parent COA
    31/07/2017 [TKG] join ke table COA tidak perlu kondisi aktif atau tidak.
    12/04/2018 [HAR] validasi entity berdasarkan entity di group user berdasarkan parameter  
    25/05/2018 [HAR] tambah kolom voucher dan voucher request
    25/06/2018 [HAR] BUg fixing (duplicate data) saat join ke voucher 
    20/07/2018 [HAR] tambah informasi vendor dan customer berdasarkan parameter, tambah giro number, openingdate, due dt 
    31/07/2018 [HAR] validasi berdasarkan parameter IsGLShowDataCustomerVendor lain bukan doctitle
    28/08/2018 [TKG] ubah cara proses informasi customer dan vendor 
    15/10/2018 [HAR] tambah parameter buat set munculin currency
    15/10/2019 [WED/IMS] filter COA Alias
    16/10/2019 [DITA/VIR] filter COA berdasrkan projectcode
    21/10/2019 [TKG/TWC] tambah opening balance
    19/05/2020 [DITA/YK] Filter COA berdasarkan group --> param = IsCOAFilteredByGroup
    27/05/2020 [WED/YK] COA ter filter nya ngelihat dari parent nya saja
    23/06/2020 [WED/SRN] tambah checkbox Equalize Journal berdasarkan parameter MenuCodeForRptGLEqualize
    28/07/2020 [IBL/MMM] tambah kolom description voucher berdasarkan parameter IsRptGLShowVoucherDesc
    20/10/2020 [VIN/PHT] Menambahkan parameter baru IsRptGLBtnPrintInvisible
    09/11/2020 [TKG/PHT] menambah filter multi coa account# dan multi entity
    01/12/2020 [DITA/PHT] Bug saat filterbygroup di SetCcbAcNo
    06/01/2021 [HAR/PHT] tambahan filter profitcenter berdasarkan costcenter di journal
    25/01/2021 [TKG/PHT] ubah SetCcbEntCode divalidasi berdasarkan cost center group user
    04/02/2021 [TKG/PHT] ubah proses mengambil profit center
    04/02/2021 [HAR/PHT] BUG query dan order filter profitcenter
    07/03/2021 [TKG/PHT] profit center divalidasi berdasarkan parent juga.
    16/03/2021 [TKG/PHT] divalidasi berdasarkan group profit center.
    12/04/2021 [TKG/PHT] filter multi profit center diurutkan berdasarkan kodenya.
    30/06/2021 [VIN/IMS] bug filter COA 
    04/07/2021 [TKG/PHT] tambahan filter period dan informasi period
    21/09/2021 [TKG/ALL] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
    01/10/2021 [RDA/AMKA] bug filter cost center belum bisa diakses
    15/02/2022 [TKG/PHT] merubah GetParameter()
    06/04/2022 [IBL/PRODUCT] penyesuaian di ShowCurrencyRate(), sbg penyesuaian perubahan journal recvvd - auto do dari hdr ke dtl
    20/04/2022 [RIS/VIR] Menambah field coa digit dengan parameter IsGeneralLedgerUseCOADigit
    18/05/2022 [BRI/VIR] perbaiki posisi filter
    11/11/2022 [HPH/PHT] Merubah source code dan menambah checklist yang dapat memvalidasi munculnya jurnal dari cost center child saja
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptGL : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty, 
            mMainCurCode = string.Empty;
        private bool
            mIsEntityMandatory = false,
            mIsReportingFilterByEntity = false,
            mIsGLShowDataCustomerVendor = false,
            mIsGLShowDataCurrencyRate = false,
            mIsRptGLInclOpeningBalance = false,
            mIsCOAFilteredByGroup = false,
            mMenuCodeForRptGLEqualize = false,
            mIsRptGLShowVoucherDesc = false,
            mIsRptGLBtnPrintInvisible = false,
            mIsGLShowDataProfitCenter = false,
            mIsRptGLUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsGeneralLedgerUseCOADigit = false
            //mIsReportingFilterByEntity = false
            ;

        #endregion

        #region Constructor

        public FrmRptGL(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                
                GetParameter();
                if (!mMenuCodeForRptGLEqualize) ChkEqualizeJournal.Visible = false;
                if (mIsRptGLBtnPrintInvisible) BtnPrint.Visible = false;
                ChkCCParentJournal.Checked = true;
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                SetCcbAcNo(ref CcbAcNo2);
                SetCcbEntCode(ref CcbEntCode);
                if (mIsRptGLUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                if(!mIsGeneralLedgerUseCOADigit)
                {
                    panel5.Hide();
                    label7.Top = TxtAlias.Top = ChkAlias.Top -= 20;
                    label4.Top = TxtAcDesc.Top = ChkAcDesc.Top -= 20;
                    label5.Top = CcbAcNo2.Top = ChkAcNo2.Top -= 20;
                    LblMultiProfitCenterCode.Top = CcbProfitCenterCode.Top = ChkProfitCenterCode.Top -= 20;
                    panel2.Height -= 20;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        //private string GetSQL(string Filter, string Filter2)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select DocType, DocNo, DocDt, JnDesc, Amt, Parent, ParentDesc,  ");
        //    SQL.AppendLine("AcNo, Alias, AcDesc, DAmt, CAmt, AcType, ");
        //    SQL.AppendLine("MenuCode, MenuDesc, RemarkH, RemarkD, JournalVoucherInd, ");
        //    SQL.AppendLine("VcDocNo, VRDocNO, GiroNo, Openingdt, DueDt, Dno, ProjectCode, ProjectCode2, VdName, CtName, EntName, ");
        //    if(mIsGLShowDataProfitCenter)
        //        SQL.AppendLine("ProfitCenterName");
        //    else
        //        SQL.AppendLine("Null As ProfitCenterName");
        //    SQL.AppendLine("From ( ");
        //    if (mIsRptGLInclOpeningBalance)
        //    {
        //        SQL.AppendLine("Select '1' As DocType, A.DocNo, A.DocDt, Concat('COA Opening Balance ', Yr) As JnDesc, ");
        //        SQL.AppendLine("IfNull(B.Amt, 0.00) As Amt, ");
        //        SQL.AppendLine("C.Parent, D.AcDesc As ParentDesc,  ");
        //        SQL.AppendLine("B.AcNo, C.Alias, C.AcDesc, ");
        //        SQL.AppendLine("Case When C.AcType='D' Then IfNull(B.Amt, 0.00) Else 0.00 End As DAmt, ");
        //        SQL.AppendLine("Case When C.AcType='C' Then IfNull(B.Amt, 0.00) Else 0.00 End As CAmt, ");
        //        SQL.AppendLine("C.AcType, ");
        //        SQL.AppendLine("E.MenuCode, E.MenuDesc, A.Remark As RemarkH, Null As RemarkD, ");
        //        SQL.AppendLine("'N' As JournalVoucherInd, ");
        //        SQL.AppendLine("Null As VcDocNo, Null As VRDocNO, Null As GiroNo, Null As Openingdt, Null As DueDt, Null As Dno, Null As ProjectCode, Null As ProjectCode2, ");
        //        SQL.AppendLine("Null As VdName, Null As CtName, F.EntName, ");
        //        if (mIsGLShowDataProfitCenter)
        //            SQL.AppendLine("L.ProfitCenterName");
        //        else
        //            SQL.AppendLine("null As ProfitCenterName");
        //        SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
        //        SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo=B.DocNo ");
        //        SQL.AppendLine("    And B.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null)  ");
        //        if (mIsCOAFilteredByGroup)
        //        {
        //            SQL.AppendLine("    And Exists ");
        //            SQL.AppendLine("    ( ");
        //            SQL.AppendLine("        Select 1 ");
        //            SQL.AppendLine("        From TblGroupCOA ");
        //            SQL.AppendLine("        Where GrpCode In ");
        //            SQL.AppendLine("        ( ");
        //            SQL.AppendLine("            Select GrpCode ");
        //            SQL.AppendLine("            From TblUser ");
        //            SQL.AppendLine("            Where UserCode = @UserCode ");
        //            SQL.AppendLine("        ) ");
        //            //SQL.AppendLine("        And AcNo = B.AcNo ");
        //            SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
        //            SQL.AppendLine("    ) ");
        //        }
        //        if (ChkAcNo2.Checked)
        //            SQL.AppendLine("And (B.AcNo Is Not Null And Find_In_Set(B.AcNo, @MultiAcNo)) ");
        //        SQL.AppendLine("Left Join TblCoa C On B.AcNo=C.AcNo ");
        //        SQL.AppendLine("Left Join TblCoa D On C.Parent=D.AcNo ");
        //        SQL.AppendLine("Left Join (Select MenuCode, MenuDesc From TblMenu Where Param='FrmCOAOpeningBalance' Limit 1) E On 1=1 ");
        //        SQL.AppendLine("Left Join TblEntity F On A.EntCode=F.EntCode ");
               
        //        if (mIsGLShowDataProfitCenter)
        //        {
        //            SQL.AppendLine("Left Join TblProfitCenter L On A.ProfitCenterCode=L.ProfitCenterCode ");
        //            //SQL.AppendLine("LEFT JOIN ( ");
        //            //SQL.AppendLine("    SELECT S1.CCCode, S2.ProfitCenterCode, S2.ProfitCenterName  ");
        //            //SQL.AppendLine("    From TblCostcenter S1 ");
        //            //SQL.AppendLine("    INNER JOIN TblProfitCenter S2 ON S1.ProfitCenterCode = S2.ProfitCenterCode ");
        //            //SQL.AppendLine(")L ON A.CCCode = L.CCCode ");
        //            //if (ChkProfitCenterCode.Checked)
        //            //    SQL.AppendLine("And (L.ProfitCenterCode Is Not Null And Find_In_Set(L.ProfitCenterCode, @ProfitCenterCode)) ");
        //        }
        //        SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
        //        SQL.AppendLine("And A.CancelInd='N' ");
        //        SQL.AppendLine(Filter2);
        //        if (mIsReportingFilterByEntity)
        //        {
        //            SQL.AppendLine("And (A.EntCode Is Null Or (A.EntCode Is Not Null ");
        //            SQL.AppendLine("And Exists( ");
        //            SQL.AppendLine("    Select 1 From TblGroupEntity ");
        //            SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
        //            SQL.AppendLine("    And GrpCode In ( ");
        //            SQL.AppendLine("        Select GrpCode From TblUser ");
        //            SQL.AppendLine("        Where UserCode=@UserCode ");
        //            SQL.AppendLine("    ) ");
        //            SQL.AppendLine("))) ");
        //        }
        //        if (mIsRptGLUseProfitCenter)
        //        {
        //            SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");
        //            if (ChkProfitCenterCode.Checked)
        //                SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
        //            else
        //            {
        //                SQL.AppendLine("    And A.ProfitCenterCode In ( ");
        //                SQL.AppendLine("        Select ProfitCenterCode ");
        //                SQL.AppendLine("        From TblCostCenter ");
        //                SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
        //                SQL.AppendLine("        And ActInd='Y' ");
        //                SQL.AppendLine("        And CCCode In ( ");
        //                SQL.AppendLine("            Select Distinct CCCode From TblGroupCostCenter T ");
        //                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
        //                SQL.AppendLine("        ) ");
        //                SQL.AppendLine("    ) ");
        //            }
        //        }
        //        if (ChkEntCode.Checked)
        //            SQL.AppendLine("And (A.EntCode Is Not Null And Find_In_Set(A.EntCode, @EntCode)) ");

        //        SQL.AppendLine("Union All ");
        //    }

        //    SQL.AppendLine("Select Distinct '2' As DocType, A.DocNo, A.DocDt, A.JnDesc, ");
        //    SQL.AppendLine("IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt, ");
        //    SQL.AppendLine("C.Parent, D.AcDesc As ParentDesc,  ");
        //    SQL.AppendLine("B.AcNo, C.Alias, C.AcDesc, B.DAmt, B.CAmt, C.AcType, ");
        //    SQL.AppendLine("A.MenuCode, A.MenuDesc, A.Remark As RemarkH, B.Remark As RemarkD, ");
        //    SQL.AppendLine("Case When A.MenuDesc Like '%Journal Voucher%' Then 'Y' Else 'N' End As JournalVoucherInd, ");
        //    SQL.AppendLine("E.VcDocNo, E.VRDocNO, E.GiroNo, E.Openingdt, E.DueDt, B.Dno, J.ProjectCode, J.ProjectCode2, ");
        //    if (mIsGLShowDataCustomerVendor)
        //        SQL.AppendLine("G.VdName, I.CtName, ");
        //    else
        //        SQL.AppendLine("Null As VdName, Null As CtName, ");
        //    SQL.AppendLine("K.EntName, ");
        //    if (mIsGLShowDataProfitCenter)
        //        SQL.AppendLine("M.ProfitCenterName");
        //    else
        //        SQL.AppendLine("Null As ProfitCenterName");
        //    SQL.AppendLine("From TblJournalHdr A ");
        //    SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
        //    if (mIsCOAFilteredByGroup)
        //    {
        //        SQL.AppendLine("    And Exists ");
        //        SQL.AppendLine("    ( ");
        //        SQL.AppendLine("        Select 1 ");
        //        SQL.AppendLine("        From TblGroupCOA ");
        //        SQL.AppendLine("        Where GrpCode In ");
        //        SQL.AppendLine("        ( ");
        //        SQL.AppendLine("            Select GrpCode ");
        //        SQL.AppendLine("            From TblUser ");
        //        SQL.AppendLine("            Where UserCode = @UserCode ");
        //        SQL.AppendLine("        ) ");
        //        //SQL.AppendLine("        And AcNo = B.AcNo ");
        //        SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
        //        SQL.AppendLine("    ) ");
        //    }
        //    if (mIsReportingFilterByEntity)
        //    {
        //        SQL.AppendLine("And (B.EntCode Is Null Or (B.EntCode Is Not Null ");
        //        SQL.AppendLine("And Exists( ");
        //        SQL.AppendLine("    Select 1 From TblGroupEntity ");
        //        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
        //        SQL.AppendLine("    And GrpCode In ( ");
        //        SQL.AppendLine("        Select GrpCode From TblUser ");
        //        SQL.AppendLine("        Where UserCode=@UserCode ");
        //        SQL.AppendLine("    ) ");
        //        SQL.AppendLine("))) ");
        //    }
        //    if (ChkEntCode.Checked) SQL.AppendLine("And (B.EntCode Is Not Null And Find_In_Set(B.EntCode, @EntCode)) ");
        //    if (ChkAcNo2.Checked) SQL.AppendLine("And (B.AcNo Is Not Null And Find_In_Set(B.AcNo, @MultiAcNo)) ");
        //    SQL.AppendLine("Left Join TblCoa C On B.AcNo=C.AcNo ");
        //    SQL.AppendLine("Left Join TblCOA D On C.Parent=D.AcNo ");
        //    SQL.AppendLine("Left Join  ");
        //    SQL.AppendLine("(  ");
        //    SQL.AppendLine("    Select VCDocNo, VRDocno, JournalDocNo, GiroNo, OpeningDt, DueDt ");
        //    SQL.AppendLine("    From (  ");
        //    SQL.AppendLine("        Select DocNo As VCDocNo, VoucherRequestDocNo As VRDocNo, JournalDocNo, GiroNo, OpeningDt, DueDt "); 
        //    SQL.AppendLine("        From TblVoucherHdr  ");
        //    SQL.AppendLine("        Where JournalDocno Is Not Null ");
        //    SQL.AppendLine("        And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
        //    SQL.AppendLine("        Union All  ");
        //    SQL.AppendLine("        Select DocNo As VCDocNo, VoucherRequestDocNo As VRDocNo, JournalDocNo2 As JournalDocNo, GiroNo, OpeningDt, DueDt ");
        //    SQL.AppendLine("        From TblVoucherHdr  ");
        //    SQL.AppendLine("        Where JournalDocNo2 Is Not Null ");
        //    SQL.AppendLine("        And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
        //    SQL.AppendLine("    ) T   ");
        //    SQL.AppendLine(") E On A.DocNo = E.JournalDocNo ");
        //    if (mIsGLShowDataCustomerVendor)
        //    {
        //        SQL.AppendLine("Left Join ( ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, VdCode ");
        //        SQL.AppendLine("    From TblRecvVdHdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.VdCode ");
        //        SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
        //        SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //        SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T2.JournalDocNo2 As JournalDocNo, T1.VdCode ");
        //        SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
        //        SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //        SQL.AppendLine("    And T2.JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And T2.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, VdCode ");
        //        SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, VdCode ");
        //        SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
        //        SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo, T2.VdCode ");
        //        SQL.AppendLine("    From TblVoucherHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T2.VdCode ");
        //        SQL.AppendLine("    From TblVoucherHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo, T3.VdCode ");
        //        SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
        //        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T3.VdCode ");
        //        SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
        //        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine(") F On A.DocNo=F.JournalDocNo ");
        //        SQL.AppendLine("Left Join TblVendor G On F.VdCode=G.VdCode ");

        //        SQL.AppendLine("Left Join ( ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblDOCtHdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
        //        SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
        //        SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //        SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblDOCt4Hdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
        //        SQL.AppendLine("    From TblDOCt4Hdr T1, TblDOCt4Dtl T2 ");
        //        SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //        SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblDOCt2Hdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
        //        SQL.AppendLine("    From TblDOCt2Hdr T1, TblDOCt2Dtl T2 ");
        //        SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //        SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblSalesInvoiceHdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblSalesInvoiceHdr ");
        //        SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblSalesInvoice2Hdr ");
        //        SQL.AppendLine("    Where JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, CtCode ");
        //        SQL.AppendLine("    From TblSalesInvoice2Hdr ");
        //        SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo, T2.CtCode ");
        //        SQL.AppendLine("    From TblVoucherHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T2.CtCode ");
        //        SQL.AppendLine("    From TblVoucherHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo, T3.CtCode ");
        //        SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
        //        SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine("    Union All ");
        //        SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T3.CtCode ");
        //        SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
        //        SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
        //        SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
        //        SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

        //        SQL.AppendLine(") H On A.DocNo=H.JournalDocNo ");
        //        SQL.AppendLine("Left Join TblCustomer I On H.CtCode=I.CtCode ");
        //    }
        //    SQL.AppendLine("Left Join TblSOContractHdr J On J.DocNo=C.SOCDocNo ");
        //    SQL.AppendLine("Left Join TblEntity K On B.EntCode=K.EntCode ");
        //    if (mIsGLShowDataProfitCenter)
        //    {
        //        SQL.AppendLine("Left Join TblCostCenter L On A.CCCode=L.CCCode ");
        //        SQL.AppendLine("Left Join TblProfitCenter M On L.ProfitCenterCode=M.ProfitCenterCode ");

        //        //if (ChkProfitCenterCode.Checked)
        //        //    SQL.AppendLine("INner JOIN ( ");
        //        //else
        //        //    SQL.AppendLine("Left JOIN ( ");
        //        //SQL.AppendLine("    SELECT S1.CCCode, S2.ProfitCenterCode, S2.ProfitCenterName  ");
        //        //SQL.AppendLine("    From TblCostcenter S1 ");
        //        //SQL.AppendLine("    INNER JOIN TblProfitCenter S2 ON S1.ProfitCenterCode = S2.ProfitCenterCode ");
        //        //SQL.AppendLine(")L ON A.CCCode = L.CCCode ");
        //    }
        //    SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
        //    if (mIsRptGLUseProfitCenter)
        //    {
        //        if (ChkProfitCenterCode.Checked)
        //        {
        //            SQL.AppendLine("    And A.CCCode Is Not Null ");
        //            SQL.AppendLine("    And A.CCCode In ( ");
        //            SQL.AppendLine("        Select Distinct CCCode ");
        //            SQL.AppendLine("        From TblCostCenter ");
        //            SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
        //            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
        //            SQL.AppendLine("    ) ");
        //        }
        //        else
        //        {
        //            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
        //            SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter ");
        //            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
        //            SQL.AppendLine("        ))) ");
        //        }
        //    }
        //    SQL.AppendLine(Filter);
        //    SQL.AppendLine(") Tbl ");
        //    SQL.AppendLine("Order By Tbl.DocDt, Tbl.DocType, Tbl.DocNo, Tbl.AcNo  ");
        //    SQL.AppendLine("; ");

        //    return SQL.ToString();
        //}

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Date",
                        "Document#", 
                        "Description",
                        "Amount",
                        "Parent#",
                        
                        //6-10
                        "Parent Description",
                        "Account#",
                        "Account Description",
                        "Debit",
                        "Credit",                       
                        
                        //11-15
                        "Type",
                        "Balance",
                        "Test",
                        "Menu Code",
                        "Menu Description",
                        
                        //16-18
                        "Voucher Request",
                        "Voucher",
                        "Giro", 
                        "Opening Date",
                        "Due Date",

                        //21-25
                        "Customer",
                        "Vendor",
                        "Remark Header",
                        "Remark Detail",
                        "Dno",

                        //26-30
                        "Currency",
                        "Rate",
                        "RateDt",
                        "Alias",
                        "Description",

                        //31-33
                        "Entity",
                        "Profit Center",
                        "Period"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        100, 150, 300, 100, 150, 
                        //6-10
                        300, 150, 350, 150, 150, 
                        //11-15
                        40, 150, 150, 100, 250, 
                        //16-20
                        120, 120, 120, 100, 100, 
                        //21-25
                        200, 200, 300, 300, 20,  
                        //26-30
                        100, 120, 120, 120, 120,
                        //31-33
                        200, 200, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 9, 10, 12, 27}, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1, 19, 20, 28 });
            if (mIsGLShowDataCustomerVendor)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 11, 13, 14, 16, 17, 18, 19, 20 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4, 11, 13, 14, 16, 17, 18, 19, 20, 21, 22}, false);
            }
            if (mIsGLShowDataCurrencyRate)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 25, 28 }, false);
                Grd1.Cols[26].Move(4);
                Grd1.Cols[27].Move(5);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 25, 26, 27, 28 }, false);
            }
            if (!mIsRptGLShowVoucherDesc)
                Sm.GrdColInvisible(Grd1, new int[] { 30 }, false);

            Grd1.Cols[29].Move(8);
            Grd1.Cols[31].Move(16);
            Grd1.Cols[33].Move(3);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            if (mIsGLShowDataCustomerVendor)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                ValidateCOA()
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ", Filter2 = " ", Filter3 = " ";
                var cm = new MySqlCommand();

                SetProfitCenter();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if(mIsGeneralLedgerUseCOADigit)
                {
                    Sm.CmParam<String>(ref cm, "@acno1", "%"+TxtAcNo1.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno2", "%"+TxtAcNo2.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno3", "%"+TxtAcNo3.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno4", "%"+TxtAcNo4.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno5", "%"+TxtAcNo5.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno6", "%"+TxtAcNo6.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno7", "%"+TxtAcNo7.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno8", "%"+TxtAcNo8.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno9", "%"+TxtAcNo9.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno10", "%"+TxtAcNo10.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno11", "%"+TxtAcNo11.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno12", "%"+TxtAcNo12.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno13", "%"+TxtAcNo13.Text+"%");
                    Sm.CmParam<String>(ref cm, "@acno14", "%"+TxtAcNo14.Text+"%");
                }
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "AcDesc", false);
                if (ChkAcNo2.Checked) Sm.CmParam<String>(ref cm, "@MultiAcNo", GetCcbAcNo());
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", GetCcbEntCode()); 
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 
                if (ChkAlias.Checked) Filter += " And Alias Like '" + TxtAlias.Text.Replace("'", "") + "%' ";
                if (ChkAcNo.Checked)
                {
                    Filter += " And (AcNo Like '" + TxtAcNo.Text.Replace("'", "") + "%' Or ProjectCode Like '" + TxtAcNo.Text.Replace("'", "") + "%' Or ProjectCode2 Like '" + TxtAcNo.Text.Replace("'", "") + "%' ) ";
                    Filter2 += " And (B.AcNo Like '" + TxtAcNo.Text.Replace("'", "") + "%' ) ";
                }
                if(mIsGeneralLedgerUseCOADigit)
                {
                    Filter3 += " AND SUBSTRING(AcNo, 1,1) LIKE @acno1 " +
                        "AND SUBSTRING(AcNo, 3,1) LIKE @acno2 " +
                        "AND SUBSTRING(AcNo, 5,1) LIKE @acno3 " +
                        "AND SUBSTRING(AcNo, 7,1) like @acno4 " +
                        "AND SUBSTRING(AcNo, 9,1) LIKE @acno5 " +
                        "AND SUBSTRING(AcNo, 11,1) LIKE @acno6 " +
                        "AND SUBSTRING(AcNo, 13,1) LIKE @acno7 " +
                        "AND SUBSTRING(AcNo, 15,1) LIKE @acno8 AND SUBSTRING(AcNo, 16,1) LIKE @acno9 AND SUBSTRING(AcNo, 17,1) LIKE @acno10 " +
                        "AND SUBSTRING(AcNo, 19,1) LIKE @acno11 AND SUBSTRING(AcNo, 20,1) LIKE @acno12 AND SUBSTRING(AcNo, 21,1) LIKE @acno13 AND SUBSTRING(AcNo, 22,1) LIKE @acno14 ";
                }

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocType, DocNo, DocDt, Period, JnDesc, Amt, Parent, ParentDesc,  ");
                SQL.AppendLine("AcNo, Alias, AcDesc, DAmt, CAmt, AcType, ");
                SQL.AppendLine("MenuCode, MenuDesc, RemarkH, RemarkD, JournalVoucherInd, ");
                SQL.AppendLine("VcDocNo, VRDocNO, GiroNo, Openingdt, DueDt, Dno, ProjectCode, ProjectCode2, VdName, CtName, EntName, ");
                if (mIsGLShowDataProfitCenter)
                    SQL.AppendLine("ProfitCenterName");
                else
                    SQL.AppendLine("Null As ProfitCenterName");
                SQL.AppendLine("From ( ");
                if (mIsRptGLInclOpeningBalance)
                {
                    SQL.AppendLine("Select '1' As DocType, A.DocNo, A.DocDt, Null As Period, Concat('COA Opening Balance ', Yr) As JnDesc, ");
                    SQL.AppendLine("IfNull(B.Amt, 0.00) As Amt, ");
                    SQL.AppendLine("C.Parent, D.AcDesc As ParentDesc,  ");
                    SQL.AppendLine("B.AcNo, C.Alias, C.AcDesc, ");
                    SQL.AppendLine("Case When C.AcType='D' Then IfNull(B.Amt, 0.00) Else 0.00 End As DAmt, ");
                    SQL.AppendLine("Case When C.AcType='C' Then IfNull(B.Amt, 0.00) Else 0.00 End As CAmt, ");
                    SQL.AppendLine("C.AcType, ");
                    SQL.AppendLine("E.MenuCode, E.MenuDesc, A.Remark As RemarkH, Null As RemarkD, ");
                    SQL.AppendLine("'N' As JournalVoucherInd, ");
                    SQL.AppendLine("Null As VcDocNo, Null As VRDocNO, Null As GiroNo, Null As Openingdt, Null As DueDt, Null As Dno, Null As ProjectCode, Null As ProjectCode2, ");
                    SQL.AppendLine("Null As VdName, Null As CtName, F.EntName, ");
                    if (mIsGLShowDataProfitCenter)
                        SQL.AppendLine("L.ProfitCenterName");
                    else
                        SQL.AppendLine("null As ProfitCenterName");
                    SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
                    SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("    And B.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null)  ");
                    if (mIsCOAFilteredByGroup)
                    {
                        SQL.AppendLine("    And Exists ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select 1 ");
                        SQL.AppendLine("        From TblGroupCOA ");
                        SQL.AppendLine("        Where GrpCode In ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select GrpCode ");
                        SQL.AppendLine("            From TblUser ");
                        SQL.AppendLine("            Where UserCode = @UserCode ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                        SQL.AppendLine("    ) ");
                    }
                    if (ChkAcNo2.Checked)
                        SQL.AppendLine("And (B.AcNo Is Not Null And Find_In_Set(B.AcNo, @MultiAcNo)) ");
                    SQL.AppendLine("Left Join TblCoa C On B.AcNo=C.AcNo ");
                    SQL.AppendLine("Left Join TblCoa D On C.Parent=D.AcNo ");
                    SQL.AppendLine("Left Join (Select MenuCode, MenuDesc From TblMenu Where Param='FrmCOAOpeningBalance' Limit 1) E On 1=1 ");
                    SQL.AppendLine("Left Join TblEntity F On A.EntCode=F.EntCode ");

                    if (mIsGLShowDataProfitCenter)
                        SQL.AppendLine("Left Join TblProfitCenter L On A.ProfitCenterCode=L.ProfitCenterCode ");
                    SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                    SQL.AppendLine("And A.CancelInd='N' ");
                    SQL.AppendLine(Filter2);
                    if (mIsReportingFilterByEntity)
                    {
                        SQL.AppendLine("And (A.EntCode Is Null Or (A.EntCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                        SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine("))) ");
                    }
                    if (mIsRptGLUseProfitCenter)
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                        if (!mIsAllProfitCenterSelected)
                        {
                            var Filter_2 = string.Empty;
                            int i = 0;
                            foreach (var x in mlProfitCenter.Distinct())
                            {
                                if (Filter_2.Length > 0) Filter_2 += " Or ";
                                Filter_2 += " (A.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                                i++;
                            }
                            if (Filter_2.Length == 0)
                                SQL.AppendLine("    And 1=0 ");
                            else
                                SQL.AppendLine("    And (" + Filter_2 + ") ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode2) ");
                                if (ChkProfitCenterCode.Checked)
                                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);
                            }
                            else
                            {
                                SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("    ) ");
                            }
                        }


                        //SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");
                        //if (ChkProfitCenterCode.Checked)
                        //    SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        //else
                        //{
                        //    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        //    SQL.AppendLine("        Select ProfitCenterCode ");
                        //    SQL.AppendLine("        From TblCostCenter ");
                        //    SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
                        //    SQL.AppendLine("        And ActInd='Y' ");
                        //    SQL.AppendLine("        And CCCode In ( ");
                        //    SQL.AppendLine("            Select Distinct CCCode From TblGroupCostCenter T ");
                        //    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        //    SQL.AppendLine("        ) ");
                        //    SQL.AppendLine("    ) ");
                        //}
                    }
                    if (ChkEntCode.Checked)
                        SQL.AppendLine("And (A.EntCode Is Not Null And Find_In_Set(A.EntCode, @EntCode)) ");

                    SQL.AppendLine("Union All ");
                }

                SQL.AppendLine("Select Distinct '2' As DocType, A.DocNo, A.DocDt, A.Period, A.JnDesc, ");
                SQL.AppendLine("IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt, ");
                SQL.AppendLine("C.Parent, D.AcDesc As ParentDesc,  ");
                SQL.AppendLine("B.AcNo, C.Alias, C.AcDesc, B.DAmt, B.CAmt, C.AcType, ");
                SQL.AppendLine("A.MenuCode, A.MenuDesc, A.Remark As RemarkH, B.Remark As RemarkD, ");
                SQL.AppendLine("Case When A.MenuDesc Like '%Journal Voucher%' Then 'Y' Else 'N' End As JournalVoucherInd, ");
                SQL.AppendLine("E.VcDocNo, E.VRDocNO, E.GiroNo, E.Openingdt, E.DueDt, B.Dno, J.ProjectCode, J.ProjectCode2, ");
                if (mIsGLShowDataCustomerVendor)
                    SQL.AppendLine("G.VdName, I.CtName, ");
                else
                    SQL.AppendLine("Null As VdName, Null As CtName, ");
                SQL.AppendLine("K.EntName, ");
                if (mIsGLShowDataProfitCenter)
                    SQL.AppendLine("M.ProfitCenterName ");
                else
                    SQL.AppendLine("Null As ProfitCenterName ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
                if (mIsCOAFilteredByGroup)
                {
                    SQL.AppendLine("    And Exists ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select 1 ");
                    SQL.AppendLine("        From TblGroupCOA ");
                    SQL.AppendLine("        Where GrpCode In ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select GrpCode ");
                    SQL.AppendLine("            From TblUser ");
                    SQL.AppendLine("            Where UserCode = @UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                    SQL.AppendLine("    ) ");
                }
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And (B.EntCode Is Null Or (B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("))) ");
                }
                if (ChkEntCode.Checked) SQL.AppendLine("And (B.EntCode Is Not Null And Find_In_Set(B.EntCode, @EntCode)) ");
                if (ChkAcNo2.Checked) SQL.AppendLine("And (B.AcNo Is Not Null And Find_In_Set(B.AcNo, @MultiAcNo)) ");
                SQL.AppendLine("Left Join TblCoa C On B.AcNo=C.AcNo ");
                SQL.AppendLine("Left Join TblCOA D On C.Parent=D.AcNo ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select VCDocNo, VRDocno, JournalDocNo, GiroNo, OpeningDt, DueDt ");
                SQL.AppendLine("    From (  ");
                SQL.AppendLine("        Select DocNo As VCDocNo, VoucherRequestDocNo As VRDocNo, JournalDocNo, GiroNo, OpeningDt, DueDt ");
                SQL.AppendLine("        From TblVoucherHdr  ");
                SQL.AppendLine("        Where JournalDocno Is Not Null ");
                SQL.AppendLine("        And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
                SQL.AppendLine("        Union All  ");
                SQL.AppendLine("        Select DocNo As VCDocNo, VoucherRequestDocNo As VRDocNo, JournalDocNo2 As JournalDocNo, GiroNo, OpeningDt, DueDt ");
                SQL.AppendLine("        From TblVoucherHdr  ");
                SQL.AppendLine("        Where JournalDocNo2 Is Not Null ");
                SQL.AppendLine("        And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
                SQL.AppendLine("    ) T   ");
                SQL.AppendLine(") E On A.DocNo = E.JournalDocNo ");

                #region tambahan informasi

                if (mIsGLShowDataCustomerVendor)
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, VdCode ");
                    SQL.AppendLine("    From TblRecvVdHdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.VdCode ");
                    SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T2.JournalDocNo2 As JournalDocNo, T1.VdCode ");
                    SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T2.JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And T2.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, VdCode ");
                    SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, VdCode ");
                    SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                    SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo, T2.VdCode ");
                    SQL.AppendLine("    From TblVoucherHdr T1 ");
                    SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T2.VdCode ");
                    SQL.AppendLine("    From TblVoucherHdr T1 ");
                    SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo, T3.VdCode ");
                    SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                    SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                    SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T3.VdCode ");
                    SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                    SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                    SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine(") F On A.DocNo=F.JournalDocNo ");
                    SQL.AppendLine("Left Join TblVendor G On F.VdCode=G.VdCode ");

                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblDOCtHdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
                    SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblDOCt4Hdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
                    SQL.AppendLine("    From TblDOCt4Hdr T1, TblDOCt4Dtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblDOCt2Hdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
                    SQL.AppendLine("    From TblDOCt2Hdr T1, TblDOCt2Dtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblSalesInvoiceHdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblSalesInvoiceHdr ");
                    SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblSalesInvoice2Hdr ");
                    SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, CtCode ");
                    SQL.AppendLine("    From TblSalesInvoice2Hdr ");
                    SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo, T2.CtCode ");
                    SQL.AppendLine("    From TblVoucherHdr T1 ");
                    SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T2.CtCode ");
                    SQL.AppendLine("    From TblVoucherHdr T1 ");
                    SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo, T3.CtCode ");
                    SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                    SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                    SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T3.CtCode ");
                    SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                    SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                    SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                    SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                    SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                    SQL.AppendLine(") H On A.DocNo=H.JournalDocNo ");
                    SQL.AppendLine("Left Join TblCustomer I On H.CtCode=I.CtCode ");
                }

                #endregion

                SQL.AppendLine("Left Join TblSOContractHdr J On J.DocNo=C.SOCDocNo ");
                SQL.AppendLine("Left Join TblEntity K On B.EntCode=K.EntCode ");
                SQL.AppendLine("Left Join TblCostCenter L On A.CCCode=L.CCCode ");                
                if (mIsGLShowDataProfitCenter)
                    SQL.AppendLine("Left Join TblProfitCenter M On L.ProfitCenterCode=M.ProfitCenterCode ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                if (!ChkCCParentJournal.Checked) SQL.AppendLine("And L.NotParentInd = 'Y' ");
                if (TxtPeriod.Text.Length > 0)
                {
                    SQL.AppendLine("And A.Period Is Not Null And A.Period Like @Period ");
                    Sm.CmParam<String>(ref cm, "@Period", "%" + TxtPeriod.Text + "%");
                }
                if (mIsRptGLUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_1 = string.Empty;
                        int i = 0;

                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )  ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_1.Length > 0) Filter_1 += " Or ";
                            Filter_1 += " (ProfitCenterCode=@ProfitCenterCode1_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode1_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_1.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter_1 + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode1) ");
                            SQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode1", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                            
                        }
                    }
                    //if (ChkProfitCenterCode.Checked)
                    //{
                    //    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    //    SQL.AppendLine("    And A.CCCode In ( ");
                    //    SQL.AppendLine("        Select Distinct CCCode ");
                    //    SQL.AppendLine("        From TblCostCenter ");
                    //    SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
                    //    SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    //    SQL.AppendLine("    ) ");
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    //    SQL.AppendLine("        Select Distinct CCCode From TblGroupCostCenter ");
                    //    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    //    SQL.AppendLine("        ))) ");
                    //}
                }

                #region Journal Memorial

                if (mIsRptAccountingShowJournalMemorial)
                {
                    SQL.AppendLine("Union All ");

                    SQL.AppendLine("Select Distinct '2' As DocType, A.DocNo, A.DocDt, A.Period, A.JnDesc, ");
                    SQL.AppendLine("IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt, ");
                    SQL.AppendLine("C.Parent, D.AcDesc As ParentDesc,  ");
                    SQL.AppendLine("B.AcNo, C.Alias, C.AcDesc, B.DAmt, B.CAmt, C.AcType, ");
                    SQL.AppendLine("A.MenuCode, A.MenuDesc, A.Remark As RemarkH, B.Remark As RemarkD, ");
                    SQL.AppendLine("'N' As JournalVoucherInd, ");
                    SQL.AppendLine("Null As VcDocNo, Null As VRDocNO, Null As GiroNo, Null As Openingdt, Null As DueDt, B.Dno, Null As ProjectCode, Null As ProjectCode2, ");
                    SQL.AppendLine("Null As VdName, Null As CtName, ");
                    SQL.AppendLine("K.EntName, ");
                    if (mIsGLShowDataProfitCenter)
                        SQL.AppendLine("M.ProfitCenterName ");
                    else
                        SQL.AppendLine("Null As ProfitCenterName ");
                    SQL.AppendLine("From TblJournalMemorialHdr A ");
                    SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo=B.DocNo ");
                    if (mIsCOAFilteredByGroup)
                    {
                        SQL.AppendLine("    And Exists ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select 1 ");
                        SQL.AppendLine("        From TblGroupCOA ");
                        SQL.AppendLine("        Where GrpCode In ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select GrpCode ");
                        SQL.AppendLine("            From TblUser ");
                        SQL.AppendLine("            Where UserCode = @UserCode ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                        SQL.AppendLine("    ) ");
                    }
                    if (mIsReportingFilterByEntity)
                    {
                        SQL.AppendLine("And (B.EntCode Is Null Or (B.EntCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine("))) ");
                    }
                    if (ChkEntCode.Checked) SQL.AppendLine("And (B.EntCode Is Not Null And Find_In_Set(B.EntCode, @EntCode)) ");
                    if (ChkAcNo2.Checked) SQL.AppendLine("And (B.AcNo Is Not Null And Find_In_Set(B.AcNo, @MultiAcNo)) ");
                    SQL.AppendLine("Left Join TblCoa C On B.AcNo=C.AcNo ");
                    SQL.AppendLine("Left Join TblCOA D On C.Parent=D.AcNo ");
                    SQL.AppendLine("Left Join TblEntity K On B.EntCode=K.EntCode ");
                    if (mIsGLShowDataProfitCenter)
                    {
                        SQL.AppendLine("Left Join TblCostCenter L On A.CCCode=L.CCCode ");
                        SQL.AppendLine("Left Join TblProfitCenter M On L.ProfitCenterCode=M.ProfitCenterCode ");
                    }
                    SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                    SQL.AppendLine("And A.Status='O' And A.CancelInd='N' ");
                    if (TxtPeriod.Text.Length > 0)
                    {
                        SQL.AppendLine("And A.Period Is Not Null And A.Period Like @Period ");
                        Sm.CmParam<String>(ref cm, "@Period", "%" + TxtPeriod.Text + "%");
                    }
                    if (mIsRptGLUseProfitCenter)
                    {
                        if (!mIsAllProfitCenterSelected)
                        {
                            var Filter_1 = string.Empty;
                            int i = 0;

                            SQL.AppendLine("    And A.CCCode Is Not Null ");
                            SQL.AppendLine("    And A.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In ( ");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        ) ");
                            foreach (var x in mlProfitCenter.Distinct())
                            {
                                if (Filter_1.Length > 0) Filter_1 += " Or ";
                                Filter_1 += " (ProfitCenterCode=@ProfitCenterCode1_" + i.ToString() + ") ";
                                //Sm.CmParam<String>(ref cm, "@ProfitCenterCode1_" + i.ToString(), x);
                                i++;
                            }
                            if (Filter_1.Length == 0)
                                SQL.AppendLine("    And 1=0 ");
                            else
                                SQL.AppendLine("    And (" + Filter_1 + ") ");
                            SQL.AppendLine("    ) ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And A.CCCode Is Not Null ");
                                SQL.AppendLine("    And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode1) ");
                                SQL.AppendLine("    ) ");
                                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode1", GetCcbProfitCenterCode());
                            }
                            else
                            {
                                SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                                SQL.AppendLine("        Select Distinct CCCode ");
                                SQL.AppendLine("        From TblCostCenter ");
                                SQL.AppendLine("        Where ActInd='Y' ");
                                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                                SQL.AppendLine("        And ProfitCenterCode In (");
                                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("        )))) ");
                            }
                        }
                    }
                }
                #endregion

                SQL.AppendLine(") Tbl ");
                SQL.AppendLine("Where 1=1 ");
                if(mIsGeneralLedgerUseCOADigit)
                    SQL.AppendLine(Filter3);
                SQL.AppendLine(Filter);

                //SQL.AppendLine("Where AcDesc Like '%"+TxtAcDesc.Text+"%' ");
                SQL.AppendLine("Order By Tbl.DocDt, Tbl.DocType, Tbl.DocNo, Tbl.AcNo  ");
                SQL.AppendLine("; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "DocDt",

                        //1-5
                        "DocNo", "JnDesc", "Amt", "Parent", "ParentDesc", 

                        //6-10
                        "AcNo", "AcDesc", "DAmt", "CAmt", "AcType", 

                        //11-15
                        "MenuCode", "MenuDesc", "JournalVoucherInd", "RemarkH", "RemarkD",

                        //16-20
                        "VcDocNo", "VRDocNo", "GiroNo", "Openingdt", "DueDt",

                        //21-25
                        "VdName", "CtName", "Dno", "Alias", "EntName", 

                        //26-27
                        "ProfitCenterName", "Period"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                            Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                        else
                            Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 10) - Sm.GetGrdDec(Grd1, Row, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 19);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 27);
                    }, true, false, false, false
                );

                if (mIsRptGLShowVoucherDesc)
                {
                    ShowVoucherRemark();
                }

                if (mMenuCodeForRptGLEqualize)
                {
                    if (Grd1.Rows.Count > 0 && ChkEqualizeJournal.Checked &&
                        (
                            TxtAcNo.Text.Length > 0 ||
                            TxtAcDesc.Text.Length > 0 ||
                            TxtAlias.Text.Length > 0 ||
                            Sm.GetCcb(CcbAcNo2).Length > 0
                        ))
                    {
                        ShowEqualizeJournal(Filter);
                    }
                }

                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 12 });
                Grd1.EndUpdate();

                if (mIsGLShowDataCurrencyRate) ShowCurrencyRate();
             }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Bal()
        {
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
            {
                if (Grd1.Rows[row].Type == iGRowType.Normal)
                    Grd1.Cells[row, 12].ForeColor = Color.Transparent;
            }
        }


        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptGLUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetCcbEntCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col From (");
            SQL.AppendLine("    Select EntName AS Col ");
            SQL.AppendLine("    From TblEntity ");
            SQL.AppendLine("    Where EntCode In ( ");
            SQL.AppendLine("        Select Distinct EntCode From TblGroupEntity T ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Order By Col ");
            SQL.AppendLine(") Tbl; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbEntCode()
        {
            var Value = Sm.GetCcb(CcbEntCode);
            if (Value.Length == 0) return string.Empty;
            return GetEntCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbEntCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetEntCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetEntCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select EntCode As Code From TblEntity Where Find_In_Set(EntName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetCcbAcNo(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(T.AcNo, ' [', T.AcDesc, ']') As Col ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In (");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        And AcNo Like Concat(T.AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By Concat(T.AcNo, ' [', T.AcDesc, ']');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbAcNo()
        {
            string Value = string.Empty;

            Value = Sm.GetCcb(CcbAcNo2);
            if (Value.Length > 0)
            {
                Value = GetAcNo(Value);
                Value = Value.Replace(", ", ",");
            }

            return Value;
        }

        private string GetAcNo(string Value)
        {
            if (Value.Length != 0)
            {
                string initValue = Value;

                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct T.AcNo Separator ',') EntCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select AcNo From TblCOA ");
                SQL.AppendLine("    Where Find_In_Set(Concat(AcNo, ' [', AcDesc, ']'), @Param) ");
                SQL.AppendLine(") T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
                if (initValue.Contains("Consolidate"))
                    Value = string.Concat("Consolidate, ", Value);
            }

            return Value;
        }

        private void ShowVoucherRemark()
        {
            var l = new List<JournalTypeVoucher>();
            string mDocNo = string.Empty;
            string Data = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mDocNo.Length > 0) mDocNo += ",";
                    mDocNo += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            if (mDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.DocNo, B.remark From TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblVoucherHdr B ON A.DocNo = B.JournalDocNo ");
                SQL.AppendLine("Where Find_In_Set(A.DocNo,'" + mDocNo + "') ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.DocNo, B.remark From TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblVoucherHdr B ON A.DocNo = B.JournalDocNo2 ");
                SQL.AppendLine("Where Find_In_Set(A.DocNo,'" + mDocNo + "') ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "DocNo",
                         "Remark"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new JournalTypeVoucher()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                Remark = Sm.DrStr(dr, c[1]),
                            });
                        }
                    }
                    dr.Close();
                }

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                        {
                            foreach (var x in l.Where(w => w.DocNo == Sm.GetGrdStr(Grd1, i, 2)))
                            {
                                Grd1.Cells[i, 30].Value = x.Remark;
                                break;
                            }
                        }
                    }
                }
            }

            l.Clear();
        }

        private void ShowEqualizeJournal(string Filter)
        {
            string AcNo = string.Empty;
            string DocNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (AcNo.Length > 0) AcNo += ",";
                    AcNo += Sm.GetGrdStr(Grd1, i, 7);
                }
            }

            if (AcNo.Length > 0)
            {
                var SQL1 = new StringBuilder();
                string DocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
                string DocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);

                SQL1.AppendLine("Select Group_Concat(Distinct A.DocNo) DocNo ");
                SQL1.AppendLine("From TblJournalDtl A ");
                SQL1.AppendLine("Inner Join TblJournalHdr B On A.DocNo = B.DocNo ");
                SQL1.AppendLine("    And B.DocDt Between @Param1 And @Param2 ");
                SQL1.AppendLine("    And B.CancelInd = 'N' ");
                SQL1.AppendLine("    And Find_In_set(A.AcNo, @Param3); ");

                DocNo = Sm.GetValue(SQL1.ToString(), DocDt1, DocDt2, AcNo);

                if (DocNo.Length > 0)
                {
                    ShowOtherData(DocNo, AcNo, Filter);
                }
            }
        }

        private void ShowOtherData(string DocNo, string AcNo, string Filter)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int Row = Grd1.Rows.Count;

            #region query

            SQL.AppendLine("Select Distinct '2' As DocType, A.DocNo, A.DocDt, A.JnDesc, ");
            SQL.AppendLine("IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt, ");
            SQL.AppendLine("C.Parent, D.AcDesc As ParentDesc,  ");
            SQL.AppendLine("B.AcNo, C.Alias, C.AcDesc, B.DAmt, B.CAmt, C.AcType, ");
            SQL.AppendLine("A.MenuCode, A.MenuDesc, A.Remark As RemarkH, B.Remark As RemarkD, ");
            SQL.AppendLine("Case When A.MenuDesc Like '%Journal Voucher%' Then 'Y' Else 'N' End As JournalVoucherInd, ");
            SQL.AppendLine("E.VcDocNo, E.VRDocNo, E.GiroNo, E.Openingdt, E.DueDt, B.Dno, J.ProjectCode, J.ProjectCode2, ");
            if (mIsGLShowDataCustomerVendor)
                SQL.AppendLine("G.VdName, I.CtName ");
            else
                SQL.AppendLine("Null As VdName, Null As CtName ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Find_In_set(A.DocNo, @DocNo) ");
            SQL.AppendLine("    And Not Find_In_set(B.AcNo, @AcNo) ");
            if (mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                //SQL.AppendLine("        And AcNo = B.AcNo ");
                SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblCoa C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Left Join TblCOA D On C.Parent=D.AcNo ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select VCDocNo, VRDocno, JournalDocNo, GiroNo, OpeningDt, DueDt ");
            SQL.AppendLine("    From (  ");
            SQL.AppendLine("        Select DocNo As VCDocNo, VoucherRequestDocNo As VRDocNo, JournalDocNo, GiroNo, OpeningDt, DueDt  ");
            SQL.AppendLine("        From TblVoucherHdr  ");
            SQL.AppendLine("        Where JournalDocno Is Not Null ");
            SQL.AppendLine("        And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
            SQL.AppendLine("        Union All  ");
            SQL.AppendLine("        Select DocNo As VCDocNo, VoucherRequestDocNo As VRDocNo, JournalDocNo2 As JournalDocNo, GiroNo, OpeningDt, DueDt ");
            SQL.AppendLine("        From TblVoucherHdr  ");
            SQL.AppendLine("        Where JournalDocNo2 Is Not Null ");
            SQL.AppendLine("        And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
            SQL.AppendLine("    ) T   ");
            SQL.AppendLine(") E On A.DocNo = E.JournalDocNo ");
            if (mIsGLShowDataCustomerVendor)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct JournalDocNo, VdCode ");
                SQL.AppendLine("    From TblRecvVdHdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.VdCode ");
                SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T2.JournalDocNo2 As JournalDocNo, T1.VdCode ");
                SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T2.JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And T2.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo, VdCode ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, VdCode ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo, T2.VdCode ");
                SQL.AppendLine("    From TblVoucherHdr T1 ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T2.VdCode ");
                SQL.AppendLine("    From TblVoucherHdr T1 ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo, T3.VdCode ");
                SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T3.VdCode ");
                SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine(") F On A.DocNo=F.JournalDocNo ");
                SQL.AppendLine("Left Join TblVendor G On F.VdCode=G.VdCode ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblDOCtHdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
                SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblDOCt4Hdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
                SQL.AppendLine("    From TblDOCt4Hdr T1, TblDOCt4Dtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblDOCt2Hdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T2.JournalDocNo, T1.CtCode ");
                SQL.AppendLine("    From TblDOCt2Hdr T1, TblDOCt2Dtl T2 ");
                SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    And T2.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T2.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblSalesInvoiceHdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblSalesInvoiceHdr ");
                SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblSalesInvoice2Hdr ");
                SQL.AppendLine("    Where JournalDocNo Is Not Null ");
                SQL.AppendLine("    And JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct JournalDocNo2 As JournalDocNo, CtCode ");
                SQL.AppendLine("    From TblSalesInvoice2Hdr ");
                SQL.AppendLine("    Where JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo, T2.CtCode ");
                SQL.AppendLine("    From TblVoucherHdr T1 ");
                SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T2.CtCode ");
                SQL.AppendLine("    From TblVoucherHdr T1 ");
                SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo, T3.CtCode ");
                SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.JournalDocNo2 As JournalDocNo, T3.CtCode ");
                SQL.AppendLine("    From TblVoucherJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblIncomingPaymentHdr T3 On T2.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                SQL.AppendLine("    Where T1.JournalDocNo2 Is Not Null ");
                SQL.AppendLine("    And T1.JournalDocNo2 In (Select DocNo From TblJournalHdr Where DocDt Between @DocDt1 And @DocDt2)  ");

                SQL.AppendLine(") H On A.DocNo=H.JournalDocNo ");
                SQL.AppendLine("Left Join TblCustomer I On H.CtCode=I.CtCode ");
            }
            SQL.AppendLine("Left Join TblSOContractHdr J On J.DocNo=C.SOCDocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And (B.EntCode Is Null Or (B.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            
            #endregion

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocDt",

                    //1-5
                    "DocNo", "JnDesc", "Amt", "Parent", "ParentDesc", 

                    //6-10
                    "AcNo", "AcDesc", "DAmt", "CAmt", "AcType", 

                    //11-15
                    "MenuCode", "MenuDesc", "JournalVoucherInd", "RemarkH", "RemarkD",

                    //16-20
                    "VcDocNo", "VRDocNo", "GiroNo", "Openingdt", "DueDt",

                    //21-24
                    "VdName", "CtName", "Dno", "Alias"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();

                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                            Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                        else
                            Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 10) - Sm.GetGrdDec(Grd1, Row, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 16);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 17);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 14);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 15);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 18);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 19);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 20, 20);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 22);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 29, 24);

                        Row += 1;
                    }
                }
                dr.Close();
            }
        }

        private bool ValidateCOA()
        {
            if (TxtAcNo.Text.Length > 0 && (TxtAcNo1.Text.Length > 0 || TxtAcNo2.Text.Length > 0 || TxtAcNo3.Text.Length > 0 || TxtAcNo4.Text.Length > 0 || TxtAcNo5.Text.Length > 0
                || TxtAcNo6.Text.Length > 0 || TxtAcNo7.Text.Length > 0 || TxtAcNo8.Text.Length > 0 || TxtAcNo9.Text.Length > 0 || TxtAcNo10.Text.Length > 0
                || TxtAcNo11.Text.Length > 0 || TxtAcNo12.Text.Length > 0 || TxtAcNo13.Text.Length > 0 || TxtAcNo14.Text.Length > 0))
            {
                Sm.StdMsg(mMsgType.Warning, "You Can only Choose Account# or COA Digit!");
                return true;
            }
            return false;
        }

        private void GetParameter()
        {
            
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mIsGLShowDataCustomerVendor = Sm.GetParameterBoo("IsGLShowDataCustomerVendor");
            
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptGLUseProfitCenter', 'MainCurCode', 'MenuCodeForRptGLEqualize', 'IsGLShowDataCurrencyRate', ");
            SQL.AppendLine("'IsGLShowDataProfitCenter', 'IsRptGLBtnPrintInvisible', 'IsRptGLShowVoucherDesc', 'IsCOAFilteredByGroup', 'IsRptGLInclOpeningBalance', ");
            SQL.AppendLine("'IsEntityMandatory', 'IsReportingFilterByEntity', 'IsGLShowDataCustomerVendor', 'IsGeneralLedgerUseCOADigit' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsGLShowDataCustomerVendor": mIsGLShowDataCustomerVendor = ParValue == "Y"; break;
                            case "IsRptGLInclOpeningBalance": mIsRptGLInclOpeningBalance = ParValue == "Y"; break;
                            case "IsCOAFilteredByGroup": mIsCOAFilteredByGroup = ParValue == "Y"; break;
                            case "IsRptGLShowVoucherDesc": mIsRptGLShowVoucherDesc = ParValue == "Y"; break;
                            case "IsRptGLBtnPrintInvisible": mIsRptGLBtnPrintInvisible = ParValue == "Y"; break;
                            case "IsGLShowDataProfitCenter": mIsGLShowDataProfitCenter = ParValue == "Y"; break;
                            case "IsGLShowDataCurrencyRate": mIsGLShowDataCurrencyRate = ParValue == "Y"; break;
                            case "MenuCodeForRptGLEqualize": mMenuCodeForRptGLEqualize = mMenuCode==ParValue ; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptGLUseProfitCenter": mIsRptGLUseProfitCenter = ParValue == "Y"; break;
                            case "IsGeneralLedgerUseCOADigit": mIsGeneralLedgerUseCOADigit = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion 

        #region Compute Rate

        private void Process1(ref List<DataJournal> lDataJournal)
        {
            lDataJournal.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string journalDocNo = string.Empty;
            string Filter = string.Empty;

             if (Grd1.Rows.Count >= 1)
             {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    journalDocNo = Sm.GetGrdStr(Grd1, Row, 2);
                    if (journalDocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(X.DocNo=@JournalDocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@JournalDocNo" + No.ToString(), journalDocNo);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParam<string>(ref cm, "@MainCurCode", mMainCurCode);
         
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

               SQL.AppendLine("Select X.DocNo, X.Dno, X.DocDt, X.CurCode From ( ");
               SQL.AppendLine(" -- IP, OP, AP, AR ");
               SQL.AppendLine("     Select '1', A.DocNo, B.Dno, A.DocDt, C.CurCode ");
               //SQL.AppendLine("     IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt ");
               SQL.AppendLine("     From tblJournalHdr A ");
               SQL.AppendLine("     Inner Join TblJournalDtl B On A.DocNo = B.DocNo  ");
               SQL.AppendLine("     inner Join TblVoucherHdr C On A.DocNo = C.journalDocNo And C.CurCode<>@MainCurCode ");
               //SQL.AppendLine("     Where A.menuCode = '0102025004' ");
               SQL.AppendLine("     And C.Doctype in ('01', '02', '03', '04') ");
               SQL.AppendLine("     Where A.DocDt Between @DocDt1 And @DocDt2 ");
               SQL.AppendLine("     UNION ALL -- DOCt ");
               SQL.AppendLine("     Select '2', A.DocNo, B.Dno, A.DocDt, C.CurCode ");
               //SQL.AppendLine("     IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt ");
               SQL.AppendLine("     From tblJournalHdr A ");
               SQL.AppendLine("     Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
               SQL.AppendLine("     Inner Join TblDOCthdr C On A.DocNo = C.journalDocNo And C.CurCode<>@MainCurCode ");
               //SQL.AppendLine("     Where A.menuCode = '01040404' And C.CurCode is not null ");
               SQL.AppendLine("     Where A.DocDt Between @DocDt1 And @DocDt2 ");
               SQL.AppendLine("     Union ALL -- PI ");
               SQL.AppendLine("     Select '3', A.DocNo, B.Dno, A.DocDt, C.CurCode ");
               //SQL.AppendLine("     IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt ");
               SQL.AppendLine("     From tblJournalHdr A ");
               SQL.AppendLine("     Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
               SQL.AppendLine("     Inner Join TblPurchaseInvoicehdr C On A.DocNo = C.journalDocNo And C.CurCode<>@MainCurCode ");
               //SQL.AppendLine("     Where A.menuCode = '0102024002' And C.CurCode is not null ");
               SQL.AppendLine("     Where A.DocDt Between @DocDt1 And @DocDt2 ");
               SQL.AppendLine("     Union ALL --  recv vd ");
               SQL.AppendLine("     Select '4', A.DocNo, B.Dno, A.DocDt,  C.CurCode ");
               //SQL.AppendLine("     IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt ");
               SQL.AppendLine("     From tblJournalHdr A ");
               SQL.AppendLine("     Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
               SQL.AppendLine("     Inner Join TblRecvVdhdr C On A.DocNo = C.journalDocNo And C.CurCode<>@MainCurCode ");
               //SQL.AppendLine("     Where A.menuCode = '01040411' And C.CurCode is not null ");
               SQL.AppendLine("     Where A.DocDt Between @DocDt1 And @DocDt2 ");
               SQL.AppendLine("     UNION ALL ");
               SQL.AppendLine("     Select '5', A.DocNo, B.Dno, A.DocDt, C.CurCode ");
               //SQL.AppendLine("     IfNull(B.DAmt, 0)+IfNull(B.CAmt, 0) As Amt ");
               SQL.AppendLine("     From tblJournalHdr A ");
               SQL.AppendLine("     Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
               SQL.AppendLine("     Inner Join TblVoucherHdr C On A.DocNo = C.journalDocNo And C.CurCode<>@MainCurCode ");
               SQL.AppendLine("     Inner Join TblVoucherrequesthdr D on C.DocNo = D.VoucherDocno ");
               //SQL.AppendLine("     Where A.menuCode = '010314' ");
               SQL.AppendLine("     Where A.DocDt Between @DocDt1 And @DocDt2 ");
               SQL.AppendLine("     Union All -- Recv Vd Dtl ");
               SQL.AppendLine("     Select '6', A.DocNo, B.DNo, A.DocDt, D.CurCode ");
               SQL.AppendLine("     From TblJournalHdr A ");
               SQL.AppendLine("     Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
               SQL.AppendLine("     Inner Join TblRecvVdDtl C On A.JournalDocNo = C.JournalDocNo ");
               SQL.AppendLine("     Inner Join TblRecvVdHdr D On C.DocNo = D.DocNo And D.CurCode <> @MainCurCode ");
               SQL.AppendLine("     Where A.DocDt Between @DocDt1 And @DocDt2 ");
               SQL.AppendLine(")X Where 0=0 ");
               SQL.AppendLine("Order by X.DocDt ");

               cm.CommandText = SQL.ToString();
               

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Dno", "DocDt", "CurCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lDataJournal.Add(new DataJournal()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Dno = Sm.DrStr(dr, c[1]),
                            DocDt = Sm.DrStr(dr, c[2]),
                            CurCode = Sm.DrStr(dr, c[3]),
                            Amt1 = 0m, // Sm.DrDec(dr, c[4]),
                            Amt2 = 1m,
                            RateDt = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<DataJournal> lDataJournal, ref List<DataRate> lDataRate)
        {
            lDataRate.Clear();

            string StartDt = string.Empty;
            string EndDt = string.Empty;

            string minDt = string.Empty, maxDt = string.Empty; 
            
            maxDt = lDataJournal.Max(r => r.DocDt);
            minDt = lDataJournal.Min(r => r.DocDt);

            DateTime MinDt = new DateTime(
                Int32.Parse(minDt.Substring(0, 4)),
                Int32.Parse(minDt.Substring(4, 2)),
                Int32.Parse(minDt.Substring(6, 2)),
                0, 0, 0
                );

            MinDt = MinDt.AddDays(-90);
            minDt =
                MinDt.Year.ToString() +
                ("00" + MinDt.Month.ToString()).Substring(("00" + MinDt.Month.ToString()).Length - 2, 2) +
                ("00" + MinDt.Day.ToString()).Substring(("00" + MinDt.Day.ToString()).Length - 2, 2);

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select RateDt, CurCode1, Amt ");
                SQL.AppendLine("From TblCurrencyRate ");
                SQL.AppendLine("Where CurCode2=@MainCurCode ");
                if (maxDt.Length>0 && minDt.Length>0)
                    SQL.AppendLine("And RateDt Between @MinDt and @MaxDt;");
                else
                    SQL.AppendLine("And 1=0;");
                
                Sm.CmParamDt(ref cm, "@MinDt", minDt);
                Sm.CmParamDt(ref cm, "@MaxDt", maxDt);
                Sm.CmParam<string>(ref cm, "@MainCurCode", mMainCurCode);

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RateDt", "CurCode1", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lDataRate.Add(new DataRate()
                        {
                            RateDt = Sm.DrStr(dr, c[0]),
                            CurCode1 = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }

            if (lDataJournal.Count > 0)
            {
                //lDataJournal.OrderBy(x => x.DocDt);
                //lDataRate.OrderBy(x => x.RateDt);
                //int Temp = 0;

                foreach (var i in lDataJournal.Where(w=>!Sm.CompareStr(w.CurCode, "IDR")))
                {
                    foreach (var j in lDataRate
                        .Where(w => 
                            Sm.CompareStr(w.CurCode1, i.CurCode) &&
                            Sm.CompareDtTm(w.RateDt, i.DocDt) < 1
                            ).OrderByDescending(o=>o.RateDt))
                    {
                        i.Amt2 = j.Amt;
                        i.RateDt = j.RateDt;
                        break;
                    }
                }

                //for (var i = 0; i < lDataJournal.Count; i++)
                //{
                //    if (lDataJournal[i].CurCode != "IDR")
                //    {
                //        for (var j = Temp; j < lDataRate.Count; j++)
                //        {
                //            if (decimal.Parse(lDataRate[j].RateDt) <= decimal.Parse(lDataJournal[i].DocDt) &&
                //               (lDataJournal[i].CurCode == lDataRate[j].CurCode1))
                //            {
                //                lDataJournal[i].Amt2 = lDataRate[j].Amt;
                //                lDataJournal[i].RateDt = lDataRate[j].RateDt;
                //            }
                //        }

                //    }
                //}
            }
        }

        private void Process3(ref List<DataJournal> lDataJournal)
        {
            Grd1.BeginUpdate();

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {    
                Grd1.Cells[r, 26].Value = mMainCurCode;
                Grd1.Cells[r, 27].Value = 1m;
                Grd1.Cells[r, 28].Value = null;
            }
            for (var i = 0; i < lDataJournal.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 2), lDataJournal[i].DocNo) &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 25), lDataJournal[i].Dno)
                        )
                    {
                        Grd1.Cells[Row, 26].Value = lDataJournal[i].CurCode;
                        Grd1.Cells[Row, 27].Value = lDataJournal[i].Amt2;
                        Grd1.Cells[Row, 28].Value = lDataJournal[i].RateDt;
                        break;
                    }
                }
            }

            Grd1.EndUpdate();
        }

        private void ShowCurrencyRate()
        {
            var lDataJournal = new List<DataJournal>();
            var lDataRate = new List<DataRate>();
            Process1(ref lDataJournal);
            if (lDataJournal.Count > 0)
            {
                Process2(ref lDataJournal, ref lDataRate);
                Process3(ref lDataJournal);
            }
            lDataJournal.Clear();
            lDataRate.Clear();
        }

        #endregion

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }        

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
            
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            //Sm.FilterSetTextEdit(this, sender, "Account#");
            if (!ChkAcNo.Checked)
                TxtAcNo.Text = string.Empty;
            if (ChkAcNo.Checked)
                if (TxtAcNo.Text.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Account# is empty!");
                    ChkAcNo.Checked = false;
                }
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo3_CheckedChanged(object sender, EventArgs e)
        {
            if(!ChkAcNo3.Checked)
            {
                foreach (Control c in panel5.Controls)
                {
                    if (c is TextEdit)
                    {
                        c.Text = "";
                    }
                }
            }
            else
            {
                var alert = false;
                foreach (Control c in panel5.Controls)
                {
                    if (c is TextEdit)
                    {
                        if (c.Text.Length > 0)
                        {
                            alert = true;
                            break;
                        }
                    }
                }
                if (!alert)
                {
                    Sm.StdMsg(mMsgType.Warning, "COA Digit is Empty!");
                    ChkAcNo3.Checked = false;
                }
            }
        }

        private void TxtAcNo1_Validated(object sender, EventArgs e)
        {
            if (mIsGeneralLedgerUseCOADigit)
            {
                //ValidateCOADigit();
                bool ada = false;
                foreach (Control c in panel5.Controls)
                {
                    if (c is TextEdit)
                    {
                        if (c.Text.Length > 0)
                            ada = true;
                    }
                }
                if (ada)
                    ChkAcNo3.Checked = true;
                else
                    ChkAcNo3.Checked = false;
            }
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account Description");
        }

        private void ChkAlias_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Alias");
        }

        private void TxtAlias_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void CcbEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Entity");
        }

        private void CcbAcNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkAcNo2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi COA's account");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void TxtPeriod_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Period");
        }

        #endregion

        #region Class

        private class JournalTypeVoucher
        {
            public string DocNo { get; set; }
            public string Remark { get; set; }
        }

        private class COA
        {
            public string ParValue { get; set; }
        }

        private class DataJournal
        {
            public string DocNo { get; set; }
            public string Dno { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public string RateDt { get; set; }
        }

        private class DataRate
        {
            public string RateDt { get; set; }
            public string CurCode1 { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
