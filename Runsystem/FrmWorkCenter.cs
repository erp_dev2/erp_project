﻿#region Update
/*
    30/11/2021 [MYA/IOK] Pada menu Work Center (010601), mengubah (menambah atau mengurangi) Machine atau Employee tanpa harus membuat Work Center baru
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWorkCenter : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty;
        internal FrmWorkCenterFind FrmFind;
        internal bool
            mIsWorkCenterDetailEditable = false;

        #endregion

        #region Constructor

        public FrmWorkCenter(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Work Center";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueUomCode(ref LueUomCode);
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetGrd1();
                SetGrd2();
                SetGrd3();

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd1()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "",
                        //1-3
                        "Machine"+Environment.NewLine+"Code",
                        "Machine"+Environment.NewLine+"Name",
                        "Capacity"
                    },
                    new int[]
                    {
                        20,
                        100, 300, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
        }

        private void SetGrd2()
        {
            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                         //0
                        "",
                        //1-4
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's"+Environment.NewLine+"Name",
                        "Department",
                        "Capacity"
                    },
                    new int[]
                    {
                        //0
                        20,
                        //1-4
                        100, 250, 200, 100
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2, 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 4 }, 0);

        }

        private void SetGrd3()
        {
            Grd3.Cols.Count = 4;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-3
                        "Line Production"+Environment.NewLine+"Code",
                        "Line Production"+Environment.NewLine+"Name",
                        "Capacity"
                    },
                    new int[]
                    {
                        20,
                        100, 300, 100
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 3 }, 0);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, TxtDocName, TxtShortCode, TxtCapacity,
                        LueWhsCode, TxtLocation, LueUomCode, LueDeptCode
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    ChkActiveInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocName, TxtShortCode, DteDocDt, TxtCapacity, LueWhsCode,
                        TxtLocation, LueUomCode, LueDeptCode
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    TxtDocName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocName, TxtShortCode, LueWhsCode, TxtLocation, LueDeptCode
                    }, false);
                    if (mIsWorkCenterDetailEditable)
                    {
                        Grd1.ReadOnly = false;
                        Grd2.ReadOnly = false;
                    }
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkActiveInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
              TxtDocNo, TxtDocName, TxtShortCode, DteDocDt, TxtCapacity,
              TxtLocation, LueUomCode, LueDeptCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
               TxtCapacity,
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.ClearGrd(Grd3, true);
            Sm.FocusGrd(Grd3, 0, 0);
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWorkCenterFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActiveInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method 1

        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWorkCenterDlg1(this));
            }




            if (BtnSave.Enabled)
            {
                if (mIsWorkCenterDetailEditable)
                {
                    if (TxtDocNo.Text.Length != 0)
                    {
                        int Row = Convert.ToInt16(Sm.GetValue(
                        "SELECT COUNT(A.DocNo) " +
                        "FROM TblWorkCenterDtlAssetMachine A " +
                        "WHERE A.DocNo = '" + TxtDocNo.Text + "' " +
                        "GROUP BY A.DocNo "
                        ));
                        if (e.RowIndex < Row)
                            e.DoDefault = false;
                    }

                }
                else
                {
                    if (TxtDocNo.Text.Length != 0 &&
                    ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                        e.DoDefault = false;
                }

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (mIsWorkCenterDetailEditable)
            {
                if (!IsWorkCenterDetailOld(Grd1, "AssetMachine", "AssetCode")) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            else
            {
                if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmWorkCenterDlg1(this));
        }


        #endregion

        #region Grid Method 2

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWorkCenterDlg2(this));
            }



            if (BtnSave.Enabled)
            {
                if (mIsWorkCenterDetailEditable)
                {
                    if (TxtDocNo.Text.Length != 0)
                    {
                        int Row = Convert.ToInt16(Sm.GetValue(
                        "SELECT COUNT(A.DocNo) " +
                        "FROM tblworkcenterdtlemp A " +
                        "WHERE A.DocNo = '" + TxtDocNo.Text + "' " +
                        "GROUP BY A.DocNo "
                        ));
                        if (e.RowIndex < Row)
                            e.DoDefault = false;
                    }
                }
                else
                {
                    if (TxtDocNo.Text.Length != 0 &&
                    ((e.ColIndex == 1 && Sm.GetGrdBool(Grd2, e.RowIndex, 2)) || e.ColIndex != 1))
                        e.DoDefault = false;
                }

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (mIsWorkCenterDetailEditable)
            {
                if (!IsWorkCenterDetailOld(Grd2, "Emp", "EmpCode")) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            }
            else
            {
                if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            }
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmWorkCenterDlg2(this));
        }



        #endregion

        #region Grid Method 3
        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            //if (mIsWorkCenterDetailEditable)
            //{
            //    if (!IsWorkCenterDetailOld(Grd3)) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            //}
            //else
            //{
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            //}
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWorkCenterDlg3(this));
            }

            if (BtnSave.Enabled)
            {
                if (mIsWorkCenterDetailEditable)
                {
                    if (TxtDocNo.Text.Length != 0)
                    {
                        int Row = Convert.ToInt16(Sm.GetValue(
                        "SELECT COUNT(A.DocNo) " +
                        "FROM TblWorkCenterDtlAssetLine A " +
                        "WHERE A.DocNo = '" + TxtDocNo.Text + "' " +
                        "GROUP BY A.DocNo "

                        ));
                        if (e.RowIndex < Row)
                            e.DoDefault = false;
                    }
                }
                else
                {
                    if (TxtDocNo.Text.Length != 0 &&
                    ((e.ColIndex == 1 && Sm.GetGrdBool(Grd3, e.RowIndex, 2)) || e.ColIndex != 1))
                        e.DoDefault = false;
                }


                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmWorkCenterDlg3(this));
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WorkCenter", "TblWorkCenterHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveWorkCenterHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveWorkCenterDtlAssetMachine(DocNo, Row));
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveWorkCenterDtlEmp(DocNo, Row));
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveWorkCenterDtlAssetLine(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private void EditData()
        {
            if (IsWorkCenterAlreadyNotActive() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsWorkCenterDetailEditable)
            {
                cml.Add(EditWorkCenterHdr(TxtDocNo.Text));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveWorkCenterDtlAssetMachine(TxtDocNo.Text, Row));
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveWorkCenterDtlEmp(TxtDocNo.Text, Row));
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveWorkCenterDtlAssetLine(TxtDocNo.Text, Row));
            }
            else
            {

                cml.Add(EditWorkCenterHdr(TxtDocNo.Text));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }


        private MySqlCommand SaveWorkCenterHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWorkCenterHdr(DocNo, DocName, ShortCode, DocDt, Capacity, UomCode, WhsCode, Location, ActiveInd, DeptCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocName, @ShortCode, @DocDt, @Capacity, @UomCode, @WhsCode, @Location, @ActiveInd, @DeptCode, @CreateBy, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("Update DocName=@DocName, ShortCode=@ShortCode, ActiveInd=@ActiveInd, ");
            //SQL.AppendLine("WhsCode=@WhsCode, Location=@Location, DeptCode=@DeptCode, ");
            //SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            //SQL.AppendLine("Delete From TblWorkCenterDtlAssetMachine Where DocNo=@DocNo; ");
            //SQL.AppendLine("Delete From TblWorkCenterDtlAssetMachine Where DocNo=@DocNo; ");
            //SQL.AppendLine("Delete From TblWorkCenterDtlAssetMachine Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Capacity", Decimal.Parse(TxtCapacity.Text));
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueUomCode));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Location", TxtLocation.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditWorkCenterHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWorkCenterHdr(DocNo, DocName, ShortCode, DocDt, Capacity, UomCode, WhsCode, Location, ActiveInd, DeptCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocName, @ShortCode, @DocDt, @Capacity, @UomCode, @WhsCode, @Location, @ActiveInd, @DeptCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update DocName=@DocName, ShortCode=@ShortCode, ActiveInd=@ActiveInd, ");
            SQL.AppendLine("WhsCode=@WhsCode, Location=@Location, DeptCode=@DeptCode, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            if (mIsWorkCenterDetailEditable)
            {
                SQL.AppendLine("Delete From TblWorkCenterDtlAssetMachine Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblWorkCenterDtlEmp Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblWorkCenterDtlAssetLine Where DocNo=@DocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Capacity", Decimal.Parse(TxtCapacity.Text));
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueUomCode));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Location", TxtLocation.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveWorkCenterDtlAssetMachine(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblWorkCenterDtlAssetMachine(DocNo, DNo, AssetCode, Capacity, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @AssetCode, @Capacity, @CreateBy, CurrentDateTime()) ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Capacity", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand EditWorkCenterDtlAssetMachine(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //        "Delete From TblWorkCenterDtlAssetMachine Where DocNo=@DocNo; " + 
        //        "Insert Into TblWorkCenterDtlAssetMachine(DocNo, DNo, AssetCode, Capacity, CreateBy, CreateDt) " +
        //        "Values(@DocNo, @DNo, @AssetCode, @Capacity, @CreateBy, CurrentDateTime());" 
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Capacity", Sm.GetGrdDec(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveWorkCenterDtlEmp(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblWorkCenterDtlEmp(DocNo, DNo, EmpCode, Capacity, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @EmpCode, @Capacity, @CreateBy, CurrentDateTime()) ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Capacity", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand EditWorkCenterDtlEmp(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //        "Delete From TblWorkCenterDtlEmp Where DocNo=@DocNo; " +
        //        "Insert Into TblWorkCenterDtlEmp(DocNo, DNo, EmpCode, Capacity, CreateBy, CreateDt) " +
        //        "Values(@DocNo, @DNo, @EmpCode, @Capacity, @CreateBy, CurrentDateTime()) ; "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Capacity", Sm.GetGrdDec(Grd2, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveWorkCenterDtlAssetLine(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblWorkCenterDtlAssetLine(DocNo, DNo, AssetCode, Capacity, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @AssetCode, @Capacity, @CreateBy, CurrentDateTime()) ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Capacity", Sm.GetGrdDec(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand EditWorkCenterDtlAssetLine(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //        "Delete From TblWorkCenterDtlAssetLine Where DocNo=@DocNo; " +
        //        "Insert Into TblWorkCenterDtlAssetLine(DocNo, DNo, AssetCode, Capacity, CreateBy, CreateDt) " +
        //        "Values(@DocNo, @DNo, @AssetCode, @Capacity, @CreateBy, CurrentDateTime()) ;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Capacity", Sm.GetGrdDec(Grd3, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocName, "Work Center Name", false) ||
                IsCapacityNotValid() ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueUomCode, "UoM") ||
                IsGrdEmpty();
        }

        private bool IsWorkCenterAlreadyNotActive()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWorkCenterHdr " +
                "Where DocNo=@Param And ActiveInd='N' Limit 1;",
                TxtDocNo.Text,
                "This work center already not active."
                );
        }

        private bool IsCapacityNotValid()
        {
            decimal Capacity = 0m;

            if (TxtCapacity.Text.Length != 0) Capacity = decimal.Parse(TxtCapacity.Text);

            if (Capacity <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Capacity is not valid.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd2.Rows.Count == 1 && Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in one of tab grid.");
                return true;
            }
            else if (Grd1.Rows.Count > 1 && Grd2.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only input in one of tab grid.");
                return true;
            }
            else if (Grd2.Rows.Count > 1 && Grd3.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only input in one of tab grid.");
                return true;
            }
            else if (Grd1.Rows.Count > 1 && Grd3.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only input in one of tab grid.");
                return true;
            }
            return false;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowWorkCenterHdr(DocNo);
                ShowWorkCenterDtlAssetMachine(DocNo);
                ShowWorkCenterDtlAssetLine(DocNo);
                ShowWorkCenterDtlEmp(DocNo);

                if (mIsWorkCenterDetailEditable)
                {
                    if (Grd1.Rows.Count >= 1)
                    {
                        Grd1.Rows.Add();
                    }
                    else if (Grd2.Rows.Count >= 1)
                    {
                        Grd2.Rows.Add();
                    }
                    else if (Grd3.Rows.Count >= 1)
                    {
                        Grd3.Rows.Add();
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWorkCenterHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocName, ShortCode, DocDt, Capacity, UomCode, WhsCode, Location, ActiveInd, DeptCode " +
                    "From TblWorkCenterHdr Where DocNo=@DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocName", "ShortCode", "DocDt", "Capacity", "UomCode",  
                        
                        //6-9
                        "WhsCode", "Location", "ActiveInd", "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDocName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtShortCode.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[3]));
                        TxtCapacity.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        Sm.SetLue(LueUomCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[6]));
                        TxtLocation.EditValue = Sm.DrStr(dr, c[7]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[8]), "Y");
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[9]));
                    }, true
                );
        }

        private void ShowWorkCenterDtlAssetMachine(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AssetCode, B.AssetName, ");
                SQL.AppendLine("A.Capacity");
                SQL.AppendLine("From TblWorkCenterDtlAssetMachine A Join TblAsset B On A.AssetCode = B.AssetCode");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "AssetCode","AssetName", "Capacity"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                    }, false, false, false, false
            );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWorkCenterDtlAssetLine(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AssetCode, B.AssetName, ");
                SQL.AppendLine("A.Capacity");
                SQL.AppendLine("From TblWorkCenterDtlAssetLine A Join TblAsset B On A.AssetCode = B.AssetCode");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "AssetCode","AssetName", "Capacity"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd3, dr, c, Row, 3, 2);
                    }, false, false, false, false
            );
                Sm.FocusGrd(Grd3, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWorkCenterDtlEmp(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode, B.EmpName, C.DeptName, A.Capacity ");
                SQL.AppendLine("From TblWorkCenterDtlEmp A");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode");
                SQL.AppendLine("Left Join TblDepartment C on B.DeptCode = C.DeptCode");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[]
                    {
                        "EmpCode","EmpName", "DeptName", "Capacity"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 4, 3);
                    }, false, false, false, false
            );
                Sm.FocusGrd(Grd2, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsWorkCenterDetailEditable = Sm.GetParameterBoo("IsWorkCenterDetailEditable");
        }

        internal string GetSelectedTabPage2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedTabPage3()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd3, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedTabPage1()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private bool IsWorkCenterDetailOld(iGrid Grd, string table, string column)
        {
            string Dtl = Sm.GetValue(
                    "SELECT GROUP_CONCAT(B." + column + ") AS Dtl " +
                    "FROM TblWorkCenterHdr A " +
                    "INNER JOIN TblWorkCenterDtl" + table + " B ON A.DocNo = B.DocNo " +
                    "WHERE A.DocNo = '" + TxtDocNo.Text + "' ; "
            );
            string Item = "";
            string[] mDtl = { };

            if (Dtl.Length > 0)
            {
                mDtl = Dtl.Split(',');
            }

            for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
                Item = Sm.GetGrdStr(Grd, Grd.SelectedRows[Index].Index, 1);

            for (int row = 0; row < Grd.Rows.Count; row++)
            {
                if (Item.Length > 0)
                {
                    foreach (var mDtlInd in mDtl)
                    {
                        if (Item == mDtlInd)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Can't remove this item");
                            return true;
                        }
                    }

                }
            }
            return false;
        }

        #endregion

        #region Event

        private void TxtWorkCenterCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void TxtWorkCenterName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocName);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void TxtLocation_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocation);
        }

        private void TxtCapacity_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCapacity, 0);
        }

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void TxtShortCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShortCode);
        }

        #endregion
    }
}
