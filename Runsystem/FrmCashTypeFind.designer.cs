﻿namespace RunSystem
{
    partial class FrmCashTypeFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkCashType = new DevExpress.XtraEditors.CheckEdit();
            this.TxtCashType = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LueCashTypeGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCashTypeGroup = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCashType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCashTypeGroup.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkCashTypeGroup);
            this.panel2.Controls.Add(this.LueCashTypeGroup);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ChkCashType);
            this.panel2.Controls.Add(this.TxtCashType);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(672, 63);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 410);
            this.Grd1.TabIndex = 14;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkCashType
            // 
            this.ChkCashType.Location = new System.Drawing.Point(441, 9);
            this.ChkCashType.Name = "ChkCashType";
            this.ChkCashType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCashType.Properties.Appearance.Options.UseFont = true;
            this.ChkCashType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCashType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCashType.Properties.Caption = " ";
            this.ChkCashType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCashType.Size = new System.Drawing.Size(31, 22);
            this.ChkCashType.TabIndex = 11;
            this.ChkCashType.ToolTip = "Remove filter";
            this.ChkCashType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCashType.ToolTipTitle = "Run System";
            this.ChkCashType.CheckedChanged += new System.EventHandler(this.ChkCashType_CheckedChanged);
            // 
            // TxtCashType
            // 
            this.TxtCashType.EnterMoveNextControl = true;
            this.TxtCashType.Location = new System.Drawing.Point(120, 10);
            this.TxtCashType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCashType.Name = "TxtCashType";
            this.TxtCashType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCashType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCashType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCashType.Properties.Appearance.Options.UseFont = true;
            this.TxtCashType.Properties.MaxLength = 80;
            this.TxtCashType.Size = new System.Drawing.Size(319, 20);
            this.TxtCashType.TabIndex = 10;
            this.TxtCashType.Validated += new System.EventHandler(this.TxtCashType_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(53, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Cash Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Cash Type Group";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashTypeGroup
            // 
            this.LueCashTypeGroup.EnterMoveNextControl = true;
            this.LueCashTypeGroup.Location = new System.Drawing.Point(120, 32);
            this.LueCashTypeGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashTypeGroup.Name = "LueCashTypeGroup";
            this.LueCashTypeGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.Appearance.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashTypeGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashTypeGroup.Properties.DropDownRows = 12;
            this.LueCashTypeGroup.Properties.NullText = "[Empty]";
            this.LueCashTypeGroup.Properties.PopupWidth = 500;
            this.LueCashTypeGroup.Size = new System.Drawing.Size(319, 20);
            this.LueCashTypeGroup.TabIndex = 13;
            this.LueCashTypeGroup.ToolTip = "F4 : Show/hide list";
            this.LueCashTypeGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashTypeGroup.EditValueChanged += new System.EventHandler(this.LueCashTypeGroup_EditValueChanged);
            // 
            // ChkCashTypeGroup
            // 
            this.ChkCashTypeGroup.Location = new System.Drawing.Point(441, 31);
            this.ChkCashTypeGroup.Name = "ChkCashTypeGroup";
            this.ChkCashTypeGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCashTypeGroup.Properties.Appearance.Options.UseFont = true;
            this.ChkCashTypeGroup.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCashTypeGroup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCashTypeGroup.Properties.Caption = " ";
            this.ChkCashTypeGroup.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCashTypeGroup.Size = new System.Drawing.Size(31, 22);
            this.ChkCashTypeGroup.TabIndex = 14;
            this.ChkCashTypeGroup.ToolTip = "Remove filter";
            this.ChkCashTypeGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCashTypeGroup.ToolTipTitle = "Run System";
            this.ChkCashTypeGroup.CheckedChanged += new System.EventHandler(this.ChkCashTypeGroup_CheckedChanged);
            // 
            // FrmCashTypeFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmCashTypeFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCashType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCashTypeGroup.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkCashType;
        private DevExpress.XtraEditors.TextEdit TxtCashType;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueCashTypeGroup;
        private DevExpress.XtraEditors.CheckEdit ChkCashTypeGroup;
    }
}