﻿#region Update
/*
    16/12/2020 [WED/IMS] new apps (PI with approval)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoice3Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseInvoice3 mFrmParent;
        private string 
            mSQL = string.Empty, mVdCode = string.Empty, 
            mPIQRCodeDPTaxDocType = string.Empty, 
            mPIQRCodeDPTaxDocTypeDesc = string.Empty,
            mPIQRCodeTaxDocType = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseInvoice3Dlg4(
            FrmPurchaseInvoice3 FrmParent, 
            string VdCode, 
            string PIQRCodeDPTaxDocType,
            string PIQRCodeTaxDocType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
            mPIQRCodeDPTaxDocType = PIQRCodeDPTaxDocType;
            mPIQRCodeDPTaxDocTypeDesc = Sm.GetValue("Select OptDesc From TblOption Where OptCat='PurchaseInvoiceDocType' And OptCode=@Param;", mPIQRCodeDPTaxDocType);
            mPIQRCodeTaxDocType = PIQRCodeTaxDocType;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Document#",
                        "DNo",
                        "",
                        "Date",
                        
                        //6-10
                        "PO#",
                        "QR Code", 
                        "Document#",  
                        "Tax Invoice"+Environment.NewLine+"Date", 
                        "Currency",
                        
                        //11-15
                        "Amount", 
                        "Tax", 
                        "Indicator Code", 
                        "Indicator", 
                        "Remark", 

                        //16
                        "RemarkXML"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 140, 0, 20, 80, 
                        
                        //6-10
                        140, 130, 130, 100, 80, 

                        //11-15
                        120, 120, 0, 100, 250, 
                        
                        //16
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 13, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.DocNo, C.DNo, A.DocDt, A.PODocNo, ");
            SQL.AppendLine("C.DocNumber, A.CurCode, C.Amt, C.TaxAmt, C.DocInd, D.OptDesc As DocIndDesc, ");
            SQL.AppendLine("C.TaxInvDt, C.QRCode, C.Remark, C.RemarkXml ");
            SQL.AppendLine("From TblAPDownpayment A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo And B.VdCode=@VdCode ");
            SQL.AppendLine("Inner Join TblAPDownPaymentDtl2 C On A.DocNo=C.DocNo And C.PurchaseInvoiceDocNo Is Null ");
            SQL.AppendLine("Left Join TblOption D On C.DocInd=D.OptCode And D.OptCat='PurchaseInvoiceDocInd' ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string Filter = string.Empty, DocNo = string.Empty, DNo = string.Empty;
            var cm = new MySqlCommand();

            try
            {
                
                if (mFrmParent.Grd3.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd3, r, 13);
                        DNo = Sm.GetGrdStr(mFrmParent.Grd3, r, 14);
                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += " Not(C.DocNo=@DocNo0" + r.ToString() + " And C.DNo=@DNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), DNo);
                        }
                    }
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@VdCode", mVdCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DNo", 
                             "DocDt", 
                             "PODocNo", 
                             "QRCode", 
                             "DocNumber", 
                             
                             //6-10
                             "TaxInvDt", 
                             "CurCode", 
                             "Amt", 
                             "TaxAmt", 
                             "DocInd", 
                             
                             //11-13
                             "DocIndDesc", 
                             "Remark", 
                             "RemarkXml"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 13, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 14, Grd1, Row2, 3);

                        mFrmParent.Grd3.Cells[Row1, 1].Value = mPIQRCodeDPTaxDocType;
                        mFrmParent.Grd3.Cells[Row1, 2].Value = mPIQRCodeDPTaxDocTypeDesc;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 7);

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 8, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 9, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 10, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 11, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 12, Grd1, Row2, 16);

                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 7, 8 });

                        decimal TaxAmt = 0m;
                        //bool IsOnlyDPTaxDocType = mFrmParent.IsOnlyDPTaxDocTypeExisted();

                        //if (IsOnlyDPTaxDocType)
                        if (Sm.CompareStr(mFrmParent.mPIQRCodeTaxDocType, Sm.GetGrdStr(mFrmParent.Grd3, Row1, 1)))
                        {
                            var TaxInvoiceNo = Sm.GetGrdStr(mFrmParent.Grd3, Row1, 5);
                            var TaxInvDt = Sm.GetGrdDate(mFrmParent.Grd3, Row1, 6);
                            //var TaxAmt = 0m;

                            //if (Sm.GetGrdStr(mFrmParent.Grd3, Row1, 8).Length > 0)
                            //    TaxAmt = Sm.GetGrdDec(mFrmParent.Grd3, Row1, 8);

                            if (TaxInvoiceNo.Length > 0)
                            {
                                mFrmParent.TxtQRCodeTaxInvoiceNo.EditValue = TaxInvoiceNo;
                                mFrmParent.TxtTaxInvoiceNo.EditValue = TaxInvoiceNo;
                            }
                            else
                                mFrmParent.TxtQRCodeTaxInvoiceNo.EditValue = null;

                            if (TaxInvDt.Length > 0)
                            {
                                TaxInvDt = TaxInvDt.Substring(0, 8);
                                Sm.SetDte(mFrmParent.DteQRCodeTaxInvoiceDt, TaxInvDt);
                                Sm.SetDte(mFrmParent.DteTaxInvoiceDt, TaxInvDt);
                            }
                            else
                                mFrmParent.DteQRCodeTaxInvoiceDt.EditValue = null;
                        }

                        for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                        {
                            //if (IsOnlyDPTaxDocType)
                            //{
                            //    if (Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(mFrmParent.Grd3, r, 1)))
                            //    {
                            //        if (Sm.GetGrdStr(mFrmParent.Grd3, r, 8).Length > 0)
                            //            TaxAmt += Sm.GetGrdDec(mFrmParent.Grd3, r, 8);
                            //    }
                            //}
                            //else
                            //{
                            //    if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(mFrmParent.Grd3, r, 1)))
                            //    {
                            //        if (Sm.GetGrdStr(mFrmParent.Grd3, r, 8).Length > 0)
                            //            TaxAmt += Sm.GetGrdDec(mFrmParent.Grd3, r, 8);
                            //    }
                            //}
                            
                            if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(mFrmParent.Grd3, r, 1)) ||
                                Sm.CompareStr(mPIQRCodeDPTaxDocType, Sm.GetGrdStr(mFrmParent.Grd3, r, 1)))
                            {
                                if (Sm.GetGrdStr(mFrmParent.Grd3, r, 8).Length > 0)
                                    TaxAmt += Sm.GetGrdDec(mFrmParent.Grd3, r, 8);
                            }
                        }
                        mFrmParent.TxtQRCodeTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
                        mFrmParent.ComputeAmt();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var key = string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
            for (int Index = 0; Index <= mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key, 
                    string.Concat(Sm.GetGrdStr(mFrmParent.Grd3, Index, 13), Sm.GetGrdStr(mFrmParent.Grd3, Index, 14)))) 
                    return true;
            return false;
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAPDownpayment("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAPDownpayment("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion

    }
}
