﻿#region Update

/*
 *  31/05/2022 [SET/PRODUCT] Menu baru Acquisition of Debt Securities
 *  15/06/2022 [SET/PRODUCT] Penyesuaian
 *  27/06/2022 [SET/PRODUCT] Menyesuaikan field Investment Category terisi dengan nama Investment Category
 *                           Tambah % untuk Interest Rate
 *  28/06/2022 [SET/PRODUCT] menyesuaikan dlg3 untuk source data merujuk dari menu Type of Expenses
 *                           menyesuaikan SaveStock() - Qty menjadi NominalAmt
 *  29/06/2022 [SET/PRODUCT] Menyesuaikan Settlement Date
 *  30/06/2022 [SET/PRODUCT] Penyesuaian perhitungan Amount di detail tab Expenses
 *  01/07/2022 [SET/PRODUCT] muncul "Value Should Not Less Than 0" kena di GrdAfterCommitEditSetZeroIfEmpty()
 *                           menyesuaikan value Amount detail tab Expenses
 *  05/07/2022 [SET/PRODUCT] penyesuain perhitungan Accrued Interest
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionDebtSecurities : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        iGCell fCell;
        bool fAccept;
        //internal Frm FrmFind;
        internal string
            mBankAccountTypeForInvestment = string.Empty,
            mDocType = "04",
            mMainCurCode = string.Empty,
            mInvestmentDebtCode = string.Empty,
            mInvestmentBankAccount = string.Empty,
            mInvestmentCategory = string.Empty;
        internal FrmAcquisitionDebtSecuritiesFind FrmFind;
        //private string
        //    mDocType = "02",
        //    mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmAcquisitionDebtSecurities(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",
                    
                    //1-5
                    "Expenses Code", "Document Type", "", "CoA Account", "CoA Name", 

                    //6-10
                    "Doc Ref#", "Doc Date", "Amount", "Remarks", "Rate", 

                    //11-14
                    "Amt", "TaxInd", "DNo", "Formula"

                },
                new int[]
                {
                    //0
                    20,

                    //1-5
                    100, 150, 20, 100, 150, 
                    
                    //6-10
                    120, 100, 150, 100, 100, 

                    //11-14
                    100, 20, 50, 50

                }
            );

            Sm.GrdColCheck(Grd1, new int[] { 12 });
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 11 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 10, 11, 12, 13, 14 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 5;
            //Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",
                    
                    //1-4
                    "User", "Status", "Date", "Remark",

                },
                new int[]
                {
                    //0
                    25,

                    //1-5
                    100, 100, 100, 100,

                }
            );
            //Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdFormatDec(Grd2, new int[] { }, 0);
            Sm.GrdColButton(Grd2, new int[] { });
            Sm.GrdColInvisible(Grd2, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, TxtDocNo, MeeCancelReason, LueFinancialInstitution, TxtInvestmentBankAcc, TxtStatus, TxtInvestmentCost, TxtTotalExpenses,
                        DteTradeDt, LueFinancialInstitution, MeeRemark, DteSettlementDt, TxtInvestmentBankAcc, TxtCurCode, TxtCurRate, TxtAccruedInterest,
                        TxtTotalAcquisition, TxtDocNoVoucher, ChkCancelInd, LueInvestmentType, DteListingDt, DteMaturityDt, DteLastCouponDt, DteNextCouponDt,
                        TxtAmt, TxtAcquisitionPrice
                    }, true);
                    //Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    BtnInvestmentBankAcc.Enabled = false;
                    BtnInvestmentCode.Enabled = false;
                    BtnInvestmentName.Enabled = false;
                    BtnRefreshData.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueFinancialInstitution, DteTradeDt, DteSettlementDt, MeeRemark, LueInvestmentType, TxtAmt, TxtAcquisitionPrice
                    }, false);
                    //Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 4, 7, 9 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 6, 7, 8, 9 });
                    BtnInvestmentBankAcc.Enabled = true;
                    BtnInvestmentCode.Enabled = true;
                    BtnInvestmentName.Enabled = true;
                    BtnRefreshData.Enabled = true;
                    TxtCurRate.Text = "1";
                    TxtStatus.Text = "Outstanding";
                    TxtAmt.Text = "0.00";
                    //Sm.FormatNumTxt(TxtAmt, 0);
                    TxtAcquisitionPrice.Text = "100.10";
                    Sm.FormatNumTxt(TxtCurRate, 0);
                    Sm.FormatNumTxt(TxtInvestmentCost, 0);
                    Sm.FormatNumTxt(TxtAccruedInterest, 0);
                    Sm.FormatNumTxt(TxtTotalExpenses, 0);
                    Sm.FormatNumTxt(TxtTotalAcquisition, 0);
                    Sm.FormatNumTxt(TxtCouponInterestRate, 0);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, false);
                    //Sm.GrdColReadOnly(false, true, Grd1, new int[] {  });
                    MeeCancelReason.Focus();
                    //Grd1.ReadOnly = true;
                    //Grd2.ReadOnly = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueFinancialInstitution, TxtInvestmentBankAcc, TxtStatus, TxtDocNoVoucher, TxtInvestmentCode,
                TxtInvestmentName, LueInvestmentType, TxtCtInvestment, TxtAmt, TxtAcquisitionPrice, TxtInvestmentCost, TxtCouponInterestRate, TxtAnnualDays,
                TxtInterestFrequency, DteTradeDt, DteSettlementDt, DteListingDt, DteMaturityDt, DteLastCouponDt, DteNextCouponDt, TxtCurCode, TxtCurRate, 
                TxtAccruedInterest, TxtTotalExpenses, TxtTotalAcquisition, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ClearGrd()
        {
            //Sm.ClearGrd(Grd1, true);
            //Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2, 8 });
            //Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 9, 10, 11, 12 });

            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 10, 11 });

            //Sm.ClearGrd(Grd3, true);
        }


        #endregion

        #region Button Method

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteTradeDt);
                SetSettlementDate(Sm.GetDte(DteTradeDt));
                Sl.SetLueOption(ref LueInvestmentType, "InvestmentType");
                SetLueFinancialInstitutionCode(ref LueFinancialInstitution, string.Empty);
                TxtStatus.Text = "Purchased"; //belum fix
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAcquisitionDebtSecuritiesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AcquisitionDebtSecurities", "TblAcquisitionDebtSecuritiesHdr");

            cml.Add(SaveAcquisitionDebtSecurities(DocNo));
            //cml.Add(SaveAcquisitionEquitySecuritiesDtl2(DocNo));

            cml.Add(SaveStock(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueFinancialInstitution, "Financial Institution") ||
                Sm.IsTxtEmpty(TxtInvestmentBankAcc, "Investment Bank Account", false) ||
                Sm.IsLueEmpty(LueInvestmentType, "Investment Type") ||
                Sm.IsTxtEmpty(TxtAmt, "Nominal Amount", false) ||
                Sm.IsTxtEmpty(TxtAcquisitionPrice, "Acquistion Price", false) ||
                Sm.IsDteEmpty(DteTradeDt, "Trade Date") ||
                Sm.IsDteEmpty(DteSettlementDt, "Settlement Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 detail.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Investment is null")) return true;
            }

            return false;
        }

        private MySqlCommand SaveAcquisitionDebtSecurities(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();

            //Cancel Document
            //SQL.AppendLine("UPDATE tblpositionadjustmenthdr ");
            //SQL.AppendLine("SET CancelInd = 'Y', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            //SQL.AppendLine("WHERE CancelInd = 'N'; ");

            //Hdr
            SQL.AppendLine("Insert Into TblAcquisitionDebtSecuritiesHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, SekuritasCode, BankAcCode, Status, VoucherDocNo, InvestmentDebtCode, InvestmentCode, ");
            SQL.AppendLine("InvestmentType, Amt, AcquisitionPrice, InvestmentCost, TradeDt, SettlementDt, CurCode, CurRate, AccruedInterest, ");
            SQL.AppendLine("TotalExpenses, TotalAcquisition, Remark, Source, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @SekuritasCode, @BankAcCode, @Status, NULL, @InvestmentDebtCode, @InvestmentCode, ");
            SQL.AppendLine("@InvestmentType, @Amt, @AcquisitionPrice, @InvestmentCost, @TradeDt, @SettlementDt, @CurCode, @CurRate, @AccruedInterest, ");
            SQL.AppendLine("@TotalExpenses, @TotalAcquisition, @Remark, CONCAT(@DocType, '*', @DocNo), @CreateBy, CurrentDateTime()); ");

            //Update Last Coupon & Next Coupon di Investment Item Debt
            SQL.AppendLine("UPDATE TblInvestmentItemDebt ");
            SQL.AppendLine("SET LastCouponDt=@LastCouponDt, NextCouponDt=@NextCouponDt ");
            SQL.AppendLine("WHERE InvestmentDebtCode = @InvestmentDebtCode AND LastCouponDt IS NULL OR NextCouponDt IS NULL; ");

            //Doc Approval
            //SQL.AppendLine("INSERT INTO tbldocapproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("SELECT T.DocType, @DocNo, '001', T.DNo, @Usercode, CurrentDateTime() ");
            //SQL.AppendLine("FROM tbldocapprovalsetting T ");
            //SQL.AppendLine("WHERE DocType = ''; ");
            //SQL.AppendLine("");

            //Update Status (if DocApproval Not Exists)
            //SQL.AppendLine("Update TblAcquisitionEquitySecuritiesHdr Set Status='Purchased' ");
            //SQL.AppendLine("Where DocNo=@DocNo; ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select 1 From TblDocApproval ");
            //SQL.AppendLine("    Where DocType='TblAcquisitionEquitySecuritiesHdr' ");
            //SQL.AppendLine("    And DocNo=@DocNo ");
            //SQL.AppendLine("    ); ");

            //Dtl
            SQLDtl.AppendLine("Insert Into tblacquisitiondebtsecuritiesdtl ");
            SQLDtl.AppendLine("(DocNo, DNo, ExpensesCode, AcNo, DocRef, DocDt, Amt, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl.AppendLine(", ");

                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @ExpensesCode_" + r.ToString() + ", @AcNo_" + r.ToString() + ", @DocRef_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@DocDt_" + r.ToString() + ", @Amt_" + r.ToString() + ", @Remark_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@CreateBy, CurrentDateTime()) ");

                if(Sm.GetGrdStr(Grd1, r, 13) != null)
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
                else
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ExpensesCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                Sm.CmParam<String>(ref cm, "@DocRef_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                Sm.CmParamDt(ref cm, "@DocDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 7));
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
            }
            SQLDtl.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQLDtl.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SekuritasCode", Sm.GetLue(LueFinancialInstitution));
            Sm.CmParam<String>(ref cm, "@BankAcCode", mInvestmentBankAccount);
            Sm.CmParam<String>(ref cm, "@Status", TxtStatus.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentCode", TxtInvestmentCode.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentDebtCode", mInvestmentDebtCode);
            Sm.CmParam<String>(ref cm, "@InvestmentType", Sm.GetLue(LueInvestmentType));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AcquisitionPrice", decimal.Parse(TxtAcquisitionPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@InvestmentCost", decimal.Parse(TxtInvestmentCost.Text));
            Sm.CmParamDt(ref cm, "@TradeDt", Sm.GetDte(DteTradeDt));
            Sm.CmParamDt(ref cm, "@SettlementDt", Sm.GetDte(DteSettlementDt));
            Sm.CmParamDt(ref cm, "@LastCouponDt", Sm.GetDte(DteLastCouponDt));
            Sm.CmParamDt(ref cm, "@NextCouponDt", Sm.GetDte(DteNextCouponDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@CurRate", decimal.Parse(TxtCurRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@AccruedInterest", decimal.Parse(TxtAccruedInterest.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalExpenses", decimal.Parse(TxtTotalExpenses.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAcquisition", decimal.Parse(TxtTotalAcquisition.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* AcquisitionDebtSecurities - Stock */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, NominalAmt, Source, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, '001', A.DocDt, NULL, A.BankAcCode, '-', '-', A.InvestmentDebtCode, A.InvestmentType, '-', A.Amt, A.Source, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From TblAcquisitionDebtSecuritiesHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockSummary(WhsCode, BankAcCode, Lot, Bin, InvestmentCode, BatchNo, NominalAmt, Source, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '-', A.BankAcCode, '-', '-', A.InvestmentDebtCode, '-', A.Amt, A.Source, Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblAcquisitionDebtSecuritiesHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockPrice(InvestmentCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.InvestmentDebtCode, '-', A.Source, A.CurCode, ");
            SQL.AppendLine("A.AcquisitionPrice, ");
            SQL.AppendLine("A.CurRate As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblAcquisitionDebtSecuritiesHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAcquisitionDebtSecurities());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private MySqlCommand EditAcquisitionDebtSecurities()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update Tblacquisitiondebtsecuritieshdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, CancelReason = @CancelReason, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Qty, Source, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, '001', 'Y', A.DocDt, NULL, A.BankAcCode, '-', '-', A.InvestmentDebtCode, A.InvestmentType, '-', -1*A.Amt, A.Source, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblAcquisitionDebtSecuritiesHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            //SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, InvestmentType, BatchNo, Source, Qty, Remark, ");
            //SQL.AppendLine("CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, NULL, A.BankAcCode, '-', '-', B.InvestmentEquityCode, B.InvestmentType, '-', B.Source, -1*B.Qty, ");
            //SQL.AppendLine("A.Remark, ");
            //SQL.AppendLine("@UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblAcquisitionEquitySecuritiesHdr A ");
            //SQL.AppendLine("Inner Join TblAcquisitionEquitySecuritiesDtl B On A.DocNo=B.DocNo /*And B.Status='A' */And A.CancelInd='Y' ");
            //SQL.AppendLine("Inner Join TblInvestmentItemEquity C On B.InvestmentEquityCode=C.InvestmentEquityCode And C.ActInd='Y' ");
            //SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("UPDATE tblinvestmentstocksummary T ");
            SQL.AppendLine("    INNER JOIN tblacquisitiondebtsecuritieshdr X ON T.Source = X.Source AND X.DocNo = @DocNo ");
            SQL.AppendLine("    SET T.Qty=0, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("    WHERE T.BankAcCode = X.BankAcCode AND T.Source = X.Source; ");

            //SQL.AppendLine("UPDATE tblinvestmentstocksummary T ");
            //SQL.AppendLine("    INNER JOIN tblacquisitionequitysecuritiesdtl X ON T.Source = X.Source AND X.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN tblacquisitionequitysecuritieshdr Y ON X.DocNo = Y.DocNo ");
            //SQL.AppendLine("    SET T.Qty=0, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("    WHERE T.BankAcCode = Y.BankAcCode AND T.Source = X.Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.SekuritasCode, B.BankAcNm, A.`Status`, A.VoucherDocNo, A.InvestmentCode, D.PortofolioName InvestmentName, ");
                SQL.AppendLine("E.OptCode, F.InvestmentCtName, A.Amt, A.AcquisitionPrice, A.InvestmentCost, C.InterestRateAmt, G.OptDesc AnnualDays, C.InterestFreq, A.TradeDt, A.SettlementDt, ");
                SQL.AppendLine("C.ListingDt, C.MaturityDt, C.LastCouponDt, C.NextCouponDt, A.CurCode, A.CurRate, A.AccruedInterest, A.TotalExpenses, A.TotalAcquisition, A.Remark ");
                SQL.AppendLine("FROM tblacquisitiondebtsecuritieshdr A ");
                SQL.AppendLine("INNER JOIN tblbankaccount B ON A.BankAcCode = B.BankAcCode ");
                SQL.AppendLine("INNER JOIN tblinvestmentitemdebt C ON A.InvestmentCode = C.PortofolioId ");
                SQL.AppendLine("INNER JOIN tblinvestmentportofolio D ON A.InvestmentCode = D.PortofolioId ");
                SQL.AppendLine("INNER JOIN tbloption E ON A.InvestmentType = E.OptCode AND E.OptCat = 'InvestmentType' ");
                SQL.AppendLine("INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode ");
                SQL.AppendLine("INNER JOIN tbloption G ON C.AnnualDays = G.OptCode AND G.OptCat = 'AnnualDaysAssumption' ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "CancelReason", "SekuritasCode", "BankAcNm", 

                            //6-10
                            "Status", "VoucherDocNo", "InvestmentCode", "InvestmentName", "OptCode", 

                            //11-15
                            "InvestmentCtName", "Amt", "AcquisitionPrice", "InvestmentCost", "InterestRateAmt", 
                            
                            //16-20
                            "AnnualDays", "InterestFreq", "TradeDt", "SettlementDt", "ListingDt",

                            //21-25
                            "MaturityDt", "LastCouponDt", "NextCouponDt", "CurCode", "CurRate",

                            //26-29
                            "AccruedInterest", "TotalExpenses", "TotalAcquisition", "Remark"

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            MeeCancelReason.Text = Sm.DrStr(dr, c[3]);
                            SetLueFinancialInstitutionCode(ref LueFinancialInstitution, Sm.DrStr(dr, c[4]));
                            TxtInvestmentBankAcc.Text = Sm.DrStr(dr, c[5]);
                            TxtStatus.Text = Sm.DrStr(dr, c[6]);
                            TxtDocNoVoucher.Text = Sm.DrStr(dr, c[7]);
                            TxtInvestmentCode.Text = Sm.DrStr(dr, c[8]);
                            TxtInvestmentName.Text = Sm.DrStr(dr, c[9]);
                            SetLueInvestmentType(ref LueInvestmentType, Sm.DrStr(dr, c[10]));
                            TxtCtInvestment.Text = Sm.DrStr(dr, c[11]);
                            TxtAmt.Text = Sm.DrStr(dr, c[12]);
                            TxtAcquisitionPrice.Text = Sm.DrStr(dr, c[13]);
                            TxtInvestmentCost.Text = Sm.DrStr(dr, c[14]);
                            TxtCouponInterestRate.Text = Sm.DrStr(dr, c[15]);
                            TxtAnnualDays.Text = Sm.DrStr(dr, c[16]);
                            TxtInterestFrequency.Text = Sm.DrStr(dr, c[17]);
                            Sm.SetDte(DteTradeDt, Sm.DrStr(dr, c[18]));
                            Sm.SetDte(DteSettlementDt, Sm.DrStr(dr, c[19]));
                            Sm.SetDte(DteListingDt, Sm.DrStr(dr, c[20]));
                            Sm.SetDte(DteMaturityDt, Sm.DrStr(dr, c[21]));
                            Sm.SetDte(DteLastCouponDt, Sm.DrStr(dr, c[22]));
                            Sm.SetDte(DteNextCouponDt, Sm.DrStr(dr, c[23]));
                            TxtCurCode.Text = Sm.DrStr(dr, c[24]);
                            TxtCurRate.Text = Sm.DrStr(dr, c[25]);
                            TxtAccruedInterest.Text = Sm.DrStr(dr, c[26]);
                            TxtTotalExpenses.Text = Sm.DrStr(dr, c[27]);
                            TxtTotalAcquisition.Text = Sm.DrStr(dr, c[28]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[29]);
                        }, true
                    );
                TxtAmt.EditValue = Sm.FormatNum(Decimal.Parse(TxtAmt.Text), 0);
                TxtAcquisitionPrice.EditValue = Sm.FormatNum(Decimal.Parse(TxtAcquisitionPrice.Text), 0);
                TxtInvestmentCost.EditValue = Sm.FormatNum(Decimal.Parse(TxtInvestmentCost.Text), 0);
                Sm.FormatNumTxt(TxtCouponInterestRate, 0);
                Sm.FormatNumTxt(TxtAnnualDays, 0);
                Sm.FormatNumTxt(TxtCurRate, 0);
                TxtAccruedInterest.EditValue = Sm.FormatNum(Decimal.Parse(TxtAccruedInterest.Text), 0);
                TxtTotalExpenses.EditValue = Sm.FormatNum(Decimal.Parse(TxtTotalExpenses.Text), 0);
                TxtTotalAcquisition.EditValue = Sm.FormatNum(Decimal.Parse(TxtTotalAcquisition.Text), 0);
                ShowAcquisitionEquitySecuritiesDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAcquisitionEquitySecuritiesDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT A.ExpensesCode, C.DocType, A.AcNo, D.AcDesc, A.DocRef, A.DocDt, A.Amt Amount, A.Remark, C.Rate, C.Amt, C.TaxInd, A.DNo ");
            SQL.AppendLine("FROM tblacquisitiondebtsecuritiesdtl A ");
            SQL.AppendLine("INNER JOIN tblexpensestypehdr B ON A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("INNER JOIN tblexpensestypedtl C ON B.ExpensesCode = C.ExpensesCode AND A.DNo = C.DNo ");
            SQL.AppendLine("INNER JOIN tblcoa D ON A.AcNo = D.AcNo ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ORDER BY DNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "ExpensesCode", "DocType", "AcNo", "AcDesc", "DocRef", "DocDt", "Amount", "Remark", "Rate", "Amt", "TaxInd", "DNo" },
                (MySqlDataReader dr, iGrid Grd1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            //TxtInvestmentCost.Text = Sm.GetGrdStr(Grd1, 0, 10);
            //Sm.FormatNumTxt(TxtInvestmentCost, 0);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 11 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal void CheckCouponDate()
        {
            if(Sm.GetDte(DteLastCouponDt) == "")
            {
                Sm.SetDteCurrentDate(DteLastCouponDt);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            DteLastCouponDt
                        }, false);
            }
            if(Sm.GetDte(DteNextCouponDt) == "")
            {
                Sm.SetDteCurrentDate(DteNextCouponDt);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            DteNextCouponDt
                        }, false);
            }
        }

        internal string GetSelectedExpenses()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            '#' +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeTotalInvestment()
        {
            decimal InvestmentCost = 0m, TotalExpenses = 0m;
            InvestmentCost = Decimal.Parse(TxtInvestmentCost.Text);
            TotalExpenses = Decimal.Parse(TxtTotalExpenses.Text);

            TxtTotalAcquisition.Text = Convert.ToString(InvestmentCost + TotalExpenses);
            //Sm.FormatNumTxt(TxtTotalAcquisition, 0);
            TxtTotalAcquisition.EditValue = Sm.FormatNum(Decimal.Parse(TxtTotalAcquisition.Text), 0);
        }

        internal void ComputeTotalExpenses()
        {
            decimal TotalExpenses = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                TotalExpenses += Sm.GetGrdDec(Grd1, r, 8);
            }
            TxtTotalExpenses.Text = Convert.ToString(TotalExpenses);
            //Sm.FormatNumTxt(TxtTotalExpenses, 0);
            TxtTotalExpenses.EditValue = Sm.FormatNum(Decimal.Parse(TxtTotalExpenses.Text), 0);
        }

        internal void ComputeAmountDtlExpenses()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                decimal AccruedInterest = 0m, Amount = 0m, NominalAmt = 0, Rate = 0m, Amt = 0m;
                AccruedInterest = Decimal.Parse(TxtAccruedInterest.Text);
                Rate = Sm.GetGrdDec(Grd1, r, 10);
                Amt = Sm.GetGrdDec(Grd1, r, 11);

                if (Sm.GetGrdStr(Grd1, r, 14) == "1")
                {
                    if (Amt == 0)
                    {
                        Amount = AccruedInterest * (Rate / 100);
                        Grd1.Cells[r, 8].Value = Sm.Round(Amount, 2);
                    } 
                    else if (Rate == 0)
                    {
                        Grd1.Cells[r, 8].Value = Amt;
                    }
                }
                else if (Sm.GetGrdStr(Grd1, r, 14) == "2")
                {
                    if (Amt == 0)
                    {
                        Amount = AccruedInterest * (Rate / 100);
                        Grd1.Cells[r, 8].Value = Sm.Round(Amount, 2);
                    }
                    else if (Rate == 0)
                    {
                        Grd1.Cells[r, 8].Value = Amt;
                    }
                }
            }
        }

        private void GetExpensesData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ExpensesCode, B.DocType, B.AcNo, C.AcDesc, B.Rate, B.Amt, B.TaxInd, B.Formula, B.DNo ");
            SQL.AppendLine("From TblExpensesTypeHdr A ");
            SQL.AppendLine("Inner Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.TransactionCode = @TransactionCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TransactionCode", mMenuCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                {
                        //0
                        "ExpensesCode",

                        //1-5
                        "DocType", "AcNo", "AcDesc", "Rate",  "Amt", 

                        //6-8
                        "TaxInd", "DNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grid1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 5);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 12, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 8);
                    //Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 8);
                    Grd1.Cells[Row, 7].Value = DteTradeDt.Text;
                    Grd1.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 11 });

            ComputeAccruedInterest();
        }

        internal void ComputeTotalAcquisitionCost()
        {
            decimal InvestCost = 0m, TotalExpenses = 0m;

            InvestCost = Decimal.Parse(TxtInvestmentCost.Text);
            TotalExpenses = Decimal.Parse(TxtTotalExpenses.Text);

            TxtTotalAcquisition.Text = Convert.ToString(InvestCost + TotalExpenses);
            TxtTotalAcquisition.EditValue = Sm.FormatNum(Decimal.Parse(TxtTotalAcquisition.Text), 0);
            //Sm.FormatNumTxt(TxtTotalAcquisition, 0);
        }

        internal void ComputeAccruedInterest()
        {
            decimal NominalAmt = 0m, CouponInterestRate = 0m, AnnualDays = 0m;
            //string SettlementDt = string.Empty, LastCouponDt = string.Empty;

            NominalAmt = Decimal.Parse(TxtAmt.Text);
            CouponInterestRate = Decimal.Parse(TxtCouponInterestRate.Text);
            if(TxtAnnualDays.Text != "")
                AnnualDays = Decimal.Parse(TxtAnnualDays.Text);
            //SettlementDt = Sm.Left(Sm.GetDte(DteSettlementDt), 8);
            //LastCouponDt = Sm.Left(Sm.GetDte(DteLastCouponDt), 8);
            //TimeSpan diff = LastCouponDt.Substract(SettlementDt); 
            //TimeSpan a = Sm.DateValue(SettlementDt) - Sm.DateValue(LastCouponDt);
            DateTime SettlementDt = DteSettlementDt.DateTime;
            DateTime LastCouponDt = DteLastCouponDt.DateTime;
            TimeSpan diff = SettlementDt - LastCouponDt;
            decimal c = Convert.ToDecimal(diff.TotalDays);

            TxtAccruedInterest.Text = Convert.ToString(Sm.Round(NominalAmt * (CouponInterestRate/100) / AnnualDays * Math.Abs(c), 2));
            if(Grd1.Rows.Count > 1)
            {
                //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                //{
                //    decimal AccruedInterest = Decimal.Parse(TxtAccruedInterest.Text);

                    //if (Sm.GetGrdStr(Grd1, Row, 14) == "2")
                    //    TxtAccruedInterest.Text = Convert.ToString(AccruedInterest * Sm.GetGrdDec(Grd1, Row, 10));
                //}
                ComputeAmountDtlExpenses();
                ComputeTotalExpenses();
            }
            ComputeTotalInvestment();
            TxtAccruedInterest.EditValue = Sm.FormatNum(Decimal.Parse(TxtAccruedInterest.Text), 0);
        }

        internal void ComputeInvestCost()
        {
            decimal NominalAmt = 0m, AcquisitionPrice = 0m, InvestCost = 0m;

            NominalAmt = Decimal.Parse(TxtAmt.Text);
            AcquisitionPrice = Decimal.Parse(TxtAcquisitionPrice.Text) * 0.01m;
            InvestCost = NominalAmt * AcquisitionPrice;
            TxtInvestmentCost.EditValue = Sm.FormatNum(InvestCost, 0);
        }

        private void GetParameter()
        {
            mBankAccountTypeForInvestment = Sm.GetParameter("BankAccountTypeForInvestment");
        }

        internal void SetLueFinancialInstitutionCode(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SekuritasCode Col1, SekuritasName Col2 ");
                SQL.AppendLine("From TblInvestmentSekuritas ");
                if (Code.Length > 0)
                    SQL.AppendLine("Where SekuritasCode = @Code; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueInvestmentType(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode Col1, OptDesc Col2 ");
                SQL.AppendLine("From TblOption ");
                if (Code.Length > 0)
                    SQL.AppendLine("Where OptCat = 'InvestmentType' AND OptCode = @Code; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSettlementDate(string Dte2)
        {
            String mTradeDt = Sm.Left(Dte2, 8);
            String mSettlementDt = Sm.ConvertDate(mTradeDt).AddDays(2).ToString("yyyyMMdd");
            Sm.SetDte(DteSettlementDt, mSettlementDt);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFinancialInstitution_EditValueChanged(object sender, EventArgs e)
        {
            string code = Sm.GetLue(LueFinancialInstitution);
            Sm.RefreshLookUpEdit(LueFinancialInstitution, new Sm.RefreshLue2(SetLueFinancialInstitutionCode), string.Empty);
        }

        private void BtnInvestmentCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAcquisitionDebtSecuritiesDlg2(this));
        }

        private void BtnInvestmentBankAcc_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAcquisitionDebtSecuritiesDlg(this));
        }

        private void BtnInvestmentName_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAcquisitionDebtSecuritiesDlg2(this));
        }

        private void TxtCurCode_EditValueChanged(object sender, EventArgs e)
        {
            var Rate = string.Empty;
            if (TxtCurCode.Text.Length > 0)
                if (TxtCurCode.Text == "IDR")
                    TxtCurRate.Text = "1";
                else
                {
                    Rate = Sm.GetValue("SELECT Amt " +
                        "FROM tblcurrencyrate WHERE CurCode1 = '" + TxtCurCode.Text + "' AND CurCode2 = 'IDR' " +
                        "AND RateDt = (SELECT MAX(RateDt) FROM tblcurrencyrate) ");
                    TxtCurRate.Text = Rate;
                }
            Sm.FormatNumTxt(TxtCurRate, 0);
        }

        private void LueInvestmentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentType, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentType");
        }

        private void DteTradeDt_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteTradeDt).Length > 0)
                SetSettlementDate(Sm.GetDte(DteTradeDt));
        }

        private void TxtAmt_EditValueChanged(object sender, EventArgs e)
        {
            //if (TxtAmt.Text.Length != 0 && TxtAcquisitionPrice.Text.Length != 0 && BtnSave.Enabled && TxtInvestmentCode.Text != "")
            //    ComputeInvestCost();
            //if (TxtAmt.Text.Length != 0 && TxtAcquisitionPrice.Text.Length != 0 && BtnSave.Enabled && TxtInvestmentCode.Text != "")
            //    ComputeAccruedInterest();
            if (TxtAmt.Text != "")
                TxtAmt.EditValue = Sm.FormatNum(Decimal.Parse(TxtAmt.Text), 0);
        }

        private void TxtAcquisitionPrice_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FormatNumTxt(TxtAcquisitionPrice, 0);
            //if (TxtAcquisitionPrice.Text.Length != 0 && TxtAmt.Text.Length != 0 && BtnSave.Enabled)
            //    ComputeInvestCost();
            if (TxtAcquisitionPrice.Text != "")
                TxtAcquisitionPrice.EditValue = Sm.FormatNum(Decimal.Parse(TxtAcquisitionPrice.Text), 0);
        }

        private void TxtInvestmentCode_EditValueChanged(object sender, EventArgs e)
        {
            //ComputeAccruedInterest();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmAcquisitionDebtSecuritiesDlg3(this, e.RowIndex));
            if (e.ColIndex == 3)
                Sm.FormShowDialog(new FrmAcquisitionDebtSecuritiesDlg4(this));
        }

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 &&
                !Sm.IsTxtEmpty(TxtInvestmentCode, "Investment", false) &&
                !Sm.IsTxtEmpty(TxtInvestmentName, "Investment", false))
            {
                GetExpensesData();
                //ComputeAccruedInterest();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void DteExpensesDocDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteExpensesDocDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteExpensesDocDt, ref fCell, ref fAccept);
        }

        private void TxtInvestmentCost_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FormatNum(Decimal.Parse(TxtInvestmentCost.Text), 0);
            //Sm.FormatNumTxt(TxtInvestmentCost, 0);
        }

        private void TxtCouponInterestRate_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCouponInterestRate, 0);
        }

        private void TxtAnnualDays_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FormatNumTxt(TxtAnnualDays, 0);
            if (TxtAnnualDays.Text != "")
                TxtAnnualDays.EditValue = Sm.FormatNum(Decimal.Parse(TxtAnnualDays.Text), 0);
        }

        private void TxtCurRate_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCurRate, 0);
        }

        private void TxtAccruedInterest_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FormatNumTxt(TxtAccruedInterest, 0);
        }

        private void TxtTotalExpenses_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FormatNumTxt(TxtTotalExpenses, 0);
        }

        private void TxtTotalAcquisition_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FormatNumTxt(TxtTotalAcquisition, 0);
        }

        private void DteSettlementDt_EditValueChanged(object sender, EventArgs e)
        {
            if (TxtInvestmentCode.Text.Length != 0 && Sm.GetDte(DteSettlementDt) != string.Empty && BtnSave.Enabled)
                ComputeAccruedInterest();
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) ||
                ChkCancelInd.Checked) return;
            SetFormControl(mState.Edit);
        }

        private void DteLastCouponDt_Validated(object sender, EventArgs e)
        {
            ComputeAccruedInterest();
        }

        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            
        }

        private void TxtAcquisitionPrice_Validated(object sender, EventArgs e)
        {
            if (TxtAcquisitionPrice.Text.Length != 0 && TxtAmt.Text.Length != 0 && BtnSave.Enabled)
                ComputeInvestCost();
            if (TxtAmt.Text.Length != 0 && TxtAcquisitionPrice.Text.Length != 0 && BtnSave.Enabled && TxtInvestmentCode.Text != "")
                ComputeAccruedInterest();
        }

        private void DteNextCouponDt_Validated(object sender, EventArgs e)
        {
            ComputeAccruedInterest();
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (TxtAmt.Text.Length != 0 && TxtAcquisitionPrice.Text.Length != 0 && BtnSave.Enabled && TxtInvestmentCode.Text != "")
                ComputeInvestCost();
            if (TxtAmt.Text.Length != 0 && TxtAcquisitionPrice.Text.Length != 0 && BtnSave.Enabled && TxtInvestmentCode.Text != "")
                ComputeAccruedInterest();
            //Sm.FormatNum(TxtInvestmentCost.Text, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }



        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 11 });
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 7) Sm.DteRequestEdit(Grd1, DteExpensesDocDt, ref fCell, ref fAccept, e);

                //if (Sm.IsGrdColSelected(new int[] { 2, 5, 6, 7, 8 }, e.ColIndex))
                //{
                //    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                //    IsExpensesCodeExists();
                //}
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 10, 11 });
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {

            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 10, 11 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { }, e);
                if (Grd1.Rows.Count > 1)
                {
                    //ComputeAccruedInterest();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            //Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
