﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFrequencyRange : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmFrequencyRangeFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmFrequencyRange(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtFrequencyRangeCode, TxtFrequencyRangeName, TxtStartMHz, TxtEndMHz, ChkActInd }, true);
                    TxtFrequencyRangeCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtFrequencyRangeCode, TxtFrequencyRangeName, TxtStartMHz, TxtEndMHz }, false);
                    TxtFrequencyRangeCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtFrequencyRangeName, TxtStartMHz, TxtEndMHz, ChkActInd }, false);
                    TxtFrequencyRangeName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtFrequencyRangeCode, TxtFrequencyRangeName });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtStartMHz, TxtEndMHz }, 1 );
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFrequencyRangeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFrequencyRangeCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFrequencyRangeCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblFrequencyRange Where FrequencyRangeCode=@FrequencyRangeCode" };
                Sm.CmParam<String>(ref cm, "@FrequencyRangeCode", TxtFrequencyRangeCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblFrequencyRange(FrequencyRangeCode, FrequencyRangeName, StartMHz, EndMHz, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@FrequencyRangeCode, @FrequencyRangeName, @StartMHz, @EndMHz, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update FrequencyRangeName=@FrequencyRangeName, StartMHz=@StartMHz, EndMHz=@EndMHz, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FrequencyRangeCode", TxtFrequencyRangeCode.Text);
                Sm.CmParam<String>(ref cm, "@FrequencyRangeName", TxtFrequencyRangeName.Text);
                Sm.CmParam<Decimal>(ref cm, "@StartMHz", decimal.Parse(TxtStartMHz.Text));
                Sm.CmParam<Decimal>(ref cm, "@EndMHz", decimal.Parse(TxtEndMHz.Text));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtFrequencyRangeCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string FrequencyRangeCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@FrequencyRangeCode", FrequencyRangeCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select FrequencyRangeCode, FrequencyRangeName, StartMHz, EndMHz, ActInd From TblFrequencyRange Where FrequencyRangeCode=@FrequencyRangeCode;",
                        new string[] { "FrequencyRangeCode", "FrequencyRangeName", "StartMHz", "EndMHz", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtFrequencyRangeCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtFrequencyRangeName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtStartMHz.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 1);
                            TxtEndMHz.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 1);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFrequencyRangeCode, "Frequency range code", false) ||
                Sm.IsTxtEmpty(TxtFrequencyRangeName, "Frequency range name", false) ||
                Sm.IsTxtEmpty(TxtStartMHz, "Start MHz", true) ||
                Sm.IsTxtEmpty(TxtEndMHz, "End MHz", true) ||
                IsCodeAlreadyExisted() ||
                IsMHzNotValid();
        }

        private bool IsMHzNotValid()
        { 
            var StartMHz = decimal.Parse(TxtStartMHz.Text);
            var EndMHz = decimal.Parse(TxtEndMHz.Text);
            if (StartMHz > EndMHz)
            {
                Sm.StdMsg(mMsgType.Warning, "Start MHz is bigger than end MHz.");
                TxtStartMHz.Focus();
                return true;
            }
            return false;
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtFrequencyRangeCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select FrequencyRangeCode From TblFrequencyRange Where FrequencyRangeCode=@Param;",
                    TxtFrequencyRangeCode.Text,
                    "Frequency range code ( " + TxtFrequencyRangeCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtFrequencyRangeCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFrequencyRangeCode);
        }

        private void TxtFrequencyRangeName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFrequencyRangeName);
        }

        private void TxtStartMHz_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtStartMHz, 1);
        }

        private void TxtEndMHz_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEndMHz, 1);
        }

        #endregion

        #endregion
    }
}
