﻿#region Update
/*
    21/08/2019 [TKG] New reporting for DO to department (no request)
    09/09/2019 [TKG] bug saat menampilkan quantity di bulan 2-4
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptQuarterlyDODept : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mMth1 = string.Empty, mYr1 = string.Empty,
            mMth2 = string.Empty, mYr2 = string.Empty,
            mMth3 = string.Empty, mYr3 = string.Empty,
            mMth4 = string.Empty, mYr4 = string.Empty;

        #endregion

        #region Constructor

        public FrmRptQuarterlyDODept(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.ItCode, T2.ItName, T2.InventoryUomCode, ");
            SQL.AppendLine("T1.Qty1, T1.Amt1, "); 
            SQL.AppendLine("T1.Qty2, T1.Amt2, "); 
            SQL.AppendLine("T1.Qty3, T1.Amt3, "); 
            SQL.AppendLine("T1.Qty4, T1.Amt4 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, "); 
            SQL.AppendLine("    Sum(Qty1) AS Qty1, Sum(Amt1) AS Amt1, "); 
            SQL.AppendLine("    Sum(Qty2) AS Qty2, Sum(Amt2) AS Amt2, "); 
            SQL.AppendLine("    Sum(Qty3) AS Qty3, Sum(Amt3) AS Amt3, "); 
            SQL.AppendLine("    Sum(Qty4) AS Qty4, Sum(Amt4) AS Amt4 "); 
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select B.ItCode, "); 
            SQL.AppendLine("        B.Qty AS Qty1, B.Qty*C.UPrice*C.ExcRate As Amt1, "); 
            SQL.AppendLine("        0.00 As Qty2, 0.00 As Amt2, ");
            SQL.AppendLine("        0.00 As Qty3, 0.00 As Amt3, ");
            SQL.AppendLine("        0.00 As Qty4, 0.00 As Amt4 ");
            SQL.AppendLine("        From TblDODeptHdr A "); 
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Where A.DORequestDeptDocNo Is Null "); 
            SQL.AppendLine("        And A.WODocNo Is Null "); 
            SQL.AppendLine("        And A.WhsCode=@WhsCode "); 
            SQL.AppendLine("        And Left(A.DocDt, 6)=@YrMth1 ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.ItCode, "); 
            SQL.AppendLine("        0.00 As Qty1, 0.00 As Amt1, "); 
            SQL.AppendLine("        B.Qty AS Qty2, B.Qty*C.UPrice*C.ExcRate As Amt2, "); 
            SQL.AppendLine("        0.00 As Qty3, 0.00 As Amt3, "); 
            SQL.AppendLine("        0.00 As Qty4, 0.00 As Amt4 "); 
            SQL.AppendLine("        From TblDODeptHdr A "); 
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' "); 
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source "); 
            SQL.AppendLine("        Where A.DORequestDeptDocNo Is Null "); 
            SQL.AppendLine("        And A.WODocNo Is Null "); 
            SQL.AppendLine("        And A.WhsCode=@WhsCode "); 
            SQL.AppendLine("        And Left(A.DocDt, 6)=@YrMth2 "); 
            SQL.AppendLine("        Union All "); 
            SQL.AppendLine("        Select B.ItCode, ");  
            SQL.AppendLine("        0.00 As Qty1, 0.00 As Amt1, "); 
            SQL.AppendLine("        0.00 As Qty2, 0.00 As Amt2, "); 
            SQL.AppendLine("        B.Qty AS Qty3, B.Qty*C.UPrice*C.ExcRate As Amt3, "); 
            SQL.AppendLine("        0.00 As Qty4, 0.00 As Amt4 "); 
            SQL.AppendLine("        From TblDODeptHdr A "); 
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' "); 
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source "); 
            SQL.AppendLine("        Where A.DORequestDeptDocNo Is Null ");  
            SQL.AppendLine("        And A.WODocNo Is Null ");  
            SQL.AppendLine("        And A.WhsCode=@WhsCode ");  
            SQL.AppendLine("        And Left(A.DocDt, 6)=@YrMth3 "); 
            SQL.AppendLine("        Union All "); 
            SQL.AppendLine("        Select B.ItCode, "); 
            SQL.AppendLine("        0.00 As Qty1, 0.00 As Amt1, "); 
            SQL.AppendLine("        0.00 As Qty2, 0.00 As Amt2, "); 
            SQL.AppendLine("        0.00 As Qty3, 0.00 As Amt3, "); 
            SQL.AppendLine("        B.Qty AS Qty4, B.Qty*C.UPrice*C.ExcRate As Amt4 "); 
            SQL.AppendLine("        From TblDODeptHdr A "); 
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' "); 
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Where A.DORequestDeptDocNo Is Null "); 
            SQL.AppendLine("        And A.WODocNo Is Null ");
            SQL.AppendLine("        And A.WhsCode=@WhsCode ");
            SQL.AppendLine("        And Left(A.DocDt, 6)=@YrMth4 ");
            SQL.AppendLine("    ) T Group By T.ItCode "); 
            SQL.AppendLine(") T1  ");
            SQL.AppendLine("Inner Join TblItem T2 ON T1.ItCode=T2.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            var CurrentDate = Sm.ServerCurrentDateTime();
            string Mth1 = string.Empty, Mth2 = string.Empty, Mth3 = string.Empty, Mth4 = string.Empty; 

            mMth4 = CurrentDate.Substring(4, 2);
            mYr4 = Sm.Left(CurrentDate, 4);

            if (Sm.CompareStr(mMth4, "01"))
            {
                mMth3 = "12";
                mYr3 = (int.Parse(mYr4) - 1).ToString();
            }
            else
            {
                mMth3 = Sm.Right("0"+(int.Parse(mMth4)-1).ToString(), 2);
                mYr3 = mYr4;
            }

            if (Sm.CompareStr(mMth3, "01"))
            {
                mMth2 = "12";
                mYr2 = (int.Parse(mYr3) - 1).ToString();
            }
            else
            {
                mMth2 = Sm.Right("0" + (int.Parse(mMth3) - 1).ToString(), 2);
                mYr2 = mYr3;
            }

            if (Sm.CompareStr(mMth2, "01"))
            {
                mMth1 = "12";
                mYr1 = (int.Parse(mYr2) - 1).ToString();
            }
            else
            {
                mMth1 = Sm.Right("0" + (int.Parse(mMth2) - 1).ToString(), 2);
                mYr1 = mYr2;
            }

            Mth1 = string.Concat(Sm.GetMonthName(int.Parse(mMth1)), " ");
            Mth2 = string.Concat(Sm.GetMonthName(int.Parse(mMth2)), " ");
            Mth3 = string.Concat(Sm.GetMonthName(int.Parse(mMth3)), " ");
            Mth4 = string.Concat(Sm.GetMonthName(int.Parse(mMth4)), " ");

            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code", 
                        "Item's Name",
                        "UoM",
                        string.Concat(Mth1, mYr1) + Environment.NewLine + "Quantity",
                        string.Concat(Mth1, mYr1) + Environment.NewLine + "Amount",

                        //6-10
                        string.Concat(Mth2, mYr2) + Environment.NewLine + "Quantity",
                        string.Concat(Mth2, mYr2) + Environment.NewLine + "Amount",
                        string.Concat(Mth3, mYr3) + Environment.NewLine + "Quantity",
                        string.Concat(Mth3, mYr3) + Environment.NewLine + "Amount",
                        string.Concat(Mth4, mYr4) + Environment.NewLine + "Quantity",

                        //11
                        string.Concat(Mth4, mYr4) + Environment.NewLine + "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 120, 130, 
                        
                        //6-10
                        120, 130, 120, 130, 120, 
                        
                        //11
                        130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@YrMth1", string.Concat(mYr1, mMth1));
                Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(mYr2, mMth2));
                Sm.CmParam<String>(ref cm, "@YrMth3", string.Concat(mYr3, mMth3));
                Sm.CmParam<String>(ref cm, "@YrMth4", string.Concat(mYr4, mMth4));
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "InventoryUomCode", "Qty1", "Amt1", "Qty2", 
                            
                            //6-10
                            "Amt2", "Qty3", "Amt3", "Qty4", "Amt4"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        #endregion

        #endregion
    }
}
