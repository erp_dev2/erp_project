﻿#region Update
/*
    14/01/2020 [WED/VIR] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSiteTargetSOC : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mYr = string.Empty;
        internal bool mIsFilterBySite = false;
        private string CurrentDateTime = Sm.ServerCurrentDateTime();
        iGCell fCell;
        bool fAccept;
        internal FrmSiteTargetSOCFind FrmFind;

        #endregion

        #region Constructor

        public FrmSiteTargetSOC(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueYr(LueYr, string.Empty);
                SetGrd();
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                LueSiteCode.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "SiteCode", 

                    //1-2
                    "Site", "Amount"
                },
                 new int[] 
                {
                    //0
                    0,

                    //1-2
                    200, 120
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueYr }, true);
                    LueYr.Focus();
                    Grd1.ReadOnly = true;
                    BtnProcess.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueYr }, false);
                    LueYr.Focus();
                    Grd1.ReadOnly = false;
                    BtnProcess.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueYr }, false);
                    Grd1.ReadOnly = false;
                    LueYr.Focus();
                    BtnProcess.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueYr });
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSiteTargetSOCFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string Yr)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowYr(Yr);
                ShowSiteTargetSOC(Yr);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowYr(string Yr)
        {
            Sm.SetLue(LueYr, Yr);
            LueYr.Focus();
        }

        private void ShowSiteTargetSOC(string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SiteCode, B.SiteName, A.Amt ");
            SQL.AppendLine("From TblSiteTargetSOC A ");
            SQL.AppendLine("Inner Join TblSite B On A.SiteCode = B.SiteCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblGroupSite C On B.SiteCode = C.SiteCode ");
                SQL.AppendLine("And C.GrpCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select GrpCode ");
                SQL.AppendLine("    From TblUser ");
                SQL.AppendLine("    Where UserCode = @UserCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Where A.Yr = @Yr ");
            SQL.AppendLine("Order By A.SiteCode; ");

            var cmDtl = new MySqlCommand();
            Sm.CmParam<String>(ref cmDtl, "@Yr", Yr);
            Sm.CmParam<String>(ref cmDtl, "@UserCode", Gv.CurrentUserCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cmDtl,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "SiteCode", 

                    //1-2
                    "SiteName", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsSiteDuplicated();
        }

        private bool IsSiteDuplicated()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                for (int j = (i + 1); j < Grd1.Rows.Count - 1; ++j)
                {
                    if (Sm.GetGrdStr(Grd1, i, 0) == Sm.GetGrdStr(Grd1, j, 0))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Site duplicated.");
                        Sm.FocusGrd(Grd1, j, 1);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 site.");
                return true;
            }

            return false;
        }

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;
            
            string mExistingData = string.Empty;
            mExistingData = GetExistingData(Sm.GetLue(LueYr));
            if (mExistingData.Length > 0)
            {
                if (Sm.StdMsgYN("Question", "Data for this year is exists. Do you want to update it ?") == DialogResult.No) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveSiteTargetSOC(Row));

            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueYr));
        }

        private MySqlCommand SaveSiteTargetSOC(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSiteTargetSOC(SiteCode, Yr, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SiteCode, @Yr, @Amt, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update ");
            SQL.AppendLine("    Amt = @Amt, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd1, LueSiteCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }

        #endregion

        #region Additional Method

        private void GetAllSite()
        {
            string mSiteCode = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int mCurrRowIndex = Grd1.Rows.Count;

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (mSiteCode.Length > 0) mSiteCode += ",";
                    mSiteCode += Sm.GetGrdStr(Grd1, i, 0);
                }
            }

            SQL.AppendLine("Select SiteCode, SiteName ");
            SQL.AppendLine("From TblSite ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if(mSiteCode.Length > 0)
                SQL.AppendLine("And Not Find_In_Set(SiteCode, @SiteCode) ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite T ");
                SQL.AppendLine("        Where T.SiteCode=IfNull(SiteCode, '') ");
                SQL.AppendLine("        And T.GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("Order By SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        //Sm.DrStr(dr, c[0]),
                        Grd1.Rows.Add();

                        Grd1.Cells[mCurrRowIndex - 1, 0].Value = Sm.DrStr(dr, c[0]);
                        Grd1.Cells[mCurrRowIndex - 1, 1].Value = Sm.DrStr(dr, c[1]);
                        Grd1.Cells[mCurrRowIndex - 1, 2].Value = Sm.FormatNum(0m, 0);

                        mCurrRowIndex += 1;
                    }
                }
                dr.Close();
            }
        }

        private string GetExistingData(string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSiteTargetSOC ");
            SQL.AppendLine("Where Yr = @Param ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), Yr);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
            }
        }

        private void LueSiteCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd1, ref fAccept, e);
            }
        }

        private void LueSiteCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueSiteCode.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueSiteCode).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 0].Value =
                        Grd1.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueSiteCode);
                        Grd1.Cells[fCell.RowIndex, 1].Value = LueSiteCode.GetColumnValue("Col2");
                    }
                    LueSiteCode.Visible = false;
                }
            }
        }

        #endregion

        #region Button Clicks

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                GetAllSite();
            }
        }

        #endregion

        #endregion
    }
}
