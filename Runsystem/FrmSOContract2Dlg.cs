﻿#region Update
/*
    05/11/2019 [WED/IMS] tambah kolom SPPH (Quotation Letter#) dari LOP
    26/11/2019 [DITA/IMS] tambah kolom project code, project name, ntp docno
    18/02/2020 [TKG/IMS] tambah term of payment
    20/02/2020 [WED/IMS] bikin revision
    22/04/2021 [TKG/IMS] ubah periode filter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContract2 mFrmParent;
        private string mSQL = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract2Dlg(FrmSOContract2 FrmParent, string CtCode)
        {
            InitializeComponent();
            this.Text = "List Bill of Quantity";
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1200);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CtCode, B.CtName, A.Remark, A.CtContactPersonName As CtCntCode, C.QtLetterNo, ");
            SQL.AppendLine("D.ProjectCode, D.ProjectName, F.DocNo As NTPDocNo, A.PtCode, G.PtName ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode And A.DocType = '2' ");
            SQL.AppendLine("Inner Join TblLOPHdr C On C.DocNo = A.LOPDocNo ");
            SQL.AppendLine("Left Join TblProjectGroup D On D.PGCode = C.PGCode ");
            SQL.AppendLine("Left Join TblSOContractHdr E On A.DocNo = E.BOQDocNo ");
            SQL.AppendLine("Left Join TblNoticeToProceed F On C.DocNo = F.LOPDocNo ");
            SQL.AppendLine("Left Join TblPaymentTerm G On A.PtCode=G.PtCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' And A.Status = 'A'  ");
            SQL.AppendLine("And A.CtCode=@CtCode And A.DocNo not In ( ");
            SQL.AppendLine("  Select BOQDocNo From TblSOContracthdr Where CancelInd = 'N' And Status In ('O', 'A') ");
            SQL.AppendLine(") And C.ProcessInd In ('P', 'L') And C.Status = 'A' And C.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer",
                    "Contact Person",

                    //6-10
                    "Quotation Letter#",
                    "Notice To Proceed#",
                    "Project's Code",
                    "Project's Name",
                    "Payment Term Code",

                    //11-12
                    "Term of Payment",
                    "Remark"
                },
                new int[]
                {
                    50, 
                    150, 20, 80, 200, 0, 
                    150, 150, 130, 200, 0,
                    200, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtQtLetterNo.Text, "C.QtLetterNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            "DocNo", 
                            "DocDt", "CtName", "CtCntCode", "QtLetterNo", "NTPDocNo",
                            "ProjectCode", "ProjectName", "PtCode", "PtName", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            string ContactPerson = string.Empty;
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtBOQDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtCustomerContactperson.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                mFrmParent.ShowBOQService(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ShowBOQInventory(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                Sm.SetLue(mFrmParent.LuePtCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10));
                mFrmParent.ComputeTotalBOQ(mFrmParent.IsInsert);
                mFrmParent.ComputeTotalBOM(mFrmParent.IsInsert);
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBOQ(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmBOQ(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtQtLetterNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQtLetterNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Quotation Letter#");
        }

        #endregion

        #endregion
    }
}
