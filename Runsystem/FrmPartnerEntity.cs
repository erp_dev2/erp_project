﻿#region Update 
/*
 *      15/12/2022 [ICA/MNET] New Apps
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPartnerEntity : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmPartnerEntityFind FrmFind;

        #endregion

        #region Constructor

        public FrmPartnerEntity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            Sl.SetLueCtCode(ref LueCtCode);
            Sl.SetLueVdCode(ref LueVdCode);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtPartnerEntityCode, TxtPartnerEntityName, LueCtCode, LueVdCode, ChkActInd
                    }, true);
                    TxtPartnerEntityCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                         TxtPartnerEntityName, LueCtCode, LueVdCode
                    }, false);
                    TxtPartnerEntityName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        ChkActInd
                    }, false);
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtPartnerEntityCode, TxtPartnerEntityName, LueCtCode, LueVdCode
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPartnerEntityFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            ChkActInd.Checked = true;

            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPartnerEntityCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtPartnerEntityCode.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PartnerEntityCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PartnerEntityCode", PartnerEntityCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblPartnerEntity Where PartnerEntityCode = @PartnerEntityCode",
                        new string[]
                        {
                            "PartnerEntityCode",
                            "PartnerEntityName", "ActInd", "CtCode", "VdCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPartnerEntityCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPartnerEntityName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y"; 
                            Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[4]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPartnerEntityName, "Partner Entity", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor");
        }

        private void InsertData(object sender, EventArgs e)
        {
            if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string PartnerEntityCode = string.Empty;

            if (TxtPartnerEntityCode.Text.Length == 0)
                PartnerEntityCode = GeneratePartnerEntityCode();

            var cml = new List<MySqlCommand>();
            cml.Add(SavePartnerEntity(PartnerEntityCode));

            Sm.ExecCommands(cml);
            ShowData(PartnerEntityCode);
        }

        private MySqlCommand SavePartnerEntity(string PartnerEntityCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblPartnerEntity(PartnerEntityCode, PartnerEntityName, ActInd, CtCode, VdCode, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@PartnerEntityCode, @PartnerEntityName, @ActInd, @CtCode, @VdCode, @UserCode, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PartnerEntityCode", PartnerEntityCode);
            Sm.CmParam<String>(ref cm, "@PartnerEntityName", TxtPartnerEntityName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", "Y");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GeneratePartnerEntityCode()
        {
            var SQL = new StringBuilder();
            int lenPartnerEntityCode = 5;

            var MaxPartnerEntityCode = Sm.GetValue("SELECT MAX(LENGTH(PartnerEntityCode)) FROM TblPartnerEntity");
            if (MaxPartnerEntityCode.Length > 0)
                lenPartnerEntityCode = Convert.ToInt32(MaxPartnerEntityCode);

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            SQL.AppendLine("    Select Cast(IfNull(Max(PartnerEntityCode), '00000') As UNSIGNED)+1 As Value ");
            SQL.AppendLine("    From TblPartnerEntity ");
            SQL.AppendLine("    WHERE LENGTH(PartnerEntityCode) = " + lenPartnerEntityCode + " ");
            SQL.AppendLine("    Group By PartnerEntityCode ");
            SQL.AppendLine("    Order By PartnerEntityCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), " + lenPartnerEntityCode + ") As PartnerEntityCode");

            return Sm.GetValue(SQL.ToString());
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            string PartnerEntityCode = TxtPartnerEntityCode.Text;
            cml.Add(EditPartnerEntity(PartnerEntityCode));

            Sm.ExecCommands(cml);
            ShowData(PartnerEntityCode);
        }

        private MySqlCommand EditPartnerEntity(string PartnerEntityCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblPartnerEntity ");
            SQL.AppendLine("Set ActInd = @ActInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where PartnerEntityCode = @PartnerEntityCode ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@PartnerEntityCode", PartnerEntityCode);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsDataInActiveAlready() ||
                IsDataActive();
        }

        private bool IsDataActive()
        {
            if (ChkActInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to inactive this data.");
                return true;
            }
            return false;
        }

        private bool IsDataInActiveAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PartnerEntityCode From TblPartnerEntity ");
            SQL.AppendLine("Where ActInd='N' And PartnerEntityCode = @PartnerEntityCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PartnerEntityCode", TxtPartnerEntityCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive.");
                return true;
            }

            return false;
        }

        #endregion

        #region Additional Method


        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtPartnerEntityCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPartnerEntityCode);
        }
        private void TxtPartnerEntityName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPartnerEntityName);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        }

        #endregion

        #endregion
    }
}
