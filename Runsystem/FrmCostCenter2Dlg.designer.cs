﻿namespace RunSystem
{
    partial class FrmCostCenter2Dlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkAcNo = new DevExpress.XtraEditors.CheckEdit();
            this.ChkAcCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtAccount = new DevExpress.XtraEditors.TextEdit();
            this.ChkAccount = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAccount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkAcCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueAcCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtAccount);
            this.panel2.Controls.Add(this.ChkAccount);
            this.panel2.Controls.Add(this.ChkAcNo);
            this.panel2.Size = new System.Drawing.Size(672, 55);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 418);
            this.Grd1.TabIndex = 16;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkAcNo
            // 
            this.ChkAcNo.Location = new System.Drawing.Point(258, 3);
            this.ChkAcNo.Name = "ChkAcNo";
            this.ChkAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcNo.Properties.Appearance.Options.UseFont = true;
            this.ChkAcNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcNo.Properties.Caption = " ";
            this.ChkAcNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcNo.Size = new System.Drawing.Size(31, 22);
            this.ChkAcNo.TabIndex = 27;
            this.ChkAcNo.ToolTip = "Remove filter";
            this.ChkAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcNo.ToolTipTitle = "Run System";
            // 
            // ChkAcCode
            // 
            this.ChkAcCode.Location = new System.Drawing.Point(391, 25);
            this.ChkAcCode.Name = "ChkAcCode";
            this.ChkAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcCode.Properties.Appearance.Options.UseFont = true;
            this.ChkAcCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcCode.Properties.Caption = " ";
            this.ChkAcCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcCode.Size = new System.Drawing.Size(31, 22);
            this.ChkAcCode.TabIndex = 15;
            this.ChkAcCode.ToolTip = "Remove filter";
            this.ChkAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcCode.ToolTipTitle = "Run System";
            this.ChkAcCode.CheckedChanged += new System.EventHandler(this.ChkAcCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(21, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAcCode
            // 
            this.LueAcCode.EnterMoveNextControl = true;
            this.LueAcCode.Location = new System.Drawing.Point(66, 26);
            this.LueAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcCode.Name = "LueAcCode";
            this.LueAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcCode.Properties.DropDownRows = 25;
            this.LueAcCode.Properties.NullText = "[Empty]";
            this.LueAcCode.Properties.PopupWidth = 500;
            this.LueAcCode.Size = new System.Drawing.Size(321, 20);
            this.LueAcCode.TabIndex = 14;
            this.LueAcCode.ToolTip = "F4 : Show/hide list";
            this.LueAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcCode.EditValueChanged += new System.EventHandler(this.LueAcCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Account";
            // 
            // TxtAccount
            // 
            this.TxtAccount.EnterMoveNextControl = true;
            this.TxtAccount.Location = new System.Drawing.Point(66, 5);
            this.TxtAccount.Name = "TxtAccount";
            this.TxtAccount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAccount.Properties.Appearance.Options.UseFont = true;
            this.TxtAccount.Properties.MaxLength = 250;
            this.TxtAccount.Size = new System.Drawing.Size(321, 20);
            this.TxtAccount.TabIndex = 11;
            this.TxtAccount.Validated += new System.EventHandler(this.TxtAccount_Validated);
            // 
            // ChkAccount
            // 
            this.ChkAccount.Location = new System.Drawing.Point(391, 3);
            this.ChkAccount.Name = "ChkAccount";
            this.ChkAccount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAccount.Properties.Appearance.Options.UseFont = true;
            this.ChkAccount.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAccount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAccount.Properties.Caption = " ";
            this.ChkAccount.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAccount.Size = new System.Drawing.Size(31, 22);
            this.ChkAccount.TabIndex = 12;
            this.ChkAccount.ToolTip = "Remove filter";
            this.ChkAccount.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAccount.ToolTipTitle = "Run System";
            this.ChkAccount.CheckedChanged += new System.EventHandler(this.ChkAccount_CheckedChanged);
            // 
            // FrmCostCenter2Dlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmCostCenter2Dlg";
            this.Text = "List of COA\'s Account";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAccount.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkAcNo;
        private DevExpress.XtraEditors.CheckEdit ChkAcCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueAcCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtAccount;
        private DevExpress.XtraEditors.CheckEdit ChkAccount;
    }
}