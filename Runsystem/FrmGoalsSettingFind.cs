﻿#region Update
/*
    19/04/2022 [TYO/HIN] New Apps
    22/11/2022 [RDA/HIN] perubahan beberapa nama kolom
    07/12/2022 [WED/HIN] tambah informasi status cancelled
 */
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmGoalsSettingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmGoalsSetting mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGoalsSettingFind(FrmGoalsSetting FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ActInd, A.GoalsName, A.PICCode, B.EmpName AS PICName,   ");
            SQL.AppendLine("E.OptDesc AS DirectorateName, A.EvaluatorCode, C.EmpName AS EvaluatorName, A.TopEvaluatorCode,  ");
            SQL.AppendLine("D.EmpName As TopEvaluatorName, A.Remark,   ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("	when A.`Status` = 'O' then 'Outstanding'  ");
            SQL.AppendLine("    when A.`Status` = 'C' then 'Cancelled' ");
            SQL.AppendLine("	when A.`Status` = 'A' then 'Approved'  ");
            SQL.AppendLine("END AS `Status`, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpdt  ");
            SQL.AppendLine("FROM TblGoalsSettingHdr A  ");
            SQL.AppendLine("LEFT JOIN tblemployee B ON B.EmpCode = A.PICCode  ");
            SQL.AppendLine("LEFT JOIN tblemployee C ON C.EmpCode = A.EvaluatorCode ");
            SQL.AppendLine("LEFT JOIN tblemployee D ON D.EmpCode = A.TopEvaluatorCode ");
            SQL.AppendLine("INNER JOIN tbloption E ON E.OptCode = A.DirectorateCode AND E.OptCat = 'GoalsSettingDirectorate'  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document", "Date", "Goal Setting", "Active", "Employee Code",

                        //6-10
                        "Employee Name", "Directorate", "Appraiser 1 Code", "Appraiser 1 Name", "Appraiser 2 "+Environment.NewLine+"Code",

                        //11-13
                        "Appraiser 2 "+Environment.NewLine+"Name", "Remark", "Status"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 150, 60, 100, 
                        
                        //6-10
                        150, 150, 100, 200, 100, 
                        
                        //11-13
                        200, 200, 100

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
            Cursor.Current = Cursors.WaitCursor;
            string Filter = string.Empty;

            var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtGoalSetting.Text, new string[] { "A.GoalsName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "GoalsName", "ActInd", "PICCode", "PICName",

                            //6-10
                            "DirectorateName", "EvaluatorCode", "EvaluatorName", "TopEvaluatorCode", "TopEvaluatorName",

                            //11-12
                            "Remark", "Status"

                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Events
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validating(object sender, CancelEventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtGoalSetting_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGoalSetting_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Goals Setting");
        }

        #endregion

        #endregion
    }
}
