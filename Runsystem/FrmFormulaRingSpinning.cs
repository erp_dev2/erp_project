﻿#region
/*
    01/11/2017 [TKG] Aplikasi baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFormulaRingSpinning : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmFormulaRingSpinningFind FrmFind;

        #endregion

        #region Constructor

        public FrmFormulaRingSpinning(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Code",
                        "Name",
                        "RPM",
                        "TPI",
                        "NE",

                        //6-7
                        "Avg Spindle",
                        "Kg/Hr"
                    },
                     new int[] 
                    {
                        20,
                        100, 200, 100, 100, 100,
                        120, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6 }, 2);
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 6);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtFRSCode, TxtFRSName, MeeRemark, ChkActInd }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7 });
                    TxtFRSCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFRSCode, TxtFRSName, MeeRemark, ChkActInd }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7 });
                    TxtFRSCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFRSName, MeeRemark, ChkActInd }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7 });
                    TxtFRSName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtFRSCode, TxtFRSName, MeeRemark });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6, 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFormulaRingSpinningFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFRSCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                    IsDataNotValid()) return;
                
                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveFormulaRingSpinningHdr());
                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                            cml.Add(SaveFormulaRingSpinningDtl(r));
                }

                Sm.ExecCommands(cml);

                ShowData(TxtFRSCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) 
                        Sm.FormShowDialog(new FrmFormulaRingSpinningDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 3, 4, 5, 6, 7 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 7 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmFormulaRingSpinningDlg(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3,4,5,6,7 }, e);
        }

        #endregion

        #region Show Data

        public void ShowData(string FRSCode)
        {
            try
            {
                ClearData();
                ShowFormulaRingSpinningHdr(FRSCode);
                ShowFormulaRingSpinningDtl(FRSCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowFormulaRingSpinningHdr(string FRSCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@FRSCode", FRSCode);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select FRSCode, FRSName, ActInd, Remark " +
                  "From TblFormulaRingSpinningHdr Where FRSCode=@FRSCode;",
                 new string[] 
                   {
                      //0
                      "FRSCode",
                      //1-3
                      "FRSName", "ActInd", "Remark"

                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtFRSCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtFRSName.EditValue = Sm.DrStr(dr, c[1]);
                     ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                     MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                 }, true
             );
        }

        private void ShowFormulaRingSpinningDtl(string FRSCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@FRSCode", FRSCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ItCode, B.ItName, A.RPM, A.TPI, A.NE, A.AvgSPL, A.MFR " +
                    "From TblFormulaRingSpinningDtl A " +
                    "Left Join TblItemSpinning B On A.ItCode=B.ItCode " +
                    "Where A.FRSCode=@FRSCode Order By B.ItName;",

                    new string[] 
                       { 
                           //0
                           "ItCode",

                           //1-5
                           "ItName", "RPM", "TPI", "NE", "AvgSPL", 
                           
                           //6
                           "MFR" 
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 7 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFRSCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtFRSName, "Name", false) ||
                IsFRSCodeExisted() ||
                IsGrdEmpty() ||
                IsFRSCodeExisted();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.")) return true;
            return false;
        }

        private bool IsFRSCodeExisted()
        {
            if (!TxtFRSCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblFormulaRingSpinningHdr Where FRSCode=@Param;", TxtFRSCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Code ( " + TxtFRSCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveFormulaRingSpinningHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblFormulaRingSpinningHdr ");
            SQL.AppendLine("(FRSCode, FRSName, ActInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@FRSCode, @FRSName, @ActInd, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update FRSName=@FRSName, ActInd=@ActInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (TxtFRSCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblFormulaRingSpinningDtl Where FRSCode=@FRSCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@FRSCode", TxtFRSCode.Text);
            Sm.CmParam<String>(ref cm, "@FRSName", TxtFRSName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveFormulaRingSpinningDtl(int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblFormulaRingSpinningDtl(FRSCode, ItCode, RPM, TPI, NE, AvgSPL, MFR, CreateBy, CreateDt) " +
                    "Values(@FRSCode, @ItCode, @RPM, @TPI, @NE, @AvgSPL, @MFR, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@FRSCode", TxtFRSCode.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<Decimal>(ref cm, "@RPM", Sm.GetGrdDec(Grd1, r, 3));
            Sm.CmParam<Decimal>(ref cm, "@TPI", Sm.GetGrdDec(Grd1, r, 4));
            Sm.CmParam<Decimal>(ref cm, "@NE", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@AvgSPL", Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<Decimal>(ref cm, "@MFR", Sm.GetGrdDec(Grd1, r, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Misc Event

        #region Misc Control Event

        private void TxtFRSCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFRSCode);
        }

        private void TxtFRSName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFRSName);
        }

        #endregion

        #endregion
        
    }
}
