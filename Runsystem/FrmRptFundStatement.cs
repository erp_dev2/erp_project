﻿#region Update
/*
    13/09/2021 [DITA/AMKA] New Reporting
    23/09/2021 [WED/AMKA] perbaikan rumus
    12/11/2021 [ARI/AMKA] set lue profit center terfilter berdasarkan group dengan menggunakan filter IsFilterByProfitCenter
    23/03/2022 [RDA/AMKA] perbaikan opening balance untuk fund statement
    01/03/2022 [RDA/AMKA] perbaikan rumus Laba Bersih Setelah Pajak Penghasilan menjadi (4+6+8.1)-(5+7-8.2) Tahun ini
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptFundStatement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<String> mlProfitCenter;
        private bool mIsAllProfitCenterSelected = false,
                        mIsFilterByProfitCenter = false;


        #endregion
        #region Constructor

        public FrmRptFundStatement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                mlProfitCenter = new List<String>();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                var CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
               Grd1, new string[]  
                    {
                        //0
                        "ID#",
                        
                        //1-5
                        "No.",
                        "Account Group",
                        "Account Name",
                        "Year To"+Environment.NewLine+"Date", 
                        "Month To"+Environment.NewLine+"Date",

                        //6
                        "Current Month",
                       
                    },
                new int[] 
                    {
                        //0
                        30, 

                        //1-5
                        40, 150, 200, 150, 150,  
                        
                        //6
                        150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsProfitCenterInvalid()
                ) return;
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<COAFundStatement>();
                var l2 = new List<FundStatementFormula>();
                var l3 = new List<FundStatementFormula2>();
                var l4 = new List<COAOpeningBalance>();
                var l5 = new List<COAJournal>();
                var l6 = new List<FundStatement>();
                var l7 = new List<COAOpeningBalanceFund>();
                var l8 = new List<COAJournalFund>();

                GetCOAFundStatement(ref l);
                GetParentInDataFundStatement(ref l);
                PrepFundStatementFormula(ref l2);
                PrepFundStatementFormula2(ref l3);
                PrepFundStatement(ref l6);
                if (l.Count > 0)
                {
                    SetProfitCenter();
                    ProcessCOAOpeningBalance(ref l, ref l4, Yr);
                    ProcessCOAJournal(ref l, ref l5, Mth, Yr);
                    ProcessOpeningBalance(ref l4, ref l7, ref l);
                    ProcessJournal(ref l5, ref l8, ref l);
                    ProcessChild(ref l6, ref l2, ref l7, ref l8);
                    ProcessNonChild(ref l6, ref l3);
                    ProcessGrid(ref l6);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                Grd1.GroupObject.Add(2);
                Grd1.Group();

                
                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear(); l6.Clear(); l7.Clear(); l8.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetCOAFundStatement(ref List<COAFundStatement> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct AcNo ");
            SQL.AppendLine("From TblRptFundStatementDtl ");
            SQL.AppendLine("ORDER BY AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAFundStatement()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            ParentInData = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetParentInDataFundStatement(ref List<COAFundStatement> l)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                if (!l[i].ParentInData)
                {
                    for (int j = i + 1; j < l.Count; ++j)
                    {
                        if (l[i].AcNo.Length <= l[j].AcNo.Length)
                        {
                            if (l[i].AcNo == Sm.Left(l[j].AcNo, l[i].AcNo.Length))
                            {
                                l[i].ParentInData = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void PrepFundStatementFormula(ref List<FundStatementFormula> l2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RptId, AcNo, GrpId, CurrYr ");
            SQL.AppendLine("From TblRptFundStatementDtl  ");
            SQL.AppendLine("ORDER BY RptId, GrpId; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptId", "AcNo", "GrpId", "CurrYr" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new FundStatementFormula()
                        {
                            RptId = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            GrpId = Sm.DrStr(dr, c[2]),
                            CurrYr = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepFundStatementFormula2(ref List<FundStatementFormula2> l3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RptId, RptId2 ");
            SQL.AppendLine("From TblRptFundStatementDtl2  ");
            SQL.AppendLine("ORDER BY RptId, RptId2; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptId", "RptId2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new FundStatementFormula2()
                        {
                            RptId = Sm.DrStr(dr, c[0]),
                            RptId2 = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCOAOpeningBalance(ref List<COAFundStatement> l, ref List<COAOpeningBalance> l4, string Yr)
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            int i = 0, j = 0, k = 0;
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.AcNo, Sum(T.Amt) Amt, T.Yr ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select B.AcNo,  A.Yr, ");
            SQL.AppendLine("    Case When E.ParValue='2' Then  ");
            SQL.AppendLine("        Case When D.AcType!=C.AcType Then B.Amt*-1 Else B.Amt End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When D.AcType!=C.AcType Then B.Amt Else B.Amt End ");
            SQL.AppendLine("    End As Amt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And ( ");
            foreach (var y in l)
            {
                if (j != 0) SQL.AppendLine(" Or ");

                SQL.AppendLine("    B.AcNo Like '" + y.AcNo + "%' ");

                j += 1;
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblCOA D On D.Parent Is Null And D.Level = 1 And Left(B.AcNo, Length(D.AcNo)) = D.AcNo ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode = 'COACalculationFormula' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

            if (!mIsAllProfitCenterSelected)
            {
                Filter = string.Empty;
                i = 0;
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
            }
            else
            {
                SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select B.AcNo, A.Yr, ");
            SQL.AppendLine("    Case When E.ParValue='2' Then  ");
            SQL.AppendLine("        Case When D.AcType!=C.AcType Then B.Amt*-1 Else B.Amt End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When D.AcType!=C.AcType Then B.Amt Else B.Amt End ");
            SQL.AppendLine("    End As Amt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And ( ");
            foreach (var y in l)
            {
                if (k != 0) SQL.AppendLine(" Or ");

                SQL.AppendLine("    B.AcNo Like '" + y.AcNo + "%' ");

                k += 1;
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblCOA D On D.Parent Is Null And D.Level = 1 And Left(B.AcNo, Length(D.AcNo)) = D.AcNo ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode = 'COACalculationFormula' ");
            SQL.AppendLine("Where A.Yr = @Yr2 ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

            if (!mIsAllProfitCenterSelected)
            {
                Filter = string.Empty;
                i = 0;
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
            }
            else
            {
                SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");

            }


            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.AcNo, T.Yr ");
            SQL.AppendLine("Order By T.AcNo, T.Yr ; ");
            


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Yr2", (Int32.Parse(Yr) - 1).ToString());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Yr", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new COAOpeningBalance()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Yr = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            AcNoFund = string.Empty
                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => !w.ParentInData))
            {
                foreach (var y in l4.Where(w => w.AcNoFund.Length == 0))
                {
                    if (y.AcNo.Length >= x.AcNo.Length)
                    {
                        if (y.AcNo.Substring(0, x.AcNo.Length) == x.AcNo)
                        {
                            y.AcNoFund = x.AcNo;
                        }
                    }
                }
            }

          
        }

        private void ProcessCOAJournal(ref List<COAFundStatement> l, ref List<COAJournal> l5, string Mth, string Yr)
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            int i = 0, j = 0, k=0;
            var cm = new MySqlCommand();

       
            SQL.AppendLine("Select T.AcNo, Sum(T.Amt) Amt , T.Yr, T.Mth From ( ");
            SQL.AppendLine("Select B.AcNo, Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) Mth, ");
            SQL.AppendLine("Sum( ");
            SQL.AppendLine("    Case When E.ParValue='2' Then  ");
            SQL.AppendLine("        Case When D.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When C.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine(") As Amt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And ( ");
            foreach (var y in l)
            {
                if (j != 0) SQL.AppendLine(" Or ");

                SQL.AppendLine("    B.AcNo Like '" + y.AcNo + "%' ");

                j += 1;
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Inner Join TblCOA D On D.Parent Is Null And D.Level = 1 And Left(B.AcNo, Length(D.AcNo)) = D.AcNo ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode = 'COACalculationFormula' ");
            SQL.AppendLine("Where Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("    And Left(A.DocDt, 6)<=@YrMth ");

            if (!mIsAllProfitCenterSelected)
            {
                Filter = string.Empty;
                i = 0;

                SQL.AppendLine("    And A.CCCode Is Not Null ");
                SQL.AppendLine("    And A.CCCode In ( ");
                SQL.AppendLine("        Select Distinct CCCode ");
                SQL.AppendLine("        From TblCostCenter ");
                SQL.AppendLine("        Where ActInd='Y' ");
                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And ProfitCenterCode In ( ");
                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("    And A.CCCode Is Not Null ");
                SQL.AppendLine("    And A.CCCode In ( ");
                SQL.AppendLine("        Select Distinct CCCode ");
                SQL.AppendLine("        From TblCostCenter ");
                SQL.AppendLine("        Where ActInd='Y' ");
                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                SQL.AppendLine("    ) ");
                
               
            }
            SQL.AppendLine("Group By B.AcNo, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select B.AcNo, Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) Mth, ");
            //SQL.AppendLine("Sum(Case When C.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End) As Amt ");
            SQL.AppendLine("Sum( ");
            SQL.AppendLine("    Case When E.ParValue='2' Then  ");
            SQL.AppendLine("        Case When D.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When C.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine(") As Amt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And ( ");
            foreach (var y in l)
            {
                if (k != 0) SQL.AppendLine(" Or ");

                SQL.AppendLine("    B.AcNo Like '" + y.AcNo + "%' ");

                k += 1;
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Inner Join TblCOA D On D.Parent Is Null And D.Level = 1 And Left(B.AcNo, Length(D.AcNo)) = D.AcNo ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode = 'COACalculationFormula' ");
            SQL.AppendLine("Where Left(A.DocDt, 4)=@Yr2 ");
            SQL.AppendLine("    And Left(A.DocDt, 6)<=@YrMth2 ");

            if (!mIsAllProfitCenterSelected)
            {
                Filter = string.Empty;
                i = 0;

                SQL.AppendLine("    And A.CCCode Is Not Null ");
                SQL.AppendLine("    And A.CCCode In ( ");
                SQL.AppendLine("        Select Distinct CCCode ");
                SQL.AppendLine("        From TblCostCenter ");
                SQL.AppendLine("        Where ActInd='Y' ");
                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And ProfitCenterCode In ( ");
                SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("    And A.CCCode Is Not Null ");
                SQL.AppendLine("    And A.CCCode In ( ");
                SQL.AppendLine("        Select Distinct CCCode ");
                SQL.AppendLine("        From TblCostCenter ");
                SQL.AppendLine("        Where ActInd='Y' ");
                SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                SQL.AppendLine("    ) ");


            }
            SQL.AppendLine("Group By B.AcNo, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");

            SQL.AppendLine(") T ");

            SQL.AppendLine("Group By T.AcNo, T.Yr, T.Mth ");
            SQL.AppendLine("Order By T.AcNo, T.Yr, T.Mth ; ");
      


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Yr2", (Int32.Parse(Yr) - 1).ToString());
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
                Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat((Int32.Parse(Yr) - 1).ToString(), Mth));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Yr", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l5.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Yr = Sm.DrStr(dr, c[1]),
                            Mth = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            AcNoFund = string.Empty
                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => !w.ParentInData))
            {
                foreach (var y in l5.Where(w => w.AcNoFund.Length == 0))
                {
                    if (y.AcNo.Length >= x.AcNo.Length)
                    {
                        if (y.AcNo.Substring(0, x.AcNo.Length) == x.AcNo)
                        {
                            y.AcNoFund = x.AcNo;
                        }
                    }
                }
            }

        }

        private void PrepFundStatement(ref List<FundStatement> l6)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.RptId, A.No, B.AcGrpName, A.AcName, A.PositiveInd, A.ZeroFormula, A.CalcGroupFormula, ");
            SQL.AppendLine("If(C.RptId Is null, 'N', 'Y') ChildInd ");
            SQL.AppendLine("From TblRptFundStatementHdr A ");
            SQL.AppendLine("Inner Join TblRptFundStatementGroup B On A.AcGrpCode = B.AcGrpCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct RptId ");
            SQL.AppendLine("    From TblRptFundStatementDtl ");
            SQL.AppendLine(") C On A.RptId = C.RptId ");
            SQL.AppendLine("Order By A.RptId; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptId", "No", "AcGrpName", "AcName", "ChildInd", "CalcGroupFormula", "PositiveInd", "ZeroFormula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l6.Add(new FundStatement()
                        {
                            RptId = Sm.DrStr(dr, c[0]),
                            No = Sm.DrStr(dr, c[1]),
                            AcGrpName = Sm.DrStr(dr, c[2]),
                            AcName = Sm.DrStr(dr, c[3]),
                            ChildInd = Sm.DrStr(dr, c[4]),
                            CalcGroupFormula = Sm.DrStr(dr, c[5]),
                            PositiveInd = Sm.DrStr(dr, c[6]),
                            ZeroFormula = Sm.DrStr(dr, c[7]),
                            CurrAmt = 0m,
                            MthToDtAmt = 0m,
                            YrToDtAmt = 0m
                            
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOpeningBalance(ref List<COAOpeningBalance> l4, ref List<COAOpeningBalanceFund> l7, ref List<COAFundStatement> l)
        {
            l7 = l4.GroupBy(c => new
            {
                c.AcNoFund,
                c.Yr,
            })
                .Select(t => new COAOpeningBalanceFund
                {
                    AcNoFund = t.Key.AcNoFund,
                    Yr = t.Key.Yr,
                    Amt = t.Sum(s => s.Amt),

                }).ToList();

            if (l7.Count > 0)
            {
                bool IsExist = false;

                foreach (var x in l.Where(w => w.ParentInData))
                {
                    IsExist = false;

                    foreach (var y in l7.OrderBy(o => o.AcNoFund).ThenBy(o => o.Yr))
                    {
                        if (y.AcNoFund.Length >= x.AcNo.Length)
                        {
                            if (x.AcNo == Sm.Left(y.AcNoFund, x.AcNo.Length))
                            {
                                if (!IsExist)
                                {
                                    l7.Add(new COAOpeningBalanceFund() 
                                    { 
                                        AcNoFund = x.AcNo,
                                        Amt = 0m,
                                        Yr = Sm.GetLue(LueYr)
                                    });

                                    l7.Add(new COAOpeningBalanceFund()
                                    {
                                        AcNoFund = x.AcNo,
                                        Amt = 0m,
                                        Yr = (Int32.Parse(Sm.GetLue(LueYr)) - 1).ToString()
                                    });

                                    IsExist = true;
                                }

                                foreach (var z in l7.Where(w => w.AcNoFund == x.AcNo && w.Yr == y.Yr))
                                {
                                    z.Amt += y.Amt;
                                }
                            }
                        }
                    }
                }

                foreach (var x in l4.Where(w => w.AcNoFund.Length == 0).OrderBy(o => o.AcNo))
                {
                    foreach (var y in l.Where(w => w.ParentInData && w.AcNo == Sm.Left(x.AcNo, w.AcNo.Length)))
                    {
                        foreach (var z in l7.Where(w => w.AcNoFund == y.AcNo && w.Yr == x.Yr))
                        {
                            z.Amt += x.Amt;
                        }
                    }
                }
            }
        }

        private void ProcessJournal(ref List<COAJournal> l5, ref List<COAJournalFund> l8, ref List<COAFundStatement> l)
        {
            l8 = l5.GroupBy(c => new
            {
                c.AcNoFund,
                c.Yr,
                c.Mth
            })
                .Select(t => new COAJournalFund
                {
                    AcNoFund = t.Key.AcNoFund,
                    Yr = t.Key.Yr,
                    Mth = t.Key.Mth,
                    Amt = t.Sum(s => s.Amt),

                }).ToList();

            if (l8.Count > 0)
            {
                bool IsExist = false;

                foreach (var x in l.Where(w => w.ParentInData))
                {
                    IsExist = false;

                    foreach (var y in l8.OrderBy(o => o.AcNoFund).ThenBy(o => o.Yr).ThenBy(o => o.Mth))
                    {
                        if (y.AcNoFund.Length >= x.AcNo.Length)
                        {
                            if (x.AcNo == Sm.Left(y.AcNoFund, x.AcNo.Length))
                            {
                                if (!IsExist)
                                {
                                    for (int i = Int32.Parse(Sm.GetLue(LueYr)) - 1; i <= Int32.Parse(Sm.GetLue(LueYr)); ++i)
                                    {
                                        for (int j = 1; j < 13; ++j)
                                        {
                                            l8.Add(new COAJournalFund()
                                            {
                                                AcNoFund = x.AcNo,
                                                Amt = 0m,
                                                Yr = i.ToString(),
                                                Mth = Sm.Right(string.Concat("00", j.ToString()), 2)
                                            });
                                        }
                                    }

                                    IsExist = true;
                                }

                                foreach (var z in l8.Where(w => w.AcNoFund == x.AcNo && w.Yr == y.Yr && w.Mth == y.Mth))
                                {
                                    z.Amt += y.Amt;
                                }
                            }
                        }
                    }
                }

                foreach (var x in l5.Where(w => w.AcNoFund.Length == 0).OrderBy(o => o.AcNo))
                {
                    foreach (var y in l.Where(w => w.ParentInData && w.AcNo == Sm.Left(x.AcNo, w.AcNo.Length)))
                    {
                        foreach (var z in l8.Where(w => w.AcNoFund == y.AcNo && w.Yr == x.Yr && w.Mth == x.Mth))
                        {
                            z.Amt += x.Amt;
                        }
                    }
                }
            }
        }

        private void ProcessChild(ref List<FundStatement> l6, ref List<FundStatementFormula> l2, ref List<COAOpeningBalanceFund> l7, ref List<COAJournalFund> l8)
        {
            foreach (var x in l6.Where(w => w.ChildInd == "Y"))
            {

                string mYr = string.Empty;
                int mMth = Int32.Parse(Sm.GetLue(LueMth));

                string RptId;
                string No;
                string AcGrpName;
                string AcName;
                decimal YrToDtAmt;
                decimal MthToDtAmt;
                decimal CurrAmt;
                string ChildInd;
                string CalcGroupFormula;
                string PositiveInd;
                string ZeroFormula;

                 RptId = x.RptId;
                 No = x.No;
                 AcGrpName = x.AcGrpName;
                 AcName = x.AcName;
                 YrToDtAmt = x.YrToDtAmt;
                 MthToDtAmt = x.MthToDtAmt;
                 CurrAmt = x.CurrAmt;
                 ChildInd = x.ChildInd;
                 CalcGroupFormula = x.CalcGroupFormula;
                 PositiveInd = x.PositiveInd;
                 ZeroFormula = x.ZeroFormula;

                foreach (var y in l2.Where(w => w.RptId == x.RptId))
                {

                    if (y.CurrYr == "Y") mYr = Sm.GetLue(LueYr);
                    else mYr = (Int32.Parse(Sm.GetLue(LueYr)) - 1).ToString();

                    foreach (var z in l7.Where(w => w.AcNoFund == y.AcNo && w.Yr == mYr))
                    {
                        if (y.GrpId == "1" || (y.GrpId != "1" && x.CalcGroupFormula == "+"))
                        {
                            x.YrToDtAmt += z.Amt;
                        }
                        else
                        {
                            if (z.AcNoFund == "8.2") z.Amt = z.Amt * -1;
                            x.YrToDtAmt -= z.Amt;
                        }
                    } 

                    foreach (var a in l8.Where(w => w.AcNoFund == y.AcNo && w.Yr == mYr))
                    {
                        if (y.GrpId == "1" || (y.GrpId != "1" && x.CalcGroupFormula == "+"))
                        {
                            if (Int32.Parse(a.Mth) < mMth)
                            {
                                x.YrToDtAmt += a.Amt;
                                x.MthToDtAmt += a.Amt;
                            }
                            if (Int32.Parse(a.Mth) == mMth)
                            {
                                x.YrToDtAmt += a.Amt;
                                x.CurrAmt += a.Amt;
                            }
                        }
                        else
                        {
                            if (a.AcNoFund == "8.2") a.Amt = a.Amt * -1;

                            if (Int32.Parse(a.Mth) < mMth)
                            {
                                x.YrToDtAmt -= a.Amt;
                                x.MthToDtAmt -= a.Amt;
                            }
                            if (Int32.Parse(a.Mth) == mMth)
                            {
                                x.YrToDtAmt -= a.Amt;
                                x.CurrAmt -= a.Amt;
                            }
                        }
                    } 

                }

                if (x.PositiveInd == "Y")
                {
                    /*if (x.YrToDtAmt < 0m)*/
                    //x.YrToDtAmt *= -1;
                    /*if (x.MthToDtAmt < 0m)*/
                    //x.MthToDtAmt *= -1;
                    /*if (x.CurrAmt < 0m)*/
                    //x.CurrAmt *= -1;

                    if (x.ZeroFormula.Length == 0)
                    {
                        x.YrToDtAmt *= -1;
                        x.MthToDtAmt *= -1;
                        x.CurrAmt *= -1;
                    }
                    else
                    {
                        if (x.ZeroFormula == "1")
                        {
                            if (x.YrToDtAmt < 0m) x.YrToDtAmt *= -1; else x.YrToDtAmt = 0m;
                            if (x.MthToDtAmt < 0m) x.MthToDtAmt *= -1; else x.MthToDtAmt = 0m;
                            if (x.CurrAmt < 0m) x.CurrAmt *= -1; else x.CurrAmt = 0m;
                        }
                        else
                        {
                            if (x.YrToDtAmt <= 0m) x.YrToDtAmt = 0m;
                            if (x.MthToDtAmt <= 0m) x.MthToDtAmt = 0m;
                            if (x.CurrAmt <= 0m) x.CurrAmt = 0m;
                        }
                    }
                }
                else
                {
                    if (x.ZeroFormula == "1")
                    {
                        if (x.YrToDtAmt >= 0m) x.YrToDtAmt = 0m;
                        if (x.MthToDtAmt >= 0m) x.MthToDtAmt = 0m;
                        if (x.CurrAmt >= 0m) x.CurrAmt = 0m;
                    }
                }
            } 
        }

        private void ProcessNonChild(ref List<FundStatement> l6, ref List<FundStatementFormula2> l3)
        {
            foreach (var x in l6.Where(w => w.ChildInd == "N").OrderBy(o => o.RptId))
            {

                bool IsFirst = true;

                foreach (var y in l3.Where(w => w.RptId == x.RptId).OrderBy(o => o.RptId2))
                {
                    foreach (var z in l6.Where(w => w.RptId == y.RptId2))
                    {
                        if (IsFirst || (!IsFirst && x.CalcGroupFormula == "+"))
                        {
                            x.YrToDtAmt += z.YrToDtAmt;
                            x.MthToDtAmt += z.MthToDtAmt;
                            x.CurrAmt += z.CurrAmt;

                            if (IsFirst) IsFirst = false;
                        }
                        else
                        {
                            x.YrToDtAmt -= z.YrToDtAmt;
                            x.MthToDtAmt -= z.MthToDtAmt;
                            x.CurrAmt -= z.CurrAmt;
                        }
                    }
                }
            } 
        }

        private void ProcessGrid(ref List<FundStatement> l6)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l6)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = x.RptId;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.No;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcGrpName;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = x.AcName;
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.YrToDtAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.MthToDtAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.FormatNum(x.CurrAmt, 0);

                Row += 1;
            }

            Grd1.EndUpdate();
        }


        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                IsCompleted = false;
           
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T1.ProfitCenterName As Col, T1.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T1 ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T1.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        #endregion

            #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

       

        #endregion

        #endregion

        #region Class

        class COAFundStatement
        {
            public string AcNo { get; set; }
            public bool ParentInData { get; set; }
        }

        class FundStatementFormula
        {
            public string RptId { get; set; }
            public string AcNo { get; set; }
            public string GrpId { get; set; }
            public string CurrYr { get; set; }
        }

        class FundStatementFormula2
        {
            public string RptId { get; set; }
            public string RptId2 { get; set; }
        }

        class COAOpeningBalance
        {
            public string AcNo { get; set; }
            public string AcNoFund { get; set; }
            public string Yr { get; set; }
            public decimal Amt { get; set; }
        }

        class COAJournal
        {
            public string AcNo { get; set; }
            public string AcNoFund { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        class FundStatement
        {
            public string RptId { get; set; }
            public string No { get; set; }
            public string AcGrpName  { get; set; }
            public string AcName  { get; set; }
            public decimal YrToDtAmt { get; set; }
            public decimal MthToDtAmt { get; set; }
            public decimal CurrAmt { get; set; }
            public string ChildInd { get; set; }
            public string CalcGroupFormula { get; set; }
            public string PositiveInd { get; set; }
            public string ZeroFormula { get; set; }
        }

        class COAOpeningBalanceFund
        {
            public string AcNoFund { get; set; }
            public string Yr { get; set; }
            public decimal Amt { get; set; }
        }

        class COAJournalFund
        {
            public string AcNoFund { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
