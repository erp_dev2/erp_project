﻿#region Update
/*
    01/09/2020 [TKG/SIER] ubah perhitungan brutto, transferred
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPayroll12Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestPayroll12 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestPayroll12Dlg(FrmVoucherRequestPayroll12 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, A.PayrunName, D.SiteName, B.DeptName, A.StartDt, A.EndDt, ");
            SQL.AppendLine("C.Brutto, C.Tax, C.Transferred, C.SSEmployeeHealth, C.SSEmployeeEmployment, C.SSEmployeePension, ");
            SQL.AppendLine("C.SSEmployerHealth, C.SSEmployerEmployment, C.SSEmployerPension, F.EntCode, F.EntName ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.PayrunCode, ");
            SQL.AppendLine("    Sum(T1.Brutto) As Brutto, ");
            SQL.AppendLine("    Sum(T1.Transferred) As Transferred, ");
            SQL.AppendLine("    Sum(T1.Tax) As Tax, ");
            SQL.AppendLine("    Sum(T1.TaxAllowance) As TaxAllowance, ");
            SQL.AppendLine("    Sum(T1.SSEmployeeHealth) As SSEmployeeHealth, ");
            SQL.AppendLine("    Sum(T1.SSEmployeeEmployment) As SSEmployeeEmployment, ");
            SQL.AppendLine("    Sum(T1.SSEmployeePension+T1.SSEmployeePension2+T1.SSEePension) As SSEmployeePension, ");
            SQL.AppendLine("    Sum(T1.SSEmployerHealth) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T1.SSEmployerEmployment) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T1.SSEmployerPension+T1.SSEmployerPension2+T1.SSErPension) As SSEmployerPension ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 ");
            SQL.AppendLine("        On T1.PayrunCode=T2.PayrunCode ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.VoucherRequestPayrollInd In ('O', 'P') ");
            if (mFrmParent.mIsNotForStd)
            {
                if (mFrmParent.mIsForMonthlyEmployee)
                    SQL.AppendLine("        And T2.PayrunPeriod=@PayrunPeriod ");
                else
                    SQL.AppendLine("        And T2.PayrunPeriod<>@PayrunPeriod ");
            }
            SQL.AppendLine("    Where T1.VoucherRequestPayrollDocNo Is Null ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=T1.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select X2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr X1 ");
                SQL.AppendLine("        Inner Join TblPPADtl X2 On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("        Where X1.ActInd='Y' And X1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine(") C On A.PayrunCode=C.PayrunCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity F On E.EntCode=F.EntCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.VoucherRequestPayrollInd In ('O', 'P') ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Payrun's Code", 
                        "Payrun's Name",
                        "Site",
                        "Department",
                        
                        //6-10
                        "Start Date",
                        "End Date",
                        "Brutto",
                        "Tax",
                        "Transferred",
                        
                        //11-15
                        "Employee's" +Environment.NewLine + "Health",
                        "Employee's" +Environment.NewLine + "Employment",
                        "Employee's" +Environment.NewLine + "Pension",
                        "Employer's" +Environment.NewLine + "Health",
                        "Employer's" +Environment.NewLine + "Employment",
                        
                        //16-19
                        "Employer's" +Environment.NewLine + "Pension",
                        "Total SS",
                        "Entity Code",
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 150, 150, 

                        //6-10
                        80, 80, 120, 120, 120, 

                        //11-15
                        120, 120, 120, 120, 120,

                        //16-19
                        120, 120, 0, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 18 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, true);
            if (mFrmParent.mIsEntityMandatory)
                Grd1.Cols[19].Move(6);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd3, r, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " And ";
                        Filter += "(A.PayrunCode<>@PayrunCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@PayrunCode0" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd3, r, 1));
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ")";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@PayrunPeriod", mFrmParent.mPayrunPeriodBulanan);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayrunName.Text, new string[] { "A.PayrunCode", "A.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.PayrunCode;",
                        new string[] 
                        { 
                             //0
                             "PayrunCode", 
                             
                             //1-5
                             "PayrunName", 
                             "SiteName",
                             "DeptName", 
                             "StartDt",
                             "EndDt",
                             
                             //6-10
                             "Brutto",
                             "Tax",
                             "Transferred",
                             "SSEmployeeHealth",
                             "SSEmployeeEmployment",
                             
                             //11-15
                             "SSEmployeePension",
                             "SSEmployerHealth",
                             "SSEmployerEmployment",
                             "SSEmployerPension",
                             "EntCode",

                             //16
                             "EntName"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Grd.Cells[Row, 17].Value = 
                                dr.GetDecimal(c[9]) + dr.GetDecimal(c[10]) + dr.GetDecimal(c[11]) +
                                dr.GetDecimal(c[12]) + dr.GetDecimal(c[13]) + dr.GetDecimal(c[14]);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsPayrunCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 8, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 9, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 10, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 11, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 12, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 13, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 14, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 15, Grd1, Row2, 19);

                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                    }
                }
            }

            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 payrun.");
            else
                mFrmParent.ComputePayrunInfo();
        }

        private bool IsPayrunCodeAlreadyChosen(int Row)
        {
            var PayrunCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 1), PayrunCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPayrun(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPayrunCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPayrun(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPayrunCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayrunName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayrunName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
