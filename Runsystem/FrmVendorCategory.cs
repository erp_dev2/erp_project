﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmVendorCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmVendorCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdCtCode, TxtVdCtName
                    }, true);
                    TxtVdCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdCtCode, TxtVdCtName
                    }, false);
                    TxtVdCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtVdCtCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdCtName
                    }, false);
                    TxtVdCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtVdCtCode, TxtVdCtName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVendorCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVdCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVdCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblVendorCategory Where VdCtCode=@VdCtCode" };
                Sm.CmParam<String>(ref cm, "@VdCtCode", TxtVdCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblVendorCategory(VdCtCode, VdCtName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@VdCtCode, @VdCtName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update VdCtName=@VdCtName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@VdCtCode", TxtVdCtCode.Text);
                Sm.CmParam<String>(ref cm, "@VdCtName", TxtVdCtName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtVdCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string VdCtCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@VdCtCode", VdCtCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblVendorCategory Where VdCtCode=@VdCtCode",
                        new string[] 
                        {
                            "VdCtCode", "VdCtName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtVdCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtVdCtName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtVdCtCode, "Vendor's category code", false) ||
                Sm.IsTxtEmpty(TxtVdCtName, "Vendor's category name", false) ||
                IsVdCtCodeExisted();
        }

        private bool IsVdCtCodeExisted()
        {
            if (!TxtVdCtCode.Properties.ReadOnly && Sm.IsDataExist("Select VdCtCode From TblVendorCategory Where VdCtCode='" + TxtVdCtCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor's category code ( " + TxtVdCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVdCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVdCtCode);
        }

        private void TxtVdCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVdCtName);
        }

        #endregion

        #endregion
    }
}
