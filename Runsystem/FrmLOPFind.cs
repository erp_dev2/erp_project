﻿#region Update
/*
    17/07/2018 [HAR] tambah informasi site center, costcenter, scope, resource
    07/08/2018 [WED] filter DocDt belum masuk ke query
    14/08/2018 [WED] kolom Status --> ProcessInd
    17/01/2019 [MEY] menambahkan informasi Approval 
    06/08/2019 [DITA] Bug tampilan lose pada LOP Find kosong
    08/08/2019 [TKG] tambah filter berdasarkan site. 
    24/09/2019 [TKG/IMS] tambah project group
    30/12/2019 [VIN/VIR] tambah confident level
    18/01/2020 [TKG/IMS] tambah BOQ
    30/01/2020 [TKG/IMS] tambah quotation letter
    11/02/2020 [VIN/YK] tambah filter phase
    05/05/2020 [VIN/YK] penggunaan parameter @PhaseCodetoDisplay diganti menggunakan checkbox finished
    09/06/2021 [RDA/IMS] tambah checklist Exclude Document Cancel
    08/03/2022 [TYO/GSS] menambah kolom route, phase, status di detail berdasarkan parameter IsLOPFindUseRouteAndPhase
    05/01/2023 [ICA/MNET] source PIC ambil dari user berdasarkan parameter IsLOPShowPICSales
    27/03/2023 [RDA/MNET] penyesuaian isi kolom scope
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmLOPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmLOP mFrmParent;
        private string mSQL = string.Empty;
        private string mSQL2 = string.Empty;
        

        #endregion

        #region Constructor

        public FrmLOPFind(FrmLOP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //GetParameter();
                SetGrd();
                
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueOption(ref LueConfidentLvl,"ConfidentLvl");
                Sl.SetLuePGCode(ref LuePGCode, string.Empty, "N");
                mFrmParent.SetLuePhaseCode(ref LuePhase);
                ChkFinishedPhase.Checked = false;
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ConfidentLvl, A.DocNo, A.DocDt, A.CancelInd, Case A.ProcessInd When 'P' Then 'On Progress' When 'C' Then 'Cancelled' When 'L' Then 'Closed'When 'S' Then 'Lose' End ProcessInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding'  End As Approval, ");
            SQL.AppendLine("A.ProjectName, J.PGName, B.Ctname, C.OptDesc, E.Cityname, A.EstValue, A.Remark, A.QtLetterNo, ");
            if (mFrmParent.mIsLOPShowPICSales)
                SQL.AppendLine("D.UserName As PICName, ");
            else
                SQL.AppendLine("D.Empname As PICName, ");
            SQL.AppendLine("F.SiteName, G.CCName, IFNULL(H.OptDesc, A.ProjectScope) As Scope, I.OptDesc as Resource, K.OptDesc As ConfidentLevel, L.DocNo As BOQDocNo, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, E.CityName,  ");
            if (mFrmParent.mIsLOPUseRouteAndPhase)
                SQL.AppendLine("M.Route, N.PhaseName, Case When M.Status = 'P' Then 'Progress' When M.status = 'F' Then 'Finished' End As Status ");
            else
                SQL.AppendLine("Null As Route, Null As PhaseName, Null As Status ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.* From TblLOPHdr T ");
            SQL.AppendLine("    Where T.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (T.SiteCode Is Null Or ( ");
                SQL.AppendLine("    T.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("             Where UserCode=@UserCode   ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblCustomer B  On A.CtCode = B.CtCode  ");
            SQL.AppendLine("Inner Join TblOption C  On A.ProjectType = C.OptCode And C.OptCat ='ProjectType'  ");
            if (mFrmParent.mIsLOPShowPICSales)
                SQL.AppendLine("Inner Join TblUser D On A.PICCode = D.UserCode ");
            else
                SQL.AppendLine("Inner Join TblEmployee D  On A.picCode = D.EmpCode   ");
            SQL.AppendLine("Left Join TblCity E On A.CityCode = E.CityCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode = F.SiteCode ");
            SQL.AppendLine("Left Join TblCostCenter G On A.CCCode = G.CCCode ");
            SQL.AppendLine("Left Join tblOption H On A.ProjectScope = H.OptCode And H.OptCat = 'ProjectScope' ");
            SQL.AppendLine("Left Join tblOption I On A.ProjectResource = I.OptCode And I.OptCat = 'ProjectResource' ");
            SQL.AppendLine("Left Join TblProjectGroup J On A.PGCode=J.PGCode ");
            SQL.AppendLine("Left Join tblOption K On A.ConfidentLvl = K.OptCode And K.OptCat = 'ConfidentLvl' ");
            SQL.AppendLine("Left Join TblBOQHdr L On A.DocNo=L.LOPDocNo And L.Status In ('O', 'A') ");
            if (mFrmParent.mIsLOPUseRouteAndPhase)
            {
                SQL.AppendLine("Inner Join TblLopDtl M On A.DocNo = M.DocNo ");
                SQL.AppendLine("Inner Join TblPhase N On M.PhaseCode = N.PhaseCode ");
            }
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Approval",
                    "Cancel",
                    "Process",

                     //6-10
                    "Project Name",
                    "Project's Group",
                    "Customer",
                    "Project Type",
                    "City",
                    
                    //11-15
                    "Person In Charge",                       
                    "Estimated Value",
                    "Site",
                    "Cost Center",
                    "Scope",

                    //16-20
                    "Resource",
                    "Confident Level",
                    "BOQ",  
                    "Quotation Letter#",
                    "Remark",
                    
                    //21-25
                    "Created By",
                    "Created Date",
                    "Created Time",
                    "Last Updated By",                      
                    "Last Updated Date", 

                    //26-29
                    "Last Updated Time",
                    "Route",
                    "Phase",
                    "Status"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    160, 100, 100,60, 80,

                    //6-10
                    180, 150, 150, 100, 100,

                    //11-15
                    180,  120, 120, 120, 120, 

                    //16-20
                    120, 130, 150, 200, 120, 

                    //21-25
                    120, 120, 120, 120, 120,

                    //26-29
                    120, 150, 150, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 22, 25 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 23, 26 });
            Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24, 25, 26, 27, 28, 29 }, false);
            Sm.SetGrdProperty(Grd1, false);

            if(mFrmParent.mIsLOPUseRouteAndPhase)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 27, 28, 29 }, true);
                Grd1.Cols[27].Move(9);
                Grd1.Cols[28].Move(10);
                Grd1.Cols[29].Move(11);
            }
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                
                
                if (ChkFinishedPhase.Checked)
                    {
                        var mSQL2 = new StringBuilder();

                        mSQL2.AppendLine("where A.Docno IN ");
                        mSQL2.AppendLine("  ( ");
                        mSQL2.AppendLine("      SELECT DISTINCT Docno ");
                        mSQL2.AppendLine("      FROM TblLOPDtl ");
                        mSQL2.AppendLine("      WHERE Find_In_Set(STATUS , 'F') ");
                        mSQL2.AppendLine("      AND DocNo NOT In ");
                        mSQL2.AppendLine("      ( ");
                        mSQL2.AppendLine("          SELECT DISTINCT T.DocNo ");
                        mSQL2.AppendLine("          FROM TblLOPDtl T ");
                        mSQL2.AppendLine("          WHERE T.Status != 'F' ");
                        mSQL2.AppendLine("      ) ");
                        mSQL2.AppendLine("  ) ");

                        Filter = mSQL2.ToString();
                    }
                else
                      Filter = " Where 1 = 1 ";


                if (Sm.GetLue(LuePhase).Length > 0)
                {
                    
                    var mSQL3 = new StringBuilder();


                    mSQL3.AppendLine(" And A.DocNo IN ");
                    mSQL3.AppendLine("( ");
                    mSQL3.AppendLine("    SELECT DISTINCT DocNo ");
                    mSQL3.AppendLine("    FROM TblLOPDtl ");
                    mSQL3.AppendLine("    WHERE PhaseCOde = @PhaseCode ");
                    mSQL3.AppendLine(") ");

                    Filter += mSQL3.ToString();
                }

                
                if (ChkExCancelDoc.Checked)
                {
                    var mSQL4 = new StringBuilder();

                    mSQL4.AppendLine("And A.CancelInd = 'N' ");

                    Filter += mSQL4.ToString();
                }
                

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@PhaseCodetoDisplay", mFrmParent.mPhaseCodetoDisplay);
                Sm.CmParam<String>(ref cm, "@PhaseCode", Sm.GetLue(LuePhase));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "A.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePGCode), "A.PGCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueConfidentLvl), "A.ConfidentLvl", true);
                


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt","Approval","CancelInd", "ProcessInd", "ProjectName", 

                            //6-10
                            "PGName", "Ctname", "OptDesc", "CityName", "PICName", 

                            //11-15
                            "EstValue", "Sitename", "CCName", "Scope", "Resource",

                            //16-20
                           "ConfidentLevel", "BOQDocNo", "QtLetterNo", "Remark", "CreateBy", 
                           
                           //21-25
                           "CreateDt", "LastUpBy", "LastUpDt", "Route", "PhaseName",

                           //26
                           "Status"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            //mPhaseCodetoDisplay = Sm.GetParameter("PhaseCodetoDisplay");
        }
        #endregion 

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue3(Sl.SetLuePGCode), string.Empty, "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project group");
        }

        private void LueConfidentLvl_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueConfidentLvl, new Sm.RefreshLue2(Sl.SetLueOption), "ConfidentLvl");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkConfidentLvl_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Confident Level");
        }
       
        private void LuePhase_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePhase, new Sm.RefreshLue1(mFrmParent.SetLuePhaseCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPhase_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Phase");
        }
        
        #endregion

        

        



        #endregion


    }
}
