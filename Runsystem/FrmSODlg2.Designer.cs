﻿namespace RunSystem
{
    partial class FrmSODlg2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSODlg2));
            this.LblAgtCode = new System.Windows.Forms.Label();
            this.LueAgtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtItemName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtQty = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtUomCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUPrice = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDiscRate1 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDiscRate2 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDiscRate3 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtDiscRate4 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtDiscRate5 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPromoRate = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtUPriceBefore = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTax = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtUPriceAfter = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnItem = new DevExpress.XtraEditors.SimpleButton();
            this.BtnMasterItem = new DevExpress.XtraEditors.SimpleButton();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItemName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPromoRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBefore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnAdd
            // 
            this.BtnAdd.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAdd.Appearance.Options.UseBackColor = true;
            this.BtnAdd.Appearance.Options.UseFont = true;
            this.BtnAdd.Appearance.Options.UseForeColor = true;
            this.BtnAdd.Appearance.Options.UseTextOptions = true;
            this.BtnAdd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.BtnItem);
            this.panel2.Controls.Add(this.TxtTotal);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtUPriceAfter);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.TxtAmt);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtTax);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TxtUPriceBefore);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtPromoRate);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtDiscRate5);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtDiscRate4);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtDiscRate3);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtDiscRate2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtDiscRate1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtUPrice);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtUomCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtQty);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.TxtItemName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.BtnMasterItem);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.LblAgtCode);
            this.panel2.Controls.Add(this.LueAgtCode);
            // 
            // LblAgtCode
            // 
            this.LblAgtCode.AutoSize = true;
            this.LblAgtCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAgtCode.ForeColor = System.Drawing.Color.Black;
            this.LblAgtCode.Location = new System.Drawing.Point(82, 13);
            this.LblAgtCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAgtCode.Name = "LblAgtCode";
            this.LblAgtCode.Size = new System.Drawing.Size(41, 14);
            this.LblAgtCode.TabIndex = 11;
            this.LblAgtCode.Text = "Agent";
            this.LblAgtCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAgtCode
            // 
            this.LueAgtCode.EnterMoveNextControl = true;
            this.LueAgtCode.Location = new System.Drawing.Point(126, 10);
            this.LueAgtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAgtCode.Name = "LueAgtCode";
            this.LueAgtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgtCode.Properties.Appearance.Options.UseFont = true;
            this.LueAgtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAgtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAgtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAgtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAgtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAgtCode.Properties.DropDownRows = 12;
            this.LueAgtCode.Properties.NullText = "[Empty]";
            this.LueAgtCode.Properties.PopupWidth = 500;
            this.LueAgtCode.Size = new System.Drawing.Size(293, 20);
            this.LueAgtCode.TabIndex = 12;
            this.LueAgtCode.ToolTip = "F4 : Show/hide list";
            this.LueAgtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAgtCode.EditValueChanged += new System.EventHandler(this.LueAgtCode_EditValueChanged);
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(126, 33);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Size = new System.Drawing.Size(244, 20);
            this.TxtItCode.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(58, 35);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 14);
            this.label12.TabIndex = 13;
            this.label12.Text = "Item Code";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItemName
            // 
            this.TxtItemName.EnterMoveNextControl = true;
            this.TxtItemName.Location = new System.Drawing.Point(126, 56);
            this.TxtItemName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItemName.Name = "TxtItemName";
            this.TxtItemName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItemName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItemName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItemName.Properties.Appearance.Options.UseFont = true;
            this.TxtItemName.Properties.MaxLength = 16;
            this.TxtItemName.Size = new System.Drawing.Size(293, 20);
            this.TxtItemName.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(55, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Item Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQty
            // 
            this.TxtQty.EnterMoveNextControl = true;
            this.TxtQty.Location = new System.Drawing.Point(126, 79);
            this.TxtQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty.Properties.Appearance.Options.UseFont = true;
            this.TxtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty.Size = new System.Drawing.Size(166, 20);
            this.TxtQty.TabIndex = 20;
            this.TxtQty.Validated += new System.EventHandler(this.TxtQty_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(69, 82);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 14);
            this.label11.TabIndex = 19;
            this.label11.Text = "Quantity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUomCode
            // 
            this.TxtUomCode.EnterMoveNextControl = true;
            this.TxtUomCode.Location = new System.Drawing.Point(126, 102);
            this.TxtUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUomCode.Name = "TxtUomCode";
            this.TxtUomCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUomCode.Properties.MaxLength = 16;
            this.TxtUomCode.Size = new System.Drawing.Size(166, 20);
            this.TxtUomCode.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(28, 105);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 14);
            this.label2.TabIndex = 21;
            this.label2.Text = "Sales Uom Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPrice
            // 
            this.TxtUPrice.EnterMoveNextControl = true;
            this.TxtUPrice.Location = new System.Drawing.Point(126, 125);
            this.TxtUPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice.Name = "TxtUPrice";
            this.TxtUPrice.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPrice.Size = new System.Drawing.Size(166, 20);
            this.TxtUPrice.TabIndex = 24;
            this.TxtUPrice.Validated += new System.EventHandler(this.TxtUPrice_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(64, 128);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Unit Price";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscRate1
            // 
            this.TxtDiscRate1.EnterMoveNextControl = true;
            this.TxtDiscRate1.Location = new System.Drawing.Point(126, 148);
            this.TxtDiscRate1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscRate1.Name = "TxtDiscRate1";
            this.TxtDiscRate1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscRate1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscRate1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscRate1.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscRate1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscRate1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscRate1.Size = new System.Drawing.Size(97, 20);
            this.TxtDiscRate1.TabIndex = 26;
            this.TxtDiscRate1.Validated += new System.EventHandler(this.TxtDiscRate1_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(29, 150);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 14);
            this.label5.TabIndex = 25;
            this.label5.Text = "Discount Rate 1";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscRate2
            // 
            this.TxtDiscRate2.EnterMoveNextControl = true;
            this.TxtDiscRate2.Location = new System.Drawing.Point(126, 170);
            this.TxtDiscRate2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscRate2.Name = "TxtDiscRate2";
            this.TxtDiscRate2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscRate2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscRate2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscRate2.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscRate2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscRate2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscRate2.Size = new System.Drawing.Size(97, 20);
            this.TxtDiscRate2.TabIndex = 28;
            this.TxtDiscRate2.Validated += new System.EventHandler(this.TxtDiscRate2_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(29, 172);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "Discount Rate 2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscRate3
            // 
            this.TxtDiscRate3.EnterMoveNextControl = true;
            this.TxtDiscRate3.Location = new System.Drawing.Point(126, 192);
            this.TxtDiscRate3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscRate3.Name = "TxtDiscRate3";
            this.TxtDiscRate3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscRate3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscRate3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscRate3.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscRate3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscRate3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscRate3.Size = new System.Drawing.Size(97, 20);
            this.TxtDiscRate3.TabIndex = 30;
            this.TxtDiscRate3.Validated += new System.EventHandler(this.TxtDiscRate3_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(29, 195);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 14);
            this.label7.TabIndex = 29;
            this.label7.Text = "Discount Rate 3";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscRate4
            // 
            this.TxtDiscRate4.EnterMoveNextControl = true;
            this.TxtDiscRate4.Location = new System.Drawing.Point(126, 215);
            this.TxtDiscRate4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscRate4.Name = "TxtDiscRate4";
            this.TxtDiscRate4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscRate4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscRate4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscRate4.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscRate4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscRate4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscRate4.Size = new System.Drawing.Size(97, 20);
            this.TxtDiscRate4.TabIndex = 32;
            this.TxtDiscRate4.Validated += new System.EventHandler(this.TxtDiscRate4_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(29, 219);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 14);
            this.label8.TabIndex = 31;
            this.label8.Text = "Discount Rate 4";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscRate5
            // 
            this.TxtDiscRate5.EnterMoveNextControl = true;
            this.TxtDiscRate5.Location = new System.Drawing.Point(126, 238);
            this.TxtDiscRate5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscRate5.Name = "TxtDiscRate5";
            this.TxtDiscRate5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscRate5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscRate5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscRate5.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscRate5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscRate5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscRate5.Size = new System.Drawing.Size(97, 20);
            this.TxtDiscRate5.TabIndex = 34;
            this.TxtDiscRate5.Validated += new System.EventHandler(this.TxtDiscRate5_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(29, 241);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 33;
            this.label9.Text = "Discount Rate 5";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPromoRate
            // 
            this.TxtPromoRate.EnterMoveNextControl = true;
            this.TxtPromoRate.Location = new System.Drawing.Point(126, 261);
            this.TxtPromoRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPromoRate.Name = "TxtPromoRate";
            this.TxtPromoRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPromoRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPromoRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPromoRate.Properties.Appearance.Options.UseFont = true;
            this.TxtPromoRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPromoRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPromoRate.Size = new System.Drawing.Size(166, 20);
            this.TxtPromoRate.TabIndex = 36;
            this.TxtPromoRate.Validated += new System.EventHandler(this.TxtPromoRate_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(52, 264);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 14);
            this.label10.TabIndex = 35;
            this.label10.Text = "Promo Rate";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPriceBefore
            // 
            this.TxtUPriceBefore.EnterMoveNextControl = true;
            this.TxtUPriceBefore.Location = new System.Drawing.Point(126, 284);
            this.TxtUPriceBefore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPriceBefore.Name = "TxtUPriceBefore";
            this.TxtUPriceBefore.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUPriceBefore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPriceBefore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceBefore.Properties.Appearance.Options.UseFont = true;
            this.TxtUPriceBefore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceBefore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceBefore.Size = new System.Drawing.Size(166, 20);
            this.TxtUPriceBefore.TabIndex = 38;
            this.TxtUPriceBefore.Validated += new System.EventHandler(this.TxtUPriceBefore_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(3, 287);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 14);
            this.label13.TabIndex = 37;
            this.label13.Text = "Unit Price Before Tax";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTax
            // 
            this.TxtTax.EnterMoveNextControl = true;
            this.TxtTax.Location = new System.Drawing.Point(126, 307);
            this.TxtTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTax.Name = "TxtTax";
            this.TxtTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTax.Size = new System.Drawing.Size(97, 20);
            this.TxtTax.TabIndex = 40;
            this.TxtTax.Validated += new System.EventHandler(this.TxtTax_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(67, 310);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 14);
            this.label14.TabIndex = 39;
            this.label14.Text = "Tax Rate";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(126, 330);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtAmt.TabIndex = 42;
            this.TxtAmt.Validated += new System.EventHandler(this.TxtAmt_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(44, 333);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 14);
            this.label15.TabIndex = 41;
            this.label15.Text = "Tax Amount";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPriceAfter
            // 
            this.TxtUPriceAfter.EnterMoveNextControl = true;
            this.TxtUPriceAfter.Location = new System.Drawing.Point(126, 353);
            this.TxtUPriceAfter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPriceAfter.Name = "TxtUPriceAfter";
            this.TxtUPriceAfter.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUPriceAfter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPriceAfter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPriceAfter.Properties.Appearance.Options.UseFont = true;
            this.TxtUPriceAfter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPriceAfter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPriceAfter.Size = new System.Drawing.Size(166, 20);
            this.TxtUPriceAfter.TabIndex = 44;
            this.TxtUPriceAfter.Validated += new System.EventHandler(this.TxtUPriceAfter_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(4, 356);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 14);
            this.label16.TabIndex = 43;
            this.label16.Text = "Unit Price After Tax";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(126, 376);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Size = new System.Drawing.Size(166, 20);
            this.TxtTotal.TabIndex = 46;
            this.TxtTotal.Validated += new System.EventHandler(this.TxtTotal_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(84, 378);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 14);
            this.label17.TabIndex = 45;
            this.label17.Text = "Total";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnItem
            // 
            this.BtnItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItem.Appearance.Options.UseBackColor = true;
            this.BtnItem.Appearance.Options.UseFont = true;
            this.BtnItem.Appearance.Options.UseForeColor = true;
            this.BtnItem.Appearance.Options.UseTextOptions = true;
            this.BtnItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItem.Location = new System.Drawing.Point(372, 32);
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Size = new System.Drawing.Size(24, 21);
            this.BtnItem.TabIndex = 15;
            this.BtnItem.ToolTip = "Find Item";
            this.BtnItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItem.ToolTipTitle = "Run System";
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // BtnMasterItem
            // 
            this.BtnMasterItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMasterItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMasterItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMasterItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMasterItem.Appearance.Options.UseBackColor = true;
            this.BtnMasterItem.Appearance.Options.UseFont = true;
            this.BtnMasterItem.Appearance.Options.UseForeColor = true;
            this.BtnMasterItem.Appearance.Options.UseTextOptions = true;
            this.BtnMasterItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMasterItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMasterItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnMasterItem.Image")));
            this.BtnMasterItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMasterItem.Location = new System.Drawing.Point(397, 32);
            this.BtnMasterItem.Name = "BtnMasterItem";
            this.BtnMasterItem.Size = new System.Drawing.Size(24, 21);
            this.BtnMasterItem.TabIndex = 16;
            this.BtnMasterItem.ToolTip = "Find Item";
            this.BtnMasterItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMasterItem.ToolTipTitle = "Run System";
            this.BtnMasterItem.Click += new System.EventHandler(this.BtnMasterItem_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(227, 151);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 14);
            this.label18.TabIndex = 47;
            this.label18.Text = "%";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(227, 173);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 14);
            this.label19.TabIndex = 48;
            this.label19.Text = "%";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(228, 218);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 14);
            this.label20.TabIndex = 49;
            this.label20.Text = "%";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(227, 194);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 14);
            this.label21.TabIndex = 50;
            this.label21.Text = "%";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(228, 241);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 14);
            this.label22.TabIndex = 51;
            this.label22.Text = "%";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(225, 310);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 14);
            this.label23.TabIndex = 52;
            this.label23.Text = "%";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(126, 398);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(166, 20);
            this.DteDocDt.TabIndex = 48;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(44, 400);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 14);
            this.label24.TabIndex = 47;
            this.label24.Text = "DeliveryDate";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSODlg2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmSODlg2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItemName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscRate5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPromoRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceBefore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPriceAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblAgtCode;
        private DevExpress.XtraEditors.LookUpEdit LueAgtCode;
        public DevExpress.XtraEditors.SimpleButton BtnMasterItem;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtItemName;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtQty;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtPromoRate;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtDiscRate5;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtDiscRate4;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtDiscRate3;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtDiscRate2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtDiscRate1;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtUomCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtUPriceAfter;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTax;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtUPriceBefore;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.SimpleButton BtnItem;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label24;
    }
}