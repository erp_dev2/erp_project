﻿#region Update
/*
    23/04/2020 [DITA/IMS] new apps
    21/04/2021 [WED/ALL] COA bisa dipilih lebih dari sekali berdasarkan parameter IsCOACouldBeChosenMoreThanOnce
    30/04/2021 [VIN/ALL] tambah param IsSOCDPSettlementJournalReverse
    18/06/2021 [VIN/IMS] tambah validasi closing jurnal tidak bisa save
    23/06/2021 [RDA/IMS] tambah show data Local Document#
    23/06/2021 [DITA/IMS] outstanding didapat dari outstanding sebelumnya dikurangin nilai settled nya
    27/07/2021 [ICA/IMS] penyesuaian journal, otomatis save CC Keuangan jika ada coa kepala 5 dan value CC masih kosong. Berdasarkan param CostCenterFormulaForAPAR = 2.
    29/07/2021 [IBL/IMS] BUG: journal cancel belum menyimpan CCCode
    16/09/2021 [MYA/ALL] Validasi tidak bisa SAVE untuk transaksi yang terbentuk jurnal otomatis ketika terdapat setting COA otomatis yang masih kosong (baik dari Menu Master Data maupun System's Parameter) sehingga terhindar dari jurnal yang tidak seimbang.
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDPSettlement : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mCtCode = string.Empty, mMainCurCode = string.Empty, mSOContractDocNo = string.Empty,
            mCostCenterFormulaForAPAR = string.Empty;
        private bool mIsAutoJournalActived = false,
            mIsCheckCOAJournalNotExists = false;
        internal bool mIsCOACouldBeChosenMoreThanOnce = false,
            mIsSOCDPSettlementJournalReverse = false;
        internal FrmSOContractDPSettlementFind FrmFind;

        #endregion

        #region Constructor

        public FrmSOContractDPSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "SO Contract's Termin/Downpayment Settlement";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6
                        "Duplicated"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 6 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark, MeeCancelReason }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnSOContractDownpaymentDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5 });
                    BtnSOContractDownpaymentDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeCancelReason }, false);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtSOContractDownpaymentDocNo, TxtCtCode, 
                TxtCurCode, MeeRemark, TxtVoucherRequestDocNo, TxtJournalDocNo, TxtJournalDocNo2, TxtLocalDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtSOContractDownpaymentAmt, TxtOutstandingAmt, TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContractDPSettlementFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

       
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && 
                TxtDocNo.Text.Length == 0 &&
                !Sm.IsTxtEmpty(TxtSOContractDownpaymentDocNo, "SO Contract's Termin/Downpayment#", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSOContractDPSettlementDlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmtWithCOA();
                Sm.GrdEnter(Grd1, e);
                Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtSOContractDownpaymentDocNo, "SO Contract's Termin/Downpayment#", false))
                Sm.FormShowDialog(new FrmSOContractDPSettlementDlg(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Grd1.Cells[e.RowIndex, 4].Value = 0m;
                ComputeAmtWithCOA();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Grd1.Cells[e.RowIndex, 3].Value = 0m;
                ComputeAmtWithCOA();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOContractDPSettlement", "TblSOContractDPSettlementHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractDPSettlementHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSOContractDPSettlementDtl(DocNo, Row));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtSOContractDownpaymentDocNo, "SO Contract's Termin/Downpayment#", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Settlement Amount", true) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                //IsDataAlreadyProcessedToVoucher() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsJournalAmtNotBalanced() ||
                IsSettlementBiggerThanOutstanding() ||
                IsSettlementSmallerThanZero() ||
                IsJournalSettingInvalid() ||
                IsDuplicateCOANotHaveRemark();
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;
            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            if (IsJournalSettingInvalid_COAEmpty(Msg)) return true;


            return false;
        }

        private bool IsJournalSettingInvalid_COAEmpty(string Msg)
        {
            string AcNo = string.Empty, AcDesc = string.Empty;
            int CountGrd = Grd1.Rows.Count - 1; 

            for (int r = 0; r < CountGrd; r++)
            {
                AcNo = Sm.GetGrdStr(Grd1, r, 1);
                AcDesc = Sm.GetGrdStr(Grd1, r, 2);

                if (AcNo == string.Empty && AcDesc.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Item's COA account# (" + AcDesc + ") is empty.");
                    return true;
                }
            }

            return false;
        }

        private bool IsDuplicateCOANotHaveRemark()
        {
            if (!mIsCOACouldBeChosenMoreThanOnce) return false;
            if (Grd1.Rows.Count <= 1) return false;

            GetDuplicateCOAIndicator();

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd1, i, 6) && Sm.GetGrdStr(Grd1, i, 5).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to fill this remark due to account duplication.");
                    Sm.FocusGrd(Grd1, i, 5);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 COA's account.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "COA data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsSettlementBiggerThanOutstanding()
        {
            //ComputeOutstandingAmt();
            decimal 
                OutstandingAmt = Decimal.Parse(TxtOutstandingAmt.Text), 
                Amt = Decimal.Parse(TxtAmt.Text);
            if (Amt > OutstandingAmt)
            {
                Sm.StdMsg(mMsgType.Warning, "Settled amount is bigger than outstanding amount.");
                return true;
            }
            return false;
        }

        private bool IsSettlementSmallerThanZero()
        {
            decimal Amt = Decimal.Parse(TxtAmt.Text);
            if (Amt<= 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Settlemet amount must be greater than 0.");
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd1, Row, 3);
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd1, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSOContractDPSettlementHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Set @VoucherRequestDocNo:=(Select VoucherRequestDocNo From TblSOContractDownpaymentHdr Where DocNo=@SOContractDownpaymentDocNo); ");

            //SQL.AppendLine("Set @DNo:=(Select Max(Convert(DNo, Decimal))+1 From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo); ");

            SQL.AppendLine("Insert Into TblSOContractDPSettlementHdr(DocNo, DocDt, CancelReason, CancelInd, SOContractDownpaymentDocNo, OutstandingAmt, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @SOContractDownpaymentDocNo, @OutstandingAmt, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            //SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select VoucherRequestDocNo, Right(Concat('000', Convert(@DNo, Char)), 3), Concat('Settlement : ', @DocNo), -1.00*@Amt, @Remark, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblSOContractDownpaymentHdr ");
            //SQL.AppendLine("Where DocNo=@SOContractDownpaymentDocNo; ");

            //SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            //SQL.AppendLine("    Amt=(Select Sum(Amt) From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo), ");
            //SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SOContractDownpaymentDocNo", TxtSOContractDownpaymentDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@OutstandingAmt", Decimal.Parse(TxtOutstandingAmt.Text) - Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSOContractDPSettlementDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSOContractDPSettlementDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractDPSettlementHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('ARDP For Project Settlement : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblSOContractDPSettlementHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, ");
            if (mIsCOACouldBeChosenMoreThanOnce)
                SQL.AppendLine("Remark, ");
            SQL.AppendLine("SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, B.DNo, B.AcNo, ");
            if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
                SQL.AppendLine("B.DAmt, B.CAMt, ");
            else
                SQL.AppendLine("B.CAmt*IfNull(C.Amt, 0.00) As DAmt, B.DAMt*IfNull(C.Amt, 0.00) As CAmt, ");

            if (mIsCOACouldBeChosenMoreThanOnce)
                SQL.AppendLine("B.Remark, ");
            SQL.AppendLine("E.SOContractDocNo, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblSOContractDPSettlementDtl B On B.DocNo=@DocNo ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                SQL.AppendLine(") C On 0=0 ");
            }
            SQL.AppendLine("Inner Join TblSOContractDPSettlementHdr D On B.DocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblSOContractDownpaymentHdr E On D.SOContractDownpaymentDocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo Order By B.AcNo;");

            if (mCostCenterFormulaForAPAR == "2")
            {
                SQL.AppendLine("Update TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblSOContractDPSettlementHdr B On A.DocNo = B.JournalDocNo ");
                SQL.AppendLine("Inner Join TblParameter C On C.ParCode = 'CCCodeForJournalAPAR' And C.ParValue Is Not Null ");
                SQL.AppendLine("Set A.CCCode = C.ParValue ");
                SQL.AppendLine("Where B.DocNo = @DocNo ");
                SQL.AppendLine("And A.CCCode Is Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblJournalDtl ");
                SQL.AppendLine("    Where DocNo = A.DocNo And AcNo Like '5.%' ");
                SQL.AppendLine("); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(MeeRemark.Text, " | SO Contract# : ", mSOContractDocNo));

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelSOContractDPSettlementHdr());
            if (mIsAutoJournalActived) cml.Add(SaveJournal2());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment();
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblIncomingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.InvoiceDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            string IncomingDocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            if (IncomingDocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to Incoming Payment #" + IncomingDocNo);
                return true;
            }

            return false;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSOContractDPSettlementHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");   
        }

        private MySqlCommand CancelSOContractDPSettlementHdr()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Set @VoucherRequestDocNo:=(Select VoucherRequestDocNo From TblSOContractDownpaymentHdr Where DocNo=@SOContractDownpaymentDocNo); ");

            SQL.AppendLine("Update TblSOContractDPSettlementHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            //SQL.AppendLine("Update TblVoucherRequestDtl Set ");
            //SQL.AppendLine("    Amt=0.00, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where DocNo=@VoucherRequestDocNo ");
            //SQL.AppendLine("And Description=Concat('Settlement : ', @DocNo); ");

            //SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            //SQL.AppendLine("    Amt=(Select Sum(Amt) From TblVoucherRequestDtl Where DocNo=@VoucherRequestDocNo), ");
            //SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SOContractDownpaymentDocNo", TxtSOContractDownpaymentDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblSOContractDPSettlementHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblSOContractDPSettlementHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, Remark, SOContractDocNo, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblSOContractDPSettlementHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSOContractDPSettlementHdr(DocNo);
                ShowSOContractDPSettlementDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractDPSettlementHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.SOContractDownpaymentDocNo, C.CtCode, D.CtName, ");
            SQL.AppendLine("B.CurCode, B.Amt As SOContractDownpaymentAmt, A.OutstandingAmt, A.Amt, A.Remark, B.VoucherRequestDocNo, A.JournalDocNo, A.JournalDocNo2, B.LocalDocNo ");
            SQL.AppendLine("From TblSOContractDPSettlementHdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpaymentHdr B On A.SOContractDownpaymentDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOContractDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer D On C.CtCode=D.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "SOContractDownpaymentDocNo", "CtCode", 
                        //6-10
                        "CtName", "CurCode", "SOContractDownpaymentAmt", "OutstandingAmt", "Amt",  
                        //11-15
                        "Remark", "VoucherRequestDocNo", "JournalDocNo", "JournalDocNo2", "LocalDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtSOContractDownpaymentDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        mCtCode = Sm.DrStr(dr, c[5]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSOContractDownpaymentAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtOutstandingAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[13]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[14]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[15]);
                    }, true
                );
        }

        private void ShowSOContractDPSettlementDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ACNo, B.ACDesc, A.DAmt, A.CAmt, A.Remark  ");
            SQL.AppendLine("From TblSOContractDPSettlementDtl A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetDuplicateCOAIndicator()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    string AcNo1 = Sm.GetGrdStr(Grd1, i, 1);
                    for (int j = (i + 1); j < Grd1.Rows.Count - 1; ++j)
                    {
                        string AcNo2 = Sm.GetGrdStr(Grd1, j, 1);

                        if (AcNo1 == AcNo2)
                        {
                            Grd1.Cells[i, 6].Value = true;
                            Grd1.Cells[j, 6].Value = true;
                        }
                    }
                }
            }
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsCOACouldBeChosenMoreThanOnce = Sm.GetParameterBoo("IsCOACouldBeChosenMoreThanOnce");
            mIsSOCDPSettlementJournalReverse = Sm.GetParameterBoo("IsSOCDPSettlementJournalReverse");
            mCostCenterFormulaForAPAR = Sm.GetParameter("CostCenterFormulaForAPAR");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");

            if (mCostCenterFormulaForAPAR.Length == 0) mCostCenterFormulaForAPAR = "1";
        }

        internal void ComputeAmtWithCOA()
        {
            var Amt = 0m;
            var AcNo = string.Concat(Sm.GetParameter("CustomerAcNoAR"), mCtCode);
            var AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", AcNo);
            if (AcType.Length > 0)
            {
                if (!mIsSOCDPSettlementJournalReverse)
                {
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd1, r, 1)))
                        {
                            if (Sm.GetGrdDec(Grd1, r, 3) != 0m)
                            {
                                if (AcType == "C")
                                    Amt -= Sm.GetGrdDec(Grd1, r, 3);
                                else
                                    Amt += Sm.GetGrdDec(Grd1, r, 3);
                            }
                            if (Sm.GetGrdDec(Grd1, r, 4) != 0m)
                            {
                                if (AcType == "C")
                                    Amt += Sm.GetGrdDec(Grd1, r, 4);
                                else
                                    Amt -= Sm.GetGrdDec(Grd1, r, 4);
                            }
                        }
                    }
                }
                else
                {
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    {
                        if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd1, r, 1)))
                        {
                            if (Sm.GetGrdDec(Grd1, r, 3) != 0m)
                            {
                                if (AcType == "D")
                                    Amt -= Sm.GetGrdDec(Grd1, r, 3);
                                else
                                    Amt += Sm.GetGrdDec(Grd1, r, 3);
                            }
                            if (Sm.GetGrdDec(Grd1, r, 4) != 0m)
                            {
                                if (AcType == "D")
                                    Amt += Sm.GetGrdDec(Grd1, r, 4);
                                else
                                    Amt -= Sm.GetGrdDec(Grd1, r, 4);
                            }
                        }
                    }
                }
            }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        if (!mIsCOACouldBeChosenMoreThanOnce)
                            SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        public void ComputeOutstandingAmt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Amt-IfNull(B.Amt, 0.00) As OutstandingAmt ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblSOContractDPSettlementHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And SOContractDownpaymentDocNo=@Param ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.DocNo=@Param; ");

            var Amt = Sm.GetValueDec(SQL.ToString(), TxtSOContractDownpaymentDocNo.Text);
            TxtOutstandingAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button Event

        private void BtnSOContractDownpaymentDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FrmSOContractDPSettlementDlg2(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnSOContractDownpaymentDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDownpaymentDocNo, "SO Contract Downpayment", false))
            {
                try
                {
                    var f1 = new FrmSOContractDownpayment("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = TxtSOContractDownpaymentDocNo.Text;
                    f1.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                var f = new FrmVoucherRequest("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion


    }
}
