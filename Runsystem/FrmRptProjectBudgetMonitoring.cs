﻿#region Update
/*
    05/12/2020 [DITA/IMS] New Apps
    12/08/2020 [DITA/IMS] Ubah link receiving ujntuk account ambil dai item category nya
    09/04/2021 [WED/IMS] passing row pas klik dialog Recv
    26/07/2021 [VIN/IMS] Process 2, Period journalhdr bukan dtl
    05/08/2021 [TKG/IMS] tambah baris total di grid
    10/08/2021 [VIN/IMS] tambah param IsMovingAvgEnable 
    01/11/2021 [DITA/IMS] di printout, nilai sales musti dikasi exc rate nya
    02/11/2021 [DITA/IMS] semua amount yang berhubungan dengan so contract dikalikan dengan rate
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetMonitoring : RunSystem.FrmBase6
    {
        #region Field

        internal string
           mAccessInd = string.Empty,
           mMenuCode = string.Empty,
           mSelectedCOA = string.Empty,
           mMainCurCode = string.Empty
           ;
        private string
           mSQL = string.Empty,
           mEndDt = string.Empty;
        internal bool
            mIsAccountingRptUseJournalPeriod = false,
            mIsMovingAvgEnabled = false
            ;
        internal decimal mSOContractAmt = 0m;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetMonitoring(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                var CurrentDateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteDocDt1.DateTime = CurrentDateTime;
                DteDocDt2.DateTime = CurrentDateTime;
                SetNumValueZero();
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Account#",
                    "Account Name", 
                    "Bid & Pricing"+Environment.NewLine+"Quotation",
                    "Financial Budget",
                    "Receiving Item"+Environment.NewLine+"From Vendor",

                    //6-10
                    "Receiving DocNo",
                    "",
                    "Realization To"+Environment.NewLine+mEndDt,
                    "Journal DocNo",
                    "",

                    //11-12
                    "Realization(%)",
                    "Remark"
                },
                new int[] 
                {
                    //0
                    25,

                    //1-5
                    250, 250, 150, 150, 150,

                    //6-10
                    0, 20, 150, 0, 20,

                    //11-12
                    100, 200

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 8, 11 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 7, 10 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2,3, 4, 5, 6, 8, 9, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 9 }, false);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract", false)||
                Sm.IsDteEmpty(DteDocDt1, "Start Date") ||
                Sm.IsDteEmpty(DteDocDt2, "End Date")||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            var Dt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            var Dt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            mSelectedCOA = string.Empty;
            string mSOContratDocNo = TxtSOContractDocNo.Text;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProjectBudgetMonitoringDtl>();
                var l2 = new List<RecvVd>();
                var l3 = new List<COAJournal>();
                var l4 = new List<ProjectBudgetMonitoringHdr>();

                PrepDataProjectBudgetMonitoringDtl(ref l, mSOContratDocNo);
                if (l.Count > 0)
                {
                    mSelectedCOA = GetSelectedCOA(ref l);
                    Process1(ref l2, mSelectedCOA, mSOContratDocNo, Dt1, Dt2);
                    Process2(ref l3, mSelectedCOA, mSOContratDocNo, Dt1, Dt2);
                    Process3(ref l, ref l2, ref l3);
                    Process4(ref l, ref l4);
                    Process5(ref l, ref l4);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);


                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmRptProjectBudgetMonitoringDlg2(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mRecvVdDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.mSelectedCOA = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmRptProjectBudgetMonitoringDlg3(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mJournalDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.mAcNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmRptProjectBudgetMonitoringDlg2(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mRecvVdDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.mSelectedCOA = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmRptProjectBudgetMonitoringDlg3(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mJournalDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.mAcNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }


        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetNumValueZero()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> {
                TxtContractAmt, TxtVCBidPricingAndQt, TxtVCFinancialBudget, TxtVCRealization, TxtVCRealizationPercentage, TxtVCRecvVd,
                TxtCMBidPricingAndQt, TxtCMBidPricingAndQtPercentage, TxtCMFinancialBudget, TxtCMFinancialBudgetPercentage ,TxtCMRealization, TxtCMRealizationPercentage, TxtCMRecvVd, TxtCMRecvVdPercentage
            }, 0);
        }

        private void GetParameter()
        {
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mMainCurCode = Sm.GetParameter("MainCurCode");

        }

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        private void PrepDataProjectBudgetMonitoringDtl(ref List<ProjectBudgetMonitoringDtl> l, string mSOContractDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, C.AcDesc, B.BidPricingQtAmt, B.FinancialBudgetAmt, B.Remark ");
            SQL.AppendLine("FROM TblProjectBudgetResourceHdr A ");
            SQL.AppendLine("INNER JOIN TblProjectBudgetResourceDtl B ON A.DocNo=B.DocNo AND A.SOContractDocNo=@SOCOntractDocNo ");
            SQL.AppendLine("INNER JOIN TblCoa C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.Status = 'A' And A.ProcessInd = 'F'; ");
            

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SOCOntractDocNo", mSOContractDocNo);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "AcNo", 

                    //1-4
                    "AcDesc", 
                    "BidPricingQtAmt" ,
                    "FinancialBudgetAmt",
                    "Remark",
                    
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProjectBudgetMonitoringDtl()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            BidAndPricingQtAmt = Sm.DrDec(dr, c[2]),
                            FinancialBudgetAmt = Sm.DrDec(dr, c[3]),
                            Remark = Sm.DrStr(dr, c[4]),
                            RecvVdDocNo = string.Empty,
                            RecvVdAmt = 0m,
                            RealizationDocNo = string.Empty,
                            RealizationToEndDtAmt = 0m,
                            RealizationPercentage = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<ProjectBudgetMonitoringDtl> l)
        {
            string AcNo = string.Empty;

            foreach (var x in l)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        //Process amt Recv Vd
        private void Process1(ref List<RecvVd> l2, string mSelectedCOA, string mSOContractDocNo, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT sum((A.Qty*D.UPrice)) Amt, GROUP_CONCAT( distinct A.DocNo) DocNo, G.ACNo2 As AcNo ");
            SQL.AppendLine("From TblRecvVdDtl A  ");
            SQL.AppendLine("Inner Join TblRecvVdHdr A1 ON A.DocNo = A1.DocNo AND A.CancelInd = 'N' AND A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo   ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo  ");
            SQL.AppendLine("Inner Join TblQtDtl D ON C.QtDocNo = D.DocNo AND C.QtDNo = D.Dno ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo  ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr E1 ON E.DocNo = E1.DocNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode  ");
            SQL.AppendLine("Inner Join TblItemCategory G ON G.ItCtCode = F.ItCtCode ");
            //SQL.AppendLine("Inner Join TblCostCategory H ON G.CCtCode = H.CCtCode AND H.CCCode = G.CCCode ");
            SQL.AppendLine("WHERE A1.DocDt BETWEEN @DocDt1 AND @DocDt2 AND E1.SOCDocNo = @SOContractDocNo ");
            SQL.AppendLine("AND Find_In_Set(G.ACNo2, @SelectedCOA) ");
            SQL.AppendLine("GROUP BY G.ACNo2; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SOCOntractDocNo", mSOContractDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", mSelectedCOA);
                Sm.CmParam<String>(ref cm, "@DocDt1", Dt1);
                Sm.CmParam<String>(ref cm, "@DocDt2", Dt2);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "AcNo", 

                    //1-4
                    "DocNo", 
                    "Amt" ,
                    
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new RecvVd()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),

                        });
                    }
                }
                dr.Close();
            }
        }

        //Process amt Journal
        private void Process2(ref List<COAJournal> l3, string mSelectedCOA, string mSOContractDocNo, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.AcNo, A.AcType, B.DocNo, ");
            SQL.AppendLine("Case A.AcType When 'D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End As Amt ");
            SQL.AppendLine("FROM TblCOA A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("	SELECT B.AcNo, GROUP_CONCAT(DISTINCT B.DocNo) DocNo, SUM(B.DAmt) DAmt, SUM(B.CAmt)CAmt ");
            SQL.AppendLine("	FROM TblJournalHdr A ");
            SQL.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo And B.SOContractDocNo=@SOContractDocNo ");
            SQL.AppendLine("	WHERE FIND_IN_SET(B.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And A.Period Is Not Null ");
                SQL.AppendLine("And Left(A.Period, 6)>= Left(@Dt1, 6)");
                SQL.AppendLine("And Left(A.Period, 6)<= Left(@Dt2, 6) ");
            }
            else
            {
                SQL.AppendLine("	AND A.DocDt BETWEEN @Dt1 And @Dt2 ");
            }
            SQL.AppendLine("GROUP BY B.AcNo ");
            SQL.AppendLine(") B On A.AcNo = B.AcNo ");

            SQL.AppendLine("AND A.ActInd = 'Y' ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
                Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", mSOContractDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", mSelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType", "DocNo", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DocNo = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),

                        });
                    }
                }
                dr.Close();
            }
        }

        // ceplokin Amount ke list Reporting
        private void Process3(ref List<ProjectBudgetMonitoringDtl> l, ref List<RecvVd> l2, ref List<COAJournal> l3)
        {
            foreach (var x in l2)
            {
                foreach (var y in l.Where(w => w.AcNo == x.AcNo))
                {
                    y.RecvVdDocNo = x.DocNo;
                    y.RecvVdAmt = x.Amt;
                }
            }

            foreach (var z in l3)
            {
                foreach (var y in l.Where(w => w.AcNo == z.AcNo))
                {
                    y.RealizationDocNo = z.DocNo;
                    y.RealizationToEndDtAmt = z.Amt;
                }
            }

            foreach (var x in l)
            {
                if (x.FinancialBudgetAmt != 0m)
                    x.RealizationPercentage = (x.RealizationToEndDtAmt / x.FinancialBudgetAmt) * 100m;
            }
            
        }

        //ceplokin data ke list header
        private void Process4(ref List<ProjectBudgetMonitoringDtl> l, ref List<ProjectBudgetMonitoringHdr> l4)
        {
            decimal VCBidPricingAndQt = 0m, VCFinancialBudget = 0m, VCRecvVd = 0m, VCRealization = 0m; 
             //variable cost
                foreach(var x in l)
                {
                    VCBidPricingAndQt += x.BidAndPricingQtAmt;
                    VCFinancialBudget += x.FinancialBudgetAmt;
                    VCRecvVd += x.RecvVdAmt;
                    VCRealization += x.RealizationToEndDtAmt;
                }
                l4.Add(new ProjectBudgetMonitoringHdr()
                {
                    ContractAmt = mSOContractAmt,
                    VCBidPricingAndQt = VCBidPricingAndQt,
                    VCFinancialBudget = VCFinancialBudget,
                    VCRecvVd = VCRecvVd,
                    VCRealization = VCRealization,
                    VCRealizationPercentage = VCFinancialBudget != 0m ? VCRealization / VCFinancialBudget * 100 : 0m,

                    //contribution margin
                    CMBidPricingAndQt = mSOContractAmt-VCBidPricingAndQt,
                    CMFinancialBudget = mSOContractAmt-VCFinancialBudget,
                    CMRecvVd= mSOContractAmt-VCRecvVd,
                    CMRealization = mSOContractAmt-VCRealization,
                    CMBidPricingAndQtPercentage = mSOContractAmt != 0 ? ((mSOContractAmt-VCBidPricingAndQt)/mSOContractAmt)*100 : 0m,
                    CMFinancialPercentage = mSOContractAmt != 0 ? ((mSOContractAmt - VCFinancialBudget) / mSOContractAmt) * 100 : 0m,
                    CMRecvVdPercentage = mSOContractAmt != 0 ? ((mSOContractAmt - VCRecvVd) / mSOContractAmt) * 100 : 0m,
                    CMRealizationPercentage = mSOContractAmt != 0 ? ((mSOContractAmt - VCRealization) / mSOContractAmt) * 100 : 0m,
                });

            

        }

        // ceplokin data ke grid
        private void Process5(ref List<ProjectBudgetMonitoringDtl> l, ref List<ProjectBudgetMonitoringHdr> l4)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.FormatNum(x.BidAndPricingQtAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.FinancialBudgetAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.RecvVdAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = x.RecvVdDocNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.FormatNum(x.RealizationToEndDtAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = x.RealizationDocNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 11].Value = Sm.FormatNum(x.RealizationPercentage, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 12].Value = x.Remark;
                Row += 1;
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 8, 11 });

            Grd1.EndUpdate();

            foreach (var y in l4)
            {
                TxtContractAmt.EditValue = Sm.FormatNum(y.ContractAmt, 0);
                TxtVCBidPricingAndQt.EditValue = Sm.FormatNum(y.VCBidPricingAndQt, 0);
                TxtVCFinancialBudget.EditValue = Sm.FormatNum(y.VCFinancialBudget, 0);
                TxtVCRecvVd.EditValue = Sm.FormatNum(y.VCRecvVd, 0);
                TxtVCRealization.EditValue = Sm.FormatNum(y.VCRealization, 0);
                TxtVCRealizationPercentage.EditValue = Sm.FormatNum(y.VCRealizationPercentage, 0);
                TxtCMBidPricingAndQt.EditValue = Sm.FormatNum(y.CMBidPricingAndQt, 0);
                TxtCMFinancialBudget.EditValue = Sm.FormatNum(y.CMFinancialBudget, 0);
                TxtCMRecvVd.EditValue = Sm.FormatNum(y.CMRecvVd, 0);
                TxtCMRealization.EditValue = Sm.FormatNum(y.CMRealization, 0);
                TxtCMBidPricingAndQtPercentage.EditValue = Sm.FormatNum(y.CMBidPricingAndQtPercentage, 0);
                TxtCMFinancialBudgetPercentage.EditValue = Sm.FormatNum(y.CMFinancialPercentage, 0);
                TxtCMRecvVdPercentage.EditValue = Sm.FormatNum(y.CMRecvVdPercentage, 0);
                TxtCMRealizationPercentage.EditValue = Sm.FormatNum(y.CMRealizationPercentage, 0);

            }
            
        }

        private void ParPrint()
        {
            var l = new List<PrintoutProjectBudgetMonitoringHdr>();
            var l2 = new List<PrintoutProjectBudgetMonitoringDtl>();
            decimal Numb = 0;
            string[] TableName = { "PrintoutProjectBudgetMonitoringHdr", "PrintoutProjectBudgetMonitoringDtl" };
            List<IList> myLists = new List<IList>();
            
            #region Header

            l.Add(new PrintoutProjectBudgetMonitoringHdr()
            {
                 CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                 Dt1 = DteDocDt1.Text,
                 Dt2 = DteDocDt2.Text,
                 SOContractDocNo = TxtSOContractDocNo.Text,
                 ProjectCode = TxtProjectCode.Text,
                 ProjectName = TxtProjectName.Text,
                 PONo = TxtPONo.Text,
                 Customer = TxtCtName.Text,
                 ContractAmt = mSOContractAmt,
                 VCBidPricingAndQt = Decimal.Parse(TxtVCBidPricingAndQt.Text),
                 VCFinancialBudget = Decimal.Parse(TxtVCFinancialBudget.Text),
                 VCRealization = Decimal.Parse(TxtVCRealization.Text),
                 VCRealizationPercentage = Decimal.Parse(TxtVCRealizationPercentage.Text),
                 VCRecvVd = Decimal.Parse(TxtVCRecvVd.Text),
                 CMBidPricingAndQt = Decimal.Parse(TxtCMBidPricingAndQt.Text),
                 CMBidPricingAndQtPercentage = Decimal.Parse(TxtCMBidPricingAndQtPercentage.Text),
                 CMFinancialBudget = Decimal.Parse(TxtCMFinancialBudget.Text),
                 CMFinancialPercentage = Decimal.Parse(TxtCMFinancialBudgetPercentage.Text),
                 CMRealization = Decimal.Parse(TxtCMRealization.Text),
                 CMRealizationPercentage = Decimal.Parse(TxtCMRealizationPercentage.Text),
                 CMRecvVd = Decimal.Parse(TxtCMRecvVd.Text),
                 CMRecvVdPercentage = Decimal.Parse(TxtCMRecvVdPercentage.Text)
             });

            myLists.Add(l);

            #endregion

            #region Detail
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                Numb = Numb + 1;

                l2.Add(new PrintoutProjectBudgetMonitoringDtl()
                {
                    No = Numb,
                    AcNo =  Sm.GetGrdStr(Grd1, i, 1),
                    AcDesc =  Sm.GetGrdStr(Grd1, i, 2),
                    BidAndPricingQtAmt =  Sm.GetGrdDec(Grd1, i, 3),
                    FinancialBudgetAmt = Sm.GetGrdDec(Grd1, i, 4),
                    RecvVdAmt = Sm.GetGrdDec(Grd1, i, 5),
                    RealizationToEndDtAmt = Sm.GetGrdDec(Grd1, i, 8),
                    RealizationPercentage = Sm.GetGrdDec(Grd1, i, 11),
                    Remark = Sm.GetGrdStr(Grd1, i, 12),

                   
                });
            }
            myLists.Add(l2);

            #endregion

            Sm.PrintReport("RptProjectBudgetMonitoring", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Events

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
           Sm.FormShowDialog(new FrmRptProjectBudgetMonitoringDlg(this));

        }

        #endregion

        #region Class

        private class ProjectBudgetMonitoringHdr
        {
            public decimal ContractAmt { get; set; }
            public decimal VCBidPricingAndQt { get; set; }
            public decimal VCFinancialBudget { get; set; }
            public decimal VCRealization { get; set; }
            public decimal VCRealizationPercentage { get; set; }
            public decimal VCRecvVd { get; set; }
            public decimal CMBidPricingAndQt { get; set; }
            public decimal CMBidPricingAndQtPercentage { get; set; }
            public decimal CMFinancialBudget { get; set; }
            public decimal CMFinancialPercentage { get; set; }
            public decimal CMRealization { get; set; }
            public decimal CMRealizationPercentage { get; set; }
            public decimal CMRecvVd { get; set; }
            public decimal CMRecvVdPercentage {get; set;}
        }

        private class ProjectBudgetMonitoringDtl
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal BidAndPricingQtAmt { get; set; }
            public decimal FinancialBudgetAmt { get; set; }
            public string RecvVdDocNo { get; set; }
            public decimal RecvVdAmt { get; set; }
            public string RealizationDocNo{ get; set; }
            public decimal RealizationToEndDtAmt { get; set; }
            public decimal RealizationPercentage { get; set; }
            public string Remark { get; set; }
        }

        private class RecvVd
        {
            public string DocNo { get; set; }
            public decimal Amt { get; set; }
            public string AcNo { get; set; }
        }

        private class COAJournal
        {
            public string AcType{ get; set; }
            public string AcNo { get; set; }
            public string DocNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class PrintoutProjectBudgetMonitoringHdr
        {
            public string CompanyLogo { get; set; }
            public string SOContractDocNo { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string PONo { get; set; }
            public string Customer { get; set; }
            public string Dt1 { get; set; }
            public string Dt2 { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal VCBidPricingAndQt { get; set; }
            public decimal VCFinancialBudget { get; set; }
            public decimal VCRealization { get; set; }
            public decimal VCRealizationPercentage { get; set; }
            public decimal VCRecvVd { get; set; }
            public decimal CMBidPricingAndQt { get; set; }
            public decimal CMBidPricingAndQtPercentage { get; set; }
            public decimal CMFinancialBudget { get; set; }
            public decimal CMFinancialPercentage { get; set; }
            public decimal CMRealization { get; set; }
            public decimal CMRealizationPercentage { get; set; }
            public decimal CMRecvVd { get; set; }
            public decimal CMRecvVdPercentage { get; set; }
        }

        private class PrintoutProjectBudgetMonitoringDtl
        {
            public decimal No { get ; set;}
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal BidAndPricingQtAmt { get; set; }
            public decimal FinancialBudgetAmt { get; set; }
            public decimal RecvVdAmt { get; set; }
            public decimal RealizationToEndDtAmt { get; set; }
            public decimal RealizationPercentage { get; set; }
            public string Remark { get; set; }
        }


      
        #endregion

        
    }
}
