﻿#region Update
/*
    31/10/2017 [WED] tambah tarik data dari personal information sesuai position yang dipilih
    26/03/2018 [WED] tambah education dan experience
    31/03/2018 [TKG] tambah filter site dan level 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProposeCandidateDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProposeCandidate mFrmParent;
        private string 
            mSQL = string.Empty, 
            mCompetenceCode = string.Empty, 
            mPosCode = string.Empty,
            mPosCode2 = string.Empty,
            mExperienceCode = string.Empty,
            mEducationCode = string.Empty;

        #endregion

        #region Constructor

        public FrmProposeCandidateDlg2(FrmProposeCandidate FrmParent, string CompetenceCode, string PosCode, string PosCode2, string ExperienceCode, string EducationCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCompetenceCode = CompetenceCode;
            mPosCode2 = PosCode2;
            mExperienceCode = ExperienceCode;
            mEducationCode = EducationCode;
            mPosCode = PosCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Employee Code",
                        "",
                        "Employee Name", 
                        "Match Competence",

                        //6-10
                        "DocType",
                        "Match"+Environment.NewLine+"Education Level",
                        "Match Experience",
                        "Minimum Age"+Environment.NewLine+"(year)",
                        "Maximum Age"+Environment.NewLine+"(year)",

                        //11
                        "Employee's"+Environment.NewLine+"Age (year)"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 20, 200, 200,

                        //6-10
                        0, 250, 200, 120, 120,

                        //11
                        120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11 }, 2);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            
            SQL.AppendLine("Select '1' As DocType, X.EmpCode, X.EmpName, X.Competence, X.Education, X.Experience, X.MinAge, X.MaxAge, X.EmpAge ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, B.Competence, C.Education, D.Experience, ");
            SQL.AppendLine("    IfNull(G.MinAge, 0) MinAge, IfNull(G.MaxAge, 0) MaxAge, TimeStampDiff(Year, IfNull(A.BirthDt, Now()), Now()) As EmpAge ");
	        SQL.AppendLine("    From TblEmployee A ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select T1.EmpCode, Group_Concat(T2.CompetenceName Separator ', ') As Competence ");
		    SQL.AppendLine("        From ");
		    SQL.AppendLine("        (	 ");
			SQL.AppendLine("            Select X1.EmpCode, X1.CompetenceCode ");
			SQL.AppendLine("            From TblEmployeeCompetence X1 ");
			SQL.AppendLine("            Where Find_In_Set(X1.CompetenceCode, @CompetenceCode) ");
		    SQL.AppendLine("        )T1 ");
		    SQL.AppendLine("        Inner Join TblCompetence T2 On T1.CompetenceCode = T2.CompetenceCode	 ");
		    SQL.AppendLine("        Group BY T1.EmpCode ");
	        SQL.AppendLine("    ) B On A.EmpCode = B.EmpCode ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Distinct T3.EmpCode, T5.OptDesc As Education ");
		    SQL.AppendLine("        From ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select X2.EmpCode ");
			SQL.AppendLine("            From TblEmployeeEducation X2 ");
			SQL.AppendLine("            Where Find_In_Set(X2.Level, @EducationCode) ");
		    SQL.AppendLine("        )T3  ");
		    SQL.AppendLine("        Inner Join ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select X3.EmpCode, MIN(X3.Level) As Level ");
			SQL.AppendLine("            From TblEmployeeEducation X3 ");
			SQL.AppendLine("            Group By X3.EmpCode ");
		    SQL.AppendLine("        ) T4 On T3.EmpCode = T4.EmpCode ");
		    SQL.AppendLine("        Inner Join TblOption T5 On T5.OptCode = T4.Level And T5.OptCat = 'EmployeeEducationLevel' ");
	        SQL.AppendLine("    ) C On A.EmpCode = C.EmpCode ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Distinct T6.EmpCode, T7.Experience ");
		    SQL.AppendLine("        From ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select X4.EmpCode ");
			SQL.AppendLine("            From TblEmployeeWorkExp X4 ");
			SQL.AppendLine("            Where Find_In_Set(X4.Position, @ExperienceCode) ");
		    SQL.AppendLine("        ) T6 ");
		    SQL.AppendLine("        Inner Join ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select X5.EmpCode, Group_Concat(Concat(X5.Position, ' [', X5.Period, ']') Separator '; ') As Experience ");
			SQL.AppendLine("            From TblEmployeeWorkExp X5 ");
			SQL.AppendLine("            Group By X5.EmpCode ");
		    SQL.AppendLine("        ) T7 On T6.EmpCode = T7.EmpCode ");
	        SQL.AppendLine("    ) D On A.EmpCode = D.EmpCode ");
	        SQL.AppendLine("    Inner Join TblSite E On A.SiteCode = E.SiteCode ");
	        SQL.AppendLine("    Inner Join TblDepartment F On A.DeptCode = F.DeptCode ");
            SQL.AppendLine("    Inner Join TblOrganizationalStructure G On G.PosCode2 = @PosCode2 ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
            
            if(TxtEmpCode.Text.Length > 0)
                SQL.AppendLine("        And (A.EmpCode Like '%" + TxtEmpCode.Text + "%' Or A.EmpName Like '%" + TxtEmpCode.Text + "%' Or A.EmpCodeOld Like '%" + TxtEmpCode.Text + "%') ");
            
            SQL.AppendLine(") X Where (X.Competence Is Not Null Or X.Education Is Not Null Or X.Experience Is Not Null) ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("  Select '2' As DocType, A.DocNo As EmpCode, A.EmpName, Null As Competence, Null As Education, Null As Experience, ");
            SQL.AppendLine("  0, 0, TimeStampDiff(Year, IfNull(A.BirthDt, Now()), Now()) As EmpAge ");
            SQL.AppendLine("  From TblEmployeeRecruitment A ");
            if (mFrmParent.mIsFilterBySiteHR || mFrmParent.mIsFilterByLevelHR)
            {
                SQL.AppendLine(" Inner Join TblEmployeeRequest B On A.EmployeeRequestDocNo=B.DocNo ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And B.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsFilterByLevelHR)
                {
                    SQL.AppendLine("And B.LevelCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupLevel ");
                    SQL.AppendLine("    Where LevelCode=IfNull(B.LevelCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine(" Where A.PosCode = @PosCode ");

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where T.EmpCode Not In (" + mFrmParent.GetSelectedEmployee() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CompetenceCode", mCompetenceCode);
                Sm.CmParam<String>(ref cm, "@EducationCode", mEducationCode);
                Sm.CmParam<String>(ref cm, "@ExperienceCode", mExperienceCode);
                Sm.CmParam<String>(ref cm, "@PosCode2", mPosCode2);
                Sm.CmParam<String>(ref cm, "@PosCode", mPosCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By T.EmpName;",
                        new string[] 
                        { 
                            //0
                            "EmpCode",
                            
                            //1-5
                            "EmpName", "Competence", "DocType", "Education", "Experience", 

                            //6-8
                            "MinAge", "MaxAge", "EmpAge"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 8);
                        mFrmParent.Grd2.Rows.Add();
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index <= mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd2, Index, 2),
                    Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6) == "1")
                {
                    var f = new FrmEmployee("XXX");
                    f.Tag = "XXX";
                    f.Text = "Master Employee";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 6) == "2")
                {
                    var f = new FrmEmployeeRecruitment("XXX");
                    f.Tag = "XXX";
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmployeeRecruitment' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6) == "1")
                {
                    var f = new FrmEmployee("XXX");
                    f.Tag = "XXX";
                    f.Text = "Master Employee";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 6) == "2")
                {
                    var f = new FrmEmployeeRecruitment("XXX");
                    f.Tag = "XXX";
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmEmployeeRecruitment' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

    }
}
