﻿#region Update
/*
    25/06/2020 [WED/KSM] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt7Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt7 mFrmParent;
        private string mSQL = string.Empty;
        private byte mFlag = 1;

        #endregion

        #region Constructor

         public FrmDOCt7Dlg4(FrmDOCt7 FrmParent, byte Flag)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mFlag = Flag;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        
                        //1-4
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-4
                        120, 250, 100, 200
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;

            Sm.ClearGrd(Grd1, false);

            try
            {                
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName ");
                SQL.AppendLine("From TblEmployee A  ");
                SQL.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                SQL.AppendLine("And A.PosCode Is Not Null ");
                SQL.AppendLine("And Find_In_Set(");
                SQL.AppendLine("    IfNull(A.PosCode, ''), ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode=@ParCode), '') ");
                SQL.AppendLine("    ) ");

                var cm = new MySqlCommand();

                string Filter = " ", PosCode = string.Empty;

                if (mFlag == 1 || mFlag == 2) PosCode = "PosCodeChecker";
                if (mFlag == 3 || mFlag == 4) PosCode = "PosCodeForkliftDriver";
                Sm.CmParam<String>(ref cm, "@ParCode", PosCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString() + Filter + " Order By A.EmpName;",
                    new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-3
                    "EmpName", "EmpCodeOld", "PosName"
        
                },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                if (mFlag == 1)
                {
                    mFrmParent.mEmpCode1 = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtEmpCode1.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                }
                if (mFlag == 2)
                {
                    mFrmParent.mEmpCode2 = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtEmpCode2.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                }
                if (mFlag == 3)
                {
                    mFrmParent.mEmpCode3 = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtEmpCode3.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                }
                if (mFlag == 4)
                {
                    mFrmParent.mEmpCode4 = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtEmpCode4.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                }
                this.Close();
            }
        }

        override protected void CloseForm()
        {
            
            if (mFlag == 1)
            {
                if (mFrmParent.mEmpCode1.Length > 0 && Sm.StdMsgYN("Question", "Do you want to clear existing employee ?") == DialogResult.Yes)
                {
                    mFrmParent.mEmpCode1 = string.Empty;
                    mFrmParent.TxtEmpCode1.EditValue = null;
                }
            }
            if (mFlag == 2)
            {
                if (mFrmParent.mEmpCode2.Length > 0 && Sm.StdMsgYN("Question", "Do you want to clear existing employee ?") == DialogResult.Yes)
                {
                    mFrmParent.mEmpCode2 = string.Empty;
                    mFrmParent.TxtEmpCode2.EditValue = null;
                }
            }
            if (mFlag == 3)
            {
                if (mFrmParent.mEmpCode3.Length > 0 && Sm.StdMsgYN("Question", "Do you want to clear existing employee ?") == DialogResult.Yes)
                {
                    mFrmParent.mEmpCode3 = string.Empty;
                    mFrmParent.TxtEmpCode3.EditValue = null;
                }
            }
            if (mFlag == 4)
            {
                if (mFrmParent.mEmpCode4.Length > 0 && Sm.StdMsgYN("Question", "Do you want to clear existing employee ?") == DialogResult.Yes)
                {
                    mFrmParent.mEmpCode4 = string.Empty;
                    mFrmParent.TxtEmpCode4.EditValue = null;
                }
            }
        
            this.Close();
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
       
        #endregion

    }
}
