﻿#region Update
/*
    09/10/2020 [IBL/PHT] New application
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCostRevenueRate : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = string.Empty;
        internal FrmCostRevenueRateFind FrmFind;
        private bool mIsInsert = false;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmCostRevenueRate(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Sales Memo";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                Sl.SetLueCurCode2(ref LueCurCode);
                Sl.SetLueOption(ref LueTransactionCode, "CostRevenueRateTransaction");
                SetGrd();
                LueCurCode.Visible = false;
                LueTransactionCode.Visible = false;
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "DNo",
                        "Item's Code",
                        "Item",
                        "",

                        //6-10
                        "Transaction Code",
                        "Transaction",
                        "Rate",
                        "Currency Code",
                        "Currency",
                        
                        //11
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        60,
 
                        //1-5
                        20, 0, 100, 150, 20,

                        //6-10
                        0, 120, 100, 0, 100,

                        //11
                        180
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 9 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       MeeCancelReason, DteDocDt, LueCurCode, LueTransactionCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11 });
                    ChkCancelInd.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    mIsInsert = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueCurCode, LueTransactionCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 7, 8, 10, 11 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    mIsInsert = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                      MeeCancelReason, LueTransactionCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7, 8 });
                    ChkCancelInd.Enabled = true;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, MeeCancelReason, DteDocDt, LueCurCode, LueTransactionCode, MeeRemark, 
            });

            ChkCancelInd.Checked = false;

            ClearGrd();

        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCostRevenueRateFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
                {
                    if (e.ColIndex == 7) Sm.LueRequestEdit(ref Grd1, ref LueTransactionCode, ref fCell, ref fAccept, e);
                }

                if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
                {
                    if (e.ColIndex == 10) Sm.LueRequestEdit(ref Grd1, ref LueCurCode, ref fCell, ref fAccept, e);
                }

                //Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(Grd1, Grd1.Rows.Count - 1, new int[] { 8 });
            }

            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmCostRevenueRateDlg(this));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmCostRevenueRateDlg(this));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (mIsInsert)
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to save this data ?") == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CostRevenueRate", "TblCostRevenueRateHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveCostRevenueRateHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) cml.Add(SaveCostRevenueRateDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private MySqlCommand SaveCostRevenueRateHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCostRevenueRateHdr(DocNo, DocDt, CancelInd, CancelReason, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', NULL, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.Left(Sm.GetDte(DteDocDt), 8));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCostRevenueRateDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCostRevenueRateDtl(DocNo, DNo, ItCode, TransactionCode, ExcRate, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @TransactionCode, @ExcRate, @CurCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@TransactionCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date ") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Transaction is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, true, "Rate should be bigger than 0.00.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Currency is empty.")
                    )
                    return true;
            }

            if (Grd1.Rows.Count > 1)
            {
                int r1, r2 = 0;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        if (Row2 != Row
                            && Sm.GetGrdStr(Grd1, Row2, 3) == Sm.GetGrdStr(Grd1, Row, 3)
                            && Sm.GetGrdStr(Grd1, Row2, 6) == Sm.GetGrdStr(Grd1, Row, 6))
                        {
                            r1 = Row + 1;
                            r2 = Row2 + 1;

                            Msg += "No. : " + r1 + "\n";
                            Msg += "Item : " + Sm.GetGrdStr(Grd1, Row, 4) + "\n";
                            Msg += "Transaction : " + Sm.GetGrdStr(Grd1, Row, 7) + "\n \n";
                            Msg += "No. : " + r2 + "\n";
                            Msg += "Item : " + Sm.GetGrdStr(Grd1, Row2, 4) + "\n";
                            Msg += "Transaction : " + Sm.GetGrdStr(Grd1, Row2, 7) + "\n\n";
                            Msg += "There should be no same combination of items and transactions more than one.";

                            Sm.StdMsg(mMsgType.Warning, Msg);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            string Transaction = string.Empty, Rate = string.Empty;

            var cml = new List<MySqlCommand>();

            cml.Add(EditCostRevenueRateHdr());
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Transaction = Sm.GetValue("Select TransactionCode From TblCostRevenueRateDtl Where DocNo = @Param And DNo = " + Sm.GetGrdStr(Grd1, Row, 2) + ";", TxtDocNo.Text);
                Rate = Sm.GetValue("Select ExcRate From TblCostRevenueRateDtl Where DocNo = @Param And DNo = " + Sm.GetGrdStr(Grd1, Row, 2) + ";", TxtDocNo.Text);

                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6) != Transaction || Sm.GetGrdStr(Grd1, Row, 8) != Rate)
                        cml.Add(EditCostRevenueRateDtl(TxtDocNo.Text, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand EditCostRevenueRateHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCostRevenueRateHdr");
            SQL.AppendLine("Set CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y":"N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditCostRevenueRateDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCostRevenueRateDtl ");
            SQL.AppendLine("Set ExcRate=@ExcRate, CurCode=@CurCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And DNo = @DNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsDataCancelledAlready() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsDataCancelledAlready()
        {
            string CancelInd = Sm.GetValue("Select CancelInd From TblCostRevenueRateHdr Where DocNo = @Param ", TxtDocNo.Text);
            if(CancelInd == "Y")
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been cancelled already.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowCostRevenueRateHdr(DocNo);
                ShowCostRevenueRateDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowCostRevenueRateHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelReason, CancelInd, Remark " +
                    "From TblCostRevenueRateHdr " +
                    "Where DocNo = @DocNo ",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "CancelReason", "CancelInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        public void ShowCostRevenueRateDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DNo, B.ItCode, B.ItName, C.OptCode, C.OptDesc, IfNull(A.ExcRate, 0) Rate, ");
            SQL.AppendLine("A.CurCode, A.Remark ");
            SQL.AppendLine("From TblCostRevenueRateDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Inner Join TblOption C On A.TransactionCode = C.OptCode And C.OptCat = 'CostRevenueRateTransaction' ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "ItCode", "ItName", "OptCode", "OptDesc", "Rate",
                    
                    //6-7
                    "CurCode", "Remark",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row+1;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                }, false, false, true, false
            );

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueTransactionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTransactionCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostRevenueRateTransaction");
        }

        private void LueTransactionCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTransactionCode_Leave(object sender, EventArgs e)
        {
            if (LueTransactionCode.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (Sm.GetLue(LueTransactionCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 6].Value =
                    Grd1.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueTransactionCode);
                    Grd1.Cells[fCell.RowIndex, 7].Value = LueTransactionCode.GetColumnValue("Col2");
                }
                LueTransactionCode.Visible = false;
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode2));
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueCurCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 9].Value = 
                    Grd1.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueCurCode);
                    Grd1.Cells[fCell.RowIndex, 10].Value = LueCurCode.GetColumnValue("Col2");
                }
                LueCurCode.Visible = false;
            }
        }

        #endregion

        #region Grid Control Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdEnter(Grd1, e);
        }

        #endregion

        #endregion
    }
}
