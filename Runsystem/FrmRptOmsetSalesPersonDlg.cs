﻿#region update
// 2017/09/30 [HAR] amountnya ambil dari unit price 
// 2017/10/06 [HAR] date ambil dari date voucher, sales person di SO jk kosong maka ambil dari sales invoice 
// 2017/10/16 [HAR] bug fixing SO 
// 2018/02/27 [HAR] feedback : total group (500-999, >999) dihilangkan
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOmsetSalesPersonDlg : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mSalesPerson = "";
        private string mYear = "";
        #endregion

        #region Constructor

        public FrmRptOmsetSalesPersonDlg(string MenuCode, string SalesPerson, string Year)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
            mSalesPerson = SalesPerson;
            mYear = Year;
        }


        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Detail Of Sales Omset";
                LblSalesPerson.Text = mSalesPerson;
                LblYear.Text = mYear;
                SetGrd();
                SetSQL();
             
                base.FrmLoad(sender, e);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

                SQL.AppendLine("Select A.SpCode, A.Yr, A.Mth, A.DocNo, SUM(Qty) As Qty, SUM(UPrice) As Uprice, SUM(Amt) As Amt, '0' TypeTrx From (    ");
                //Sales invoice CBD 5-99
	            SQL.AppendLine("    Select ifnull(A.SpCode, L.SpCode) As SpCode, Left(K.DocDt, 4) As yr, substring(K.DocDt, 5, 2) As Mth, ");
	            SQL.AppendLine("    A.DocNo, B.Qty, E.Uprice, (B.Qty*E.Uprice) Amt, '1' As TypeTrx  ");
	            SQL.AppendLine("    From TblSOhdr A  ");
	            SQL.AppendLine("    Inner Join TblSODtl B On A.DocNo = B.DocNo  ");
	            SQL.AppendLine("    Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo  ");
	            SQL.AppendLine("    Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno  ");
	            SQL.AppendLine("    Inner Join TblItemPriceDtl E On D.ItemPriceDocNo = E.DocNo And D.itemPriceDno = E.Dno  ");
	            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr F On A.DocNo = F.SODocNO And F.CancelInd = 'N' And F.SODocNo is not null  ");
	            SQL.AppendLine("    Inner Join tblSalesInvoiceDtl G On F.DocNO = G.DocNO And B.DocNo = G.DOCtDocNo And B.Dno = G.DOCtDno  ");
                SQL.AppendLine("    Inner Join ");
                SQL.AppendLine("    ( ");
	            SQL.AppendLine("        Select distinct H.InvoiceDocNo, MAX(K.DocDt) DocDt ");
	            SQL.AppendLine("        From TblIncomingPaymentDtl H ");
	            SQL.AppendLine("        Inner Join TblIncomingPaymenthDr I On H.DocNo = I.DocNo And  I.CancelInd = 'N' ");
	            SQL.AppendLine("        Inner Join TblVoucherRequesthdr J On I.VoucherrequestDocNo = J.DocNo And J.CancelInd = 'N' ");
	            SQL.AppendLine("        Inner Join TblVoucherHdr K On J.VoucherDocNo = K.DocNo And  K.cancelInd = 'N' ");
	            SQL.AppendLine("        Group By H.InvoiceDocNo ");
                SQL.AppendLine("    )K On F.DocNo = K.InvoiceDocNo ");

                SQL.AppendLine("    Left Join tblSalesPerson L On LEFT(F.salesname,LOCATE(' ',F.Salesname) - 1)  = L.UserCode ");
                SQL.AppendLine("    Where A.CancelInd = 'N' And Left(K.DocDt, 4) = @Yr And E.Uprice between 500000 And 999999 ");
	            SQL.AppendLine("    Union All  ");
                //Sales Inovice TOP 5-99
                SQL.AppendLine("    Select ifnull(A.SpCode, I.SpCode) As SpCode, Left(H.DocDt, 4) As yr, substring(H.DocDt, 5, 2) As Mth,   ");
	            SQL.AppendLine("    A.DocNo, B.Qty, E.Uprice, (B.Qty*E.Uprice) Amt, '1' As TypeTrx   ");
	            SQL.AppendLine("    From TblSOhdr A   ");
	            SQL.AppendLine("    Inner Join TblSODtl B On A.DocNo = B.DocNo   ");
	            SQL.AppendLine("    Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo   ");
	            SQL.AppendLine("    Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno   ");
	            SQL.AppendLine("    Inner Join TblItemPriceDtl E On D.ItemPriceDocNo = E.DocNo And D.itemPriceDno = E.Dno   ");
	            SQL.AppendLine("    Inner Join TblDRDtl F On B.DocNo = F.SoDocNo And B.DNo = F.SODNo   ");
	            SQL.AppendLine("    Inner Join TblDRHdr G On F.DocNo = G.DocNo And G.CancelInd= 'N'  ");
	            SQL.AppendLine("    Inner Join   ");
	            SQL.AppendLine("    (  ");
		        SQL.AppendLine("        Select X.DocNo, X2.Dno, X.DrDocNO, X2.DrDno, X.DocDt, X.SalesName   ");
		        SQL.AppendLine("        from (  ");
			    SQL.AppendLine("            Select Distinct A.DocNO, A.DRDocNo, H.DocDt, D.SalesName  ");
			    SQL.AppendLine("            from TblDOCt2Hdr A  ");
			    SQL.AppendLine("            Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo  ");
			    SQL.AppendLine("            Inner Join tblSalesInvoiceDtl C On B.DocNo = C.DOCtDocNo And B.Dno = C.DOCtDno  ");
			    SQL.AppendLine("            Inner Join TblSalesInvoiceHdr D On C.DocNo = D.DocNO And D.CancelInd = 'N' And D.SODocNo is null  ");
                
                SQL.AppendLine("            Inner Join ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select distinct H.InvoiceDocNo, MAX(K.DocDt) DocDt ");
                SQL.AppendLine("                From TblIncomingPaymentDtl H ");
                SQL.AppendLine("                Inner Join TblIncomingPaymenthDr I On H.DocNo = I.DocNo And  I.CancelInd = 'N' ");
                SQL.AppendLine("                Inner Join TblVoucherRequesthdr J On I.VoucherrequestDocNo = J.DocNo And J.CancelInd = 'N' ");
                SQL.AppendLine("                Inner Join TblVoucherHdr K On J.VoucherDocNo = K.DocNo And  K.cancelInd = 'N' ");
                SQL.AppendLine("                Group By H.InvoiceDocNo ");
                SQL.AppendLine("             )H On D.DocNo = H.InvoiceDocNo ");

                SQL.AppendLine("        )X  ");
	            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 X2 On X.DocNo = X2.DocnO  ");
	            SQL.AppendLine("    )H On F.DocNo = H.DRDocNo And F.DNo = H.DRDno      ");
                SQL.AppendLine("    Left Join tblSalesPerson I On LEFT(H.salesname,LOCATE(' ',H.Salesname) - 1)  = I.UserCode ");
	            SQL.AppendLine("    Where A.CancelInd = 'N' And Left(H.DocDt, 4) = @Yr  And E.Uprice between 500000 And 999999 ");
	            SQL.AppendLine("    UNION ALL    ");
                //Sales invoice POS 5-99
                SQL.AppendLine("    Select  X1.SpCode, X1.Yr, X1.mth, concat(X1.TrnNo,'-', X1.BsDate) TrnNo, X1.Qty, X1.UPrice, (X1.Qty*X1.PayAmtNett) PayAmtNett, '1'   "); 
	            SQL.AppendLine("    From (    ");
		        SQL.AppendLine("        Select A.TrnNo, A.BSDate, C.SpCode, Left(A.BSDate, 4) Yr, Substring(A.BSDate, 5, 2) Mth, B.Qty, B.UPrice, ");
                SQL.AppendLine("        if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett2, '3' As TypeDoc   "); 
		        SQL.AppendLine("        From TblPostrnhdr A    ");
		        SQL.AppendLine("        Inner Join TblPostrnDtl B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo   ");  
		        SQL.AppendLine("        Inner Join Tblsalesperson C On A.UserCode = C.UserCode   ");
		        SQL.AppendLine("        Where B.UPrice between 500000 And 999999  ");
	            SQL.AppendLine("    )X1 Where Left(X1.BSDate, 4) = @Yr    ");
                SQL.AppendLine(")A Where A.Yr= @Yr And A.SpCode=@SpCode ");
                SQL.AppendLine("Group BY A.SpCode, A.Yr, A.Mth, A.DocNo ");
                SQL.AppendLine("UNion All     ");
                SQL.AppendLine("Select A.SpCode, A.Yr, A.Mth, A.DocNo, SUM(Qty), SUM(UPrice), SUM(Amt), '0' TypeTrx From (    ");
                //Sales invoice CBD >99
                SQL.AppendLine("    Select ifnull(A.SpCode, L.SpCode) As SPCode, Left(K.DocDt, 4) As yr, substring(K.DocDt, 5, 2) As Mth,   ");
	            SQL.AppendLine("    A.DocNo, B.Qty, E.Uprice, (B.Qty*E.Uprice) Amt, '2' As TypeTrx    ");
	            SQL.AppendLine("    From TblSOhdr A    ");
	            SQL.AppendLine("    Inner Join TblSODtl B On A.DocNo = B.DocNo    ");
	            SQL.AppendLine("    Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo    ");
	            SQL.AppendLine("    Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno    ");
	            SQL.AppendLine("    Inner Join TblItemPriceDtl E On D.ItemPriceDocNo = E.DocNo And D.itemPriceDno = E.Dno    ");
	            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr F On A.DocNo = F.SODocNO And F.CancelInd = 'N' And F.SODocNo is not null    ");
	            SQL.AppendLine("    Inner Join tblSalesInvoiceDtl G On F.DocNO = G.DocNO And B.DocNo = G.DOCtDocNo And B.Dno = G.DOCtDno    ");
                SQL.AppendLine("    Inner Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select distinct H.InvoiceDocNo, MAX(K.DocDt) DocDt ");
                SQL.AppendLine("        From TblIncomingPaymentDtl H ");
                SQL.AppendLine("        Inner Join TblIncomingPaymenthDr I On H.DocNo = I.DocNo And  I.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblVoucherRequesthdr J On I.VoucherrequestDocNo = J.DocNo And J.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblVoucherHdr K On J.VoucherDocNo = K.DocNo And  K.cancelInd = 'N' ");
                SQL.AppendLine("        Group By H.InvoiceDocNo ");
                SQL.AppendLine("    )K On F.DocNo = K.InvoiceDocNo ");
    
                SQL.AppendLine("    Left Join tblSalesPerson L On LEFT(F.salesname,LOCATE(' ',F.Salesname) - 1)  = L.UserCode ");
	            SQL.AppendLine("    Where A.CancelInd = 'N' And Left(K.DocDt, 4) = @Yr And E.UPrice >999999   ");
	            SQL.AppendLine("    Union All    ");
                //Sales invoice TOP >99
                SQL.AppendLine("    Select ifnull(A.SpCode, I.SpCode) SpCode, Left(H.DocDt, 4) As yr, substring(H.DocDt, 5, 2) As Mth,     ");
	            SQL.AppendLine("    A.DocNo, B.Qty, E.Uprice, (B.Qty*E.Uprice) Amt, '2' As TypeTrx     ");
	            SQL.AppendLine("    From TblSOhdr A     ");
	            SQL.AppendLine("    Inner Join TblSODtl B On A.DocNo = B.DocNo     ");
	            SQL.AppendLine("    Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo     ");
	            SQL.AppendLine("    Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno     ");
	            SQL.AppendLine("    Inner Join TblItemPriceDtl E On D.ItemPriceDocNo = E.DocNo And D.itemPriceDno = E.Dno    ");
	            SQL.AppendLine("    Inner Join TblDRDtl F On B.DocNo = F.SoDocNo And B.DNo = F.SODNo     ");
	            SQL.AppendLine("    Inner Join TblDRHdr G On F.DocNo = G.DocNo And G.CancelInd= 'N'    ");
	            SQL.AppendLine("    Inner Join     ");
	            SQL.AppendLine("    (    ");
		        SQL.AppendLine("        Select X.DocNo, X2.Dno, X.DrDocNO, X2.DrDno, X.DocDt, X.SalesName  ");
		        SQL.AppendLine("        from (    ");
			    SQL.AppendLine("            Select Distinct A.DocNO, A.DRDocNo, H.DocDt, D.SalesName    ");
			    SQL.AppendLine("            from TblDOCt2Hdr A    ");
			    SQL.AppendLine("            Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo    ");
			    SQL.AppendLine("            Inner Join tblSalesInvoiceDtl C On B.DocNo = C.DOCtDocNo And B.Dno = C.DOCtDno    ");
			    SQL.AppendLine("            Inner Join TblSalesInvoiceHdr D On C.DocNo = D.DocNO And D.CancelInd = 'N' And D.SODocNo is null    ");
               
                SQL.AppendLine("                    Inner Join ");
                SQL.AppendLine("                    ( ");
                SQL.AppendLine("                        Select distinct H.InvoiceDocNo, MAX(K.DocDt) DocDt ");
                SQL.AppendLine("                        From TblIncomingPaymentDtl H ");
                SQL.AppendLine("                        Inner Join TblIncomingPaymenthDr I On H.DocNo = I.DocNo And  I.CancelInd = 'N' ");
                SQL.AppendLine("                        Inner Join TblVoucherRequesthdr J On I.VoucherrequestDocNo = J.DocNo And J.CancelInd = 'N' ");
                SQL.AppendLine("                        Inner Join TblVoucherHdr K On J.VoucherDocNo = K.DocNo And  K.cancelInd = 'N' ");
                SQL.AppendLine("                        Group By H.InvoiceDocNo ");
                SQL.AppendLine("                    )H On D.DocNo = H.InvoiceDocNo ");

		        SQL.AppendLine("        )X    ");
		        SQL.AppendLine("        Inner Join TblDOCt2Dtl2 X2 On X.DocNo = X2.DocnO    ");
	            SQL.AppendLine("    )H On F.DocNo = H.DRDocNo And F.DNo = H.DRDno   ");
                SQL.AppendLine("    Left Join tblSalesPerson I On LEFT(H.salesname,LOCATE(' ',H.Salesname) - 1)  = I.UserCode ");
	            SQL.AppendLine("    Where A.CancelInd = 'N' And Left(H.DocDt, 4) = @Yr And E.UPrice >999999   ");
	            SQL.AppendLine("    UNION ALL      ");
                //Sales invoice POS >99
                SQL.AppendLine("    Select  X1.SpCode, X1.Yr, X1.mth, Concat(X1.TrnNo, '-', X1.BsDate) TrnNo, X1.Qty, X1.Uprice,  (X1.Qty*X1.PayAmtNett) PayAmtNett, '2'      ");
	            SQL.AppendLine("    From (      ");
		        SQL.AppendLine("        Select A.TrnNo, A.BSDate, C.SpCode, Left(A.BSDate, 4) Yr, Substring(A.BSDate, 5, 2) Mth, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett, B.Qty, B.UPrice, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett2, '3' As TypeDoc      ");
		        SQL.AppendLine("        From TblPostrnhdr A      ");
		        SQL.AppendLine("        Inner Join TblPostrnDtl B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo      ");
		        SQL.AppendLine("        Inner Join Tblsalesperson C On A.UserCode = C.UserCode      ");
		        SQL.AppendLine("        Where B.UPrice >999999 And Left(A.BSDate, 4) = @Yr     ");
	            SQL.AppendLine("    )X1    ");
                SQL.AppendLine(")A Where A.Yr= @Yr And A.SpCode=@SpCode ");
                SQL.AppendLine("Group BY A.SpCode, A.Yr, A.Mth, A.DocNo ");

            mSQL = SQL.ToString();
            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Sales"+Environment.NewLine+"person", 
                        "Year",
                        "Month",
                        "Document"+Environment.NewLine+"Transaction",
                        "Quantity",

                        //6-8
                        "Price",
                        "Amount",
                        "Transaction Type",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 80, 120, 180, 150, 
                        
                        //6-9
                        180, 180, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[]{5, 6, 7},0);
            Sm.GrdColInvisible(Grd1, new int[] { 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", LblYear.Text);
                Sm.CmParam<String>(ref cm, "@SpCode", Sm.GetValue("Select SpCode From TblSalesPerson Where SpName = '"+mSalesPerson+"' "));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "spcode", 

                            //1-5
                            "yr", "mth", "docno", "qty", "uprice", 
                            
                            //6
                            "amt", "typetrx"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = mSalesPerson;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Grd.Cells[Row, 8].Value = Sm.DrStr(dr, c[7]) == "1"? "500.000-999.999" : ">1.000.000";
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7 });
        }

        #endregion

     

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
           
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion
    }
}
