﻿#region Update
/*
    06/02/2018 [TKG] Reporting Voucher
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVoucher : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mBankAccountFormat = string.Empty;
        private bool mIsVoucherBankAccountFilteredByGrp = false;

        #endregion

        #region Constructor

        public FrmRptVoucher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        { 
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mIsVoucherBankAccountFilteredByGrp = Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp");
        }

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T1.LocalDocNo, T1.DocType, T1.Remark, T1.CurCode, ");
            SQL.AppendLine("T1.DocTypeDesc, T1.BankAcDesc, T2.VdName, T3.CtName, T1.DAmt, T1.CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.LocalDocNo, A.DocType, A.Remark, A.CurCode, ");
            SQL.AppendLine("    B.OptDesc As DocTypeDesc, A.BankAcCode, ");
            if (Sm.GetParameter("BankAccountFormat") == "1")
            {
                SQL.AppendLine("    Trim(Concat( ");
                SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(D.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(C.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(C.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When C.Remark Is Not Null Then Concat(' ', '(', C.Remark, ')') Else '' End ");
                SQL.AppendLine("    )) ");
            }
            else
            {
                SQL.AppendLine("    Trim(Concat( ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(C.BankAcNo, ' [', IfNull(C.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(C.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(' ', D.BankName) Else '' End ");
                SQL.AppendLine("    )) ");
            }
            SQL.AppendLine("    As BankAcDesc, ");

            SQL.AppendLine("    Case A.DocType ");
            SQL.AppendLine("        When '03' Then E.VdCode ");
            SQL.AppendLine("        When '04' Then G.VdCode ");
            SQL.AppendLine("        When '14' Then H.VdCode ");
            SQL.AppendLine("        When '17' Then I.VdCode ");
            SQL.AppendLine("        When '19' Then K.VdCode ");
            SQL.AppendLine("        Else Null ");
            SQL.AppendLine("    End As VdCode, ");
            SQL.AppendLine("    Case A.DocType ");
            SQL.AppendLine("        When '02' Then L.CtCode ");
            SQL.AppendLine("        When '05' Then M.CtCode ");
            //SQL.AppendLine("      When '23' Then N.CtCode ");
            SQL.AppendLine("        When '18' Then O.CtCode ");
            SQL.AppendLine("        When '20' Then P.CtCode ");
            SQL.AppendLine("        Else Null ");
            SQL.AppendLine("    End As CtCode, ");
            SQL.AppendLine("    Case A.AcType When 'D' Then A.Amt When 'C' Then 0.00 End As DAmt, ");
            SQL.AppendLine("    Case A.AcType When 'D' Then 0.00 When 'C' Then A.Amt End As CAmt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='VoucherDocType' ");
            SQL.AppendLine("    Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank D On C.BankCode=D.BankCode ");

            //Vendor
            SQL.AppendLine("    Left Join TblOutgoingPaymentHdr E On A.VoucherRequestDocNo=E.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join TblAPDownpayment F On A.VoucherRequestDocNo=F.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join TblPOHdr G On F.PODocNo=G.DocNo ");
            SQL.AppendLine("    Left Join TblReturnAPDownpayment H On A.VoucherRequestDocNo=H.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join TblOutgoingPaymentHdr I On I.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=I.VoucherRequestDocNo2 ");
            SQL.AppendLine("    Left Join TblAPDownpayment J On J.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=J.VoucherRequestDocNo2 ");
            SQL.AppendLine("    Left Join TblPOHdr K On J.PODocNo=K.DocNo ");

            //Customer
            SQL.AppendLine("    Left Join TblIncomingPaymentHdr L On A.VoucherRequestDocNo=L.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join TblARDownpayment M On A.VoucherRequestDocNo=M.VoucherRequestDocNo ");
            //SQL.AppendLine("  Left Join TblReturnARDownpayment N On A.VoucherRequestDocNo=N.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join TblIncomingPaymentHdr O On O.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=O.VoucherRequestDocNo2 ");
            SQL.AppendLine("    Left Join TblARDownpayment P On P.VoucherRequestDocNo2 Is Not Null And A.VoucherRequestDocNo=P.VoucherRequestDocNo2 ");
            
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.LocalDocNo, A.DocType, A.Remark, A.CurCode2 As CurCode, ");
            SQL.AppendLine("    B.OptDesc As DocTypeDesc, A.BankAcCode2 As BankAcCode, ");
            if (Sm.GetParameter("BankAccountFormat") == "1")
            {
                SQL.AppendLine("    Trim(Concat( ");
                SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(D.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(C.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(C.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When C.Remark Is Not Null Then Concat(' ', '(', C.Remark, ')') Else '' End ");
                SQL.AppendLine("    )) ");
            }
            else
            {
                SQL.AppendLine("    Trim(Concat( ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(C.BankAcNo, ' [', IfNull(C.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(C.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(' ', D.BankName) Else '' End ");
                SQL.AppendLine("    )) ");
            }
            SQL.AppendLine("    As BankAcDesc, ");

            SQL.AppendLine("    Null As VdCode, Null As CtCode, ");
            SQL.AppendLine("    Case A.AcType2 When 'D' Then A.Amt*A.ExcRate When 'C' Then 0.00 End As DAmt, ");
            SQL.AppendLine("    Case A.AcType2 When 'D' Then 0.00 When 'C' Then A.Amt*A.ExcRate End As CAmt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='VoucherDocType' ");
            SQL.AppendLine("    Left Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank D On C.BankCode=D.BankCode ");

            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.BankAcCode2 Is Not Null ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join TblVendor T2 On T1.VdCode=T2.VdCode ");
            SQL.AppendLine("Left Join TblCustomer T3 On T1.CtCode=T3.CtCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("Order By T1.BankAcDesc, T1.CurCode, T1.DocDt, T1.DocNo;");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Voucher#", 
                        "Date",
                        "Local#",
                        "Type",
                        "Type",

                        //6-10
                        "Cash/Bank Account",
                        "Vendor",
                        "Customer",
                        "Remark",
                        "Currency",
                        
                        //11-12
                        "Debit",
                        "Credit"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 130, 0, 150,
                        
                        //6-10
                        250, 200, 200, 200, 60,

                        //11-12
                        120, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ", Filter2 = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueBankAcCode), "T1.BankAcCode", true);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueVdCode), "T1.VdCode", true);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueCtCode), "T1.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter, Filter2),
                        new string[]
                        {
                            //0
                            "DocNo",  

                            //1-5
                            "DocDt", "LocalDocNo", "DocType", "DocTypeDesc", "BankAcDesc",

                            //6-10
                            "VdName", "CtName", "Remark", "CurCode", "DAmt",

                            //11
                            "CAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cash/Bank account");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion
    }
}
