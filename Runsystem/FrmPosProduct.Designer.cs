﻿namespace RunSystem
{
    partial class FrmPosProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.BtnFind = new System.Windows.Forms.Button();
            this.TxtPdName = new DevExpress.XtraEditors.TextEdit();
            this.TxtPdCode = new DevExpress.XtraEditors.TextEdit();
            this.LueSupplierCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueGroup1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueGroup2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueGroup3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueGroup4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtBarCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtLocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtWeight = new DevExpress.XtraEditors.TextEdit();
            this.TxtMinPrice = new DevExpress.XtraEditors.TextEdit();
            this.TxtUnitPrice = new DevExpress.XtraEditors.TextEdit();
            this.BtnInsert = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSupplierCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnitPrice.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Indigo;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.BtnExit);
            this.panel1.Controls.Add(this.BtnCancel);
            this.panel1.Controls.Add(this.BtnSave);
            this.panel1.Controls.Add(this.BtnDelete);
            this.panel1.Controls.Add(this.BtnEdit);
            this.panel1.Controls.Add(this.BtnInsert);
            this.panel1.Controls.Add(this.BtnFind);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(852, 87);
            this.panel1.TabIndex = 1;
            // 
            // BtnExit
            // 
            this.BtnExit.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnExit.FlatAppearance.BorderSize = 0;
            this.BtnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnExit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(516, 0);
            this.BtnExit.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(86, 83);
            this.BtnExit.TabIndex = 7;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnCancel.FlatAppearance.BorderSize = 0;
            this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCancel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Location = new System.Drawing.Point(430, 0);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(86, 83);
            this.BtnCancel.TabIndex = 6;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // BtnSave
            // 
            this.BtnSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnSave.FlatAppearance.BorderSize = 0;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(344, 0);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(86, 83);
            this.BtnSave.TabIndex = 5;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnDelete.FlatAppearance.BorderSize = 0;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(258, 0);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(86, 83);
            this.BtnDelete.TabIndex = 4;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnEdit.FlatAppearance.BorderSize = 0;
            this.BtnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEdit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Location = new System.Drawing.Point(172, 0);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(86, 83);
            this.BtnEdit.TabIndex = 3;
            this.BtnEdit.Text = "&Edit";
            this.BtnEdit.UseVisualStyleBackColor = true;
            // 
            // BtnFind
            // 
            this.BtnFind.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnFind.FlatAppearance.BorderSize = 0;
            this.BtnFind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFind.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Location = new System.Drawing.Point(0, 0);
            this.BtnFind.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnFind.Name = "BtnFind";
            this.BtnFind.Size = new System.Drawing.Size(86, 83);
            this.BtnFind.TabIndex = 1;
            this.BtnFind.Text = "&Find";
            this.BtnFind.UseVisualStyleBackColor = true;
            // 
            // TxtPdName
            // 
            this.TxtPdName.EnterMoveNextControl = true;
            this.TxtPdName.Location = new System.Drawing.Point(125, 220);
            this.TxtPdName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPdName.Name = "TxtPdName";
            this.TxtPdName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPdName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPdName.Properties.Appearance.Options.UseFont = true;
            this.TxtPdName.Properties.MaxLength = 40;
            this.TxtPdName.Size = new System.Drawing.Size(417, 22);
            this.TxtPdName.TabIndex = 16;
            // 
            // TxtPdCode
            // 
            this.TxtPdCode.EnterMoveNextControl = true;
            this.TxtPdCode.Location = new System.Drawing.Point(125, 245);
            this.TxtPdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPdCode.Name = "TxtPdCode";
            this.TxtPdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPdCode.Properties.MaxLength = 16;
            this.TxtPdCode.Size = new System.Drawing.Size(257, 22);
            this.TxtPdCode.TabIndex = 14;
            // 
            // LueSupplierCode
            // 
            this.LueSupplierCode.EnterMoveNextControl = true;
            this.LueSupplierCode.Location = new System.Drawing.Point(125, 93);
            this.LueSupplierCode.Margin = new System.Windows.Forms.Padding(6);
            this.LueSupplierCode.Name = "LueSupplierCode";
            this.LueSupplierCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSupplierCode.Properties.Appearance.Options.UseFont = true;
            this.LueSupplierCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSupplierCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSupplierCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSupplierCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSupplierCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSupplierCode.Properties.DropDownRows = 12;
            this.LueSupplierCode.Properties.NullText = "[Empty]";
            this.LueSupplierCode.Properties.PopupWidth = 500;
            this.LueSupplierCode.Size = new System.Drawing.Size(324, 22);
            this.LueSupplierCode.TabIndex = 17;
            this.LueSupplierCode.ToolTip = "F4 : Show/hide list";
            this.LueSupplierCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSupplierCode.EditValueChanged += new System.EventHandler(this.LueSupplierCode_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(15, 96);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 16);
            this.label20.TabIndex = 28;
            this.label20.Text = "Supplier";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 126);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "Category 1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGroup1
            // 
            this.LueGroup1.EnterMoveNextControl = true;
            this.LueGroup1.Location = new System.Drawing.Point(125, 120);
            this.LueGroup1.Margin = new System.Windows.Forms.Padding(6);
            this.LueGroup1.Name = "LueGroup1";
            this.LueGroup1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup1.Properties.Appearance.Options.UseFont = true;
            this.LueGroup1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGroup1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGroup1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGroup1.Properties.DropDownRows = 12;
            this.LueGroup1.Properties.NullText = "[Empty]";
            this.LueGroup1.Properties.PopupWidth = 500;
            this.LueGroup1.Size = new System.Drawing.Size(324, 22);
            this.LueGroup1.TabIndex = 30;
            this.LueGroup1.ToolTip = "F4 : Show/hide list";
            this.LueGroup1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGroup1.EditValueChanged += new System.EventHandler(this.LueGroup1_EditValueChanged);
            // 
            // LueGroup2
            // 
            this.LueGroup2.EnterMoveNextControl = true;
            this.LueGroup2.Location = new System.Drawing.Point(125, 145);
            this.LueGroup2.Margin = new System.Windows.Forms.Padding(6);
            this.LueGroup2.Name = "LueGroup2";
            this.LueGroup2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup2.Properties.Appearance.Options.UseFont = true;
            this.LueGroup2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGroup2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGroup2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGroup2.Properties.DropDownRows = 12;
            this.LueGroup2.Properties.NullText = "[Empty]";
            this.LueGroup2.Properties.PopupWidth = 500;
            this.LueGroup2.Size = new System.Drawing.Size(324, 22);
            this.LueGroup2.TabIndex = 32;
            this.LueGroup2.ToolTip = "F4 : Show/hide list";
            this.LueGroup2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(15, 151);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 31;
            this.label4.Text = "Category 2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGroup3
            // 
            this.LueGroup3.EnterMoveNextControl = true;
            this.LueGroup3.Location = new System.Drawing.Point(125, 170);
            this.LueGroup3.Margin = new System.Windows.Forms.Padding(6);
            this.LueGroup3.Name = "LueGroup3";
            this.LueGroup3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup3.Properties.Appearance.Options.UseFont = true;
            this.LueGroup3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGroup3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGroup3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGroup3.Properties.DropDownRows = 12;
            this.LueGroup3.Properties.NullText = "[Empty]";
            this.LueGroup3.Properties.PopupWidth = 500;
            this.LueGroup3.Size = new System.Drawing.Size(324, 22);
            this.LueGroup3.TabIndex = 34;
            this.LueGroup3.ToolTip = "F4 : Show/hide list";
            this.LueGroup3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(15, 176);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 16);
            this.label5.TabIndex = 33;
            this.label5.Text = "Category 3";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGroup4
            // 
            this.LueGroup4.EnterMoveNextControl = true;
            this.LueGroup4.Location = new System.Drawing.Point(125, 195);
            this.LueGroup4.Margin = new System.Windows.Forms.Padding(6);
            this.LueGroup4.Name = "LueGroup4";
            this.LueGroup4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup4.Properties.Appearance.Options.UseFont = true;
            this.LueGroup4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGroup4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGroup4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGroup4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGroup4.Properties.DropDownRows = 12;
            this.LueGroup4.Properties.NullText = "[Empty]";
            this.LueGroup4.Properties.PopupWidth = 500;
            this.LueGroup4.Size = new System.Drawing.Size(324, 22);
            this.LueGroup4.TabIndex = 36;
            this.LueGroup4.ToolTip = "F4 : Show/hide list";
            this.LueGroup4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(15, 201);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 16);
            this.label6.TabIndex = 35;
            this.label6.Text = "Category 4";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(18, 226);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 37;
            this.label7.Text = "Product Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(18, 251);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 38;
            this.label8.Text = "Product Code";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(18, 276);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 16);
            this.label9.TabIndex = 39;
            this.label9.Text = "Barcode";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBarCode
            // 
            this.TxtBarCode.EnterMoveNextControl = true;
            this.TxtBarCode.Location = new System.Drawing.Point(125, 270);
            this.TxtBarCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBarCode.Name = "TxtBarCode";
            this.TxtBarCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBarCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBarCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBarCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBarCode.Properties.MaxLength = 16;
            this.TxtBarCode.Size = new System.Drawing.Size(257, 22);
            this.TxtBarCode.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(18, 301);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "Weight";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(18, 325);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 16);
            this.label2.TabIndex = 43;
            this.label2.Text = "Minimum Price";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(19, 351);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 16);
            this.label10.TabIndex = 45;
            this.label10.Text = "Unit Price";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocNo
            // 
            this.TxtLocNo.EnterMoveNextControl = true;
            this.TxtLocNo.Location = new System.Drawing.Point(125, 371);
            this.TxtLocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocNo.Name = "TxtLocNo";
            this.TxtLocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocNo.Properties.MaxLength = 16;
            this.TxtLocNo.Size = new System.Drawing.Size(132, 22);
            this.TxtLocNo.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(19, 375);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 16);
            this.label11.TabIndex = 47;
            this.label11.Text = "Location";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWeight
            // 
            this.TxtWeight.EnterMoveNextControl = true;
            this.TxtWeight.Location = new System.Drawing.Point(125, 295);
            this.TxtWeight.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWeight.Name = "TxtWeight";
            this.TxtWeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeight.Properties.Appearance.Options.UseFont = true;
            this.TxtWeight.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWeight.Properties.MaxLength = 40;
            this.TxtWeight.Size = new System.Drawing.Size(132, 22);
            this.TxtWeight.TabIndex = 49;
            // 
            // TxtMinPrice
            // 
            this.TxtMinPrice.EnterMoveNextControl = true;
            this.TxtMinPrice.Location = new System.Drawing.Point(125, 320);
            this.TxtMinPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinPrice.Name = "TxtMinPrice";
            this.TxtMinPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtMinPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinPrice.Properties.MaxLength = 40;
            this.TxtMinPrice.Size = new System.Drawing.Size(132, 22);
            this.TxtMinPrice.TabIndex = 50;
            // 
            // TxtUnitPrice
            // 
            this.TxtUnitPrice.EnterMoveNextControl = true;
            this.TxtUnitPrice.Location = new System.Drawing.Point(125, 345);
            this.TxtUnitPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUnitPrice.Name = "TxtUnitPrice";
            this.TxtUnitPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUnitPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtUnitPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUnitPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUnitPrice.Properties.MaxLength = 40;
            this.TxtUnitPrice.Size = new System.Drawing.Size(132, 22);
            this.TxtUnitPrice.TabIndex = 51;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnInsert.FlatAppearance.BorderSize = 0;
            this.BtnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnInsert.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnInsert.Location = new System.Drawing.Point(86, 0);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(3, 5, 5, 5);
            this.BtnInsert.Name = "BtnInsert";
            this.BtnInsert.Size = new System.Drawing.Size(86, 83);
            this.BtnInsert.TabIndex = 2;
            this.BtnInsert.UseVisualStyleBackColor = true;
            this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // FrmPosProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(852, 466);
            this.Controls.Add(this.TxtUnitPrice);
            this.Controls.Add(this.TxtMinPrice);
            this.Controls.Add(this.TxtWeight);
            this.Controls.Add(this.TxtLocNo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtBarCode);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LueGroup4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LueGroup3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LueGroup2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LueGroup1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.LueSupplierCode);
            this.Controls.Add(this.TxtPdName);
            this.Controls.Add(this.TxtPdCode);
            this.Controls.Add(this.panel1);
            this.Name = "FrmPosProduct";
            this.Text = "FrmPosProduct";
            this.Load += new System.EventHandler(this.FrmPosProduct_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtPdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSupplierCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGroup4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnitPrice.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.Button BtnInsert;
        private System.Windows.Forms.Button BtnFind;
        private DevExpress.XtraEditors.TextEdit TxtPdName;
        internal DevExpress.XtraEditors.TextEdit TxtPdCode;
        internal DevExpress.XtraEditors.LookUpEdit LueSupplierCode;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueGroup1;
        internal DevExpress.XtraEditors.LookUpEdit LueGroup2;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueGroup3;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.LookUpEdit LueGroup4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtBarCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtLocNo;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtWeight;
        internal DevExpress.XtraEditors.TextEdit TxtMinPrice;
        internal DevExpress.XtraEditors.TextEdit TxtUnitPrice;
    }
}