﻿#region Update
/*
    17/11/2017 [TKG] Tambah informasi month AP dibreakdown per transaksi     
*/
#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyAPDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptMonthlyAP mFrmParent;
        private string mVdCode = string.Empty, mYr = string.Empty, mMth = string.Empty, mAcNo = string.Empty;
        private int mRow = 0;
        private byte mType = 1;

        #endregion

        #region Constructor

        public FrmRptMonthlyAPDlg(FrmRptMonthlyAP FrmParent, int Row, byte Type, string Yr, string Mth, string AcNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
            mType = Type;
            mYr = Yr;
            mMth = Mth;
            mAcNo = AcNo;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                SetGrd();
                ShowData();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "Document#", 
                        "Date",
                        "Type",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-4
                        150, 100, 150, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 6);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowData()
        {
            var SQL = string.Empty;
            var cm = new MySqlCommand();

            TxtVdName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 2);
            TxtSiteName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 4);
            TxtCurCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 5);

            if (mType == 0) SQL = GetSQL0();
            if (mType == 1) SQL = GetSQL1();
            if (mType == 2) SQL = GetSQL2();
            if (mType == 3) SQL = GetSQL3();
            if (mType == 4) SQL = GetSQL4();
            if (mType == 5) SQL = GetSQL5();
            
            Sm.CmParam<String>(ref cm, "@Dt", string.Concat(mYr, mMth, "01"));
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(mYr, mMth));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(mFrmParent.Grd1, mRow, 1));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetGrdStr(mFrmParent.Grd1, mRow, 3));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(mFrmParent.Grd1, mRow, 5));
            Sm.CmParam<String>(ref cm, "@AcNo", mAcNo);
            Sm.CmParam<int>(ref cm, "@AcNoLength", mAcNo.Length);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL,
                    new string[]
                    {
                        //0
                        "Docno",

                        //1-3
                        "DocDt",
                        "DocType",
                        "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    }, false, false, false, false
                );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4 });
        }

        private string GetSQL0()
        {
            //Opening Balance

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, DocType, Amt From ( ");

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, 'Opening (Purchase)' As DocType, ");
            SQL.AppendLine("T1.Amt-IfNull(T2.Amt, 0.000000)-IfNull(T3.Amt, 0.000000)+IfNull(T4.Amt, 0.000000) As Amt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select DocNo, DocDt, ");
            SQL.AppendLine("    Amt+TaxAmt-Downpayment As Amt ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocDt<@Dt ");
            SQL.AppendLine("    And VdCode=@VdCode ");
            SQL.AppendLine("    And IfNull(SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("    And CurCode=@CurCode ");
            SQL.AppendLine(") T1 ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select D.DocNo, Sum(C.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B ");
            SQL.AppendLine("        On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr D ");
            SQL.AppendLine("        On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("        And D.VdCode=@VdCode ");
            SQL.AppendLine("        And IfNull(D.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And D.CurCode=@CurCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            SQL.AppendLine("    Group By D.DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.DocNo, Sum(A.Amt) Amt ");
            SQL.AppendLine("    From TblAPSHdr A ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr B ");
            SQL.AppendLine("        On A.PurchaseInvoiceDocNo=B.DocNo ");
            SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("        And IfNull(B.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And B.CurCode=@CurCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            SQL.AppendLine("    Group By B.DocNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select D.DocNo, Sum(C.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B ");
            SQL.AppendLine("        On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='2' ");
            SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceHdr D ");
            SQL.AppendLine("        On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("        And D.VdCode=@VdCode ");
            SQL.AppendLine("        And IfNull(D.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And D.CurCode=@CurCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            SQL.AppendLine("    Group By D.DocNo ");
            SQL.AppendLine(") T4 On T1.DocNo=T4.DocNo ");
            SQL.AppendLine("Where T1.Amt-IfNull(T2.Amt, 0.000000)-IfNull(T3.Amt, 0.000000)+IfNull(T4.Amt, 0.000000)>0.000000 ");
            
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select DocNo, DocDt, 'Opening (Return)' As DocType, ");
            SQL.AppendLine("-1.000000*Sum(Amt) Amt ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And DocDt<@Dt ");
            SQL.AppendLine("And VdCode=@VdCode ");
            SQL.AppendLine("And IfNull(SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("Group By DocNo, DocDt ");
            
            SQL.AppendLine(") T Where T.Amt<>0.000000 ");
            SQL.AppendLine("Order By T.DocType, T.DocDt, T.DocNo; ");

            return SQL.ToString();
        }

        private string GetSQL1()
        {
            //Purchase Invoice
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, 'Invoice' As DocType, ");
            SQL.AppendLine("T1.Amt+T1.TaxAmt-T1.Downpayment+IfNull(T2.DAmt, 0.000000)-IfNull(T2.CAmt, 0.000000) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.DocNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl4 B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And Length(B.AcNo)>@AcNoLength ");
            SQL.AppendLine("        And Left(B.AcNo, @AcNoLength)=@AcNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("    And A.VdCode=@VdCode ");
            SQL.AppendLine("    And IfNull(A.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("    And A.CurCode=@CurCode ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And Left(T1.DocDt, 6)=@YrMth ");
            SQL.AppendLine("And T1.VdCode=@VdCode ");
            SQL.AppendLine("And IfNull(T1.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("And T1.CurCode=@CurCode ");
            SQL.AppendLine("Order By T1.DocDt, T1.DocNo;");

            return SQL.ToString();
        }

        private string GetSQL2()
        {
            //Purchase Invoice (Additional Amount)

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, 'Cost/Disc/etc' As DocType, ");
            SQL.AppendLine("Sum(-1.000000*B.DAmt)+Sum(B.CAmt) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl4 B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Length(B.AcNo)>@AcNoLength ");
            SQL.AppendLine("    And Left(B.AcNo, @AcNoLength)=@AcNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("And A.VdCode=@VdCode ");
            SQL.AppendLine("And IfNull(A.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("And A.CurCode=@CurCode ");
            SQL.AppendLine("Group By A.DocNo, A.DocDt ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            return SQL.ToString();
        }

        private string GetSQL3()
        {
            //Purchase Return Invoice

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, 'Purchase Return' As DocType, ");
            SQL.AppendLine("-1.000000*Amt As Amt ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And Left(DocDt, 6)=@YrMth ");
            SQL.AppendLine("And VdCode=@VdCode ");
            SQL.AppendLine("And IfNull(SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("Order By DocDt, DocNo;");

            return SQL.ToString();
        }

        private string GetSQL4()
        {
            //Voucher

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, 'Voucher' As DocType, -1.000000*C.Amt As Amt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    And B.VdCode=@VdCode ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr D ");
            SQL.AppendLine("    On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    And D.VdCode=@VdCode ");
            SQL.AppendLine("    And IfNull(D.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("    And D.CurCode=@CurCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, 'Voucher' As DocType, -1.000000*C.Amt As Amt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    And B.VdCode=@VdCode ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='2' ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceHdr D ");
            SQL.AppendLine("    On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    And D.VdCode=@VdCode ");
            SQL.AppendLine("    And IfNull(D.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("    And D.CurCode=@CurCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("Order By DocDt, DocNo;");

            return SQL.ToString();
        }

        private string GetSQL5()
        {
            //AP Settlement

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, 'AP Settlement' As DocType, -1.000000*A.Amt As Amt ");
            SQL.AppendLine("From TblAPSHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr B ");
            SQL.AppendLine("    On A.PurchaseInvoiceDocNo=B.DocNo ");
            SQL.AppendLine("    And B.VdCode=@VdCode ");
            SQL.AppendLine("    And IfNull(B.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("    And B.CurCode=@CurCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            return SQL.ToString();
        }

        #endregion

        #region Event

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion
    }
}
