﻿#region Update
/*
    29/11/2017 [TKG] reporting MR to invoice for cashier.
    25/12/2017 [TKG] tambah informasi approval status
    07/03/2018 [TKG] Berdasarkan parameter IsRptMRToPIFilterByPT, data yg ditampilkan perlu difilter berdasarkan payment term atau tidak.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMRToPI : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, 
            mMainCurCode = string.Empty;
        private bool 
            mIsFilterByItCt = false,
            mIsRptMRToPIFilterByPT = false;

        #endregion

        #region Constructor

        public FrmRptMRToPI(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptMRToPIFilterByPT = Sm.IsDataExist("Select 1 From TblParameter Where ParCode='PtCodeCash' And ParValue Is Not Null;");
        }

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();
            var Filter3 = string.Empty;
            if (Filter1.Length > 0) Filter3 = Filter1.Replace("A.", "T1.");

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DeptName, ");
            SQL.AppendLine("B.ItCode, D.ItName, D.PurchaseUomCode, ");
            SQL.AppendLine("B.Qty As MaterialRequestQty, ");
            SQL.AppendLine("Case B.Status  ");
	        SQL.AppendLine("    When 'O' Then 'Outstanding' ");
	        SQL.AppendLine("    When 'A' Then 'Approved' "); 
	        SQL.AppendLine("    When 'C' Then 'Cancelled' "); 
            SQL.AppendLine("End As MaterialRequestStatusDesc, ");
            SQL.AppendLine("IfNull(E.Qty, 0.00) As MRQtyCancelQty, ");
            SQL.AppendLine("IfNull(F.Qty, 0.00) As PORequestQty, ");
            SQL.AppendLine("IfNull(G.Qty, 0.00) As POQty, ");
            SQL.AppendLine("IfNull(H.Qty, 0.00) As POQtyCancelQty, ");
            SQL.AppendLine("IfNull(I.Qty, 0.00) As RecvVdQty, ");
            SQL.AppendLine("IfNull(J.Qty, 0.00) As DOVdQty, ");
            SQL.AppendLine("IfNull(K.Qty, 0.00) As PurchaseInvoiceQty, ");
            SQL.AppendLine("IfNull(L.Qty, 0.00) As PurchaseReturnInvoiceQty, ");
            SQL.AppendLine("IfNull(M.Amt, 0.00) As POAmtBeforeTax, ");
            SQL.AppendLine("IfNull(M.TaxAmt, 0.00) As POTaxAmt, ");
            SQL.AppendLine("IfNull(N.Amt, 0.00) As PurchaseInvoiceAmtBeforeTax, ");
            SQL.AppendLine("IfNull(N.TaxAmt, 0.00) As PurchaseInvoiceTaxAmt, ");
            SQL.AppendLine("O.Remark As Vendor, ");
            SQL.AppendLine("P.Remark As PO, ");
            SQL.AppendLine("Q.Remark As RecvVd, ");
            SQL.AppendLine("R.Remark As PurchaseInvoice, B.DNo ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
	        SQL.AppendLine("    On A.DocNo=B.DocNo ");
	        SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine(Filter2);
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T3.Qty) As Qty ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblMRQtyCancel T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MRDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MRDNo ");
		    SQL.AppendLine("        And T3.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") E On B.DocNo=E.DocNo And B.DNo=E.DNo ");
            SQL.AppendLine("Inner Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T3.Qty) As Qty  ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
		    SQL.AppendLine("        And T3.CancelInd='N' ");
		    SQL.AppendLine("        And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") F On B.DocNo=F.DocNo And B.DNo=F.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T4.Qty) As Qty  ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo "); 
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") G On B.DocNo=G.DocNo And B.DNo=G.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T5.Qty) As Qty  ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblPOQtyCancel T5 "); 
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") H On B.DocNo=H.DocNo And B.DNo=H.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T5.QtyPurchase) As Qty  ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  "); 
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") I On B.DocNo=I.DocNo And B.DNo=I.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T6.Qty) As Qty  ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblDOVdDtl T6  ");
		    SQL.AppendLine("        On T5.DocNo=T6.RecvVdDocNo  ");
		    SQL.AppendLine("        And T5.DNo=T6.RecvVdDNo ");
		    SQL.AppendLine("        And T6.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") J On B.DocNo=J.DocNo And B.DNo=J.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T5.QtyPurchase) As Qty "); 
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo "); 
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T6 "); 
		    SQL.AppendLine("    On T5.DocNo=T6.RecvVdDocNo  ");
		    SQL.AppendLine("    And T5.DNo=T6.RecvVdDNo ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T7 On T6.DocNo=T7.DocNo  And T7.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") K On B.DocNo=K.DocNo And B.DNo=K.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T6.Qty) As Qty  ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblDOVdDtl T6  ");
		    SQL.AppendLine("        On T5.DocNo=T6.RecvVdDocNo  ");
		    SQL.AppendLine("        And T5.DNo=T6.RecvVdDNo ");
		    SQL.AppendLine("        And T6.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceDtl T7  ");
		    SQL.AppendLine("        On T6.DocNo=T7.DOVdDocNo  ");
		    SQL.AppendLine("        And T6.DNo=T7.DOVdDNo ");
	        SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceHdr T8  ");
		    SQL.AppendLine("        On T7.DocNo=T8.DocNo  And T8.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") L On B.DocNo=L.DocNo And B.DNo=L.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo,  ");
	        SQL.AppendLine("    Sum(( ");
		    SQL.AppendLine("    (T4.Qty*T5.UPrice) - ");
	        SQL.AppendLine("    (T4.Qty*T5.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("    (T4.Qty*T4.DiscountAmt)+ ");
		    SQL.AppendLine("    (T4.Qty*T4.RoundingValue)) ");
	        SQL.AppendLine("    * ");
            SQL.AppendLine("    Case When T10.CurCode=@MainCurCode Then 1.00 Else ");
	        SQL.AppendLine("    (IfNull(( ");
		    SQL.AppendLine("    Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("    Where RateDt<=T6.DocDt And CurCode1=T10.CurCode And CurCode2=@MainCurCode ");
		    SQL.AppendLine("    Order By RateDt Desc Limit 1  ");
		    SQL.AppendLine("    ), 0.00) ");
	        SQL.AppendLine("    ) End) As Amt, ");
	        SQL.AppendLine("    Sum(( ");
		    SQL.AppendLine("    (((T4.Qty*T5.UPrice) - ");
	        SQL.AppendLine("    (T4.Qty*T5.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("    (T4.Qty*T4.DiscountAmt)+ ");
		    SQL.AppendLine("    (T4.Qty*T4.RoundingValue)) ");
		    SQL.AppendLine("    *IfNull(T7.TaxRate, 0.00)*0.01)+ ");
		    SQL.AppendLine("    (((T4.Qty*T5.UPrice) - ");
	        SQL.AppendLine("    (T4.Qty*T5.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("    (T4.Qty*T4.DiscountAmt)+ ");
		    SQL.AppendLine("    (T4.Qty*T4.RoundingValue)) ");
		    SQL.AppendLine("    *IfNull(T8.TaxRate, 0.00)*0.01)+ ");		
		    SQL.AppendLine("    (((T4.Qty*T5.UPrice) - ");
	        SQL.AppendLine("    (T4.Qty*T5.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("    (T4.Qty*T4.DiscountAmt)+ ");
		    SQL.AppendLine("    (T4.Qty*T4.RoundingValue)) ");
		    SQL.AppendLine("    *IfNull(T9.TaxRate, 0.00)*0.01)) ");
			SQL.AppendLine("    * ");
            SQL.AppendLine("    Case When T10.CurCode=@MainCurCode Then 1.00 Else ");
	        SQL.AppendLine("    (IfNull(( ");
		    SQL.AppendLine("        Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("        Where RateDt<=T6.DocDt And CurCode1=T10.CurCode And CurCode2=@MainCurCode ");
		    SQL.AppendLine("        Order By RateDt Desc Limit 1  ");
		    SQL.AppendLine("    ), 0.00) ");
	        SQL.AppendLine("    ) End) As TaxAmt ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblQtDtl T5  ");
		    SQL.AppendLine("        On T3.QtDocNo=T5.DocNo  ");
		    SQL.AppendLine("        And T3.QtDNo=T5.DNo ");
	        SQL.AppendLine("    Inner Join TblPOHdr T6 on T4.DocNo=T6.DocNo ");
	        SQL.AppendLine("    Left Join TblTax T7 on T6.TaxCode1=T7.TaxCode ");
            SQL.AppendLine("    Left Join TblTax T8 on T6.TaxCode2=T8.TaxCode ");
            SQL.AppendLine("    Left Join TblTax T9 on T6.TaxCode3=T9.TaxCode ");
	        SQL.AppendLine("    Inner Join TblQtHdr T10 On T3.QtDocNo=T10.DocNo  ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") M On B.DocNo=M.DocNo And B.DNo=M.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo,  ");
	        SQL.AppendLine("    Sum(( ");
		    SQL.AppendLine("    (T5.QtyPurchase*T8.UPrice) - ");
	        SQL.AppendLine("    (T5.QtyPurchase*T8.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("    ((T5.QtyPurchase/T4.Qty)*T4.DiscountAmt)+ ");
		    SQL.AppendLine("    ((T5.QtyPurchase/T4.Qty)*T4.RoundingValue) ");
	        SQL.AppendLine("    )* ");
            SQL.AppendLine("    Case When T12.CurCode=@MainCurCode Then 1.00 Else ");
	        SQL.AppendLine("    (IfNull(( ");
		    SQL.AppendLine("        Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("        Where RateDt<=T7.DocDt And CurCode1=T12.CurCode And CurCode2=@MainCurCode  ");
		    SQL.AppendLine("        Order By RateDt Desc Limit 1  ");
		    SQL.AppendLine("    ), 0.00) ");
	        SQL.AppendLine("    ) End) As Amt, ");
	        SQL.AppendLine("    Sum( ");
		    SQL.AppendLine("        (((T5.QtyPurchase*T8.UPrice) - ");
	        SQL.AppendLine("        (T5.QtyPurchase*T8.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("        ((T5.QtyPurchase/T4.Qty)*T4.DiscountAmt)+ ");
		    SQL.AppendLine("        ((T5.QtyPurchase/T4.Qty)*T4.RoundingValue)) ");
		    SQL.AppendLine("        *IfNull(T9.TaxRate, 0.00)*0.01)+ ");
		    SQL.AppendLine("        (((T5.QtyPurchase*T8.UPrice) - ");
	        SQL.AppendLine("        (T5.QtyPurchase*T8.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("        ((T5.QtyPurchase/T4.Qty)*T4.DiscountAmt)+ ");
		    SQL.AppendLine("        ((T5.QtyPurchase/T4.Qty)*T4.RoundingValue)) ");
		    SQL.AppendLine("        *IfNull(T10.TaxRate, 0.00)*0.01)+ ");
		    SQL.AppendLine("        (((T5.QtyPurchase*T8.UPrice) - ");
	        SQL.AppendLine("        (T5.QtyPurchase*T8.UPrice*T4.Discount*0.01) - ");
		    SQL.AppendLine("        ((T5.QtyPurchase/T4.Qty)*T4.DiscountAmt)+ ");
		    SQL.AppendLine("        ((T5.QtyPurchase/T4.Qty)*T4.RoundingValue)) ");
		    SQL.AppendLine("        *IfNull(T11.TaxRate, 0.00)*0.01) ");
	        SQL.AppendLine("        * ");
            SQL.AppendLine("        Case When T12.CurCode=@MainCurCode Then 1.00 Else ");
	        SQL.AppendLine("        (IfNull(( ");
		    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
		    SQL.AppendLine("            Where RateDt<=T7.DocDt And CurCode1=T12.CurCode And CurCode2=@MainCurCode ");
		    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
		    SQL.AppendLine("        ), 0.00) ");
	        SQL.AppendLine("    ) End) As TaxAmt ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T6 "); 
		    SQL.AppendLine("        On T5.DocNo=T6.RecvVdDocNo  ");
		    SQL.AppendLine("        And T5.DNo=T6.RecvVdDNo ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T7  ");
		    SQL.AppendLine("        On T6.DocNo=T7.DocNo  And T7.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblQtDtl T8  ");
		    SQL.AppendLine("        On T3.QtDocNo=T8.DocNo  ");
		    SQL.AppendLine("        And T3.QtDNo=T8.DNo ");
	        SQL.AppendLine("    Left Join TblTax T9 on T7.TaxCode1=T9.TaxCode ");
            SQL.AppendLine("    Left Join TblTax T10 on T7.TaxCode2=T10.TaxCode ");
            SQL.AppendLine("    Left Join TblTax T11 on T7.TaxCode3=T11.TaxCode ");
	        SQL.AppendLine("    Inner Join TblQtHdr T12 On T3.QtDocNo=T12.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") N On B.DocNo=N.DocNo And B.DNo=N.DNo ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo,  ");
	        SQL.AppendLine("    Group_Concat( ");
	        SQL.AppendLine("        Concat(VdName, ' ( ', IfNull(PtName, ''), ' : ', ");
	        SQL.AppendLine("        T4.CurCode, ' ',  ");
	        SQL.AppendLine("        Convert(Format(T5.UPrice, 2) Using utf8),' X ', ");
	        SQL.AppendLine("        Convert(Format(T3.Qty, 2) Using utf8), ' )' ");
	        SQL.AppendLine("        ) Separator '; ') As Remark ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
            SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
		    SQL.AppendLine("        And T3.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblQtHdr T4 On T3.QtDocNo=T4.DocNo  ");
	        SQL.AppendLine("    Inner Join TblQtDtl T5 On T3.QtDocNo=T5.DocNo And T3.QtDNo=T5.DNo ");
	        SQL.AppendLine("    Inner Join TblVendor T6 On T4.VdCode=T6.VdCode ");
            SQL.AppendLine("    Inner Join TblPaymentTerm T7 On T4.PtCode=T7.PtCode ");
            if (mIsRptMRToPIFilterByPT)
            {
                SQL.AppendLine("        And Find_In_Set(T7.PtCode, ");
                SQL.AppendLine("        (Select ParValue From TblParameter ");
                SQL.AppendLine("        Where ParCode='PtCodeCash' ");
                SQL.AppendLine("        And ParValue Is Not Null)) ");
            }
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") O On B.DocNo=O.DocNo And B.DNo=O.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo,  ");
	        SQL.AppendLine("    Group_Concat(Distinct T4.DocNo Separator ', ') As Remark ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
		    SQL.AppendLine("        And T3.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") P On B.DocNo=P.DocNo And B.DNo=P.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo,  ");
	        SQL.AppendLine("    Group_Concat(Distinct  ");
	        SQL.AppendLine("    Concat(T6.DocNo, ' on ', T7.WhsName,  ");
	        SQL.AppendLine("    Case When T6.VdDONo Is Null Then '' Else Concat('(DO#: ', T6.VdDONo, ')') 	 End)  ");
	        SQL.AppendLine("    Separator ', ') As Remark ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
		    SQL.AppendLine("        And T3.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblRecvVdHdr T6 On T5.DocNo=T6.DocNo  ");
	        SQL.AppendLine("    Inner Join TblWarehouse T7 On T6.WhsCode=T7.WhsCode  ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") Q On B.DocNo=Q.DocNo And B.DNo=Q.DNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T2.DocNo, T2.DNo,  ");
	        SQL.AppendLine("    Group_Concat(Distinct T6.DocNo Separator ', ') As Remark ");
	        SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo  ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T3  ");
		    SQL.AppendLine("        On T2.DocNo=T3.MaterialRequestDocNo  ");
		    SQL.AppendLine("        And T2.DNo=T3.MaterialRequestDNo ");
		    SQL.AppendLine("        And T3.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblPODtl T4  ");
		    SQL.AppendLine("        On T3.DocNo=T4.PORequestDocNo  ");
		    SQL.AppendLine("        And T3.DNo=T4.PORequestDNo ");
		    SQL.AppendLine("        And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T5  ");
		    SQL.AppendLine("        On T4.DocNo=T5.PODocNo  ");
		    SQL.AppendLine("        And T4.DNo=T5.PODNo ");
		    SQL.AppendLine("        And T5.CancelInd='N' ");
		    SQL.AppendLine("        And T5.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl T6 On T5.DocNo=T6.RecvVdDocNo And T5.DNo=T6.RecvVdDNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter3);
	        SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") R On B.DocNo=R.DocNo And B.DNo=R.DNo ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Material Request#",
                        "Date",
                        "Department",
                        "Item's Code",
                        "Item's Name",
                        
                        //6-10
                        "UoM",
                        "Status",
                        "",
                        "MR"+Environment.NewLine+"Quantity",
                        "MR Cancellation"+Environment.NewLine+"Quantity",
                        
                        //11-15
                        "PO Request"+Environment.NewLine+"Quantity",
                        "PO"+Environment.NewLine+"Quantity",
                        "PO Cancellation"+Environment.NewLine+"Quantity",
                        "Received"+Environment.NewLine+"Quantity",
                        "Returned"+Environment.NewLine+"Quantity",
                        
                        //16-20
                        "Purchase Invoice"+Environment.NewLine+"Quantity",
                        "Purchase Return Invoice"+Environment.NewLine+"Quantity",
                        "PO"+Environment.NewLine+"Amount Before Tax"+Environment.NewLine+mMainCurCode,
                        "PO"+Environment.NewLine+"Tax"+Environment.NewLine+mMainCurCode,
                        "PO"+Environment.NewLine+"Amount After Tax"+Environment.NewLine+mMainCurCode,
                        
                        //21-25
                        "Purchase Invoice"+Environment.NewLine+"Amount Before Tax"+Environment.NewLine+mMainCurCode,
                        "Purchase Invoice"+Environment.NewLine+"Tax"+Environment.NewLine+mMainCurCode,
                        "Purchase Invoice"+Environment.NewLine+"Amount After Tax"+Environment.NewLine+mMainCurCode,
                        "Vendor",
                        "Purchase Order",
                        
                        //26-28
                        "Receiving From Vendor",
                        "Purchase Invoice",
                        "DNo"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        
                        //1-5
                        140, 80, 180, 80, 200,

                        //6-10
                        80, 80, 20, 120, 120, 

                        //11-15
                        120, 120, 120, 120, 120,

                        //16-20
                        120, 120, 120, 120, 120,

                        //21-25
                        120, 120, 120, 200, 200,  

                        //26-28
                        200, 200, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 13, 15, 17, 28 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter1 = " ", Filter2 = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.FilterStr(ref Filter1, ref cm, TxtMaterialRequestDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter1, Filter2),
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DeptName", "ItCode", "ItName", "PurchaseUomCode", 
            
                            //6-10
                            "MaterialRequestStatusDesc", "MaterialRequestQty", "MRQtyCancelQty", "PORequestQty", "POQty", 
                            
                            //11-15
                            "POQtyCancelQty", "RecvVdQty", "DOVdQty", "PurchaseInvoiceQty", "PurchaseReturnInvoiceQty", 
                            
                            //16-20
                            "POAmtBeforeTax", "POTaxAmt", "PurchaseInvoiceAmtBeforeTax", "PurchaseInvoiceTaxAmt", "Vendor", 
                            
                            //21-24
                            "PO", "RecvVd", "PurchaseInvoice", "DNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Grd.Cells[Row, 20].Value = Sm.GetGrdDec(Grd, Row, 18) + Sm.GetGrdDec(Grd, Row, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Grd.Cells[Row, 23].Value = Sm.GetGrdDec(Grd, Row, 21) + Sm.GetGrdDec(Grd, Row, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 18, 19, 20, 21, 22, 23 });

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowApproval(Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 28));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowApproval(Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 28));
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void ShowApproval(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();
            var Msg = string.Empty;
            var IsFirst = true;
            var Type = string.Empty;

            SQL.AppendLine("Select Type, DocNo, StatusDesc, UserCode, DocApprovalStatusDesc, LastUpDt, Remark From (");
            SQL.AppendLine("    Select '1' As Type, T1.DocNo, T2.ApprovalDNo, ");
            SQL.AppendLine("    Case T1.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("    T2.UserCode, ");
            SQL.AppendLine("    Case T2.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As DocApprovalStatusDesc, ");
            SQL.AppendLine("    Case When T2.LastUpDt Is Not Null Then  ");
            SQL.AppendLine("    Concat(Substring(T2.LastUpDt, 7, 2), '/', Substring(T2.LastUpDt, 5, 2), '/', Left(T2.LastUpDt, 4))  ");
            SQL.AppendLine("    Else Null End As LastUpDt, ");
            SQL.AppendLine("    T2.Remark  ");
            SQL.AppendLine("    From TblMaterialRequestDtl T1 ");
            SQL.AppendLine("    Inner Join TblDocApproval T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo And T2.DocType='MaterialRequest' And T2.Status In ('A', 'C') ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    And T1.DNo=@DNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select '2' As Type, T1.DocNo, T2.ApprovalDNo, ");
            SQL.AppendLine("    Case T1.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("    T2.UserCode, ");
            SQL.AppendLine("    Case T2.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As DocApprovalStatusDesc, ");
            SQL.AppendLine("    Case When T2.LastUpDt Is Not Null Then  ");
            SQL.AppendLine("    Concat(Substring(T2.LastUpDt, 7, 2), '/', Substring(T2.LastUpDt, 5, 2), '/', Left(T2.LastUpDt, 4))  ");
            SQL.AppendLine("    Else Null End As LastUpDt, ");
            SQL.AppendLine("    T2.Remark  ");
            SQL.AppendLine("    From TblPORequestDtl T1 ");
            SQL.AppendLine("    Inner Join TblDocApproval T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo And T2.DocType='PORequest' And T2.Status In ('A', 'C') ");
            SQL.AppendLine("    Where T1.MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("    And T1.MaterialRequestDNo=@DNo ");
            SQL.AppendLine(") T Order By T.Type, T.DocNo, T.ApprovalDNo;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandText = SQL.ToString(),
                        CommandTimeout = 600
                    };
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm, "@DNo", DNo);
                    using (var dr = cm.ExecuteReader())
                    {
                        var c = Sm.GetOrdinal(dr, new string[] 
                        { 
                            "Type",
                            "DocNo", "StatusDesc", "UserCode", "DocApprovalStatusDesc", "LastUpDt", 
                            "Remark" 
                        });
                        while (dr.Read())
                        {

                            if (Sm.DrStr(dr, 0) == "1")
                            {
                                if (IsFirst)
                                {
                                    Type = "1";
                                    Msg =
                                        "Material Request# : " + Sm.DrStr(dr, 1) +
                                        " (" + Sm.DrStr(dr, 2) + ")" + Environment.NewLine + Environment.NewLine;
                                }
                            }

                            if (Sm.DrStr(dr, 0) == "2" && Type=="1")
                            {
                                Type = "2";
                                Msg +=
                                    Environment.NewLine + "PO Request# : " + Sm.DrStr(dr, 1) + 
                                    " (" + Sm.DrStr(dr, 2) + ")" + Environment.NewLine + Environment.NewLine;
                            }

                            Msg +=
                                "User : " + Sm.DrStr(dr, 3) + 
                                " (" + Sm.DrStr(dr, 4) + " on " + Sm.DrStr(dr, 5) + ")" +
                                ((Sm.DrStr(dr, 6).Length == 0) ?
                                string.Empty :
                                "Remark : " + Sm.DrStr(dr, 6)) +
                                Environment.NewLine;

                            if (IsFirst) IsFirst = false;   
                        }
                        cm.Dispose();
                    }
                }
                if (Msg.Length == 0) Msg = "No data approved/cancelled.";
                Sm.StdMsg(mMsgType.Info, Msg);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtMaterialRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMaterialRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Material request#");
        }
   
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
