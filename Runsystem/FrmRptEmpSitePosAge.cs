﻿#region update
/*
      10/09/2020 [ICA/SRN] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSitePosAge : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpSitePosAge(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueDivisionCode(ref LueDivisionCode);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Sm.Left(CurrentDateTime, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private void GetParameter()
        //{

        //}

        protected override void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.SiteName, C.DivisionName, D.DeptName, E.PositionStatusName, ");
            SQL.AppendLine("COUNT(U1.EmpCode) Usia1, COUNT(U2.EmpCode) Usia2, COUNT(U3.EmpCode) Usia3, COUNT(U4.EmpCode) Usia4, COUNT(U5.EmpCode) Usia5, ");
            SQL.AppendLine("COUNT(U1.EmpCode) + COUNT(U2.EmpCode) + COUNT(U3.EmpCode) + COUNT(U4.EmpCode) + COUNT(U5.EmpCode) Total ");
            SQL.AppendLine("FROM tblemployee A ");
            SQL.AppendLine("LEFT JOIN tblsite B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("LEFT JOIN tbldivision C ON A.DivisionCode = C.DivisionCode ");
            SQL.AppendLine("LEFT JOIN tbldepartment D ON A.DeptCode = D.DeptCode ");
            SQL.AppendLine("LEFT JOIN tblpositionstatus E ON A.PositionStatusCode = E.PositionStatusCode ");
            SQL.AppendLine("LEFT JOIN tblemployee U1 ON A.EmpCode=U1.EmpCode AND (Date_format( From_Days( TO_DAYS(CONCAT(@yr, @Mth, '01')) - TO_DAYS(Concat(Substring(A.BirthDt, 1, 6), '01'))), '%Y' ) + 0) <= 20 ");
            SQL.AppendLine("LEFT JOIN tblemployee U2 ON A.EmpCode=U2.EmpCode AND (Date_format( From_Days( TO_DAYS(CONCAT(@yr, @Mth, '01')) - TO_DAYS(Concat(Substring(A.BirthDt, 1, 6), '01'))), '%Y' ) + 0) between 21 AND 30 ");
            SQL.AppendLine("LEFT JOIN tblemployee U3 ON A.EmpCode=U3.EmpCode AND (Date_format( From_Days( TO_DAYS(CONCAT(@yr, @Mth, '01')) - TO_DAYS(Concat(Substring(A.BirthDt, 1, 6), '01'))), '%Y' ) + 0) BETWEEN 31 AND 40 ");
            SQL.AppendLine("LEFT JOIN tblemployee U4 ON A.EmpCode=U4.EmpCode AND (Date_format( From_Days( TO_DAYS(CONCAT(@yr, @Mth, '01')) - TO_DAYS(Concat(Substring(A.BirthDt, 1, 6), '01'))), '%Y' ) + 0) BETWEEN 41 AND 50 ");
            SQL.AppendLine("LEFT JOIN tblemployee U5 ON A.EmpCode=U5.EmpCode AND (Date_format( From_Days( TO_DAYS(CONCAT(@yr, @Mth, '01')) - TO_DAYS(Concat(Substring(A.BirthDt, 1, 6), '01'))), '%Y' ) + 0) BETWEEN 51 AND 56 ");
            SQL.AppendLine("WHERE (A.ResignDt IS NULL OR (A.ResignDt IS NOT NULL AND A.ResignDt > Left(CurrentDateTime(), 8))) ");
 

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Site", 
                        "Division",
                        "Department",
                        "Position Status",
                        "<=20",
                        
                        //6-10
                        "21-30",
                        "31-40",
                        "41-50",
                        "51-56",
                        "TOTAL",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 270, 270, 150, 70,  
                        
                        //6-10
                        70, 70, 70, 70, 70,   
                        
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        //override protected void HideInfoInGrd()
        //{
        //    Sm.GrdColInvisible(Grd1, new int[] {}, !ChkHideInfoInGrd.Checked);
        //}

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "AND 0=0";

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDivisionCode), "A.DivisionCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " GROUP BY B.SiteName, C.DivisionName, D.DeptName, E.PositionStatusName ",
                        new string[]
                        {
                            //0
                            "SiteName",

                            //1-5
                            "DivisionName", "DeptName", "PositionStatusName", "Usia1",   
                            
                            //6-9
                            "Usia2", "Usia3", "Usia4", "Usia5", "Total"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);

                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkDivisionCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Division");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion
    }
}
