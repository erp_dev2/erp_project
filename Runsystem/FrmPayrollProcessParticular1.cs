﻿#region Update
/*
    11/10/2019 [TKG/MMM] MMM
    04/11/2019 [HAR/MMM] perhitungan PPH21 Gross UP berdasarkan payroll formula
    14/11/2019 [TKG/MMM] Menghilangkan transport/meal
    10/12/2019 [TKG/MMM] ubah proses payroll processing
    13/12/2019 [TKG/MMM] ubah proses perhitungan gaji dan prorate.
    03/01/2019 [HAR/MMM] perhitungan pajak di sesuaikan dengan berdasarkan payroll formula
    13/01/2019 [RF/MMM] amount insentif/penalti diambil dari detail
    29/01/2020 [RF] membuat OT fleksibel
    30/01/2020 [RF] membuat Social Security fleksibel
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollProcessParticular1 : Form
    {
        #region Field, Property

        private string
            mDeptCode = string.Empty,
            mSystemType = string.Empty,
            mPayrunPeriod = string.Empty,
            mPGCode = string.Empty,
            mSiteCode = string.Empty,

            mNeglectLeaveCode = "002",
            mOTRequestType = "1",
            mCoordinatorLeaveCode = "007",
            mDisasterLeaveCode = "014",
            mGoHomeEarlyLeaveCode = "017",
            mMaternityLeaveCode = "015",
            mMiscarriageLeaveCode = "016",
            mOperatorLeaveCode = "001",
            mEmpSystemTypeHarian = "1",
            mEmpSystemTypeBorongan = "2",
            mEmpSystemTypeAllIn = "3",
            mEmploymentStatusTetap = "1",
            mEmploymentStatusLepas = "2",
            mEmploymentStatusSpesial = "3",
            mPayrunPeriodBulanan = "1",
            mPayrunPeriod2Mingguan = "2",
            mADCodeTransport = "03",
            mADCodeMeal = "05",
            mSSPCodeForHealth = "BPJS-KES",
            mSSPCodeForEmployment = "BPJS-TNK",
            mSSForRetiring = "Pens",
            mSSOldAgeInsurance = "JHT",
            mSalaryInd = "1",
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty,
            mWSCodesHolidayWithExtraFooding = string.Empty,
            mSSCodePension = string.Empty,
            mSSCodePension2 = string.Empty,
            mADCodeOTHoliday = string.Empty,
            mADCodeOTNonHoliday = string.Empty,
            mProratedADBasedOnLeave = string.Empty,
            mLeaveForProratedAD = string.Empty,
            mProratedAD = string.Empty,
            mSSYr = string.Empty,
            mSSMth = string.Empty,
            mOTFormulaForNonHoliday5WorkingDays = string.Empty,
            mOTFormulaForHoliday5WorkingDays = string.Empty,
            mOTFormulaForHoliday6WorkingDays = string.Empty,
            mOTFormulaForNonHoliday6WorkingDays = string.Empty,
            mOTFormulaForNationalHoliday = string.Empty,
            mTxtOTFormulaCode = string.Empty,
            mWorkingGroup5WorkingDays = string.Empty,
            mWorkingGroup6WorkingDays = string.Empty;
            
        List<string> mlProratedADBasedOnLeave = new List<string>();
        List<string> mlLeaveForProratedAD = new List<string>();

        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 25,
            mFunctionalExpenses = 5m,
            mFunctionalExpensesMaxAmt = 500000m,
            mOTFormulaHr1 = 1m,
            mOTFormulaHr2 = 24m,
            mOTFormulaIndex1 = 1.5m,
            mOTFormulaIndex2 = 2m,
            mHOWorkingDayPerMth = 25m,
            mSiteWorkingDayPerMth = 21m,
            mHoliday1stHrForHO = 7m,
            mHoliday1stHrForSite = 7m,
            mHoliday2ndHrForHO = 1m,
            mHoliday2ndHrForSite = 1m,
            mHoliday1stHrIndex = 2m,
            mHoliday2ndHrIndex = 3m,
            mHoliday3rdHrIndex = 4m,
            mNonNPWPTaxPercentage = 120m,
            mADCodeEmploymentPeriodYr = 5m,
            mSSEmploymentErPerc = 0m,
            mSSOldAgeEePerc = 0m,
            mSSPensionEePerc = 0m,
            mFieldAssignmentMinWorkDuration = 0m,
            mXMinToleranceLateMealTransportDeduction = 0m,
            mXPercentageLateMealTransportDeduction = 0m,
            mADOTHour = 6m,
            mADOTMinHour = 1m,
            mMinLeaveDaysForProratedAD = 12m,
            mWorkingDayPerWeek = 0m;

        private bool
            mIsUseLateValidation = false,
            mIsUseOTReplaceLeave = false,
            mIsOTRoutineRounding = false,
            mIsOTBasedOnOTRequest = false,
            mIsWorkingHrBasedOnSchedule = false,
            mIsPayrollProcessUseSiteWorkingDayPerMth = false,
            mHOInd = true,
            mIsOTHolidayDeduct1Hr = false,
            mIsUseLateMealTransportDeduction = false,
            mIsPPProcessAllOutstandingSS = false,
            mIsPayrunSSYrMthEnabled = false,
            mIsOTBaseOnOTFormula = false;

        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTFormula> mlOTFormula = null;
        private List<OTAdjustment> mlOTAdjustment = null;
        private List<AdvancePaymentProcess> mlAdvancePaymentProcess = null;
        private List<EmpOT> mEmpOT = null;
       
        #endregion

        #region Constructor

        public FrmPayrollProcessParticular1(string MenuCode)
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Setting

        private void GetParameter()
        {
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            mOTRequestType = Sm.GetParameter("OTRequestType");
            mWSCodesHolidayWithExtraFooding = Sm.GetParameter("WSCodesHolidayWithExtraFooding");
            mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");
            mDirectLaborDailyWorkHour = Sm.GetParameterDec("DirectLaborDailyWorkHour");
            mWorkingHr = Sm.GetParameterDec("WorkingHr");
            mNoWorkDay2Mingguan = Sm.GetParameterDec("NoWorkDay2Mingguan");
            mNoWorkDayBulanan = Sm.GetParameterDec("NoWorkDayBulanan");
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
            mHOWorkingDayPerMth = Sm.GetParameterDec("HOWorkingDayPerMth");
            mSiteWorkingDayPerMth = Sm.GetParameterDec("SiteWorkingDayPerMth");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mCoordinatorLeaveCode = Sm.GetParameter("CoordinatorLeaveCode");
            mDisasterLeaveCode = Sm.GetParameter("DisasterLeaveCode");
            mGoHomeEarlyLeaveCode = Sm.GetParameter("GoHomeEarlyLeaveCode");
            mMaternityLeaveCode = Sm.GetParameter("MaternityLeaveCode");
            mMiscarriageLeaveCode = Sm.GetParameter("MiscarriageLeaveCode");
            mOperatorLeaveCode = Sm.GetParameter("OperatorLeaveCode");
            mEmpSystemTypeHarian = Sm.GetParameter("EmpSystemTypeHarian");
            mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            mEmpSystemTypeAllIn = Sm.GetParameter("EmpSystemTypeAllIn");
            mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");
            mEmploymentStatusLepas = Sm.GetParameter("EmploymentStatusLepas");
            mEmploymentStatusSpesial = Sm.GetParameter("EmploymentStatusSpesial");
            mPayrunPeriodBulanan = Sm.GetParameter("PayrunPeriodBulanan");
            mPayrunPeriod2Mingguan = Sm.GetParameter("PayrunPeriod2Mingguan");
            mADCodeTransport = Sm.GetParameter("ADCodeTransport");
            mADCodeMeal = Sm.GetParameter("ADCodeMeal");
            mSSPCodeForHealth = Sm.GetParameter("SSPCodeForHealth");
            mSSPCodeForEmployment = Sm.GetParameter("SSPCodeForEmployment");
            mSSForRetiring = Sm.GetParameter("SSForRetiring");
            mSSOldAgeInsurance = Sm.GetParameter("SSOldAgeInsurance");
            mIsWorkingHrBasedOnSchedule = Sm.GetParameterBoo("IsWorkingHrBasedOnSchedule");
            mIsUseLateValidation = Sm.GetParameterBoo("IsUseLateValidation");
            mIsUseOTReplaceLeave = Sm.GetParameterBoo("IsUseOTReplaceLeave");
            mIsOTRoutineRounding = Sm.GetParameterBoo("IsOTRoutineRounding");
            mIsOTBasedOnOTRequest = Sm.GetParameterBoo("IsOTBasedOnOTRequest");
            mIsPayrollProcessUseSiteWorkingDayPerMth = Sm.GetParameterBoo("IsPayrollProcessUseSiteWorkingDayPerMth");
            mHoliday1stHrForHO = Sm.GetParameterDec("Holiday1stHrForHO");
            mHoliday1stHrForSite = Sm.GetParameterDec("Holiday1stHrForSite");
            mHoliday2ndHrForHO = Sm.GetParameterDec("Holiday2ndHrForHO");
            mHoliday2ndHrForSite = Sm.GetParameterDec("Holiday2ndHrForSite");
            mHoliday1stHrIndex = Sm.GetParameterDec("Holiday1stHrIndex");
            mHoliday2ndHrIndex = Sm.GetParameterDec("Holiday2ndHrIndex");
            mHoliday3rdHrIndex = Sm.GetParameterDec("Holiday3rdHrIndex");
            mNonNPWPTaxPercentage = Sm.GetParameterDec("NonNPWPTaxPercentage");
            mADCodeEmploymentPeriodYr = Sm.GetParameterDec("ADCodeEmploymentPeriodYr");
            mIsOTHolidayDeduct1Hr = Sm.GetParameterBoo("IsOTHolidayDeduct1Hr");
            mFieldAssignmentMinWorkDuration = Sm.GetParameterDec("FieldAssignmentMinWorkDuration");
            mSSCodePension = Sm.GetParameter("SSCodePension");
            mSSCodePension2 = Sm.GetParameter("SSCodePension2");
            mXMinToleranceLateMealTransportDeduction = Sm.GetParameterDec("XMinToleranceLateMealTransportDeduction");
            mXPercentageLateMealTransportDeduction = Sm.GetParameterDec("XPercentageLateMealTransportDeduction");
            mIsUseLateMealTransportDeduction = Sm.GetParameterBoo("IsUseLateMealTransportDeduction");
            mIsPPProcessAllOutstandingSS = Sm.GetParameterBoo("IsPPProcessAllOutstandingSS");
            mADOTHour = Sm.GetParameterDec("ADOTHour");
            mADOTMinHour = Sm.GetParameterDec("ADOTMinHour");
            mMinLeaveDaysForProratedAD= Sm.GetParameterDec("MinLeaveDaysForProratedAD");
            mProratedADBasedOnLeave = Sm.GetParameter("ProratedADBasedOnLeave");
            mLeaveForProratedAD = Sm.GetParameter("LeaveForProratedAD");
            mIsPayrunSSYrMthEnabled = Sm.GetParameterBoo("IsPayrunSSYrMthEnabled");
            mIsOTBaseOnOTFormula = Sm.GetParameterBoo("IsOTBaseOnOTFormula");
            mOTFormulaForNonHoliday5WorkingDays = Sm.GetParameter("OTFormulaForNonHoliday5WorkingDays");
            mOTFormulaForHoliday5WorkingDays = Sm.GetParameter("OTFormulaForHoliday5WorkingDays");
            mOTFormulaForNonHoliday6WorkingDays = Sm.GetParameter("OTFormulaForNonHoliday6WorkingDays");
            mOTFormulaForHoliday6WorkingDays = Sm.GetParameter("OTFormulaForHoliday6WorkingDays");
            mOTFormulaForNationalHoliday = Sm.GetParameter("OTFormulaForNationalHoliday");
            mWorkingDayPerWeek = Sm.GetParameterDec("WorkingDayPerWeek");
            mWorkingGroup5WorkingDays = Sm.GetParameter("WorkingGroup5WorkingDays");
            mWorkingGroup6WorkingDays = Sm.GetParameter("WorkingGroup6WorkingDays");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6-10
                        "Join Date",
                        "Resign Date",
                        "Employment" + Environment.NewLine + "Status",
                        "Type",
                        "Grade Level",
                        
                        //11-13
                        "Period",
                        "Group",
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 250, 80, 180, 200, 
                        
                        //6-10
                        100, 100, 130, 130, 150,

                        //11-13
                        130, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            mSystemType = string.Empty; 
            mPayrunPeriod = string.Empty;
            mPGCode = string.Empty;
            mSiteCode = string.Empty;
            mHOInd = true;

            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.Clear();

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDeptCode, TxtSystemType, TxtPayrunPeriod, TxtPGCode, TxtSiteCode,
                DteStartDt, DteEndDt 
            });
            ChkDelData.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        private void SetLuePayrunCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct PayrunCode As Col1, Concat(PayrunCode, ' : ', PayrunName) As Col2 ");
            SQL.AppendLine("From TblPayrun ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And IfNull(Status, 'O')='O' ");
            SQL.AppendLine("Order By Concat(PayrunCode, ' : ', PayrunName);");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Show Data

        private void ShowPayrunInfo()
        {
            if (Sm.IsLueEmpty(LuePayrunCode, "Payrun")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ShowPayrunInfo1();
                ShowPayrunInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPayrunInfo1()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DeptCode, B.DeptName, ");
            SQL.AppendLine("A.SystemType, C.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, D.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.PGCode, E.PGName, A.SiteCode, F.SiteName, F.HOInd, ");
            SQL.AppendLine("A.StartDt, A.EndDt, A.SSYr, A.SSMth ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.SystemType=C.OptCode And C.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption D On A.PayrunPeriod=D.OptCode And D.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr E On A.PGCode=E.PGCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode;");

            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DeptCode", 
                        
                        //1-5
                        "DeptName", 
                        "SystemType", 
                        "SystemTypeDesc", 
                        "PayrunPeriod", 
                        "PayrunPeriodDesc", 
                        
                        //6-10
                        "PGCode", 
                        "PGName",
                        "SiteCode",
                        "SiteName",
                        "HOInd",
                        
                        //11-14
                        "StartDt", 
                        "EndDt",
                        "SSYr",
                        "SSMth"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mDeptCode = Sm.DrStr(dr, c[0]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[1]);
                        mSystemType = Sm.DrStr(dr, c[2]);
                        TxtSystemType.EditValue = Sm.DrStr(dr, c[3]);
                        mPayrunPeriod = Sm.DrStr(dr, c[4]);
                        TxtPayrunPeriod.EditValue = Sm.DrStr(dr, c[5]);
                        mPGCode = Sm.DrStr(dr, c[6]);
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[7]);
                        mSiteCode = Sm.DrStr(dr, c[8]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[9]);
                        mHOInd = Sm.DrStr(dr, c[10]) == "Y";
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[11]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[12]));
                        mSSYr = Sm.DrStr(dr, c[13]);
                        mSSMth = Sm.DrStr(dr, c[14]);
                    }, true
                );
        }

        private void ShowPayrunInfo2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, ");
            SQL.AppendLine("E.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("G.GrdLvlName, ");
            SQL.AppendLine("H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("I.PGName, J.SiteName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            if (ChkEmploymentStatus.Checked)
                SQL.AppendLine("    And T2.EmploymentStatus=@EmploymentStatus ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, Max(X.StartDt) As StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblEmployee ");
            SQL.AppendLine("            Where EmpCode=X.EmpCode ");
            SQL.AppendLine("            And JoinDt<=@EndDt ");
            SQL.AppendLine("            And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@StartDt)) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And X.DeptCode=@DeptCode ");
            SQL.AppendLine("        And X.SystemType=@SystemType ");
            SQL.AppendLine("        And X.PayrunPeriod=@PayrunPeriod ");
            SQL.AppendLine("        And IfNull(X.PGCode, '')=IfNull(@PGCode, '') ");
            SQL.AppendLine("        And IfNull(X.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("        Group By X.EmpCode ");
            SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode And T1.StartDt=T3.StartDt ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On B.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On B.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SystemType", mSystemType);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", mPayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            if (ChkEmploymentStatus.Checked) Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt",  
                    
                    //6-10
                    "ResignDt", "EmploymentStatusDesc", "SystemTypeDesc", "GrdLvlName", "PayrunPeriodDesc", 
                    
                    //11-12
                    "PGName", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Check Data

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LuePayrunCode, "Payrun") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsPayrunNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "List of employees is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsPayrunNotValid()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblPayrun Where PayrunCode=@Param And IfNull(Status, 'O')='C';",
                    Sm.GetLue(LuePayrunCode),
                    "Payrun Code : " + Sm.GetLue(LuePayrunCode) + Environment.NewLine +
                    "Payrun Name : " + LuePayrunCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "This payrun already closed." + Environment.NewLine +
                    "You need to cancel it's voucher request."
                );
        }

        #endregion

        #region Save Data

        private MySqlCommand DeletePayrollProcess()
        {
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var cm = new MySqlCommand();

            if (!ChkDelData.Checked)
            {
                if (Grd1.Rows.Count >= 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                        if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                            No += 1;
                        }
                    }
                }
                Filter = " And (" + Filter + ") ";
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcess1 Where PayrunCode=@PayrunCode " + Filter + ";") ;
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcessAD Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblEmployeeOt Where PayrunCode=@PayrunCode " + Filter + ";");

            if (ChkDelData.Checked)
            {
                SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=Null ");
                SQL.AppendLine("Where PayrunCode Is Not Null And PayrunCode=@PayrunCode ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo From TblEmpInsPntHdr ");
                SQL.AppendLine("    Where CancelInd='N' And DocDt Between @StartDt And @EndDt ");
                SQL.AppendLine("    );");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            return cm;
        }

        private MySqlCommand SavePayrollProcess1(ref Result2 r)
        { 
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblPayrollProcess1 ");
            SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, NPWP, ");
            SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
            SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, ");
            SQL.AppendLine("IncEmployee, VarAllowance, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
            SQL.AppendLine("DedEmployee, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
            SQL.AppendLine("SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
            SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
            SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
            SQL.AppendLine("SalaryAdjustment, Tax, TaxAllowance, Amt, SSErDPLK, SSEeDPLK, SSErBNILife, SSEeBNILife, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PayrunCode, @EmpCode, @JoinDt, @ResignDt, @NPWP, ");
            SQL.AppendLine("@PTKP, @Salary, @WorkingDay, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
            SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, ");
            SQL.AppendLine("@OT1Amt, @OT2Amt, @OTHolidayAmt, @NonTaxableFixAllowance, @TaxableFixAllowance, @FixAllowance, ");
            SQL.AppendLine("@IncEmployee, @VarAllowance, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @SSEmployerPension2, @NonTaxableFixDeduction, @TaxableFixDeduction, @FixDeduction, ");
            SQL.AppendLine("@DedEmployee, @EmpAdvancePayment, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployeePension2, ");
            SQL.AppendLine("@SSErLifeInsurance, @SSEeLifeInsurance, @SSErWorkingAccident, @SSEeWorkingAccident, @SSErRetirement, @SSEeRetirement, @SSErPension, @SSEePension, ");
            SQL.AppendLine("@CreditCode1, @CreditCode2, @CreditCode3, @CreditCode4, @CreditCode5, @CreditCode6, @CreditCode7, @CreditCode8, @CreditCode9, @CreditCode10, ");
            SQL.AppendLine("@CreditAdvancePayment1, @CreditAdvancePayment2, @CreditAdvancePayment3, @CreditAdvancePayment4, @CreditAdvancePayment5, @CreditAdvancePayment6, @CreditAdvancePayment7, @CreditAdvancePayment8, @CreditAdvancePayment9, @CreditAdvancePayment10, ");
            SQL.AppendLine("@SalaryAdjustment, @Tax, @TaxAllowance, @Amt, @SSErDPLK, @SSEeDPLK, @SSErBNILife, @SSEeBNILife, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblPayrollProcessAD ");
            SQL.AppendLine("(PayrunCode, EmpCode, ADCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PayrunCode, @EmpCode, A.ADCode, @RecomputedValueForFixedAD*Sum(Amt) As Amt, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group BY A.ADCode;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "@NPWP", r.NPWP);
            Sm.CmParam<String>(ref cm, "@PTKP", r.PTKP);
	        Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
	        Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
	        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
	        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
	        Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
	        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
	        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
	        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
	        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
	        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
	        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
	        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
            Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance", r.NonTaxableFixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance", r.TaxableFixAllowance);
            Sm.CmParam<Decimal>(ref cm, "@FixAllowance", r.FixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", r.SSEmployerHealth);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", r.SSEmployerEmployment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", r.SSEmployerPension);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2", r.SSEmployerPension2);
            Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction", r.NonTaxableFixDeduction);
            Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction", r.TaxableFixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "@FixDeduction", r.FixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "@DedEmployee", r.DedEmployee);
            Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment", r.EmpAdvancePayment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", r.SSEmployeeHealth);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", r.SSEmployeeEmployment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", r.SSEmployeePension);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2", r.SSEmployeePension2);
            Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance", r.SSErLifeInsurance);
            Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance", r.SSEeLifeInsurance);
            Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident", r.SSErWorkingAccident);
            Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident", r.SSEeWorkingAccident);
            Sm.CmParam<Decimal>(ref cm, "@SSErRetirement", r.SSErRetirement);
            Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement", r.SSEeRetirement);
            Sm.CmParam<Decimal>(ref cm, "@SSErPension", r.SSErPension);
            Sm.CmParam<Decimal>(ref cm, "@SSEePension", r.SSEePension);
            Sm.CmParam<Decimal>(ref cm, "@SSErDPLK", r.SSErDPLK);
            Sm.CmParam<Decimal>(ref cm, "@SSEeDPLK", r.SSEeDPLK);
            Sm.CmParam<Decimal>(ref cm, "@SSErBNILife", r.SSErBNILife);
            Sm.CmParam<Decimal>(ref cm, "@SSEeBNILife", r.SSEeBNILife);
	        Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment", r.SalaryAdjustment);
            Sm.CmParam<String>(ref cm, "@CreditCode1", r.CreditCode1);
            Sm.CmParam<String>(ref cm, "@CreditCode2", r.CreditCode2);
            Sm.CmParam<String>(ref cm, "@CreditCode3", r.CreditCode3);
            Sm.CmParam<String>(ref cm, "@CreditCode4", r.CreditCode4);
            Sm.CmParam<String>(ref cm, "@CreditCode5", r.CreditCode5);
            Sm.CmParam<String>(ref cm, "@CreditCode6", r.CreditCode6);
            Sm.CmParam<String>(ref cm, "@CreditCode7", r.CreditCode7);
            Sm.CmParam<String>(ref cm, "@CreditCode8", r.CreditCode8);
            Sm.CmParam<String>(ref cm, "@CreditCode9", r.CreditCode9);
            Sm.CmParam<String>(ref cm, "@CreditCode10", r.CreditCode10);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1", r.CreditAdvancePayment1);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2", r.CreditAdvancePayment2);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3", r.CreditAdvancePayment3);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4", r.CreditAdvancePayment4);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5", r.CreditAdvancePayment5);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6", r.CreditAdvancePayment6);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7", r.CreditAdvancePayment7);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8", r.CreditAdvancePayment8);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9", r.CreditAdvancePayment9);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10", r.CreditAdvancePayment10);
	        Sm.CmParam<Decimal>(ref cm, "@Tax", r.Tax);
            Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", r.TaxAllowance);
            Sm.CmParam<Decimal>(ref cm, "@Amt", r.Amt);
            Sm.CmParam<Decimal>(ref cm, "@VarAllowance", r.VarAllowance);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForFixedAD", r._RecomputedValueForFixedAD);

            return cm;
        }

        private MySqlCommand SavePayrollProcess2(ref Result1 r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPayrollProcess2 ");
            SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
            SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
            SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
            SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
            SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
            SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
            SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
            SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
            SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
            SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
            SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
            SQL.AppendLine("DedProdLeave, CreateBy, CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("@PayrunCode, @EmpCode, @Dt, @ProcessInd, @LatestPaidDt, ");
            SQL.AppendLine("@JoinDt, @ResignDt, @PosCode, @DeptCode, @GrdLvlCode, @LevelCode, ");
            SQL.AppendLine("@SystemType, @EmploymentStatus, @PayrunPeriod, @PGCode, @SiteCode, @WorkingDay, ");
            SQL.AppendLine("@WSCode, @HolInd, @HolidayIndex, @WSHolidayInd, @WSIn1, ");
            SQL.AppendLine("@WSOut1, @WSIn2, @WSOut2, @WSIn3, @WSOut3, ");
            SQL.AppendLine("@OneDayInd, @LateInd, @ActualIn, @ActualOut, @WorkingIn, ");
            SQL.AppendLine("@WorkingOut, @WorkingDuration, @EmpSalary, @EmpSalary2, @BasicSalary, @BasicSalary2, @SalaryPension, @ProductionWages, ");
            SQL.AppendLine("@Salary, @LeaveCode, @LeaveType, @PaidLeaveInd, @LeaveStartTm,");
            SQL.AppendLine("@LeaveEndTm, @LeaveDuration, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
            SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, @OT1Amt, ");
            SQL.AppendLine("@OT2Amt, @OTHolidayAmt, @OTToLeaveInd, @IncMinWages, @IncProduction, @IncEmployee, ");
            SQL.AppendLine("@IncPerformance, @PresenceRewardInd, @HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @ServiceChargeIncentive, @DedProduction, ");
            SQL.AppendLine("@DedProdLeave, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
            Sm.CmParam<String>(ref cm, "@ProcessInd", r.ProcessInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@LatestPaidDt", r.LatestPaidDt);
            Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "@PosCode", r.PosCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", r.DeptCode);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", r.GrdLvlCode);
            Sm.CmParam<String>(ref cm, "@LevelCode", r.LevelCode);
            Sm.CmParam<String>(ref cm, "@SystemType", r.SystemType);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", r.EmploymentStatus);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", r.PayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", r.PGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", r.SiteCode);
            Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
            Sm.CmParam<String>(ref cm, "@WSCode", r.WSCode);
            Sm.CmParam<String>(ref cm, "@HolInd", r.HolInd?"Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@HolidayIndex", r.HolidayIndex);
            Sm.CmParam<String>(ref cm, "@WSHolidayInd", r.WSHolidayInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@WSIn1", r.WSIn1);
            Sm.CmParam<String>(ref cm, "@WSOut1", r.WSOut1);
            Sm.CmParam<String>(ref cm, "@WSIn2", r.WSIn2);
            Sm.CmParam<String>(ref cm, "@WSOut2", r.WSOut2);
            Sm.CmParam<String>(ref cm, "@WSIn3", r.WSIn3);
            Sm.CmParam<String>(ref cm, "@WSOut3", r.WSOut3);
            Sm.CmParam<String>(ref cm, "@OneDayInd", r.OneDayInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LateInd", r.LateInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActualIn", r.ActualIn);
            Sm.CmParam<String>(ref cm, "@ActualOut", r.ActualOut);
            Sm.CmParam<String>(ref cm, "@WorkingIn", r.WorkingIn);
            Sm.CmParam<String>(ref cm, "@WorkingOut", r.WorkingOut);
            Sm.CmParam<Decimal>(ref cm, "@WorkingDuration", r.WorkingDuration);
            Sm.CmParam<Decimal>(ref cm, "@EmpSalary", r.EmpSalary);
            Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", r.EmpSalary2);
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary", r.BasicSalary);
            Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", r.BasicSalary2);
            Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
            Sm.CmParam<Decimal>(ref cm, "@ProductionWages", r.ProductionWages);
            Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
            Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
            Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
            Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
            Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
            Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
            Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
            Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
            Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
            Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
            Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
            Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
            Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
            Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
            Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
            Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
            Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
            Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
            Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
            Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
            Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
            Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
            Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
            Sm.CmParam<Decimal>(ref cm, "Transport", r.Transport);
            Sm.CmParam<Decimal>(ref cm, "Meal", r.Meal);
            Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
            Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
            Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcess()
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            if (Filter.Length != 0)
                Filter = " And (" + Filter + ")";
            else
                Filter = " And 1=0 ";

           
            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='Y' And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            SQL.AppendLine("    And Yr=@SSYr ");
            SQL.AppendLine("    And Mth=@SSMth ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ((T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) Or T.PayrunCode Is Null) ");
            SQL.AppendLine("And T.Docno In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
            SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (PayrunCode Is Not Null And PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select DocNo From TblEmpInsPntHdr ");
            SQL.AppendLine("    Where CancelInd='N' And DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "@SSYr", mSSYr);
            Sm.CmParam<String>(ref cm, "@SSMth", mSSMth);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentProcess(ref AdvancePaymentProcess x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
            SQL.AppendLine("Where DocNo=@DocNo And Yr=@Yr And Mth=@Mth And EmpCode=@EmpCode; ");

            SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
            SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @Yr, @Mth, @EmpCode, @Amt, @PayrunCode, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "Yr", x.Yr);
            Sm.CmParam<String>(ref cm, "Mth", x.Mth);
            Sm.CmParam<String>(ref cm, "EmpCode", x.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();

            var cm = new MySqlCommand();
            string TaxAddQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='01' ");
            string TaxMinQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='02' ");
            string AmtTHPQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='03' ");

            SQL.AppendLine(TaxAddQuery);
            SQL2.AppendLine(TaxMinQuery);
            SQL3.AppendLine(AmtTHPQuery);

            string TaxAddAmt = Sm.GetValue(SQL.ToString(), r.PayrunCode, r.EmpCode, r.JoinDt);
            string TaxMinAmt = Sm.GetValue(SQL2.ToString(), r.PayrunCode, r.EmpCode, r.JoinDt);
            string THPAmt = Sm.GetValue(SQL3.ToString(), r.PayrunCode, r.EmpCode, r.JoinDt);

            ProcessTax(ref r, ref lNTI, ref  lTI, ref lTI2, decimal.Parse(THPAmt), decimal.Parse(TaxAddAmt), decimal.Parse(TaxMinAmt));

            var SQL4 = new StringBuilder();
            var cm4 = new MySqlCommand();

            SQL4.AppendLine("Update TblPayrollProcess1 ");
            SQL4.AppendLine("SET TaxAllowance = @TaxAllowance, Tax=@Tax, Amt=@Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime()  ");
            SQL4.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            cm4.CommandText = SQL4.ToString();
            Sm.CmParam<String>(ref cm4, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<Decimal>(ref cm4, "@TaxAllowance", r.TaxAllowance);
            Sm.CmParam<Decimal>(ref cm4, "@Tax", r.Tax);
            Sm.CmParam<Decimal>(ref cm4, "@Amt", r.Amt);
            Sm.CmParam<String>(ref cm4, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm4, "@CreateBy", Gv.CurrentUserCode);

            return cm4;
        }

        private MySqlCommand SaveEmployeeOT(ref EmpOT mEmpOT)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeOT ");
            SQL.AppendLine("(PayrunCode, EmpCode, OTDt, OTFormulaCode, Hr, Amt, HolidayInd, CreateBy, CreateDt)");
            SQL.AppendLine("Values ( ");
            SQL.AppendLine("@PayrunCode, @EmpCode, @OTDt, @OTFormulaCode, @Hr, ");
            SQL.AppendLine("@Amt, @HolidayInd, @CreateBy, CurrentDateTime() )");

            var cm5 = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm5, "@PayrunCode", mEmpOT.PayrunCode);
            Sm.CmParam<String>(ref cm5, "@EmpCode", mEmpOT.EmpCode);
            Sm.CmParam<String>(ref cm5, "@OTDt", mEmpOT.OTDt);
            Sm.CmParam<String>(ref cm5, "@OTFormulaCode", mEmpOT.OTFormulaCode);
            Sm.CmParam<Decimal>(ref cm5, "@Hr", mEmpOT.Hr);
            Sm.CmParam<Decimal>(ref cm5, "@Amt", mEmpOT.Amt);
            Sm.CmParam<String>(ref cm5, "@HolidayInd", mEmpOT.HolidayInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm5, "@CreateBy", Gv.CurrentUserCode);

            return cm5;
        }
        #endregion

        #region Process Data

        private void ProcessData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsProcessedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            #region Get Table Data

            var lEmployeePPS = new List<EmployeePPS>();
            var lOT = new List<OT>();
            var lNTI = new List<NTI>();
            var lTI2 = new List<TI2>();
            var lTI = new List<TI>();
            
            ProcessOT(ref lOT);
            ProcessNTI(ref lNTI);
            ProcessTI(ref lTI);
            ProcessTI2(ref lTI2);
            ProcessAdvancePaymentProcess(ref mlAdvancePaymentProcess);
            
            #endregion

            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);
            
            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r =>{ Process4(ref r); });
                lResult1.Sort(
                    delegate(Result1 r1a, Result1 r1b)
                    {
                        int f = r1a.EmpCode.CompareTo(r1b.EmpCode);
                        return f != 0 ? f : r1a.Dt.CompareTo(r1b.Dt);
                    });
            }

            var lResult2 = new List<Result2>();

            Process5(ref lResult2);

            lResult2.ForEach(r => { Process6(ref r, ref lResult1, ref lNTI, ref lTI, ref lTI2); });

            #region Save Data

            var cml = new List<MySqlCommand>();
            var cml2 = new List<MySqlCommand>();
            var cml3 = new List<MySqlCommand>();

            cml.Add(DeletePayrollProcess());

            lResult2.ForEach(r => { cml.Add(SavePayrollProcess1(ref r)); });
            lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });
            cml.Add(SavePayrollProcess());

            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.ForEach(x => { cml.Add(SaveAdvancePaymentProcess(ref x)); });

            Sm.ExecCommands(cml);

            lResult2.ForEach(r =>
            {
                cml2.Add(SavePayrollProcessTax(ref r, ref lNTI, ref lTI, ref lTI2));

            });

            Sm.ExecCommands(cml2);

            mEmpOT.ForEach(x =>
            {
                cml3.Add(SaveEmployeeOT(ref x));
            });
            Sm.ExecCommands(cml3);

            mEmpOT.Clear();

            #endregion

            LuePayrunCode.EditValue = null;
            ClearData();
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lDt = new List<string>();
            GetDt(ref lDt);

            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (Grd1.Rows.Count >= 1 && lDt.Count>0 && lEmployeePPS.Count>0)
            {
                var PayrunCodeTemp = Sm.GetLue(LuePayrunCode);
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var LevelCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;
                var HOIndTemp = string.Empty; 
                
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        foreach (string DtTemp in lDt)
                        {
                            DeptCodeTemp = string.Empty;
                            GrdLvlCodeTemp = string.Empty;
                            LevelCodeTemp = string.Empty;
                            EmploymentStatusTemp = string.Empty;
                            SystemTypeTemp = string.Empty;
                            PayrunPeriodTemp = string.Empty;
                            PGCodeTemp = string.Empty;
                            SiteCodeTemp = string.Empty;
                            PosCodeTemp = string.Empty;
                            HOIndTemp = string.Empty;

                            foreach (var i in 
                                lEmployeePPS
                                    .Where(Index => Index.EmpCode == EmpCodeTemp)
                                    .OrderByDescending(x=>x.StartDt)) 
                            {
                                if(Sm.CompareDtTm(DtTemp, i.StartDt)>=0)
                                {
                                    DeptCodeTemp = i.DeptCode;
                                    GrdLvlCodeTemp = i.GrdLvlCode;
                                    LevelCodeTemp = i.LevelCode;
                                    EmploymentStatusTemp = i.EmploymentStatus;
                                    SystemTypeTemp = i.SystemType;
                                    PayrunPeriodTemp = i.PayrunPeriod;
                                    PGCodeTemp = i.PGCode;
                                    SiteCodeTemp = i.SiteCode;
                                    PosCodeTemp = i.PosCode;
                                    HOIndTemp = i.HOInd;
                                    break;
                                };
                            }

                            lResult1.Add(new Result1()
                                {
                                    PayrunCode = PayrunCodeTemp,
                                    EmpCode = EmpCodeTemp,
                                    Dt = DtTemp,
                                    DeptCode=DeptCodeTemp,
                                    GrdLvlCode = GrdLvlCodeTemp,
                                    LevelCode = LevelCodeTemp,
                                    EmploymentStatus=EmploymentStatusTemp,
                                    SystemType=SystemTypeTemp,
                                    PayrunPeriod=PayrunPeriodTemp,
                                    PGCode = PGCodeTemp,
                                    SiteCode = SiteCodeTemp,
                                    PosCode=PosCodeTemp,
                                    _IsHOInd = HOIndTemp=="Y",
                                    ProcessInd = true,
                                    LatestPaidDt = string.Empty,
                                    JoinDt = string.Empty,
                                    ResignDt = string.Empty,
                                    WorkingDay = 0m,
                                    WSCode  = string.Empty,
                                    HolInd = false,
                                    HolidayIndex = mStdHolidayIndex,
                                    WSHolidayInd = false,
                                    WSIn1  = string.Empty,
                                    WSOut1  = string.Empty,
                                    WSIn2  = string.Empty,
                                    WSOut2  = string.Empty,
                                    WSIn3  = string.Empty,
                                    WSOut3  = string.Empty,
                                    OneDayInd = true,
                                    LateInd = false,
                                    ActualIn  = string.Empty,
                                    ActualOut  = string.Empty,
                                    WorkingIn  = string.Empty,
                                    WorkingOut  = string.Empty,
                                    WorkingDuration = 0m,
                                    BasicSalary = 0m,
                                    BasicSalary2 = 0m,
                                    _LatestMonthlyGrdLvlSalary = 0m,
                                    _LatestDailyGrdLvlSalary = 0m,
                                    ProductionWages = 0m,
                                    Salary = 0m,
                                    LeaveCode  = string.Empty,
                                    LeaveType = string.Empty,
                                    PaidLeaveInd = false,
                                    CompulsoryLeaveInd = false,
                                    _DeductTHPInd = false,
                                    LeaveStartTm  = string.Empty,
                                    LeaveEndTm  = string.Empty,
                                    LeaveDuration = 0m,
                                    PLDay = 0m,
                                    PLHr = 0m,
                                    PLAmt = 0m,
                                    ProcessPLAmt = 0m,
                                    OT1Hr = 0m,
                                    OT2Hr = 0m,
                                    OTHolidayHr = 0m,
                                    OT1Amt = 0m,
                                    OT2Amt = 0m,
                                    OTHolidayAmt = 0m,
                                    OTToLeaveInd = false,
                                    IncMinWages = 0m,
                                    IncProduction = 0m,
                                    IncPerformance = 0m,
                                    PresenceRewardInd = false,
                                    HolidayEarning = 0m,
                                    ExtraFooding = 0m,
                                    FieldAssignment = 0m,
                                    DedProduction = 0m,
                                    DedProdLeave = 0m,
                                    EmpSalary  = 0m,
                                    EmpSalary2 = 0m,
                                    SalaryPension = 0m,
                                    UPLDay  = 0m,
                                    UPLHr  = 0m,
                                    UPLAmt = 0m,
                                    ProcessUPLAmt = 0m,
                                    Transport = 0m,
                                    Meal = 0m,
                                    ServiceChargeIncentive = 0m,
                                    IncEmployee = 0m,
                                    ADOT = 0m,
                                    _ExtraFooding = 0m,
                                    _FieldAssignment = 0m,
                                    _IncPerformance = 0m,
                                    _IsFullDayInd = false,
                                    _IsUseLatestGrdLvlSalary = false
                                });
                        }
                    }
                }
                lDt.Clear();
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                var WSCodesForHoliday = mWSCodesForHoliday.Split('#');
                var WSCodesForNationalHoliday = mWSCodesForNationalHoliday.Split('#');

               foreach (var ews in lEmpWorkSchedule)
               {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ews.EmpCode && Index.Dt == ews.Dt))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd=="Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                    }
               }
               lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Employee's Paid Date

            var lEmpPaidDt = new List<EmpPaidDt>();
            ProcessEmpPaidDt(ref lEmpPaidDt);
            if (lEmpPaidDt.Count > 0)
            {
                lResult1.ForEach(A =>
                {
                    foreach (var x in lEmpPaidDt.Where(x => x.EmpCode == A.EmpCode && x.Dt == A.Dt))
                    {
                        A.LatestPaidDt = x.Dt;
                        break;
                    }
                });

                //lResult1.ForEach(A =>
                //{
                    
                //    A.LatestPaidDt = lEmpPaidDt.FirstOrDefault(
                //        x => A.EmpCode.Equals(x.EmpCode??"") && A.Dt.Equals(x.Dt??"")
                //        ).Dt??"";
                //});
                
               lEmpPaidDt.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            var lLeaveDtl = new List<LeaveDtl>();
            ProcessLeaveDtl(ref lLeaveDtl);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd=="Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        foreach (var ld in lLeaveDtl.Where(
                            Index => 
                                Index.LeaveCode == l.LeaveCode && 
                                Index.SystemType == r.SystemType &&
                                Index.PayrunPeriod == r.PayrunPeriod
                                ))
                        {
                            r._DeductTHPInd = ld.DeductTHPInd;
                        }
                        break;
                    }
                }
                lLeave.Clear();
                lLeaveDtl.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == e.EmpCode))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;
                        r.WGCode = e.WorkGroupCode;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0) r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0) r.ProcessInd = false;
                        }

                        if (r.LatestPaidDt.Length > 0) r.ProcessInd = false;
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalarySS = new List<EmployeeSalarySS>();
            ProcessEmployeeSalarySS(ref lEmployeeSalarySS);
            if (lEmployeeSalarySS.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var i in lEmployeeSalarySS
                        .Where(w => Sm.CompareStr(w.EmpCode, r.EmpCode))
                        .OrderByDescending(o => o.StartDt))
                    {
                        if (Sm.CompareDtTm(i.StartDt, r.Dt) <= 0)
                        {
                            r.SalaryPension = i.Amt;
                            break;
                        }
                    }
                }
                lEmployeeSalarySS.Clear();
            }

            #endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            #region OTFormula

            mlOTFormula.Clear();
            ProcessOTFormula(ref mlOTFormula, mTxtOTFormulaCode);

            #endregion

            #region OT Adjustment

            mlOTAdjustment.Clear();
            ProcessOTAdjustment(ref mlOTAdjustment);

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            var EmpCodeTemp = string.Empty;
            var PayrunPeriodTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                string 
                    SystemTypeTemp = string.Empty, 
                    GrdLvlCodeTemp = string.Empty,
                    LevelCodeTemp = string.Empty;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        SystemTypeTemp = string.Empty;
                        PayrunPeriodTemp = string.Empty;
                        GrdLvlCodeTemp = string.Empty;
                        LevelCodeTemp = string.Empty;

                        foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderBy(x => x.Dt))
                        {
                            x.Salary = x.EmpSalary2; // Berdasarkan Employee Salary
                            
                            if (x.ProcessInd)
                            {
                                if (!(
                                    Sm.CompareStr(x.DeptCode, mDeptCode) &&
                                    Sm.CompareStr(x.SystemType, mSystemType) &&
                                    Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                    Sm.CompareStr(x.PGCode, mPGCode) &&
                                    Sm.CompareStr(x.SiteCode, mSiteCode)
                                    ))
                                    x.ProcessInd = false;
                                
                            }
                        }
                    }
                }
            }
        }

        private void Process4(ref Result1 r
            )
        {
            string
                ActualDtIn = string.Empty, 
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty, 
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode, 
                Dt = r.Dt;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 && 
                r.WSOut1.Length > 0 && 
                Sm.CompareDtTm(r.WSIn1, r.WSOut1)>0)
                //int.Parse(r.WSIn1) > int.Parse(r.WSOut1))
                r.OneDayInd = false;

            #endregion

            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set working duration

            decimal 
                WorkingDuration = 0m, 
                BreakDuration = 0m;

            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType.Length>0)
                {
                    WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn);
                }
                else
                {
                    //No Leave
                    WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn);
                    if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                    {
                        // Menghitung lama waktu istirahat
                        if (r.OneDayInd)
                        {
                            // Shift 1, 2
                            if (Sm.CompareDtTm(r.WorkingOut, r.Dt+r.WSIn2) > 0)
                                BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.WSIn2, r.WSOut2)<=0)
                            //if (decimal.Parse(r.WSIn2) <= decimal.Parse(r.WSOut2))
                                BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            else
                                BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                        }
                    }
                    WorkingDuration -= BreakDuration;  
                }
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }
            
            #endregion

            #region Set leave information (PLDay, PLHr, PLAmt)

            if (r.PaidLeaveInd && r.LeaveType.Length>0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.PLDay = 1;
                }
                else
                    r.PLHr = r.LeaveDuration;

                if (
                        r.LeaveDuration != 0m &&
                        mWorkingHr != 0m &&
                        !(r.SystemType == mEmpSystemTypeBorongan && r.LeaveCode == mOperatorLeaveCode)
                    )
                {
                    //if (r.SystemType == mEmpSystemTypeBorongan)
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.PLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.PLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r.PLAmt > 0)
                    r.ProcessPLAmt = r.PLAmt;
            }

            if (!r.PaidLeaveInd && r.LeaveType.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.UPLDay = 1;
                }
                else
                    r.UPLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m) 
                {
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            if (r.LeaveCode == mCoordinatorLeaveCode && r.LeaveType != "F")
                                r.UPLAmt = r.ProductionWages * (r.LeaveDuration / mWorkingHr);
                            else
                                r.UPLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.UPLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.UPLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.UPLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r._DeductTHPInd)
                    r.ProcessUPLAmt = r.UPLAmt;
            }

            #endregion

            decimal TotalOTHr = 0m;

            if (r.SystemType == "1")
            {
                #region Menghitung OT menggunakan OT request


                decimal OTHr = 0m;
                string
                    StartTm = string.Empty,
                    EndTm = string.Empty;

                foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                {
                    OTHr = 0m;
                    StartTm = x.Tm1;
                    EndTm = x.Tm2;

                    if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                    {
                        if (EndTm == "0000")
                            OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                        else
                        {
                            if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                OTHr = ComputeDuration(Dt + EndTm, Dt + StartTm);
                            else
                                OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                        }
                    }
                    if (OTHr < 0) OTHr = 0;
                    TotalOTHr += OTHr;
                }

                #endregion
            }

            if (r.SystemType == "1")
            {
                #region Menghitung OT Amount

                if (r.EmpSalary != 0m && TotalOTHr >= 0.5m)
                {
                    if (mIsOTBaseOnOTFormula)
                    {
                        decimal TotalOTHrTemp = TotalOTHr;
                        if (r.HolInd || r.WSHolidayInd)
                         {
                            r.OTHolidayHr = TotalOTHrTemp;

                            //string WSCode = r.WSCode;
                            //string[] lWSCodeNationalHoliday = mWSCodesForHoliday.Split('#');
                            //var WSCodeNationalHoliday = Array.FindAll(lWSCodeNationalHoliday, x => x.Equals(WSCode));
                            
                            string NationalHoliday = Sm.GetValue("Select * From TblHoliday Where HolDt = @Param", r.Dt);
                            if (NationalHoliday.Length > 0)
                            {
                                mlOTFormula.Clear();
                                ProcessOTFormula(ref mlOTFormula, mOTFormulaForNationalHoliday);
                                //while (TotalOTHrTemp > 0)
                                //{
                                var FirstHr = mlOTFormula.Min(hr => hr.Hr);
                                List<decimal> HrTemp = new List<decimal>();
                                List<decimal> One = new List<decimal>();
                                HrTemp.Add(FirstHr);

                                for (int index = 1; index < mlOTFormula.Count(); index++)
                                {
                                    if ((index + 1) == mlOTFormula.Count())
                                    {
                                        HrTemp.Add(TotalOTHrTemp - FirstHr - One.Count());
                                    }
                                    else
                                    {
                                        One.Add(1);
                                        HrTemp.Add(1);

                                    }
                                }

                                decimal[] ArrHrTemp = HrTemp.ToArray();
                                decimal OTAmtTemp = 0m;
                                string OTFormulaCodeTemp = null;
                                decimal MaxHr = mlOTFormula.Max(hr => hr.Hr);

                                if (TotalOTHrTemp < FirstHr)
                                {
                                    OTAmtTemp = TotalOTHrTemp * mlOTFormula.Min(index => index.Index) * (r.EmpSalary / mlOTFormula.Min(hr => hr.HrPerMth));
                                    mEmpOT.Add(new EmpOT()
                                    {
                                        PayrunCode = r.PayrunCode,
                                        EmpCode = r.EmpCode,
                                        OTDt = r.Dt,
                                        OTFormulaCode = mlOTFormula.Min(code => code.OTFormulaCode),
                                        Hr = TotalOTHrTemp,
                                        Amt = Math.Round(OTAmtTemp, 2),
                                        HolidayInd = true,
                                    });
                                }
                                else
                                {
                                    int iterasi = 0;
                                    foreach (var x in mlOTFormula.OrderBy(x => x.Sequence))
                                    {
                                        int i = 1;
                                        while (TotalOTHrTemp > 0)
                                        {
                                            if (i == x.Hr && i != MaxHr && TotalOTHrTemp > 0)
                                            {
                                                OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                                TotalOTHrTemp -= ArrHrTemp[iterasi];
                                                OTFormulaCodeTemp = x.OTFormulaCode;
                                                break;
                                            }

                                            if (i == x.Hr && i == MaxHr && TotalOTHrTemp > 0)
                                            {
                                                OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                                TotalOTHrTemp = 0m;
                                                OTFormulaCodeTemp = x.OTFormulaCode;
                                                break;
                                            }
                                            i++;
                                        }

                                        mEmpOT.Add(new EmpOT()
                                        {
                                            PayrunCode = r.PayrunCode,
                                            EmpCode = r.EmpCode,
                                            OTDt = r.Dt,
                                            OTFormulaCode = OTFormulaCodeTemp,
                                            Hr = ArrHrTemp[iterasi],
                                            Amt = Math.Round(OTAmtTemp, 2),
                                            HolidayInd = true,
                                        });
                                        iterasi++;
                                    }
                                }
                                //    i++;
                                //}
                            }
                            else
                            {
                                if (r.WGCode == mWorkingGroup5WorkingDays)
                                {
                                    mlOTFormula.Clear();
                                    ProcessOTFormula(ref mlOTFormula, mOTFormulaForHoliday5WorkingDays);

                                    var FirstHr = mlOTFormula.Min(hr => hr.Hr);
                                    List<decimal> HrTemp = new List<decimal>();
                                    List<decimal> One = new List<decimal>();
                                    HrTemp.Add(FirstHr);

                                    for (int index = 1; index < mlOTFormula.Count(); index++)
                                    {
                                        if ((index + 1) == mlOTFormula.Count())
                                        {
                                            HrTemp.Add(TotalOTHrTemp - FirstHr - One.Count());
                                        }
                                        else
                                        {
                                            One.Add(1);
                                            HrTemp.Add(1);

                                        }
                                    }

                                    decimal[] ArrHrTemp = HrTemp.ToArray();
                                    decimal OTAmtTemp = 0m;
                                    string OTFormulaCodeTemp = null;
                                    decimal MaxHr = mlOTFormula.Max(hr => hr.Hr);

                                    if (TotalOTHrTemp < FirstHr)
                                    {
                                        OTAmtTemp = TotalOTHrTemp * mlOTFormula.Min(index => index.Index) * (r.EmpSalary / mlOTFormula.Min(hr => hr.HrPerMth));
                                        mEmpOT.Add(new EmpOT()
                                        {
                                            PayrunCode = r.PayrunCode,
                                            EmpCode = r.EmpCode,
                                            OTDt = r.Dt,
                                            OTFormulaCode = mlOTFormula.Min(code => code.OTFormulaCode),
                                            Hr = TotalOTHrTemp,
                                            Amt = Math.Round(OTAmtTemp, 2),
                                            HolidayInd = true,
                                        });
                                    }
                                    else
                                    {
                                        int iterasi = 0;
                                        foreach (var x in mlOTFormula.OrderBy(x => x.Sequence))
                                        {
                                            int i = 1;
                                            while (TotalOTHrTemp > 0)
                                            {
                                                if (i == x.Hr && i != MaxHr && TotalOTHrTemp > 0)
                                                {
                                                    OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                                    TotalOTHrTemp -= ArrHrTemp[iterasi];
                                                    OTFormulaCodeTemp = x.OTFormulaCode;
                                                    break;
                                                }

                                                if (i == x.Hr && i == MaxHr && TotalOTHrTemp > 0)
                                                {
                                                    OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                                    TotalOTHrTemp = 0m;
                                                    OTFormulaCodeTemp = x.OTFormulaCode;
                                                    break;
                                                }
                                                i++;
                                            }

                                            mEmpOT.Add(new EmpOT()
                                            {
                                                PayrunCode = r.PayrunCode,
                                                EmpCode = r.EmpCode,
                                                OTDt = r.Dt,
                                                OTFormulaCode = OTFormulaCodeTemp,
                                                Hr = ArrHrTemp[iterasi],
                                                Amt = Math.Round(OTAmtTemp, 2),
                                                HolidayInd = true,
                                            });
                                            iterasi++;
                                        }
                                    }
                                }
                                else if (r.WGCode == mWorkingGroup6WorkingDays)
                                {
                                    mlOTFormula.Clear();
                                    ProcessOTFormula(ref mlOTFormula, mOTFormulaForHoliday6WorkingDays);

                                    var FirstHr = mlOTFormula.Min(hr => hr.Hr);
                                    List<decimal> HrTemp = new List<decimal>();
                                    List<decimal> One = new List<decimal>();
                                    HrTemp.Add(FirstHr);

                                    for (int index = 1; index < mlOTFormula.Count(); index++)
                                    {
                                        if ((index + 1) == mlOTFormula.Count())
                                        {
                                            HrTemp.Add(TotalOTHrTemp - FirstHr - One.Count());
                                        }
                                        else
                                        {
                                            One.Add(1);
                                            HrTemp.Add(1);

                                        }
                                    }

                                    decimal[] ArrHrTemp = HrTemp.ToArray();
                                    decimal OTAmtTemp = 0m;
                                    string OTFormulaCodeTemp = null;
                                    decimal MaxHr = mlOTFormula.Max(hr => hr.Hr);

                                    if (TotalOTHrTemp < FirstHr)
                                    {
                                        OTAmtTemp = TotalOTHrTemp * mlOTFormula.Min(index => index.Index) * (r.EmpSalary / mlOTFormula.Min(hr => hr.HrPerMth));
                                        mEmpOT.Add(new EmpOT()
                                        {
                                            PayrunCode = r.PayrunCode,
                                            EmpCode = r.EmpCode,
                                            OTDt = r.Dt,
                                            OTFormulaCode = mlOTFormula.Min(code => code.OTFormulaCode),
                                            Hr = TotalOTHrTemp,
                                            Amt = Math.Round(OTAmtTemp, 2),
                                            HolidayInd = true,
                                        });
                                    }
                                    else
                                    {
                                        int iterasi = 0;
                                        foreach (var x in mlOTFormula.OrderBy(x => x.Sequence))
                                        {
                                            int i = 1;
                                            while (TotalOTHrTemp > 0)
                                            {
                                                if (i == x.Hr && i != MaxHr && TotalOTHrTemp > 0)
                                                {
                                                    OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                                    TotalOTHrTemp -= ArrHrTemp[iterasi];
                                                    OTFormulaCodeTemp = x.OTFormulaCode;
                                                    break;
                                                }

                                                if (i == x.Hr && i == MaxHr && TotalOTHrTemp > 0)
                                                {
                                                    OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                                    TotalOTHrTemp = 0m;
                                                    OTFormulaCodeTemp = x.OTFormulaCode;
                                                    break;
                                                }
                                                i++;
                                            }

                                            mEmpOT.Add(new EmpOT()
                                            {
                                                PayrunCode = r.PayrunCode,
                                                EmpCode = r.EmpCode,
                                                OTDt = r.Dt,
                                                OTFormulaCode = OTFormulaCodeTemp,
                                                Hr = ArrHrTemp[iterasi],
                                                Amt = Math.Round(OTAmtTemp, 2),
                                                HolidayInd = true,
                                            });
                                            iterasi++;
                                        }
                                    }
                                }
                                
                            }
                            NationalHoliday = string.Empty;
                        }
                        else
                        {
                            if (r.WGCode == mWorkingGroup5WorkingDays)
                            {
                                mlOTFormula.Clear();
                                ProcessOTFormula(ref mlOTFormula, mOTFormulaForNonHoliday5WorkingDays);

                                var FirstHr = mlOTFormula.Min(hr => hr.Hr);
                                List<decimal> HrTemp = new List<decimal>();
                                List<decimal> One = new List<decimal>();
                                HrTemp.Add(FirstHr);

                                for (int index = 1; index < mlOTFormula.Count(); index++)
                                {
                                    if ((index + 1) == mlOTFormula.Count())
                                    {
                                        HrTemp.Add(TotalOTHrTemp - FirstHr - One.Count());
                                    }
                                    else
                                    {
                                        One.Add(1);
                                        HrTemp.Add(1);

                                    }
                                }

                                decimal[] ArrHrTemp = HrTemp.ToArray();
                                decimal OTAmtTemp = 0m;
                                string OTFormulaCodeTemp = null;
                                decimal MaxHr = mlOTFormula.Max(hr => hr.Hr);

                                int iterasi = 0;
                                foreach (var x in mlOTFormula.OrderBy(x => x.Sequence))
                                {
                                    int i = 1;
                                    while (TotalOTHrTemp > 0)
                                    {
                                        if (i == x.Hr && i != MaxHr && TotalOTHrTemp > 0)
                                        {
                                            OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                            TotalOTHrTemp -= ArrHrTemp[iterasi];
                                            OTFormulaCodeTemp = x.OTFormulaCode;
                                            break;
                                        }

                                        if (i == x.Hr && i == MaxHr && TotalOTHrTemp > 0)
                                        {
                                            OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                            TotalOTHrTemp = 0m;
                                            OTFormulaCodeTemp = x.OTFormulaCode;
                                            break;
                                        }
                                        i++;
                                    }

                                    mEmpOT.Add(new EmpOT()
                                    {
                                        PayrunCode = r.PayrunCode,
                                        EmpCode = r.EmpCode,
                                        OTDt = r.Dt,
                                        OTFormulaCode = OTFormulaCodeTemp,
                                        Hr = ArrHrTemp[iterasi],
                                        Amt = Math.Round(OTAmtTemp, 2),
                                        HolidayInd = false,
                                    });
                                    iterasi++;
                                }
                            }
                            else if (r.WGCode == mWorkingGroup6WorkingDays)
                            {
                                mlOTFormula.Clear();
                                ProcessOTFormula(ref mlOTFormula, mOTFormulaForNonHoliday6WorkingDays);

                                var FirstHr = mlOTFormula.Min(hr => hr.Hr);
                                List<decimal> HrTemp = new List<decimal>();
                                List<decimal> One = new List<decimal>();
                                HrTemp.Add(FirstHr);

                                for (int index = 1; index < mlOTFormula.Count(); index++)
                                {
                                    if ((index + 1) == mlOTFormula.Count())
                                    {
                                        HrTemp.Add(TotalOTHrTemp - FirstHr - One.Count());
                                    }
                                    else
                                    {
                                        One.Add(1);
                                        HrTemp.Add(1);

                                    }
                                }

                                decimal[] ArrHrTemp = HrTemp.ToArray();
                                decimal OTAmtTemp = 0m;
                                string OTFormulaCodeTemp = null;
                                decimal MaxHr = mlOTFormula.Max(hr => hr.Hr);

                                int iterasi = 0;
                                foreach (var x in mlOTFormula.OrderBy(x => x.Sequence))
                                {
                                    int i = 1;
                                    while (TotalOTHrTemp > 0)
                                    {
                                        if (i == x.Hr && i != MaxHr && TotalOTHrTemp > 0)
                                        {
                                            OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                            TotalOTHrTemp -= ArrHrTemp[iterasi];
                                            OTFormulaCodeTemp = x.OTFormulaCode;
                                            break;
                                        }

                                        if (i == x.Hr && i == MaxHr && TotalOTHrTemp > 0)
                                        {
                                            OTAmtTemp = ArrHrTemp[iterasi] * x.Index * (r.EmpSalary / x.HrPerMth);
                                            TotalOTHrTemp = 0m;
                                            OTFormulaCodeTemp = x.OTFormulaCode;
                                            break;
                                        }
                                        i++;
                                    }

                                    mEmpOT.Add(new EmpOT()
                                    {
                                        PayrunCode = r.PayrunCode,
                                        EmpCode = r.EmpCode,
                                        OTDt = r.Dt,
                                        OTFormulaCode = OTFormulaCodeTemp,
                                        Hr = ArrHrTemp[iterasi],
                                        Amt = Math.Round(OTAmtTemp, 2),
                                        HolidayInd = false,
                                    });
                                    iterasi++;
                                }
                            }
                        }
                    }
                    else
                    {
                        decimal TotalOTHrTemp = TotalOTHr;
                        if (r.HolInd || r.WSHolidayInd)
                        {
                            r.OTHolidayHr = TotalOTHrTemp;

                            int i = 1;

                            while (TotalOTHrTemp > 0)
                            {
                                if (i == 1)
                                {
                                    if (TotalOTHrTemp >= mHoliday1stHrForHO)
                                    {
                                        r.OTHolidayAmt = mHoliday1stHrForHO * mHoliday1stHrIndex * (r.EmpSalary / 173);
                                        TotalOTHrTemp -= mHoliday1stHrForHO;
                                    }
                                    else
                                    {
                                        r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * (r.EmpSalary / 173);
                                        TotalOTHrTemp = 0m;
                                    }
                                }
                                if (i == 2)
                                {
                                    if (TotalOTHrTemp >= mHoliday2ndHrForHO)
                                    {
                                        r.OTHolidayAmt += (mHoliday2ndHrForHO * mHoliday2ndHrIndex * (r.EmpSalary / 173));
                                        TotalOTHrTemp -= mHoliday2ndHrForHO;
                                    }
                                    else
                                    {
                                        r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * (r.EmpSalary / 173));
                                        TotalOTHrTemp = 0m;
                                    }
                                }
                                if (i == 3)
                                {
                                    r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * (r.EmpSalary / 173));
                                    TotalOTHrTemp = 0m;
                                }
                                i++;
                            }
                        }
                        else
                        {
                            if (TotalOTHrTemp <= mOTFormulaHr1)
                            {
                                r.OT1Hr = TotalOTHrTemp;
                                r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * (r.EmpSalary / 173);
                                TotalOTHrTemp = 0m;
                            }
                            else
                            {
                                r.OT1Hr = mOTFormulaHr1;
                                r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * (r.EmpSalary / 173);
                                TotalOTHrTemp -= mOTFormulaHr1;
                            }

                            if (TotalOTHrTemp > 0m && TotalOTHrTemp <= mOTFormulaHr2)
                            {
                                r.OT2Hr = TotalOTHrTemp;
                                r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * (r.EmpSalary / 173);
                                TotalOTHrTemp = 0m;
                            }
                        }
                    }
                }

                #endregion
            }

            #region Unprocessed data

            if (!r.ProcessInd)
            {
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                r.LeaveCode = string.Empty;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr = 0m;
                r.OT2Hr = 0m;
                r.OTHolidayHr = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt = 0m;
                r.OTHolidayAmt = 0m;
                r.IncProduction = 0m;
                r.DedProduction = 0m;
                r.DedProdLeave = 0m;
                r.IncEmployee = 0m;
            }

            #endregion
        }


        private void Process5(ref List<Result2> r2)
        {
            string
                PayrunCode = Sm.GetLue(LuePayrunCode),
                Filter = string.Empty,
                EmpCode = string.Empty;

            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length>0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " (" + Filter + ") ";
            else
                Filter = " (0=1)";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, A.NPWP, A.PTKP, ");
            SQL.AppendLine("IfNull(B.SalaryAdjustment, 0) As SalaryAdjustment, ");
            SQL.AppendLine("IfNull(C.SSEmployerHealth, 0) As SSEmployerHealth, ");
            SQL.AppendLine("IfNull(C.SSEmployeeHealth, 0) As SSEmployeeHealth, ");
            SQL.AppendLine("IfNull(D.SSEmployerEmployment, 0) As SSEmployerEmployment, ");
            SQL.AppendLine("IfNull(D.SSEmployeeEmployment, 0) As SSEmployeeEmployment, ");
            SQL.AppendLine("IfNull(E.SSErLifeInsurance, 0.00) As SSErLifeInsurance, ");
            SQL.AppendLine("IfNull(E.SSEeLifeInsurance, 0.00) As SSEeLifeInsurance, ");
            SQL.AppendLine("IfNull(F.SSErWorkingAccident, 0.00) As SSErWorkingAccident, ");
            SQL.AppendLine("IfNull(F.SSEeWorkingAccident, 0.00) As SSEeWorkingAccident, ");
            SQL.AppendLine("IfNull(G.SSErRetirement, 0.00) As SSErRetirement, ");
            SQL.AppendLine("IfNull(G.SSEeRetirement, 0.00) As SSEeRetirement, ");
            SQL.AppendLine("IfNull(H.Amt, 0.00) As FixAllowance, ");
            SQL.AppendLine("IfNull(I.Amt, 0.00) As FixDeduction, ");
            SQL.AppendLine("IfNull(J.TaxableFixedAllowance, 0.00) As TaxableFixedAllowance, ");
            SQL.AppendLine("IfNull(K.TaxableFixedDeduction, 0.00) As TaxableFixedDeduction, ");
            SQL.AppendLine("IfNull(L.IncEmployee, 0.00) As IncEmployee, ");
            SQL.AppendLine("IfNull(M.Amt, 0.00) As VarAllowance, ");
            SQL.AppendLine("IfNull(N.SSErPension, 0.00) As SSErPension, ");
            SQL.AppendLine("IfNull(N.SSEePension, 0.00) As SSEePension, ");
            SQL.AppendLine("IfNull(O.SSErDPLK, 0.00) As SSErDPLK, ");
            SQL.AppendLine("IfNull(O.SSEeDPLK, 0.00) As SSEeDPLK, ");
            SQL.AppendLine("IfNull(P.SSErBNILife, 0.00) As SSErBNILife, ");
            SQL.AppendLine("IfNull(P.SSEeBNILife, 0.00) As SSEeBNILife ");
            SQL.AppendLine("From TblEmployee A ");

            //Salary Adjustment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As SalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr T1 ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And (T1.PayrunCode Is Null Or (T1.PayrunCode Is Not Null And T1.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T1.") + ") ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustment2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblSalaryAdjustment2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("            And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");

            //SS Health
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='SSCodeForHealth' And ParValue Is Not Null ");
            SQL.AppendLine("    )) ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");

            //SS Employment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='SSCodeForEmployment' And ParValue Is Not Null ");
            SQL.AppendLine("    )) ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");

            //BPJS Jaminan Kematian
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErLifeInsurance, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeLifeInsurance ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='SSLifeInsurance' And ParValue Is Not Null ");
            SQL.AppendLine("        )) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");

            //BPJS Jaminan Kecelakaan Kerja
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErWorkingAccident, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeWorkingAccident ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='SSWorkingAccident' And ParValue Is Not Null ");
            SQL.AppendLine("        )) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");

            //BPJS Jaminan Hari Tua
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErRetirement, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeRetirement ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='SSOldAgeInsurance' And ParValue Is Not Null ))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            //Fixed Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") H On A.EmpCode=H.EmpCode ");

            //Fixed Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");

            //Taxable Fixed Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");

            //Taxable Fixed Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedDeduction ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.TaxInd='Y' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") K On A.EmpCode=K.EmpCode ");

            //Incentive/Penalty
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.AmtInspnt) As IncEmployee ");
            SQL.AppendLine("    From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpInsPntDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("    Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='01' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") L On A.EmpCode=L.EmpCode ");

            //Variable Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.AmtType='2' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") M On A.EmpCode=M.EmpCode ");

            //Social Security Pension
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErPension, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='SSCodeForPension' And ParValue Is Not Null ))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") N On A.EmpCode=N.EmpCode ");

            //Social Security DPLK
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErDPLK, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeDPLK ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='SSCodeForDPLK' And ParValue Is Not Null ))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") O On A.EmpCode=O.EmpCode ");

            //Social Security BNI Life
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErBNILife, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeBNILife ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And FIND_IN_SET(T2.SSCode, ( ");
            SQL.AppendLine("        Select ParValue From TblParameter ");
            SQL.AppendLine("        Where ParCode='SSCodeForBNILife' And ParValue Is Not Null ))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") P On A.EmpCode=P.EmpCode ");
            SQL.AppendLine("Where " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                Sm.CmParam<String>(ref cm, "@SSYr", mSSYr);
                Sm.CmParam<String>(ref cm, "@SSMth", mSSMth);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "JoinDt", 
                    "ResignDt", 
                    "NPWP", 
                    "PTKP", 
                    "SalaryAdjustment", 

                    //6-10
                    "SSEmployerHealth", 
                    "SSEmployeeHealth", 
                    "SSEmployerEmployment", 
                    "SSEmployeeEmployment",
                    "SSErLifeInsurance",

                    //11-15
                    "SSEeLifeInsurance",
                    "SSErWorkingAccident",
                    "SSEeWorkingAccident",
                    "SSErRetirement",
                    "SSEeRetirement",
                    
                    //16-20
                    "FixAllowance",
                    "FixDeduction",
                    "TaxableFixedAllowance",
                    "TaxableFixedDeduction",
                    "IncEmployee",

                    //21-25
                    "VarAllowance",
                    "SSErPension",
                    "SSEePension",
                    "SSErDPLK",
                    "SSEeDPLK",

                    //26-27
                    "SSErBNILife",
                    "SSEeBNILife"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        {
                            PayrunCode = PayrunCode,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            NPWP = Sm.DrStr(dr, c[3]),
                            PTKP = Sm.DrStr(dr, c[4]),
                            SalaryAdjustment = Sm.DrDec(dr, c[5]),
                            SSEmployerHealth = Sm.DrDec(dr, c[6]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[7]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[8]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[9]),
                            SSErLifeInsurance= Sm.DrDec(dr, c[10]),
                            SSEeLifeInsurance= Sm.DrDec(dr, c[11]),
                            SSErWorkingAccident= Sm.DrDec(dr, c[12]),
                            SSEeWorkingAccident= Sm.DrDec(dr, c[13]),
                            SSErRetirement= Sm.DrDec(dr, c[14]),
                            SSEeRetirement = Sm.DrDec(dr, c[15]),
                            FixAllowance = Sm.DrDec(dr, c[16]),
                            FixDeduction = Sm.DrDec(dr, c[17]),
                            TaxableFixAllowance = Sm.DrDec(dr, c[18]),
                            TaxableFixDeduction = Sm.DrDec(dr, c[19]),
                            IncEmployee = Sm.DrDec(dr, c[20]),
                            VarAllowance = Sm.DrDec(dr, c[21]),
                            SSErPension = Sm.DrDec(dr, c[22]),
                            SSEePension = Sm.DrDec(dr, c[23]),
                            SSErDPLK = Sm.DrDec(dr, c[24]),
                            SSEeDPLK = Sm.DrDec(dr, c[25]),
                            SSErBNILife = Sm.DrDec(dr, c[26]),
                            SSEeBNILife = Sm.DrDec(dr, c[27]),
                            CreditCode1 = string.Empty,
                            CreditCode2 = string.Empty,
                            CreditCode3 = string.Empty,
                            CreditCode4 = string.Empty,
                            CreditCode5 = string.Empty,
                            CreditCode6 = string.Empty,
                            CreditCode7 = string.Empty,
                            CreditCode8 = string.Empty,
                            CreditCode9 = string.Empty,
                            CreditCode10 = string.Empty,
                            _RecomputedValueForFixedAD = 1m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            string
                EmpCode = r.EmpCode,
                EmploymentStatus = string.Empty;
            decimal 
                LatestSalary = 0m,
                LatestSalary2 = 0m;

            foreach (var x in lResult1.Where(x => x.EmpCode == EmpCode && x.ProcessInd).OrderBy(Index=>Index.Dt))
            {
                EmploymentStatus = x.EmploymentStatus;
                LatestSalary = x.EmpSalary;
                LatestSalary2 = x.EmpSalary2;
                if (x.ProcessInd)
                {
                    r.PLDay += x.PLDay;
                    r.PLHr += x.PLHr;
                    r.PLAmt += x.PLAmt;
                    r.UPLDay += x.UPLDay;
                    r.UPLHr += x.UPLHr;
                    r.UPLAmt += x.UPLAmt;
                    r.ProcessPLAmt += x.ProcessPLAmt;
                    r.ProcessUPLAmt += x.ProcessUPLAmt;
                    r.OT1Hr += x.OT1Hr;
                    r.OT2Hr += x.OT2Hr;
                    r.OTHolidayHr += x.OTHolidayHr;
                    r.OT1Amt += x.OT1Amt;
                    r.OT2Amt += x.OT2Amt;
                    r.OTHolidayAmt += x.OTHolidayAmt;
                    if (x.WorkingDuration > 0) r.WorkingDay += 1;
                }
            }

            //untuk menghitung salary dan fixed allowance apabila pada 1 period terdiri dari lbh dari 1 payrun

            //kalau resign.
            //kalau payrun info berbeda dalam 1 periode.
            //kalau sudah pernah dibayar.

            Boolean IsPayrunDifferent = false, IsLatestPaidDtExisted = false;
            decimal
                AbsentDayForJoin = 0m,
                AbsentDayForResign = 0m,
                RecomputedProcessedWorkingDay = 0m;

            foreach (var x in lResult1
                .Where(x => Sm.CompareStr(x.EmpCode, EmpCode))
                .OrderBy(Index => Index.Dt))
            {
                if (Sm.CompareDtTm(x.Dt, x.JoinDt) <= 0)
                    AbsentDayForJoin += 1;

                if (x.ResignDt.Length > 0 && Sm.CompareDtTm(x.ResignDt, x.Dt) <= 0)
                    AbsentDayForResign += 1;
                else
                {
                    if (x.LatestPaidDt.Length > 0 && Sm.CompareDtTm(x.LatestPaidDt, x.Dt) == 0)
                        IsLatestPaidDtExisted = true;

                    if (!IsPayrunDifferent &&
                        !(
                        Sm.CompareStr(x.DeptCode, mDeptCode) &&
                        Sm.CompareStr(x.SystemType, mSystemType) &&
                        Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                        Sm.CompareStr(x.PGCode, mPGCode) &&
                        Sm.CompareStr(x.SiteCode, mSiteCode)
                        ))

                        IsPayrunDifferent = true;
                }

                if (!(x.HolInd || x.WSHolidayInd))
                {
                    if (x.ProcessInd && x.WorkingDuration>0m) RecomputedProcessedWorkingDay += 1;
                }
            }

            if (mSystemType == "1") // harian
            {
                if (RecomputedProcessedWorkingDay > 25m) RecomputedProcessedWorkingDay = 25m;
                r._RecomputedValueForFixedAD = RecomputedProcessedWorkingDay / 25m;
                r.Salary = RecomputedProcessedWorkingDay * LatestSalary2;
                if (r.Salary > LatestSalary) r.Salary = LatestSalary;
                r.FixAllowance *= r._RecomputedValueForFixedAD;
                r.TaxableFixAllowance *= r._RecomputedValueForFixedAD;
                r.NonTaxableFixAllowance *= r._RecomputedValueForFixedAD;
            }
            else
            {
                r.Salary = LatestSalary;

                if (AbsentDayForJoin > 0 ||
                    AbsentDayForResign > 0 ||
                    IsLatestPaidDtExisted ||
                    IsPayrunDifferent)
                {
                    if (RecomputedProcessedWorkingDay > 20m) RecomputedProcessedWorkingDay = 20m;
                    r._RecomputedValueForFixedAD = RecomputedProcessedWorkingDay / 20m;
                    r.Salary = RecomputedProcessedWorkingDay * LatestSalary2; 
                    if (r.Salary > LatestSalary) r.Salary = LatestSalary;
                    r.FixAllowance *= r._RecomputedValueForFixedAD;
                    r.TaxableFixAllowance *= r._RecomputedValueForFixedAD;
                    r.NonTaxableFixAllowance *= r._RecomputedValueForFixedAD;
                }
            }
            
            r.VarAllowance *= RecomputedProcessedWorkingDay;
            if (r.Salary < 0m) r.Salary = 0m;
            if (r.FixAllowance < 0) r.FixAllowance = 0;
            if (r.TaxableFixAllowance < 0) r.TaxableFixAllowance = 0;
            if (r.NonTaxableFixAllowance < 0) r.NonTaxableFixAllowance = 0;
            if (r.VarAllowance < 0) r.VarAllowance = 0m;
            
            decimal EmpAdvancePayment = 0m;
            int CreditAdvancePaymentNo = 0;
            string CreditCode = string.Empty;
            foreach (var x in mlAdvancePaymentProcess.Where(x => Sm.CompareStr(x.EmpCode, EmpCode)).OrderBy(o => o.CreditCode))
            {
                if (x.CreditCode.Length > 0)
                {
                    if (Sm.CompareStr(x.CreditCode, CreditCode))
                    {
                        if (CreditAdvancePaymentNo == 1)
                            r.CreditAdvancePayment1 += x.Amt;

                        if (CreditAdvancePaymentNo == 2)
                            r.CreditAdvancePayment2 += x.Amt;

                        if (CreditAdvancePaymentNo == 3)
                            r.CreditAdvancePayment3 += x.Amt;

                        if (CreditAdvancePaymentNo == 4)
                            r.CreditAdvancePayment4 += x.Amt;

                        if (CreditAdvancePaymentNo == 5)
                            r.CreditAdvancePayment5 += x.Amt;

                        if (CreditAdvancePaymentNo == 6)
                            r.CreditAdvancePayment6 += x.Amt;

                        if (CreditAdvancePaymentNo == 7)
                            r.CreditAdvancePayment7 += x.Amt;

                        if (CreditAdvancePaymentNo == 8)
                            r.CreditAdvancePayment8 += x.Amt;

                        if (CreditAdvancePaymentNo == 9)
                            r.CreditAdvancePayment9 += x.Amt;

                        if (CreditAdvancePaymentNo == 10)
                            r.CreditAdvancePayment10 += x.Amt;
                    }
                    else
                    {
                        CreditAdvancePaymentNo += 1;

                        if (CreditAdvancePaymentNo == 1)
                        {
                            r.CreditCode1 = x.CreditCode;
                            r.CreditAdvancePayment1 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 2)
                        {
                            r.CreditCode2 = x.CreditCode;
                            r.CreditAdvancePayment2 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 3)
                        {
                            r.CreditCode3 = x.CreditCode;
                            r.CreditAdvancePayment3 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 4)
                        {
                            r.CreditCode4 = x.CreditCode;
                            r.CreditAdvancePayment4 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 5)
                        {
                            r.CreditCode5 = x.CreditCode;
                            r.CreditAdvancePayment5 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 6)
                        {
                            r.CreditCode6 = x.CreditCode;
                            r.CreditAdvancePayment6 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 7)
                        {
                            r.CreditCode7 = x.CreditCode;
                            r.CreditAdvancePayment7 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 8)
                        {
                            r.CreditCode8 = x.CreditCode;
                            r.CreditAdvancePayment8 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 9)
                        {
                            r.CreditCode9 = x.CreditCode;
                            r.CreditAdvancePayment9 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 10)
                        {
                            r.CreditCode10 = x.CreditCode;
                            r.CreditAdvancePayment10 += x.Amt;
                        }
                    }
                    CreditCode = x.CreditCode;
                }
                EmpAdvancePayment += x.Amt;
            }

            r.EmpAdvancePayment = EmpAdvancePayment;

            //r.Amt =
            // r.Salary +
            // r.OT1Amt +
            // r.OT2Amt +
            // r.OTHolidayAmt +
            // r.SalaryAdjustment +
            // r.FixAllowance +
            // r.VarAllowance +
            // r.IncEmployee -
            // r.FixDeduction -
            // r.EmpAdvancePayment -
            // r.SSEmployeeHealth -
            // r.SSEmployeeEmployment;

            //if (r.Amt < 0m) r.Amt = 0m;

            //if (r.PTKP.Length > 0 && r.NPWP.Length > 0 && r.Amt > 0)
            //    ComputeTax(ref r, ref lNTI, ref lTI2);
            
            //r.TaxAllowance = r.Tax;
            //r.Amt = (r.Amt - r.Tax) + r.TaxAllowance;
            //if (r.Amt < 0m) r.Amt = 0m;
            //r.Amt = decimal.Truncate(r.Amt);
        }

        private void ProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2, decimal Salary, decimal penambahTax, decimal pengurangTax)
        {
            r.Amt = Salary;
            if (r.NPWP.Length == 0 || r.PTKP.Length == 0) return;

            if (r.PTKP.Length > 0 && r.NPWP.Length > 0 && r.Amt > 0)
                ComputeTax(ref r, ref lNTI, ref lTI, ref lTI2, penambahTax, pengurangTax);
        }

        private void ComputeTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2, decimal penambahTax, decimal PengurangTax)
        {
            decimal
                    Value = 0m,
                    NTIAmt = 0m,
                    FunctionalExpenses = 0m,
                    Amt = 0m,
                    JoinYr = 0m;

            string NTI = r.PTKP;

            bool IsFullYr = true;

            var Penambah = penambahTax;
            var Pengurang = PengurangTax;

            Amt = Penambah;

            FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Value = Amt - FunctionalExpenses - Pengurang;

            if (r.JoinDt.Length >= 4)
                JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
                Value = Value * 12m;
            else
            {
                if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
                {
                    Value = Value * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
                    IsFullYr = false;
                }
            }

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Value > NTIAmt)
                Value -= NTIAmt;
            else
                Value = 0m;

            //if (Value > 0 && lTI.Count > 0)
            //{
            //    foreach (TI i in lTI.OrderBy(x => x.SeqNo))
            //    {
            //        if (Value >= i.Amt1 && Value <= i.Amt2)
            //        {
            //            Value = ((Value - (i.Amt1 - 1m)) * (i.TaxRate / 100));
            //            break;
            //        }
            //    }
            //}

            if (Value > 0 && lTI2.Count > 0)
            {
                foreach (TI2 i in lTI2.OrderBy(x => x.SeqNo))
                {
                    if (Value >= i.Amt1 && Value <= i.Amt2)
                    {
                        Value = ((Value - (i.Amt1 - 1m)) * (i.Value1 / i.Value2)) + i.Value3;
                        break;
                    }
                }
            }

            Value = Value / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
            if (Value <= 0) Value = 0m;
            r.TaxAllowance = Math.Round(Value, 0);
            r.Tax = r.TaxAllowance;
        }
       
        #endregion

        #region Get Tables Data

        private void ProcessAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select T1.DocNo, T1.EmpCode, T1.CreditCode, T2.Amt ");
            SQL.AppendLine("From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Yr=@Yr ");
            SQL.AppendLine("    And T2.Mth=@Mth ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine(Filter.Replace("T1.", string.Empty));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Or Not Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' " + Filter);;
            SQL.AppendLine(";");

            string Year = Sm.Left(Sm.GetDte(DteEndDt), 4), Month = Sm.GetDte(DteEndDt).Substring(4, 2);

            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "CreditCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdvancePaymentProcess()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            CreditCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Yr = Year,
                            Mth = Month
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            //SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.OTDt Between @StartDt And @EndDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTFormula(ref List<OTFormula> l, string mTxtOTFormulaCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@OTFormulaCode", mTxtOTFormulaCode);

            SQL.AppendLine("Select A.OTFormulaCode, A.TotalWHrIn1Mth, B.SeqNo, B.Hr, B.Ind  ");
            SQL.AppendLine("From TblOtFormulaHdr2 A ");
            SQL.AppendLine("Inner Join TblOtFormulaDtl2 B On A.OTFormulaCode = B.OTFormulaCode ");
            SQL.AppendLine("Where A.OTFormulaCode = @OTFormulaCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTFormulaCode", "TotalWHrIn1Mth", "SeqNo", "Hr", "Ind" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTFormula()
                        {
                            OTFormulaCode = Sm.DrStr(dr, c[0]),
                            HrPerMth = Sm.DrDec(dr, c[1]),
                            Sequence = Sm.DrDec(dr, c[2]),
                            Hr = Sm.DrDec(dr, c[3]),
                            Index = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T3.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI2(ref List<TI2> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.Value1, B.Value2, B.Value3 ");
            SQL.AppendLine("From TblTI2Hdr A ");
            SQL.AppendLine("Inner Join TblTI2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "SeqNo", "Amt1", "Amt2", "Value1", "Value2",
                    "Value3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI2()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Value1 = Sm.DrDec(dr, c[4]),
                            Value2 = Sm.DrDec(dr, c[5]),
                            Value3 = Sm.DrDec(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveDtl(ref List<LeaveDtl> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LeaveCode, SystemType, PayrunPeriod, DeductTHPInd ");
            SQL.AppendLine("From TblLeaveDtl;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LeaveCode", "SystemType", "PayrunPeriod", "DeductTHPInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveDtl()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            SystemType = Sm.DrStr(dr, c[1]),
                            PayrunPeriod = Sm.DrStr(dr, c[2]),
                            DeductTHPInd = Sm.DrStr(dr, c[3])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.PosCode, A.DeptCode, A.GrdLvlCode, C.LevelCode, A.EmploymentStatus, ");
            SQL.AppendLine("A.SystemType, A.PayrunPeriod, A.PGCode, A.SiteCode, IfNull(B.HOInd, 'N') As HOInd ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Where ");
            if (Filter.Length != 0)
                SQL.AppendLine(Filter + ";");
            else
                SQL.AppendLine(" 0=1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "LevelCode", "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", 

                    //11-12
                    "SiteCode", "HOInd"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            LevelCode = Sm.DrStr(dr, c[6]),
                            EmploymentStatus = Sm.DrStr(dr, c[7]),
                            SystemType = Sm.DrStr(dr, c[8]),
                            PayrunPeriod = Sm.DrStr(dr, c[9]),
                            PGCode = Sm.DrStr(dr, c[10]),
                            SiteCode = Sm.DrStr(dr, c[11]),
                            HOInd = Sm.DrStr(dr, c[12])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "In1", "Out1", 
                    
                    //6-9
                    "In2", "Out2", "In3", "Out3", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3]),
                            In1 = Sm.DrStr(dr, c[4]),
                            Out1 = Sm.DrStr(dr, c[5]),
                            In2 = Sm.DrStr(dr, c[6]),
                            Out2 = Sm.DrStr(dr, c[7]),
                            In3 = Sm.DrStr(dr, c[8]),
                            Out3 = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("Where A.CancelInd='N'; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T1.") + ") ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, JoinDt, ResignDt, WorkGroupCode ");
            SQL.AppendLine("From TblEmployee ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter + ");");
            else
                SQL.AppendLine("Where 1=0;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "JoinDt", "ResignDt", "WorkGroupCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            WorkGroupCode = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, StartDt, Amt, Amt2 ");
            SQL.AppendLine("From TblEmployeeSalary ");
            SQL.AppendLine("Where StartDt<=@EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ");");
            else
                SQL.AppendLine("And 0=1;");

            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    { 
                        "EmpCode", 
                        "StartDt", "Amt", "Amt2" 
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalarySS(ref List<EmployeeSalarySS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r= 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " Where ( " + Filter + ") "; 
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join TblParameter B On A.SSPCode=B.ParValue And B.ParCode='SSProgramForPension' And B.ParValue Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("); ");
            
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "EndDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalarySS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var EmpCodeTemp = string.Empty;
            var DtTemp = string.Empty;
            var TmTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Dt, Tm ");
            SQL.AppendLine("From TblAttendanceLog Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            var EndDtTemp = Sm.GetDte(DteEndDt);

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        DtTemp = Sm.DrStr(dr, c[1]);
                        TmTemp = Sm.DrStr(dr, c[2]);
                        if (TmTemp.Length > 4) TmTemp = Sm.Left(TmTemp, 4);
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = DtTemp,
                            Tm = TmTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpPaidDt(ref List<EmpPaidDt> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select Distinct A.EmpCode, A.Dt ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("And IfNull(A.PayrunCode, '')<>@PayrunCode ");
            SQL.AppendLine("And A.ProcessInd='Y' ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<string>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpPaidDt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private void GetDt(ref List<string> l)
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            DateTime
                Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    ),
                Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

            var TotalDays = (Dt2 - Dt1).Days;
            l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            }
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #region Event

        private void FrmPayrollProcess_Load(object sender, EventArgs e)
        {
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                GetParameter();
                SetLuePayrunCode(ref LuePayrunCode);
                SetGrd();

                mlAttendanceLog = new List<AttendanceLog>();
                mlOT = new List<OT>();
                mlOTFormula = new List<OTFormula>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
                mEmpOT = new List<EmpOT>();
                mEmpOT.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LuePayrunCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData();
            Sm.RefreshLookUpEdit(LuePayrunCode, new Sm.RefreshLue1(SetLuePayrunCode));
        }

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            ClearData();
            ShowPayrunInfo();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
                ProcessData();
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment status");
        }

        #endregion

        #region Class

        private class Result1
        {
            public bool _IsUseLatestGrdLvlSalary { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string _LatestLevelCode { get; set; }
            public decimal _LatestMonthlyGrdLvlSalary { get; set; }
            public decimal _LatestDailyGrdLvlSalary { get; set; }
            public bool _IsFullDayInd { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _FieldAssignment { get; set; }
            public decimal _IncPerformance { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal SalaryAD { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal ADOT { get; set; }
            public string WGCode { get; set; }
        }

        private class Result2
        {
            public decimal _RecomputedValueForFixedAD { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
	        public decimal Salary { get; set; }
	        public decimal WorkingDay { get; set; }
	        public decimal PLDay { get; set; }
	        public decimal PLHr { get; set; }
	        public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
	        public decimal OT2Hr { get; set; }
	        public decimal OTHolidayHr { get; set; }
	        public decimal OT1Amt { get; set; }
	        public decimal OT2Amt { get; set; }
	        public decimal OTHolidayAmt { get; set; }
	        public decimal FixAllowance { get; set; }
	        public decimal IncEmployee { get; set; }
	        public decimal SSEmployerHealth { get; set; }
	        public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployerPension2 { get; set; }
            public decimal SSEmployerNonRetiring { get; set; }
            public decimal SSEmployeeOldAgeInsurance { get; set; }
	        public decimal FixDeduction { get; set; }
	        public decimal DedEmployee { get; set; }
	        public decimal EmpAdvancePayment { get; set; }
	        public decimal SSEmployeeHealth { get; set; }
	        public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SSEmployeePension2 { get; set; }
            public decimal SalaryAdjustment { get; set; }
	        public decimal Tax { get; set; }
            public decimal TaxAllowance { get; set; }
            public decimal Amt { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal NonTaxableFixAllowance { get; set; }
            public decimal NonTaxableFixDeduction { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal TaxableFixDeduction { get; set; }
            public decimal SSErLifeInsurance { get; set; }
            public decimal SSEeLifeInsurance { get; set; }
            public decimal SSErWorkingAccident { get; set; }
            public decimal SSEeWorkingAccident { get; set; }
            public decimal SSErRetirement { get; set; }
            public decimal SSEeRetirement { get; set; }
            public decimal SSErPension { get; set; }
            public decimal SSEePension { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public decimal VarAllowance { get; set; }
            public decimal SSErDPLK { get; set; }
            public decimal SSEeDPLK { get; set; }
            public decimal SSErBNILife { get; set; }
            public decimal SSEeBNILife { get; set; }
        }

        #region Additional Class

        private class AdvancePaymentProcess
        {
            public string DocNo { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public string CreditCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
        }

        private class EmployeeSalarySS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class OTFormula
        {
            public string OTFormulaCode { get; set; }
            public decimal HrPerMth { get; set; }
            public decimal Sequence { get; set; }
            public decimal Hr { get; set; }
            public decimal Index { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class NTI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        private class TI2
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
            public string HOInd { get; set; }
        }

        private class EmpWorkSchedule
         {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class LeaveDtl
        {
            public string LeaveCode { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public bool DeductTHPInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LatestPaidDt { get; set; }
            public string WorkGroupCode { get; set; }
            public decimal IncPerformance { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class EmpPaidDt
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
        }

        private class EmployeeAllowance
        {
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeADOT
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
            public bool HolidayInd { get; set; }
        }

        private class PayrollProcessADOT
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string Dt { get; set; }
            public decimal Amt { get; set; }
            public decimal Duration { get; set; }
        }

        private class LeaveMealTransport
        {
            public string LeaveCode { get; set; }
        }

        private class OTIncentive
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string OTIncentiveDt { get; set; }
            public string OTIncentiveStartTm { get; set; }
            public string OTIncentiveEndTm { get; set; }
        }

        private class WorkScheduleADMealTransport
        {
            public string WSCode { get; set; }
            public string ADCode { get; set; }
        }

        private class EmpOT
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string OTDt { get; set; }
            public string OTFormulaCode { get; set; }
            public decimal Hr { get; set; }
            public decimal Amt { get; set; }
            public bool HolidayInd { get; set; }
        }

        #endregion

        #endregion
    }
}
