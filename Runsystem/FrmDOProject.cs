﻿#region Update
/*
    10/12/2019 [WED/IMS] new apps -> DO To Project
    16/12/2019 [TKG/IMS] journal untuk moving average
    06/02/2020 [TKG/IMS] menampilkan iformasi project implementation's item
    03/03/2020 [TKG/IMS] tambah proses journal
    12/05/2020 [WED/IMS] coa journal diganti, diambil dari parameter AcNoForDOToProductOfFinishedGoods dan AcNoForDOToProductOfWIP
    13/05/2020 [WED/IMS] project code nya ambil dari SO Contract
    11/06/2020 [WED/IMS] remark hdr pakai SOContract#, acno journal tanpa project code
    29/06/2020 [WED/IMS] BUG detail project implementation tidak muncul
    30/09/2002 [IBL/IMS] tambah informasi project name dan PO customer#
    13/11/2020 [ICA/IMS] Mengganti isi alert saat pilih item di mutated to dengan kondisi PRJI masih kosong
    16/11/2020 [DITA/IMS] Tambah specification di mutated from dan to, sumber qty dari listof item soc detail
    17/11/2020 [DITA/IMS] tambah socontractno di mutated from dan kolom no di mutated to
    12/02/2020 [WED/IMS] panel pertama grid hanya tampil 1 item sesuai yg dipilih
    09/02/2021 [WED/IMS] tambah biaya HPP
    03/03/2021 [WED/IMS] masih ada BUG
    17/03/2021 [VIN/IMS] socontractno ambil dari dialog
    22/03/2021 [WED/IMS] bug saat kalkulasi unit price
    25/03/2021 [WED/IMS] item yang diambil dari BOQDtl3 dan BOQDtl2
    09/07/2021 [TRI/IMS] item yang dipilih dari list project implementation hanya tampil 1 sesuai yang dipilih saja
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDOProject : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mMainCurCode = string.Empty,
            mSOContractDNo = string.Empty,
            mDocNo = string.Empty
            ;
        internal FrmDOProjectFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool
            mIsShowMutationPriceInfo = false,
            mIsShowForeignName = false;
        private string
            mDocType1 = "54",
            mDocType2 = "55",
            mEntCode = string.Empty;
        internal bool
            mIsItGrpCodeShow = false,
            mIsAutoJournalActived = false;
        private bool 
            mIsBatchNoUseDocDtIfEmpty = false, 
            mIsMovingAvgEnabled = false,
            mIsCOGSCalculated = false
            ;
        List<AdditionalCOGS> l = null;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmDOProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetIsShowMutationPriceInfo();
                SetGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, 
                        TxtTotalA, TxtTotal2A, TxtTotal3A, 
                        TxtInventoryUomCodeA, TxtInventoryUomCode2A, TxtInventoryUomCode3A,
                        TxtTotalB, TxtTotal2B, TxtTotal3B, 
                        TxtInventoryUomCodeB, TxtInventoryUomCode2B, TxtInventoryUomCode3B,

                    }, true);
                SetFormControl(mState.View);
                LuePropCode.Visible = false;
                l = new List<AdditionalCOGS>();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Local Code",
                        "Item's Name",
                        
                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",

                        //11-15
                        "Bin",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",
                        
                        //16-20
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        
                        //21-25
                        "Currency",
                        "Unit Price",
                        "Group",
                        "Foreign Name", 
                        "Specification", 

                        //26
                        "No."

                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 80, 20, 80, 200,

                        //6-10
                        0, 0, 200, 170, 60, 

                        //11-15
                        80, 100, 100, 80, 100,

                        //16-20
                        100, 80, 100, 100, 80,

                        //21-25
                        80, 100, 100, 150, 250,

                        //26
                        30,

                        
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 15, 16, 18, 19, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 9, 10, 11, 15, 16, 17, 18, 19, 20, 23 }, false);
            if (!mIsShowMutationPriceInfo) Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 24 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17, 18, 20, 21, 22, 23, 24, 25, 26});
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[23].Visible = true;
                Grd1.Cols[23].Move(5);
            }

            Grd1.Cols[24].Move(6);
            Grd1.Cols[26].Move(1);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 30;
            Grd2.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Local Code",
                        "Item's Name",

                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",
                        
                        //11-15
                        "Bin",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",

                        //16-20
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",

                        //21-25
                        "Currency",
                        "Unit Price" + Environment.NewLine + "(Based On Formula)",
                        "Unit Price",
                        "Group",
                        "Foreign Name",

                        //26-29
                        "Specification",
                        "SOContractDNo",
                        "SO Contract's" + Environment.NewLine + "Item No.",
                        "Unit Price"+Environment.NewLine+"Before COGS"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 80, 20, 80, 200,

                        //6-10
                        0, 0, 200, 170, 60,

                        //11-15
                        80, 100, 100, 80, 100,

                        //16-20
                        100, 80, 100, 100, 80,

                        //21-25
                        80, 120, 120, 100, 150,

                        //26-29
                        250, 0, 100, 0
                    }
                );

            Sm.GrdColButton(Grd2, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 12, 13, 15, 16, 18, 19, 22, 23, 29 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 3, 4, 6, 9, 10, 11, 15, 16, 17, 18, 19, 20, 24, 27, 29 }, false);
            if (!mIsShowMutationPriceInfo) Sm.GrdColInvisible(Grd2, new int[] { 21, 22, 23 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd2, new int[] { 25 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 4, 5, 6, 9, 12, 14, 15, 17, 18, 20, 21, 22, 24, 25, 26, 27, 28, 29 });
            if (mIsItGrpCodeShow)
            {
                Grd2.Cols[24].Visible = true;
                Grd2.Cols[24].Move(5);
            }

            Grd2.Cols[25].Move(6);
            Grd2.Cols[28].Move(2);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item's Code",
                    "Item's Name",
                    "Local Code",
                    "Quantity",
                    "UoM",

                    //6
                    "Specification",
                },
                new int[] 
                {
                    50,
                    100, 200, 100, 100, 80, 
                    250
                }
            );
            Sm.GrdFormatDec(Grd3, new int[] { 4 }, 0);

            #endregion

            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 15, 16, 17 }, true);
                PanelTotal2A.Visible = true;
                PanelTotal2B.Visible = true;
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 15, 16, 17, 18, 19, 20 }, true);
                PanelTotal2A.Visible = true;
                PanelTotal2B.Visible = true;
                PanelTotal3A.Visible = true;
                PanelTotal3B.Visible = true;
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeCancelReason, ChkCancelInd, MeeRemark, TxtPRJIDocNo, TxtProjectCode,
                        TxtProjectName, TxtPONo
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 13, 16, 19 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 7, 8, 10, 11, 13, 16, 19, 23 });
                    TxtDocNo.Focus();
                    BtnPRJIDocNo.Enabled = false;
                    BtnCOGS.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 13, 16, 19 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 7, 8, 10, 11, 13, 16, 19, 23 });
                    DteDocDt.Focus();
                    BtnCOGS.Enabled = true;
                    BtnPRJIDocNo.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mIsCOGSCalculated = false;
            l.Clear();
            mSOContractDNo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, MeeCancelReason, MeeRemark, LuePropCode,
                TxtInventoryUomCodeA, TxtInventoryUomCode2A, TxtInventoryUomCode3A,
                TxtInventoryUomCodeB, TxtInventoryUomCode2B, TxtInventoryUomCode3B,
                TxtProjectCode, TxtPRJIDocNo, TxtProjectName, TxtPONo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTotalA, TxtTotal2A, TxtTotal3A,
                TxtTotalB, TxtTotal2B, TxtTotal3B, TxtCOGSAmt
            }, 0);
            mEntCode = string.Empty;

            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 13, 15, 16, 18, 19, 22 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 12, 13, 15, 16, 18, 19, 22, 23, 29 });

            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4 });
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 3, 4, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOProjectFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue(
                            "Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' limit 1;");

                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                ParPrint(TxtDocNo.Text, (int)(mNumberOfInventoryUomCode));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOProject", "TblDOProjectHdr");
            string JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMutationsHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveMutationsDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveMutationsDtl2(DocNo, Row));

            cml.Add(SaveStockMovement1(DocNo, "N"));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveStockSummary1(1, Row));

            cml.Add(SaveStockMovement2(DocNo, "N"));
            cml.Add(SaveStockSummary2a(DocNo));
            cml.Add(SaveStockPrice2(DocNo));

            if (mIsAutoJournalActived)
            {
                cml.Add(SaveDOProjectJournal(DocNo, JournalDocNo, ref l));
                cml.Add(SaveJournal(DocNo, JournalDocNo));
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsTxtEmpty(TxtPRJIDocNo, "PRJI#", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty(Grd1) ||
                IsGrdExceedMaxRecords(Grd1) ||
                IsGrdValueNotValid() ||
                IsGrdEmpty(Grd2) ||
                IsGrdExceedMaxRecords(Grd2) ||
                IsGrdValueNotValid2() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) ||
                IsCOGSInvalid()
                    ;
        }

        private bool IsCOGSInvalid()
        {
            if (!mIsCOGSCalculated)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process additional COGS first.");
                BtnCOGS.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty(iGrid Grd)
        {
            if (Grd.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords(iGrid Grd)
        {
            if (Grd.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            ReComputeStock(ref Grd1);

            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    //"Property : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 13) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 12) < Sm.GetGrdDec(Grd1, Row, 13))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than mutated quantity.");
                    return true;
                }

                if (Grd1.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 16) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 15) < Sm.GetGrdDec(Grd1, Row, 16))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than mutated quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 18) < Sm.GetGrdDec(Grd1, Row, 19))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than mutated quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Item is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                    "Local Code : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine +
                    //"Property : " + Sm.GetGrdStr(Grd2, Row, 7) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd2, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd2, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd2, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd2, Row, 11) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd2, Row, 13) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd2.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd2, Row, 16) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                }

                if (Grd2.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd2, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveMutationsHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOProjectHdr(DocNo, CancelInd, DocDt, WhsCode, PRJIDocNo, SOContractDNo, COGSAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, 'N', @DocDt, @WhsCode, @PRJIDocNo, @SOContractDNo, @COGSAmt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@SOContractDNo", mSOContractDNo);
            Sm.CmParam<Decimal>(ref cm, "@COGSAmt", TxtCOGSAmt.Text.Length > 0 ? Decimal.Parse(TxtCOGSAmt.Text) : 0m);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveMutationsDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOProjectDtl(DocNo, DNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, Stock, Stock2, Stock3, Qty, Qty2, Qty3, CurCode, UPrice, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Stock, @Stock2, @Stock3, @Qty, @Qty2, @Qty3, @CurCode, @UPrice, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 11));

            Sm.CmParam<Decimal>(ref cm, "@Stock", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Stock2", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Stock3", Sm.GetGrdDec(Grd1, Row, 18));

            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMutationsDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblDOProjectDtl2(DocNo, DNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, CurCode, UPrice, SOContractDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, IfNull(@PropCode, '-'), IfNull(@BatchNo, ");
            SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
            SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), @Qty, @Qty2, @Qty3, @CurCode, @UPrice, @SOContractDNo,@CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType2);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd2, Row, 19));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 23));
            Sm.CmParam<String>(ref cm, "@SOContractDNo", Sm.GetGrdStr(Grd2, Row, 27));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveStock(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDOProjectHdr A ");
        //    SQL.AppendLine("Inner Join TblDOProjectDtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo; ");

        //    SQL.AppendLine("Update TblStockSummary As T1  ");
        //    SQL.AppendLine("Inner Join TblStockMovement T2 On T1.WhsCode=T2.WhsCode And T1.ItCode=T2.ItCode And T1.PropCode=T2.PropCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source And T1.Lot=T2.Lot And T1.Bin=T2.Bin And T2.DocType=@DocType And T2.DocNo=@DocNo ");
        //    SQL.AppendLine("Set T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

        //    SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDOProjectHdr A ");
        //    SQL.AppendLine("Inner Join TblDOProjectDtl2 B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo; ");

        //    SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDOProjectHdr A ");
        //    SQL.AppendLine("Inner Join TblDOProjectDtl2 B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo; ");

        //    SQL.AppendLine("Insert Into TblStockPrice(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select ItCode, PropCode, BatchNo, Source, CurCode, UPrice, 1, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDOProjectDtl2 ");
        //    SQL.AppendLine("Where DocNo=@DocNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType1);
        //    Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveStockMovement1(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            if (CancelInd == "N")
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            else
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType1);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary1(Byte Type, int Row)
        {
            //Type=1 -> Insert
            //Type=2 -> Edit

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            if (Type == 1)
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            else
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement2(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, @CancelInd, A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            if (CancelInd == "N")
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            else
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary2a(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary2b(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd2, Row, 19));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockPrice2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockPrice ");
            SQL.AppendLine("(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.ItCode, T.PropCode, T.BatchNo, T.Source, T.CurCode, T.UPrice, ");
            SQL.AppendLine("Case When T.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOProjectDtl2 T ");
            SQL.AppendLine("Where T.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblDOProjectHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('DO To Product (Mutations) : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Concat('SO Contract# : ', C.SOCDocNo), A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, E.SOCDocNo, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            
            #region Old Code
            //SQL.AppendLine("        Select D.AcNo As AcNo, Sum(A.Qty*B.UPrice*B.ExcRate) As DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("        From TblDOProjectDtl2 A ");
            //SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            //SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            //SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='N' ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");
            //SQL.AppendLine("        Group By D.AcNo ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("        Select D.AcNo5 As AcNo, 0.00 As DAmt, Sum(A.Qty*B.UPrice*B.ExcRate) As CAmt  ");
            //SQL.AppendLine("        From TblDOProjectDtl A ");
            //SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            //SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            //SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo5 Is Not Null And D.MovingAvgInd='N' ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");
            //SQL.AppendLine("        Group By D.AcNo5 ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("        Select D.AcNo As AcNo, Sum(A.Qty*IfNull(B.MovingAvgPrice, 0.00)) As DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("        From TblDOProjectDtl2 A ");
            //SQL.AppendLine("        Inner Join TblItemMovingAvg B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            //SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='Y' ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");
            //SQL.AppendLine("        Group By D.AcNo "); 
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("        Select D.AcNo5 As AcNo, 0.00 As DAmt, Sum(A.Qty*IfNull(B.MovingAvgPrice, 0.00)) As CAmt  ");
            //SQL.AppendLine("        From TblDOProjectDtl A ");
            //SQL.AppendLine("        Inner Join TblItemMovingAvg B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            //SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo5 Is Not Null And D.MovingAvgInd='Y' ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");
            //SQL.AppendLine("        Group By D.AcNo5 ");
            #endregion

            SQL.AppendLine("        Select C.Parvalue As AcNo, Sum(A.Qty*B.UPrice*B.ExcRate) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblDOProjectDtl2 A ");
            SQL.AppendLine("        Inner Join TblDOProjectHdr A1 On A.DocNo = A1.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode = 'AcNoForDOToProductOfFinishedGoods' And C.ParValue Is Not Null ");
            //SQL.AppendLine("        Inner Join TblProjectImplementationHdr D On D.DocNo = A1.PRJIDocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractHdr F On E.SOCDocNo = F.DocNo And F.ProjectCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblItem K On A.ItCode = K.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory L ON K.ItCtCode = L.ItCtCode And L.MovingAvgInd = 'N' ");
            //SQL.AppendLine("        Group By Concat(C.ParValue, '.', F.ProjectCode) ");
            SQL.AppendLine("        Group By C.ParValue ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.Parvalue As AcNo, 0.00 As DAmt, Sum(A.Qty*D.UPriceBeforeCOGS*B.ExcRate) As CAmt ");
            SQL.AppendLine("        From TblDOProjectDtl2 A ");
            SQL.AppendLine("        Inner Join TblDOProjectHdr A1 On A.DocNo = A1.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode = 'AcNoForDOToProductOfWIP' And C.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select If(T2.Qty > 0, (T1.Total / T2.Qty), 0.00) UPriceBeforeCOGS ");
            SQL.AppendLine("            From ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                Select Sum(Qty * UPrice) Total ");
            SQL.AppendLine("                From TblDOProjectDtl ");
            SQL.AppendLine("                Where DocNo = @DocNo ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("            Inner Join ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                Select Sum(Qty) Qty ");
            SQL.AppendLine("                From TblDOProjectDtl2 ");
            SQL.AppendLine("                Where DocNo = @DocNo ");
            SQL.AppendLine("            ) T2 On 0 = 0 ");
            SQL.AppendLine("        ) D On 0 = 0 ");
            //SQL.AppendLine("        Inner Join TblProjectImplementationHdr D On D.DocNo = A1.PRJIDocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractHdr F On E.SOCDocNo = F.DocNo And F.ProjectCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblItem K On A.ItCode = K.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory L ON K.ItCtCode = L.ItCtCode And L.MovingAvgInd = 'N' ");
            //SQL.AppendLine("        Group By Concat(C.ParValue, '.', F.ProjectCode) ");
            SQL.AppendLine("        Group By C.ParValue ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.AcNo, 0.00 As DAmt,  ");
            SQL.AppendLine("        Sum( ");
            SQL.AppendLine("        Case C.AcType ");
            SQL.AppendLine("            When 'D' Then B.DAmt - B.CAmt ");
            SQL.AppendLine("            Else B.CAmt - B.DAmt ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("        ) As CAmt ");
            SQL.AppendLine("        From TblDOProjectJournal A ");
            SQL.AppendLine("        Inner Join TblJournalDtl B On A.JournalDocNo = B.DocNo ");
            SQL.AppendLine("            And B.AcNo Like '5%' ");
            SQL.AppendLine("        Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("        Where A.DODocNo = @DocNo ");
            SQL.AppendLine("        And A.JournalDocNo Not In (Select JournalDocNo From TblDOProjectHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("        Group By B.AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.ParValue As AcNo, Sum(A.Qty*IfNull(J.MovingAvgPrice, 0.00)) + A1.COGSAmt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblDOProjectDtl2 A ");
            SQL.AppendLine("        Inner Join TblDOProjectHdr A1 On A.DocNo = A1.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode = 'AcNoForDOToProductOfFinishedGoods' And C.ParValue Is Not Null ");
            //SQL.AppendLine("        Inner Join TblProjectImplementationHdr D On D.DocNo = A1.PRJIDocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractHdr F On E.SOCDocNo = F.DocNo And F.ProjectCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblItemMovingAvg J On A.ItCode = J.ItCode ");
            SQL.AppendLine("        Inner Join TblItem K On J.ItCode = K.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory L ON K.ItCtCode = L.ItCtCode And L.MovingAvgInd = 'Y' ");
            //SQL.AppendLine("        Group By Concat(C.ParValue, '.', F.ProjectCode) ");
            SQL.AppendLine("        Group By C.ParValue ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select C.ParValue As AcNo, 0.00 As DAmt, Sum(A.Qty*IfNull(J.MovingAvgPrice, 0.00)) As CAmt ");
            SQL.AppendLine("        From TblDOProjectDtl2 A ");
            SQL.AppendLine("        Inner Join TblDOProjectHdr A1 On A.DocNo = A1.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode = 'AcNoForDOToProductOfWIP' And C.ParValue Is Not Null ");
            //SQL.AppendLine("        Inner Join TblProjectImplementationHdr D On D.DocNo = A1.PRJIDocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo ");
            //SQL.AppendLine("        Inner Join TblSOContractHdr F On E.SOCDocNo = F.DocNo And F.ProjectCode Is Not Null ");
            SQL.AppendLine("        Inner Join TblItemMovingAvg J On A.ItCode = J.ItCode ");
            SQL.AppendLine("        Inner Join TblItem K On J.ItCode = K.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory L ON K.ItCtCode = L.ItCtCode And L.MovingAvgInd = 'Y' ");
            //SQL.AppendLine("        Group By Concat(C.ParValue, '.', F.ProjectCode) ");
            SQL.AppendLine("        Group By C.ParValue ");
            SQL.AppendLine(") B On AcNo Is Not Null  ");
            SQL.AppendLine("Inner Join TblDOProjectHdr C On C.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr D ON C.PRJIDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            
            return cm;
        }

        private MySqlCommand SaveDOProjectJournal(string DocNo, string JournalDocNo, ref List<AdditionalCOGS> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;

            // journal nya dia sendiri
            SQL.AppendLine("Insert Into TblDOProjectJournal(DODocNo, JournalDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @JournalDocNo, @CreateBy, CurrentDateTime()); ");

            // journal dari COGS nya
            foreach (var x in l)
            {
                SQL.AppendLine("Insert Into TblDOProjectJournal(DODocNo, JournalDocNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @JournalDocNo_" + i.ToString() + ", @CreateBy, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key Update ");
                SQL.AppendLine("    CreateBy = @CreateBy, CreateDt = CurrentDateTime(); ");

                Sm.CmParam<String>(ref cm, "@JournalDocNo_" + i.ToString(), x.DocNo);

                i += 1;
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditMutationsHdr());

            cml.Add(SaveStockMovement1(TxtDocNo.Text, "Y"));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveStockSummary1(2, Row));

            cml.Add(SaveStockMovement2(TxtDocNo.Text, "Y"));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveStockSummary2b(Row));

            cml.Add(DeleteDOProjectJournal());

            if (mIsAutoJournalActived)
            {
                cml.Add(SaveJournal());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            ReComputeStock(ref Grd2);
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataAlreadyCancelled() ||
                IsGrdValueNotValid3();
        }

        private bool IsGrdValueNotValid3()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                {
                    Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine +
                        "Property : " + Sm.GetGrdStr(Grd2, Row, 7) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd2, Row, 8) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd2, Row, 9) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd2, Row, 10) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd2, Row, 11) + Environment.NewLine + Environment.NewLine;


                    if (Sm.GetGrdDec(Grd2, Row, 12) < Sm.GetGrdDec(Grd2, Row, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than mutated quantity.");
                        return true;
                    }

                    if (Grd2.Cols[16].Visible)
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 15) < Sm.GetGrdDec(Grd2, Row, 16))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than mutated quantity (2).");
                            return true;
                        }
                    }

                    if (Grd2.Cols[19].Visible)
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 18) < Sm.GetGrdDec(Grd2, Row, 19))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than mutated quantity (3).");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            if (TxtDocNo.Text.Length != 0)
            {
                var cm = new MySqlCommand() { CommandText = "Select DocNo From TblDOProjectHdr Where DocNo=@DocNo And CancelInd='Y' Limit 1" };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand EditMutationsHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOProjectHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteDOProjectJournal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblDOProjectJournal ");
            SQL.AppendLine("Where DODocNo = @DocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDOProjectHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblDOProjectHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblDOProjectHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @EntCode, SOContractDocNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblDOProjectHdr Where DocNo=@DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowMutationsHdr(DocNo);
                ShowMutationsDtl(DocNo);
                ShowMutationsDtl2(DocNo);
                ShowProjectImplementationItem(TxtPRJIDocNo.Text, mSOContractDNo);
                ComputeTotalA();
                ComputeTotalB();
                ComputeUnitPriceBasedOnFormula();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMutationsHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.CancelInd, A.DocDt, A.WhsCode, A.CancelReason, A.Remark, ");
            SQL.AppendLine("A.PRJIDocNo, IfNull(G.ProjectCode, D.ProjectCode2) ProjectCode, G.ProjectName, D.PONo, A.SOContractDNo, A.COGSAmt ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo = D.DOcNo ");
            SQL.AppendLine("Inner Join TblBOQHdr E On D.BOQDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup G On F.PGCode = G.PGCode ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "CancelInd", "DocDt", "WhsCode", "CancelReason", "Remark",
                    "PRJIDocNo", "ProjectCode", "ProjectName", "PONo", "SOContractDNo",
                    "COGSAmt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y");
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[7]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[8]);
                    TxtPONo.EditValue = Sm.DrStr(dr, c[9]);
                    mSOContractDNo = Sm.DrStr(dr, c[10]);
                    TxtCOGSAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                }, true
            );
        }

        private void ShowMutationsDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, ");
            SQL.AppendLine("B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.CurCode, B.UPrice, C.ItGrpCode, C.Specification,  ");
            SQL.AppendLine("IfNull(E.Qty, 0.00) + Case When A.CancelInd='N' Then B.Qty Else 0.00 End As Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0.00) + Case When A.CancelInd='N' Then B.Qty2 Else 0.00 End As Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0.00) + Case When A.CancelInd='N' Then B.Qty3 Else 0.00 End As Stock3 ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItCodeiNternal", "ItName", "PropCode", "PropName", 
                    
                    //6-10
                    "BatchNo", "Source", "Lot", "Bin", "Stock", 
                    
                    //11-15
                    "Qty", "InventoryUomCode", "Stock2", "Qty2", "InventoryUomCode2", 
                    
                    //16-20
                    "Stock3", "Qty3", "InventoryUomCode3", "CurCode", "UPrice",

                    //21-23
                    "ItGrpCode", "ForeignName", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                    Grd.Cells[Row, 26].Value = Row + 1;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMutationsDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, ");
            SQL.AppendLine("B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.CurCode, B.UPrice, C.ItGrpCode, I. Remark Specification, I.No SOContractNo,  ");
            SQL.AppendLine("B.Qty As Stock, ");
            SQL.AppendLine("B.Qty2 As Stock2, ");
            SQL.AppendLine("B.Qty3 As Stock3 ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Inner Join Tblprojectimplementationhdr F On F.DocNo = A.PRJIDocNo ");
            SQL.AppendLine("Inner Join tblsocontractrevisionhdr G ON F.SOContractDocno=G.Docno ");
            SQL.AppendLine("Inner Join tblsocontracthdr H ON G.SOCDocNo = H.DocNo ");
            SQL.AppendLine("Inner Join tblsocontractdtl I ON H.DocNo = I.DocNo And B.ItCode = I.ItCode And I.DNo = B.SOContractDNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItCodeiNternal", "ItName", "PropCode", "PropName", 
                    
                    //6-10
                    "BatchNo", "Source", "Lot", "Bin", "Stock", 
                    
                    //11-15
                    "Qty", "InventoryUomCode", "Stock2", "Qty2", "InventoryUomCode2", 
                    
                    //16-20
                    "Stock3", "Qty3", "InventoryUomCode3", "CurCode", "UPrice",

                    //21-24
                    "ItGrpCode", "ForeignName", "Specification", "SOContractNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Grd.Cells[Row, 22].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22, 23 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowProjectImplementationItem(string DocNo, string SOContractDNo)
        {
            Sm.ClearGrd(Grd3, false);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select E.ItCode, E.ItName, E.ItCodeInternal, F.QtyPackagingUnit, E.PurchaseUomCode, F.Remark Specification ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSoContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo=B.DOcNo ");
            SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("        Select T1.DocNo, T2.SOCDocNo, T5.ItCode, T5.SeqNo, T4.Qty, T5.no ");
            SQL.AppendLine("        From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("        Inner JOin TblSOCOntractRevisionHdr T2 On T1.SOContractDocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.DocNo = @DocNo ");
            SQL.AppendLine("        Inner JOin TblSOCOntractHdr T3 On T2.SOCDocNo = T3.DocNo ");
            SQL.AppendLine("        Inner Join TblBOQDtl3 T4 On T3.BOQDocNo = T4.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractDtl5 T5 On T3.DocNo = T5.DocNo And T4.ItCode = T5.ItCode And T4.SeqNo = T5.SeqNo ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select T1.DocNo, T2.SOCDocNo, T5.ItCode, T5.SeqNo, T4.Qty, T5.no ");
            SQL.AppendLine("        From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("        Inner JOin TblSOCOntractRevisionHdr T2 On T1.SOContractDocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.DocNo = @DocNo ");
            SQL.AppendLine("        Inner JOin TblSOCOntractHdr T3 On T2.SOCDocNo = T3.DocNo ");
            SQL.AppendLine("        Inner Join TblBOQDtl2 T4 On T3.BOQDocNo = T4.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractDtl4 T5 On T3.DocNo = T5.DocNo And T4.ItCode = T5.ItCode And T4.SeqNo = T5.SeqNo ");
            SQL.AppendLine(") D On A.DocNo = D.DocNo And B.DocNo = D.SOCDocNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode And E.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblSOContractDtl F ON B.DocNo = F.DocNo And D.ItCode = F.ItCode AND F.no = D.no");
            if (SOContractDNo.Length > 0)
                SQL.AppendLine("    And F.DNo = @SOContractDNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By E.ItCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDNo", SOContractDNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "ItCode", 
                    "ItName", "ItCodeInternal", "QtyPackagingUnit", "PurchaseUomCode", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ClearCOGSIndicator()
        {
            if (Grd1.Rows.Count <= 1 || Grd1.Rows.Count <= 1)
            {
                mIsCOGSCalculated = false;
            }
        }

        private void CalculateCOGS()
        {
            l.Clear();

            GetDOProjectJournal(ref l);
            CalculateCOGSAmt(ref l);
        }

        private void GetDOProjectJournal(ref List<AdditionalCOGS> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.AcNo, B.DAmt, B.CAmt, C.AcType ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.MenuCode In (Select MenuCode From TblMenu Where Param In ('FrmJournal' , 'FrmVoucherJournal')) ");
            SQL.AppendLine("    And B.SOContractDocNo Is Not Null ");
            SQL.AppendLine("    And B.SOContractDocNo In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T2.SOCDocNo ");
            SQL.AppendLine("        From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("        Inner Join TblSOContractRevisionHdr T2 On T1.SOContractDocNO = T2.DocNo ");
            SQL.AppendLine("            And T1.DocNo = @PRJIDocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.DocNo Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select JournalDocNo ");
            SQL.AppendLine("        From TblDOProjectJournal ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Like '5%' ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AcNo", "DAmt", "CAmt", "AcType" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdditionalCOGS()
                        {
                            DocNo = Sm.DrStr(dr, 0),
                            AcNo = Sm.DrStr(dr, 1),
                            DAmt = Sm.DrDec(dr, 2),
                            CAmt = Sm.DrDec(dr, 3),
                            AcType = Sm.DrStr(dr, 4)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void CalculateCOGSAmt(ref List<AdditionalCOGS> l)
        {
            decimal Amt = 0m;

            foreach (var x in l)
            {
                Amt += (x.AcType == "D") ? (x.DAmt - x.CAmt) : (x.CAmt - x.DAmt);
            }

            TxtCOGSAmt.EditValue = Sm.FormatNum((Amt), 0);
        }

        internal void Grd2AfterCommitEdit(int Row, int Col)
        {

            if (Col == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd2, Row, 2, 13, 16, 19, 14, 17, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd2, Row, 2, 13, 19, 16, 14, 20, 17);
            }

            if (Col == 16)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd2, Row, 2, 16, 13, 19, 17, 14, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd2, Row, 2, 16, 19, 13, 17, 20, 14);
            }

            if (Col == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd2, Row, 2, 19, 13, 16, 20, 14, 17);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd2, Row, 2, 19, 16, 13, 20, 17, 14);
            }

            if (Col == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 14), Sm.GetGrdStr(Grd2, Row, 17)))
                Sm.CopyGrdValue(Grd2, Row, 16, Grd2, Row, 13);

            if (Col == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 14), Sm.GetGrdStr(Grd2, Row, 20)))
                Sm.CopyGrdValue(Grd2, Row, 19, Grd2, Row, 13);

            if (Col == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 17), Sm.GetGrdStr(Grd2, Row, 20)))
                Sm.CopyGrdValue(Grd2, Row, 19, Grd2, Row, 16);

            if (Col == 13 || Col == 16 || Col == 19) ComputeUnitPriceBasedOnFormula();

            if (Sm.IsGrdColSelected(new int[] { 13, 16, 19 }, Col)) ComputeTotalB();
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsBatchNoUseDocDtIfEmpty = Sm.GetParameterBoo("IsBatchNoUseDocDtIfEmpty");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
        }

        private void SetIsShowMutationPriceInfo()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select ParValue From TblParameter ");
            SQL.AppendLine("Where ParCode='MutationPriceInfoGroup' ");
            SQL.AppendLine("And ParValue Like Concat('%#', (Select GrpCode From TblUser Where UserCode=@UserCode),'#%') ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            mIsShowMutationPriceInfo = Sm.IsDataExist(cm);
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem(ref iGrid Grd)
        {
            var SQL = string.Empty;
            if (Grd.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd, Row, 2) +
                            Sm.GetGrdStr(Grd, Row, 6) +
                            Sm.GetGrdStr(Grd, Row, 8) +
                            Sm.GetGrdStr(Grd, Row, 9) +
                            Sm.GetGrdStr(Grd, Row, 10) +
                            Sm.GetGrdStr(Grd, Row, 11) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem2(ref iGrid Grd)
        {
            var SQL = string.Empty;
            if (Grd.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd, Row, 9).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd, Row, 9) +
                            Sm.GetGrdStr(Grd, Row, 10) +
                            Sm.GetGrdStr(Grd, Row, 11) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock(ref iGrid Grd)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd, Row, 9).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd, Row, 9);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd, Row, 9), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd, Row, 10), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd, Row, 11), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 3);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 4);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 5);
                                break;
                            }
                        }
                    }
                    Grd.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void ComputeUnitPriceBasedOnFormula()
        {
            decimal Total = 0m, Qty = 0m, UPrice = 0m, TotalBeforeCOGS = 0m, UPriceBeforeCOGS = 0m;
            string CurCode = string.Empty;

            if (Grd1.Rows.Count > 1 && Sm.GetGrdStr(Grd1, 0, 21).Length > 0)
                CurCode = Sm.GetGrdStr(Grd1, 0, 21);

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 13).Length > 0 && Sm.GetGrdStr(Grd1, Row, 22).Length > 0)
                    Total += Sm.GetGrdDec(Grd1, Row, 13) * Sm.GetGrdDec(Grd1, Row, 22);
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 13).Length > 0) Qty += Sm.GetGrdDec(Grd2, Row, 13);
            }

            TotalBeforeCOGS = Total;
            if (TxtCOGSAmt.Text.Length > 0) Total += Decimal.Parse(TxtCOGSAmt.Text);

            if (Qty > 0)
            {
                UPrice = Total / Qty;
                UPriceBeforeCOGS = TotalBeforeCOGS / Qty;
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                {
                    Grd2.Cells[Row, 22].Value = UPrice;
                    Grd2.Cells[Row, 29].Value = UPriceBeforeCOGS;
                    //TxtCOGSAmt.EditValue = Sm.FormatNum(UPriceBeforeCOGS, 0);
                    if (BtnSave.Enabled)
                    {
                        Grd2.Cells[Row, 21].Value = CurCode;
                        Grd2.Cells[Row, 23].Value = UPrice;
                    }
                }
            }
        }

        //from = credit
        private decimal GetAmountFrom()
        {
            decimal Amt = 0, Qty = 0m, UPrice = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 13);
                    if (Sm.GetGrdStr(Grd1, Row, 22).Length > 0) UPrice = Sm.GetGrdDec(Grd1, Row, 22);
                    Amt += (Qty*UPrice);
                }
            }
            return Amt;
        }

        //to = debet
        private decimal GetAmountTo()
        {
            decimal Amt = 0m, Qty = 0m, UPrice = 0m;
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 13).Length > 0) Qty = Sm.GetGrdDec(Grd2, Row, 13);
                    if (Sm.GetGrdStr(Grd2, Row, 23).Length > 0) UPrice = Sm.GetGrdDec(Grd2, Row, 23);
                    Amt += (Qty*UPrice);
                }
            }
            return Amt;
        }


        private void ParPrint(string DocNo, int parValue)
        {
            var l = new List<SMut>();
            var ldtl = new List<SMutDtl>();
            var ldtl2 = new List<SMutDtl2>();

            string[] TableName = { "SMut", "SMutDtl", "SMutDtl2" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, A.Remark ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         "WhsName",
                         "Remark",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SMut()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            HRemark = Sm.DrStr(dr, c[7]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select B.DNo, B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQLDtl.AppendLine("B.Qty, C.InventoryUomCode, B.Qty2, C.InventoryUomCode2, B.Qty3, C.InventoryUomCode3, B.CurCode, B.UPrice, A.Remark, C.ItGrpCode ");
                SQLDtl.AppendLine("From TblDOProjectHdr A ");
                SQLDtl.AppendLine("Inner Join TblDOProjectDtl B On A.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By B.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",
                         
                         //1-5
                         "ItCode" ,
                         "ItName" ,
                         "BatchNo",
                         "Source",
                         "Lot",
                         
                         //6-10
                         "Bin",
                         "Qty" ,
                         "InventoryUomCode",
                         "Qty2" ,
                         "InventoryUomCode2" ,
                         
                         //11-15
                         "Qty3" ,
                         "InventoryUomCode3",
                         "CurCode" ,
                         "UPrice" ,
                         "ItGrpCode"
                });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new SMutDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[3]),
                            Source = Sm.DrStr(drDtl, cDtl[4]),
                            Lot = Sm.DrStr(drDtl, cDtl[5]),
                            Bin = Sm.DrStr(drDtl, cDtl[6]),
                            Qty = Sm.DrDec(drDtl, cDtl[7]),
                            UomCode = Sm.DrStr(drDtl, cDtl[8]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[9]),
                            UomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[11]),
                            UomCode3 = Sm.DrStr(drDtl, cDtl[12]),
                            CurCode = Sm.DrStr(drDtl, cDtl[13]),
                            UPrice = Sm.DrDec(drDtl, cDtl[14]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[15]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.DNo, A.ItCode, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
                SQLDtl2.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, ");
                SQLDtl2.AppendLine("A.CurCode, A.UPrice, B.ItGrpCode ");
                SQLDtl2.AppendLine("From TblDOProjectDtl2 A ");
                SQLDtl2.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                         //0
                         "DNo",
                         
                         //1-5
                         "ItCode" ,
                         "ItName" ,
                         "BatchNo",
                         "Source",
                         "Lot",

                         //6-10
                         "Bin",
                         "Qty" ,
                         "InventoryUomCode",
                         "Qty2" ,
                         "InventoryUomCode2" ,
                         
                         //11-15
                         "Qty3",
                         "InventoryUomCode3",
                         "CurCode",
                         "UPrice",
                         "ItGrpCode"
                    });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new SMutDtl2()
                        {
                            DNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItCode = Sm.DrStr(drDtl2, cDtl2[1]),

                            ItName = Sm.DrStr(drDtl2, cDtl2[2]),
                            BatchNo = Sm.DrStr(drDtl2, cDtl2[3]),
                            Source = Sm.DrStr(drDtl2, cDtl2[4]),
                            Lot = Sm.DrStr(drDtl2, cDtl2[5]),
                            Bin = Sm.DrStr(drDtl2, cDtl2[6]),

                            Qty = Sm.DrDec(drDtl2, cDtl2[7]),
                            UomCode = Sm.DrStr(drDtl2, cDtl2[8]),
                            Qty2 = Sm.DrDec(drDtl2, cDtl2[9]),
                            UomCode2 = Sm.DrStr(drDtl2, cDtl2[10]),
                            Qty3 = Sm.DrDec(drDtl2, cDtl2[11]),

                            UomCode3 = Sm.DrStr(drDtl2, cDtl2[12]),
                            CurCode = Sm.DrStr(drDtl2, cDtl2[13]),
                            UPrice = Sm.DrDec(drDtl2, cDtl2[14]),
                            ItGrpCode = Sm.DrStr(drDtl2, cDtl2[15])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("StockMutations", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("StockMutations2", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("StockMutations3", myLists, TableName, false);
                    break;
            }
        }

        internal void ComputeTotalA()
        {
            bool IsFirst = true;
            decimal Total = 0m, Total2 = 0m, Total3 = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 2).Length != 0)
                {
                    if (IsFirst)
                    {
                        TxtInventoryUomCodeA.Text = Sm.GetGrdStr(Grd1, row, 14);
                        TxtInventoryUomCode2A.Text = Sm.GetGrdStr(Grd1, row, 17);
                        TxtInventoryUomCode3A.Text = Sm.GetGrdStr(Grd1, row, 20);
                        IsFirst = false;
                    }
                    Total += Sm.GetGrdDec(Grd1, row, 13);
                    Total2 += Sm.GetGrdDec(Grd1, row, 16);
                    Total3 += Sm.GetGrdDec(Grd1, row, 19);
                }
            }
            TxtTotalA.EditValue = Sm.FormatNum(Total, 0);
            TxtTotal2A.EditValue = Sm.FormatNum(Total2, 0);
            TxtTotal3A.EditValue = Sm.FormatNum(Total3, 0);
        }

        internal void ComputeTotalB()
        {
            bool IsFirst = true;
            decimal Total = 0m, Total2 = 0m, Total3 = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0)
                {
                    if (IsFirst)
                    {
                        TxtInventoryUomCodeB.Text = Sm.GetGrdStr(Grd2, row, 14);
                        TxtInventoryUomCode2B.Text = Sm.GetGrdStr(Grd2, row, 17);
                        TxtInventoryUomCode3B.Text = Sm.GetGrdStr(Grd2, row, 20);
                        IsFirst = false;
                    }
                    Total += Sm.GetGrdDec(Grd2, row, 13);
                    Total2 += Sm.GetGrdDec(Grd2, row, 16);
                    Total3 += Sm.GetGrdDec(Grd2, row, 19);
                }
            }
            TxtTotalB.EditValue = Sm.FormatNum(Total, 0);
            TxtTotal2B.EditValue = Sm.FormatNum(Total2, 0);
            TxtTotal3B.EditValue = Sm.FormatNum(Total3, 0);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLuePropertyCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropCode As Col1, B.PropName As Col2 ");
            SQL.AppendLine("From TblItemProperty A ");
            SQL.AppendLine("Inner Join TblProperty B On A.PropCode = B.PropCode ");
            SQL.AppendLine("Where A.ItCode=@ItCode Order By B.PropName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtInventoryUomCodeA, TxtInventoryUomCode2A, TxtInventoryUomCode3A, TxtInventoryUomCodeB, TxtInventoryUomCode2B, 
                    TxtInventoryUomCode3B
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                {
                    TxtTotalA, TxtTotal2A, TxtTotal3A, TxtTotalB, 
                    TxtTotal2B, TxtTotal3B 
                }, 0);
                ClearGrd();
            }
        }

        private void LuePropCode_Leave(object sender, EventArgs e)
        {
            if (LuePropCode.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (Sm.GetLue(LuePropCode).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 6].Value =
                    Grd2.Cells[fCell.RowIndex, 7].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LuePropCode);
                    Grd2.Cells[fCell.RowIndex, 7].Value = LuePropCode.GetColumnValue("Col2");
                }
                LuePropCode.Visible = false;
            }
        }

        private void LuePropCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LuePropCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropCode, new Sm.RefreshLue2(SetLuePropertyCode), Sm.GetGrdStr(Grd2, Grd2.CurRow.Index, 2));
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
            }
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeTrim(MeeRemark);
        }

        private void TxtCOGSAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtCOGSAmt, 0);
        }
    
        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtPRJIDocNo, "PRJI#", false))
                        Sm.FormShowDialog(new FrmDOProjectDlg2(this));

                    if (e.ColIndex == 7 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
                    {
                        LueRequestEdit(Grd2, LuePropCode, ref fCell, ref fAccept, e);
                        //Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        SetLuePropertyCode(ref LuePropCode, Sm.GetGrdStr(Grd2, e.RowIndex, 2));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 1, 7, 8, 10, 11, 13, 16, 19, 23 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd2, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22, 23 });
                    }
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete)
                {
                    Sm.GrdRemoveRow(Grd2, e, BtnSave);
                    ComputeUnitPriceBasedOnFormula();
                }
                ComputeTotalB();
            }
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
            ClearCOGSIndicator();
        }

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtPRJIDocNo, "PRJI#", false))
                Sm.FormShowDialog(new FrmDOProjectDlg2(this));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 13, 16, 19, 23 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 7, 8, 10, 11 }, e);

            Grd2AfterCommitEdit(e.RowIndex, e.ColIndex);
        }

        private void Grd2_ColHdrDoubleClick(object sender, TenTec.Windows.iGridLib.iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 13, 16, 19 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd2, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }

            //if (e.ColIndex == 13)
            //    ComputeUnitPriceBasedOnFormula();
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOProjectDlg(this, Sm.GetLue(LueWhsCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 1, 13, 16, 19 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22 });
                    }
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeUnitPriceBasedOnFormula();
                }
                ComputeTotalA();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            ClearCOGSIndicator();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmDOProjectDlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 13, 16, 19 }, e);

            if (e.ColIndex == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 13, 16, 19, 14, 17, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 2, 13, 19, 16, 14, 20, 17);
            }

            if (e.ColIndex == 16)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 16, 13, 19, 17, 14, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 2, 16, 19, 13, 17, 20, 14);
            }

            if (e.ColIndex == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 2, 19, 13, 16, 20, 14, 17);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 2, 19, 16, 13, 20, 17, 14);
            }

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 16);

            if (e.ColIndex == 13 || e.ColIndex == 16 || e.ColIndex == 19)  ComputeUnitPriceBasedOnFormula();

            if (Sm.IsGrdColSelected(new int[] { 13, 16, 19 }, e.ColIndex)) ComputeTotalA();
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 13, 16, 19 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

      

        #endregion

        #region Button Click

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmDOProjectDlg3(this));
            }
        }

        private void BtnPRJIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "PRJI#", false))
            {
                var f = new FrmProjectImplementation2(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPRJIDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnCOGS_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtPRJIDocNo, "PRJI#", false))
            {
                if (Grd1.Rows.Count > 0 && Grd2.Rows.Count > 0)
                {
                    CalculateCOGS();
                    mIsCOGSCalculated = true;
                    ComputeUnitPriceBasedOnFormula();
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class SMut
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string HRemark { get; set; }
            public string PrintBy { get; set; }
        }

        private class SMutDtl
        {
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public string UomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string UomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string UomCode3 { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public string DRemark { get; set; }
            public string ItGrpCode { get; set; }
        }

        private class SMutDtl2
        {
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public string UomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string UomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string UomCode3 { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public string DRemark { get; set; }
            public string ItGrpCode { get; set; }
        }

        private class AdditionalCOGS
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion
    }
}
