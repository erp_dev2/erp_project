﻿#region Update
/*
    20/01/2018 [TKG] ubah query
    06/08/2019 [TKG] perhitungan cuti melihat hari libur juga.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpLeave : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmpLeave(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.Posname, D.DeptName, E.LeaveName, ");
            SQL.AppendLine("A.NumberOfDay, A.NumberOfHour, A.StartDt, A.Remark ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select EmpCode, LeaveCode, StartDt, Remark, ");
            SQL.AppendLine("   Sum(NumberOfDay) As NumberOfDay, ");
            SQL.AppendLine("   Sum(NumberOfHour) As NumberOfHour ");
            SQL.AppendLine("   From ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.LeaveCode, T1.StartDt, T1.Remark, ");
            SQL.AppendLine("        Case When T3.EmpCode Is Not Null Then 0.00 Else ");
            SQL.AppendLine("            Case T1.LeaveType When 'F' Then 1.00 When 'H' Then 0.00 Else 0.5 End ");
            SQL.AppendLine("        End As NumberOfDay,");
            SQL.AppendLine("        Case When T3.EmpCode Is Not Null Then 0.00 Else ");
            SQL.AppendLine("            Case T1.LeaveType When 'H' Then T1.DurationHour Else 0.00 End ");
            SQL.AppendLine("        End As NumberOfHour ");
            SQL.AppendLine("        From TblEmpLeaveHdr T1  ");
            SQL.AppendLine("        Inner Join TblEmpLeaveDtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.LeaveDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select Distinct X1.EmpCode, X1.Dt ");
            SQL.AppendLine("            From TblEmpWorkSchedule X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode And X2.HolidayInd='Y' ");
            SQL.AppendLine("            Where X1.Dt Between @DocDt1 And @DocDt2 ");
            if (Filter.Length > 0)
            {
                SQL.AppendLine("       And X1.EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee " + Filter);
                SQL.AppendLine("            ) ");
            }
            SQL.AppendLine("        ) T3 On T1.EmpCode=T3.EmpCode And T2.LeaveDt=T3.Dt ");
            SQL.AppendLine("       Where T1.CancelInd='N' ");
            SQL.AppendLine("       And IfNull(T1.Status, 'O') In ('O', 'A') ");
            if (Filter.Length > 0)
            {
                SQL.AppendLine("       And T1.EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee " + Filter);
                SQL.AppendLine("            ) ");
            }

            SQL.AppendLine("       Union All ");

            SQL.AppendLine("       Select T3.EmpCode, T1.LeaveCode, T1.StartDt, T1.Remark, ");
            SQL.AppendLine("        Case When  T4.EmpCode Is Not Null Then 0.00 Else ");
            SQL.AppendLine("            Case T1.LeaveType When 'F' Then 1.00 When 'H' Then 0.00 Else 0.5 End ");
            SQL.AppendLine("        End As NumberOfDay,");
            SQL.AppendLine("        Case When  T4.EmpCode Is Not Null Then 0.00 Else ");
            SQL.AppendLine("            Case T1.LeaveType When 'H' Then T1.DurationHour Else 0.00 End ");
            SQL.AppendLine("        End As NumberOfHour ");
            SQL.AppendLine("       From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("       Inner Join TblEmpLeave2Dtl2 T2 ");
            SQL.AppendLine("           On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("           And T2.LeaveDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("       Inner Join TblEmpLeave2Dtl T3 On T1.DocNo=T3.DocNo ");
            if (Filter.Length > 0)
            {
                SQL.AppendLine("       And T3.EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee " + Filter);
                SQL.AppendLine("            ) ");
            }
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select Distinct X1.EmpCode, X1.Dt ");
            SQL.AppendLine("            From TblEmpWorkSchedule X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode And X2.HolidayInd='Y' ");
            SQL.AppendLine("            Where X1.Dt Between @DocDt1 And @DocDt2 ");
            if (Filter.Length > 0)
            {
                SQL.AppendLine("       And X1.EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee " + Filter);
                SQL.AppendLine("            ) ");
            }
            SQL.AppendLine("        ) T4 On T3.EmpCode=T4.EmpCode And T2.LeaveDt=T4.Dt ");
            SQL.AppendLine("       Where T1.CancelInd='N' ");
            SQL.AppendLine("       And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("   ) T Group By EmpCode, LeaveCode, StartDt, Remark ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblLeave E On A.LeaveCode=E.LeaveCode ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's Name",
                        "Old Code", 
                        "Position",
                        "Department",
                        
                        //6-10
                        "Leave Type",
                        "Days",
                        "Hours",
                        "Start",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 80, 170, 200, 
                        
                        //6-10
                        150, 70, 70, 80, 350
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "EmpCode", "EmpCodeOld", "EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "LeaveName", 

                            //6-9
                            "NumberOfDay", "NumberOfHour", "StartDt", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
