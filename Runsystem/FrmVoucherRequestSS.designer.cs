﻿namespace RunSystem
{
    partial class FrmVoucherRequestSS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVoucherRequestSS));
            this.panel5 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.TcVoucherRequestSS = new System.Windows.Forms.TabControl();
            this.TpGeneral = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtPaidToBankCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtPaymentUser = new DevExpress.XtraEditors.TextEdit();
            this.TxtPaidToBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPaidToBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtPaidToBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAdjustmentAmt = new DevExpress.XtraEditors.TextEdit();
            this.LblAdjustmentAmt = new System.Windows.Forms.Label();
            this.TxtMth = new DevExpress.XtraEditors.TextEdit();
            this.TxtYr = new DevExpress.XtraEditors.TextEdit();
            this.BtnEmpSSListDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEmpSSListDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtSSPCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmployee = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtEmployer = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmpSSListDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.LueAcType = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TpBudget = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TcVoucherRequestSS.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAdjustmentAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpSSListDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.TpBudget.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(795, 0);
            this.panel1.Size = new System.Drawing.Size(70, 608);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcVoucherRequestSS);
            this.panel2.Size = new System.Drawing.Size(795, 336);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Location = new System.Drawing.Point(0, 336);
            this.panel3.Size = new System.Drawing.Size(795, 272);
            this.panel3.Controls.SetChildIndex(this.panel5, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 586);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(795, 155);
            this.Grd1.TabIndex = 66;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Grd2);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 155);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(795, 117);
            this.panel5.TabIndex = 31;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 22);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(795, 95);
            this.Grd2.TabIndex = 68;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(795, 22);
            this.button1.TabIndex = 67;
            this.button1.Text = "List of Approval";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // TcVoucherRequestSS
            // 
            this.TcVoucherRequestSS.Controls.Add(this.TpGeneral);
            this.TcVoucherRequestSS.Controls.Add(this.TpBudget);
            this.TcVoucherRequestSS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcVoucherRequestSS.Location = new System.Drawing.Point(0, 0);
            this.TcVoucherRequestSS.Name = "TcVoucherRequestSS";
            this.TcVoucherRequestSS.SelectedIndex = 0;
            this.TcVoucherRequestSS.Size = new System.Drawing.Size(795, 336);
            this.TcVoucherRequestSS.TabIndex = 0;
            // 
            // TpGeneral
            // 
            this.TpGeneral.Controls.Add(this.panel4);
            this.TpGeneral.Location = new System.Drawing.Point(4, 23);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TpGeneral.Size = new System.Drawing.Size(787, 309);
            this.TpGeneral.TabIndex = 0;
            this.TpGeneral.Text = "General";
            this.TpGeneral.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.TxtAdjustmentAmt);
            this.panel4.Controls.Add(this.LblAdjustmentAmt);
            this.panel4.Controls.Add(this.TxtMth);
            this.panel4.Controls.Add(this.TxtYr);
            this.panel4.Controls.Add(this.BtnEmpSSListDocNo2);
            this.panel4.Controls.Add(this.BtnEmpSSListDocNo);
            this.panel4.Controls.Add(this.TxtVoucherDocNo);
            this.panel4.Controls.Add(this.TxtSSPCode);
            this.panel4.Controls.Add(this.TxtVoucherRequestDocNo);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.TxtEmployee);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtEmployer);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.TxtTotal);
            this.panel4.Controls.Add(this.TxtEmpSSListDocNo);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.TxtStatus);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Controls.Add(this.DteDocDt);
            this.panel4.Controls.Add(this.LueAcType);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtDocNo);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(781, 303);
            this.panel4.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.TxtPaidToBankCode);
            this.panel6.Controls.Add(this.TxtAmt);
            this.panel6.Controls.Add(this.TxtPaymentUser);
            this.panel6.Controls.Add(this.TxtPaidToBankAcNo);
            this.panel6.Controls.Add(this.LueCurCode);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.TxtGiroNo);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.TxtPaidToBankBranch);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.DteDueDt);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.TxtPaidToBankAcName);
            this.panel6.Controls.Add(this.LueBankCode);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.LuePaymentType);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.LueBankAcCode);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(386, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(395, 303);
            this.panel6.TabIndex = 125;
            // 
            // TxtPaidToBankCode
            // 
            this.TxtPaidToBankCode.EnterMoveNextControl = true;
            this.TxtPaidToBankCode.Location = new System.Drawing.Point(127, 130);
            this.TxtPaidToBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankCode.Name = "TxtPaidToBankCode";
            this.TxtPaidToBankCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankCode.Properties.MaxLength = 30;
            this.TxtPaidToBankCode.Size = new System.Drawing.Size(264, 20);
            this.TxtPaidToBankCode.TabIndex = 136;
            this.TxtPaidToBankCode.Validated += new System.EventHandler(this.TxtPaidToBankCode_Validated);
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(127, 235);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Size = new System.Drawing.Size(180, 20);
            this.TxtAmt.TabIndex = 146;
            this.TxtAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // TxtPaymentUser
            // 
            this.TxtPaymentUser.EnterMoveNextControl = true;
            this.TxtPaymentUser.Location = new System.Drawing.Point(127, 109);
            this.TxtPaymentUser.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaymentUser.Name = "TxtPaymentUser";
            this.TxtPaymentUser.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaymentUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaymentUser.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaymentUser.Properties.Appearance.Options.UseFont = true;
            this.TxtPaymentUser.Properties.MaxLength = 50;
            this.TxtPaymentUser.Size = new System.Drawing.Size(264, 20);
            this.TxtPaymentUser.TabIndex = 134;
            this.TxtPaymentUser.Validated += new System.EventHandler(this.TxtPaymentUser_Validated);
            // 
            // TxtPaidToBankAcNo
            // 
            this.TxtPaidToBankAcNo.EnterMoveNextControl = true;
            this.TxtPaidToBankAcNo.Location = new System.Drawing.Point(127, 193);
            this.TxtPaidToBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankAcNo.Name = "TxtPaidToBankAcNo";
            this.TxtPaidToBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankAcNo.Properties.MaxLength = 80;
            this.TxtPaidToBankAcNo.Size = new System.Drawing.Size(264, 20);
            this.TxtPaidToBankAcNo.TabIndex = 142;
            this.TxtPaidToBankAcNo.Validated += new System.EventHandler(this.TxtPaidToBankAcNo_Validated);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(127, 214);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 20;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 150;
            this.LueCurCode.Size = new System.Drawing.Size(264, 20);
            this.LueCurCode.TabIndex = 144;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(75, 258);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 147;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(74, 112);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 14);
            this.label24.TabIndex = 133;
            this.label24.Text = "Paid To";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(127, 67);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(264, 20);
            this.TxtGiroNo.TabIndex = 130;
            this.TxtGiroNo.Validated += new System.EventHandler(this.TxtGiroNo_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(34, 175);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 14);
            this.label22.TabIndex = 139;
            this.label22.Text = "Account Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(67, 217);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 14);
            this.label8.TabIndex = 143;
            this.label8.Text = "Currency";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(6, 70);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 14);
            this.label10.TabIndex = 129;
            this.label10.Text = "Giro Bilyet / Cheque";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankBranch
            // 
            this.TxtPaidToBankBranch.EnterMoveNextControl = true;
            this.TxtPaidToBankBranch.Location = new System.Drawing.Point(127, 151);
            this.TxtPaidToBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankBranch.Name = "TxtPaidToBankBranch";
            this.TxtPaidToBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankBranch.Properties.MaxLength = 80;
            this.TxtPaidToBankBranch.Size = new System.Drawing.Size(264, 20);
            this.TxtPaidToBankBranch.TabIndex = 138;
            this.TxtPaidToBankBranch.Validated += new System.EventHandler(this.TxtPaidToBankBranch_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(45, 238);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 145;
            this.label11.Text = "Paid Amount";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(127, 88);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(121, 20);
            this.DteDueDt.TabIndex = 132;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(43, 154);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 14);
            this.label21.TabIndex = 137;
            this.label21.Text = "Branch Name";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(63, 91);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 14);
            this.label12.TabIndex = 131;
            this.label12.Text = "Due Date";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(54, 133);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 14);
            this.label20.TabIndex = 135;
            this.label20.Text = "Bank Name";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(54, 49);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 14);
            this.label14.TabIndex = 127;
            this.label14.Text = "Bank Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankAcName
            // 
            this.TxtPaidToBankAcName.EnterMoveNextControl = true;
            this.TxtPaidToBankAcName.Location = new System.Drawing.Point(127, 172);
            this.TxtPaidToBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankAcName.Name = "TxtPaidToBankAcName";
            this.TxtPaidToBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankAcName.Properties.MaxLength = 80;
            this.TxtPaidToBankAcName.Size = new System.Drawing.Size(264, 20);
            this.TxtPaidToBankAcName.TabIndex = 140;
            this.TxtPaidToBankAcName.Validated += new System.EventHandler(this.TxtPaidToBankAcName_Validated);
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(127, 46);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 20;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(264, 20);
            this.LueBankCode.TabIndex = 128;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            this.LueBankCode.EnabledChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(60, 196);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 14);
            this.label23.TabIndex = 141;
            this.label23.Text = "Account#";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(35, 7);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 14);
            this.label15.TabIndex = 123;
            this.label15.Text = "Payment Type";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(127, 4);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 20;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 250;
            this.LuePaymentType.Size = new System.Drawing.Size(264, 20);
            this.LuePaymentType.TabIndex = 124;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            this.LuePaymentType.EnabledChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(69, 28);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 14);
            this.label13.TabIndex = 125;
            this.label13.Text = "Account";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(127, 25);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 20;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 500;
            this.LueBankAcCode.Size = new System.Drawing.Size(264, 20);
            this.LueBankAcCode.TabIndex = 126;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            this.LueBankAcCode.EnabledChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EditValue = "";
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(127, 256);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 350;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(650, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(264, 20);
            this.MeeRemark.TabIndex = 148;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtAdjustmentAmt
            // 
            this.TxtAdjustmentAmt.EnterMoveNextControl = true;
            this.TxtAdjustmentAmt.Location = new System.Drawing.Point(139, 278);
            this.TxtAdjustmentAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAdjustmentAmt.Name = "TxtAdjustmentAmt";
            this.TxtAdjustmentAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAdjustmentAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAdjustmentAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAdjustmentAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAdjustmentAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAdjustmentAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAdjustmentAmt.Size = new System.Drawing.Size(179, 20);
            this.TxtAdjustmentAmt.TabIndex = 124;
            this.TxtAdjustmentAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            this.TxtAdjustmentAmt.Validated += new System.EventHandler(this.TxtAdjustmentAmt_Validated);
            // 
            // LblAdjustmentAmt
            // 
            this.LblAdjustmentAmt.AutoSize = true;
            this.LblAdjustmentAmt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAdjustmentAmt.ForeColor = System.Drawing.Color.Black;
            this.LblAdjustmentAmt.Location = new System.Drawing.Point(65, 280);
            this.LblAdjustmentAmt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAdjustmentAmt.Name = "LblAdjustmentAmt";
            this.LblAdjustmentAmt.Size = new System.Drawing.Size(71, 14);
            this.LblAdjustmentAmt.TabIndex = 123;
            this.LblAdjustmentAmt.Text = "Adjustment";
            this.LblAdjustmentAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMth
            // 
            this.TxtMth.EnterMoveNextControl = true;
            this.TxtMth.Location = new System.Drawing.Point(139, 131);
            this.TxtMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth.Name = "TxtMth";
            this.TxtMth.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtMth.Properties.MaxLength = 16;
            this.TxtMth.Properties.ReadOnly = true;
            this.TxtMth.Size = new System.Drawing.Size(79, 20);
            this.TxtMth.TabIndex = 84;
            // 
            // TxtYr
            // 
            this.TxtYr.EnterMoveNextControl = true;
            this.TxtYr.Location = new System.Drawing.Point(139, 110);
            this.TxtYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtYr.Name = "TxtYr";
            this.TxtYr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtYr.Properties.Appearance.Options.UseFont = true;
            this.TxtYr.Properties.MaxLength = 16;
            this.TxtYr.Properties.ReadOnly = true;
            this.TxtYr.Size = new System.Drawing.Size(79, 20);
            this.TxtYr.TabIndex = 82;
            // 
            // BtnEmpSSListDocNo2
            // 
            this.BtnEmpSSListDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpSSListDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpSSListDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpSSListDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpSSListDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnEmpSSListDocNo2.Appearance.Options.UseFont = true;
            this.BtnEmpSSListDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnEmpSSListDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpSSListDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpSSListDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpSSListDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpSSListDocNo2.Image")));
            this.BtnEmpSSListDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpSSListDocNo2.Location = new System.Drawing.Point(346, 89);
            this.BtnEmpSSListDocNo2.Name = "BtnEmpSSListDocNo2";
            this.BtnEmpSSListDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpSSListDocNo2.TabIndex = 80;
            this.BtnEmpSSListDocNo2.ToolTip = "Show Social Security";
            this.BtnEmpSSListDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpSSListDocNo2.ToolTipTitle = "Run System";
            this.BtnEmpSSListDocNo2.Click += new System.EventHandler(this.BtnEmpSSListDocNo2_Click);
            // 
            // BtnEmpSSListDocNo
            // 
            this.BtnEmpSSListDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpSSListDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpSSListDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpSSListDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpSSListDocNo.Appearance.Options.UseBackColor = true;
            this.BtnEmpSSListDocNo.Appearance.Options.UseFont = true;
            this.BtnEmpSSListDocNo.Appearance.Options.UseForeColor = true;
            this.BtnEmpSSListDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnEmpSSListDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpSSListDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpSSListDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpSSListDocNo.Image")));
            this.BtnEmpSSListDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpSSListDocNo.Location = new System.Drawing.Point(320, 89);
            this.BtnEmpSSListDocNo.Name = "BtnEmpSSListDocNo";
            this.BtnEmpSSListDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpSSListDocNo.TabIndex = 79;
            this.BtnEmpSSListDocNo.ToolTip = "Find Social Security Document";
            this.BtnEmpSSListDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpSSListDocNo.ToolTipTitle = "Run System";
            this.BtnEmpSSListDocNo.Click += new System.EventHandler(this.BtnEmpSSListDocNo_Click);
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(139, 257);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 16;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(178, 20);
            this.TxtVoucherDocNo.TabIndex = 96;
            // 
            // TxtSSPCode
            // 
            this.TxtSSPCode.EnterMoveNextControl = true;
            this.TxtSSPCode.Location = new System.Drawing.Point(139, 152);
            this.TxtSSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSSPCode.Name = "TxtSSPCode";
            this.TxtSSPCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSSPCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSSPCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSSPCode.Properties.MaxLength = 16;
            this.TxtSSPCode.Properties.ReadOnly = true;
            this.TxtSSPCode.Size = new System.Drawing.Size(228, 20);
            this.TxtSSPCode.TabIndex = 86;
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(139, 236);
            this.TxtVoucherRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(178, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 94;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(74, 258);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 14);
            this.label6.TabIndex = 95;
            this.label6.Text = "Voucher#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployee
            // 
            this.TxtEmployee.EnterMoveNextControl = true;
            this.TxtEmployee.Location = new System.Drawing.Point(139, 194);
            this.TxtEmployee.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployee.Name = "TxtEmployee";
            this.TxtEmployee.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmployee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployee.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployee.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployee.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmployee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmployee.Properties.ReadOnly = true;
            this.TxtEmployee.Size = new System.Drawing.Size(178, 20);
            this.TxtEmployee.TabIndex = 90;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(25, 239);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 14);
            this.label4.TabIndex = 93;
            this.label4.Text = "Voucher Request#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(76, 197);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 14);
            this.label9.TabIndex = 89;
            this.label9.Text = "Employee";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployer
            // 
            this.TxtEmployer.EnterMoveNextControl = true;
            this.TxtEmployer.Location = new System.Drawing.Point(139, 173);
            this.TxtEmployer.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployer.Name = "TxtEmployer";
            this.TxtEmployer.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmployer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployer.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployer.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmployer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmployer.Properties.ReadOnly = true;
            this.TxtEmployer.Size = new System.Drawing.Size(178, 20);
            this.TxtEmployer.TabIndex = 88;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(79, 176);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 14);
            this.label18.TabIndex = 87;
            this.label18.Text = "Employer";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(104, 113);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 14);
            this.label19.TabIndex = 81;
            this.label19.Text = "Year";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(94, 135);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 14);
            this.label25.TabIndex = 83;
            this.label25.Text = "Month";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(2, 156);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(134, 14);
            this.label26.TabIndex = 85;
            this.label26.Text = "Social Security Program";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(139, 215);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(178, 20);
            this.TxtTotal.TabIndex = 92;
            // 
            // TxtEmpSSListDocNo
            // 
            this.TxtEmpSSListDocNo.EnterMoveNextControl = true;
            this.TxtEmpSSListDocNo.Location = new System.Drawing.Point(139, 89);
            this.TxtEmpSSListDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpSSListDocNo.Name = "TxtEmpSSListDocNo";
            this.TxtEmpSSListDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpSSListDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpSSListDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpSSListDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpSSListDocNo.Properties.MaxLength = 16;
            this.TxtEmpSSListDocNo.Properties.ReadOnly = true;
            this.TxtEmpSSListDocNo.Size = new System.Drawing.Size(178, 20);
            this.TxtEmpSSListDocNo.TabIndex = 78;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(42, 92);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 14);
            this.label28.TabIndex = 77;
            this.label28.Text = "Social Security#";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(51, 71);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 14);
            this.label16.TabIndex = 75;
            this.label16.Text = "Account Type";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(101, 218);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(35, 14);
            this.label29.TabIndex = 91;
            this.label29.Text = "Total";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(139, 47);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(121, 20);
            this.TxtStatus.TabIndex = 73;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(94, 49);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 14);
            this.label17.TabIndex = 72;
            this.label17.Text = "Status";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(263, 48);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 74;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(139, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 71;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // LueAcType
            // 
            this.LueAcType.EnterMoveNextControl = true;
            this.LueAcType.Location = new System.Drawing.Point(139, 68);
            this.LueAcType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType.Name = "LueAcType";
            this.LueAcType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.Appearance.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType.Properties.DropDownRows = 20;
            this.LueAcType.Properties.NullText = "[Empty]";
            this.LueAcType.Properties.PopupWidth = 143;
            this.LueAcType.Size = new System.Drawing.Size(121, 20);
            this.LueAcType.TabIndex = 76;
            this.LueAcType.ToolTip = "F4 : Show/hide list";
            this.LueAcType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType.EnabledChanged += new System.EventHandler(this.LueAcType_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(103, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 70;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(139, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(228, 20);
            this.TxtDocNo.TabIndex = 69;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(63, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 68;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpBudget
            // 
            this.TpBudget.Controls.Add(this.panel7);
            this.TpBudget.Location = new System.Drawing.Point(4, 23);
            this.TpBudget.Name = "TpBudget";
            this.TpBudget.Padding = new System.Windows.Forms.Padding(3);
            this.TpBudget.Size = new System.Drawing.Size(764, 309);
            this.TpBudget.TabIndex = 1;
            this.TpBudget.Text = "Budget";
            this.TpBudget.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.LueDeptCode);
            this.panel7.Controls.Add(this.label34);
            this.panel7.Controls.Add(this.TxtRemainingBudget);
            this.panel7.Controls.Add(this.label35);
            this.panel7.Controls.Add(this.LueBCCode);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 303);
            this.panel7.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(30, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 62;
            this.label3.Text = "Department";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(107, 5);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(311, 20);
            this.LueDeptCode.TabIndex = 63;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(6, 52);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(97, 14);
            this.label34.TabIndex = 60;
            this.label34.Text = "Available Budget";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(107, 49);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(229, 20);
            this.TxtRemainingBudget.TabIndex = 61;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(3, 30);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 14);
            this.label35.TabIndex = 58;
            this.label35.Text = "Budget Category";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(107, 27);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(311, 20);
            this.LueBCCode.TabIndex = 59;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // FrmVoucherRequestSS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 608);
            this.Name = "FrmVoucherRequestSS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TcVoucherRequestSS.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAdjustmentAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpSSListDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.TpBudget.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button1;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabControl TcVoucherRequestSS;
        private System.Windows.Forms.TabPage TpGeneral;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtAdjustmentAmt;
        private System.Windows.Forms.Label LblAdjustmentAmt;
        internal DevExpress.XtraEditors.TextEdit TxtMth;
        internal DevExpress.XtraEditors.TextEdit TxtYr;
        public DevExpress.XtraEditors.SimpleButton BtnEmpSSListDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnEmpSSListDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtSSPCode;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmployee;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtEmployer;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        internal DevExpress.XtraEditors.TextEdit TxtEmpSSListDocNo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.LookUpEdit LueAcType;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage TpBudget;
        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankCode;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        internal DevExpress.XtraEditors.TextEdit TxtPaymentUser;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankAcNo;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankBranch;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankAcName;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label label35;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
    }
}