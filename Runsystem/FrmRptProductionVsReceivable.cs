﻿#region Update
/*
    27/03/2020 [WED/YK] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionVsReceivable : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mJointOperationTypeForKSOLead = string.Empty,
            mJointOperationTypeForKSOMember = string.Empty,
            mJointOperationTypeForNonKSO = string.Empty,
            mSiteCode = string.Empty,
            mTypeForDetailRealization = string.Empty,
            mDescForDetailRealization = string.Empty;

        private int[] mColDec = 
        {
            11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 
            26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 54, 55, 60, 
            61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 
            76, 77, 78, 79, 80, 81, 82, 83, 84
        };

        #endregion

        #region Constructor

        public FrmRptProductionVsReceivable(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueSiteCode(ref LueSiteCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 86;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "KSO/Non-KSO",
                    "Carry Over/New",
                    "NAS",
                    "UP",
                    "Project Code",
                    
                    //6-10
                    "Project Name", 
                    "Contract Date",
                    "Delivery Date", 
                    "Number of "+Environment.NewLine+"Month",
                    "SY/MY",

                    //11-15
                    "Contract Amount",
                    "Production Amount",
                    "Last"+Environment.NewLine+"Settled Amount",
                    "Settled"+Environment.NewLine+"January",
                    "Settled"+Environment.NewLine+"February",

                    //16-20
                    "Settled"+Environment.NewLine+"March",
                    "Total 1st Quarterly",
                    "Settled"+Environment.NewLine+"April",
                    "Settled"+Environment.NewLine+"May",
                    "Settled"+Environment.NewLine+"June",

                    //21-25
                    "Total 2nd Quarterly",
                    "Settled"+Environment.NewLine+"July",
                    "Settled"+Environment.NewLine+"August",
                    "Settled"+Environment.NewLine+"September",
                    "Total 3rd Quarterly",

                    //26-30
                    "Settled"+Environment.NewLine+"October",
                    "Settled"+Environment.NewLine+"November",
                    "Settled"+Environment.NewLine+"December",
                    "Total 4th Quarterly",
                    "Total Production",

                    //31-35
                    "Total Co +"+Environment.NewLine+"Production",
                    "Outstanding Production",
                    "Voucher Amount",
                    "Incoming"+Environment.NewLine+"January",
                    "Incoming"+Environment.NewLine+"February",

                    //36-40
                    "Incoming"+Environment.NewLine+"March",
                    "Total 1st Quarterly",
                    "Incoming"+Environment.NewLine+"April",
                    "Incoming"+Environment.NewLine+"May",
                    "Incoming"+Environment.NewLine+"June",

                    //41-45
                    "Total 2nd Quarterly",
                    "Incoming"+Environment.NewLine+"July",
                    "Incoming"+Environment.NewLine+"August",
                    "Incoming"+Environment.NewLine+"September",
                    "Total 3rd Quarterly",

                    //46-50
                    "Incoming"+Environment.NewLine+"October",
                    "Incoming"+Environment.NewLine+"November",
                    "Incoming"+Environment.NewLine+"December",
                    "Total 4th Quarterly",
                    "Total Incoming",

                    //51-55
                    "Total Co"+Environment.NewLine+"Incoming",
                    "Receivable",
                    "Receivable"+Environment.NewLine+"Remarks",
                    "RBP Amount",
                    "RBP"+Environment.NewLine+"Production Percentage",

                    //56-60
                    "Contract Date",
                    "Delivery Date", 
                    "Number of "+Environment.NewLine+"Month",
                    "SY/MY",
                    "Total Production",

                    //61-65
                    "Outstanding Production",
                    "Planning of Production"+Environment.NewLine+"This Year",
                    "Last"+Environment.NewLine+"Settled Amount",
                    "Plan"+Environment.NewLine+"January",
                    "Plan"+Environment.NewLine+"February",

                    //66-70
                    "Plan"+Environment.NewLine+"March",
                    "Total 1st Quarterly",
                    "Plan"+Environment.NewLine+"April",
                    "Plan"+Environment.NewLine+"May",
                    "Plan"+Environment.NewLine+"June",

                    //71-75
                    "Total 2nd Quarterly",
                    "Plan"+Environment.NewLine+"July",
                    "Plan"+Environment.NewLine+"August",
                    "Plan"+Environment.NewLine+"September",
                    "Total 3rd Quarterly",

                    //76-80
                    "Plan"+Environment.NewLine+"October",
                    "Plan"+Environment.NewLine+"November",
                    "Plan"+Environment.NewLine+"December",
                    "Total 4th Quarterly",
                    "Total Production",

                    //81-85
                    "Percentage towards Target",
                    "Total Co"+Environment.NewLine+"Production",
                    "Percentage towards Target",
                    "Outstanding Production",
                    "Production Remarks"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    130, 100, 60, 60, 150, 
                    
                    //6-10
                    200, 80, 150, 150, 150, 

                    //11-15
                    150, 100, 100, 100, 100, 

                    //16-20
                    150, 50, 150, 150, 150, 

                    //21-25
                    150, 150, 150, 150, 150, 

                    //26-30
                    150, 150, 150, 150, 150, 

                    //31-35
                    150, 150, 150, 150, 150, 

                    //36-40
                    150, 150, 150, 150, 150, 
                    
                    //41-45
                    150, 150, 150, 150, 150, 

                    //46-50
                    150, 150, 150, 150, 150, 

                    //51-55
                    150, 150, 150, 150, 150, 

                    //56-60
                    150, 150, 150, 150, 150, 

                    //61-65
                    150, 150, 150, 150, 150, 

                    //66-70
                    150, 150, 150, 150, 150, 

                    //71-75
                    150, 150, 150, 150, 150, 

                    //76-80
                    150, 150, 150, 150, 150, 

                    //81-85
                    150, 150, 150, 150, 150

                }
            );
            Sm.GrdFormatDec(Grd1, mColDec, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueSiteCode, "Site")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProductionVsReceivable>();
                var l2 = new List<AuctionBranchToSite>();
                var l3 = new List<DummyCounter>();
                var l4 = new List<SiteCounter>();

                mSiteCode = Sm.GetLue(LueSiteCode);

                ProcessSiteCounter(ref l3);
                ProcessBranchToSite(ref l2);

                if (l2.Count > 0 && l3.Count > 0)
                {
                    ProcessSiteCounter2(ref l2, ref l3, ref l4);
                }

                Process1(ref l, ref l4);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods


        private void ProcessBranchToSite(ref List<AuctionBranchToSite> l)
        {
            string sSQL = "Select OptCode, OptDesc From TblOption Where OptCat = 'AuctionBranchToSite';";
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = sSQL;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AuctionBranchToSite()
                        {
                            BranchCode = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                    {
                        if (mSiteCode.Length > 0) mSiteCode += ",";
                        mSiteCode += x.BranchCode;
                    }
                }

                if (mSiteCode.Length == 0) mSiteCode = Sm.GetLue(LueSiteCode);
            }
        }


        private void ProcessSiteCounter(ref List<DummyCounter> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.SiteCode, Count(A.DocNo) Counter ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("    And C.Status = 'A' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("    And Left(C.DocDt, 4) = @Yr ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr D On C.DocNo = D.SOContractDocNo ");
            SQL.AppendLine("Group By A.SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "Counter" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DummyCounter()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            No = Sm.DrInt(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSiteCounter2(ref List<AuctionBranchToSite> l, ref List<DummyCounter> l2, ref List<SiteCounter> l3)
        {
            foreach (var x in l2)
            {
                foreach (var y in l)
                {
                    if (x.SiteCode == y.BranchCode)
                    {
                        x.SiteCode = y.SiteCode;
                        break;
                    }
                }
            }

            l3 = l2.GroupBy(x => x.SiteCode)
                .Select(t => new SiteCounter()
                {
                    SiteCode = t.Key,
                    No = 0,
                    No2 = t.Sum(s => s.No)
                }).ToList();

            for (int i = 0; i < l3.Count; ++i)
            {
                if (i != 0)
                {
                    l3[i].No = l3[i - 1].No2 + 1;
                    l3[i].No2 += l3[i - 1].No2;
                }
                else
                {
                    l3[i].No = 1;
                }
            }
        }


        private void SetLueSiteCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT SiteCode Col1, SiteName Col2 ");
            SQL.AppendLine("FROM tblsite ");
            SQL.AppendLine("WHERE FIND_IN_SET(sitecode, (SELECT parvalue FROM tblparameter WHERE parcode = 'SiteCodeForAuctionInfo')) ");
            SQL.AppendLine("AND sitecode NOT IN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT optcode ");
            SQL.AppendLine("    FROM tbloption ");
            SQL.AppendLine("    WHERE optcat = 'AuctionBranchToSite' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By SiteCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mTypeForDetailRealization = Sm.GetParameter("TypeForDetailRealization");
            mDescForDetailRealization = Sm.GetParameter("DescForDetailRealization");
            mJointOperationTypeForKSOLead = Sm.GetParameter("JointOperationTypeForKSOLead");
            mJointOperationTypeForKSOMember = Sm.GetParameter("JointOperationTypeForKSOMember");
            mJointOperationTypeForNonKSO = Sm.GetParameter("JointOperationTypeForNonKSO");

            if (mTypeForDetailRealization.Length == 0) mTypeForDetailRealization = "Carry Over,Proyek Baru";
            if (mDescForDetailRealization.Length == 0) mDescForDetailRealization = "PROYEK-PROYEK NON-KSO,PROYEK-PROYEK KSO,PROYEK-PROYEK KSO (Member) ";
        }

        private void Process1(ref List<ProductionVsReceivable> l, ref List<SiteCounter> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int mUP = 1;
            int mNas = 1;

            foreach (var x in l2)
            {
                if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                {
                    mNas = x.No;
                    break;
                }
            }

            string[] mProjectDesc = mDescForDetailRealization.Split(',');
            string[] mProjectType = mTypeForDetailRealization.Split(',');

            SQL.AppendLine("SELECT T1.* FROM ( ");

            for (int i = 0; i < mProjectDesc.Count(); ++i)
            {
                for (int j = 0; j < mProjectType.Count(); ++j)
                {
                    SQL.AppendLine("SELECT '" + mProjectDesc[i] + "' AS ProjectDesc, '" + mProjectType[j] + "' AS ProjectDesc2, If(F.Parvalue = '2', D.ProjectCode2, ProjectCode) ProjectCode2, B.ProjectName, Date_Format(D.DocDt, '%d %b %Y') DocDt, IfNull(Date_Format(E.DeliveryDt, '%d %b %Y'), '') DeliveryDt, ");
                    SQL.AppendLine("TIMESTAMPDIFF(MONTH, D.DOcDt, E.DeliveryDt) NumberOfMonth,  ");
                    SQL.AppendLine("Case When TIMESTAMPDIFF(MONTH, D.DocDt, E.DeliveryDt) <=12 Then 'SY' When TIMESTAMPDIFF(MONTH, D.DocDt, E.DeliveryDt) >12 Then  'MY' END AS Duration, ");
                    SQL.AppendLine("If(C.TaxInd = 'Y', (G.Amt + (G.Amt * 0.1)), G.Amt) ContractAmt, G.Amt ProductionAmt, IFNULL(I.SettledAmt, 0.00) LastSettledAmt, IFNULL(J.Amt1, 0.00) PAmt1, IFNULL(J.Amt2, 0.00) PAmt2, IFNULL(J.Amt3, 0.00) PAmt3, ");
                    SQL.AppendLine("IFNULL(J.Amt4, 0.00) PAmt4, IFNULL(J.Amt5, 0.00) PAmt5, IFNULL(J.Amt6, 0.00) PAmt6, IFNULL(J.Amt7, 0.00) PAmt7, IFNULL(J.Amt8, 0.00) PAmt8, IFNULL(J.Amt9, 0.00) PAmt9, IFNULL(J.Amt10, 0.00) PAmt10, ");
                    SQL.AppendLine("IFNULL(J.Amt11, 0.00) PAmt11, IFNULL(J.Amt12, 0.00) PAmt12, IFNULL(K.Amt, 0.00) VAmt, IFNULL(L.Amt1, 0.00) IAmt1, IFNULL(L.Amt2, 0.00) IAmt2, IFNULL(L.Amt3, 0.00) IAmt3, IFNULL(L.Amt4, 0.00) IAmt4, ");
                    SQL.AppendLine("IFNULL(L.Amt5, 0.00) IAmt5, IFNULL(L.Amt6, 0.00) IAmt6, IFNULL(L.Amt7, 0.00) IAmt7, IFNULL(L.Amt8, 0.00) IAmt8, IFNULL(L.Amt9, 0.00) IAmt9, IFNULL(L.Amt10, 0.00) IAmt10, IFNULL(L.Amt11, 0.00) IAmt11, ");
                    SQL.AppendLine("IFNULL(L.Amt12, 0.00) IAmt12, H.TotalResource, H.ExclPPNPercentage, If(SUBSTR(H.DocDt, 5, 2) = '01', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt1,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '02', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt2,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '03', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt3,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '04', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt4,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '05', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt5,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '06', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt6,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '07', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt7,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '08', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt8,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '09', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt9,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '10', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt10,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '11', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt11,  ");
                    SQL.AppendLine("If(SUBSTR(H.DocDt, 5, 2) = '12', IFNULL(If(H.ExclPPNPercentage = 0.00, 0.00, (H.RemunerationAmt + H.DirectCostAmt)/H.ExclPPNPercentage), 0.00), 0.00) RDAmt12,  ");
                    SQL.AppendLine("D.Remark ");
                    SQL.AppendLine("FROM TblSite A ");
                    SQL.AppendLine("INNER JOIN TblLOPHdr B ON A.SiteCOde = B.SiteCode ");
                    SQL.AppendLine("    AND (FIND_IN_SET(A.SiteCode, @SiteCode)) ");
                    SQL.AppendLine("    AND B.CancelInd = 'N' ");
                    SQL.AppendLine("    AND B.STATUS = 'A' ");
                    SQL.AppendLine("INNER JOIN TblBOQHdr C ON B.DocNo = C.LOPDocNo ");
                    SQL.AppendLine("INNER JOIN TblSOContractHdr D ON C.DocNo = D.BOQDocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(D.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(D.DocDt, 4) = @Yr ");

                    SQL.AppendLine("INNER JOIN TblSOContractDtl E ON D.DocNo = E.DocNo ");
                    SQL.AppendLine("LEFT JOIN TblParameter F ON F.Parcode = 'ProjectAcNoFormula' ");
                    SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr G ON D.DocNo = G.SOCDocNo ");
                    SQL.AppendLine("INNER JOIN TblProjectImplementationHdr H ON G.DocNo = H.SOContractDocNo ");
                    SQL.AppendLine("    AND H.CancelInd = 'N' ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T1.DocNo, SUM(T2.SettledAmt) SettledAmt ");
                    SQL.AppendLine("    FROM TblProjectImplementationHdr T1 ");
                    SQL.AppendLine("    INNER JOIN TblProjectImplementationDtl T2 ON T1.DocNO = T2.DocNo ");
                    SQL.AppendLine("        AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T2.SettledInd = 'Y' ");
                    SQL.AppendLine("        AND T2.SettleDt IS NOT NULL ");
                    SQL.AppendLine("        AND LEFT(T2.SettleDt, 4) = (@Yr - 1) ");
                    SQL.AppendLine("        AND T2.SettledAmt != 0.00 ");
                    SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T3 ON T1.SOContractDocNo = T3.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblSOCOntractHdr T4 ON T3.SOCDocNo = T4.DOcNo ");
                    
                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) = @Yr ");

                    SQL.AppendLine("    INNER JOIN TblBOQHdr T5 ON T4.BOQDocNO = T5.DOcNo ");
                    SQL.AppendLine("    INNER JOIN TblLOPHdr T6 ON T5.LOPDocNO = T6.DOcNo ");
                    SQL.AppendLine("        AND FIND_IN_SET(T6.SiteCode, @SiteCode) ");
                    SQL.AppendLine("    GROUP BY T1.DocNo ");
                    SQL.AppendLine(") I ON H.DocNo = I.DocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T.SOCDocNo, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.Amt3) Amt3, SUM(T.Amt4) Amt4, ");
                    SQL.AppendLine("    SUM(T.Amt5) Amt5, SUM(T.Amt6) Amt6, SUM(T.Amt7) Amt7, SUM(T.Amt8) Amt8,  ");
                    SQL.AppendLine("    SUM(T.Amt9) Amt9, SUM(T.Amt10) Amt10, SUM(T.Amt11) Amt11, SUM(T.Amt12) Amt12 ");
                    SQL.AppendLine("    FROM  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT T5.SOCDocNo, If(SUBSTR(T7.SettleDt, 5, 2) = '01', T7.SettledAmt, 0.00) Amt1, ");
                    SQL.AppendLine("        If(SUBSTR(T7.SettleDt, 5, 2) = '02', T7.SettledAmt, 0.00) Amt2, If(SUBSTR(T7.SettleDt, 5, 2) = '03', T7.SettledAmt, 0.00) Amt3,  ");
                    SQL.AppendLine("        If(SUBSTR(T7.SettleDt, 5, 2) = '04', T7.SettledAmt, 0.00) Amt4, If(SUBSTR(T7.SettleDt, 5, 2) = '05', T7.SettledAmt, 0.00) Amt5,  ");
                    SQL.AppendLine("        If(SUBSTR(T7.SettleDt, 5, 2) = '06', T7.SettledAmt, 0.00) Amt6, If(SUBSTR(T7.SettleDt, 5, 2) = '07', T7.SettledAmt, 0.00) Amt7,  ");
                    SQL.AppendLine("        If(SUBSTR(T7.SettleDt, 5, 2) = '08', T7.SettledAmt, 0.00) Amt8, If(SUBSTR(T7.SettleDt, 5, 2) = '09', T7.SettledAmt, 0.00) Amt9,  ");
                    SQL.AppendLine("        If(SUBSTR(T7.SettleDt, 5, 2) = '10', T7.SettledAmt, 0.00) Amt10, If(SUBSTR(T7.SettleDt, 5, 2) = '11', T7.SettledAmt, 0.00) Amt11,  ");
                    SQL.AppendLine("        If(SUBSTR(T7.SettleDt, 5, 2) = '12', T7.SettledAmt, 0.00) Amt12 ");
                    SQL.AppendLine("        FROM TblSite T1 ");
                    SQL.AppendLine("        INNER JOIN TblLOPHdr T2 ON T1.SIteCode = T2.SiteCOde ");
                    SQL.AppendLine("            AND FIND_IN_SET(T1.SiteCode, @SiteCode) ");
                    SQL.AppendLine("            AND T2.CancelInd = 'Y' ");
                    SQL.AppendLine("            AND T2.STATUS = 'A' ");
                    SQL.AppendLine("        INNER JOIN TblBOQHdr T3 ON T2.DOcNo = T3.LOPDocNo ");
                    SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.DocNo = T4.BOQDocNo ");
                    
                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) = @Yr ");
                    
                    SQL.AppendLine("        INNER JOIN TblSOCOntractRevisionHdr T5 ON T4.DocNo = T5.SOCDocNo ");
                    SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T6 ON T5.DocNo = T6.SOContractDocNo ");
                    SQL.AppendLine("        INNER JOIN TblProjectImplementationDtl T7 ON T6.DocNo = T7.DocNo ");
                    SQL.AppendLine("            AND T7.SettledInd = 'Y' ");
                    SQL.AppendLine("            AND T7.SettleDt IS NOT NULL ");
                    SQL.AppendLine("            AND T7.SettledAmt != 0.00 ");
                    SQL.AppendLine("            AND LEFT(T7.SettleDt, 4) = @Yr ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("    GROUP BY T.SOCDocNO ");
                    SQL.AppendLine(") J ON D.DocNo = J.SOCDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T.SOCDocNo, SUM(T.Amt) Amt ");
                    SQL.AppendLine("    FROM  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT T5.SOCDocNo, T10.Amt ");
                    SQL.AppendLine("        FROM TblSite T1 ");
                    SQL.AppendLine("        INNER JOIN TblLOPHdr T2 ON T1.SiteCode = T2.SiteCode ");
                    SQL.AppendLine("            AND FIND_IN_SET(T1.SiteCode, @SiteCode) ");
                    SQL.AppendLine("            AND T2.CancelInd = 'N' ");
                    SQL.AppendLine("            AND T2.STATUS = 'A' ");
                    SQL.AppendLine("        INNER JOIN TblBOQHdr T3 ON T2.DocNo = T3.LOPDocNO ");
                    SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.DocNo = T4.BOQDocNO ");

                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) = @Yr ");

                    SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T5 ON T4.DocNo = T5.SOCDocNo ");
                    SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T6 ON T5.DocNo = T6.SOContractDocNo ");
                    SQL.AppendLine("        INNER JOIN TblSalesInvoice5Dtl T7 ON T6.DocNo = T7.ProjectImplementationDocNo ");
                    SQL.AppendLine("        INNER JOIN TblIncomingPaymentDtl T8 ON T8.InvoiceDocNo = T7.DocNo ");
                    SQL.AppendLine("            AND T8.InvoiceType = '5' ");
                    SQL.AppendLine("        INNER JOIN TblIncomingPaymentHdr T9 ON T8.DocNo = T9.DocNo ");
                    SQL.AppendLine("            AND T9.CancelInd = 'N' ");
                    SQL.AppendLine("        INNER JOIN TblVoucherHdr T10 ON T9.VoucherRequestDocNo = T10.VoucherRequestDocNo ");
                    SQL.AppendLine("            AND T10.CancelInd = 'N' ");
                    SQL.AppendLine("            AND LEFT(T10.DocDt, 4) = (@Yr - 1) ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("    GROUP BY T.SOCDocNo ");
                    SQL.AppendLine(") K ON D.DocNo = K.SOCDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T.SOCDocNo, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.Amt3) Amt3, SUM(T.Amt4) Amt4, ");
                    SQL.AppendLine("    SUM(T.Amt5) Amt5, SUM(T.Amt6) Amt6, SUM(T.Amt7) Amt7, SUM(T.Amt8) Amt8,  ");
                    SQL.AppendLine("    SUM(T.Amt9) Amt9, SUM(T.Amt10) Amt10, SUM(T.Amt11) Amt11, SUM(T.Amt12) Amt12 ");
                    SQL.AppendLine("    FROM  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT T5.SOCDocNo, If(SUBSTR(T9.DocDt, 5, 2) = '01', T9.Amt, 0.00) Amt1, ");
                    SQL.AppendLine("        If(SUBSTR(T9.DocDt, 5, 2) = '02', T9.Amt, 0.00) Amt2, If(SUBSTR(T9.DocDt, 5, 2) = '03', T9.Amt, 0.00) Amt3,  ");
                    SQL.AppendLine("        If(SUBSTR(T9.DocDt, 5, 2) = '04', T9.Amt, 0.00) Amt4, If(SUBSTR(T9.DocDt, 5, 2) = '05', T9.Amt, 0.00) Amt5,  ");
                    SQL.AppendLine("        If(SUBSTR(T9.DocDt, 5, 2) = '06', T9.Amt, 0.00) Amt6, If(SUBSTR(T9.DocDt, 5, 2) = '07', T9.Amt, 0.00) Amt7,  ");
                    SQL.AppendLine("        If(SUBSTR(T9.DocDt, 5, 2) = '08', T9.Amt, 0.00) Amt8, If(SUBSTR(T9.DocDt, 5, 2) = '09', T9.Amt, 0.00) Amt9,  ");
                    SQL.AppendLine("        If(SUBSTR(T9.DocDt, 5, 2) = '10', T9.Amt, 0.00) Amt10, If(SUBSTR(T9.DocDt, 5, 2) = '11', T9.Amt, 0.00) Amt11,  ");
                    SQL.AppendLine("        If(SUBSTR(T9.DocDt, 5, 2) = '12', T9.Amt, 0.00) Amt12 ");
                    SQL.AppendLine("        FROM TblSite T1 ");
                    SQL.AppendLine("        INNER JOIN TblLOPHdr T2 ON T1.SIteCode = T2.SiteCOde ");
                    SQL.AppendLine("            AND FIND_IN_SET(T1.SiteCode, @SiteCode) ");
                    SQL.AppendLine("            AND T2.CancelInd = 'Y' ");
                    SQL.AppendLine("            AND T2.STATUS = 'A' ");
                    SQL.AppendLine("        INNER JOIN TblBOQHdr T3 ON T2.DOcNo = T3.LOPDocNo ");
                    SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.DocNo = T4.BOQDocNo ");

                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) = @Yr ");

                    SQL.AppendLine("        INNER JOIN TblSOCOntractRevisionHdr T5 ON T4.DocNo = T5.SOCDocNo ");
                    SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T6 ON T5.DocNo = T6.SOContractDocNo ");
                    SQL.AppendLine("        INNER JOIN TblSalesInvoice5Dtl T7 ON T6.DocNo = T7.ProjectImplementationDocNo ");
                    SQL.AppendLine("        INNER JOIN TblIncomingPaymentDtl T8 ON T8.InvoiceDocNo = T7.DocNo ");
                    SQL.AppendLine("            AND T8.InvoiceType = '5' ");
                    SQL.AppendLine("        INNER JOIN TblIncomingPaymentHdr T9 ON T8.DocNo = T9.DocNo ");
                    SQL.AppendLine("            AND T9.CancelInd = 'N' ");
                    SQL.AppendLine("            AND LEFT(T9.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("    GROUP BY T.SOCDocNo ");
                    SQL.AppendLine(") L ON D.DocNo = L.SOCDocNo ");

                    if (i != (mProjectDesc.Count() - 1) || j != (mProjectType.Count() - 1))
                        SQL.AppendLine("    UNION ALL ");
                }
            }

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Order By T1.ProjectDesc, T1.ProjectDesc2; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOLead", mJointOperationTypeForKSOLead);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOMember", mJointOperationTypeForKSOMember);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForNonKSO", mJointOperationTypeForNonKSO);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ProjectDesc",
                    //1-5
                    "ProjectDesc2", "ProjectCode2", "ProjectName", "DocDt", "DeliveryDt", 
                    //6-10
                    "NumberOfMonth", "Duration", "ContractAmt", "ProductionAmt", "LastSettledAmt", 
                    //11-15
                    "PAmt1", "PAmt2", "PAmt3", "PAmt4", "PAmt5", 
                    //16-20
                    "PAmt6", "PAmt7", "PAmt8", "PAmt9", "PAmt10", 
                    //21-25
                    "PAmt11", "PAmt12", "VAmt", "IAmt1", "IAmt2", 
                    //26-30
                    "IAmt3", "IAmt4", "IAmt5", "IAmt6", "IAmt7", 
                    //31-34
                    "IAmt8", "IAmt9", "IAmt10", "IAmt11", "IAmt12", 
                    //36-40
                    "TotalResource", "ExclPPNPercentage", "RDAmt1", "RDAmt2", "RDAmt3", 
                    //41-45
                    "RDAmt4", "RDAmt5", "RDAmt6", "RDAmt7", "RDAmt8", 
                    //46-50
                    "RDAmt9", "RDAmt10", "RDAmt11", "RDAmt12", "Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionVsReceivable()
                        {
                            ProjectDesc = Sm.DrStr(dr, c[0]),
                            ProjectDesc2 = Sm.DrStr(dr, c[1]),
                            NAS = mNas.ToString(),
                            UP = mUP.ToString(),
                            ProjectCode2 = Sm.DrStr(dr, c[2]),
                            ProjectName = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            DeliveryDt = Sm.DrStr(dr, c[5]),
                            NumberOfMonth = Sm.DrDec(dr, c[6]),
                            Duration = Sm.DrStr(dr, c[7]),
                            ContractAmt = Sm.DrDec(dr, c[8]),
                            ProductionAmt = Sm.DrDec(dr, c[9]),
                            LastSettledAmt = Sm.DrDec(dr, c[10]),
                            PAmt1 = Sm.DrDec(dr, c[11]),
                            PAmt2 = Sm.DrDec(dr, c[12]),
                            PAmt3 = Sm.DrDec(dr, c[13]),
                            PTotal1 = Sm.DrDec(dr, c[11]) + Sm.DrDec(dr, c[12]) + Sm.DrDec(dr, c[13]),
                            PAmt4 = Sm.DrDec(dr, c[14]),
                            PAmt5 = Sm.DrDec(dr, c[15]),
                            PAmt6 = Sm.DrDec(dr, c[16]),
                            PTotal2 = Sm.DrDec(dr, c[14]) + Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16]),
                            PAmt7 = Sm.DrDec(dr, c[17]),
                            PAmt8 = Sm.DrDec(dr, c[18]),
                            PAmt9 = Sm.DrDec(dr, c[19]),
                            PTotal3 = Sm.DrDec(dr, c[17]) + Sm.DrDec(dr, c[18]) + Sm.DrDec(dr, c[19]),
                            PAmt10 = Sm.DrDec(dr, c[20]),
                            PAmt11 = Sm.DrDec(dr, c[21]),
                            PAmt12 = Sm.DrDec(dr, c[22]),
                            PTotal4 = Sm.DrDec(dr, c[20]) + Sm.DrDec(dr, c[21]) + Sm.DrDec(dr, c[22]),
                            TotalProduction = 0m,
                            TotalCoProduction = 0m,
                            OutstandingProduction = 0m,
                            VAmt = Sm.DrDec(dr, c[23]),
                            IAmt1 = Sm.DrDec(dr, c[24]),
                            IAmt2 = Sm.DrDec(dr, c[25]),
                            IAmt3 = Sm.DrDec(dr, c[26]),
                            ITotal1 = Sm.DrDec(dr, c[24]) + Sm.DrDec(dr, c[25]) + Sm.DrDec(dr, c[26]),
                            IAmt4 = Sm.DrDec(dr, c[27]),
                            IAmt5 = Sm.DrDec(dr, c[28]),
                            IAmt6 = Sm.DrDec(dr, c[29]),
                            ITotal2 = Sm.DrDec(dr, c[27]) + Sm.DrDec(dr, c[28]) + Sm.DrDec(dr, c[29]),
                            IAmt7 = Sm.DrDec(dr, c[30]),
                            IAmt8 = Sm.DrDec(dr, c[31]),
                            IAmt9 = Sm.DrDec(dr, c[32]),
                            ITotal3 = Sm.DrDec(dr, c[30]) + Sm.DrDec(dr, c[31]) + Sm.DrDec(dr, c[32]),
                            IAmt10 = Sm.DrDec(dr, c[33]),
                            IAmt11 = Sm.DrDec(dr, c[34]),
                            IAmt12 = Sm.DrDec(dr, c[35]),
                            ITotal4 = Sm.DrDec(dr, c[33]) + Sm.DrDec(dr, c[34]) + Sm.DrDec(dr, c[35]),
                            TotalIncoming = 0m,
                            TotalCoIncoming = 0m,
                            Receivable = 0m,
                            ReceivableRemarks = string.Empty,
                            TotalResource = Sm.DrDec(dr, c[36]),
                            ExclPPNPercentage = Sm.DrDec(dr, c[37]),
                            OutstandingProduction2 = 0m,
                            RDAmt1 = Sm.DrDec(dr, c[38]),
                            RDAmt2 = Sm.DrDec(dr, c[39]),
                            RDAmt3 = Sm.DrDec(dr, c[40]),
                            RDTotal1 = Sm.DrDec(dr, c[38]) + Sm.DrDec(dr, c[39]) + Sm.DrDec(dr, c[40]),
                            RDAmt4 = Sm.DrDec(dr, c[41]),
                            RDAmt5 = Sm.DrDec(dr, c[42]),
                            RDAmt6 = Sm.DrDec(dr, c[43]),
                            RDTotal2 = Sm.DrDec(dr, c[41]) + Sm.DrDec(dr, c[42]) + Sm.DrDec(dr, c[43]),
                            RDAmt7 = Sm.DrDec(dr, c[44]),
                            RDAmt8 = Sm.DrDec(dr, c[45]),
                            RDAmt9 = Sm.DrDec(dr, c[46]),
                            RDTotal3 = Sm.DrDec(dr, c[44]) + Sm.DrDec(dr, c[45]) + Sm.DrDec(dr, c[46]),
                            RDAmt10 = Sm.DrDec(dr, c[47]),
                            RDAmt11 = Sm.DrDec(dr, c[48]),
                            RDAmt12 = Sm.DrDec(dr, c[49]),
                            RDTotal4 = Sm.DrDec(dr, c[47]) + Sm.DrDec(dr, c[48]) + Sm.DrDec(dr, c[49]),
                            TotalRD = 0m,
                            TotalCoRD = 0m,
                            PercentageTarget = 0m,
                            PercentageTarget2 = 0m,
                            OutstandingRD = 0m,
                            PlanRemark = Sm.DrStr(dr, c[50]),
                        });

                        mUP += 1;
                        mNas += 1;
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ProductionVsReceivable> l)
        {
            foreach (var x in l)
            {
                x.TotalProduction = x.PTotal1 + x.PTotal2 + x.PTotal3 + x.PTotal4;
                x.TotalCoProduction = x.LastSettledAmt + x.TotalProduction;
                x.OutstandingProduction = x.ProductionAmt - x.TotalCoProduction;
                x.TotalIncoming = x.ITotal1 + x.ITotal2 + x.ITotal3 + x.ITotal4;
                x.TotalCoIncoming = x.VAmt + x.TotalIncoming;
                x.Receivable = x.ProductionAmt - x.TotalIncoming;
                x.OutstandingProduction2 = x.ProductionAmt - x.LastSettledAmt;
                x.TotalRD = x.RDTotal1 + x.RDTotal2 + x.RDTotal3 + x.RDTotal4;
                if (x.OutstandingProduction2 != 0m) x.PercentageTarget = (x.TotalRD / x.OutstandingProduction2) * 100m;
                x.TotalCoRD = x.LastSettledAmt + x.TotalRD;
                if (x.ProductionAmt != 0m) x.PercentageTarget2 = (x.TotalCoRD / x.ProductionAmt) * 100m;
                x.OutstandingRD = x.OutstandingProduction2 - x.TotalRD;
            }
        }

        private void Process3(ref List<ProductionVsReceivable> l)
        {
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.ProjectDesc;
                Grd1.Cells[Row, 2].Value = x.ProjectDesc2;
                Grd1.Cells[Row, 3].Value = x.NAS;
                Grd1.Cells[Row, 4].Value = x.UP;
                Grd1.Cells[Row, 5].Value = x.ProjectCode2;
                Grd1.Cells[Row, 6].Value = x.ProjectName;
                Grd1.Cells[Row, 7].Value = x.DocDt;
                Grd1.Cells[Row, 8].Value = x.DeliveryDt;
                Grd1.Cells[Row, 9].Value = x.NumberOfMonth;
                Grd1.Cells[Row, 10].Value = x.Duration;
                Grd1.Cells[Row, 11].Value = x.ContractAmt;
                Grd1.Cells[Row, 12].Value = x.ProductionAmt;
                Grd1.Cells[Row, 13].Value = x.LastSettledAmt;
                Grd1.Cells[Row, 14].Value = x.PAmt1;
                Grd1.Cells[Row, 15].Value = x.PAmt2;
                Grd1.Cells[Row, 16].Value = x.PAmt3;
                Grd1.Cells[Row, 17].Value = x.PTotal1;
                Grd1.Cells[Row, 18].Value = x.PAmt4;
                Grd1.Cells[Row, 19].Value = x.PAmt5;
                Grd1.Cells[Row, 20].Value = x.PAmt6;
                Grd1.Cells[Row, 21].Value = x.PTotal2;
                Grd1.Cells[Row, 22].Value = x.PAmt7;
                Grd1.Cells[Row, 23].Value = x.PAmt8;
                Grd1.Cells[Row, 24].Value = x.PAmt9;
                Grd1.Cells[Row, 25].Value = x.PTotal3;
                Grd1.Cells[Row, 26].Value = x.PAmt10;
                Grd1.Cells[Row, 27].Value = x.PAmt11;
                Grd1.Cells[Row, 28].Value = x.PAmt12;
                Grd1.Cells[Row, 29].Value = x.PTotal4;
                Grd1.Cells[Row, 30].Value = x.TotalProduction;
                Grd1.Cells[Row, 31].Value = x.TotalCoProduction;
                Grd1.Cells[Row, 32].Value = x.OutstandingProduction;
                Grd1.Cells[Row, 33].Value = x.VAmt;
                Grd1.Cells[Row, 34].Value = x.IAmt1;
                Grd1.Cells[Row, 35].Value = x.IAmt2;
                Grd1.Cells[Row, 36].Value = x.IAmt3;
                Grd1.Cells[Row, 37].Value = x.ITotal1;
                Grd1.Cells[Row, 38].Value = x.IAmt4;
                Grd1.Cells[Row, 39].Value = x.IAmt5;
                Grd1.Cells[Row, 40].Value = x.IAmt6;
                Grd1.Cells[Row, 41].Value = x.ITotal2;
                Grd1.Cells[Row, 42].Value = x.IAmt7;
                Grd1.Cells[Row, 43].Value = x.IAmt8;
                Grd1.Cells[Row, 44].Value = x.IAmt9;
                Grd1.Cells[Row, 45].Value = x.ITotal3;
                Grd1.Cells[Row, 46].Value = x.IAmt10;
                Grd1.Cells[Row, 47].Value = x.IAmt11;
                Grd1.Cells[Row, 48].Value = x.IAmt12;
                Grd1.Cells[Row, 49].Value = x.ITotal4;
                Grd1.Cells[Row, 50].Value = x.TotalIncoming;
                Grd1.Cells[Row, 51].Value = x.TotalCoIncoming;
                Grd1.Cells[Row, 52].Value = x.Receivable;
                Grd1.Cells[Row, 53].Value = x.ReceivableRemarks;
                Grd1.Cells[Row, 54].Value = x.TotalResource;
                Grd1.Cells[Row, 55].Value = x.ExclPPNPercentage;
                Grd1.Cells[Row, 56].Value = x.DocDt;
                Grd1.Cells[Row, 57].Value = x.DeliveryDt;
                Grd1.Cells[Row, 58].Value = x.NumberOfMonth;
                Grd1.Cells[Row, 59].Value = x.Duration;
                Grd1.Cells[Row, 60].Value = x.ProductionAmt;
                Grd1.Cells[Row, 61].Value = x.OutstandingProduction2;
                Grd1.Cells[Row, 62].Value = x.OutstandingProduction2;
                Grd1.Cells[Row, 63].Value = x.LastSettledAmt;
                Grd1.Cells[Row, 64].Value = x.RDAmt1;
                Grd1.Cells[Row, 65].Value = x.RDAmt2;
                Grd1.Cells[Row, 66].Value = x.RDAmt3;
                Grd1.Cells[Row, 67].Value = x.RDTotal1;
                Grd1.Cells[Row, 68].Value = x.RDAmt4;
                Grd1.Cells[Row, 69].Value = x.RDAmt5;
                Grd1.Cells[Row, 70].Value = x.RDAmt6;
                Grd1.Cells[Row, 71].Value = x.RDTotal2;
                Grd1.Cells[Row, 72].Value = x.RDAmt7;
                Grd1.Cells[Row, 73].Value = x.RDAmt8;
                Grd1.Cells[Row, 74].Value = x.RDAmt9;
                Grd1.Cells[Row, 75].Value = x.RDTotal1;
                Grd1.Cells[Row, 76].Value = x.RDAmt10;
                Grd1.Cells[Row, 77].Value = x.RDAmt11;
                Grd1.Cells[Row, 78].Value = x.RDAmt12;
                Grd1.Cells[Row, 79].Value = x.RDTotal4;
                Grd1.Cells[Row, 80].Value = x.TotalRD;
                Grd1.Cells[Row, 81].Value = x.PercentageTarget;
                Grd1.Cells[Row, 82].Value = x.TotalCoRD;
                Grd1.Cells[Row, 83].Value = x.PercentageTarget2;
                Grd1.Cells[Row, 84].Value = x.OutstandingRD;
                Grd1.Cells[Row, 85].Value = x.PlanRemark;

                Row += 1;
            }

            Grd1.GroupObject.Add(1);
            Grd1.GroupObject.Add(2);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, mColDec);
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Class

        private class ProductionVsReceivable
        {
            public string ProjectDesc { get; set; }
            public string ProjectDesc2 { get; set; }
            public string NAS { get; set; }
            public string UP { get; set; }
            public string ProjectCode2 { get; set; }
            public string ProjectName { get; set; }
            public string DocDt { get; set; }
            public string DeliveryDt { get; set; }
            public decimal NumberOfMonth { get; set; }
            public string Duration { get; set; }
            public decimal ContractAmt { get; set; }
            public decimal ProductionAmt { get; set; }
            public decimal LastSettledAmt { get; set; }
            public decimal PAmt1 { get; set; }
            public decimal PAmt2 { get; set; }
            public decimal PAmt3 { get; set; }
            public decimal PTotal1 { get; set; }
            public decimal PAmt4 { get; set; }
            public decimal PAmt5 { get; set; }
            public decimal PAmt6 { get; set; }
            public decimal PTotal2 { get; set; }
            public decimal PAmt7 { get; set; }
            public decimal PAmt8 { get; set; }
            public decimal PAmt9 { get; set; }
            public decimal PTotal3 { get; set; }
            public decimal PAmt10 { get; set; }
            public decimal PAmt11 { get; set; }
            public decimal PAmt12 { get; set; }
            public decimal PTotal4 { get; set; }
            public decimal TotalProduction { get; set; }
            public decimal TotalCoProduction { get; set; }
            public decimal OutstandingProduction { get; set; }
            public decimal VAmt { get; set; }
            public decimal IAmt1 { get; set; }
            public decimal IAmt2 { get; set; }
            public decimal IAmt3 { get; set; }
            public decimal ITotal1 { get; set; }
            public decimal IAmt4 { get; set; }
            public decimal IAmt5 { get; set; }
            public decimal IAmt6 { get; set; }
            public decimal ITotal2 { get; set; }
            public decimal IAmt7 { get; set; }
            public decimal IAmt8 { get; set; }
            public decimal IAmt9 { get; set; }
            public decimal ITotal3 { get; set; }
            public decimal IAmt10 { get; set; }
            public decimal IAmt11 { get; set; }
            public decimal IAmt12 { get; set; }
            public decimal ITotal4 { get; set; }
            public decimal TotalIncoming { get; set; }
            public decimal TotalCoIncoming { get; set; }
            public decimal Receivable { get; set; }
            public string ReceivableRemarks { get; set; }
            public decimal TotalResource { get; set; }
            public decimal ExclPPNPercentage { get; set; }
            public decimal OutstandingProduction2 { get; set; }
            public decimal RDAmt1 { get; set; }
            public decimal RDAmt2 { get; set; }
            public decimal RDAmt3 { get; set; }
            public decimal RDTotal1 { get; set; }
            public decimal RDAmt4 { get; set; }
            public decimal RDAmt5 { get; set; }
            public decimal RDAmt6 { get; set; }
            public decimal RDTotal2 { get; set; }
            public decimal RDAmt7 { get; set; }
            public decimal RDAmt8 { get; set; }
            public decimal RDAmt9 { get; set; }
            public decimal RDTotal3 { get; set; }
            public decimal RDAmt10 { get; set; }
            public decimal RDAmt11 { get; set; }
            public decimal RDAmt12 { get; set; }
            public decimal RDTotal4 { get; set; }
            public decimal TotalRD { get; set; }
            public decimal TotalCoRD { get; set; }
            public decimal PercentageTarget { get; set; }
            public decimal PercentageTarget2 { get; set; }
            public decimal OutstandingRD { get; set; }
            public string PlanRemark { get; set; }
        }

        private class AuctionBranchToSite
        {
            public string BranchCode { get; set; }
            public string SiteCode { get; set; }
        }

        private class SiteCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
            public int No2 { get; set; }
        }

        private class DummyCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
        }

        #endregion
    }
}
