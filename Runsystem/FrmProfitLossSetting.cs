﻿#region Update
/*
    30/11/2021 [HAR/PHT] menu baru  
    24/12/2021 [HAR/PHT] BUG : rename pilihan 
    19/01/2022 [VIN/PHT] BUG : tambah minsert pada validasi IsSettingCodeAlreadyProcess
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
#endregion

namespace RunSystem
{
    public partial class FrmProfitLossSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "", mMaxAccountCategory = "";
        internal bool mInsert = false;

        internal FrmProfitLossSettingFind FrmFind;

        #endregion

        #region Constructor

        public FrmProfitLossSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Profit Loss Setting";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                GetParameter();
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        //6
                        "Process"
                    },
                    new int[]
                    {
                        //0
                        20,
 
                        //1-5
                        20, 150, 200, 80, 150,
                        //6
                        80
                    }
                );
            Sm.GrdColCheck(Grd1,new int[] {6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5,6 });
            Sm.GrdColInvisible(Grd1, new int[] { 6 });
            #endregion

            #region Grid2

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Account#",
                        "Description",
                        "Type",
                        "Alias",
                        //6
                        "Process"
                    },
                    new int[]
                    {
                        //0
                        20,

                        20, 150, 200, 80, 150,

                        20
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 6 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 });
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd2, new int[] { 6 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtSettingCode, DteDocDt, TxtAcNo, TxtAcDesc, LueYr
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    ChkProfitLossInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = false;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      TxtSettingCode, DteDocDt, LueYr
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkProfitLossInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      DteDocDt, LueYr
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkProfitLossInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    break;
            }
        }

        private void GetParameter()
        {
            mMaxAccountCategory = Sm.GetParameter("MaxAccountCategory");
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtSettingCode, DteDocDt, TxtAcNo, TxtAcDesc, LueYr
            });
            ClearGrd();
            ChkActiveInd.Checked = false;
            ChkProfitLossInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProfitLossSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

            ChkActiveInd.Checked = true;
            mInsert = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSettingCode, "", false)) return;
            SetFormControl(mState.Edit);
            mInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {             
                   InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
               !Sm.IsLueEmpty(LueYr, "Year"))
            {
                Sm.FormShowDialog(new FrmProfitLossSettingDlg(this, "3"));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
               !Sm.IsLueEmpty(LueYr, "Year"))
            {
                Sm.FormShowDialog(new FrmProfitLossSettingDlg(this, "2"));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }



        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string SettingCode = string.Empty;

           
            SettingCode = TxtSettingCode.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProfitLossSettingHdr(SettingCode));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveProfitLossSettingDtl(SettingCode, Row));
            }
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveProfitLossSettingDtl2(SettingCode, Row));
            }
            Sm.ExecCommands(cml);
            ShowData(SettingCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSettingCode, "Setting Code", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtAcNo, "Current Earning Account", false) ||
                IsSettingCodeAlreadyProcess() ||
                IsDocNoAlreadyProcess() ||
                IsGrdEmpty();
        }

        private bool IsSettingCodeAlreadyProcess()
        {
            if (mInsert)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select SettingCode From TblProfitLossSettingHdr Where SettingCode=@SettingCode And ActInd='N'"
                };

                Sm.CmParam<String>(ref cm, "@SettingCode", TxtSettingCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "SettingCode already Exist.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDocNoAlreadyProcess()
        {
            if (mInsert)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select ProfitLossInd From TblProfitLossSettingHdr Where ProfitLossInd ='Y' And Yr=@Yr And ActInd = 'Y' ;"
                };

                //Sm.CmParam<String>(ref cm, "@SettingCode", TxtSettingCode.Text);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                if (Sm.IsDataExist(cm) && ChkProfitLossInd.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Profit Loss Indicator For this year already exist.");
                    return true;
                }
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 city.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveProfitLossSettingHdr(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProfitLossSettingHdr(SettingCode, Dt, ActInd, ProfitLossInd, Yr, CurrentEarningAcNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @Dt, @ActInd, @ProfitLossInd, @Yr, @CurrentEarningAcNo, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update Dt=@Dt, ActInd=@ActInd, ProfitLossInd=@ProfitLossInd, Yr=@Yr, CurrentEarningAcNo=@CurrentEarningAcNo, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblProfitLossSettingDtl Where SettingCode=@SettingCode; ");
            SQL.AppendLine("Delete From TblProfitLossSettingDtl2 Where SettingCode=@SettingCode; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProfitLossInd", ChkProfitLossInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CurrentEarningAcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveProfitLossSettingDtl(string SettingCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProfitLossSettingDtl(SettingCode, DNo, AcNo, ProcessInd,  CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @DNo, @AcNo, @ProcessInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdBool(Grd1, Row, 6)? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProfitLossSettingDtl2(string SettingCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProfitLossSettingDtl2(SettingCode, DNo, AcNo, ProcessInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SettingCode, @DNo, @AcNo, @ProcessInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdBool(Grd2, Row, 6) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
       
        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditProfitLossSettingActive(TxtSettingCode.Text);
            ShowData(TxtSettingCode.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSettingCode, "Setting Code", false) ||
                IsActIndEditedAlready();
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select SettingCode From TblProfitLossSettingHdr Where SettingCode=@SettingCode And ActInd='N' "
            };

            Sm.CmParam<String>(ref cm, "@SettingCode", TxtSettingCode.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This setting already non-active.");
                return true;
            }
            return false;
        }

        private void EditProfitLossSettingActive(string SettingCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblProfitLossSettingHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where SettingCode=@SettingCode ;"
            };
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string SettingCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowProfitlossSettingHdr(SettingCode);
                ShowProfitLossSettingDtl(SettingCode);
                ShowProfitLossSettingDtl2(SettingCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProfitlossSettingHdr(string SettingCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.SettingCode, A.Dt, A.Yr, A.ActInd, A.ProfitLossInd, A.CurrentEarningAcno, B.AcDesc " +
                    "From TblProfitLossSettingHdr A " +
                    "Inner Join Tblcoa B On A.CurrentEarningAcno = B.Acno " +
                    "Where A.SettingCode=@SettingCode",
                    new string[]
                    { 
                        //0
                        "SettingCode", 

                        //1-5
                        "Dt", "Yr", "ActInd", "ProfitLossInd", "CurrentEarningAcno", "AcDesc",  

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSettingCode.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueYr(LueYr, string.Empty);
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        ChkProfitLossInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowProfitLossSettingDtl(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Acno, B.AcDesc, B.AcType, B.Alias, A.Processind ");
            SQL.AppendLine("From tblProfitLossSettingdtl A ");
            SQL.AppendLine("Left Join TblCOA B On A.Acno = B.Acno ");
            SQL.AppendLine("Where A.SettingCode=@SettingCode Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "AcType", "Alias", "ProcessInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowProfitLossSettingDtl2(string SettingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Acno, B.AcDesc, B.AcType, B.Alias, A.Processind ");
            SQL.AppendLine("From tblProfitLossSettingdtl2 A ");
            SQL.AppendLine("Left Join TblCOA B On A.Acno = B.Acno ");
            SQL.AppendLine("Where A.SettingCode=@SettingCode Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SettingCode", SettingCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "AcType", "Alias", "ProcessInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd2, dr, c, Row, 6, 5);

                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }


        #endregion

        #endregion

        #region Event
        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProfitLossSettingDlg(this, "1"));
        }
        #endregion
    }
}
