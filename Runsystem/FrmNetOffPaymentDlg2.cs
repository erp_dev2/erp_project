﻿#region Update
/*
    19/12/2022 [ICA/MNET] new Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmNetOffPaymentDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty, mCtCode = string.Empty, mDocTypeSLI = string.Empty;
        private FrmNetOffPayment mFrmParent;

        #endregion

        #region Constructor

        public FrmNetOffPaymentDlg2(FrmNetOffPayment FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Sales Invoice/ Sales Return Invoice";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Document#",
                    "Date",
                    "",
                    "Type",

                    //6-10
                    "Type",
                    "Currency",
                    "Outstanding"+Environment.NewLine+"Amount",
                    "Due Date",
                    "Local Document",

                    //11-12
                    "CBD",
                    "DO's Remark",
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 130, 80, 20, 0, 
                    
                    //6-10
                    150, 60, 130, 100, 100,

                    //11-15
                    0, 400
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 11 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 11 }, false);
            if (!mFrmParent.mIsIncomingPaymentShowDORemark)
                Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From (");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '1' As InvoiceType, ");
            SQL.AppendLine("    case  ");
            SQL.AppendLine("        when A.DocNo NOT LIKE '%SLIDO%' then 'Sales Invoice'  ");
            SQL.AppendLine("        ELSE 'Sales Invoice Based on DO (Based On Delivery Request-Sales Contract)' ");
            SQL.AppendLine("    end As InvoiceTypeDesc,  ");
            SQL.AppendLine("    A.CurCode, A.Amt-IfNull(B.Amt, 0)-IfNull(C.Amt, 0)-IfNull(E.ARSAmt, 0) As Amt, A.DueDt, A.LocalDocNo, A.CBDInd, F.DOCtRemark ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("            And T1.CtCode=@CtCode ");
            SQL.AppendLine("            Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");

            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT T2.InvoiceDocNo AS DocNo, SUM(T2.Amt) Amt ");
            SQL.AppendLine("        FROM TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr T4 On T2.InvoiceDocNo=T4.DocNo AND T4.CtCode=@CtCode And IFNULL(T4.ProcessInd, 'O')<>'F'  ");
            SQL.AppendLine("        WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("            AND T3.CtCode = @CtCode ");
            SQL.AppendLine("        GROUP BY T2.InvoiceDocNo ");
            SQL.AppendLine("    ) C ON A.DocNo = C.DocNo  ");

            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select  A.SalesInvoiceDocNo, Sum(A.Amt) As ARSAmt ");
            SQL.AppendLine("        From TblARSHdr A  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("        Group by A.SalesInvoiceDocNo ");
            SQL.AppendLine("    ) E On A.DocNo=E.SalesInvoiceDocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T3.Remark Order By T2.DNo Separator ' ') As DOCtRemark ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T3 On T2.DOCtDocNo=T3.DocNo And T3.Remark Is Not Null ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    And T1.CtCode=@CtCode ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine("    ) F On A.DocNo=F.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    And A.CtCode = @CtCode ");

            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null ");
                SQL.AppendLine("    Or (A.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '2' As InvoiceType, 'Sales Return Invoice' As InvoiceTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.TotalAmt-IfNull(B.Amt, 0)-IfNull(C.Amt, 0) As Amt, Null As DueDt, Null As LocalDocNo, 'N' As CBDInd, Null As DOCtRemark ");
            SQL.AppendLine("    From TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.IncomingPaymentInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("            And T1.CtCode=@CtCode ");
            SQL.AppendLine("            Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");

            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT T2.InvoiceDocNo AS DocNo, SUM(T2.Amt) Amt ");
            SQL.AppendLine("        FROM TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr T4 On T2.InvoiceDocNo=T4.DocNo And T4.CtCode=@CtCode And IfNull(T4.IncomingPaymentInd, 'O')<>'F' ");
            SQL.AppendLine("        WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("            AND T3.CtCode = @CtCode ");
            SQL.AppendLine("        GROUP BY T2.InvoiceDocNo ");
            SQL.AppendLine("    ) C ON A.DocNo = C.DocNo  ");

            SQL.AppendLine("    Where A.IncomingPaymentInd<>'F' And A.CancelInd='N' ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");

            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null ");
                SQL.AppendLine("    Or (A.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '3' As InvoiceType, 'Sales Invoice 2' As InvoiceTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.Amt-IfNull(B.Amt, 0)-IfNull(E.ARSAmt, 0) As Amt, A.DueDt, A.LocalDocNo, 'N' CBDInd, Null As DOCtRemark ");
            SQL.AppendLine("    From TblSalesInvoice2Hdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("        Inner Join TblSalesInvoice2Hdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        And T1.CtCode=@CtCode ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");

            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT T2.InvoiceDocNo AS DocNo, SUM(T2.Amt) Amt ");
            SQL.AppendLine("        FROM TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("        Inner Join TblSalesInvoice2Hdr T4 On T2.InvoiceDocNo=T4.DocNo AND T4.CtCode=@CtCode And IFNULL(T4.ProcessInd, 'O')<>'F'  ");
            SQL.AppendLine("        WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("            AND T3.CtCode = @CtCode ");
            SQL.AppendLine("        GROUP BY T2.InvoiceDocNo ");
            SQL.AppendLine("    ) C ON A.DocNo = C.DocNo  ");

            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select  A.SalesInvoiceDocNo, Sum(A.Amt) As ARSAmt ");
            SQL.AppendLine("        From TblARSHdr A  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("        Group by A.SalesInvoiceDocNo ");
            SQL.AppendLine("    ) E On A.DocNo=E.SalesInvoiceDocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");

            SQL.AppendLine("    Union ALL ");

            SQL.AppendLine("    Select A.DocNo, A.DocDt, '5' As InvoiceType, 'Sales project' As InvoiceTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.Amt-IfNull(B.Amt, 0.00)-IfNull(D.ARSAmt, 0.00) As Amt, A.DueDt, A.LocalDocNo, 'N' CBDInd, Null As DOCtRemark ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct T2.ProjectImplementationDocNo, T1.DocNo, T1.DocDt, T1.CurCode,  ");
            SQL.AppendLine("        T1.Amt, T1.DueDt, T1.LocalDocNo, T1.CancelInd, T1.ProcessInd, T1.CtCode ");
            SQL.AppendLine("        From TblSalesInvoice5Hdr T1 ");
            SQL.AppendLine("        Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("         And T1.CancelInd = 'N' ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='5' ");
            SQL.AppendLine("        Inner Join TblSalesInvoice5Hdr T3 On T2.InvoiceDocNo=T3.DocNo  And T3.CtCode=@CtCode And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("            And T1.CtCode=@CtCode ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");

            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT T2.InvoiceDocNo AS DocNo, SUM(T2.Amt) Amt ");
            SQL.AppendLine("        FROM TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("        INNER JOIN TblNetOffPaymentDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        INNER JOIN TblPartnerEntity T3 ON T1.PartnerEntityCode = T3.PartnerEntityCode ");
            SQL.AppendLine("        Inner Join TblSalesInvoice5Hdr T4 On T2.InvoiceDocNo=T4.DocNo AND T4.CtCode=@CtCode And IFNULL(T4.ProcessInd, 'O')<>'F'  ");
            SQL.AppendLine("        WHERE T1.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(T1.Status, 'O') <> 'C' ");
            SQL.AppendLine("            AND T3.CtCode = @CtCode ");
            SQL.AppendLine("        GROUP BY T2.InvoiceDocNo ");
            SQL.AppendLine("    ) C ON A.DocNo = C.DocNo  ");

            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select  A.SalesInvoiceDocNo, Sum(A.Amt) As ARSAmt  ");
            SQL.AppendLine("        From TblARSHdr A   ");
            SQL.AppendLine("        Where A.CancelInd = 'N'  ");
            SQL.AppendLine("        Group by A.SalesInvoiceDocNo  ");
            SQL.AppendLine("    ) D On A.DocNo=D.SalesInvoiceDocNo  ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(A.ProcessInd, 'O')<>'F' And A.CtCode=@CtCode ");

            SQL.AppendLine(") T Where Amt>0 And Locate(Concat('##', T.DocNo, T.InvoiceType, '##'), @SelectedInvoice)<1 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SelectedInvoice", mFrmParent.GetSelectedSalesInvoice());
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@Now", Sm.GetValue("Select Concat(curdate(), ' ', Curtime()) "));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By InvoiceType, DocDt, DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "InvoiceType", "InvoiceTypeDesc",  "CurCode", "Amt", 
                        //6-10
                        "DueDt", "LocalDocNo", "CBDInd", "DOCtRemark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);

                        mFrmParent.Grd1.Cells[Row1, 11].Value = null;

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9 });
                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12 });
                        mFrmParent.ComputeSLIAmt();
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 sales invoice or sales return invoice document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 5);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key,
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 5)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    mDocTypeSLI = Sm.GetValue("SELECT doctype FROM tblsalesinvoicedtl WHERE docno = @Param limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                    if (mDocTypeSLI == "1")
                    {
                        var f1 = new FrmSalesInvoice(mFrmParent.mMenuCode);
                        f1.Tag = mFrmParent.mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                    else
                    {
                        string SLIDO = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        if (!SLIDO.Contains("SLIDO"))
                        {
                            var f1 = new FrmSalesInvoice3(mFrmParent.mMenuCode);
                            f1.Tag = mFrmParent.mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                            f1.ShowDialog();
                        }
                        else
                        {
                            var f1 = new FrmSalesInvoice8(mFrmParent.mMenuCode);
                            f1.Tag = mFrmParent.mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                            f1.ShowDialog();
                        }
                    }
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "5"))
                {
                    var f1 = new FrmSalesInvoice5(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)

            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    mDocTypeSLI = Sm.GetValue("SELECT doctype FROM tblsalesinvoicedtl WHERE docno = @Param limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                    if (mDocTypeSLI == "1")
                    {
                        var f1 = new FrmSalesInvoice(mFrmParent.mMenuCode);
                        f1.Tag = mFrmParent.mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                    else
                    {
                        string SLIDO = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        if (!SLIDO.Contains("SLIDO"))
                        {
                            var f1 = new FrmSalesInvoice3(mFrmParent.mMenuCode);
                            f1.Tag = mFrmParent.mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                            f1.ShowDialog();
                        }
                        else
                        {
                            var f1 = new FrmSalesInvoice8(mFrmParent.mMenuCode);
                            f1.Tag = mFrmParent.mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                            f1.ShowDialog();
                        }

                    }
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "5"))
                {
                    var f1 = new FrmSalesInvoice5(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        #endregion

        #endregion

    }
}
