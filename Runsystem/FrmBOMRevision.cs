﻿#region Update
/*
    20/11/2019 [DITA/IMS] new apps
    27/11/2019 [WED/IMS] tambah local code dan specification
    19/05/2020 [IBL/IMS] Revisi BOM tidak boleh menghapus item, bisa mengurangi quantity menjadi 0, tanda untuk material yang direvisi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOMRevision : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = String.Empty, CreateDt = String.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmBOMRevisionFind FrmFind;

        internal List<BOMDtl> mlBOMDtl = new List<BOMDtl>();

        #endregion

        #region Constructor

        public FrmBOMRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "BOM Revision";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Material Result Indicator",
                    "BOM#",
                    "BOM DNo",
                    "Item Code",
                    "Item Name",
                    
                    //6-9
                    "Quantity",
                    "Item's Local Code",
                    "Specification",
                    "ChangeInd"
                   
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    200, 150, 100,100, 200, 

                    //6-8
                    80, 120, 120, 50
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 9 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 7, 8, 9 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtBOQDocNo, TxtProjectCode,
                        TxtProjectName, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    //Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    BtnCopyData.Enabled = BtnBOQDocNo.Enabled = BtnBOQDocNo2.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    //Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 6 });
                    BtnCopyData.Enabled = BtnBOQDocNo.Enabled = BtnBOQDocNo2.Enabled= true;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtBOQDocNo, TxtProjectCode, TxtProjectName, MeeRemark
            });
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBOMRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ", false))
            {
                var f = new FrmBOMRevisionDlg3(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(e.ColIndex == 0)
                    Sm.FormShowDialog(new FrmBOMRevisionDlg2(this));
            }
        }
        
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmBOMRevisionDlg2(this));
                }
                if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex) && BtnSave.Enabled)
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                //Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }
      
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Concat(TxtBOQDocNo.Text, '/', GenerateDocNo());

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBOMRevisionHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveBOMRevisionDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }
       
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string StartDt = string.Empty, EndDt = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "BOM# is empty.")
                    ) return true;

            }
            return false;
        }

        private MySqlCommand SaveBOMRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOMRevisionHdr(DocNo, DocDt, BOQDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @BOQDocNo, @Remark, @CreateBy, CurrentDateTime()); ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBOMRevisionDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBOMRevisionDtl(DocNo, DNo, MaterialResultInd, BOMDocNo, BOMDNo, Qty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @MaterialResultInd, @BOMDocNo, @BOMDNo, @Qty, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@MaterialResultInd", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowBOMRevisionHdr(DocNo);
                ShowBOMRevisionDtl(DocNo);
                CompareBOMDataRevision(DocNo, CreateDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

       
        private void ShowBOMRevisionHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.BOQDocNo, A.Remark, IFNULL(E.ProjectCode, C.ProjectCode2) AS ProjectCode, IFNULL(E.ProjectName, D.ProjectName) AS ProjectName, A.CreateDt ");
            SQL.AppendLine("From TblBOMRevisionHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B ON A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr D ON B.LOPDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup E ON D.PGCode = E.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "BOQDocNo", "ProjectCode", "ProjectName", "Remark",

                        //6
                        "CreateDt"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[4]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        CreateDt = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowBOMRevisionDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Y' ChangeInd, T.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.DNo, A.MaterialResultInd, A.BOMDocNo, A.BOMDNo, A.Qty, C.ItCode, C.ItName, C.ItCodeInternal, C.Specification ");
            SQL.AppendLine("From TblBOMRevisionDtl A ");
            SQL.AppendLine("Inner Join  TblBOMDtl B ON A.BOMDocNo = B.DocNo AND A.BOMDNo = B.DNo AND A.MaterialResultInd = '1' ");
            SQL.AppendLine("Inner Join  TblItem C ON B.DocCode = C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DNo, A.MaterialResultInd, A.BOMDocNo, A.BOMDNo, A.Qty, C.ItCode, C.ItName, C.ItCodeInternal, C.Specification ");
            SQL.AppendLine("From TblBOMRevisionDtl A ");
            SQL.AppendLine("Inner Join TblBOMDtl2 B ON A.BOMDocNo = B.DocNo AND A.BOMDNo = B.DNo AND A.MaterialResultInd = '2' ");
            SQL.AppendLine("Inner Join TblItem C ON B.ItCode = C.ItCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine(")T Order By T.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "MaterialResultInd",
                    //1-5
                    "BOMDocNo", "BOMDNo","ItCode", "ItName", "Qty",
                    //6-7
                    "ItCodeInternal", "Specification", "ChangeInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void CompareBOMDataRevision(string DocNo, string CreateDt)
        {
            string BOMDocNo = GetBOMDocNo(DocNo, CreateDt);
            List<BOMDtl> BOMDtlList = GetBOMDataBeforeRev(BOMDocNo);

            if(BOMDtlList.Count != 0)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    foreach (var li in BOMDtlList)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 4) == li.ItCode)
                        {
                            if (Sm.GetGrdDec(Grd1, i, 6) == li.Qty)
                            {
                                Grd1.Cells[i, 9].Value = 'N';
                            }
                        }
                    }
                }


                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 9) == "Y")
                    {
                        Grd1.Rows[i].BackColor = Color.Green;
                    }
                }
            }
        }

        internal string GetBOMDocNo(string DocNo, string CreateDt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string BOMDocNo = string.Empty;

            SQL.AppendLine("SELECT DocNo FROM TblBOMRevisionHdr ");
            SQL.AppendLine("WHERE DocNo <> @DocNo ");
            SQL.AppendLine("AND BOQDocNo = @BOQDocNo ");
            SQL.AppendLine("AND CreateDt < @DocDt ");
            SQL.AppendLine("ORDER BY CreateDt DESC "); 
            SQL.AppendLine("LIMIT 1; ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm.Connection = cn2;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DocDt", CreateDt);

                var dr2 = cm.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                         //0
                        "DocNo",                      
                    });
                if (dr2.HasRows)
                {
                    if (dr2.Read())
                    {
                        BOMDocNo = Sm.DrStr(dr2, c2[0]);
                    }
                }
                dr2.Close();
            }
            
            return BOMDocNo;
        }

        internal List<BOMDtl> GetBOMDataBeforeRev(string BOMDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            mlBOMDtl.Clear();

            SQL.AppendLine("Select A.Qty, C.ItCode ");
            SQL.AppendLine("From TblBOMRevisionDtl A ");
            SQL.AppendLine("Inner Join  TblBOMDtl B ON A.BOMDocNo = B.DocNo AND A.BOMDNo = B.DNo ");
            SQL.AppendLine("Inner Join  TblItem C ON B.DocCode = C.ItCode ");
            SQL.AppendLine("WHERE A.DocNo = @BOMDocNo ");
            SQL.AppendLine("ORDER BY A.DNo");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm.Connection = cn2;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@BOMDocNo", BOMDocNo);
                var dr2 = cm.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ItCode",

                         //1
                         "Qty",                         
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        mlBOMDtl.Add(new BOMDtl()
                        {
                            ItCode = Sm.DrStr(dr2, c2[0]),
                            Qty = Sm.DrDec(dr2, c2[1]),
                        });
                    }
                }
                dr2.Close();
            }

            return mlBOMDtl;
        }
        
        #endregion

        #region Additional Method

        internal string GetSelectedBOMData()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowBOMData(string BOQDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.MaterialResultInd, B.BOMDocNo, B.BOMDNo, C.ItCode, D.ItName, B.Qty, D.ItCodeInternal, D.Specification ");
            SQL.AppendLine("From (Select Max(docno) docno From TblBOMRevisionHdr Where boqdocno = @DocNo) A ");
            SQL.AppendLine("Inner Join TblBOMRevisionDtl B On A.DocNo = B.DocNo And B.MaterialResultInd = '2' ");
            SQL.AppendLine("Inner Join TblBOMDtl2 C On B.BOMDocNo = C.DocNo And B.BOMDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.MaterialResultInd, B.BOMDocNo, B.BOMDNo, C.DocCode ItCode, D.ItName, B.Qty, D.ItCodeInternal, D.Specification ");
            SQL.AppendLine("From (Select Max(docno) docno from TblBOMRevisionHdr Where boqdocno = @DocNo) A ");
            SQL.AppendLine("Inner Join TblBOMRevisionDtl B On A.DocNo = B.DocNo And B.MaterialResultInd = '1' ");
            SQL.AppendLine("Inner Join TblBOMDtl C On B.BOMDocNo = C.DocNo And B.BOMDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.DocCode = D.ItCode ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", BOQDocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "MaterialResultInd", 
                    //1-5
                    "BOMDocNo", "BOMDNo","ItCode", "ItName", "Qty",
                    //6-7
                    "ItCodeInternal", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowBOMData2(string BOMRevDocNo)
        {
            Sm.ClearGrd(Grd1, false);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.MaterialResultInd, B.BOMDocNo, B.BOMDNo, C.ItCode, D.ItName, B.Qty, D.ItCodeInternal, D.Specification ");
            SQL.AppendLine("From TblBOMRevisionHdr A ");
            SQL.AppendLine("Inner Join TblBOMRevisionDtl B On A.DocNo = B.DocNo And B.MaterialResultInd = '2' And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOMDtl2 C On B.BOMDocNo = C.DocNo And B.BOMDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.MaterialResultInd, B.BOMDocNo, B.BOMDNo, C.DocCode ItCode, D.ItName, B.Qty, D.ItCodeInternal, D.Specification ");
            SQL.AppendLine("From TblBOMRevisionHdr A ");
            SQL.AppendLine("Inner Join TblBOMRevisionDtl B On A.DocNo = B.DocNo And B.MaterialResultInd = '1' And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOMDtl C On B.BOMDocNo = C.DocNo And B.BOMDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.DocCode = D.ItCode ");
            SQL.AppendLine("; ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", BOMRevDocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "MaterialResultInd", 
                    //1-5
                    "BOMDocNo", "BOMDNo","ItCode", "ItName", "Qty",
                    //6-7
                    "ItCodeInternal", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblBOMRevisionHdr  ");
            SQL.AppendLine("       Where BOQDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text);
        }
       
        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmBOMRevisionDlg(this));
            }
        }

        private void BtnBOQDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false))
            {
                var f = new FrmBOQ2(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmBOQ2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBOQDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        internal class BOMDtl
        {
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
        }

        internal class BOMDtl2
        {
            public string ItCode2 { get; set; }
            public decimal Qty2 { get; set; }
        }


        #endregion
    }
}
