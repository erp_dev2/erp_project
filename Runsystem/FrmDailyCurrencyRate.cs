﻿#region Update
/*
    13/03/2020 [IBL/KBN] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDailyCurrencyRate : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmDailyCurrencyRateFind FrmFind;

        #endregion

        #region Constructor

        public FrmDailyCurrencyRate(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            Sl.SetLueCurCode(ref LueCurCode1);
            Sl.SetLueCurCode(ref LueCurCode2);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteRateDt, LueCurCode1, LueCurCode2, TxtAmt
                    }, true);
                    DteRateDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteRateDt, LueCurCode1, LueCurCode2, TxtAmt
                    }, false);
                    DteRateDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtAmt, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteRateDt, LueCurCode1, LueCurCode2
                    }, true);
                    TxtAmt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                DteRateDt, LueCurCode1, LueCurCode2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt 
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDailyCurrencyRateFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            DteRateDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteRateDt, "")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteRateDt, "") || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblDailyCurrencyRate Where RateDt=@RateDt And CurCode1=@CurCode1 And CurCode2=@CurCode2" };
                Sm.CmParam<String>(ref cm, "@RateDt", Sm.GetDte(DteRateDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@CurCode1", Sm.GetLue(LueCurCode1));
                Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode2));
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblDailyCurrencyRate(RateDt, CurCode1, CurCode2, Amt, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@RateDt, @CurCode1, @CurCode2, @Amt, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@RateDt", Sm.GetDte(DteRateDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@CurCode1", Sm.GetLue(LueCurCode1));
                Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode2));
                Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(Sm.GetDte(DteRateDt).Substring(0, 8), Sm.GetLue(LueCurCode1), Sm.GetLue(LueCurCode2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string RateDt, string CurCode1, string CurCode2)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@RateDt", RateDt);
                Sm.CmParam<String>(ref cm, "@CurCode1", CurCode1);
                Sm.CmParam<String>(ref cm, "@CurCode2", CurCode2);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select RateDt, CurCode1, CurCode2, Amt From TblDailyCurrencyRate Where RateDt=@RateDt And CurCode1=@CurCode1 And CurCode2=@CurCode2",
                        new string[] 
                        {
                            "RateDt", "CurCode1", "CurCode2", "Amt"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            Sm.SetDte(DteRateDt, Sm.DrStr(dr, c[0]));
                            Sm.SetLue(LueCurCode1, Sm.DrStr(dr, c[1]));
                            Sm.SetLue(LueCurCode2, Sm.DrStr(dr, c[2]));
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteRateDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode1, "Currency (From)") ||
                Sm.IsLueEmpty(LueCurCode2, "Currency (To)") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                IsDailyCurrencyRateExisted();
        }

        private bool IsDailyCurrencyRateExisted()
        {
            if (!DteRateDt.Properties.ReadOnly && Sm.IsDataExist("Select RateDt From TblDailyCurrencyRate Where RateDt='" + Sm.GetDte(DteRateDt) + "' And CurCode1='" + Sm.GetLue(LueCurCode1) + "' And CurCode2='" + Sm.GetLue(LueCurCode2) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Currency rate Data already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        
        private void LueCurCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode1, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCurCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode2, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        #endregion

        #endregion

    }
}
