﻿#region Update
/*
    09/06/2017 [TKG] tambah pilihan dari POS
    03/09/2017 [TKG] tambah informasi incoming payment# dan date
    12/11/2017 [TKG] VR Tax bisa in atau out berdasarkan debit/creditnya.
    28/11/2017 [TKG] perubahan perhitungan vr tax
    03/06/2018 [TKG] bug saat proses sales return
    01/08/2018 [WED] ubah ambil TaxInvoiceDt Sales Invoice
    07/08/2019 [TKG] tambah fasilitas select multiple record
    15/09/2020 [IBL/YK] Bisa menarik sales invoice for project
    31/05/2020 [HAR/IOK] VR PPN saat milih document keluaran ada error  filter
    03/02/2022 [BRI/PHT] tambah filter multi profit center berdasarkan param IsFicoUseMultiProfitCenterFilter
    10/02/2022 [BRI/PHT] multi profit center get child
    09/08/2022 [SET/PHT] Penyesuaian source amount tax
    22/09/2022 [SET/IOK] penyesuaian source list document berdasar param IsVRVATFilteredByTaxGroup
    28/09/2022 [ICA/PHT] menambah pemanggilan method setNO saat choose data
    05/10/2022 [TYO/PRODUCT] bisa menarik lebih dari satu tax dari purchase invoice, source amount bisa dari detail berdasarkan param SalesInvoiceTaxCalculationFormula
    23/11/2022 [TYO/PRODUCT] Penyesuaian Query SetSQL
    19/01/2023 [TYO/IOK] Bug show data saat if (DocType == "1") A.TaxCode diubah B.TaxCode
    19/01/2023 [TYO/IOK] Bug show data saat if (DocType == "1") tambah mDocTitle != "IOK" karena SLI IOK tidak pakai tax
    25/01/2023 [TYO/IOK] ketika show data DocType == "1" menambah validasi cek DocNonya saja berdasarkan parameter !IsVRVATFilteredByTaxGroups
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPPNDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private FrmVoucherRequestPPN mFrmParent;
        private string 
            mDocNo = string.Empty,
            mTaxGrpCode = string.Empty;
        private bool
            mIsAllProfitCenterSelected = false;

        #endregion

        #region Constructor

        public FrmVoucherRequestPPNDlg2(FrmVoucherRequestPPN FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Document";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueCtCode(ref LueCtCode);
                mDocNo = mFrmParent.TxtDocNo.Text;
                mTaxGrpCode = Sm.GetLue(mFrmParent.LueTaxGrpCode);
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                else
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Type",
                        "Type",
                        "Document#", 
                        "",

                        //6-10
                        "Date",
                        "Customer",
                        "SO/" + Environment.NewLine + "Incoming Payment",
                        "Tax Invoice#",
                        "Tax Invoice Date",
                        
                        //11-15
                        "Amount",
                        "Rate",
                        "Amount" + Environment.NewLine + "(" + mFrmParent.mMainCurCode + ")",
                        "Remark",
                        "Currency",

                        //16-18
                        "Tax Code",
                        "Tax Invoice Name",
                        "Tax Invoice Rate"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 180, 130, 20,

                        //6-10
                        80, 180, 400, 130, 100,

                        //11-15
                        120, 120, 120, 400, 60,

                        //16-18
                        150, 250, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 16 }, false);
            Grd1.Cols[17].Move(10);
            Grd1.Cols[18].Move(11);
            Grd1.Cols[15].Move(12);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter, string Filter2, string Filter3, string Filter4, string Filter5, string Filter6, string Filter7, string Filter8, string Filter9)
        {
            var SQL = new StringBuilder();

            #region Old Code
                //SQL.AppendLine("Select '1' As DocType, 'Sales Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                //SQL.AppendLine("B.CtName, C.SODocNo, A.TaxInvDocument As TaxInvoiceNo, IfNull(A.TaxInvDt, A.DocDt) As TaxInvoiceDt, ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("A.TotalTax ");
                //else
                //{
                //    if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1")
                //    {
                //        SQL.AppendLine("    If(IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt1, 0.00) + ");
                //        SQL.AppendLine("    If(IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt2, 0.00) + ");
                //        SQL.AppendLine("    If(IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt3, 0.00) ");
                //    }
                //    else if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                //    {
                //        SQL.AppendLine("    If((IfNull(A.TaxCode1, '') AND F.TaxInd1 = 'Y') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), F.TaxAmt1, 0.00) + ");
                //        SQL.AppendLine("    If((IfNull(A.TaxCode2, '') AND F.TaxInd2 = 'Y') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), F.TaxAmt2, 0.00) + ");
                //        SQL.AppendLine("    If((IfNull(A.TaxCode3, '') AND F.TaxInd3 = 'Y') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), F.TaxAmt3, 0.00) ");
                //    }
                //}
                //SQL.AppendLine("As Amt, ");
                //SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                //SQL.AppendLine("IfNull(( ");
                //SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                //SQL.AppendLine("    Where RateDt<=A.DocDt ");
                //SQL.AppendLine("    And CurCode1=A.CurCode ");
                //SQL.AppendLine("    And CurCode2=@MainCurCode ");
                //SQL.AppendLine("    Order By RateDt Desc ");
                //SQL.AppendLine("    Limit 1 ");
                //SQL.AppendLine("), 0.00) ");
                //SQL.AppendLine("End As ExcRate, ");
                //SQL.AppendLine("Null As Remark, A.CurCode ");
                //SQL.AppendLine("From TblSalesInvoiceHdr A ");
                //SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.SODocNo, ");
                //SQL.AppendLine("    Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                //SQL.AppendLine("    ) As SODocNo From ( ");
                //SQL.AppendLine("    Select T.DocNo, ");
                //SQL.AppendLine("    Group_Concat(Distinct T.SODocNo Order By T.SODocNo ASC Separator ', ') As SODocNo ");
                //SQL.AppendLine("    From ( ");
                //SQL.AppendLine("        Select Distinct T1.DocNo, T5.SODocNo ");
                //SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                //SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
                //SQL.AppendLine("        Inner Join TblDRDtl T5 On T3.DRDocNo=T5.DocNo And T4.DRDNo=T5.DNo ");
                //SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("        And T1.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("        And T1.TotalTax<>0 ");
                //else
                //{
                //    SQL.AppendLine("        And ( ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //}
                //SQL.AppendLine("        And T1.SODocNo Is Null ");
                //SQL.AppendLine("        Union All ");
                //SQL.AppendLine("        Select Distinct T1.DocNo, T5.SODocNo ");
                //SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                //SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
                //SQL.AppendLine("        Inner Join TblPLDtl T5 On T3.PLDocNo=T5.DocNo And T4.PLDNo=T5.DNo ");
                //SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("        And T1.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("        And T1.TotalTax<>0 ");
                //else
                //{
                //    SQL.AppendLine("        And ( ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //}
                //SQL.AppendLine("        And T1.SODocNo Is Null ");
                //SQL.AppendLine("        Union All ");
                //SQL.AppendLine("        Select Distinct DocNo, SODocNo ");
                //SQL.AppendLine("        From TblSalesInvoiceHdr ");
                //SQL.AppendLine("        Where (DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("        And CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("        And VATSettlementDocNo Is Null ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("        And TotalTax<>0 ");
                //else
                //{
                //    SQL.AppendLine("        And ( ");
                //    SQL.AppendLine("            IfNull(TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //}
                //SQL.AppendLine("        And SODocNo Is Not Null ");
                //SQL.AppendLine("    ) T Group By T.DocNo ");
                //SQL.AppendLine("    ) Tbl1 ");
                //SQL.AppendLine("    Left Join ( ");
                //SQL.AppendLine("        Select X3.DocNo, ");
                //SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                //SQL.AppendLine("        From TblIncomingPaymentHdr X1 ");
                //SQL.AppendLine("        Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='1' ");
                //SQL.AppendLine("        Inner Join TblSalesInvoiceHdr X3 ");
                //SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                //SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                //SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                //SQL.AppendLine("        Group By X3.DocNo ");
                //SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
                //SQL.AppendLine(") C On A.DocNo=C.DocNo ");
                //if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                //{
                //    SQL.AppendLine("Inner Join TblCostCenter D On A.DeptCode=D.DeptCode ");
                //    SQL.AppendLine("Inner Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode ");
                //    SQL.AppendLine(Filter6);
                //}
                //if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                //{
                //    SQL.AppendLine("Left Join (");
                //    SQL.AppendLine("        SELECT F1.DocNo, F2.TaxInd1, F2.TaxInd2, F2.TaxInd3, F2.TaxAmt1, F2.TaxAmt2, F2.TaxAmt3, ");
                //    SQL.AppendLine("        SUM(");
                //    SQL.AppendLine("        if((IFNULL(F1.TaxCode1, '') AND F2.TaxInd1 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), F2.TaxAmt1, 0.00) + ");
                //    SQL.AppendLine("        if((IFNULL(F1.TaxCode2, '') AND F2.TaxInd2 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), F2.TaxAmt2, 0.00) + ");
                //    SQL.AppendLine("        if((IFNULL(F1.TaxCode3, '') AND F2.TaxInd3 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), F2.TaxAmt3, 0.00)) ");
                //    SQL.AppendLine("        AS TaxAmt ");
                //    SQL.AppendLine("        FROM tblsalesinvoicehdr F1 ");
                //    SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl F2 ON F1.DocNo = F2.DocNo ");
                //    SQL.AppendLine("        WHERE (F1.DocDt BETWEEN @DocDt1 AND @DocDt2) AND F1.CancelInd = 'N'");
                //    SQL.AppendLine("And ( ");
                //    SQL.AppendLine("    F1.VoucherRequestPPNDocNo Is Null ");
                //    if (mDocNo.Length > 0)
                //        SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //    SQL.AppendLine(") ");
                //    SQL.AppendLine("        AND F1.VATSettlementDocNo IS NULL ");
                //    SQL.AppendLine("        GROUP BY F1.DocNo ");
                //    SQL.AppendLine("    ) F ON A.DocNo = F.DocNo ");
                //}
                //SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("And A.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("And A.TotalTax<>0 ");
                //else
                //{
                //    SQL.AppendLine("        And ( ");
                //    SQL.AppendLine("            IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //}
                //SQL.AppendLine(Filter);
                //SQL.AppendLine(Filter2);

                //SQL.AppendLine("Union All ");
                //SQL.AppendLine("Select '2' As DocType, 'Sales Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                //SQL.AppendLine("B.CtName, C.SODocNo, ");
                //if (mFrmParent.mIsVRVATFilteredByTaxGroup)
                //{
                //    if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1" || mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                //        SQL.AppendLine("H.TaxInvDocument As TaxInvoiceNo, H.TaxInvoiceDt, H.TaxAmt As Amt, ");
                //}
                //else
                //    SQL.AppendLine("A.TaxInvDocument As TaxInvoiceNo, A.TaxInvoiceDt, A.TaxAmt As Amt, ");
                //if (mFrmParent.mIsSalesReturnInvoiceCanOnlyHave1Record)
                //{
                //    SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                //    SQL.AppendLine("IfNull(( ");
                //    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                //    SQL.AppendLine("    Where RateDt<=A.DocDt ");
                //    SQL.AppendLine("    And CurCode1=A.CurCode ");
                //    SQL.AppendLine("    And CurCode2=@MainCurCode ");
                //    SQL.AppendLine("    Order By RateDt Desc ");
                //    SQL.AppendLine("    Limit 1 ");
                //    SQL.AppendLine("), 0.00) ");
                //    SQL.AppendLine("End As ExcRate, ");
                //}
                //else
                //    SQL.AppendLine("0.00 As ExcRate, ");
                //SQL.AppendLine("Null As Remark, A.CurCode ");
                //SQL.AppendLine("From TblSalesReturnInvoiceHdr A ");
                //SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                //SQL.AppendLine("Left Join ( ");

                //SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.SODocNo, ");
                //SQL.AppendLine("    Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                //SQL.AppendLine("    ) As SODocNo From ( ");

                //SQL.AppendLine("    Select T.DocNo, ");
                //SQL.AppendLine("    Group_Concat(Distinct T.SODocNo Order By T.SODocNo ASC Separator ', ') As SODocNo ");
                //SQL.AppendLine("    From ( ");
                //SQL.AppendLine("        Select Distinct T1.DocNo, T6.SODocNo ");
                //SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
                //SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
                //SQL.AppendLine("        Inner Join TblDRDtl T6 On T4.DRDocNo=T6.DocNo And T5.DRDNo=T6.DNo ");
                //SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("        And T1.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                //SQL.AppendLine("        And T1.TaxAmt<>0 ");
                //SQL.AppendLine("        Union All ");
                //SQL.AppendLine("        Select Distinct T1.DocNo, T6.SODocNo ");
                //SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
                //SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
                //SQL.AppendLine("        Inner Join TblPLDtl T6 On T4.PLDocNo=T6.DocNo And T5.PLDNo=T6.DNo ");
                //SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("        And T1.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                //SQL.AppendLine("        And T1.TaxAmt<>0 ");
                //SQL.AppendLine("    ) T Group By T.DocNo ");

                //SQL.AppendLine("    ) Tbl1 ");
                //SQL.AppendLine("    Left Join ( ");
                //SQL.AppendLine("        Select X3.DocNo, ");
                //SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                //SQL.AppendLine("        From TblIncomingPaymentHdr X1 ");
                //SQL.AppendLine("        Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='2' ");
                //SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr X3 ");
                //SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                //SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                //SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                //SQL.AppendLine("        Group By X3.DocNo ");
                //SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");

                //SQL.AppendLine(") C On A.DocNo=C.DocNo ");
                //SQL.AppendLine("Inner Join TblSalesReturnInvoiceDtl D On A.DocNo=D.DocNo And D.DNo='001' ");
                //SQL.AppendLine("Inner Join TblRecvCtDtl E On D.RecvCtDocNo=E.DocNo And D.RecvCtDNo=E.DNo ");
                //if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                //{
                //    SQL.AppendLine("Inner Join TblCostCenter F On A.DeptCode=F.DeptCode ");
                //    SQL.AppendLine("Inner Join TblProfitCenter G On F.ProfitCenterCode=G.ProfitCenterCode ");
                //    SQL.AppendLine(Filter7);
                //}
                //SQL.AppendLine("        Left Join ( ");
                //if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1")
                //{
                //    SQL.AppendLine("        SELECT H1.DocNo, H6.DocNo SLRIDocNo, H1.TaxInvDocument, H1.TaxInvDt TaxInvoiceDt, H1.TaxCode1, H1.TaxCode2, H1.TaxCode3, ");
                //    SQL.AppendLine("        SUM(");
                //    SQL.AppendLine("        if(IFNULL(H1.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H1.TaxAmt1, 0.00) + ");
                //    SQL.AppendLine("        if(IFNULL(H1.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H1.TaxAmt2, 0.00) + ");
                //    SQL.AppendLine("        if(IFNULL(H1.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H1.TaxAmt3, 0.00)) ");
                //    SQL.AppendLine("        AS TaxAmt ");
                //    SQL.AppendLine("        FROM tblsalesinvoicehdr H1 ");
                //    SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl H2 ON H1.DocNo = H2.DocNo ");
                //    SQL.AppendLine("        LEFT JOIN tbldoctdtl H3 ON H2.DOCtDocNo = H3.DocNo AND H2.DOCtDNo = H3.DNo ");
                //    SQL.AppendLine("        LEFT JOIN tblrecvctdtl H4 ON H3.DocNo = H4.DOCtDocNo AND H3.DNo = H4.DOCtDNo ");
                //    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicedtl H5 ON H4.DocNo = H5.RecvCtDocNo AND H4.DNo = H5.RecvCtDNo ");
                //    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicehdr H6 ON H5.DocNo = H6.DocNo ");
                //    SQL.AppendLine("        WHERE (H1.DocDt BETWEEN @DocDt1 AND @DocDt2) AND H1.CancelInd = 'N' ");
                //    SQL.AppendLine("And ( ");
                //    SQL.AppendLine("    H1.VoucherRequestPPNDocNo Is Null ");
                //    if (mDocNo.Length > 0)
                //        SQL.AppendLine("    Or IfNull(H1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //    SQL.AppendLine(") ");
                //    SQL.AppendLine("And H1.VATSettlementDocNo Is Null ");
                //    SQL.AppendLine("        GROUP BY H1.DocNo, H6.DocNo ");
                //}
                //if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                //{
                //    SQL.AppendLine("        SELECT H1.DocNo, H6.DocNo SLRIDocNo, H1.TaxInvDocument, H1.TaxInvDt TaxInvoiceDt, H1.TaxCode1, H1.TaxCode2, H1.TaxCode3, ");
                //    SQL.AppendLine("        SUM(");
                //    SQL.AppendLine("        if((IFNULL(H1.TaxCode1, '') AND H2.TaxInd1 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H2.TaxAmt1, 0.00) + ");
                //    SQL.AppendLine("        if((IFNULL(H1.TaxCode2, '') AND H2.TaxInd2 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H2.TaxAmt2, 0.00) + ");
                //    SQL.AppendLine("        if((IFNULL(H1.TaxCode3, '') AND H2.TaxInd3 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H2.TaxAmt3, 0.00)) ");
                //    SQL.AppendLine("        AS TaxAmt ");
                //    SQL.AppendLine("        FROM tblsalesinvoicehdr H1 ");
                //    SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl H2 ON H1.DocNo = H2.DocNo ");
                //    SQL.AppendLine("        LEFT JOIN tbldoctdtl H3 ON H2.DOCtDocNo = H3.DocNo AND H2.DOCtDNo = H3.DNo ");
                //    SQL.AppendLine("        LEFT JOIN tblrecvctdtl H4 ON H3.DocNo = H4.DOCtDocNo AND H3.DNo = H4.DOCtDNo ");
                //    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicedtl H5 ON H4.DocNo = H5.RecvCtDocNo AND H4.DNo = H5.RecvCtDNo ");
                //    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicehdr H6 ON H5.DocNo = H6.DocNo ");
                //    SQL.AppendLine("        WHERE (H1.DocDt BETWEEN @DocDt1 AND @DocDt2) AND H1.CancelInd = 'N' ");
                //    SQL.AppendLine("And ( ");
                //    SQL.AppendLine("    H1.VoucherRequestPPNDocNo Is Null ");
                //    if (mDocNo.Length > 0)
                //        SQL.AppendLine("    Or IfNull(H1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //    SQL.AppendLine(") ");
                //    SQL.AppendLine("And H1.VATSettlementDocNo Is Null ");
                //    SQL.AppendLine("        GROUP BY H1.DocNo, H6.DocNo ");
                //}
                //SQL.AppendLine("        ) H ON A.DocNo = H.SLRIDocNo ");
                //SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    Exists( ");
                //SQL.AppendLine("        Select 1 ");
                //SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                //SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T3 On T2.DOCtDocNo=T3.DocNo And T2.DOCtDNo=T3.DNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl T4 On T2.DOCtDocNo=T4.DocNo ");
                //SQL.AppendLine("        Where T1.CancelInd='N' ");
                //SQL.AppendLine("        And T4.DocNo=E.DOCtDocNo And T4.DNo=E.DOCtDNo ");
                //SQL.AppendLine("    ) Or ");
                //SQL.AppendLine("    Exists( ");
                //SQL.AppendLine("        Select 1 ");
                //SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                //SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T3 On T2.DOCtDocNo=T3.DocNo And T2.DOCtDNo=T3.DNo ");
                //SQL.AppendLine("        Inner Join TblDOCt2Dtl T4 On T2.DOCtDocNo=T4.DocNo ");
                //SQL.AppendLine("        Where T1.CancelInd='N' ");
                //SQL.AppendLine("        And T4.DocNo=E.DOCtDocNo And T4.DNo=E.DOCtDNo ");
                //SQL.AppendLine("    ) ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("And A.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                //if (mFrmParent.mIsVRVATFilteredByTaxGroup)
                //{
                //    if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1" || mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                //    {
                //        SQL.AppendLine("        And ( ");
                //        SQL.AppendLine("            IfNull(H.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //        SQL.AppendLine("            IfNull(H.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //        SQL.AppendLine("            IfNull(H.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //    }
                //}
                //else
                //    SQL.AppendLine("And A.TaxAmt<>0 ");
                //SQL.AppendLine(Filter);
                //SQL.AppendLine(Filter3);


                //SQL.AppendLine("Union All ");
                //SQL.AppendLine("Select '3' As DocType, 'Point Of Sale' As DocTypeDesc, A.DocNo, A.DocDt, ");
                //SQL.AppendLine("B.CtName, Null As SODocNo, A.TaxInvoiceNo, A.TaxInvoiceDt, ");
                //SQL.AppendLine("A.Amt, 1.00 As ExcRate, ");
                //SQL.AppendLine("Concat(IfNull(C.OptDesc, ''), ' (', ");
                //SQL.AppendLine("    Concat( ");
                //SQL.AppendLine("        Case When E.BankName Is Not Null Then Concat(E.BankName, ' : ') Else '' End, ");
                //SQL.AppendLine("        Case When D.BankAcNo Is Not Null  ");
                //SQL.AppendLine("        Then Concat(D.BankAcNo, ' [', IfNull(D.BankAcNm, ''), ']') ");
                //SQL.AppendLine("        Else IfNull(D.BankAcNm, '') End ");
                //SQL.AppendLine("    ) ");
                //SQL.AppendLine(", ')') As Remark, A.CurCode ");
                //SQL.AppendLine("From TblPosVat A ");
                //SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                //SQL.AppendLine("Left Join TblOption C On A.PaymentType=C.OptCode And C.OptCat='VoucherPaymentType' ");
                //SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
                //SQL.AppendLine("Left Join TblBank E On D.BankCode=E.BankCode ");
                //if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                //{
                //    SQL.AppendLine("Inner Join TblCostCenter F On D.CCCode=F.CCCode ");
                //    SQL.AppendLine("Inner Join TblProfitCenter G On F.ProfitCenterCode=G.ProfitCenterCode ");
                //    SQL.AppendLine(Filter8);
                //}
                //SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("And A.CancelInd='N' ");

                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                //SQL.AppendLine("And A.Amt<>0 ");
                //SQL.AppendLine(Filter);
                //SQL.AppendLine(Filter4);


                //SQL.AppendLine("Union All ");
                //SQL.AppendLine("Select '4' As DocType, 'Sales Invoice For Project' As DocTypeDesc, A.DocNo, A.DocDt, ");
                //SQL.AppendLine("B.CtName, C.SODocNo, A.TaxInvoiceNo, IfNull(A.TaxInvoiceDt, A.DocDt) As TaxInvoiceDt, ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("A.TotalTax ");
                //else
                //{
                //    SQL.AppendLine("    If(IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt1, 0.00) + ");
                //    SQL.AppendLine("    If(IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt2, 0.00) + ");
                //    SQL.AppendLine("    If(IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt3, 0.00) ");
                //}
                //SQL.AppendLine("As Amt, ");
                //SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                //SQL.AppendLine("    IfNull(( ");
                //SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                //SQL.AppendLine("        Where RateDt<=A.DocDt ");
                //SQL.AppendLine("        And CurCode1=A.CurCode ");
                //SQL.AppendLine("        And CurCode2=@MainCurCode ");
                //SQL.AppendLine("        Order By RateDt Desc ");
                //SQL.AppendLine("        Limit 1 ");
                //SQL.AppendLine("    ), 0.00) ");
                //SQL.AppendLine("End As ExcRate, ");
                //SQL.AppendLine("Null As Remark, A.CurCode ");
                //SQL.AppendLine("From TblSalesInvoice5Hdr A ");
                //SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select Tbl1.DocNo, ");
                //SQL.AppendLine("    Concat(Tbl1.SODocNo, ");
                //SQL.AppendLine("        Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                //SQL.AppendLine("    ) As SODocNo ");
                //SQL.AppendLine("    From ");
                //SQL.AppendLine("    ( ");
                //SQL.AppendLine("        Select T.DocNo, ");
                //SQL.AppendLine("        Group_Concat(Distinct T.SOCDocNo Order By T.SOCDocNo ASC Separator ', ') As SODocNo ");
                //SQL.AppendLine("        From ");
                //SQL.AppendLine("        ( ");
                //SQL.AppendLine("            Select Distinct T1.DocNo, T4.SOCDocNo ");
                //SQL.AppendLine("            From TblSalesInvoice5Hdr T1 ");
                //SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo = T2.DocNo ");
                //SQL.AppendLine("            Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo = T3.DocNo ");
                //SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo = T4.DocNo ");
                //SQL.AppendLine("            Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("            And T1.CancelInd='N' ");
                //SQL.AppendLine("            And (T1.VoucherRequestPPNDocNo Is Null ) ");
                //SQL.AppendLine("            And T1.VATSettlementDocNo Is Null ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("            And T1.TotalTax<>0 ");
                //else
                //{
                //    SQL.AppendLine("        And ( ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //}
                //SQL.AppendLine("        ) T Group By T.DocNo ");
                //SQL.AppendLine("    ) Tbl1 ");
                //SQL.AppendLine("    Left Join ( ");
                //SQL.AppendLine("        Select X3.DocNo, ");
                //SQL.AppendLine("            Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                //SQL.AppendLine("            From TblIncomingPaymentHdr X1 ");
                //SQL.AppendLine("            Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='5' ");
                //SQL.AppendLine("            Inner Join TblSalesInvoice5Hdr X3 ");
                //SQL.AppendLine("               On X2.InvoiceDocNo=X3.DocNo ");
                //SQL.AppendLine("	            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                //SQL.AppendLine("            Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                //SQL.AppendLine("            Group By X3.DocNo ");
                //SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
                //SQL.AppendLine(")C On A.DocNo=C.DocNo ");
                //if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                //{
                //    SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
                //    SQL.AppendLine("Inner Join TblCostCenter E On D.CCCode=E.CCCode ");
                //    SQL.AppendLine("Inner Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
                //    SQL.AppendLine(Filter9);
                //}
                //SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                //SQL.AppendLine("And A.CancelInd='N' ");
                //SQL.AppendLine("And ( ");
                //SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                //if (mDocNo.Length > 0)
                //    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                //SQL.AppendLine(") ");
                //SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                //if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                //    SQL.AppendLine("And A.TotalTax<>0 ");
                //else
                //{
                //    SQL.AppendLine("        And ( ");
                //    SQL.AppendLine("            IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                //    SQL.AppendLine("            IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                //}
                //SQL.AppendLine(Filter);
                //SQL.AppendLine(Filter5);

                //SQL.AppendLine("Order By DocNo; ");
                #endregion
            
            #region New Code
                SQL.AppendLine("Select '1' As DocType, 'Sales Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("C.CtName, D.SODocNo, A.TaxInvDocument As TaxInvoiceNo, B.TaxInvoiceDt, ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("A.TotalTax ");
                else
                {
                    if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1")
                    {
                        SQL.AppendLine("Case ");
                        SQL.AppendLine("    When IfNull(B.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                        SQL.AppendLine("        Then B.TaxInvoiceRate*A.TotalAmt*0.01 ");
                    }
                    else if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                    {
                        SQL.AppendLine("Case ");
                        SQL.AppendLine("    When IfNull(B.TaxCode, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode) ");
                        SQL.AppendLine("        Then B.TaxInvoiceRate*F.Amt*0.01 ");
                    }
                }
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("As Amt, ");
                else
                    SQL.AppendLine("End As Amt, ");
                SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("    Where RateDt<=A.DocDt ");
                SQL.AppendLine("    And CurCode1=A.CurCode ");
                SQL.AppendLine("    And CurCode2=@MainCurCode ");
                SQL.AppendLine("    Order By RateDt Desc ");
                SQL.AppendLine("    Limit 1 ");
                SQL.AppendLine("), 0.00) ");
                SQL.AppendLine("End As ExcRate, ");
                SQL.AppendLine("Null As Remark, A.CurCode, B.TaxCode, B.TaxInvoiceName, B.TaxInvoiceRate ");
                SQL.AppendLine("FROM  TblSalesinvoicehdr A ");
                SQL.AppendLine("LEFT Join ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT '1' TaxType, A.DocNo, A.DocDt, A.TaxAmt1 AS TaxAmt, B.TaxCode, A.TaxInvDocument,  ");
                SQL.AppendLine("	IfNull(A.TaxInvDt, A.DocDt) As TaxInvoiceDt, A.CurCode, A.CancelInd, A.VoucherRequestPPNDocNo, A.VATSettlementDocNo, A.CtCode,  ");
                SQL.AppendLine("    B.TaxName AS TaxInvoiceName, B.TaxRate AS TaxInvoiceRate, A.TotalAmt  ");
                SQL.AppendLine("	FROM tblsalesinvoicehdr A  ");
                SQL.AppendLine("	INNER JOIN tbltax B ON A.TaxCode1 = B.TaxCode  ");
                SQL.AppendLine("	WHERE (A.DocDt Between @DocDt1 And @DocDt2)  ");
                SQL.AppendLine("	AND A.CancelInd ='N'  ");
                SQL.AppendLine("	AND A.TaxCode1 IS NOT NULL   ");
                SQL.AppendLine("   And (IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode)) ");
                
                SQL.AppendLine("	UNION ALL   ");
                SQL.AppendLine("	SELECT '2' TaxType, A.DocNo, A.DocDt, A.TaxAmt2 AS TaxAmt, B.TaxCode, A.TaxInvDocument,  ");
                SQL.AppendLine("	IfNull(A.TaxInvDt, A.DocDt) As TaxInvoiceDt, A.CurCode, A.CancelInd, A.VoucherRequestPPNDocNo, A.VATSettlementDocNo, A.CtCode,  ");
                SQL.AppendLine("    B.TaxName AS TaxInvoiceName, B.TaxRate AS TaxInvoiceRate, A.TotalAmt  ");
                SQL.AppendLine("	FROM tblsalesinvoicehdr A  ");
                SQL.AppendLine("	INNER JOIN tbltax B ON A.TaxCode2 = B.TaxCode  ");
                SQL.AppendLine("	WHERE (A.DocDt Between @DocDt1 And @DocDt2)  ");
                SQL.AppendLine("	AND A.CancelInd ='N'  ");
                SQL.AppendLine("	AND A.TaxCode2 IS NOT NULL   ");
                SQL.AppendLine("    And (IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode)) ");
                
                SQL.AppendLine("	UNION ALL   ");
                SQL.AppendLine("	SELECT '3' TaxType, A.DocNo, A.DocDt, A.TaxAmt3 AS TaxAmt, B.TaxCode, A.TaxInvDocument,  ");
                SQL.AppendLine("	IfNull(A.TaxInvDt, A.DocDt) As TaxInvoiceDt, A.CurCode, A.CancelInd, A.VoucherRequestPPNDocNo, A.VATSettlementDocNo, A.CtCode,  ");
                SQL.AppendLine("    B.TaxName AS TaxInvoiceName, B.TaxRate AS TaxInvoiceRate, A.TotalAmt  ");
                SQL.AppendLine("	FROM tblsalesinvoicehdr A  ");
                SQL.AppendLine("	INNER JOIN tbltax B ON A.TaxCode3 = B.TaxCode  ");
                SQL.AppendLine("	WHERE (A.DocDt Between @DocDt1 And @DocDt2)  ");
                SQL.AppendLine("	AND A.CancelInd ='N'  ");
                SQL.AppendLine("	AND A.TaxCode3 IS NOT NULL   ");
                SQL.AppendLine("    And (IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode)) ");
                SQL.AppendLine(") B ON A.DocNo = B.DocNo ");

                SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.SODocNo, ");
                SQL.AppendLine("    Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                SQL.AppendLine("    ) As SODocNo From ( ");
                SQL.AppendLine("    Select T.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct T.SODocNo Order By T.SODocNo ASC Separator ', ') As SODocNo ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T5.SODocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
                SQL.AppendLine("        Inner Join TblDRDtl T5 On T3.DRDocNo=T5.DocNo And T4.DRDNo=T5.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("        And T1.TotalTax<>0 ");

                SQL.AppendLine("        And T1.SODocNo Is Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T5.SODocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
                SQL.AppendLine("        Inner Join TblPLDtl T5 On T3.PLDocNo=T5.DocNo And T4.PLDNo=T5.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("        And T1.TotalTax<>0 ");

                SQL.AppendLine("        And T1.SODocNo Is Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T1.SODocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("        And T1.TotalTax<>0 ");

                SQL.AppendLine("        And SODocNo Is Not Null ");
                SQL.AppendLine("    ) T Group By T.DocNo ");
                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                SQL.AppendLine("        From TblIncomingPaymentHdr X1 ");
                SQL.AppendLine("        Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='1' ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceHdr X3 ");
                SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("        Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
                SQL.AppendLine(") D On A.DocNo=D.DocNo ");
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Inner Join TblCostCenter E On A.DeptCode=E.DeptCode ");
                    SQL.AppendLine("Inner Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
                    SQL.AppendLine(Filter6);
                }
                if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T.DocNo, Sum(T.Amt) Amt, T.TaxType ");
                    SQL.AppendLine("    FROM  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("	    SELECT T1.DocNo, T2.Qty*T2.UPriceBeforeTax as Amt, ");
                    SQL.AppendLine("	    case  ");
                    SQL.AppendLine("		    when T2.TaxInd1 = 'Y' then '1' ");
                    SQL.AppendLine("		    when T2.TaxInd2 = 'Y' then '2' ");
                    SQL.AppendLine("		    when T2.TaxInd3 = 'Y' then '3' ");
                    SQL.AppendLine("		    ELSE null ");
                    SQL.AppendLine("	    END AS TaxType ");
                    SQL.AppendLine("	    FROM tblsalesinvoicehdr T1 ");
                    SQL.AppendLine("	    INNER JOIN tblsalesinvoicedtl T2 ON T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("    ) T  Group By T.DocNo ");
                    SQL.AppendLine(") G on A.DocNo = G.DocNo And B.TaxType = G.TaxType ");
                }
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                    SQL.AppendLine("And A.Amt<>0 ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("And A.TotalTax<>0 ");

                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter2);

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '2' As DocType, 'Sales Return Invoice' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.CtName, C.SODocNo, ");
                if (mFrmParent.mIsVRVATFilteredByTaxGroup)
                {
                    if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1" || mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                        SQL.AppendLine("H.TaxInvDocument As TaxInvoiceNo, H.TaxInvoiceDt, H.TaxAmt As Amt, ");
                }
                else
                    SQL.AppendLine("A.TaxInvDocument As TaxInvoiceNo, A.TaxInvoiceDt, A.TaxAmt As Amt, ");
                if (mFrmParent.mIsSalesReturnInvoiceCanOnlyHave1Record)
                {
                    SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("    Where RateDt<=A.DocDt ");
                    SQL.AppendLine("    And CurCode1=A.CurCode ");
                    SQL.AppendLine("    And CurCode2=@MainCurCode ");
                    SQL.AppendLine("    Order By RateDt Desc ");
                    SQL.AppendLine("    Limit 1 ");
                    SQL.AppendLine("), 0.00) ");
                    SQL.AppendLine("End As ExcRate, ");
                }
                else
                    SQL.AppendLine("0.00 As ExcRate, ");
                SQL.AppendLine("Null As Remark, A.CurCode, Null As TaxCode, Null As TaxInvoiceName, Null As TaxInvoiceRate ");
                SQL.AppendLine("From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join ( ");

                SQL.AppendLine("    Select Tbl1.DocNo, Concat(Tbl1.SODocNo, ");
                SQL.AppendLine("    Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                SQL.AppendLine("    ) As SODocNo From ( ");

                SQL.AppendLine("    Select T.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct T.SODocNo Order By T.SODocNo ASC Separator ', ') As SODocNo ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T6.SODocNo ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
                SQL.AppendLine("        Inner Join TblDRDtl T6 On T4.DRDocNo=T6.DocNo And T5.DRDNo=T6.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("        And T1.TaxAmt<>0 ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T6.SODocNo ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesReturnInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvCtDtl T3 On T1.RecvCtDocNo=T3.DocNo And T2.RecvCtDNo=T3.DNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T4 On T3.DOCtDocNo=T4.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T5 On T3.DOCtDocNo=T5.DocNo And T3.DOCtDNo=T5.DNo ");
                SQL.AppendLine("        Inner Join TblPLDtl T6 On T4.PLDocNo=T6.DocNo And T5.PLDNo=T6.DNo ");
                SQL.AppendLine("        Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    T1.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(T1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("        And T1.VATSettlementDocNo Is Null ");
                SQL.AppendLine("        And T1.TaxAmt<>0 ");
                SQL.AppendLine("    ) T Group By T.DocNo ");

                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                SQL.AppendLine("        From TblIncomingPaymentHdr X1 ");
                SQL.AppendLine("        Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='2' ");
                SQL.AppendLine("        Inner Join TblSalesReturnInvoiceHdr X3 ");
                SQL.AppendLine("            On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("        Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");

                SQL.AppendLine(") C On A.DocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblSalesReturnInvoiceDtl D On A.DocNo=D.DocNo And D.DNo='001' ");
                SQL.AppendLine("Inner Join TblRecvCtDtl E On D.RecvCtDocNo=E.DocNo And D.RecvCtDNo=E.DNo ");
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Inner Join TblCostCenter F On A.DeptCode=F.DeptCode ");
                    SQL.AppendLine("Inner Join TblProfitCenter G On F.ProfitCenterCode=G.ProfitCenterCode ");
                    SQL.AppendLine(Filter7);
                }
                SQL.AppendLine("        Left Join ( ");
                if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1")
                {
                    SQL.AppendLine("        SELECT H1.DocNo, H6.DocNo SLRIDocNo, H1.TaxInvDocument, H1.TaxInvDt TaxInvoiceDt, H1.TaxCode1, H1.TaxCode2, H1.TaxCode3, ");
                    SQL.AppendLine("        SUM(");
                    SQL.AppendLine("        if(IFNULL(H1.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H1.TaxAmt1, 0.00) + ");
                    SQL.AppendLine("        if(IFNULL(H1.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H1.TaxAmt2, 0.00) + ");
                    SQL.AppendLine("        if(IFNULL(H1.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H1.TaxAmt3, 0.00)) ");
                    SQL.AppendLine("        AS TaxAmt ");
                    SQL.AppendLine("        FROM tblsalesinvoicehdr H1 ");
                    SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl H2 ON H1.DocNo = H2.DocNo ");
                    SQL.AppendLine("        LEFT JOIN tbldoctdtl H3 ON H2.DOCtDocNo = H3.DocNo AND H2.DOCtDNo = H3.DNo ");
                    SQL.AppendLine("        LEFT JOIN tblrecvctdtl H4 ON H3.DocNo = H4.DOCtDocNo AND H3.DNo = H4.DOCtDNo ");
                    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicedtl H5 ON H4.DocNo = H5.RecvCtDocNo AND H4.DNo = H5.RecvCtDNo ");
                    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicehdr H6 ON H5.DocNo = H6.DocNo ");
                    SQL.AppendLine("        WHERE (H1.DocDt BETWEEN @DocDt1 AND @DocDt2) AND H1.CancelInd = 'N' ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("    H1.VoucherRequestPPNDocNo Is Null ");
                    if (mDocNo.Length > 0)
                        SQL.AppendLine("    Or IfNull(H1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                    SQL.AppendLine(") ");
                    SQL.AppendLine("And H1.VATSettlementDocNo Is Null ");
                    SQL.AppendLine("        GROUP BY H1.DocNo, H6.DocNo ");
                }
                if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                {
                    SQL.AppendLine("        SELECT H1.DocNo, H6.DocNo SLRIDocNo, H1.TaxInvDocument, H1.TaxInvDt TaxInvoiceDt, H1.TaxCode1, H1.TaxCode2, H1.TaxCode3, ");
                    SQL.AppendLine("        SUM(");
                    SQL.AppendLine("        if((IFNULL(H1.TaxCode1, '') AND H2.TaxInd1 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H2.TaxAmt1, 0.00) + ");
                    SQL.AppendLine("        if((IFNULL(H1.TaxCode2, '') AND H2.TaxInd2 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H2.TaxAmt2, 0.00) + ");
                    SQL.AppendLine("        if((IFNULL(H1.TaxCode3, '') AND H2.TaxInd3 = 'Y')  In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), H2.TaxAmt3, 0.00)) ");
                    SQL.AppendLine("        AS TaxAmt ");
                    SQL.AppendLine("        FROM tblsalesinvoicehdr H1 ");
                    SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl H2 ON H1.DocNo = H2.DocNo ");
                    SQL.AppendLine("        LEFT JOIN tbldoctdtl H3 ON H2.DOCtDocNo = H3.DocNo AND H2.DOCtDNo = H3.DNo ");
                    SQL.AppendLine("        LEFT JOIN tblrecvctdtl H4 ON H3.DocNo = H4.DOCtDocNo AND H3.DNo = H4.DOCtDNo ");
                    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicedtl H5 ON H4.DocNo = H5.RecvCtDocNo AND H4.DNo = H5.RecvCtDNo ");
                    SQL.AppendLine("        LEFT JOIN tblsalesreturninvoicehdr H6 ON H5.DocNo = H6.DocNo ");
                    SQL.AppendLine("        WHERE (H1.DocDt BETWEEN @DocDt1 AND @DocDt2) AND H1.CancelInd = 'N' ");
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("    H1.VoucherRequestPPNDocNo Is Null ");
                    if (mDocNo.Length > 0)
                        SQL.AppendLine("    Or IfNull(H1.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                    SQL.AppendLine(") ");
                    SQL.AppendLine("And H1.VATSettlementDocNo Is Null ");
                    SQL.AppendLine("        GROUP BY H1.DocNo, H6.DocNo ");
                }
                SQL.AppendLine("        ) H ON A.DocNo = H.SLRIDocNo ");
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

                SQL.AppendLine("And ( ");
                SQL.AppendLine("    Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl2 T3 On T2.DOCtDocNo=T3.DocNo And T2.DOCtDNo=T3.DNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl T4 On T2.DOCtDocNo=T4.DocNo ");
                SQL.AppendLine("        Where T1.CancelInd='N' ");
                SQL.AppendLine("        And T4.DocNo=E.DOCtDocNo And T4.DNo=E.DOCtDNo ");
                SQL.AppendLine("    ) Or ");
                SQL.AppendLine("    Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl3 T3 On T2.DOCtDocNo=T3.DocNo And T2.DOCtDNo=T3.DNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl T4 On T2.DOCtDocNo=T4.DocNo ");
                SQL.AppendLine("        Where T1.CancelInd='N' ");
                SQL.AppendLine("        And T4.DocNo=E.DOCtDocNo And T4.DNo=E.DOCtDNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                if (mFrmParent.mIsVRVATFilteredByTaxGroup)
                {
                    if (mFrmParent.mSalesInvoiceTaxCalculationFormula == "1" || mFrmParent.mSalesInvoiceTaxCalculationFormula == "2")
                    {
                        SQL.AppendLine("        And ( ");
                        SQL.AppendLine("            IfNull(H.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                        SQL.AppendLine("            IfNull(H.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                        SQL.AppendLine("            IfNull(H.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                    }
                }
                else
                    SQL.AppendLine("And A.TaxAmt<>0 ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter3);


                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '3' As DocType, 'Point Of Sale' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.CtName, Null As SODocNo, A.TaxInvoiceNo, A.TaxInvoiceDt, ");
                SQL.AppendLine("A.Amt, 1.00 As ExcRate, ");
                SQL.AppendLine("Concat(IfNull(C.OptDesc, ''), ' (', ");
                SQL.AppendLine("    Concat( ");
                SQL.AppendLine("        Case When E.BankName Is Not Null Then Concat(E.BankName, ' : ') Else '' End, ");
                SQL.AppendLine("        Case When D.BankAcNo Is Not Null  ");
                SQL.AppendLine("        Then Concat(D.BankAcNo, ' [', IfNull(D.BankAcNm, ''), ']') ");
                SQL.AppendLine("        Else IfNull(D.BankAcNm, '') End ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(", ')') As Remark, A.CurCode, Null As TaxCode, Null As TaxInvoiceName, Null As TaxInvoiceRate ");
                SQL.AppendLine("From TblPosVat A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join TblOption C On A.PaymentType=C.OptCode And C.OptCat='VoucherPaymentType' ");
                SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
                SQL.AppendLine("Left Join TblBank E On D.BankCode=E.BankCode ");
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Inner Join TblCostCenter F On D.CCCode=F.CCCode ");
                    SQL.AppendLine("Inner Join TblProfitCenter G On F.ProfitCenterCode=G.ProfitCenterCode ");
                    SQL.AppendLine(Filter8);
                }
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");

                SQL.AppendLine("And ( ");
                SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                SQL.AppendLine("And A.Amt<>0 ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter4);


                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '4' As DocType, 'Sales Invoice For Project' As DocTypeDesc, A.DocNo, A.DocDt, ");
                SQL.AppendLine("B.CtName, C.SODocNo, A.TaxInvoiceNo, IfNull(A.TaxInvoiceDt, A.DocDt) As TaxInvoiceDt, ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("A.TotalTax ");
                else
                {
                    SQL.AppendLine("    If(IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt1, 0.00) + ");
                    SQL.AppendLine("    If(IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt2, 0.00) + ");
                    SQL.AppendLine("    If(IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where TaxGrpCode Is Not Null And TaxGrpCode=@TaxGrpCode), A.TaxAmt3, 0.00) ");
                }
                SQL.AppendLine("As Amt, ");
                SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=A.DocDt ");
                SQL.AppendLine("        And CurCode1=A.CurCode ");
                SQL.AppendLine("        And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc ");
                SQL.AppendLine("        Limit 1 ");
                SQL.AppendLine("    ), 0.00) ");
                SQL.AppendLine("End As ExcRate, ");
                SQL.AppendLine("Null As Remark, A.CurCode, Null As TaxCode, Null As TaxInvoiceName, Null As TaxInvoiceRate ");
                SQL.AppendLine("From TblSalesInvoice5Hdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Tbl1.DocNo, ");
                SQL.AppendLine("    Concat(Tbl1.SODocNo, ");
                SQL.AppendLine("        Case When Tbl2.IP Is Null Then '' Else Concat(', ', Tbl2.IP) End ");
                SQL.AppendLine("    ) As SODocNo ");
                SQL.AppendLine("    From ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T.DocNo, ");
                SQL.AppendLine("        Group_Concat(Distinct T.SOCDocNo Order By T.SOCDocNo ASC Separator ', ') As SODocNo ");
                SQL.AppendLine("        From ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct T1.DocNo, T4.SOCDocNo ");
                SQL.AppendLine("            From TblSalesInvoice5Hdr T1 ");
                SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            Inner Join TblProjectImplementationHdr T3 On T2.ProjectImplementationDocNo = T3.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T4 On T3.SOContractDocNo = T4.DocNo ");
                SQL.AppendLine("            Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And (T1.VoucherRequestPPNDocNo Is Null ) ");
                SQL.AppendLine("            And T1.VATSettlementDocNo Is Null ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("            And T1.TotalTax<>0 ");
                else
                {
                    SQL.AppendLine("        And ( ");
                    SQL.AppendLine("            IfNull(T1.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                    SQL.AppendLine("            IfNull(T1.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                    SQL.AppendLine("            IfNull(T1.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                }
                SQL.AppendLine("        ) T Group By T.DocNo ");
                SQL.AppendLine("    ) Tbl1 ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X3.DocNo, ");
                SQL.AppendLine("            Group_Concat(Distinct Concat(X1.DocNo, ' (', DATE_FORMAT(X1.DocDt, '%d/%m/%Y'), ')') Order By X1.DocDt, X1.DocNo ASC Separator ', ') As IP ");
                SQL.AppendLine("            From TblIncomingPaymentHdr X1 ");
                SQL.AppendLine("            Inner Join TblIncomingPaymentDtl X2 On X1.DocNo=X2.DocNo And X2.InvoiceType='5' ");
                SQL.AppendLine("            Inner Join TblSalesInvoice5Hdr X3 ");
                SQL.AppendLine("               On X2.InvoiceDocNo=X3.DocNo ");
                SQL.AppendLine("	            And X3.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Where X1.CancelInd='N' And X1.Status In ('O', 'A') ");
                SQL.AppendLine("            Group By X3.DocNo ");
                SQL.AppendLine("    ) Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
                SQL.AppendLine(")C On A.DocNo=C.DocNo ");
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
                    SQL.AppendLine("Inner Join TblCostCenter E On D.CCCode=E.CCCode ");
                    SQL.AppendLine("Inner Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
                    SQL.AppendLine(Filter9);
                }
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    A.VoucherRequestPPNDocNo Is Null ");
                if (mDocNo.Length > 0)
                    SQL.AppendLine("    Or IfNull(A.VoucherRequestPPNDocNo, '')=@DocNoTemp ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.VATSettlementDocNo Is Null ");
                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                    SQL.AppendLine("And A.TotalTax<>0 ");
                else
                {
                    SQL.AppendLine("        And ( ");
                    SQL.AppendLine("            IfNull(A.TaxCode1, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                    SQL.AppendLine("            IfNull(A.TaxCode2, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) OR ");
                    SQL.AppendLine("            IfNull(A.TaxCode3, '') In (Select TaxCode From TblTax Where IfNull(TaxGrpCode, '')=@TaxGrpCode) ) ");
                }
                SQL.AppendLine(Filter);
                SQL.AppendLine(Filter5);

                SQL.AppendLine("Order By DocNo; ");
                #endregion
            

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                (mFrmParent.mIsFicoUseMultiProfitCenterFilter && IsProfitCenterInvalid())
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", Filter2 = string.Empty, Filter3 = string.Empty, Filter4 = string.Empty, Filter5 = string.Empty,
                    Filter6 = string.Empty, Filter7 = string.Empty, Filter8 = string.Empty, Filter9 = string.Empty;
                var cm = new MySqlCommand();

                SetProfitCenter();

                if (mFrmParent.Grd2.Rows.Count >= 1)
                {
                    string DocType = string.Empty, DocNo = string.Empty, TaxCode = string.Empty;

                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                    {
                        DocType = Sm.GetGrdStr(mFrmParent.Grd2, r, 2);
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd2, r, 4);
                        TaxCode = Sm.GetGrdStr(mFrmParent.Grd2, r, 14);
                        if (DocNo.Length != 0)
                        {
                            if (DocType == "1" )
                            {
                                if (!mFrmParent.mIsVRVATFilteredByTaxGroup)
                                {
                                    if (Filter2.Length > 0) Filter2 += " And ";
                                    Filter2 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                }

                                else
                                {
                                    if (Filter2.Length > 0) Filter2 += " And ";
                                    Filter2 += "(Concat(A.DocNo,'-',B.TaxCode) <> Concat(@DocNo0" + r.ToString() + ",'-',@TaxCode0" + r.ToString() + "))";
                                    //Filter2 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                }
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                                Sm.CmParam<String>(ref cm, "@TaxCode0" + r.ToString(), TaxCode);
                            }
                            if (DocType == "2")
                            {
                                if (Filter3.Length > 0) Filter3 += " And ";
                                Filter3 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            }
                            if (DocType == "3")
                            {
                                if (Filter4.Length > 0) Filter4 += " And ";
                                Filter4 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            }
                            if (DocType == "4")
                            {
                                if (Filter5.Length > 0) Filter5 += " And ";
                                Filter5 += "(A.DocNo<>@DocNo0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            }
                        }
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (E.ProfitCenterCode=@ProfitCenter1_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter1_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter6 += "    And 1=0 ";
                        else
                            Filter6 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter6 += "    And Find_In_Set(E.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter6 += "    And E.ProfitCenterCode In ( ";
                            Filter6 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter6 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter6 += "    ) ";
                        }
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (G.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter7 += "    And 1=0 ";
                        else
                            Filter7 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter7 += "    And Find_In_Set(G.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter7 += "    And G.ProfitCenterCode In ( ";
                            Filter7 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter7 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter7 += "    ) ";
                        }
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (G.ProfitCenterCode=@ProfitCenter3_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter3_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter8 += "    And 1=0 ";
                        else
                            Filter8 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter8 += "    And Find_In_Set(G.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter8 += "    And G.ProfitCenterCode In ( ";
                            Filter8 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter8 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter8 += "    ) ";
                        }
                    }
                }

                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (F.ProfitCenterCode=@ProfitCenter4_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter4_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter9 += "    And 1=0 ";
                        else
                            Filter9 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter9 += "    And Find_In_Set(F.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter9 += "    And F.ProfitCenterCode In ( ";
                            Filter9 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter9 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter9 += "    ) ";
                        }
                    }
                }

                if (Filter2.Length > 0) Filter2 = " And ( " + Filter2 + ") ";
                if (Filter3.Length > 0) Filter3 = " And ( " + Filter3 + ") ";
                if (Filter4.Length > 0) Filter4 = " And ( " + Filter4 + ") ";
                if (Filter5.Length > 0) Filter5 = " And ( " + Filter5 + ") ";

                Sm.CmParam<String>(ref cm, "@MainCurCode", mFrmParent.mMainCurCode);
                Sm.CmParam<String>(ref cm, "@SalesInvoiceTaxCalculationFormula", mFrmParent.mSalesInvoiceTaxCalculationFormula);
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(mFrmParent.LueCurCode));
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@DocNoTemp", mDocNo);
                Sm.CmParam<String>(ref cm, "@TaxGrpCode", mTaxGrpCode);
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                
                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter, Filter2, Filter3, Filter4, Filter5, Filter6, Filter7, Filter8, Filter9),
                        new string[]
                        { 
                            //0
                            "DocType",
                            
                            //1-5
                            "DocTypeDesc", "DocNo", "DocDt", "CtName", "SODocNo", 
                            
                            //6-10
                            "TaxInvoiceNo", "TaxInvoiceDt", "Amt", "ExcRate", "Remark",

                            //11-14
                            "CurCode", "TaxCode", "TaxInvoiceName", "TaxInvoiceRate"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Grd.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 11) * Sm.GetGrdDec(Grd, Row, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                        }, true, false, false, false
                    );
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                        if (Sm.GetGrdStr(Grd1, Row2, 8).Length > 0)
                            mFrmParent.Grd2.Cells[Row1, 6].Value = Sm.GetGrdStr(Grd1, Row2, 7) + " - " + Sm.GetGrdStr(Grd1, Row2, 8);
                        else
                            mFrmParent.Grd2.Cells[Row1, 6].Value = Sm.GetGrdStr(Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 10, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 11, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 12, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 14, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 16, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 17, Grd1, Row2, 18);

                        if (Sm.GetGrdStr(mFrmParent.Grd2, Row1, 2) == "3")
                        {
                            mFrmParent.Grd2.Cells[Row1, 7].BackColor = Color.White;
                            mFrmParent.Grd2.Cells[Row1, 7].ReadOnly = iGBool.False;
                        }

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 17 });
                        mFrmParent.ComputeAmt();
                        mFrmParent.SetNo(ref mFrmParent.Grd2);
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Purchase Invoice.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string
                DocType = Sm.GetGrdStr(Grd1, Row, 2),
                DocNo = Sm.GetGrdStr(Grd1, Row, 4),
                TaxCode = Sm.GetGrdStr(Grd1, Row, 15); ;

            for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, r, 2), DocType) && Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, r, 4), DocNo) &&
                    (DocType == "1" && Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, r, 14), TaxCode))) return true;

            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                string CBD = Sm.GetValue("SELECT DocNo FROM TblSalesInvoiceHdr WHERE DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 4) + "' AND SODocNo IS NOT NULL; ");
                string SLI5 = Sm.GetValue("Select A.DocNo FROM TblSalesInvoice5Hdr A " +
                                          "Inner Join TblSalesInvoice5Dtl B On A.DocNo = B.DocNo " +
                                          "Inner Join TblProjectImplementationHdr C On B.ProjectImplementationDocNo = C.Docno " +
                                          "Inner Join TblSOContractRevisionhdr D On C.SOContractDocNo = D.DocNo " +
                                          "Where A.DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 4) + "' And D.SOCDocNo Is Not Null; ");

                if (CBD == Sm.GetGrdStr(Grd1, e.RowIndex, 4))
                {
                    var f = new FrmSalesInvoice2("XXX");
                    f.Tag = "XXX";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.Text = "Sales Invoice (Cash Before Delivery)";
                    f.ShowDialog();
                }
                else
                {
                    if (SLI5 == Sm.GetGrdStr(Grd1, e.RowIndex, 4))
                    {
                        var f = new FrmSalesInvoice5("XXX");
                        f.Tag = "XXX";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.Text = "Sales Invoice For Project";
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmSalesInvoice("XXX");
                        f.Tag = "XXX";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.Text = "Sales Invoice";
                        f.ShowDialog();
                    }
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                string CBD = Sm.GetValue("SELECT DocNo FROM TblSalesInvoiceHdr WHERE DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 2) + "' AND SODocNo IS NOT NULL; ");
                string SLI5 = Sm.GetValue("Select A.DocNo FROM TblSalesInvoice5Hdr A " +
                                          "Inner Join TblSalesInvoice5Dtl B On A.DocNo = B.DocNo " +
                                          "Inner Join TblProjectImplementationHdr C On B.ProjectImplementationDocNo = C.Docno " +
                                          "Inner Join TblSOContractRevisionhdr D On C.SOContractDocNo = D.DocNo " +
                                          "Where A.DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 4) + "' And D.SOCDocNo Is Not Null; ");

                if (CBD == Sm.GetGrdStr(Grd1, e.RowIndex, 4))
                {
                    e.DoDefault = false;
                    var f = new FrmSalesInvoice2("XXX");
                    f.Tag = "XXX";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.Text = "Sales Invoice (Cash Before Delivery)";
                    f.ShowDialog();
                }
                else
                {
                    if (SLI5 == Sm.GetGrdStr(Grd1, e.RowIndex, 4))
                    {
                        e.DoDefault = false;
                        var f = new FrmSalesInvoice5("XXX");
                        f.Tag = "XXX";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.Text = "Sales Invoice For Project";
                        f.ShowDialog();
                    }
                    else
                    {
                        e.DoDefault = false;
                        var f = new FrmSalesInvoice("XXX");
                        f.Tag = "XXX";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.Text = "Sales Invoice";
                        f.ShowDialog();
                    }
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mFrmParent.mIsFicoUseMultiProfitCenterFilter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            SQL.AppendLine("WHERE Exists(  ");
            SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
            SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine(") ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion

    }
}

