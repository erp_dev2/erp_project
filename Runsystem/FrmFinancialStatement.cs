﻿#region Update
/*
    11/07/2022 [MYA/PRODUCT] Membuat menu "Importing Financial Statement" dengan basis menggunakan template field menu "Yearly Closing and Opening Balance".  
    21/07/2022 [MYA/PRODUCT] Feedback: Di menu Importing Financial Statement untuk di Field Entity, agar yang dimunculkan hanya Entity yang 'Active' saja
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmFinancialStatement : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mEntCode = string.Empty,
            mEntName = string.Empty;
        internal FrmFinancialStatementFind FrmFind;

        internal int mRow = 0;

        #endregion

        #region Constructor

        public FrmFinancialStatement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion


        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                SetFormControl(mState.View);
                GetParameter();
                //if (mIsEntityMandatory) LblEntity.ForeColor = Color.Red;
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "Local Account#",

                        //1-4
                        "Local Account Description",
                        "Debit",
                        "Credit",
                        "Balance",
                    },
                    new int[]
                    {
                        //0
                        150,
                        
                        //1-4
                        350, 150, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4});
            //Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       DteDocDt, TxtEntityName, DteClosingDt, TxtBalanceCheck, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnEntityCode.Enabled = BtnImport.Enabled = false;
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, DteClosingDt, MeeRemark }, false);
                    //if (mIsEntityMandatory) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueEntCode }, false);
                    BtnEntityCode.Enabled = BtnImport.Enabled = true;
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               TxtDocNo, DteDocDt, DteClosingDt, TxtEntityName, TxtBalanceCheck,
               MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFinancialStatementFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't edit this data." + Environment.NewLine +
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    UpdateData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid())
                return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "FinancialStatement", "TblFinancialStatementHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveFinancialStatementHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 
                    //&&
                    //Sm.GetGrdDec(Grd1, Row, 4) != 0m
                    )
                    cml.Add(SaveFinancialStatementDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEntityName, "Entity", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteClosingDt, "Closing Date") ||
                IsGrdEmpty() ||
                IsDocAlreadyCreated() ||
                IsFinancialAmountNotBalance() ||
                IsClosingDtInvalid()
                ;
        }

        private bool IsClosingDtInvalid()
        {
            string ClosingDt = Sm.Left(Sm.FormatDate(DteClosingDt.DateTime), 8);
            string LastDt = Sm.GetValue("select DATE_FORMAT(LAST_DAY(" + Sm.GetDte(DteClosingDt) + "), '%Y%m%d')");
            
            if (ClosingDt != LastDt)
            {
                Sm.StdMsg(mMsgType.Warning, "Closing Date need to be Last Day of Month");
                return true;
            }
            return false;
        }

        private bool IsFinancialAmountNotBalance()
        {
            if (Convert.ToDecimal(TxtBalanceCheck.Text) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Financial Statement Data Imported is unbalance. Please recheck the data imported");
                return true;
            }
            return false;
        }


        private bool IsDocAlreadyCreated()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 From TblFinancialStatementHdr ");
            SQL.AppendLine("Where ClosingDt=@ClosingDt ");
            SQL.AppendLine("And CancelInd='N' ");
            //if (mIsEntityMandatory)
            SQL.AppendLine(" And EntCode Is Not Null And EntCode=@EntCode ");

            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

            if (Sm.IsDataExist(cm))
            {
                var Msg = string.Empty;

                Msg = ("Closing Date : " + (Sm.ConvertDate(Sm.GetDte(DteClosingDt)).ToString("dd/MM/yyyy")) + Environment.NewLine);
                if (TxtEntityName.Text.Length > 0) Msg += ("Entity : " + mEntName + Environment.NewLine);
                Msg += (Environment.NewLine + "Financial Statement has been created.");

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveFinancialStatementHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblFinancialStatementHdr(DocNo, DocDt, CancelInd, EntCode, ClosingDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @EntCode, @ClosingDt, @Remark, @CreateBy, @CreateDt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        private MySqlCommand SaveFinancialStatementDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblFinancialStatementDtl(DocNo, AcNo, AcDesc, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @AcNo, @AcDesc, @DAmt, @CAmt, @CreateBy, @CreateDt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@AcDesc", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditCOAOpeningBalanceHdr());
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 2) != Sm.GetGrdDec(Grd1, Row, 5)
                    )
                    cml.Add(EditCOAOpeningBalanceDtl(Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblFinancialStatementHdr Where DocNo=@DocNo And CancelInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditCOAOpeningBalanceHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblCOAOpeningBalanceHdr Set " +
                    "   CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditCOAOpeningBalanceDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblCOAOpeningBalanceDtl Set " +
                    "   Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And AcNo=@AcNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowFinancialStatementHdr(DocNo);
                ShowFinancialStatementDtl(DocNo);
                ComputeBalanceCheck(Grd1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowFinancialStatementHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, A.EntCode, B.EntName, A.ClosingDt, A.Remark " +
                    "From TblFinancialStatementHdr A " +
                    "inner Join TblEntity B On A.EntCode = B.EntCode " +
                    "Where A.DocNo=@DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "EntCode", "EntName", "ClosingDt", 
                        
                        //6
                        "Remark", 

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        mEntCode = Sm.DrStr(dr, c[3]);
                        mEntName = Sm.DrStr(dr, c[4]);
                        TxtEntityName.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetDte(DteClosingDt, Sm.DrStr(dr, c[5]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);

                    }, true
                );
        }

        private void ShowFinancialStatementDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.AcNo, A.AcDesc, A.DAmt, A.CAmt, (A.DAmt - A.CAmt) as Balance ");
            SQL.AppendLine("From TblFinancialStatementDtl A ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.AcNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "AcNo",
                    
                    //1-4
                    "AcDesc", "DAmt", "CAmt", "Balance"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        internal void ComputeBalanceCheck(iGrid Grd)
        {

            decimal BalanceCheck = 0m;

            for (int r = 0; r < Grd.Rows.Count; r++)
            {

                if (Sm.GetGrdStr(Grd, r, 0).Length > 0)
                    BalanceCheck += Sm.GetGrdDec(Grd, r, 4);
            }

            TxtBalanceCheck.EditValue = Sm.FormatNum(BalanceCheck, 0);

        }

        private void GetParameter()
        {
            //mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            //mIsCOAOpeningBalanceUseProfitCenter = Sm.GetParameterBoo("IsCOAOpeningBalanceUseProfitCenter");
            //mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            //mAcNoForCurrentEarning2 = Sm.GetParameter("AcNoForCurrentEarning2");
            //mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            //mAcNoForCost = Sm.GetParameter("AcNoForCost");
            //mCOAAssetStartYr = Sm.GetParameter("COAAssetStartYr");
            //mCOAAssetAcNo = Sm.GetParameter("COAAssetAcNo");
            //mIsCOAOpeningBalanceUseGroupInd = Sm.GetParameterBoo("IsCOAOpeningBalanceUseGroupInd");
            //mOpeningBalanceImportFormat = Sm.GetParameter("OpeningBalanceImportFormat");
        }

        


        #endregion

        #region Import CSV

        private void ProcessImport()
        {
            string FileName = string.Empty, AcType = string.Empty;
            var l = new List<FinancialStatement>();
            Sm.ClearGrd(Grd1, true);
            mRow = 0;
            FileName = OpenFileDialog();
            if (FileName.Length == 0 || FileName == "OD") return;
            ReadFileToList(ref l, FileName);
            if (l.Count > 0)
            {
                for (int i = 0; i < mRow; ++i)
                {
                    Grd1.Cells[i, 0].Value = l[i].AcNo;
                    Grd1.Cells[i, 1].Value = l[i].AcDesc;
                    Grd1.Cells[i, 2].Value = l[i].DAmt;
                    Grd1.Cells[i, 3].Value = l[i].CAmt;
                    Grd1.Cells[i, 4].Value = l[i].DAmt - l[i].CAmt;
                
                    Grd1.Rows.Add();
                }
            }
            l.Clear();
            ComputeBalanceCheck(Grd1);
        }

        private string OpenFileDialog()
        {
            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            OD.ShowDialog();

            return OD.FileName;
        }

        private void ReadFileToList(ref List<FinancialStatement> l, string FileName)
        {
            bool IsFirst = true;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 4)
                    {
                        if (splits.Count() > 4)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                        }
                        else if (splits.Count() < 2)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data.");
                        }

                        l.Clear();
                        return;
                    }
                    else
                    {
                        if (IsFirst) IsFirst = false;
                        else
                        {
                            mRow += 1;

                            var arr = splits.ToArray();
                            if (arr[0].Trim().Length > 0)
                            {
                                l.Add(new FinancialStatement()
                                {
                                    AcNo = splits[0].Trim(),
                                    AcDesc = splits[1].Trim(),
                                    DAmt = Decimal.Parse(splits[2].Trim()),
                                    CAmt = Decimal.Parse(splits[3].Trim())
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.Warning, "No data at row#" + mRow.ToString());
                            }
                        }
                    }
                }
            }
        }

        private void BtnEntityCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmFinancialStatementDlg(this));
        }

        #endregion

        #region Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                try
                {
                    ProcessImport();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }


        #endregion

        #region Class


        private class FinancialStatement
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }



        #endregion

    }
}
