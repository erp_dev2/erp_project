﻿#region Update
/*
    17/02/2022 [DITA/PHT]  New Apps
    04/03/2022 [DITA/PHT] Berdasarkan parameter ClosingBalanceInCashProfitCenterLevelToBeValidated, profit center yg bisa dipilih hanya dengan level tertentu saja.
    28/03/2022 [DITA/PHT] bank account yg muncul juga anak-anak nya profit center yg difilter
    30/05/2022 [DITA/PHT] Bug : saat generate docno belum melihat profit center child
    25/11/2022 [DITA/PHT] profit center di detail perlu disimpan supaya sesuai jika ada profit center child yg tergenerate
    19/12/2022 [DITA/PHT] bug closing balance belum melihat cancel ind
    27/01/2023 [SET/PHT] add Balance (System Currency) yg referensi dari rpt cash book
                         validasi save berdasar param IsClosingBalanceInCash 
    16/02/2023 [WED/PHT]  IncrementalSearch -> true di CcbProfitCenter
    13/03/2023 [MYA/BBT] Membuat penyesuaian di Closing Balance In Cash/Bank Account
    14/04/2023 [MYA/BBT] BUG : pada Closing Balance in Cash/Bank Account bisa insert closing berkali-kali #TI-469129
    18/04/2023 [MYA/PHT] Membuat Uang Muka Kerja tidak tertampil di transasksi Closing Balance cash/bank account
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmClosingBalanceInCash2 : RunSystem.FrmBase3
    {
        #region Field, Property

        public List<String> mlProfitCenter = null;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mProfitCenterCode = string.Empty;
        internal FrmClosingBalanceInCash2Find FrmFind;
        internal bool 
            mIsFilterByBankAccount = false, 
            mIsEntityMandatory = false, 
            mIsClosingBalanceInCashRequireOpeningBalance = false, 
            mIsFicoUseMultiProfitCenterFilter = false;
        internal string mMainCurCode = string.Empty, mAccTypeCashAndBankAccNotForCashBook = string.Empty;
        internal decimal mCBICDigitNumberDetail = 3m, mClosingBalanceInCashProfitCenterLevelToBeValidated = 0m;

        #endregion

        #region Constructor

        public FrmClosingBalanceInCash2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Closing Balance In Cash/Bank Account";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueMth(LueMth);
                Sl.SetLueYr(LueYr, "");
                mlProfitCenter = new List<String>();
                if(!mIsFicoUseMultiProfitCenterFilter)
                {
                    CcbProfitCenterCode.Visible = false;
                    LblMultiProfitCenterCode.Visible = false;
                }
                else
                {
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterByBankAccount = Sm.GetParameterBoo("IsFilterByBankAccount");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            mIsClosingBalanceInCashRequireOpeningBalance = Sm.GetParameterBoo("IsClosingBalanceInCashRequireOpeningBalance");
            mCBICDigitNumberDetail = Sm.GetParameterDec("CBICDigitNumberDetail");
            mClosingBalanceInCashProfitCenterLevelToBeValidated = Sm.GetParameterDec("ClosingBalanceInCashProfitCenterLevelToBeValidated");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mAccTypeCashAndBankAccNotForCashBook = Sm.GetParameter("AccTypeCashAndBankAccNotForCashBook");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0                        
                        "DNo",
                        
                        //1-5
                         "",
                        "Profit Center Code",
                        "Profit Center Name",
                        "Bank Account Code",
                        "Bank",
                       
                        //6-10
                        "Account#",
                        "Bank Account Name",
                        "Balance" + Environment.NewLine + "(Original Currency)",
                        "Confirmed Balance",
                        "Entity",

                        //11-14
                        "ExistInd",
                        "Currency",
                        "Balance"+Environment.NewLine+"(System Currency)",
                        "RateDt",
                       
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 200, 150, 180, 
 
                        //6-10
                         180, 450, 200, 120, 150, 

                        //11-15
                         0, 100, 150, 150
                    }
                );
            Grd1.Cols[12].Move(8);
            Grd1.Cols[13].Move(10);
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 11, 13, 14 }, false);
            if (!mIsEntityMandatory) Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
            if (!mIsFicoUseMultiProfitCenterFilter) Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 13 }, !ChkHideInfoInGrd.Checked);
            
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueMth, LueYr, MeeRemark, ChkCancelInd, CcbProfitCenterCode, MeeCancelReason
                    }, true);
                    BtnCalculate.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 9 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueMth, LueYr, MeeRemark, CcbProfitCenterCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 9 });
                    BtnCalculate.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { MeeCancelReason }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, LueMth, LueYr, MeeRemark, MeeCancelReason, CcbProfitCenterCode });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8 });
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmClosingBalanceInCash2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            mProfitCenterCode = GetCcbProfitCenterCode();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();
            

            var l = new List<CBICHdr>();
            var l2 = new List<CBICDtl>();
           

            Process1(ref l2); //insert to list
            Process2(ref l, ref l2); //process docno + dno

            foreach (var y in l)
                cml.Add(SaveClosingBalanceInCashHdr(y.DocNo, y.ProfitCenterCode));
            foreach (var z in l2.OrderBy(w=>w.DocNo))
                cml.Add(SaveClosingBalanceInCashDtl(z));

            l.Clear();l2.Clear();
            

            Sm.ExecCommands(cml);
            ClearData();

        }

        private bool IsInsertedDataNotValid()
        {
            RecheckExistenceData();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                (mIsFicoUseMultiProfitCenterFilter && IsProfitCenterInvalid()) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() || 
                (mIsClosingBalanceInCashRequireOpeningBalance && IsClosingBalanceRequireOpeningBalance());
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count-1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 11) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning,
                       "This Profit Center : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                       "with Bank Account : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                       "already processed.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, r, 8) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                       "Amount should not be less than 0.00" + Environment.NewLine +
                       "for Profit Center : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                       "with Bank Account : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine);
                        return true;
                    }

                    if (!Sm.GetGrdBool(Grd1, r, 9))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                       "You need to tick all of the Confirmed Balance");
                        return true;
                    }
                }
            }

            return false;
        }
        private void RecheckExistenceData()
        {
            var l3 = new List<CBICDtl2>();

            string Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth), BankAcCode = string.Empty, Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.BankAcCode, A.ProfitCenterCode ");
            SQL.AppendLine("From TblClosingBalanceInCashHdr A ");
            SQL.AppendLine("Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.Mth=@Mth ");
            if (TxtDocNo.Text.Length != 0) SQL.AppendLine("And A.DocNo<>@DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            if (mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("And Find_In_Set(A.ProfitCenterCode,@ProfitCenterCode) ");
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", mProfitCenterCode.ToString());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode", "BankAcCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new CBICDtl2()
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]),
                            BankAcCode = Sm.DrStr(dr, c[1]),
                           
                        });
                    }
                }
                dr.Close();
            }


            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    Grd1.Cells[r, 11].Value = "N";
                    foreach (var y in l3.Where(w =>
                                w.ProfitCenterCode == Sm.GetGrdStr(Grd1, r, 2) &&
                                w.BankAcCode == Sm.GetGrdStr(Grd1, r, 4)
                                ))
                        {
                             Grd1.Cells[r, 11].Value = "Y";
                        }
                }
            }

            l3.Clear();

        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 Bank Account.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveClosingBalanceInCashHdr(string DocNo, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblClosingBalanceInCashHdr(DocNo, DocDt, Yr, Mth, ");
            if (mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("ProfitCenterCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, @Mth, "); 
            if (mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("@ProfitCenterCode, "); 
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()) "); 

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if(mIsFicoUseMultiProfitCenterFilter) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveClosingBalanceInCashDtl(CBICDtl l2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblClosingBalanceInCashDtl(DocNo, DNo, ");
            if(mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("ProfitCenterCode, CurCode, Amt2, ");
            SQL.AppendLine("BankAcCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, ");
            if(mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("@ProfitCenterCode , @CurCode, @Amt2, ");
            SQL.AppendLine("@BankAcCode, @Amt, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l2.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo",l2.DNo);
            if(mIsFicoUseMultiProfitCenterFilter) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", l2.ProfitCenterCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", l2.BankAcCode);
            Sm.CmParam<String>(ref cm, "@CurCode", l2.CurCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", l2.Amt);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", l2.Amt2);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditClosingBalanceInCashHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand EditClosingBalanceInCashHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblClosingBalanceInCashHdr Set CancelInd='Y', CancelReason=@CancelReason , LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N';"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }


        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                ChkCancelInd.Checked && Sm.IsMeeEmpty(MeeCancelReason, "Cancel Reason") ||
                IsDocAlreadyCancelled();
        }

        private bool IsDocAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblClosingBalanceInCashHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowCBICHdr(DocNo);
                ShowCBICDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCBICHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            if (mIsFicoUseMultiProfitCenterFilter)
            {
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Yr, A.Mth, A.Remark, A.CancelInd, A.CancelReason, B.ProfitCenterName " +
                    "From TblClosingBalanceInCashHdr A " +
                    "Left Join TblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode " +
                    "Where DocNo=@DocNo",
                    new string[]
                    {
                        "DocNo",
                        "DocDt", "Yr", "Mth", "Remark", "CancelInd",
                        "CancelReason", "ProfitCenterName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[5]) == "Y";
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]);
                        CcbProfitCenterCode.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
            }
            else
            {
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Yr, A.Mth, A.Remark, A.CancelInd, A.CancelReason, null as ProfitCenterName " +
                    "From TblClosingBalanceInCashHdr A " +
                    "Where DocNo=@DocNo",
                    new string[]
                    {
                        "DocNo",
                        "DocDt", "Yr", "Mth", "Remark", "CancelInd",
                        "CancelReason", "ProfitCenterName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[5]) == "Y";
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]);
                        CcbProfitCenterCode.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
            }
        }

        private void ShowCBICDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var SQL = new StringBuilder();

                if (mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Select A.DocNo, B.Dno, F.ProfitCenterCode, F.ProfitCenterName, B.BankAcCode, D.BankName, C.BankAcNo, C.BankAcNm, B.Amt, ");
                    SQL.AppendLine("E.EntName, B.CurCode, B.Amt2 ");
                }
                else
                {

                    SQL.AppendLine("Select A.DocNo, B.Dno, null as ProfitCenterCode, null as ProfitCenterName, B.BankAcCode, D.BankName, C.BankAcNo, C.BankAcNm, B.Amt, ");
                    SQL.AppendLine("E.EntName, null as CurCode, null as Amt2");
                }
                SQL.AppendLine("From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("Inner Join TblClosingBalanceInCashDtl B On A.DocNo = B.DOcNo  ");
                SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                SQL.AppendLine("Left Join TblBank D On C.BankCode=D.BankCode ");
                SQL.AppendLine("Left Join TblEntity E On C.EntCode=E.EntCode ");
                if (mIsFicoUseMultiProfitCenterFilter)
                    SQL.AppendLine("Inner Join TblProfitCenter F On B.ProfitCenterCode=F.ProfitCenterCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo, B.DNo;");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ProfitCenterCode", "ProfitCenterName", "BankAcCode", "BankName", "BankAcNo", 
                        
                        //6-8
                        "BankAcNm", "Amt", "EntName", "CurCode", "Amt2",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Grd1.Cells[Row, 9].Value =  "Y";
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                if (BtnSave.Enabled && e.ColIndex == 1 && !IsProfitCenterInvalid() && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmClosingBalanceInCash2Dlg(this, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                }
            }
            else
            {
                if (BtnSave.Enabled && e.ColIndex == 1 && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmClosingBalanceInCash2Dlg(this, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) 
        {
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                if (e.ColIndex == 1 && !IsProfitCenterInvalid() && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                    Sm.FormShowDialog(new FrmClosingBalanceInCash2Dlg(this, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
            }
            else
            {
                if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueMth, "Month") && !Sm.IsLueEmpty(LueYr, "Year"))
                    Sm.FormShowDialog(new FrmClosingBalanceInCash2Dlg(this, Sm.GetLue(LueMth), Sm.GetLue(LueYr)));
            }
        }
        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                if (e.ColIndex == 9)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 9);
                    for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                        Grd1.Cells[Row, 9].Value = !IsSelected;
                }
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<CBICDtl> l2)
        {

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                l2.Add(new CBICDtl()
                {
                    ProfitCenterCode = Sm.GetGrdStr(Grd1, Row, 2 ),
                    DocNo =  string.Empty,
                    DNo = string.Empty,
                    BankAcCode = Sm.GetGrdStr(Grd1, Row, 4),
                    Amt = Sm.GetGrdDec(Grd1, Row, 8),
                    CurCode = Sm.GetGrdStr(Grd1, Row, 12),
                    Amt2 = Sm.GetGrdDec(Grd1, Row, 13),
                    
                });
            }
        }

        private void Process2(ref List<CBICHdr> l, ref List<CBICDtl> l2)
        {

            string CurrDocNo = string.Empty, Currx = string.Empty;
            int DNo = 1;
            string[] mProfitCenterCodes = mProfitCenterCode.Split(',');
            int index = 0;
            string CurrProfitCenterCode = string.Empty;


            if (mIsFicoUseMultiProfitCenterFilter)
            {
                foreach (var x in mProfitCenterCodes.Distinct())
                {
                    foreach (var z in l2.OrderBy(w => w.ProfitCenterCode))
                    {
                        //if (CurrProfitCenterCode.Length == 0) CurrProfitCenterCode = z.ProfitCenterCode;
                        if (x == z.ProfitCenterCode || z.ProfitCenterCode.StartsWith(x))
                        {
                            if (CurrProfitCenterCode != z.ProfitCenterCode && Currx != x)
                            {
                                index += 1;
                                z.DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingBalanceInCash", "TblClosingBalanceInCashHdr", index.ToString());
                                CurrDocNo = z.DocNo;
                                CurrProfitCenterCode = z.ProfitCenterCode;
                                Currx = x;

                                l.Add(new CBICHdr()
                                {
                                    DocNo = z.DocNo,
                                    ProfitCenterCode = x
                                });
                            }
                            else
                                z.DocNo = CurrDocNo;

                        }
                    }
                }
            }
            else
            {
                string docno = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingBalanceInCash", "TblClosingBalanceInCashHdr", "1");
                foreach (var z in l2)
                {
                    z.DocNo = docno;
                }

                l.Add(new CBICHdr()
                {
                    DocNo = docno,
                    ProfitCenterCode = string.Empty
                });
            }

            CurrDocNo = string.Empty;
            foreach (var z in l2.OrderBy(w => w.DocNo))
            {
                if (CurrDocNo.Length == 0) CurrDocNo = z.DocNo;
                if (CurrDocNo != z.DocNo)
                {
                    DNo = 1;
                    CurrDocNo = z.DocNo;
                }
                if(mIsFicoUseMultiProfitCenterFilter)
                    z.DNo = Sm.Right("00000000" + (DNo + 1).ToString(), Convert.ToInt32(mCBICDigitNumberDetail) > 0 ? Convert.ToInt32(mCBICDigitNumberDetail) : 3);
                else
                    z.DNo = Sm.Right("00000000" + (DNo + 1).ToString(), Convert.ToInt32(mCBICDigitNumberDetail) > 0 ? Convert.ToInt32(mCBICDigitNumberDetail) : 3);
                DNo++;
            }


        }
        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }
        
        private bool IsClosingBalanceRequireOpeningBalance()
        {
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            if(Mth == "01")
            {
                decimal Yr2 = Decimal.Parse(Yr) - 1;
                Yr = Convert.ToString(Yr2);
                Mth = "12"; 
            }
            else
            {
                decimal Mth2 = Decimal.Parse(Mth) - 1;
                Mth = Convert.ToString(Mth2);
            }
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblClosingBalanceInCashHdr ");
            SQL.AppendLine("Where Yr = @Yr And Mth = @Mth ");
            SQL.AppendLine("And CancelInd='N' ");
            if (mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("And Find_In_Set(ProfitCenterCode, @ProfitCenter);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProfitCenteCoder", Sm.GetCcb(CcbProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "There is no Closing Balance in month " + Mth + " of " + Yr + ".");
                return true;
            }
            return false;
            //return !Sm.IsDataExist("Select 1 From TblClosingBalanceInCashHdr " +
            //    "Where Yr = '" + Yr + "' And Mth = '" + Mth + "' " +
            //    "And Find_In_Set(ProfitCenterCode, @Param1) And CancelInd = 'N';", ProfitCenter, "There is no Closing Balance in month " + Mth + " of " + Yr + "");
            //if (Sm.GetLue(LueYr).Length > 0 && Sm.GetLue(LueMth).Length > 0 && Sm.GetCcb(CcbProfitCenterCode).Length > 0)
            //{
            //    Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
            //    return true;
            //}

            //return false;
        }

        internal void SetCCbProfitCenterCodeWithChild()
        {
            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetCCbProfitCenterCodeWithChild(ref IsFirst, ref IsCompleted);
        }

        internal void SetCCbProfitCenterCodeWithChild(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                IsCompleted = false;
                
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }


        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T1.ProfitCenterName As Col, T1.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T1 ");
            SQL.AppendLine("    WHERE Exists(  ");
            SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
            SQL.AppendLine("        Where ProfitCenterCode=T1.ProfitCenterCode  ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            if (mClosingBalanceInCashProfitCenterLevelToBeValidated > 0m)
                SQL.AppendLine("    And Level=@ClosingBalanceInCashProfitCenterLevelToBeValidated ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<decimal>(ref cm, "@ClosingBalanceInCashProfitCenterLevelToBeValidated", mClosingBalanceInCashProfitCenterLevelToBeValidated);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string  GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }


        internal void CalculateClosingBalance()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Query = string.Empty;

            var Filter = string.Empty;
            int i = 0;
            if (mIsFicoUseMultiProfitCenterFilter)
            {
            SetCCbProfitCenterCodeWithChild();
            foreach (var x in mlProfitCenter.Distinct())
            {
                if (Filter.Length > 0) Filter += " Or ";
                
                    Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                
                i++;
            }
            if (Filter.Length == 0)
                Query += "    And 1=0 ";
            else
                Query += "    And (" + Filter + ") ";
            }

            SQL.AppendLine("Select T5.ProfitCenterCode, T5.ProfitCenterName, T1.BankAcCode, T3.BankName, T2.BankAcNo, T2.BankAcNm, T1.Amt, ");
            SQL.AppendLine("T4.EntName, T2.CurCode, T1.DocDt, T1.ExcRate ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.ProfitCenterCode, T.BankAcCode, Sum(T.Amt) As Amt, T.DocDt, T.ExcRate From (");
            SQL.AppendLine("    ( ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("        Select B.ProfitCenterCode, B.BankAcCode, B.Amt, A.DocDt, ifnull(B.ExcRate, 1) ExcRate ");
            }
            else
            {
                SQL.AppendLine("        Select null as ProfitCenterCode, B.BankAcCode, B.Amt, A.DocDt, null as ExcRate ");
            }
            SQL.AppendLine("        From TblClosingBalanceInCashHdr A ");
            SQL.AppendLine("        Inner Join TblClosingBalanceInCashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And Concat(A.Yr, A.Mth)=@YrMth2 ");
            SQL.AppendLine(Query.Replace("A.", "B."));
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.ProfitCenterCode, A.BankAcCode, Case A.Actype When 'C' Then -1 Else 1 End * B.Amt As Amt, A.DocDt, Ifnull(A.ExcRate, 1) ExcRate ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            if(mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("        Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            else
                SQL.AppendLine("        Left Join TblCostCenter D On C.CCCode = D.CCCode ");
            SQL.AppendLine("        Where A.DocNo = B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 ");
            SQL.AppendLine(Query.Replace("A.", "D."));
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.ProfitCenterCode, A.BankAcCode2 As BankAcCode, (Case A.Actype2 When 'C' Then -1 Else 1 End) * B.Amt * A.ExcRate As Amt, A.DocDt, Ifnull(A.ExcRate, 1) ExcRate ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
            if(mIsFicoUseMultiProfitCenterFilter)
                SQL.AppendLine("        Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            else
                SQL.AppendLine("        Left Join TblCostCenter D On C.CCCode = D.CCCode ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 And A.AcType2 Is Not Null ");
            SQL.AppendLine(Query.Replace("A.", "D."));
            SQL.AppendLine("        ) Order By BankAcCode, DocDt Desc ");
            SQL.AppendLine("    ) T Group By T.ProfitCenterCode, T.BankAcCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblBankAccount T2 On T1.BankAcCode=T2.BankAcCode ");
            SQL.AppendLine("Left Join TblBank T3 On T2.BankCode=T3.BankCode ");
            SQL.AppendLine("Left Join TblEntity T4 On T2.EntCode=T4.EntCode ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("Inner Join TblProfitCenter T5 On T1.ProfitCenterCode=T5.ProfitCenterCode ");
                SQL.AppendLine("Where Concat(T1.ProfitCenterCode, T1.BankAcCode) Not In ( ");
                SQL.AppendLine("    Select ifnull(Concat(B.ProfitCenterCode, B.BankAcCode), '') ");
                SQL.AppendLine("    From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("    Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("    Where A.Mth=@Mth ");
                SQL.AppendLine("    And A.Yr=@Yr ");
                SQL.AppendLine("    ) ");
            }
            else
                SQL.AppendLine("Left Join TblProfitCenter T5 On T1.ProfitCenterCode=T5.ProfitCenterCode ");
            if(mAccTypeCashAndBankAccNotForCashBook.Length > 0)
                SQL.AppendLine("AND NOT Find_In_Set(T2.BankAcTp, '" + mAccTypeCashAndBankAccNotForCashBook + "')");

            string
                Mth = Sm.GetLue(LueMth),
                Yr = Sm.GetLue(LueYr),
                Mth2 = (Mth == "01") ? "12" : Sm.Right("0" + (int.Parse(Mth) - 1).ToString(), 2),
                Yr2 = (Mth == "01") ? ((int.Parse(Yr)) - 1).ToString() : Yr;
            mProfitCenterCode = GetCcbProfitCenterCode();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@YrMth1", Yr+Mth);
            Sm.CmParam<String>(ref cm, "@YrMth2", Yr2+Mth2);
            //Sm.CmParam<String>(ref cm, "@ProfitCenterCode", mProfitCenterCode.ToString());

            Sm.ShowDataInGrid(
                     ref Grd1, ref cm, SQL.ToString(),
                     new string[]
                     { 
                        //0
                        "ProfitCenterCode", 

                        //1-5
                        "ProfitCenterName", "BankAcCode", "BankName", "BankAcNo", "BankAcNm",
                        
                        //6-10
                         "Amt", "EntName", "CurCode", "DocDt"
                     },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                     }, false, false, true, false
                 );
            GetCurrencyRate();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count-1 , new int[] { 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void GetCurrencyRate()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                decimal CurRate = Sm.GetValueDec("Select Amt From TblCurrencyRate " +
                    "Where CurCode1 = @Param1 And CurCode2 = '" + mMainCurCode + "' " +
                    "And RateDt <= @Param2 " +
                    "ORDER BY RateDt DESC LIMIT 1", Sm.GetGrdStr(Grd1, r, 12), Sm.GetGrdStr(Grd1, r, 14));
                if (Sm.GetGrdStr(Grd1, r, 12) == mMainCurCode || CurRate == 0)
                    Grd1.Cells[r, 13].Value = Sm.GetGrdDec(Grd1, r, 8);
                else
                    Grd1.Cells[r, 13].Value = Sm.GetGrdDec(Grd1, r, 8) * CurRate;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueYr_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        private void LueMth_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }
        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                if (!IsProfitCenterInvalid() && !Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth, "Month"))
                    CalculateClosingBalance();
            }
            else
            {
                if (!Sm.IsLueEmpty(LueYr, "Year") && !Sm.IsLueEmpty(LueMth, "Month"))
                    CalculateClosingBalance();
            }
        }

        #endregion


        #endregion

        public class CBICHdr
        {
            public string ProfitCenterCode { get; set; }
            public string DocNo { get; set; }
        }

        public class CBICDtl
        {
            public string ProfitCenterCode { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string BankAcCode { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }

        }
        public class CBICDtl2
        {
            public string ProfitCenterCode { get; set; }
            public string BankAcCode { get; set; }
        }
    }

}
