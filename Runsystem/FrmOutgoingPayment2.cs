﻿#region Update
/*
    03/04/2017 [TKG] tambah validasi meng-update ProcessInd di TblPurchaseInvoiceHdr apabila amount PI=0 dan belum dibuatkan OP
    14/06/2017 [TKG] bug fixing amount di hdr+dtl wkt disimpan di voucher request beda.
    11/07/2017 [TKG] tambah journal untuk pembayaran giro
    06/11/2017 [TKG] tambahan opsi untuk Paid To Bank apakah berdasarkan vendor atau tidak.
    06/12/2017 [TKG] BUG FIXING/IOK : Invoice yg sudah diproses di outgoing payment dengan amount 0 masih dianggap outstanding sehingga masih bisa diproses lagi di outgoing payment.
    16/01/2018 [TKG] Set bank secara otomatis berdasarkan bank account, ditambah informasi account type bank account.
    30/01/2018 [ARI] tambah printout KIM.
    31/01/2018 [HAR] tambah parameter buat ngeset urutan bank account
    18/04/2018 [TKG] ubah proses journal untuk giro
    26/04/2018 [TKG] Outgoing payment difilter berdasarkan otorisasi group thd departemen yg diambil dari PI (Y=Difilter. N=Tidak difilter)
    26/06/2018 [WED] Print out OP untuk KIM, decimal point dihilangkan
    31/07/2018 [ARI] Username di printout (kolom approve by) huruf awal dibuat besar semua, menggunakan parameter UserTYpe
    23/08/2018 [TKG] Berdasarkan bank account untuk mengisi currency secara otomatis
    06/10/2018 [TKG] update dept di vr berdasarkan setting approval
    09/10/2018 [HAR] bug waktu print jika amountnya 0
    17/01/2019 [TKG] status otomatis berubah menjadi approved apabila tidak ada data di document approval.
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    03/04/2019 [TKG] validasi entity dan entity bank account harus sama.
    02/10/2019 [WED/TWC] validasi kalau PI yang cancelled ga boleh di save
    12/11/2019 [DITA/IMS] Printout untuk IMS
    02/03/2020 [TKG/IMS] Berdasarkan parameter IsPITotalWithoutTaxInclDownpaymentEnabled, Total without tax include downpayment
    05/03/2020 [VIN/KBN] Generate Docno 6 digit
    16/12/2020 [WED/IMS] ambil Purchase Invoice yang sudah approved
    16/12/2020 [ICA/SIER] Menambah format baru untuk menampilkan SetLueBankAcCode based on parameter BankAccountFormat
    29/12/2020 [ICA/SIER] Mengubah format lue bankaccount
    02/02/2021 [WED/PHT] item category gak boleh beda berdasarkan parameter IsItemCategoryUseCOAAPAR
    04/02/2021 [ICA/IMS] Nilai Outstanding Amount disamakan dengan Invoice Amount di PurchaseInvoice berdasarakan parameter IsOutgoingPaymentUsePIWithCOA
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    14/04/2021 [WED/ALL] IsOutgoingPaymentAmtUseCOAAmt
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    21/04/2021 [WED/ALL] COA bisa dipilih lebih dari sekali berdasarkan parameter IsCOACouldBeChosenMoreThanOnce
    30/04/2021 [VIN/ALL] bug Cancel VR dan tambah field amount PI
    07/05/2021 [VIN/PHT] Generate VR DocNo berdasarkan SeqNo
    08/06/2021 [TKG/PHT] saat cancel journal, informasi cost center tetap diisi dari journal saat diinsert sebelumnya.
    06/07/2021 [VIN/IMS] Penyesuaian Amount VR 
    13/07/2021 [WED/PHT] tambah input budget category berdasarkan parameter IsOutgoingPaymentUseBudget
    14/07/2021 [VIN/IMS] Bug ShowOutgoingPaymentHdr
    15/07/2021 [VIN/ALL] Bug IsOutgoingPaymentUseBudget saat insert
    22/07/2021 [WED/PHT] ComputeAvailableBudget ambil dari StdMtd
    19/08/2021 [VIN/IMS] RecoumputeOutstandingAmt feedback tambah AcAmt
    01/09/2021 [VIN/IMS] ComputeAmt2 Amt * Rate 
    21/09/2021 [VIN/IOK] BUG IsOutgoingPaymentAmtUseCOAAmt
    27/09/2021 [ICA/AMKA] menambah tab list of items, perhitungan amount invoice berdasarkan amount yg diisi pada items
    21/12/2021 [RDA/AMKA] Merubah rujukan parameter IsOutgoingPaymentAmtUseCOAAmt untuk merubah amount voucher Outgoing payment dari VendorAcNoAP menjadi Item Category
    27/12/2021 [MYA/AMKA] Membuat Outgoing Payment dapat menarik Purchase Invoice dengan Item's Category yang berbeda
    11/01/2022 [ICA/AMKA] Additional COA purchase invoice yg mempengaruhi outstanding amount merujuk pada COA item category nya. Saat ini masih melihat VendorAcNoAP. 
                          Berdasarkan parameter OutgoingPaymentCOAAmtCalculationMethod = "2"
    23/08/2022 [TYO/AMKA] menambah validasi IsCOAAmtNotValid untuk cek Amt balance atau tidak
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmOutgoingPayment2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDeptCode = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mIsPrintOutOP,
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mVendorAcNoAP = string.Empty,
            mPurchaseInvoiceCOAAmtCalculationMethod = string.Empty
            ;

        internal FrmOutgoingPayment2Find FrmFind;
        internal bool
            mIsOutgoingPaymentFilterByPIDept = false,
            mIsOutgoingPaymentApprovalNeedDept = false,
            mIsFilterBySite = false,
            mIsComparedToDetailDate = false,
            mUserType = false,
            mIsPurchaseInvoiceUseApproval = false,
            mIsOutgoingPaymentUsePIWithCOA = false,
            mIsOutgoingPaymentAmtUseCOAAmt = false,
            mIsCOACouldBeChosenMoreThanOnce = false
            ;
        private bool
            mIsOutgoingPaymentApprovalNeedPaymentType = false,
            mIsCancelledDocumentCopyToRemark = false,
            mIsOutgoingPaymentUseCOA = false,
            mIsRemarkForApprovalMandatory = false,
            mIsEntityMandatory = false,
            mIsAutoJournalActived = false,
            mIsOutgoingPaymentPaidToBankBasedOnVendor = false,
            mIsBankAccountCurrencyValidationInactive = false,
            mIsVoucherBankAccountFilteredByGrp = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsBudget2YearlyFormat = false,
            mIsCASUsedForBudget = false,
            mIsVRForBudgetUseAvailableBudget = false,
            mIsOutgoingPaymentUseBudget = false,
            mIsDOCtAllowMultipleItemCategory = false
            ;
            
        internal bool 
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsPITotalWithoutTaxInclDownpaymentEnabled = false;
        private List<LocalDocument> mlLocalDocument = null;
        private string 
            mVoucherCodeFormatType = "1",
            mAcNoForCurrentAccountsPayable = string.Empty,
            mMainCurCode = string.Empty,
            mBankAccountFormat = "0",
            mMRAvailableBudgetSubtraction = string.Empty,
            mBudgetBasedOn = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mOutgoingPaymentCOAAmtCalculationMethod = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmOutgoingPayment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Outgoing Payment";

            try
            {
                mlLocalDocument = new List<LocalDocument>();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                if (!mIsOutgoingPaymentUseBudget) TcOutgoingPayment.TabPages.Remove(TpBudget);
                else TcOutgoingPayment.SelectedTabPage = TpBudget;

                TcOutgoingPayment.SelectedTabPage = TpAdditional;
                Sl.SetLueBankCode(ref LueBankCode);

                TcOutgoingPayment.SelectedTabPage = TpGeneral;
                SetLueVdCode(ref LueVdCode, "");
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                if (mBankAccountFormat == "0" || mBankAccountFormat.Length == 0)
                    Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                else
                    SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueEntCode(ref LueEntCode);
                TpCOA.PageVisible = mIsOutgoingPaymentUseCOA;
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;
                if (!mIsOutgoingPaymentPaidToBankBasedOnVendor)
                    Sl.SetLueBankCode(ref LuePaidToBankCode);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                if (!mIsOutgoingPaymentUsePIWithCOA)
                {
                    LblAmt3.Visible     = TxtAmt3.Visible = false;
                    label8.Location     = new System.Drawing.Point(146, 176);
                    LueCurCode.Location = new System.Drawing.Point(206, 173);
                    label9.Location     = new System.Drawing.Point(112, 198);
                    TxtRateAmt.Location = new System.Drawing.Point(206, 195);
                    label25.Location    = new System.Drawing.Point(7, 220);
                    TxtAmt2.Location    = new System.Drawing.Point(206, 217);
                    label5.Location     = new System.Drawing.Point(154, 242);
                    MeeRemark.Location  = new System.Drawing.Point(206, 239);
                    label11.Text        = "Paid Amount (Purchase Invoice)";
                    label11.Location    = new System.Drawing.Point(18, 153);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Invoice#",
                        "Date",
                        "", 
                        "Type",

                        //6-10
                        "Type",
                        "Vendor Code",
                        "Vendor",
                        "Currency",    
                        "Outstanding"+Environment.NewLine+"Amount",

                        //11-15
                        "Amount",
                        "Due Date",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        "Voucher Request's Description",
                        "Dept Code",

                        //16-18
                        "Department",
                        "Local#",
                        "Amount"+Environment.NewLine+"Without COA",

                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        20, 130, 80, 20, 0,
                        
                        //6-10
                        130, 0, 250, 70, 100, 
                        
                        //11-15
                        100, 100, 130, 400, 0,

                        //16-18
                        150, 180, 130
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 12 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 5, 7, 15, 18 }, false);
            if (!mIsOutgoingPaymentApprovalNeedDept)
                Sm.GrdColInvisible(Grd1, new int[] { 16 }, false);
            Grd1.Cols[17].Move(3);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 17 }, false);
            if (mIsOutgoingPaymentUsePIWithCOA)
            {
                Grd1.Cols[18].Move(13);
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, true);
            }

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        150, 
                        100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 0;

            Sm.GrdHdrWithColWidth(
                    Grd3, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Bank Code", 
                        "Bank Name", 
                        "Giro#",
                        "Due Date",
                        "Currency",

                        //6
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-5
                        0, 250, 150, 100, 80,

                        //6
                        130
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdFormatDate(Grd3, new int[] { 4 });
            Sm.GrdFormatDec(Grd3, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd3, new int[] { 1 });
            Sm.SetGrdProperty(Grd3, false);

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6
                        "Duplicated"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6
                        0
                    }
                );
            Sm.GrdColCheck(Grd4, new int[] { 6 });
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd4, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 1, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1, 2, 6 });

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 11;
            Grd5.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Invoice#", 
                        "Invoice DNo", 
                        "Item's Code",
                        "Item's Name",
                        "Total Price"+Environment.NewLine+"After Tax", 

                        //6-10
                        "Downpayment",
                        "Disc, Cost, Etc",
                        "Outstanding Amount"+Environment.NewLine+"Before",
                        "Outstanding"+Environment.NewLine+"Amount",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        180, 120, 120, 300, 150, 

                        //6-9
                        150, 150, 150, 150, 150
                    }
                );
            Sm.GrdColButton(Grd5, new int[] { 0 });
            Sm.GrdColReadOnly(Grd5, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd5, new int[] { 5, 6, 7, 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd5, new int[] { 2, 3, 5, 6, 7, 8 }, false);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocalDocNo, DteDocDt, ChkCancelInd, LueVdCode, MeeVoucherRequestSummaryDesc, 
                        ChkMeeVoucherRequestSummaryInd, MeeRemark, LueAcType, LueBankAcCode, LuePaymentType, 
                        LueBankCode, TxtGiroNo, DteDueDt, LueCurCode, TxtRateAmt, 
                        TxtAmt, TxtAmt2, MeeCancelReason, TxtPaymentUser, LuePaidToBankCode, 
                        TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LueEntCode, TxtAmt3, LueBCCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 11, 14 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 10 });
                    Sm.GrdColInvisible(Grd5, new int[] { 9 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt,  LueVdCode, TxtPaymentUser, LuePaymentType, LueBankAcCode, //LueCurCode, 
                        TxtRateAmt, MeeRemark, MeeVoucherRequestSummaryDesc, ChkMeeVoucherRequestSummaryInd, LueEntCode, LueBCCode
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    if (mIsBankAccountCurrencyValidationInactive)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCurCode }, false);
                    if (!mIsOutgoingPaymentPaidToBankBasedOnVendor)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo }, false);
                    
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 14});
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 3, 4, 5 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 10 });
                    Sm.GrdColInvisible(Grd5, new int[] { 9 }, true);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false); 
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, TxtStatus, LueVdCode, 
                TxtVoucherRequestDocNo, MeeVoucherRequestSummaryDesc, ChkMeeVoucherRequestSummaryInd, TxtVoucherDocNo, MeeRemark, 
                LueAcType, LueBankAcCode, TxtBankAcTp, LuePaymentType, LueBankCode, 
                TxtGiroNo, DteDueDt, LueCurCode, TxtPaymentUser, LuePaidToBankCode, 
                TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, MeeCancelReason, TxtVoucherDocNo2, 
                TxtVoucherRequestDocNo2, MeeCancelReason, LueEntCode, TxtJournalDocNo, TxtJournalDocNo2, LueBCCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtRateAmt, TxtAmt, TxtAmt2, TxtGiroAmt, TxtCOAAmt, TxtRemainingBudget,
              TxtAmt3 }, 0);
            ChkCancelInd.Checked = ChkMeeVoucherRequestSummaryInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 18 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 6 });
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 3, 4 });
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 5, 6, 7, 8, 9, 10 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOutgoingPayment2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                SetLueVdCode(ref LueVdCode, "");
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetLue(LueAcType, "C");
                if(mIsOutgoingPaymentUseBudget) SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetGrdStr(Grd1, 0, 15));
                TxtRateAmt.EditValue = Sm.FormatNum(1, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mEntCode = Sm.GetLue(LueEntCode);
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval())|| Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "OutgoingPaymentHdr", "OutgoingPaymentDtl", "OutgoingPaymentDtl2", "OPSignIMS", "OPSignIMS2" };

            string mDocTitle = Sm.GetParameter("DocTitle");

            var l = new List<OutgoingPaymentHdr>();
            var ldtl = new List<OutgoingPaymentDtl>();
            var ldtl2 = new List<OutgoingPaymentDtl2>();
            var lDtlS = new List<OPSignIMS>();
            var lDtlS2 = new List<OPSignIMS2>();

            List<IList> myLists = new List<IList>();

            #region header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, J.CompanyName, J.CompanyPhone, J.CompanyFax, J.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyPhone, ");
            }

            SQL.AppendLine(" H.DocNo, A.DocNo As VRDocNo, DATE_FORMAT(H.DocDt,'%d %M %Y') As DocDt, DATE_FORMAT(A.DocDt,'%d %M %Y') As VRDocDt, A.CancelInd, A.DocType, A.AcType, A.VoucherDocNo, ");
            SQL.AppendLine(" (Select DATE_FORMAT(DocDt,'%d %M %Y') As DocDt from tblvoucherhdr Where DocNo=A.VoucherDocNo Limit 1) As VoucherDocDt, ");
            SQL.AppendLine(" Concat(IfNull(C.BankAcNo, ''), ' [', IfNull(C.BankAcNm, ''), ']') As BankAcc, ");
            SQL.AppendLine(" (Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine(" A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, A.Amt As AmtHdr, A.Remark As RemarkHdr, A.CurCode, A.DocEnclosure, ");
            SQL.AppendLine(" ifnull(Concat(A.PaymentUser, '     ', I.VdName), ifnull(I.VdName, A.PaymentUser)) As PaymentUser, G.DeptName, I.VdName, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='isfilterbysite') As SiteInd, C.BankAcNo,  H.PaidToBankAcName, H.PaidToBAnkAcNo, H.PaidToBankBranch, K.ProjectName ");
            SQL.AppendLine(" From TblVoucherRequestHdr A ");
            SQL.AppendLine(" Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine(" Left Join TblUser F On A.PIC=F.UserCode ");
            SQL.AppendLine(" Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine(" left Join TblOutgoingPaymentHdr H On A.DocNo = H.VoucherRequestDocNo ");
            SQL.AppendLine(" left Join TblVendor I On H.VdCode = I.VdCode ");

            SQL.AppendLine(" Left Join  ");
            SQL.AppendLine(" ( ");
	        SQL.AppendLine("     Select X1.DocNo, Group_Concat(Distinct IFNULL(X11.ProjectName, X10.ProjectName)) ProjectName ");
	        SQL.AppendLine("     From tbloutgoingpaymentdtl X1 ");
            SQL.AppendLine("     Inner Join tblpurchaseinvoicedtl X2 ON X1.InvoiceDocNo = X2.DocNo  ");
            SQL.AppendLine("     Inner Join tblrecvvddtl X3 ON X2.RecvVdDocNo = X3.DocNo AND X2.RecvVdDNo = X3.DNo ");
            SQL.AppendLine("     Inner Join tblpodtl X4 ON X3.PODocNo = X4.DocNo AND X3.PODno = X4.DNo ");
            SQL.AppendLine("     Inner Join tblporequestdtl X5 ON X4.PORequestDocNo = X5.DocNo AND X4.PORequestDno = X5.DNo ");
            SQL.AppendLine("     Inner Join tblmaterialrequestdtl X6 ON X5.MaterialRequestDocNo = X6.DocNo AND X5.MaterialRequestDNo = X6.DNo ");
            SQL.AppendLine("     Inner Join tblmaterialrequesthdr X7 ON X6.DocNo = X7.DocNo  ");
            SQL.AppendLine("     Inner Join tblsocontracthdr X8 ON X7.SOCDocNo = X8.DocNo ");
            SQL.AppendLine("     Inner Join tblboqhdr X9 ON X8.BOQDocNo = X9.DocNo ");
            SQL.AppendLine("     Inner Join tbllophdr X10 ON X9.LOPDocNo = X10.DocNo ");
            SQL.AppendLine("     Left Join tblprojectgroup X11 ON X10.PGCode = X11.PGCode ");
	        SQL.AppendLine("     Group By X1.DocNo ");
            SQL.AppendLine(" ) K ON H.DocNo = K.DocNo ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, F.EntName As CompanyName, F.EntPhone As CompanyPhone, F.EntFax As CompanyFax, F.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblVoucherRequestHdr A ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr A1 On A.DocNo = A1.VoucherRequestDocNo ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl B On A1.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr C On B.InvoiceDocNo=C.DocNo ");
                SQL.AppendLine("    Inner Join TblSite D On C.SiteCode = D.SiteCode ");
                SQL.AppendLine("    Inner Join TblProfitCenter E On  D.ProfitCenterCode=E.ProfitCenterCode ");
                SQL.AppendLine("    Inner Join TblEntity F On E.EntCode = F.EntCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(")J On A.DocNo = J.DocNo ");
            }
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select F.EntLogoName " +
                       "From TblVoucherRequestHdr A " +
                       "Inner Join TblOutgoingPaymentHdr A1 On A.DocNo = A1.VoucherRequestDocNo " +
                       "Inner Join TblOutgoingPaymentDtl B On A1.DocNo=B.DocNo " +
                       "Inner Join TblPurchaseInvoiceHdr C On B.InvoiceDocNo=C.DocNo " +
                       "Inner Join TblSite D On C.SiteCode = D.SiteCode " +
                       "Inner Join TblProfitCenter E On  D.ProfitCenterCode=E.ProfitCenterCode " +
                       "Inner Join TblEntity F On E.EntCode = F.EntCode " +
                       "Where A.Docno='" + TxtVoucherRequestDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo", 
                         "DocDt", 
                         "CancelInd",
 
                         //6-10
                         "DocType", 
                         "AcType",
                         "VoucherDocNo", 
                         "VoucherDocDt", 
                         "BankAcc", 
                         
                         //11-15
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName", 
                         "GiroDueDt", 
                         "PICName",
 
                         //16-20
                         "AmtHdr", 
                         "RemarkHdr",
                         "CompanyLogo",
                         "CurCode",
                         "DocEnclosure",

                         //21-25
                         "PaymentUser",
                         "DeptName",
                         "VRDocNo",
                         "VRDocDt",
                         "VdName",

                         //26-30
                         "SiteInd",
                         "BankAcNo", 
                         "PaidToBankAcName",
                         "PaidToBAnkAcNo",
                         "PaidToBankBranch",

                         //31
                         "ProjectName"


                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OutgoingPaymentHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),

                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            CancelInd = Sm.DrStr(dr, c[5]),
                            DocType = Sm.DrStr(dr, c[6]),
                            AcType = Sm.DrStr(dr, c[7]),
                            VoucherDocNo = Sm.DrStr(dr, c[8]),
                            VoucherDocDt = Sm.DrStr(dr, c[9]),
                            BankAcc = Sm.DrStr(dr, c[10]),
                            PaymentType = Sm.DrStr(dr, c[11]),
                            GiroNo = Sm.DrStr(dr, c[12]),
                            GiroBankName = Sm.DrStr(dr, c[13]),
                            GiroDueDt = Sm.DrStr(dr, c[14]),
                            PICName = Sm.DrStr(dr, c[15]),
                            AmtHdr = (mDocTitle == "KIM" && Sm.DrDec(dr, c[16])>0) ? Decimal.Truncate(Sm.DrDec(dr, c[16])) : Sm.DrDec(dr, c[16]),
                            Terbilang = (mDocTitle == "KIM" && Sm.DrDec(dr, c[16]) > 0) ? Sm.Terbilang(Decimal.Truncate(Sm.DrDec(dr, c[16]))) : Sm.Terbilang(Sm.DrDec(dr, c[16])),
                            Terbilang2 = (mDocTitle == "KIM" && Sm.DrDec(dr, c[16]) > 0) ? Convert(Decimal.Truncate(Sm.DrDec(dr, c[16]))) : Convert(Sm.DrDec(dr, c[16])),
                            RemarkHdr = Sm.DrStr(dr, c[17]),

                            CompanyLogo = Sm.DrStr(dr, c[18]),
                            CurCode = Sm.DrStr(dr, c[19]),
                            DocEnclosure = Sm.DrDec(dr, c[20]),
                            PaymentUser = Sm.DrStr(dr, c[21]),
                            Department = Sm.DrStr(dr, c[22]),
                            VRDocNo = Sm.DrStr(dr, c[23]),
                            VRDocDt = Sm.DrStr(dr, c[24]),
                            VdName = Sm.DrStr(dr, c[25]),
                            SiteInd = Sm.DrStr(dr, c[26]),
                            BankAcNo = Sm.DrStr(dr, c[27]),
                            PaidToBankAcName = Sm.DrStr(dr, c[28]),
                            PaidToBAnkAcNo = Sm.DrStr(dr, c[29]),
                            PaidToBankBranch = Sm.DrStr(dr, c[30]),
                            BankName = LueBankCode.Text,
                            Currency = LueCurCode.Text,

                            Project = Sm.DrStr(dr, c[31]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region detail

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select B.DNo, Concat(F.Description, ' ', ifnull(Concat('(', D.VdInvNo, ')'), '')) As Description, F.Description As Description2,  (B.Amt*A.RateAmt) As Amt, B.Remark, B.InvoiceDocNo,  DATE_FORMAT(D.DocDt,'%d %M %Y') As InvoiceDocDt " +
                "From TblOutgoingPaymentHdr A " +
                "Inner Join TblOutgoingPaymentDtl B On A.DocNo=B.DocNo " +
                "Inner Join TblVendor C On A.VdCode=C.VdCode " +
                "Left Join TblPurchaseInvoiceHdr D On B.InvoiceDocNo=D.DocNo " +
                "Left Join TblVoucherRequestDtl F On F.DocNo = A.VoucherRequestDocNo And F.Dno=B.Dno " +
                "Where A.DocNo=@DocNo Order By B.DNo ";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "Description",
                         "Amt",
                         "Remark",
                         "InvoiceDocNo",
                         "InvoiceDocDt",

                         //6
                         "Description2",
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new OutgoingPaymentDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            Description = (mDocTitle == "IMS") ? Sm.DrStr(drDtl, cDtl[6]) : Sm.DrStr(drDtl, cDtl[1]),
                            Amt = (mDocTitle == "KIM") ? Decimal.Truncate(Sm.DrDec(drDtl, cDtl[2])) : Sm.DrDec(drDtl, cDtl[2]),
                            Remark = Sm.DrStr(drDtl, cDtl[3]),
                            InvoiceDocNo = Sm.DrStr(drDtl, cDtl[4]),
                            InvoiceDocDt = Sm.DrStr(drDtl, cDtl[5])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl2.AppendLine("From ( ");
                SQLDtl2.AppendLine("    Select Distinct ");
                SQLDtl2.AppendLine("    B.UserCode, ");
                if (mUserType)
                    SQLDtl2.AppendLine(" C.UserName, ");
                else
                SQLDtl2.AppendLine("    Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl2.AppendLine("    From TblOutgoingPaymentdtl A ");
                SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='outgoingpayment' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'outgoingpayment' ");
                SQLDtl2.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine("    Union All ");
                SQLDtl2.AppendLine("    Select Distinct ");
                SQLDtl2.AppendLine("    A.CreateBy As UserCode, ");
                if (mUserType)
                    SQLDtl2.AppendLine(" B.UserName, ");
                else
                    SQLDtl2.AppendLine("    Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                SQLDtl2.AppendLine("    '00' As DNo, 0 As Level, 'Prepared By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl2.AppendLine("    From TblOutgoingPaymentdtl A ");
                SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl2.AppendLine("    Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl2.AppendLine("Order By T1.DNo desc; "); //Order By T1.DNo, T1.level;


                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {

                        ldtl2.Add(new OutgoingPaymentDtl2()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                            Space = Sm.DrStr(drDtl2, cDtl2[3]),
                            DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                            Title = Sm.DrStr(drDtl2, cDtl2[5]),
                            LastUpDt = Sm.DrStr(drDtl2, cDtl2[6])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                #region Old Code
                /*
                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblOutgoingPaymentHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                */
                #endregion

                // Pelaksana
                SQLDtlS.AppendLine("Select ");
                SQLDtlS.AppendLine("(Select ParValue From TblParameter Where Parcode = 'OutgoingPaymentPrintoutSignLabel' ) As Label1, ");
                SQLDtlS.AppendLine("B.UserName, D.PosName, ");
                SQLDtlS.AppendLine("(Select ParValue From TblParameter Where Parcode = 'OutgoingPaymentPrintoutSignLabel2' ) As Label2, ");
                SQLDtlS.AppendLine("E.UserName UserName2, E.PosName PosName2, ");
                SQLDtlS.AppendLine("(Select ParValue From TblParameter Where Parcode = 'OutgoingPaymentPrintoutSignLabel3' ) As Label3, ");
                SQLDtlS.AppendLine("(Select ParValue From TblParameter Where Parcode = 'OutgoingPaymentPrintoutSignName' ) As UserName3, ");
                SQLDtlS.AppendLine("(Select ParValue From TblParameter Where Parcode = 'OutgoingPaymentPrintoutSignPosition' ) As PosName3 ");
                SQLDtlS.AppendLine("From TblOutgoingPaymentHdr A ");
                SQLDtlS.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtlS.AppendLine("Left Join TblEmployee C On B.UserCode = C.EmpCode ");
                SQLDtlS.AppendLine("Left Join TblPosition D On C.PosCode = D.PosCode ");
                SQLDtlS.AppendLine("Left Join ");
                SQLDtlS.AppendLine("( ");
                SQLDtlS.AppendLine("    Select C.UserName, E.PosName, A1.DocNo ");
                SQLDtlS.AppendLine("    From TblOutgoingPaymentHdr A1 ");
                SQLDtlS.AppendLine("    Inner Join TblDocApproval A On A1.Docno = A.DocNo And A.DocNo = @DocNo ");
                SQLDtlS.AppendLine("    Inner Join TblDocApprovalSetting B On A.DocType = B.DocType And A.ApprovalDNo = B.DNo And B.`Level` = '1' ");
                SQLDtlS.AppendLine("    Inner Join TblUser C On A.LastUpBy = C.UserCode ");
                SQLDtlS.AppendLine("    Left Join TblEmployee D On C.UserCode = D.EmpCode ");
                SQLDtlS.AppendLine("    Left Join TblPosition E On D.PosCode = E.PosCode ");
                SQLDtlS.AppendLine(") E On A.DocNo = E.DocNo ");
                SQLDtlS.AppendLine("Where A.DocNo = @DocNo ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Label1" ,

                         //1-5
                         "UserName",
                         "PosName",
                         "Label2",
                         "UserName2",
                         "PosName2",

                         //6-8
                         "Label3",
                         "UserName3",
                         "PosName3"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new OPSignIMS()
                        {
                            Label1 = Sm.DrStr(drDtlS, cDtlS[0]),
                            Username = Sm.DrStr(drDtlS, cDtlS[1]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),

                            Label2 = Sm.DrStr(drDtlS, cDtlS[3]),
                            Username2 = Sm.DrStr(drDtlS, cDtlS[4]),
                            PosName2 = Sm.DrStr(drDtlS, cDtlS[5]),

                            Label3 = Sm.DrStr(drDtlS, cDtlS[6]),
                            Username3 = Sm.DrStr(drDtlS, cDtlS[7]),
                            PosName3 = Sm.DrStr(drDtlS, cDtlS[8]),
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);

            //Disetujui Oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;

                #region old code
                /*
                SQLDtlS2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS2.AppendLine(" T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, T1.Remark  ");
                SQLDtlS2.AppendLine("From (  ");
                SQLDtlS2.AppendLine("   Select Distinct  ");
                SQLDtlS2.AppendLine("   B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName,  ");
                SQLDtlS2.AppendLine("   B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt, 'Disetujui' As Remark  ");
                SQLDtlS2.AppendLine("   From tbloutgoingpaymenthdr A  ");
                SQLDtlS2.AppendLine("   Inner Join TblDocApproval B On B.DocType='OutgoingPayment' And A.DocNo=B.DocNo   ");
                SQLDtlS2.AppendLine("   Inner Join TblUser C On B.UserCode=C.UserCode  ");
                SQLDtlS2.AppendLine("   Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'OutgoingPayment' ");
                SQLDtlS2.AppendLine("   Left Join TblGroup E On C.GrpCode=E.GrpCode  ");
                SQLDtlS2.AppendLine("     Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtlS2.AppendLine(") T1  ");
                SQLDtlS2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode  ");
                SQLDtlS2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode  ");
                SQLDtlS2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature'  ");
                //SQLDtlS2.AppendLine("Where T1.Level >= 2  ");
                SQLDtlS2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName  ");
                SQLDtlS2.AppendLine("Order By T1.Level ");
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                */
                #endregion

                // Otorisasi
                SQLDtlS2.AppendLine("Select B.UserName, C.UserName UserName2 ");
                SQLDtlS2.AppendLine("From TblOutgoingPaymentHdr A1 ");
                SQLDtlS2.AppendLine("Left Join ");
                SQLDtlS2.AppendLine("( ");
                SQLDtlS2.AppendLine("    Select C.UserName, A1.DocNo ");
                SQLDtlS2.AppendLine("    FROM TblOutgoingPaymentHdr A1 ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApproval A On A1.DocNo=A.DocNo And A.DocNo=@DocNo ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.`Level`='2' ");
                SQLDtlS2.AppendLine("    Inner Join TblUser C On A.LastUpBy=C.UserCode ");
                SQLDtlS2.AppendLine(") B On A1.DocNo=B.DocNo ");
                SQLDtlS2.AppendLine("Left Join ");
                SQLDtlS2.AppendLine("( ");
                SQLDtlS2.AppendLine("    Select C.UserName, A1.DocNo ");
                SQLDtlS2.AppendLine("    FROM TblOutgoingPaymentHdr A1 ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApproval A On A1.DocNo=A.DocNo And A.DocNo=@DocNo ");
                SQLDtlS2.AppendLine("    Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo And B.`Level`='3' ");
                SQLDtlS2.AppendLine("    Inner Join TblUser C On A.LastUpBy=C.UserCode ");
                SQLDtlS2.AppendLine(") C On A1.DocNo=C.DocNo ");
                SQLDtlS2.AppendLine("Where A1.DocNo = @DocNo ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "Username" ,

                         //1
                         "Username2" ,
                        });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new OPSignIMS2()
                        {
                            Username = Sm.DrStr(drDtlS2, cDtlS2[0]),
                            Username2 = Sm.DrStr(drDtlS2, cDtlS2[1]),
                        });
                    }
                }
                drDtlS2.Close();
            }
            myLists.Add(lDtlS2);


            #endregion

            Sm.PrintReport(mIsPrintOutOP, myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmOutgoingPayment2Dlg(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice(mMenuCode);
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0 && !IsInvoiceEmpty(e) && Sm.IsGrdColSelected(new int[] { 4, 13 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                RemoveInvoiceItems();
                ComputeGiroAmt();
                ComputeAmt();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                Sm.FormShowDialog(new FrmOutgoingPayment2Dlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice(mMenuCode);
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 14 }, e);
            if (e.ColIndex == 11)
            {
                ComputeGiroAmt();
                ComputeAmt();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid() 
            ) return;

            Cursor.Current = Cursors.WaitCursor;

            string SubCategory = Sm.GetValue("Select Distinct ItScCode From TblPurchaseInvoiceHdr Where DocNo = '"+ Sm.GetGrdStr(Grd1, 0, 2)+"' ");
            string DocNo = string.Empty;
            //string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OutgoingPayment", "TblOutgoingPaymentHdr");
            if (mDocNoFormat == "1")
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OutgoingPayment", "TblOutgoingPaymentHdr");
            }
            else
            {
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "OutgoingPayment", "TblOutgoingPaymentHdr", mEntCode, "1");
            }
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                if (mDocNoFormat == "1")
                    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
            else
                if (mDocNoFormat == "1")
                    VoucherRequestDocNo = GenerateVoucherRequestDocNo("1");
                else
                    VoucherRequestDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "1");
            

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;

            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;


            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count != 0)
                {
                    if (IsLocalDocumentNotValid(
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr
                        )) return;

                    SetRevision(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );

                    if (SeqNo.Length > 0)
                    {
                        SetLocalDocNo(
                            "OutgoingPayment",
                            ref LocalDocNo,
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr,
                            ref Revision
                            );
                    }
                    mlLocalDocument.Clear();
                }
            }

            string LocalDocNoVcr = LocalDocNo.Replace(
                Sm.GetValue("Select DocAbbr From tblDocAbbreviation Where DocType = 'OutgoingPayment'"),
                Sm.GetValue("Select DocAbbr From tblDocAbbreviation Where DocType = 'VoucherRequest'")
                ); 
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveOutgoingPaymentHdr(DocNo, VoucherRequestDocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
                    cml.Add(SaveOutgoingPaymentDtl(DocNo, Row));

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) 
                    cml.Add(SaveOutgoingPaymentDtl2(DocNo, Row));

            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                    cml.Add(SaveOutgoingPaymentDtl4(DocNo, Row));

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo, LocalDocNoVcr, SeqNo, ItSCCode, Mth, Yr, Revision));

            if (!mIsOutgoingPaymentAmtUseCOAAmt)
            {
                if (!ChkMeeVoucherRequestSummaryInd.Checked)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, Row));
                }
            }
            else
            {
                cml.Add(SaveVoucherRequestDtlForCOAAmt(VoucherRequestDocNo));
            }

            cml.Add(UpdatePurchaseInvoiceHdr(DocNo, 1));
            cml.Add(UpdatePurchaseReturnInvoiceHdr(DocNo, 1));

            // kalau user input COA
            if (mIsOutgoingPaymentUseCOA)
            {
                if (Grd4.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) cml.Add(SaveOutgoingPaymentDtl3(DocNo, Row));

                    if (!mIsOutgoingPaymentAmtUseCOAAmt)
                    {
                        var VoucherRequestDocNo2 = string.Empty;

                        if (mVoucherCodeFormatType == "2")
                            if (mDocNoFormat == "1")
                                VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "2");
                            else
                                VoucherRequestDocNo2 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "2");
                        else
                            if (mDocNoFormat == "1")
                                VoucherRequestDocNo2 = GenerateVoucherRequestDocNo("2");
                            else
                                VoucherRequestDocNo2 = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mEntCode, "2");

                        cml.Add(SaveVoucherRequestFromCOAOutgoingPayment(VoucherRequestDocNo2, DocNo));
                    }
                }
            }
            //end kalau user input COA

            if (mIsAutoJournalActived && decimal.Parse(TxtGiroAmt.Text) != 0)
                    cml.Add(SaveJournal(DocNo));
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblOutgoingPaymentHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 2));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private bool IsInsertedDataNotValid()
        {
            mDeptCode = string.Empty;
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mIsRemarkForApprovalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                IsBudgetCategoryInvalid() ||
                IsAmountNotValid() ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsInvoiceAlreadyCancelled() ||
                IsVendorNotValid() ||
                IsCurrencyNotValid() ||
                IsBankAccountCurrencyNotValid() ||
                IsOutstandingAmtNotValid() ||
                IsDeptCodeNotValid() ||
                IsDateNotValid() ||
                IsGiroNoInValid("N") ||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                IsEntCodeNotValid() ||
                IsItemCategoryInvalid() ||
                IsDuplicateCOANotHaveRemark() ||
                IsGrd5Empty() ||
                IsInvoiceItemAmtNotValid() ||
                IsCOAAmtNotValid()
                ;
        }

        private bool IsBudgetCategoryInvalid()
        {
            if (!mIsOutgoingPaymentUseBudget) return false;

            if (Sm.GetLue(LueBCCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Budget Category is empty.");
                TcOutgoingPayment.SelectedTabPage = TpBudget;
                LueBCCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            if (!mIsOutgoingPaymentUseBudget) return false;

            ComputeRemainingBudget();
            if (Decimal.Parse(TxtAmt.Text) > Decimal.Parse(TxtRemainingBudget.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Amount should not be greater than available budget.");
                TxtAmt.Focus();
                return true;
            }

            return false;
        }

        private bool IsCOAAmtNotValid()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int r = 0; r < Grd4.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 3).Length > 0) Debit += Sm.GetGrdDec(Grd4, r, 3);
                if (Sm.GetGrdStr(Grd4, r, 4).Length > 0) Credit += Sm.GetGrdDec(Grd4, r, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "COA's Account List" + Environment.NewLine +
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }

            return false;
        }

        private bool IsDuplicateCOANotHaveRemark()
        {
            if (!mIsCOACouldBeChosenMoreThanOnce) return false;
            if (Grd4.Rows.Count <= 1) return false;

            GetDuplicateCOAIndicator();

            for (int i = 0; i < Grd4.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd4, i, 6) && Sm.GetGrdStr(Grd4, i, 5).Length == 0)
                {
                    TcOutgoingPayment.SelectedTabPage = TpCOA;
                    Sm.StdMsg(mMsgType.Warning, "You need to fill this remark due to account duplication.");
                    Sm.FocusGrd(Grd4, i, 5);
                    return true;
                }
            }

            return false;
        }

        private bool IsItemCategoryInvalid()
        {
            if (mIsItemCategoryUseCOAAPAR && mIsAutoJournalActived)
            {
                if (mIsDOCtAllowMultipleItemCategory) return false;

                string InvDocNo = string.Empty;
                string ItCtCode = string.Empty;
                string[] ItCtCodes = { };

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (InvDocNo.Length > 0) InvDocNo += ",";
                    InvDocNo += Sm.GetGrdStr(Grd1, i, 2);
                }

                if (InvDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Group_Concat(Distinct IfNull(C.ItCtCode, '')) ");
                    SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
                    SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo = B.DocNo And A.RecvVdDNo = B.DNo ");
                    SQL.AppendLine("    And Find_In_Set(A.DocNo, @Param) ");
                    SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");

                    ItCtCode = Sm.GetValue(SQL.ToString(), InvDocNo);

                    ItCtCodes = ItCtCode.Split(',');

                    if (ItCtCodes.Length > 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Different item category detected. This will affect the journal process (COA AR).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsEntCodeNotValid()
        {
            var EntCode = Sm.GetLue(LueEntCode);
            if (EntCode.Length > 0)
            {
                if (!Sm.IsDataExist(
                    "Select 1 From TblBankAccount " +
                    "Where BankAcCode=@Param1 " +
                    "And EntCode Is Not Null " +
                    "And EntCode=@Param2;",
                    Sm.GetLue(LueBankAcCode), EntCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invalid entity." + Environment.NewLine +
                        "Voucher request's entity should be the same as bank account's entity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGiroNoInValid(string ActInd)
        {
            if (Grd3.Rows.Count > 0)
            {
                var CurCode = Sm.GetGrdStr(Grd1, 0, 9);
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (ActInd=="N" && IsGiroCurrencyInValid(CurCode, Row)) return true;
                        if (IsGiroNoInValid(ActInd, Row)) return true;
                    }
                }
            }
            return false;
        }

        private bool IsGiroNoInValid(string ActInd, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ActInd ");
            SQL.AppendLine("From TblGiroSummary ");
            SQL.AppendLine("Where BusinessPartnerCode=@BusinessPartnerCode ");
            SQL.AppendLine("And BusinessPartnerType='1' ");
            SQL.AppendLine("And BankCode=@BankCode ");
            SQL.AppendLine("And GiroNo=@GiroNo ");
            SQL.AppendLine("And ActInd=@ActInd; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@GiroNo", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine +
                    "Giro# : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                    "invalid Giro#."
                    );
                Sm.FocusGrd(Grd3, Row, 2);
                return true;
            }

            return false;
        }

        private bool IsGiroCurrencyInValid(string CurCode, int Row)
        {
            if (CurCode.Length != 0 &&
                !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd3, Row, 5)))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine +
                    "Giro# : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine +
                    "Currency : " + Sm.GetGrdStr(Grd3, Row, 5) + Environment.NewLine +
                    "invalid Giro's currency."
                    );
                Sm.FocusGrd(Grd3, Row, 2);
                return true;
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 3)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 3);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Date should be the same or greater than Invoice Date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 invoice document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Purchase Invoice / Purchase Return Invoice document number is empty.") ||
                    (!ChkMeeVoucherRequestSummaryInd.Checked && Sm.IsGrdValueEmpty(Grd1, Row, 14, false, "Description for Voucher Request is empty.")))
                    return true;
            }
            return false;
        }

        private bool IsInvoiceAlreadyCancelled()
        {
            string mInvoiceDocNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mInvoiceDocNo.Length > 0) mInvoiceDocNo += ",";
                    mInvoiceDocNo += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            if (mInvoiceDocNo.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.DocNo ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("    Where Find_In_Set(DocNo, @Param) ");
                if (mIsPurchaseInvoiceUseApproval) SQL.AppendLine("    And (CancelInd = 'Y' Or Status = 'C') ");
                else SQL.AppendLine("    And CancelInd = 'Y' ");
                
                SQL.AppendLine("    Union All ");
                
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr ");
                SQL.AppendLine("    Where Find_In_Set(DocNo, @Param) ");
                SQL.AppendLine("    And CancelInd = 'Y' ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), mInvoiceDocNo))
                {
                    Sm.StdMsg(mMsgType.Warning, "This invoice#" + Sm.GetValue(SQL.ToString(), mInvoiceDocNo) + " is cancelled.");
                    return true;
                }
            }

            return false;
        }

        private bool IsVendorNotValid()
        {
            bool NotValid = false;
            string VdCode = Sm.GetLue(LueVdCode);

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0 &&
                        !Sm.CompareStr(VdCode, Sm.GetGrdStr(Grd1, Row, 7)))
                    {
                        NotValid = true;
                        break;
                    }
                    VdCode = Sm.GetGrdStr(Grd1, Row, 7);
                }
            }
            if (NotValid) Sm.StdMsg(mMsgType.Warning, "Only allowed 1 vendor in 1 invoice.");
            return NotValid;
        }

        private bool IsCurrencyNotValid()
        {
            bool NotValid = false;
            string CurCode = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 9).Length != 0 && 
                        CurCode.Length != 0 && 
                        !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 9)))
                        {
                            NotValid = true;
                            break;
                        }
                    CurCode = Sm.GetGrdStr(Grd1, Row, 9);
                }
            }
            if (NotValid) Sm.StdMsg(mMsgType.Warning, "One invoice document only allowed 1 currency type.");
            return NotValid;
        }

        private bool IsBankAccountCurrencyNotValid()
        {
            if (mIsBankAccountCurrencyValidationInactive) return false;

            var CurCode1 = Sm.GetValue("Select CurCode From TblBankAccount Where CurCode Is Not Null And BankAcCode=@Param;", Sm.GetLue(LueBankAcCode));
            var CurCode2 = Sm.GetLue(LueCurCode);
            
            if (!Sm.CompareStr(CurCode1, CurCode2)) 
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Bank account's currency : " + CurCode1 + Environment.NewLine +
                    "Outgoing payment's currency : " + CurCode2 + Environment.NewLine + Environment.NewLine +
                    "Both should have the same currency.");
                return true;
            }
            return false;
        }

        private bool IsOutstandingAmtNotValid()
        {
            ReComputeOutstandingAmt();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 && 
                    Sm.GetGrdDec(Grd1, Row, 11) > Sm.GetGrdDec(Grd1, Row, 10))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invoice Document Number : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Document Type : Purchase " + (Sm.CompareStr(Sm.Right(Sm.GetGrdStr(Grd1, Row, 5), 1), "1") ? "" : "Return") + " Invoice" + Environment.NewLine +
                        "Outstanding Amount : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Outgoing Payment Amount : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +Environment.NewLine +
                        "Outgoing payment amount is bigger than outstanding amount."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet#/Cheque# ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsDeptCodeNotValid()
        {
            if (!mIsOutgoingPaymentApprovalNeedDept) return false;

            string DeptCode = Sm.GetGrdStr(Grd1, 0, 15);

            if (Sm.GetGrdStr(Grd1, 0, 15).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Invoice# : " + Sm.GetGrdStr(Grd1, 0, 2) + Environment.NewLine +
                    "Department is empty."
                );
                return true;
            }

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (!Sm.CompareStr(DeptCode, Sm.GetGrdStr(Grd1, Row, 15)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Invoice# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Invalid department."
                            );
                        return true;
                    }
                }
            }

            mDeptCode = DeptCode;
            return false;
        }

        private bool IsInvoiceItemAmtNotValid()
        {
            for (int row = 0; row < Grd5.Rows.Count - 1; row++)
            {
                if (Sm.GetGrdDec(Grd5, row, 10) > Sm.GetGrdDec(Grd5, row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invoice# : " + Sm.GetGrdStr(Grd5, row, 1) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd5, row, 4) + Environment.NewLine +
                        "Outstanding Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd5, row, 9), 0) + Environment.NewLine +
                        "Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd5, row, 10), 0) + Environment.NewLine + Environment.NewLine +
                        "Amount is bigger than outstanding amount."
                        );
                    return true;
                }
            }

            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                if (Sm.GetGrdDec(Grd1, row, 11) == 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invoice# : " + Sm.GetGrdStr(Grd1, row, 2) + Environment.NewLine +
                        "You need to input at least 1 items's amount."
                        );
                    return true;
                }
            }
                return false;
        }

        private bool IsGrd5Empty()
        {
            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                bool ItemsInd = false;
                for (int row2=0; row2 < Grd5.Rows.Count - 1; row2++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2) == Sm.GetGrdStr(Grd5, row2, 1))
                        ItemsInd = true;
                }
                if (!ItemsInd)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invoice# : " + Sm.GetGrdStr(Grd1, row, 1) + Environment.NewLine +
                        "You need to input at least 1 items."
                        );
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveOutgoingPaymentHdr(string DocNo, string VoucherRequestDocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblOutgoingPaymentHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("CancelInd, VoucherRequestDocNo, VoucherRequestSummaryInd, VoucherRequestSummaryDesc, ");
            SQL.AppendLine("VdCode, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo,");
            if (mIsOutgoingPaymentUseBudget)
                SQL.AppendLine("BCCode, ");
            SQL.AppendLine("CurCode, RateAmt, Amt, COAAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'N', @VoucherRequestDocNo, @VoucherRequestSummaryInd, @VoucherRequestSummaryDesc, ");
            SQL.AppendLine("@VdCode, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo,");
            if (mIsOutgoingPaymentUseBudget)
                SQL.AppendLine("@BCCode, ");
            SQL.AppendLine("@CurCode, @RateAmt, @Amt, @COAAmt, @EntCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                if (DeptCode.Length > 0)
                    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                else
                    Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            }
            else
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryInd", ChkMeeVoucherRequestSummaryInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryDesc", MeeVoucherRequestSummaryDesc.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            if (Sm.GetLue(LuePaidToBankCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            else
            {
                if (mIsOutgoingPaymentPaidToBankBasedOnVendor)
                    Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.Left(Sm.GetLue(LuePaidToBankCode), Sm.GetLue(LuePaidToBankCode).Length - 3));
                else
                    Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.GetLue(LuePaidToBankCode));
            }
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@COAAmt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (mIsOutgoingPaymentUseBudget) Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));

            var Remark = MeeRemark.Text;
            if (mIsCancelledDocumentCopyToRemark)
                Remark = GenerateRemarkForCancelledDocument(Remark);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateRemarkForCancelledDocument(string Remark)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, DocNo = string.Empty, RemarkTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    DocNo = Sm.GetGrdStr(Grd1, r, 2); 
                    if (DocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.InvoiceDocNo=@DocNo0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                    }
                }
            }
            if (Filter.Length>0) 
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Group_Concat(Distinct ");
            SQL.AppendLine("Concat('- Invoice# : ', B.InvoiceDocNo, ' (OP#:', B.DocNo, ') ') ");
            SQL.AppendLine("Order By B.InvoiceDocNo, B.DocNo Separator ',$$$') As Remarks ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.CancelInd='Y' Or A.Status='C';");

            cm.CommandText = SQL.ToString();

            RemarkTemp = Sm.GetValue(cm);

            if (Remark.Length > 0)
            {
                if (RemarkTemp.Length > 0)
                    Remark = string.Concat(Remark, Environment.NewLine, "Cancelled : ", Environment.NewLine, RemarkTemp.Replace("$$$", Environment.NewLine));
            }
            else
                Remark = string.Concat("Cancelled : ", Environment.NewLine, RemarkTemp.Replace("$$$", Environment.NewLine));

            return Remark;
        }

        private MySqlCommand SaveOutgoingPaymentDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblOutgoingPaymentDtl(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @InvoiceDocno, @InvoiceType, @Amt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@InvoiceDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@InvoiceType", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveOutgoingPaymentDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOutgoingPaymentDtl3(DocNo, DNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, DueDt, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BusinessPartnerCode, '1', @BankCode, @GiroNo, @DueDt, @CurCode, @Amt, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblGiroSummary Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where BusinessPartnerCode=@BusinessPartnerCode ");
            SQL.AppendLine("And BusinessPartnerType='1' ");
            SQL.AppendLine("And BankCode=@BankCode ");
            SQL.AppendLine("And GiroNo=@GiroNo; ");

            SQL.AppendLine("Insert Into TblGiroMovement(DocType, DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values('03', @DocNo, @BusinessPartnerCode, '1', @BankCode, @GiroNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@GiroNo", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetGrdDate(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateVoucherRequestDocNo(string Val)
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");

            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                DocSeqNo = "4";
                if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            string type;

            if (Sm.GetLue(LueAcType) == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");

                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string OutgoingPaymentDocNo,
            string LocalDocNoVcr,
            string SeqNo,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();
            var DocTitle= string.Empty;
            DocTitle = Sm.GetParameter("DocTitle");

            if (DocTitle == "IMS")
            {
                SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, ItSCCode, Mth, Yr, Revision, ");
                SQL.AppendLine("CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, EntCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, @DocDt, @LocalDocNo, @SeqNo, @ItSCCode, @Mth, @Yr, @Revision, ");
                SQL.AppendLine("'N', 'O', @DeptCode2, '03', Null, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, (Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @DocEnclosure, @CurCode, @Amt, @PaymentUser, @EntCode, @Remark, @CreateBy, CurrentDateTime()); ");

            }
            else
            {
                SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, ItSCCode, Mth, Yr, Revision, ");
                SQL.AppendLine("CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, EntCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo, @DocDt, @LocalDocNo, @SeqNo, @ItSCCode, @Mth, @Yr, @Revision, ");
                SQL.AppendLine("'N', 'O', @DeptCode2, '03', Null, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, (Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @DocEnclosure, @CurCode, @Amt*@RateAmt, @PaymentUser, @EntCode, @Remark, @CreateBy, CurrentDateTime()); ");

            }
            if (!mIsOutgoingPaymentAmtUseCOAAmt)
            {
                if (ChkMeeVoucherRequestSummaryInd.Checked)
                {
                    SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values (@DocNo, '001', @VoucherRequestSummaryDesc, @Amt*@RateAmt, Null, @CreateBy, CurrentDateTime()); ");
                }
            }

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @OutgoingPaymentDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='OutgoingPayment' ");

            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)) ");

            if (mIsOutgoingPaymentApprovalNeedDept)
                SQL.AppendLine(" And T.DeptCode=@DeptCode ");
            
            if (mIsOutgoingPaymentApprovalNeedPaymentType)
                SQL.AppendLine(" And T.PaymentType=@PaymentType ");

            SQL.AppendLine(";");

            SQL.AppendLine("Update TblOutgoingPaymentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@OutgoingPaymentDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='OutgoingPayment' ");
            SQL.AppendLine("    And DocNo=@OutgoingPaymentDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='OutgoingPayment' ");
            SQL.AppendLine("    And DocNo=@OutgoingPaymentDocNo ");
            SQL.AppendLine("    ); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNoVcr);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@OutgoingPaymentDocNo", OutgoingPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            if (mIsOutgoingPaymentAmtUseCOAAmt)
                Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt2.Text));
            else
                Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestSummaryDesc", MeeVoucherRequestSummaryDesc.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        
            if (mIsOutgoingPaymentApprovalNeedDept)
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);

            if (mIsOutgoingPaymentApprovalNeedDept)
                Sm.CmParam<String>(ref cm, "@DeptCode2", mDeptCode);
            else
                Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetValue("Select ParValue From TblParameter Where ParCode='OutgoingPaymentDeptCode' "));

            //if (mIsAutoGeneratePurchaseLocalDocNo)
            //{
            //    Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetGrdStr(Grd1, 0, 15));
            //}
            //else
            //{
            //    Sm.CmParam<String>(ref cm, "@DeptCode2", Sm.GetValue("Select ParValue From TblParameter Where ParCode='OutgoingPaymentDeptCode' "));
            //}
            
            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            
            SQL.Append("Insert Into TblVoucherRequestDtl ");
            SQL.Append("(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.Append("Values (@DocNo, @DNo, @Description, ");
            SQL.Append("Case When @InvoiceType='2' Then ");
            SQL.Append("    Cast(-1.00 as Decimal(18, 8)) ");
            SQL.Append("    Else ");
            SQL.Append("    Cast(1.00 As Decimal(18, 8)) End*@Amt*@RateAmt, ");
            SQL.Append("Null, @CreateBy, CurrentDateTime());");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@InvoiceType", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtlForCOAAmt(string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();
            var DocTitle = string.Empty;
            DocTitle = Sm.GetParameter("DocTitle");

            if (DocTitle == "IMS")
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@VoucherRequestDocNo, '001', 'Paid Amount (Purchase Invoice)', @Amt*@RateAmt, @Remark, @CreateBy, CurrentDateTime()); ");

                if (Decimal.Parse(TxtCOAAmt.Text) != 0)
                {
                    SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@VoucherRequestDocNo, '002', 'Vendor Account Payable', @AmtVendor, @Remark, @CreateBy, CurrentDateTime()); ");
                }

            }
            else
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@VoucherRequestDocNo, '001', 'Paid Amount (Purchase Invoice)', @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

                if (Decimal.Parse(TxtCOAAmt.Text) != 0)
                {
                    SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@VoucherRequestDocNo, '002', 'Vendor Account Payable', @AmtVendor, @Remark, @CreateBy, CurrentDateTime()); ");
                }

            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtVendor", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestFromCOAOutgoingPayment(string VoucherRequestDocNo, string OutgoingPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, EntCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', ");
            if (mIsOutgoingPaymentApprovalNeedDept)
                SQL.AppendLine("@DeptCode, ");
            else
                SQL.AppendLine("(Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='OutgoingPaymentDeptCode'), ");
            SQL.AppendLine("'17', Null, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @CurCode, @Amt, @PaymentUser, @EntCode, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Description, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            if (mIsOutgoingPaymentApprovalNeedDept)
                SQL.AppendLine("@DeptCode ");
            else
                SQL.AppendLine("Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='OutgoingPaymentDeptCode' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblOutgoingPaymentHdr Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@OutgoingPaymentDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@OutgoingPaymentDocNo", OutgoingPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 14));
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOutgoingPaymentDtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblOutgoingPaymentDtl2(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd4, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOutgoingPaymentDtl4(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblOutgoingPaymentDtl5(DocNo, DNo, InvoiceDocNo, InvoiceDNo, ItCode, Amt, DownpaymentAmt, DiscCostAmt, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @InvoiceDocNo, @InvoiceDNo, @ItCode, @Amt, @DownpaymentAmt, @DiscCostAmt, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@InvoiceDocNo", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@InvoiceDNo", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@DownpaymentAmt", Sm.GetGrdDec(Grd5, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@DiscCostAmt", Sm.GetGrdDec(Grd5, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePurchaseInvoiceHdr(string DocNo, byte Type)
        {
            // Type = 1 => Insert
            // Type = 2 => Cancel
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseInvoiceHdr X1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, ");
            if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                SQL.AppendLine("    (A.Amt+A.TaxAmt) As Amt1, ");
            else
                SQL.AppendLine("    (A.Amt+A.TaxAmt-A.DownPayment) As Amt1, ");
            SQL.AppendLine("    IfNull(B.Amt, 0.00) As Amt2 ");
		    SQL.AppendLine("    From TblPurchaseInvoiceHdr A ");
		    SQL.AppendLine("    Left Join ( ");
			SQL.AppendLine("        Select T.PurchaseInvoiceDocNo, Sum(T.Amt) Amt ");
			SQL.AppendLine("        From ( ");
			SQL.AppendLine("            Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt "); 
			SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' "); 
			SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T3 ");
			SQL.AppendLine("                On T2.InvoiceDocNo=T3.DocNo ");
			SQL.AppendLine("                And T3.DocNo In (Select InvoiceDocNo From TblOutgoingPaymentDtl Where DocNo=@DocNo And InvoiceType='1') ");
			SQL.AppendLine("            Where T1.CancelInd='N' ");
			SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
			SQL.AppendLine("            Union All ");
			SQL.AppendLine("            Select T1.PurchaseInvoiceDocNo, T1.Amt ");
			SQL.AppendLine("            From TblApsHdr T1 ");
			SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T2 ");
			SQL.AppendLine("                On T1.PurchaseInvoiceDocNo=T2.DocNo ");
			SQL.AppendLine("                And T2.DocNo In (Select InvoiceDocNo From TblOutgoingPaymentDtl Where DocNo=@DocNo And InvoiceType='1') ");
			SQL.AppendLine("            Where T1.CancelInd='N' ");
			SQL.AppendLine("        ) T Group By T.PurchaseInvoiceDocNo ");
		    SQL.AppendLine("    ) B On A.DocNo=B.PurchaseInvoiceDocNo ");
		    SQL.AppendLine("    Where A.DocNo In ( ");
            SQL.AppendLine("        Select InvoiceDocNo From TblOutgoingPaymentDtl ");
            SQL.AppendLine("        Where DocNo=@DocNo And InvoiceType='1' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("Set X1.ProcessInd = ");
	        SQL.AppendLine("    Case When IfNull(X2.Amt2, 0.00) = 0.00 Then ");
		    SQL.AppendLine("        Case When X2.Amt1 = 0.00 Then ");
            if (Type == 1)
                SQL.AppendLine("            'F' ");
            else
                SQL.AppendLine("            'O' ");
            SQL.AppendLine("        Else 'O' End ");
	        SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(X2.Amt2, 0.00)>=X2.Amt1 Then 'F' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where X1.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblOutgoingPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='1' ");
            SQL.AppendLine("    ); ");

            if (Type != 1)
            {
                SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
                SQL.AppendLine("    ProcessInd='O' ");
                SQL.AppendLine("Where DocNo Not In ( ");
                SQL.AppendLine("    Select B.InvoiceDocNo ");
                SQL.AppendLine("    From TblOutgoingPaymentHdr A, TblOutgoingPaymentDtl B ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    And B.InvoiceType='1' ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status In ('O', 'A') ");
                SQL.AppendLine("    And A.DocNo=B.DocNo ");
                SQL.AppendLine(") ");
                if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                    SQL.AppendLine("And (Amt+TaxAmt)=0.00;");
                else
                    SQL.AppendLine("And (Amt+TaxAmt-DownPayment)=0.00;");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdatePurchaseReturnInvoiceHdr(string DocNo, byte Type)
        {
            // Type = 1 => Insert
            // Type = 2 => Cancel

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr X1 ");
            SQL.AppendLine("Inner Join ( ");
	        SQL.AppendLine("    Select A.DocNo, A.Amt As Amt1, IfNull(B.Amt, 0) As Amt2 ");
	        SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr A ");
	        SQL.AppendLine("    Left Join ( ");
			SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt ");
			SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
		    SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceHdr T3 ");
			SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
			SQL.AppendLine("            And T3.DocNo In (Select InvoiceDocNo From TblOutgoingPaymentDtl Where DocNo=@DocNo And InvoiceType='2') ");
			SQL.AppendLine("        Where T1.CancelInd='N' ");
		    SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
		    SQL.AppendLine("        Group By T2.InvoiceDocNo ");
	        SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("    Where A.DocNo In (Select InvoiceDocNo From TblOutgoingPaymentDtl Where DocNo=@DocNo And InvoiceType='2') ");
            SQL.AppendLine(") X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("Set X1.ProcessInd= ");
	        SQL.AppendLine("    Case When X2.Amt2 = 0 Then ");
		    SQL.AppendLine("        Case When X2.Amt1 = 0 Then ");
            if (Type == 1)
                SQL.AppendLine("            'F' ");
            else
                SQL.AppendLine("            'O' ");
            SQL.AppendLine("            Else 'O' End ");
	        SQL.AppendLine("    Else ");
		    SQL.AppendLine("        Case When X2.Amt2>=X2.Amt1 Then 'F' Else 'P' End ");
	        SQL.AppendLine("    End ");
            SQL.AppendLine("Where X1.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblOutgoingPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='2'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOutgoingPaymentHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo And Status='A';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Outgoing Payment (Giro) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblOutgoingPaymentHdr Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T1.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T2.AcNo, T2.DAmt, T2.CAmt, T3.EntCode, T3.CreateBy, T3.CreateDt ");
            SQL.AppendLine("From TblJournalHdr T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.AcNo, Sum(T.Amt) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Concat(C.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * B.Amt As Amt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='AcNoForGiroAP' And C.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.AcNo ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select T.AcNo, 0.00 As DAmt, Sum(T.Amt) As CAmt ");
            SQL.AppendLine("    From ( ");

            #region Old Code

            //SQL.AppendLine("        Select (");
            //SQL.AppendLine("            Select X2.COAAcNo As AcNo ");
            //SQL.AppendLine("            From TblVoucherHdr X1, TblBankAccount X2 ");
            //SQL.AppendLine("            Where X1.BankAcCode=X2.BankAcCode ");
            //SQL.AppendLine("            And X2.COAAcNo Is Not Null ");
            //SQL.AppendLine("            And X1.DocNo In ( ");
            //SQL.AppendLine("                Select Tbl1.DocNo ");
            //SQL.AppendLine("                From TblGiroMovement Tbl1, TblVoucherHdr Tbl2 ");
            //SQL.AppendLine("                Where Tbl1.DocType='01' ");
            //SQL.AppendLine("                And Tbl1.BusinessPartnerCode=B.BusinessPartnerCode ");
            //SQL.AppendLine("                And Tbl1.BankCode=B.BankCode ");
            //SQL.AppendLine("                And Tbl1.GiroNo=B.GiroNo ");
            //SQL.AppendLine("                And Tbl1.DocNo=Tbl2.DocNo ");
            //SQL.AppendLine("                And Tbl2.DocDt<=@DocDt ");
            //SQL.AppendLine("            ) Order By X1.DocDt, X1.CreateDt Desc Limit 1  ");
            //SQL.AppendLine("        ) As AcNo, ");
            //SQL.AppendLine("        Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) ");
            //SQL.AppendLine("        End * B.Amt As Amt ");
            //SQL.AppendLine("        From TblOutgoingPaymentHdr A ");
            //SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl3 B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode And C.COAAcNo Is Not Null ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            #endregion

            SQL.AppendLine("        Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("        Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * B.Amt As Amt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode And C.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Where T.AcNo Is Not Null ");
            SQL.AppendLine("    Group By T.AcNo ");
            SQL.AppendLine(") T2 On 0=0 ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr T3 On T3.DocNo=@DocNo ");
            SQL.AppendLine("Where T1.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelOutgoingPaymentHdr());
            cml.Add(UpdatePurchaseInvoiceHdr(TxtDocNo.Text, 2));
            cml.Add(UpdatePurchaseReturnInvoiceHdr(TxtDocNo.Text, 2));

            if (mIsAutoJournalActived)
            {
                if (decimal.Parse(TxtGiroAmt.Text) != 0) cml.Add(SaveJournal());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                IsGiroNoInValid("Y");
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =  
                    "Select DocNo From TblOutgoingPaymentHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelOutgoingPaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOutgoingPaymentHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
            SQL.AppendLine("Where (DocNo=@VoucherRequestDocNo OR DocNo=@VoucherRequestDocNo2) And CancelInd='N' And Status<>'C'; ");

            if (Grd3.Rows.Count > 0)
            {
                SQL.AppendLine("Insert Into TblGiroMovement ");
                SQL.AppendLine("(DocType, DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select '04', DocNo, BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblGiroMovement ");
                SQL.AppendLine("Where DocType='03' And DocNo=@DocNo;");

                SQL.AppendLine("Update TblGiroSummary T Set ");
                SQL.AppendLine("    T.ActInd='Y', T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where T.ActInd='N' ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select GiroNo ");
                SQL.AppendLine("    From TblGiroMovement ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And DocType='04' ");
                SQL.AppendLine("    And BusinessPartnerCode=T.BusinessPartnerCode ");
                SQL.AppendLine("    And BusinessPartnerType=T.BusinessPartnerType ");
                SQL.AppendLine("    And BankCode=T.BankCode ");
                SQL.AppendLine("    And GiroNo=T.GiroNo ");
                SQL.AppendLine(");");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo2", TxtVoucherRequestDocNo2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblOutgoingPaymentHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblOutgoingPaymentHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblOutgoingPaymentHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                if (mDocNoFormat=="1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            
            else
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOutgoingPaymentHdr(DocNo);
                ShowOutgoingPaymentDtl(DocNo);
                ShowOutgoingPaymentDtl2(DocNo);
                ShowOutgoingPaymentDtl3(DocNo);
                ShowOutgoingPaymentDtl4(DocNo);
                ShowDocApproval(DocNo);
                ComputeAmt();
                if (mIsOutgoingPaymentUseBudget) ComputeRemainingBudget();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOutgoingPaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, C.VoucherDocNo As VoucherDocNo2, ");
            SQL.AppendLine("(Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("Else '' End) As StatusDesc, ");
            SQL.AppendLine("B.VoucherDocNo, D.BankAcTp ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo2=C.DocNo ");
            SQL.AppendLine("Left Join TblBankAccount D On A.BankAcCode=D.BankAcCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt",  "CancelReason", "StatusDesc", "VdCode",   

                        //6-10
                        "VoucherRequestDocNo","VoucherRequestSummaryDesc", "VoucherRequestSummaryInd", "VoucherDocNo", "AcType",   

                        //11-15
                        "BankAcCode", "PaymentType", "BankCode", "GiroNo", "DueDt",    

                        //16-20
                        "CurCode", "RateAmt", "Amt", "Remark", "CancelInd", 

                        //21-25
                        "PaymentUser", "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcName", "PaidToBankAcNo",

                        //26-30
                        "VoucherDocNo2", "VoucherRequestDocNo2", "COAAmt", "EntCode", "JournalDocNo", 
                        
                        //31-33
                        "JournalDocNo2", "BankAcTp", "BCCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[5]));
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        ChkMeeVoucherRequestSummaryInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[8]), "Y") ? true : false;
                        MeeVoucherRequestSummaryDesc.EditValue = Sm.DrStr(dr, c[7]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[10]));
                        Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[13]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                        TxtRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[20]), "Y");
                        TxtPaymentUser.EditValue = Sm.DrStr(dr, c[21]);
                        if (mIsOutgoingPaymentPaidToBankBasedOnVendor)
                            SetLuePaidToBankCode(ref LuePaidToBankCode, "", "1");
                        Sm.SetLue(LuePaidToBankCode, Sm.DrStr(dr, c[22]));
                        TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[23]);
                        TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[24]);
                        TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[25]);
                        TxtVoucherDocNo2.EditValue = Sm.DrStr(dr, c[26]);
                        TxtVoucherRequestDocNo2.EditValue = Sm.DrStr(dr, c[27]);
                        TxtCOAAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[29]));
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[30]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[31]);
                        TxtBankAcTp.EditValue = Sm.DrStr(dr, c[32]);
                        if (mIsOutgoingPaymentUseBudget)
                        {
                            SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[33]), string.Empty);
                            Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[33]));
                        }
                    }, true
                );
        }

        private void ShowOutgoingPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.InvoiceDocNo, ");
            SQL.AppendLine("Case B.InvoiceType When '1' Then D.DocDt Else E.DocDt End As InvoiceDt, ");
            SQL.AppendLine("B.InvoiceType, ");
            SQL.AppendLine("Case B.InvoiceType When '1' Then 'Purchase Invoice' Else 'Purchase Return Invoice' End As InvoiceTypeDesc, ");
            SQL.AppendLine("A.VdCode, C.VdName, ");
            SQL.AppendLine("Case B.InvoiceType When '1' Then D.CurCode Else E.CurCode End As CurCode, ");
            SQL.AppendLine("B.Amt, ");
            SQL.AppendLine("Case B.InvoiceType When '1' Then D.DueDt Else Null End As DueDt, ");
            SQL.AppendLine("Case B.InvoiceType When '1' Then D.VdInvNo Else Null End As VdInvNo, ");
            SQL.AppendLine("B.Remark, IfNull(A.DeptCode, IfNull(D.DeptCode, E.DeptCode)) DeptCode, IfNull(F.DeptName, IfNull(G.DeptName, H.DeptName)) DeptName, D.LocalDocNo, D.Amt+IFNULL(D.TaxAmt, 0.00) InvoiceAmt ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr D On B.InvoiceDocNo=D.DocNo And B.InvoiceType='1' ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr E On B.InvoiceDocNo=E.DocNo And B.InvoiceType='2' ");
            SQL.AppendLine("Left Join TblDepartment F On A.DeptCode=F.DeptCode ");
            SQL.AppendLine("Left Join TblDepartment G On D.DeptCode = G.DeptCode ");
            SQL.AppendLine("Left Join TblDepartment H On E.DeptCode = H.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "InvoiceDocNo", "InvoiceDt", "InvoiceType", "InvoiceTypeDesc", "VdCode", 

                    //6-10
                    "VdName", "CurCode", "Amt", "DueDt", "VdInvNo", 

                    //11-15
                    "Remark", "DeptCode", "DeptName", "LocalDocno", "InvoiceAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Grd.Cells[Row, 10].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowOutgoingPaymentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.BankCode, B.BankName, A.GiroNo, A.DueDt, A.CurCode, A.Amt ");
            SQL.AppendLine("From TblOutgoingPaymentDtl3 A, TblBank B ");
            SQL.AppendLine("Where A.BankCode=B.BankCode And A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { "BankCode", "BankName", "GiroNo", "DueDt", "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 6 });
            ComputeGiroAmt();
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowOutgoingPaymentDtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
            SQL.AppendLine("From TblOutgoingPaymentDtl2 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowOutgoingPaymentDtl4(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT A.DocNo, A.InvoiceDocNo, A.InvoiceDNo, A.ItCode, B.ItName, A.Amt, A.DownpaymentAmt, A.DiscCostAmt ");
            SQL.AppendLine("FROM TblOutgoingpaymentDtl5 A ");
            SQL.AppendLine("INNER JOIN TblItem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "InvoiceDocNo", 
                    
                    //1-5
                    "InvoiceDNo", "ItCode", "ItName", "DownpaymentAmt", "DiscCostAmt", 

                    //6
                    "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5, 6, 7, 8,9, 10 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='OutgoingPayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (Sm.GetGrdStr(Grd1, 0, 15).Length != 0 && Sm.GetDte(DteDocDt).Length != 0)
            {
                var SQL = new StringBuilder();

                if (mIsMRBudgetBasedOnBudgetCategory)
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");
                    if (!mIsBudget2YearlyFormat && !Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                        SQL.AppendLine("        Amt2 ");
                    else
                        SQL.AppendLine("        SUM(Amt2) ");
                    SQL.AppendLine("        From TblBudgetSummary A ");
                    SQL.AppendLine("        Where A.DeptCode=@DeptCode ");
                    if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                        SQL.AppendLine("        AND A.BCCode = @BCCode ");
                    SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");
                    SQL.AppendLine("    ), 0.00)- ");
                }
                else
                {

                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Amt From TblBudget ");
                    SQL.AppendLine("        Where DeptCode=@DeptCode ");
                    SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                    SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                    //SQL.AppendLine("        And UserCode Is Not Null ");
                    SQL.AppendLine("    ), 0.00)- ");

                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(Amt) ");
                SQL.AppendLine("        From TblVoucherRequestHdr ");
                SQL.AppendLine("        Where DocNo <> @DocNo ");
                SQL.AppendLine("        And Find_In_Set(DocType, ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And Status In ('O', 'A') ");
                SQL.AppendLine("        And ReqType Is Not Null ");
                SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
                SQL.AppendLine("        And DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                    SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                    SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) + ");

                SQL.AppendLine("      IfNull(( ");
                SQL.AppendLine("      Select SUM(A.Amt) from( ");
                SQL.AppendLine("      Select Case when A.AcType = 'D' Then IFNULL(A.Amt, 0.00) ELSE IFNULL((A.Amt)*-1, 0.00) END As Amt ");
                SQL.AppendLine("        From tblvoucherhdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("	    (");
                SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("			AND X1.DocType = '58' ");
                SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("       Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("       Inner Join ");
                SQL.AppendLine("		  ( ");
                SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("			AND X1.DocType = '56' ");
                SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("       Where A.DocNo <> @DocNo  ");
                SQL.AppendLine("       And D.DeptCode = @DeptCode  ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                if (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL"))
                    SQL.AppendLine("       And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                SQL.AppendLine(" )A  ");
                SQL.AppendLine("  ), 0.00) - ");

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory && (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL")))
                    {
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }
                    SQL.AppendLine("        And A.ReqType='1' ");
                    SQL.AppendLine("        And B.CancelInd='N' ");
                    SQL.AppendLine("        And B.Status<>'C' ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    SQL.AppendLine("    ),0.00)+");
                }

                if (mMRAvailableBudgetSubtraction == "2")
                {
                    SQL.AppendLine("    IfNull(( ");

                    SQL.AppendLine("    Select Sum( ");
                    SQL.AppendLine("    X2.Amt -  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("          IfNull(( ");
                    SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End ");
                    SQL.AppendLine("      , 0.00) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory && (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL")))
                        SQL.AppendLine("        And A.BCCode=@BCCode ");

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");

                    SQL.AppendLine("    ),0.00)+");
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                if (mIsMRBudgetBasedOnBudgetCategory && (!Sm.CompareStr(Sm.GetLue(LueBCCode), "ALL")))
                    SQL.AppendLine("        And E.BCCode=@BCCode ");

                SQL.AppendLine("            And E.ReqType='1' ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                SQL.AppendLine("            And E.DocNo<>@DocNo ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
                SQL.AppendLine("    ),0.00) ");

                if (mIsCASUsedForBudget)
                {
                    SQL.AppendLine(" - IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    SQL.AppendLine("            And C.BCCode = @BCCode ");
                    SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("), 0.00) ");
                }

                SQL.AppendLine(" As RemainingBudget");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mBudgetBasedOn == "1")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, 0, 15));
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, 0, 15));

                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length != 0) ? TxtDocNo.Text : "XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));

                AvailableBudget = Sm.GetValue(cm);
            }

            return decimal.Parse(AvailableBudget);
        }

        private void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                AvailableBudget = Sm.ComputeAvailableBudget(
                    (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"),
                    Sm.GetDte(DteDocDt),
                    Sm.GetGrdStr(Grd1, 0, 15),
                    Sm.GetGrdStr(Grd1, 0, 15),
                    Sm.GetLue(LueBCCode)
                    );
                RequestedBudget = Decimal.Parse(TxtAmt.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget - RequestedBudget, 0);
        }

        private void SetLueBCCode(ref LookUpEdit Lue, string BCCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode As Col1, BCName As Col2 From TblBudgetCategory ");
            if (BCCode.Length > 0)
                SQL.AppendLine("Where BCCode=@BCCode ");
            else
                SQL.AppendLine("Where DeptCode=@DeptCode And ActInd='Y' ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            if (DeptCode.Length > 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        private void GetDuplicateCOAIndicator()
        {
            if (Grd4.Rows.Count > 1)
            {
                for (int i = 0; i < Grd4.Rows.Count - 1; ++i)
                {
                    string AcNo1 = Sm.GetGrdStr(Grd4, i, 1);
                    for (int j = (i + 1); j < Grd4.Rows.Count - 1; ++j)
                    {
                        string AcNo2 = Sm.GetGrdStr(Grd4, j, 1);

                        if (AcNo1 == AcNo2)
                        {
                            Grd4.Cells[i, 6].Value = true;
                            Grd4.Cells[j, 6].Value = true;
                        }
                    }
                }
            }
        }

        private bool IsPurchaseInvoiceUseApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocapprovalSetting Where DocType = 'PurchaseInvoice' Limit 1; ");
        }

        private void GetParameter()
        {
            mIsOutgoingPaymentApprovalNeedDept = Sm.IsApprovalNeedXCode("OutgoingPayment", "Dept");
            mIsOutgoingPaymentFilterByPIDept = Sm.GetParameterBoo("IsOutgoingPaymentFilterByPIDept");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mIsRemarkForApprovalMandatory = Sm.GetParameterBoo("IsOutgoingPaymentRemarkForApprovalMandatory");
            mIsOutgoingPaymentApprovalNeedPaymentType = Sm.GetParameterBoo("IsOutgoingPaymentApprovalNeedPaymentType");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsComparedToDetailDate = Sm.GetParameterBoo("IsComparedToDetailDate");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsPrintOutOP = Sm.GetParameter("FormPrintOutOP");
            if (mIsPrintOutOP.Length == 0) mIsPrintOutOP = "OutgoingPayment";
            mIsOutgoingPaymentUseCOA = Sm.GetParameterBoo("IsOutgoingPaymentUseCOA");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mAcNoForCurrentAccountsPayable = Sm.GetParameter("AcNoForCurrentAccountsPayable");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsOutgoingPaymentPaidToBankBasedOnVendor = Sm.GetParameterBoo("IsOutgoingPaymentPaidToBankBasedOnVendor");
            mIsCancelledDocumentCopyToRemark = Sm.GetParameterBoo("IsCancelledDocumentCopyToRemark");
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mUserType = Sm.GetParameterBoo("UserType");
            mIsBankAccountCurrencyValidationInactive = Sm.GetParameterBoo("IsBankAccountCurrencyValidationInactive");
            mIsVoucherBankAccountFilteredByGrp = Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp");
            mIsPITotalWithoutTaxInclDownpaymentEnabled = Sm.GetParameterBoo("IsPITotalWithoutTaxInclDownpaymentEnabled");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsItemCategoryUseCOAAPAR = Sm.GetParameterBoo("IsItemCategoryUseCOAAPAR");
            mIsOutgoingPaymentUsePIWithCOA = Sm.GetParameterBoo("IsOutgoingPaymentUsePIWithCOA");
            mIsOutgoingPaymentAmtUseCOAAmt = Sm.GetParameterBoo("IsOutgoingPaymentAmtUseCOAAmt");
            mIsCOACouldBeChosenMoreThanOnce = Sm.GetParameterBoo("IsCOACouldBeChosenMoreThanOnce");
            mVendorAcNoAP = Sm.GetParameter("VendorAcNoAP");
            mIsPurchaseInvoiceUseApproval = IsPurchaseInvoiceUseApproval();
            mOutgoingPaymentCOAAmtCalculationMethod = Sm.GetParameter("OutgoingPaymentCOAAmtCalculationMethod");
            mPurchaseInvoiceCOAAmtCalculationMethod = Sm.GetParameter("PurchaseInvoiceCOAAmtCalculationMethod");

            //budget
            mIsMRBudgetBasedOnBudgetCategory = Sm.GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsOutgoingPaymentUseBudget = Sm.GetParameterBoo("IsOutgoingPaymentUseBudget");
            mIsVRForBudgetUseAvailableBudget = Sm.GetParameterBoo("IsVRForBudgetUseAvailableBudget");
            mIsDOCtAllowMultipleItemCategory = Sm.GetParameterBoo("IsDOCtAllowMultipleItemCategory");

            if (mMRAvailableBudgetSubtraction.Length == 0) mMRAvailableBudgetSubtraction = "1";
            if (mBudgetBasedOn.Length == 0) mBudgetBasedOn = "1";
            if (mOutgoingPaymentCOAAmtCalculationMethod.Length == 0) mOutgoingPaymentCOAAmtCalculationMethod = "1";
            if (mPurchaseInvoiceCOAAmtCalculationMethod.Length == 0) mPurchaseInvoiceCOAAmtCalculationMethod = "1";
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (VdCode.Length == 0)
            {
                SQL.AppendLine("Select Distinct T1.VdCode As Col1, T2.VdName As Col2 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct T.VdCode ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr T ");
                SQL.AppendLine("    Where T.CancelInd='N' ");
                if (mIsPurchaseInvoiceUseApproval) SQL.AppendLine("    And T.Status = 'A' ");
                if (mIsOutgoingPaymentFilterByPIDept)
                {
                    SQL.AppendLine("    And (T.DeptCode Is Null ");
                    SQL.AppendLine("    Or (T.DeptCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("        Where DeptCode=IfNull(T.DeptCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("        And IfNull(T.ProcessInd, 'O')<>'F' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Distinct VdCode ");
                SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr ");
                SQL.AppendLine("        Where CancelInd='N' ");
                SQL.AppendLine("        And IfNull(ProcessInd, 'O')<>'F' ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Inner Join TblVendor T2 On T1.VdCode=T2.VdCode ");
                SQL.AppendLine("Order By T2.VdName; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            }
            else
            {
                SQL.AppendLine("Select VdCode As Col1, VdName As Col2 ");
                SQL.AppendLine("From TblVendor ");
                SQL.AppendLine("Where VdCode=@VdCode;");

                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            }

            cm.CommandText = SQL.ToString();
            try
            {
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetCurCode(string BankAcCode)
        {
            var CurCode = Sm.GetValue("Select CurCode From TblBankAccount Where bankAcCode=@Param And CurCode Is Not Null;", BankAcCode);
            if (CurCode.Length>0) Sm.SetLue(LueCurCode, CurCode);
        }

        private void SetBankCode()
        {
            if (!LueBankCode.Properties.ReadOnly &&
                Sm.GetLue(LueBankAcCode).Length>0)
            {
                try
                {
                    var BankCode = Sm.GetValue(
                        "Select BankCode From TblBankAccount Where BankAcCode=@Param;",
                        Sm.GetLue(LueBankAcCode));
                    if (BankCode.Length > 0) Sm.SetLue(LueBankCode, BankCode);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void SetBankAcTp()
        {
            TxtBankAcTp.EditValue = null;
            if (Sm.GetLue(LueBankAcCode).Length > 0)
            {
                try
                {
                    var BankAcTp = Sm.GetValue(
                        "Select BankAcTp From TblBankAccount Where BankAcCode=@Param;",
                        Sm.GetLue(LueBankAcCode));
                    if (BankAcTp.Length > 0) TxtBankAcTp.EditValue = BankAcTp;
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private bool IsInvoiceEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m, Amt2=0m;
            Amt += decimal.Parse(TxtGiroAmt.Text);

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 11).Length > 0)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), "1"))
                            Amt += Sm.GetGrdDec(Grd1, Row, 11);
                        else
                            Amt -= Sm.GetGrdDec(Grd1, Row, 11);
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 18).Length > 0)
                    {
                        Amt2 += Sm.GetGrdDec(Grd1, Row, 18);
                    }
                }
            }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
            TxtAmt3.EditValue = Sm.FormatNum(Amt2, 0);
            ComputeAmt2();
        }

        internal void ComputeAmt2()
        {
            decimal Amt = 0m, RateAmt=0m;
            if (TxtAmt.Text.Length != 0)
            {
                Amt = decimal.Parse(TxtAmt.Text);
                if (mIsOutgoingPaymentAmtUseCOAAmt)
                {
                    if (TxtRateAmt.Text.Length != 0)
                    {
                        RateAmt = decimal.Parse(TxtRateAmt.Text);
                        Amt = (Amt*RateAmt) + decimal.Parse(TxtCOAAmt.Text);
                    }
                    else
                        Amt = Amt + decimal.Parse(TxtCOAAmt.Text);
                }
            }

            TxtAmt2.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeInvoiceAmt()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                decimal Amt = 0m;
                for (int row = 0; row < Grd5.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2) == Sm.GetGrdStr(Grd5, row, 1))
                    {
                        Amt += Sm.GetGrdDec(Grd5, row, 10);
                    }
                }

                Grd1.Cells[Row, 11].Value = Sm.FormatNum(Amt, 0);
            }
        }

        internal void ComputeGiroAmt()
        {
            decimal Amt = 0m;
            for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                Amt += Sm.GetGrdDec(Grd3, r, 6);
            TxtGiroAmt.EditValue = Sm.FormatNum(Amt, 0);
            ComputeAmt();
        }

        internal void ComputeCOAAmt()
        {
            decimal COAAmt = 0m;
            var AcNo = string.Empty;
            var AcType = string.Empty;

            try
            {
                var SQL = new StringBuilder();

                if (mOutgoingPaymentCOAAmtCalculationMethod == "1")
                {
                    SQL.AppendLine("SELECT CONCAT( ");
                    SQL.AppendLine("   C.ParValue, A.VdCode ");
                    SQL.AppendLine(") AS AcNo ");
                    SQL.AppendLine("FROM TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("INNER JOIN TblVendor B ON A.VdCode = B.VdCode ");
                    SQL.AppendLine("LEFT JOIN TblParameter C ON C.Parcode = 'VendorAcNoAP' ");
                    SQL.AppendLine("WHERE A.VdCode = @VdCode LIMIT 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                    AcNo = Sm.GetValue(cm);

                    cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    AcType = Sm.GetValue(cm);
                }

                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (mOutgoingPaymentCOAAmtCalculationMethod == "2")
                    {
                        AcNo = Sm.GetValue("SELECT DISTINCT F.AcNo FROM tblpurchaseinvoicehdr A " +
                            "INNER JOIN tblpurchaseinvoicedtl B ON A.DocNo = B.DocNo " +
                            "INNER JOIN tblrecvvddtl C ON B.RecvVdDocNo = C.DocNo AND B.RecvVdDNo = C.DNo " +
                            "INNER JOIN tblitem D ON C.ItCode = D.ItCode " +
                            "INNER JOIN tblitemcategory E ON D.ItCtCode = E.ItCtCode " +
                            "INNER JOIN tblcoa F ON E.AcNo9 = F.AcNo " +
                            "WHERE FIND_IN_SET(A.DocNo, @Param);", GetPIDocNo());

                        AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", Sm.GetGrdStr(Grd4, Row, 1));
                    }

                    if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd4, Row, 1)))
                    {
                        if (Sm.GetGrdDec(Grd4, Row, 3) != 0)
                        {
                            if (AcType == "D")
                                COAAmt += Sm.GetGrdDec(Grd4, Row, 3);
                            else
                                COAAmt -= Sm.GetGrdDec(Grd4, Row, 3);
                        }
                        if (Sm.GetGrdDec(Grd4, Row, 4) != 0)
                        {
                            if (AcType == "C")
                                COAAmt += Sm.GetGrdDec(Grd4, Row, 4);
                            else
                                COAAmt -= Sm.GetGrdDec(Grd4, Row, 4);
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
                if (mIsOutgoingPaymentAmtUseCOAAmt)
                    TxtAmt2.EditValue = Sm.FormatNum((((Decimal.Parse(TxtAmt.Text)*Decimal.Parse(TxtRateAmt.Text)) + COAAmt)), 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetPIDocNo()
        {
            string mPIDocNo = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (mPIDocNo.Length > 0) mPIDocNo += ",";
                mPIDocNo += Sm.GetGrdStr(Grd1, Row, 2);
            }

            return mPIDocNo;
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
                    {
                        if (mIsCOACouldBeChosenMoreThanOnce && Sm.GetLue(LueVdCode).Length > 0)
                        {
                            if (Sm.GetGrdStr(Grd4, Row, 1) == string.Concat(mVendorAcNoAP, Sm.GetLue(LueVdCode)))
                            {
                                SQL += "##" + Sm.GetGrdStr(Grd4, Row, 1) + "##";
                            }
                        }
                        else
                            SQL += "##" + Sm.GetGrdStr(Grd4, Row, 1) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLuePaidToBankCode(ref DXE.LookUpEdit Lue, string VdCode, string Type)
        {
            try
            {
                //Type = 1 -> For View
                //Type = 2 -> For Insert

                var SQL = new StringBuilder();

                if (Type == "1")
                {
                    SQL.AppendLine("Select BankCode As Col1, BankName As Col2, ");
                    SQL.AppendLine("'-' As Col3, 'xxx' As Col4  ");
                    SQL.AppendLine("From TblBank Order By BankName;");
                }
                else
                {
                    SQL.AppendLine("Select Concat(A.BankCode, A.DNo) As Col1, B.BankName As Col2, ");
                    SQL.AppendLine("Trim(Concat(");
                    SQL.AppendLine("    Case When IfNull(A.BankBranch,'')='' Then '' Else Concat('Branch : ', A.BankBranch) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcName,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Then '' Else ', ' End, 'Name : ', A.BankAcName) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcNo,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Or IfNull(A.BankAcName,'')='' Then '' Else ', ' End, 'Account Number : ', A.BankAcNo) End ");
                    SQL.AppendLine(")) As Col3, ");
                    SQL.AppendLine("A.DNo As Col4  ");
                    SQL.AppendLine("From TblVendorBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                    SQL.AppendLine("Where VdCode='" + VdCode + "' ");
                    SQL.AppendLine("Order By B.BankName, A.DNo; ");
                }
                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 170, 300, 0, false, true, true, false, 
                    "Code", "Bank", "Bank Branch/Account Name/Account No.", "DNo", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPaidToBankCodeInfo(string VdCode, string DNo)
        {
            var cm = new MySqlCommand();
            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BankBranch, BankAcName, BankAcNo " +
                        "From TblVendorBankAccount " +
                        "Where VdCode=@VdCode And DNo=@DNo; ",
                        new string[] { "BankBranch", "BankAcName", "BankAcNo" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[2]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ReComputeOutstandingAmt()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocNo, '1' As InvoiceType, ");
            //SQL.AppendLine("(A.Amt+A.TaxAmt-A.DownPayment)-IfNull(( ");
            //SQL.AppendLine("    Select Sum(T2.Amt) ");
            //SQL.AppendLine("    From TblOutgoingPaymentHdr T1, TblOutgoingPaymentDtl T2 ");
            //SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    And T1.CancelInd='N' ");
            //SQL.AppendLine("    And T2.InvoiceType='1' ");
            //SQL.AppendLine("    And T2.InvoiceDocNo=A.DocNo ");
            //SQL.AppendLine("), 0) As Amt ");
            //SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            //SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            //SQL.AppendLine("Where A.CancelInd='N' ");
            //SQL.AppendLine("And Locate(Concat('##', A.DocNo, '1', '##'), @SelectedInvoice)>1 ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select A.DocNo, '2' As InvoiceType, ");
            //SQL.AppendLine("A.Amt-IfNull(( ");
            //SQL.AppendLine("    Select Sum(T2.Amt) ");
            //SQL.AppendLine("    From TblOutgoingPaymentHdr T1, TblOutgoingPaymentDtl T2 ");
            //SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    And T1.CancelInd='N' ");
            //SQL.AppendLine("    And T2.InvoiceType='2' ");
            //SQL.AppendLine("    And T2.InvoiceDocNo=A.DocNo ");
            //SQL.AppendLine("), 0) As Amt ");
            //SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
            //SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            //SQL.AppendLine("Where A.CancelInd='N' ");
            //SQL.AppendLine("And Locate(Concat('##', A.DocNo, '2', '##'), @SelectedInvoice)>0 ");

            SQL.AppendLine("Select DocNo, InvoiceType, Amt From (");
            SQL.AppendLine("Select ");
            SQL.AppendLine("A.DocNo, '1' As InvoiceType, ");
            if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
            {
                if (mIsOutgoingPaymentUsePIWithCOA)
                    SQL.AppendLine("(A.Amt+A.TaxAmt+ifnull(C.AcAmt, 0)-IfNull(B.Amt, 0)) As Amt ");
                else
                    SQL.AppendLine("(A.Amt+A.TaxAmt-IfNull(B.Amt, 0)) As Amt ");
            }
            else
                SQL.AppendLine("(A.Amt+A.TaxAmt-A.DownPayment-IfNull(B.Amt, 0)) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.PurchaseInvoiceDocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T1.PurchaseInvoiceDocNo, T1.Amt ");
            SQL.AppendLine("        From TblApsHdr T1 ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T2 On T1.PurchaseInvoiceDocNo=T2.DocNo And IfNull(T2.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.PurchaseInvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.PurchaseInvoiceDocNo ");
            if (mIsOutgoingPaymentUsePIWithCOA)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select X.DocNo, X.AcNo, ");
                SQL.AppendLine("    Case ");
                SQL.AppendLine("        When X.AcType = 'D' && X.DAmt>0 Then X.DAmt ");
                SQL.AppendLine("        When X.AcType = 'D' && X.CAmt>0 Then X.CAmt *-1 ");
                SQL.AppendLine("        When X.AcType = 'C' && X.CAmt>0 Then X.Camt ");
                SQL.AppendLine("        When X.AcType = 'C' && X.DAmt>0 Then X.DAmt *-1 ");
                SQL.AppendLine("    End As AcAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo, B.AcNo, C.Actype, B.DAmt, B.Camt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblCOA C On B.AcNo=C.Acno ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoAP' ");
                //SQL.AppendLine("        Where A.DocNo='@DocNo' ");
                SQL.AppendLine("        And A.COATaxInd = 'Y' ");
                SQL.AppendLine("        And C.AcNo=Concat(D.ParValue, A.VdCode)");
                SQL.AppendLine("    ) X ");
                SQL.AppendLine(") C On A.DocNo = C.DocNo ");
            }
            SQL.AppendLine("Where A.CancelInd='N' ");
            if (mIsPurchaseInvoiceUseApproval) SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("Select ");
            SQL.AppendLine("A.DocNo, '2' As InvoiceType, ");
            SQL.AppendLine("(A.Amt-IfNull(B.Amt, 0)) As Amt ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt ");
            SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And IfNull(T3.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T2.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            if (mIsOutgoingPaymentUsePIWithCOA)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select X.DocNo, X.AcNo, ");
                SQL.AppendLine("    Case ");
                SQL.AppendLine("        When X.AcType = 'D' && X.DAmt>0 Then X.DAmt ");
                SQL.AppendLine("        When X.AcType = 'D' && X.CAmt>0 Then X.CAmt *-1 ");
                SQL.AppendLine("        When X.AcType = 'C' && X.CAmt>0 Then X.Camt ");
                SQL.AppendLine("        When X.AcType = 'C' && X.DAmt>0 Then X.DAmt *-1 ");
                SQL.AppendLine("    End As AcAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo, B.AcNo, C.Actype, B.DAmt, B.Camt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblCOA C On B.AcNo=C.Acno ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoAP' ");
                //SQL.AppendLine("        Where A.DocNo='@DocNo' ");
                SQL.AppendLine("        And A.COATaxInd = 'Y' ");
                SQL.AppendLine("        And C.AcNo=Concat(D.ParValue, A.VdCode)");
                SQL.AppendLine("    ) X ");
                SQL.AppendLine(") C On A.DocNo = C.DocNo ");
            }
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine("Where Locate(Concat('##', DocNo, InvoiceType, '##'), @SelectedInvoice)>0; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedInvoice", GetSelectedInvoice());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "DocNo", "InvoiceType", "Amt"
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 2), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 2);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal string GetSelectedInvoice()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedInvoiceItem()
        {
            var SQL = string.Empty;
            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd5, Row, 1) +
                            Sm.GetGrdStr(Grd5, Row, 3) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");
            else
            {
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                if (mIsVoucherBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By A.Sequence; ");
            

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            string cent = string.Empty;
            if (cents.Length > 2)
            {
                decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
                cent  = Sm.Terbilang2(cettt);
            }
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblOutgoingPaymentHdr " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        internal void ProcessInvoiceItem(string InvoiceDocNo)
        {
            var l = new List<InvoiceItems>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo, T.DNo, T.ItCode, T.ItName, T.TotalPriceAfterTax, (T.TotalPriceAfterTax-T.Amt) as OutstandingAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    SELECT A.DocNo, B.DNo, C.ItCode, D.ItName, (C.Total*(IfNull(E.TaxRate, 0.00)+IfNull(F.TaxRate, 0.00)+IfNull(G.TaxRate, 0.00))/100)+C.Total As TotalPriceAfterTax, ifnull(D.Amt, 0) AS Amt  ");
            SQL.AppendLine("    FROM TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("    INNER JOIN TblPurchaseInvoiceDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN ( ");
            SQL.AppendLine("        SELECT A.DocNo, B.DNo, B.ItCode, ");
            SQL.AppendLine("        ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+((B.QtyPurchase/D.Qty)*D.RoundingValue)) As Total ");
            SQL.AppendLine("        FROM tblRecvVdHdr A ");
            SQL.AppendLine("        INNER JOIN TblRecvVdDtl B ON A.DocNo = B.DocNo And B.CancelInd='N' And IfNull(B.Status, 'O')='A' ");
            SQL.AppendLine("        Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("        Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            SQL.AppendLine("        Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("        Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine("    )C ON B.RecvVdDocNo = C.DocNo AND B.RecvVdDNo = C.DNo ");
            SQL.AppendLine("    INNER JOIN tblitem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    LEFT JOIN ( ");
            SQL.AppendLine("        SELECT C.DocNo, D.DNo, SUM(B.Amt+B.DownpaymentAmt-B.DiscCostAmt) Amt ");
            SQL.AppendLine("        FROM tbloutgoingpaymenthdr A ");
            SQL.AppendLine("        INNER JOIN tbloutgoingpaymentdtl5 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("            AND A.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(A.`Status`, 'O') <> 'C' ");
            SQL.AppendLine("        INNER JOIN tblpurchaseinvoicehdr C ON B.InvoiceDocNo = C.DocNo ");
            SQL.AppendLine("            AND C.CancelInd = 'N' ");
            SQL.AppendLine("            AND IFNULL(C.`Status`, 'O') <> 'F' ");
            SQL.AppendLine("        INNER JOIN tblpurchaseinvoicedtl D ON C.DocNo = D.DocNo AND B.InvoiceDNo = D.DNo ");
            SQL.AppendLine("        GROUP BY C.DocNo, D.DNo ");
            SQL.AppendLine("    )D ON A.DocNo = D.DocNo AND B.DNo = D.DNo ");
            SQL.AppendLine("    LEFT JOIN TblTax E ON A.TaxCode1 = E.TaxCode ");
            SQL.AppendLine("    LEFT JOIN TblTax F On A.TaxCode2 = F.TaxCode ");
            SQL.AppendLine("    LEFT JOIN TblTax G On A.TaxCode3 = G.TaxCode ");
            SQL.AppendLine("    Where A.DocNo = @DocNo ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Where T.TotalPriceAfterTax-Amt > 0 ");
            SQL.AppendLine("Order By T.TotalPriceAfterTax Desc; ");

            Sm.CmParam<String>(ref cm, "@DocNo", InvoiceDocNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DNo", "ItCode", "ItName", "TotalPriceAfterTax", "OutstandingAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new InvoiceItems() 
                        {
                            InvoiceDocNo = Sm.DrStr(dr, 0),
                            InvoiceDNo = Sm.DrStr(dr, 1),
                            ItCode = Sm.DrStr(dr, 2),
                            ItName = Sm.DrStr(dr, 3),
                            TotalPriceAfterTax = Sm.DrDec(dr, 4),
                            OutstandingAmtBefore = Sm.DrDec(dr, 5),
                            DownpaymentAmt = 0m,
                            DiscCostAmt = 0m
                        });
                }
                dr.Close();
            }
            ProcessInvoiceItem2(ref l, InvoiceDocNo);
        }

        private void ProcessInvoiceItem2(ref List<InvoiceItems> lInvoiceItem, string InvoiceDocNo)
        {
            int row = Grd5.Rows.Count - 1;
            for (int i = 0; i < lInvoiceItem.Count; i++)
            {
                Grd5.Cells[row + i, 1].Value = lInvoiceItem[i].InvoiceDocNo;
                Grd5.Cells[row + i, 2].Value = lInvoiceItem[i].InvoiceDNo;
                Grd5.Cells[row + i, 3].Value = lInvoiceItem[i].ItCode;
                Grd5.Cells[row + i, 4].Value = lInvoiceItem[i].ItName;
                Grd5.Cells[row + i, 5].Value = lInvoiceItem[i].TotalPriceAfterTax;
                Grd5.Cells[row + i, 6].Value = lInvoiceItem[i].DownpaymentAmt;
                Grd5.Cells[row + i, 7].Value = lInvoiceItem[i].DiscCostAmt;
                Grd5.Cells[row + i, 8].Value = lInvoiceItem[i].OutstandingAmtBefore;
                Grd5.Cells[row + i, 10].Value = 0m;

                Grd5.Rows.Add();
                Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10 });
            }
            ComputeItemsOutstandingAmt(InvoiceDocNo);
            Sm.FocusGrd(Grd5, 0, 1);
            lInvoiceItem.Clear();
        }

        internal void ComputeItemsOutstandingAmt(string InvoiceDocNo)
        {
            for (int row = 0; row < (InvoiceDocNo.Length == 0 ? Grd1.Rows.Count : 1); row++)
            {
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                decimal[] InvoiceHdr = new decimal[3];
                string DocNo = InvoiceDocNo.Length == 0 ? Sm.GetGrdStr(Grd1, row, 2) : InvoiceDocNo;
                decimal OutstandingAmt = 0m, DiscCost = 0m, DpAmt = 0m,
                    OutstandingAmt2 = 0m;

                SQL.AppendLine("SELECT (ifnull(A.Downpayment, 0.00)-ifnull(B.DownpaymentAmt,0.00)) AS DownpaymentAmt, (ifnull(A.DiscCost,0.00) - ifnull(B.DiscCostAmt, 0.00)) AS DiscCostAmt, ifnull(A.TaxRate, 0.00)/100 As TaxRate ");
                SQL.AppendLine("FROM( ");
                SQL.AppendLine("    SELECT A.DocNo, ");
                if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                    SQL.AppendLine("    (((IFNULL(C.TaxRate, 0.00)+IFNULL(D.TaxRate, 0.00)+IFNULL(E.TaxRate, 0.00))/100)*IFNULL(A.Downpayment, 0.00))+IFNULL(A.Downpayment, 0.00) As Downpayment, ");
                else
                    SQL.AppendLine("    A.Downpayment, ");
                SQL.AppendLine("    Case ");
                SQL.AppendLine("    When A.COATaxInd = 'N' Then (((IFNULL(C.TaxRate, 0.00)+ IFNULL(D.TaxRate, 0.00)+IFNULL(E.TaxRate, 0.00))/100)*ifnull(DiscCostAmt, 0.00))+ifnull(DiscCostAmt, 0.00) ");
                SQL.AppendLine("    Else IFNULL(DiscCostAmt, 0.00)");
                SQL.AppendLine("    End As DiscCost, ");
                SQL.AppendLine("    ifnull(C.TaxRate, 0.00) + ifnull(D.TaxRate, 0.00) + ifnull(E.TaxRate, 0.00) AS TaxRate ");
                SQL.AppendLine("    FROM TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("    LEFT JOIN (");
                SQL.AppendLine("        SELECT A.DocNo, SUM(Case When C.AcType = 'D' Then B.DAmt-B.CAmt ELSE B.CAmt-B.DAmt END ) DiscCostAmt ");
                SQL.AppendLine("        FROM TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        INNER JOIN TblPurchaseInvoiceDtl4 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
                if (mPurchaseInvoiceCOAAmtCalculationMethod == "1")
                    SQL.AppendLine("            And B.AcNo = (SELECT CONCAT(ParValue, A.VdCode) FROM TblParameter WHERE parcode = 'VendorAcNoAP') ");
                else
                {
                    SQL.AppendLine("            AND B.AcNo IN ( ");
                    SQL.AppendLine("                SELECT E.AcNo9 ");
                    SQL.AppendLine("                FROM TblPurchaseInvoiceHdr A ");
                    SQL.AppendLine("                INNER JOIN TblPurchaseInvoiceDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("                INNER JOIN TblRecvVdDtl C ON B.RecvVdDocNo = C.DocNo ");
                    SQL.AppendLine("                    AND B.RecvVdDNo = C.DNo ");
                    SQL.AppendLine("                    AND C.CancelInd = 'N' ");
                    SQL.AppendLine("                INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("                INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode ");
                    SQL.AppendLine("                WHERE A.DocNo = @DocNo ");
                    SQL.AppendLine("            ) ");
                }
                SQL.AppendLine("        WHERE A.DocNo = @DocNo ");
                SQL.AppendLine("        GROUP BY A.DocNo ");
                SQL.AppendLine("    )B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    LEFT JOIN TblTax C ON A.TaxCode1 = C.TaxCode ");
                SQL.AppendLine("    LEFT JOIN TblTax D ON A.TaxCode2 = D.TaxCode ");
                SQL.AppendLine("    LEFT JOIN TblTax E ON A.TaxCode3 = E.TaxCode ");
                SQL.AppendLine("    WHERE A.DocNo = @DocNo ");
                SQL.AppendLine(")A ");
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT B.InvoiceDocNo, SUM(ifnull(B.DiscCostAmt, 0.00)) DiscCostAmt, SUM(IFNULL(B.DownpaymentAmt, 0.00)) DownPaymentAmt ");
                SQL.AppendLine("    FROM tbloutgoingpaymenthdr A ");
                SQL.AppendLine("    INNER JOIN tbloutgoingpaymentdtl5 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    WHERE B.InvoiceDocNo = @DocNo AND A.CancelInd = 'N' AND IFNULL(A.`Status`, 'O') <> 'C' ");
                SQL.AppendLine("    GROUP BY B.InvoiceDocNo ");
                SQL.AppendLine(")B ON A.DocNo = B.InvoiceDocNo ");
                SQL.AppendLine("WHERE A.DocNo = @DocNo ");
                SQL.AppendLine("Limit 1; ");

                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DownpaymentAmt",

                    //1-5
                    "DiscCostAmt", "TaxRate"
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            InvoiceHdr[0] = Sm.DrDec(dr, 0);
                            InvoiceHdr[1] = Sm.DrDec(dr, 1);
                            InvoiceHdr[2] = Sm.DrDec(dr, 2);
                        }
                    }
                    dr.Close();
                }

                decimal 
                    OutstandingDP = InvoiceHdr[0],
                    OutstandingDiscCost = InvoiceHdr[1],
                    TaxRate = InvoiceHdr[2];

                for (int row2 = 0; row2 < Grd5.Rows.Count - 1; row2++)
                {
                    if (DocNo == Sm.GetGrdStr(Grd5, row2, 1))
                    {
                        //OutstandingAmt2 = (Sm.GetGrdDec(Grd5, row2, 8) * TaxRate) + Sm.GetGrdDec(Grd5, row2, 8); 
                        OutstandingAmt2 = Sm.GetGrdDec(Grd5, row2, 8);
                        //Get Disc,Cost, Etc and downpayment
                        if (Sm.GetGrdDec(Grd5, row2, 8) + OutstandingDiscCost >= 0)
                        {
                            OutstandingAmt = OutstandingAmt2 + OutstandingDiscCost;
                            DiscCost = OutstandingDiscCost;
                            OutstandingDiscCost = 0m;
                            if (OutstandingAmt > OutstandingDP)
                            {
                                OutstandingAmt = OutstandingAmt - OutstandingDP;
                                DpAmt = OutstandingDP;
                                OutstandingDP = 0m;
                            }
                            else
                            {
                                OutstandingDP = OutstandingDP - OutstandingAmt;
                                DpAmt = OutstandingAmt;
                                OutstandingAmt = 0m;
                            }
                        }
                        else
                        {
                            DiscCost = -OutstandingAmt2;
                            OutstandingDiscCost = OutstandingDiscCost - DiscCost;
                            OutstandingAmt = 0m;
                            DpAmt = 0;
                        }
                        Grd5.Cells[row2, 6].Value = DpAmt;
                        Grd5.Cells[row2, 7].Value = DiscCost;
                        Grd5.Cells[row2, 9].Value = OutstandingAmt;
                        if (OutstandingAmt == 0)
                        {
                            Grd5.Cells[row2, 10].ReadOnly = iGBool.True;
                            Grd5.Cells[row2, 10].BackColor = Color.FromArgb(224, 224, 224);
                        }
                    }
                }
            }
            ComputeInvoiceAmt();
        }

        private void RemoveInvoiceItems()
        {
            for (int row = Grd5.Rows.Count-2; row >= 0 ; row--)
            {
                bool removeInd = true;
                for (int row2 = 0; row2 < Grd1.Rows.Count - 1; row2++)
                {
                    if (Sm.GetGrdStr(Grd5, row, 1) == Sm.GetGrdStr(Grd1, row2, 2))
                        removeInd = false;
                }
                if (removeInd)
                {
                    Grd5.Rows.RemoveAt(row);
                }
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsOutgoingPaymentPaidToBankBasedOnVendor)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                    });
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, true);
                    Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
                    if (Sm.GetLue(LueVdCode).Length != 0)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                        SetLuePaidToBankCode(ref LuePaidToBankCode, Sm.GetLue(LueVdCode), "2");
                    }
                }

                TxtCOAAmt.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
                ComputeGiroAmt();
                ComputeAmt();
            }
        }

        private void MeeVoucherRequestSummaryDesc_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.TxtTrim(MeeVoucherRequestSummaryDesc);

                ChkMeeVoucherRequestSummaryInd.Checked =
                    (MeeVoucherRequestSummaryDesc.Text.Length == 0) ? false : true;
            }
        }

        private void MeeRemark_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(MeeRemark);
        }

        private void ChkMeeVoucherRequestSummaryInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!ChkMeeVoucherRequestSummaryInd.Checked) MeeVoucherRequestSummaryDesc.EditValue = null;
            }
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                SetBankCode();
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || 
                Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                SetBankCode();
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtRateAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtRateAmt, 0);
                ComputeAmt2();
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                LueCurCode.EditValue = null;
                try
                {
                    if (mBankAccountFormat == "1" || mBankAccountFormat == "2")
                        Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
                    else
                        Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
                    if (Sm.GetLue(LueBankAcCode).Length > 0)
                        SetCurCode(Sm.GetLue(LueBankAcCode));
                    else
                        LueCurCode.EditValue = mMainCurCode;
                    SetBankAcTp();
                    SetBankCode();
                 }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void LuePaidToBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsOutgoingPaymentPaidToBankBasedOnVendor)
                {
                    Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue3(SetLuePaidToBankCode), Sm.GetLue(LueVdCode), "2");
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo });
                    if (Sm.GetLue(LuePaidToBankCode).Length != 0)
                        ShowPaidToBankCodeInfo(
                            Sm.GetLue(LueVdCode),
                            LuePaidToBankCode.GetColumnValue("Col4").ToString()
                            );
                }
                else
                    Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
            }
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankBranch);
        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcName);
        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcNo);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(SetLueBCCode), string.Empty, Sm.GetGrdStr(Grd1, 0, 15));
                ComputeRemainingBudget();
            }
        }

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count > 1)
            {
                Grd5.Rows.Clear();
                Grd5.Rows.Count = 1;
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    ProcessInvoiceItem(Sm.GetGrdStr(Grd1, row, 2));
                }
            }
        }

        #endregion

        #region Grid Methods

        #region Grd3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmOutgoingPayment2Dlg2(this, Sm.GetLue(LueVdCode)));
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeGiroAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                Sm.FormShowDialog(new FrmOutgoingPayment2Dlg2(this, Sm.GetLue(LueVdCode)));
        }

      
        #endregion 

        #region Grid4 (List Of COA's Account Number)

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length != 0)
            {
                Grd4.Cells[e.RowIndex, 4].Value = 0;
                ComputeCOAAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length != 0)
            {
                Grd4.Cells[e.RowIndex, 3].Value = 0;
                ComputeCOAAmt();
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmOutgoingPayment2Dlg3(this, Sm.GetLue(LueVdCode)));
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                if (mIsOutgoingPaymentAmtUseCOAAmt)
                {
                    if (!Sm.IsLueEmpty(LueVdCode, "Vendor"))
                        Sm.FormShowDialog(new FrmOutgoingPayment2Dlg3(this, Sm.GetLue(LueVdCode)));
                }
                else
                    Sm.FormShowDialog(new FrmOutgoingPayment2Dlg3(this, Sm.GetLue(LueVdCode)));
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid5 (List of Items)

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length != 0)
            {
                ComputeInvoiceAmt();
                ComputeAmt();
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmOutgoingPayment2Dlg4(this));
            }
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !IsGrdEmpty())
            {
                Sm.FormShowDialog(new FrmOutgoingPayment2Dlg4(this));
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            ComputeItemsOutstandingAmt("");
            ComputeAmt();
            Sm.GrdEnter(Grd5, e);
            Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region Class

        class OutgoingPaymentHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public decimal DocEnclosure { get; set; }
            public string PaymentUser { get; set; }
            public string Department { get; set; }
            public string PrintFormat { get; set; }
            public string VRDocNo { get; set; }
            public string VRDocDt { get; set; }
            public string VdName { get; set; }
            public string SiteInd { get; set; }
            public string BankAcNo { get; set; }
            public string PaidToBankAcName { get; set; }
            public string PaidToBAnkAcNo { get; set; }
            public string PaidToBankBranch { get; set; }
            public string Project { get; set; }
            public string BankName { get; set; }
            public string Currency { get; set; }
        }

        class OutgoingPaymentDtl
        {
            public string DNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
            public string InvoiceDocNo { get; set; }
            public string InvoiceDocDt { get; set; }
        }

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class OutgoingPaymentDtl2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class OPSignIMS
        {
            //old
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            //

            public string Label1 { get; set; }
            public string Username { get; set; }
            public string PosName { get; set; }
            public string Label2 { get; set; }
            public string Username2 { get; set; }
            public string PosName2 { get; set; }
            public string Label3 { get; set; }
            public string Username3 { get; set; }
            public string PosName3 { get; set; }
        }

        private class OPSignIMS2
        {
            //old
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            //

            public string Username { get; set; }
            public string Username2 { get; set; }

        }

        private class InvoiceItems
        {
            public string InvoiceDocNo { get; set; }
            public string InvoiceDNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal TotalPriceAfterTax { get; set; }
            public decimal DownpaymentAmt { get; set; }
            public decimal DiscCostAmt { get; set; }
            public decimal OutstandingAmtBefore { get; set; }
            public decimal OutstandingAmt { get; set; }
        }

        private class InvoiceHdr
        {
            public decimal Downpayment { get; set; }
            public decimal DiscCost { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
        }

        #endregion

    }
}
