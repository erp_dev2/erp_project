﻿#region Update
/*
    22/01/2018 [TKG] tambah info dan filter site
    12/04/2018 [HAR] validasi entity berdasarkan entity di group user
    17/07/2018 [TKG] Berdasarkan IsJournalCostCenterEnabled, informasi cost center akan ditampilkan atau tidak.
    11/02/2019 [TKG] tambah indikator aktif
    17/02/2019 [TKG] memunculkan created by pada saat di-group
    25/11/2019 [TKG/TWC] tambah informasi cancel
    20/01/2020 [WED/SIER] salah ambil kolom Alias
    19/05/2020 [DITA/YK] Filter COA berdasarkan group --> param = IsCOAFilteredByGroup
    03/09/2020 [TKG/ALL] tambah filter untuk menampilkan data dengan total debit/credit yg nilainya 0 semua.
    07/12/2020 [DITA/IMS] tambah kolom socontractdocno berdasarkan parameter
    06/01/2021 [TKG/GSS] tambah kolom cancelled journal
    03/02/2021 [TKG/PHT] Berdasarkan parameter IsJournalFilterByGroupCC, cost center difilter berdasarkan cost center group
    01/03/2021 [WED/IMS] munculkan informasi remark detail berdasarkan parameter IsJournalShowRemarkDetail
    18/05/2021 [VIN/IMS] filter unbalance di round 2 decimal 
    20/05/2021 [TKG/PHT] divalidasi menggunakan otorisasi group thd profit center
    20/06/2021 [TKG/PHT] ubah query menggunakan otorisasi group thd profit center
    30/06/2021 [TKG/PHT] ubah validasi cost center berdasarkan profit center
    14/10/2021 [ARI/ALL] menambah filter field combo box cost center berdasarkan groupnya
    11/11/2021 [ISD/AMKA] membuat filter cost center terfilter berdasarkan group user dengan parameter IsJournalTransactionCCFilteredByGroup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmJournalFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmJournal mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsReportingFilterByEntity = false;
        internal bool mIsCOAFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmJournalFind(FrmJournal FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -3);
                SetLueEntCode(ref LueEntCode);
                //SetLueCCCode(ref LueCCCode);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mFrmParent.mIsJournalTransactionCCFilteredByGroup ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.JnDesc, ");
            SQL.AppendLine("B.AcNo, C.AcDesc, C.Alias, A.CurCode, B.DAmt, B.CAmt, A.MenuCode, A.MenuDesc, D.EntName, E.CCName, ");
            SQL.AppendLine("C.ActInd, A.JournalDocNo, B.SOContractDocNo, A.Remark, B.Remark As RemarkDtl, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Left Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblCoa C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Left Join TblEntity D On B.EntCode=D.EntCode ");
            if (mFrmParent.mIsJournalTransactionCCFilteredByGroup)
            {
                SQL.AppendLine("Inner Join TblCostCenter E ON A.CCCode=E.CCCode ");
                SQL.AppendLine("AND EXISTS ( ");
                SQL.AppendLine("    SELECT 1 FROM TblGroupCostCenter ");
                SQL.AppendLine("    WHERE CCCode=E.CCCode ");
                SQL.AppendLine("    AND GrpCode IN (SELECT GrpCode FROM TblUser WHERE ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblCostCenter E On A.CCCode=E.CCCode ");
            }
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mFrmParent.mIsJournalUseProfitCenter)
            {
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                SQL.AppendLine("    Select Distinct X1.CCCode ");
                SQL.AppendLine("    From TblCostCenter X1, TblGroupProfitCenter X2, TblUser X3 ");
                SQL.AppendLine("    Where X1.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And X1.ProfitCenterCode=X2.ProfitCenterCode ");
                SQL.AppendLine("    And X2.GrpCode=X3.GrpCode ");
                SQL.AppendLine("    And X3.UserCode=@UserCode ");
                SQL.AppendLine("    ))) ");
            }
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And (B.EntCode Is Null Or (B.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                //SQL.AppendLine("        And AcNo = B.AcNo ");
                SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mIsJournalFilterByGroupCC)
            {
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                SQL.AppendLine("    Select Distinct CCCode From TblGroupCostCenter ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ))) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Description",
                        "Account#",
                        
                        //6-10
                        "Account Description",
                        "Currency",
                        "Debit",
                        "Credit",
                        "Menu Code",
                        
                        //11-15
                        "Menu Description",
                        "Entity",
                        "Cost Center",
                        "Active",
                        "Alias",
                        
                        //16-20
                        "Journal (Cancel)",
                        "SO Contract#",
                        "Remark",
                        "Remark Detail",
                        "Created By",

                        //21-25
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        "Updated Date", 
                        "Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        180, 80, 80, 250, 150, 
                        
                        //6-10
                        250, 80, 150, 150, 100, 

                        //11-15
                        150, 180, 200, 80, 150, 

                        //16-20
                        150, 150, 200, 200, 120, 
                        
                        //21-25
                        120, 120, 120, 120, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 21, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25 }, false);
            if (!mFrmParent.mIsJournalShowRemarkDetail)
                Sm.GrdColInvisible(Grd1, new int[] { 19 });
            Grd1.Cols[13].Visible = mFrmParent.mIsJournalCostCenterEnabled;
            Grd1.Cols[15].Visible = mFrmParent.mIsCOAUseAlias;
            Grd1.Cols[17].Visible = mFrmParent.mIsJournalUseSOContract;
            Sm.SetGrdProperty(Grd1, false);
        }


        private void GetParameter()
        {
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mIsCOAFilteredByGroup = Sm.GetParameterBoo("IsCOAFilteredByGroup");
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkZeroAmt.Checked)
                {
                    Filter =
                        " And A.DocNo In ( " +
                        "   Select T1.DocNo " +
                        "   From TblJournalHdr T1, TblJournalDtl T2 " +
                        "   Where T1.DocNo=T2.DocNo " +
                        "   And T1.DocDt Between @DocDt1 And @DocDt2 " +
                        "   Group By T1.DocNo " +
                        "   Having Sum(T2.DAmt)=0.00 And Sum(T2.CAmt)=0.00 " +
                        "   ) ";
                }

                if (ChkUnbalancedJournal.Checked)
                {
                    Filter =
                        " And A.DocNo In ( " +
                        "   Select T1.DocNo " +
                        "   From TblJournalHdr T1, TblJournalDtl T2 " +
                        "   Where T1.DocNo=T2.DocNo " +
                        "   And T1.DocDt Between @DocDt1 And @DocDt2 " +
                        "   Group By T1.DocNo " +
                        "   Having Round(Sum(T2.DAmt),2)<>Round(Sum(T2.CAmt),2) " +
                        "   ) ";
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtJnDesc.Text, "A.JnDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "B.EntCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",
                            
                            //1-5
                            "DocDt", "CancelInd", "JnDesc", "AcNo", "AcDesc", 
                            
                            //6-10
                            "CurCode", "DAmt", "CAmt", "MenuCode", "MenuDesc", 
                            
                            //11-15
                            "EntName", "CCName", "ActInd", "Alias", "JournalDocNo", 
                            
                            //16-20
                            "SOContractDocNo", "Remark", "RemarkDtl", "CreateBy", "CreateDt", 
                            
                            //21-22
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 22);
                          }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("    Select A.EntCode, B.EntName  From TblGroupEntity A ");
                SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode And B.ActInd='Y' ");
                SQL.AppendLine("    Where A.GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T Where T.ActInd='Y'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter ");
            SQL.AppendLine("Where 1=1 ");
                if (mFrmParent.mIsJournalFilterByGroupCC)
                {
                    SQL.AppendLine("And (CCCode Is Null Or (CCCode Is Not Null And CCCode In ( ");
                    SQL.AppendLine("    Select Distinct CCCode From TblGroupCostCenter ");
                    SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }

                SQL.AppendLine("Order By CCName;");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtJnDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkJnDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);

        }
        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");

        }
        #endregion



        #endregion


    }
}
