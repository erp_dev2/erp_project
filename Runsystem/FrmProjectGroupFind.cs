﻿#region Update 
/*
    23/10/2019 [VIN/IMS] Pembuatan menu master proyek, menambah kolom find
    03/12/2020 [HAR/IMS] karena customer tidak mandatory maka query join ke dpeartment di ubah ke LEFT Join
    28/05/2021 [IBL/IMS] Penambahan kolom InternalInd berdasarkan parameter IsProjectGroupUseInternalInd
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProjectGroupFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProjectGroup mFrmParent;

        #endregion

        #region Constructor

        public FrmProjectGroupFind(FrmProjectGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "InternalInd",
                        "Project"+Environment.NewLine+"Code",
                        
                         
                        
                        //6-10
                        "Project"+Environment.NewLine+"Name",
                        "Customer",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",

                        //11-13
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        30,
                        //1-5
                        100, 200, 50, 90, 100, 
                        //6-10
                        200, 200, 100, 100, 100,100,
                        //11-13
                        100, 100,100
                    
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9,10,11,12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (!mFrmParent.mIsProjectGroupUseInternalInd)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPGCode.Text, new string[] { "PGCode", "PGName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "SELECT A.PGCode, A.PGName, A.ActInd, A.InternalInd, A.ProjectCode, A.ProjectName, B.CtName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt FROM TblProjectGroup As A LEFT JOIN TblCustomer As B ON A.CtCode=B.CtCode " 
                        + Filter + " Order By PGName ",

                        new string[]
                        {
                            //0
                            "PGCode", 
                                
                            //1-5
                            "PGName", "ActInd", "InternalInd", "ProjectCode", "ProjectName",   
                            
                            //6-10
                            "CtName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkPGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project group");
        }

        private void TxtPGCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
