﻿#region Update
/*
    14/03/2021 [TKG/PHT] new apps
 *  28/07/2021 [HAR/PHT] bug saat method IsCodeAlreadyChosen masih mengarahe ke grid13 harusnya grid14
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGroupDlg14 : RunSystem.FrmBase4
    {
        #region Field

        private FrmGroup mFrmParent;

        #endregion

        #region Constructor

        public FrmGroupDlg14(FrmGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Code",
                    "Name"
                }, new int[]
                {
                    //0
                    50,

                    //1-3
                    20, 130, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where ProfitCenterCode Not In (" + mFrmParent.GetSelectedProfitCenter() + ") ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCode.Text, new string[] { "ProfitCenterCode", "ProfitCenterName" });
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, "Select ProfitCenterCode, ProfitCenterName From TblProfitCenter " + Filter + " Order By ProfitCenterName;",
                    new string[] { "ProfitCenterCode", "ProfitCenterName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;
                        Row1 = mFrmParent.Grd14.Rows.Count - 1;
                        Row2 = Row;
                        Sm.CopyGrdValue(mFrmParent.Grd14, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd14, Row1, 2, Grd1, Row2, 3);
                        mFrmParent.Grd14.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 profit center.");
        }
        private bool IsCodeAlreadyChosen(int Row)
        {
            if (mFrmParent.Grd14.Rows.Count > 1)
            {
                for (int Index = 0; Index < mFrmParent.Grd14.Rows.Count - 1; Index++)
                    if (Sm.CompareStr(
                        Sm.GetGrdStr(mFrmParent.Grd14, Index, 1),
                        Sm.GetGrdStr(Grd1, Row, 2)
                        )) return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion

    }
}
