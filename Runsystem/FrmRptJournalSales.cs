﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptJournalSales : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptJournalSales(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'DR' As Type, T1.DocNo, T1.DocDt, IfNull(T2.Amt, 0) As Amt ");
            SQL.AppendLine("From TblDOCt2Hdr T1 ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select A.DocNo, ");
            SQL.AppendLine("    Sum(Case When C.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End*C.UPrice*B.Qty) As Amt ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct A.DocNo, G.ItCode, C.CurCode, ");
            SQL.AppendLine("        (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("        From TblDrHdr A ");
            SQL.AppendLine("        Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("        Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            SQL.AppendLine("        Where Exists( ");
            SQL.AppendLine("            Select Tbl.DocNo ");
            SQL.AppendLine("            From TblDOCt2Hdr Tbl ");
            SQL.AppendLine("            Where Tbl.DRDocNo Is Not Null ");
            SQL.AppendLine("            And Tbl.DRDocNo=A.DocNo ");
            SQL.AppendLine("            And (Tbl.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        )  ");
            SQL.AppendLine("    ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("    Where A.DrDocNo Is Not Null ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And T1.DRDocNo Is Not Null ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PL' As Type, T1.DocNo, T1.DocDt, IfNull(T2.Amt, 0) As Amt ");
            SQL.AppendLine("From TblDOCt2Hdr T1 ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select A.DocNo, ");
            SQL.AppendLine("    Sum(Case When C.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End*C.UPrice*B.Qty) As Amt ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct A.DocNo, G.ItCode, C.CurCode, ");
            SQL.AppendLine("        (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("        From TblPLHdr A ");
            SQL.AppendLine("        Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("        Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            SQL.AppendLine("        Where Exists( ");
            SQL.AppendLine("            Select Tbl.DocNo ");
            SQL.AppendLine("            From TblDOCt2Hdr Tbl ");
            SQL.AppendLine("            Where Tbl.PLDocNo Is Not Null ");
            SQL.AppendLine("            And Tbl.PLDocNo=A.DocNo ");
            SQL.AppendLine("            And (Tbl.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        )  ");
            SQL.AppendLine("    ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("    Where A.PLDocNo Is Not Null ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And T1.PLDocNo Is Not Null ");
            SQL.AppendLine("Order By Type, DocDt, DocNo;");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-4
                        "Document#", 
                        "Date",
                        "Type",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 100, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParam<string>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-3
                            "DocDt", "Type", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
