﻿#region Update
/*
   02/12/2020 [DITA/IMS] New Apps
 */ 
#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectBudgetResource : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
           mMenuCode = string.Empty,
           mAccessInd = string.Empty,
           mDocNo = string.Empty;
          internal FrmProjectBudgetResourceFind FrmFind;

        #endregion

        #region Constructor

        public FrmProjectBudgetResource(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnSOContractDocNo2.Visible = GetAccessInd("FrmSOContract2");

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueOption(ref LueProcessInd, "ProjectBudgetResourceProcessInd");

                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                { 
                    //0
                    "",

                    //1-5
                    "Account#", 
                    "Account Description", 
                    "Bid and "+Environment.NewLine+"Pricing Quotation", 
                    "Financial"+Environment.NewLine+"Budget", 
                    "Remark", 

                },
                new int[] 
                { 
                    //0
                    20, 
                    //1-5
                    150, 200, 150, 100, 200, 
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] {  1, 2 });
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtSOContractDocNo, TxtSoContractAmt,
                        TxtPONo, LueCtCode, MeeRemark, LueProcessInd, TxtProjectName,
                        TxtProjectCode, TxtNumber, TxtTotalBidAndPricingQt, TxtTotalFinancialBudget
                    }, true);
                    BtnSOContractDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, TxtNumber
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, true);
                    BtnSOContractDocNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (Sm.GetLue(LueProcessInd) == "D")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd, TxtNumber, MeeRemark }, false);
                        Grd1.ReadOnly = false;
                        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
                    }

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtSOContractDocNo, TxtSoContractAmt,
                TxtPONo, LueCtCode, MeeRemark, LueProcessInd, TxtProjectName,
                TxtProjectCode, TxtNumber, TxtTotalBidAndPricingQt, TxtTotalFinancialBudget
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotalFinancialBudget, TxtTotalBidAndPricingQt, TxtSoContractAmt}, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4 });
        }
        #endregion

        #endregion

        #region Additional Methods

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeTotalBidAndPricingQuotation()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd1, row, 3);
                }
            }
            TxtTotalBidAndPricingQt.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeTotalFinancialBudget()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd1, row, 4);
                }
            }
            TxtTotalFinancialBudget.EditValue = Sm.FormatNum(Total, 0);
        }


        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'ProjectBudgetResource' ");
          
            return Sm.IsDataExist(SQL.ToString());
        }

        internal void ShowSOContractDocument(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.CtCode, D.ProjectCode, D.ProjectName, A.Amt+A.AmtBOM SOContractAmt, A.PONo ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr C On B.LOPDocNo = C.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup D ON C.PGCode=D.PGCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.CreateDt Desc Limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "CtCode", "ProjectCode", "ProjectName", "SOContractAmt", "PONo",

                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[1]));
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[2]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[3]);
                    TxtSoContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    TxtPONo.EditValue = Sm.DrStr(dr, c[5]);
                    
                }, true
            );
        }
       
        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectBudgetResourceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetLue(LueProcessInd, "D");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    EditData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3, 4 }, e);
                if (Sm.IsGrdColSelected(new int[] { 3, 4 }, e.ColIndex))
                {
                    ComputeTotalBidAndPricingQuotation();
                    ComputeTotalFinancialBudget();
                }
            }

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd1.ReadOnly)
            {
                if (e.ColIndex == 0) Sm.FormShowDialog(new FrmProjectBudgetResourceDlg2(this));
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd1.ReadOnly)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectBudgetResourceDlg2(this));
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
                }
               
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalBidAndPricingQuotation();
                ComputeTotalFinancialBudget();
            }

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectBudgetResource", "TblProjectBudgetResourceHdr");

            var cml = new List<MySqlCommand>();

            ComputeTotalBidAndPricingQuotation();
            ComputeTotalFinancialBudget();

            cml.Add(SaveProjectBudgetResourceHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveProjectBudgetResourceDtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false) ||
                IsSOContractAlreadyProcessed() ||
                IsSOContractCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsSOContractCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSOContractRevisionHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And (B.CancelInd = 'Y' Or B.Status = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtSOContractDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO Contract is cancelled.");
                return true;
            }

            return false;
        }

        private bool IsSOContractAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectBudgetResourceHdr ");
            SQL.AppendLine("Where SOContractDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('A', 'O') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtSOContractDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO Contract document has already processed to Project Budget Resoirce #" + Sm.GetValue(SQL.ToString(), TxtSOContractDocNo.Text));
                TxtSOContractDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Account#.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Account# is empty.")) return true; 
            }


            return false;
        }

        private MySqlCommand SaveProjectBudgetResourceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectBudgetResourceHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status,ProcessInd, CancelInd, SOContractDocNo,  ");
            SQL.AppendLine(" TotalBidPricingQt, TotalFinancialBudget, Remark, Number, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O','D', 'N', @SOContractDocNo,  ");
            SQL.AppendLine(" @TotalBidPricingQt, @TotalFinancialBudget, @Remark, @Number, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='ProjectBudgetResource' ");
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblProjectBudgetResourceHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'ProjectBudgetResource' ");
            SQL.AppendLine(");     ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Number", TxtNumber.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalBidPricingQt", Sm.GetDecValue(TxtTotalBidAndPricingQt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalFinancialBudget", Sm.GetDecValue(TxtTotalFinancialBudget.Text));
           
            return cm;
        }

        private MySqlCommand SaveProjectBudgetResourceDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectBudgetResourceDtl(DocNo, DNo, AcNo, BidPricingQtAmt, FinancialBudgetAmt, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @AcNo, @BidPricingQtAmt, @FinancialBudgetAmt, @Remark,  @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@BidPricingQtAmt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@FinancialBudgetAmt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            ComputeTotalBidAndPricingQuotation();
            ComputeTotalFinancialBudget();

            cml.Add(EditProjectBudgetResourceHdr());
            if (MeeCancelReason.Text.Length <= 0)
            {
                cml.Add(DeleteProjectBudgetResourceDtl());

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveProjectBudgetResourceDtl(TxtDocNo.Text, Row));
            }


            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                IsDataCancelledAlready() ||
                IsGrdValueNotValid();
            ;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProjectBudgetResourceHdr ");
            SQL.AppendLine("Where (CancelInd='Y' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditProjectBudgetResourceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectBudgetResourceHdr Set ");
            SQL.AppendLine(" ProcessInd=@ProcessInd,CancelReason=@CancelReason, CancelInd=@CancelInd, Number=@Number, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(), ");
            SQL.AppendLine(" TotalBidPricingQt=@TotalBidPricingQt, TotalFinancialBudget=@TotalFinancialBudget, Remark=@Remark  ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status <> 'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Number", TxtNumber.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalBidPricingQt", Sm.GetDecValue(TxtTotalBidAndPricingQt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalFinancialBudget", Sm.GetDecValue(TxtTotalFinancialBudget.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
           

            return cm;
        }

        private MySqlCommand DeleteProjectBudgetResourceDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblProjectBudgetResourceDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectBudgetResourceHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProjectBudgetResourceHdr(DocNo);
                ShowProjectBudgetResourceDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectBudgetResourceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.SOContractDocNo, B.PONo, B.Amt+B.AmtBOM SOContractAmt,  ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, A.ProcessInd,  ");
            SQL.AppendLine("E.ProjectCode, E.ProjectName, ");
            SQL.AppendLine("C.CtCode, A.Number , A.TotalBidPricingQt, A.TotalFinancialBudget, A.Remark");
            SQL.AppendLine("From TblProjectBudgetResourceHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr D On C.LOPDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup E ON D.PGCode=E.PGCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "SOContractDocNo", "PONo",  
                    
                    //6-10
                    "SOContractAmt", "StatusDesc", "ProcessInd", "ProjectCode", "ProjectName",   
                    
                    //11-15
                    "CtCode", "Number", "TotalBidPricingQt", "TotalFinancialBudget", "Remark",

                    
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtPONo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtSoContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[8]));
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[9]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[10]);
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[11]));
                    TxtNumber.EditValue = Sm.DrStr(dr, c[12]);
                    TxtTotalBidAndPricingQt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                    TxtTotalFinancialBudget.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    
                    
                }, true
            );
        }

        private void ShowProjectBudgetResourceDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.AcNo, B.AcDesc, A.BidPricingQtAmt, A.FinancialBudgetAmt, A.Remark ");
            SQLDtl.AppendLine("From TblProjectBudgetResourceDtl A ");
            SQLDtl.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo");
            SQLDtl.AppendLine("Where A.DocNo = @DocNo");
            SQLDtl.AppendLine("Order By A.AcNo; ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "BidPricingQtAmt", "FinancialBudgetAmt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                   
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });

        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProjectBudgetResourceDlg(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmSOContract2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectBudgetResourceInd");
        }
      
        #endregion

        #endregion
    }
}
