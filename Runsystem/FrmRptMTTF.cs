﻿#region Update
// 25/07/2017 [HAR] rumus penyebut diganti berdasarkan siklus document
// 25/09/2017 [HAR] tambah parameter untuk mementukan formula rumus
// 05/06/2018 [ARI] tambah kolom dan filter status
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMTTF : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mMTPMFormula = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMTTF(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
                SetLueStatusCode(ref LueStatus);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMTPMFormula = Sm.GetParameter("MTPMFormula");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mMTPMFormula == "0")
            {
                SQL.AppendLine("Select  X.AssetCode, X.AssetName, X.DisplayName, ");
                SQL.AppendLine("(DateDiff(MaxDownDt, MinDownDt)/ (CountData-1))AVG, X.Status, X.Status2 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.AssetCode, D.AssetName, D.DisplayName, E.OptDesc As Status, A.Status As Status2, ");
                SQL.AppendLine("    MIN(B.DownDt) MinDownDt, MAX(B.DownDt) MaxDownDt, Count(A.AssetCode) CountData ");
                SQL.AppendLine("    From TblTOhdr A  ");
                SQL.AppendLine("    Inner Join TblWOR B On A.AssetCode=B.TOCode  ");
                SQL.AppendLine("    Inner Join TblAsset D On A.AssetCode=D.AssetCode  ");
                SQL.AppendLine("    Left Join TblOption E On A.Status=E.OptCode And E.Optcat = 'MaintenanceStatus' ");
                SQL.AppendLine("    Where B.cancelInd = 'N' And B.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    Group BY A.AssetCode, A.AssetCode, D.AssetName, D.DisplayName ");
                SQL.AppendLine("    Order By A.AssetCode ");
                SQL.AppendLine(")X  ");
            }
            else
            {
                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select Y.AssetCode, Y3.Assetname, Y3.DisplayName, ((Y2.MaxHM-Y2.MinHM) / Y2.CountData) As AVG, Y2.Status, Y2.Status2 ");
                SQL.AppendLine("    From TblTOHdr y ");
                SQL.AppendLine("    left Join  ");
                SQL.AppendLine("    (   ");
                SQL.AppendLine("        Select X.AssetCode, Min(hoursmeter) MinHM, max(hoursmeter) MaxHM, Count(DocNo) CountData, X.Status, X.Status2  ");
                SQL.AppendLine("        From  ");
                SQL.AppendLine("        (  ");
                SQL.AppendLine("            Select C.DocNo, A.AssetCode, ifnull(E.hoursMeter, 0) As hoursMeter, F.OptDesc As Status, A.Status As Status2 ");
				SQL.AppendLine("                From TblTOHdr A ");
				SQL.AppendLine("                left Join TblWOR B On A.AssetCode = B.ToCode ");
				SQL.AppendLine("                Left Join TblWOhdr C On C.WORDocNo = B.Docno And C.cancelInd = 'N' ");
				SQL.AppendLine("                Left Join tblHoursmeter E On C.HmDocNo = E.DocNo  ");
                SQL.AppendLine("                Left Join TblOption F On A.Status=F.OptCode And F.Optcat = 'MaintenanceStatus' ");
				SQL.AppendLine("                Where C.DocDt between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        )X  ");
                SQL.AppendLine("        Group By X.AssetCode  ");
                SQL.AppendLine("    )Y2 On Y.AssetCode = Y2.AssetCode ");
                SQL.AppendLine("    Inner Join tblAsset Y3 On Y.AssetCode = Y3.AssetCode ");
                SQL.AppendLine("    Group by Y.assetCode, Y3.Assetname, Y3.DisplayName ");
                SQL.AppendLine(")X ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset Code",
                        "Asset Name",
                        "Display Name",
                        "",
                        "Average "+(mMTPMFormula=="0"? "Day":"Hours")+""+Environment.NewLine+"To Failure",
                        //6
                        "Status",
                       
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-6
                        120, 180, 300, 20, 130, 250

                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 5, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "X.AssetCode", "X.AssetName", "X.DisplayName" });
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "X.Status2", true); 

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By X.AssetName ;",
                new string[]
                    {
                        //0
                        "AssetCode",

                        //1-4
                        "AssetName", "DisplayName", "AVG", "Status"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetLueStatusCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2  From TblOption Where Optcat = 'MaintenanceStatus' ");
            SQL.AppendLine("Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmTO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmTO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
       
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatusCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

    }
}
