﻿#region Update
/*
    05/07/2021 [WED/PHT] new apps
    09/08/2021 [WED/PHT] bug saat filter Cost Center
    03/02/2022 [IBL/PHT] cost center yg muncul dibuat tidak melihat group setting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJournalInterOfficeDlg : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        private byte mDocType = 0;
        private FrmJournalInterOffice mFrmParent;

        #endregion

        #region Constructor

        public FrmJournalInterOfficeDlg(FrmJournalInterOffice FrmParent, byte DocType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-4
                        "Cost Center Code", 
                        "Cost Center",
                        "Profit Center",
                        "Department",
                    },
                    new int[]
                    {
                        //0
                        60,

                        //1-4
                        120, 200, 200, 200
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, A.CCName, B.ProfitCenterName, C.DeptName ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.CCCode, A.CCName, A.ProfitCenterCode, A.DeptCode ");
            SQL.AppendLine("    From TblCostCenter A ");
            //SQL.AppendLine("    Inner Join TblGroupProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            //SQL.AppendLine("    Inner Join TblUser C On B.GrpCode=C.GrpCode And C.UserCode=@UserCode ");
            SQL.AppendLine("    Where A.NotParentInd='Y' ");
            SQL.AppendLine("    And A.ActInd='Y' ");
            SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblDepartment C ON A.DeptCode = C.DeptCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "A.CCCode", "A.CCName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CCName;",
                    new string[] { "CCCode", "CCName", "ProfitCenterName", "DeptName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if (mDocType == 1)
                {
                    mFrmParent.TxtCCCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtCCName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                }
                else
                {
                    mFrmParent.TxtCCCode2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtCCName2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                }
                this.Close();
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion
    }
}