﻿namespace RunSystem
{
    partial class FrmRptGiroMovement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkVdCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkBankCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkGiroNo = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGiroNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtGiroNo);
            this.panel2.Controls.Add(this.ChkGiroNo);
            this.panel2.Controls.Add(this.ChkBankCode);
            this.panel2.Controls.Add(this.LueBankCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ChkCtCode);
            this.panel2.Controls.Add(this.LueCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkVdCode);
            this.panel2.Controls.Add(this.LueVdCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(772, 98);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 375);
            this.Grd1.TabIndex = 20;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 98);
            this.panel3.Size = new System.Drawing.Size(772, 375);
            // 
            // ChkVdCode
            // 
            this.ChkVdCode.Location = new System.Drawing.Point(365, 6);
            this.ChkVdCode.Name = "ChkVdCode";
            this.ChkVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVdCode.Properties.Appearance.Options.UseFont = true;
            this.ChkVdCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVdCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVdCode.Properties.Caption = " ";
            this.ChkVdCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVdCode.Size = new System.Drawing.Size(19, 22);
            this.ChkVdCode.TabIndex = 10;
            this.ChkVdCode.ToolTip = "Remove filter";
            this.ChkVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVdCode.ToolTipTitle = "Run System";
            this.ChkVdCode.CheckedChanged += new System.EventHandler(this.ChkVdCode_CheckedChanged);
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(65, 6);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 300;
            this.LueVdCode.Size = new System.Drawing.Size(295, 20);
            this.LueVdCode.TabIndex = 9;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(16, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Vendor";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCtCode
            // 
            this.ChkCtCode.Location = new System.Drawing.Point(365, 27);
            this.ChkCtCode.Name = "ChkCtCode";
            this.ChkCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCtCode.Properties.Caption = " ";
            this.ChkCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkCtCode.TabIndex = 13;
            this.ChkCtCode.ToolTip = "Remove filter";
            this.ChkCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCtCode.ToolTipTitle = "Run System";
            this.ChkCtCode.CheckedChanged += new System.EventHandler(this.ChkCtCode_CheckedChanged);
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(65, 27);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(295, 20);
            this.LueCtCode.TabIndex = 12;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Customer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkBankCode
            // 
            this.ChkBankCode.Location = new System.Drawing.Point(365, 47);
            this.ChkBankCode.Name = "ChkBankCode";
            this.ChkBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBankCode.Properties.Appearance.Options.UseFont = true;
            this.ChkBankCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBankCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBankCode.Properties.Caption = " ";
            this.ChkBankCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBankCode.Size = new System.Drawing.Size(19, 22);
            this.ChkBankCode.TabIndex = 16;
            this.ChkBankCode.ToolTip = "Remove filter";
            this.ChkBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBankCode.ToolTipTitle = "Run System";
            this.ChkBankCode.CheckedChanged += new System.EventHandler(this.ChkBankCode_CheckedChanged);
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(65, 48);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(295, 20);
            this.LueBankCode.TabIndex = 15;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(30, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Bank";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(65, 69);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 30;
            this.TxtGiroNo.Size = new System.Drawing.Size(295, 20);
            this.TxtGiroNo.TabIndex = 18;
            this.TxtGiroNo.Validated += new System.EventHandler(this.TxtGiroNo_Validated);
            // 
            // ChkGiroNo
            // 
            this.ChkGiroNo.Location = new System.Drawing.Point(365, 68);
            this.ChkGiroNo.Name = "ChkGiroNo";
            this.ChkGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkGiroNo.Properties.Appearance.Options.UseFont = true;
            this.ChkGiroNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkGiroNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkGiroNo.Properties.Caption = " ";
            this.ChkGiroNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkGiroNo.Size = new System.Drawing.Size(19, 22);
            this.ChkGiroNo.TabIndex = 19;
            this.ChkGiroNo.ToolTip = "Remove filter";
            this.ChkGiroNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkGiroNo.ToolTipTitle = "Run System";
            this.ChkGiroNo.CheckedChanged += new System.EventHandler(this.ChkGiroNo_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(26, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Giro#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRptGiroMovement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptGiroMovement";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGiroNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkVdCode;
        private DevExpress.XtraEditors.LookUpEdit LueVdCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkCtCode;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkBankCode;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private DevExpress.XtraEditors.CheckEdit ChkGiroNo;
    }
}