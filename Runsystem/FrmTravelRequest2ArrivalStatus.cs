﻿#region update
/*
    27/10/2020 [WED/PHT] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTravelRequest2ArrivalStatus : RunSystem.FrmBase5
    {
        #region Field

        internal string 
            mMenuCode = string.Empty,
            mSQL = string.Empty,
            mDocType = string.Empty;
        
        #endregion

        #region Constructor

        public FrmTravelRequest2ArrivalStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -60);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.SiteName Destination, A.PICCode, C.EmpName ");
            SQL.AppendLine("From TblTravelRequestHdr A ");
            SQL.AppendLine("Inner Join TblSite B On A.SiteCode2 = B.SiteCode ");
            SQL.AppendLine("Inner Join TblEmployee C On A.PICCode = C.EmpCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.SiteCode2 Is Not Null ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status In ('O', 'A') ");
            SQL.AppendLine("And A.WebInd = 'D' ");
            SQL.AppendLine("And IfNull(A.ArrivalStatus, 'O') = 'O' ");
            SQL.AppendLine("And A.SiteCode2 In "); // site destination harus sama dengan site dari master employee sang pengakses menu ini
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select SiteCode ");
            SQL.AppendLine("    From TblEmployee ");
            SQL.AppendLine("    Where UserCode = @UserCode ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Arrive",

                    //1-5
                    "Cancel",
                    "Document#", 
                    "",
                    "Date",
                    "Destination",

                    //6-7
                    "PIC Code",
                    "PIC"
                },
                new int[] 
                {
                    //0
                    60,

                    //1-5
                    60, 180, 20, 80, 130, 
                    
                    //6-7
                    100, 180
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 0, 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 4, 5, 6, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc; ",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "Destination", "PICCode", "EmpName" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = ChkAutoChoose.Checked;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    }, true, false, true, false
                );
                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No ||
                    IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                for (int r = 0; r < Grd1.Rows.Count; ++r)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0))
                    {
                        cml.Add(UpdateArrivalStatus(r, true));
                    }

                    if (Sm.GetGrdBool(Grd1, r, 1))
                    {
                        cml.Add(UpdateArrivalStatus(r, false));
                    }
                }

                Sm.ExecCommands(cml);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                IsProcessedDataNotExists()
                ;
        }

        private bool IsProcessedDataNotExists()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0) || Sm.GetGrdBool(Grd1, Row, 1)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
            return true;
        }

        private MySqlCommand UpdateArrivalStatus(int Row, bool IsArrive)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTravelRequestHdr Set ArrivalStatus = @ArrivalStatus, ");
            SQL.AppendLine("ArrivalChecker = @CreateBy, ArrivalDtTm = Concat(CurrentDateTime(), '00') ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ArrivalStatus", IsArrive ? "A" : "C");
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                var f = new FrmTravelRequest2(mMenuCode);
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                var f = new FrmTravelRequest2(mMenuCode);
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if ((e.ColIndex == 0 || e.ColIndex == 1))
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex))
                    Grd1.Cells[e.RowIndex, (e.ColIndex == 0) ? 1 : 0].Value = false;
            }
        }

        #endregion

        #region Additional Method


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkAutoChoose_CheckedChanged(object sender, EventArgs e)
        {
            for (int row = 0; row < Grd1.Rows.Count; row++)
                if (Sm.GetGrdStr(Grd1, row, 2).Length != 0)
                    Grd1.Cells[row, 0].Value = ChkAutoChoose.Checked;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion
    }
}
