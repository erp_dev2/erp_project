﻿#region Update
/*
    11/04/2022 [TKG/IOK] merubah proses save data     
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRawMaterialOpname : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRawMaterialOpnameFind FrmFind;

        #endregion

        #region Constructor

        public FrmRawMaterialOpname(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Opname Bahan Baku";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                SetLueDocType(ref LueDocType);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "DNo",

                        //1-4
                        "Rak",
                        "Jumlah",
                        "Gambar",
                        "Keterangan"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-4
                        80, 80, 300, 300
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 1);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            SetGrdCanvas();
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueQueueNo, TxtLegalDocVerifyDocNo,
                        TxtVdCode, TxtTTCode, LueDocType, TxtCreateBy, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueQueueNo, LueDocType, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 4 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 4 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueQueueNo, TxtLegalDocVerifyDocNo, TxtVdCode, 
                TxtTTCode, LueDocType, TxtCreateBy, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 1);
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRawMaterialOpnameFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);

                SetLueQueueNo(ref LueQueueNo, "");
                SetUserName();
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RawMaterialOpname", "TblRawMaterialOpnameHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRawMaterialOpnameHdr(DocNo));
            cml.Add(SaveRawMaterialOpnameDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
            //        cml.Add(SaveRawMaterialOpnameDtl(DocNo, Row));
           
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Tanggal") ||
                Sm.IsLueEmpty(LueQueueNo, "Nomor antrian") ||
                Sm.IsLueEmpty(LueDocType, "Tipe dokumen") ||
                IsDocTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsLegalDocVerifyDocNoCancelledAlready() ||
                IsLegalDocVerifyDocNoAlreadyFulfilled();
        }

        private bool IsDocTypeNotValid()
        {
            return Sm.StdMsgYN("Question", 
                "Tipe dokumen : " + 
                LueDocType.GetColumnValue("Col2") + Environment.NewLine +
                "Apakah tipe dokumen sudah benar ?"
                ) == DialogResult.No; 
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Data jumlah bahan baku masih kosong.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Rak masih kosong.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Jumlah bahan baku tidak boleh 0.") ||
                    IsBinNotValid(Row)
                    )
                    return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data jumlah bahan baku (" + (Grd1.Rows.Count - 1).ToString() + ") melebihi jumlah maksimal yang ditentukan (999).");
                return true;
            }
            return false;
        }

        private bool IsLegalDocVerifyDocNoCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblLegalDocVerifyHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtLegalDocVerifyDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Nomor verifikasi legalitas dokumen telah dibatalkan.");
                return true;
            }

            return false;
        }

        private bool IsLegalDocVerifyDocNoAlreadyFulfilled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblLegalDocVerifyHdr " +
                    "Where (IfNull(ProcessInd1, 'O')='F' Or IfNull(ProcessInd2, 'O')='F') " +
                    "And CancelInd='N' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtLegalDocVerifyDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Dokumen legalitas telah selesai diproses.");
                return true;
            }

            return false;
        }

        private bool IsBinNotValid(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Bin From TblBin ");
            SQL.AppendLine("Where Bin=@Bin And ActInd='Y';");

            var cm = new MySqlCommand()
            { 
                CommandText = 
                    "Select Bin From TblBin " +
                    "Where Bin=@Bin And ActInd='Y';" 
            };
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 1));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Rak " + Sm.GetGrdStr(Grd1, Row, 1) + " tidak dapat digunakan.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRawMaterialOpnameHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Raw Material Opname (Hdr) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblRawMaterialOpnameHdr(DocNo, DocDt, DocType, CancelInd, ProcessInd, LegalDocVerifyDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, 'N', 'O', @LegalDocVerifyDocNo, @Remark, @UserCode, @Dt); ");
            
            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    ProcessInd2='P', LastUpBy=@UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@LegalDocVerifyDocNo; ");

            var cm = new MySqlCommand() 
            { 
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRawMaterialOpnameDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Raw Material Opname (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblRawMaterialOpnameDtl(DocNo, DNo, Shelf, Qty, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @Shelf_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Shelf_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveRawMaterialOpnameDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblRawMaterialOpnameDtl(DocNo, DNo, Shelf, Qty, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @Shelf, @Qty, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Shelf", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRawMaterialOpnameHdr());
            cml.Add(SaveRawMaterialOpnameDtl(TxtDocNo.Text));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
            //        cml.Add(SaveRawMaterialOpnameDtl(TxtDocNo.Text, Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document number", false) ||
                IsDataCancelledAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsLegalDocVerifyDocNoCancelledAlready() ||
                IsLegalDocVerifyDocNoAlreadyFulfilled()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =  
                    "Select 1 From TblRawMaterialOpnameHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data ini sudah dibatalkan.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditRawMaterialOpnameHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Update TblRawMaterialOpnameHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Delete From TblRawMaterialOpnameDtl Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    ProcessInd2= ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select 1 From TblRawmaterialOpnameHdr ");
            SQL.AppendLine("            Where CancelInd='N' ");
            SQL.AppendLine("            And LegalDocVerifyDocNo=@LegalDocVerifyDocNo Limit 1 ");
            SQL.AppendLine("        ) Then 'P' Else 'O' End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@LegalDocVerifyDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRawMaterialOpnameHdr(DocNo);
                ShowRawMaterialOpnameDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRawMaterialOpnameHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocType, A.DocDt, A.CancelInd, B.QueueNo, A.LegalDocVerifyDocNo, C.VdName, ");
            SQL.AppendLine("E.TTName, F.UserName, A.Remark ");
            SQL.AppendLine("From TblRawMaterialOpnameHdr A ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblLoadingQueue D On B.QueueNo=D.DocNo ");
            SQL.AppendLine("Left Join TblTransportType E On D.TTCode=E.TTCode ");
            SQL.AppendLine("Left Join TblUser F On A.CreateBy=F.UserCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "QueueNo", "LegalDocVerifyDocNo", "VdName", 

                        //6-9
                        "TTName", "DocType", "UserName", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        SetLueQueueNo(ref LueQueueNo, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueQueueNo, Sm.DrStr(dr, c[4]));
                        TxtLegalDocVerifyDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtTTCode.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueDocType, Sm.DrStr(dr, c[7]));
                        TxtCreateBy.EditValue = Sm.DrStr(dr, c[8]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    }, true
                );
        }

        private void ShowRawMaterialOpnameDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, 
                "Select DNo, Shelf, Qty, Remark From TblRawMaterialOpnameDtl Where DocNo=@DocNo Order By DNo;",
                new string[]{ "DNo", "Shelf", "Qty", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2 ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueQueueNo(ref DXE.LookUpEdit Lue, string DocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct Col1, Col2 From ( ");
                SQL.AppendLine("    Select DocNo As Col1, QueueNo As Col2 ");
                SQL.AppendLine("    From TblLegalDocVerifyHdr ");
                SQL.AppendLine("    Where CancelInd='N' ");
                SQL.AppendLine("    And IfNull(ProcessInd2, 'O')<>'F' ");
                if (DocNo.Length != 0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select DocNo As Col1, QueueNo As Col2 ");
                    SQL.AppendLine("    From TblLegalDocVerifyHdr ");
                    SQL.AppendLine("    Where DocNo='" + DocNo + "' ");
                }
                SQL.AppendLine(") Tbl Order By Col2");

                Sm.SetLue2(
                    ref Lue,
                    SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowLegalDocVerifyInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, B.VdName, D.TTName ");
            SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("Left Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join TblLoadingQueue C On A.QueueNo=C.DocNo ");
            SQL.AppendLine("Left Join TblTransportType D On C.TTCode=D.TTCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            try
            {
                Sm.CmParam<String>(ref cm, "@DocNo", LueQueueNo.GetColumnValue("Col1").ToString());

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] { "DocNo", "VdName", "TTName" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtLegalDocVerifyDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtVdCode.EditValue = Sm.DrStr(dr, c[1]);
                            TxtTTCode.EditValue = Sm.DrStr(dr, c[2]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrdCanvas()
        {
            iGCellStyle CanvasStyle = new iGCellStyle();
            CanvasStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
            CanvasStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)(TenTec.Windows.iGridLib.iGCellFlags.DisplayImage));

            Grd1.Cols[3].CellStyle =CanvasStyle;
        }

        private void SetUserName()
        {
            var cm = new MySqlCommand(){ CommandText = "Select UserName From TblUser Where UserCode=@UserCode;" };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            TxtCreateBy.EditValue = Sm.GetValue(cm);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 2, 4 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3 });
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            
            if (e.KeyCode == Keys.Enter && Grd1.CurRow.Index != Grd1.Rows.Count - 1)
            {
                if (Grd1.CurCell.Col.Index == 1)
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index, 2);
                else
                {
                    if (Grd1.CurCell.Col.Index == 2)
                        Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, 4);
                    else
                    {
                        if (Grd1.CurCell.Col.Index == 4)
                            Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, 1);
                    }
                }
            }

            if (e.KeyCode == Keys.Tab && Grd1.CurCell != null && Grd1.CurCell.RowIndex == Grd1.Rows.Count - 1)
            {
                int LastVisibleCol = Grd1.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd1.Cols.Count - 1; Col++)
                    if (Grd1.Cols[Col].Visible) LastVisibleCol = Col;

                if (Grd1.CurCell.Col.Order == LastVisibleCol)
                {
                    if (BtnFind.Enabled)
                        BtnFind.Focus();
                    else
                        BtnSave.Focus();
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1, 4 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2 }, e);
            if (e.ColIndex == 2) Grd1.Cells[e.RowIndex, 3].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 2);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueQueueNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueQueueNo, new Sm.RefreshLue2(SetLueQueueNo), "");

            TxtLegalDocVerifyDocNo.EditValue = null;
            TxtVdCode.EditValue = null;
            TxtTTCode.EditValue = null;

            if (Sm.GetLue(LueQueueNo).Length != 0)
                ShowLegalDocVerifyInfo();
        }


        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
        }

        #endregion

        #region Grid Event

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex != 3) return;

            try
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;

                if (myObjValue == null) return;

                Pen RedPen = new Pen(Color.Red, 2);
                Rectangle myBounds = e.Bounds;

                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;

                int X1 = myBounds.X, Y1 = myBounds.Y;
                Int32 myValue = Sm.GetGrdInt(Grd1, e.RowIndex, e.ColIndex);
                Int32 DivValue = (Int32)(myValue / 5);
                Int32 RemainingValue = myValue % 5;

                for (int intX = 0; intX < DivValue; intX++)
                {
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);
                    e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);
                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);
                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);

                    X1 = X1 + myBounds.Height + 5;
                }

                if (RemainingValue >= 1)
                {
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);
                    if (RemainingValue >= 2)
                    {
                        e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);
                        if (RemainingValue >= 3)
                        {
                            e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);
                            if (RemainingValue >= 4)
                            {
                                e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);
                                if (RemainingValue >= 5)
                                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);
                            }
                        }
                    }
                }

                int TotalWidth = 0;
                for (int intX = 0; intX <= e.ColIndex; intX++)
                    TotalWidth = TotalWidth + Grd1.Cols[intX].Width;

                if (X1 > TotalWidth)
                    Grd1.Cols[e.ColIndex].Width = Grd1.Cols[e.ColIndex].Width + (X1 - TotalWidth) + myBounds.Height;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion
    }
}
