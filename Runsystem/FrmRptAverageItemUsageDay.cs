﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAverageItemUsageDay : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private string val = string.Empty;
        internal bool mIsShowForeignName = false;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptAverageItemUsageDay(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                SetLueOptCode(ref LueOptCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }


        private string SetSQL(bool IsFilterByDocDt)
        {

            var SQL = new StringBuilder();

            if (Sm.GetLue(LueOptCode) == "1")
            {
                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("Select A.WhsCode As Option, C.WhsName, B.ItCode,  D.ItName, B.Lot, B.Bin, ");
                SQL.AppendLine("	SUM(B.Qty) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty, ");
                SQL.AppendLine("	SUM(B.Qty2) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty2, ");
                SQL.AppendLine("	SUM(B.Qty3) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty3, ");
                SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, D.ForeignName ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt1 And @DocDt2  ");
                SQL.AppendLine("Group By  A.WhsCode, B.ItCode, D.InventoryUomCode, B.Lot, B.Bin  ");
                SQL.AppendLine(") Y1  ");

               
            }
            else if (Sm.GetLue(LueOptCode) == "2")
            {
                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("Select E.CCName As Option, C.WhsName, B.ItCode,  D.ItName, B.Lot, B.Bin, ");
                SQL.AppendLine("	SUM(B.Qty) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty, ");
                SQL.AppendLine("	SUM(B.Qty2) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty2, ");
                SQL.AppendLine("	SUM(B.Qty3) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty3, ");
                SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, D.ForeignName ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblCostCenter E On A.CCCOde = E.CCCode ");
                SQL.AppendLine("Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("Group By A.CCCode, B.ItCode, D.InventoryUomCode, B.Lot, B.Bin  ");
                SQL.AppendLine(") Y1  ");
            }

            else if (Sm.GetLue(LueOptCode) == "3")
            {
                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("Select E.EmpName As Option, C.WhsName, B.ItCode,  D.ItName, B.Lot, B.Bin,  ");
                SQL.AppendLine("	SUM(B.Qty) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty, ");
                SQL.AppendLine("	SUM(B.Qty2) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty2, ");
                SQL.AppendLine("	SUM(B.Qty3) / ((dateDiff(DATE_FORMAT(@DocDt2, '%Y-%m-%d'), DATE_FORMAT(@DocDt1, '%Y-%m-%d')))+1) As Qty3, ");
                SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, D.ForeignName ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Inner Join TblEmployee E On A.EmpCode = E.EmpCode ");
                SQL.AppendLine("Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("Group By  A.EmpCode, B.ItCode, D.InventoryUomCode, B.Lot, B.Bin  ");
                SQL.AppendLine(") Y1  ");
            }
            else
            {
                SQL.AppendLine(""); 
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            GetValueFromlue();
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5  
                        ""+val+"",
                        "Warehouse",
                        "Item Code",
                        "Item Name",
                        "Lot",                        

                        //6-10  
                        "Bin",
                        "Average Usage"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Average Usage"+Environment.NewLine+"Quantity 2",
                        "Uom 2",
                       
                        //11-13
                        "Average Usage"+Environment.NewLine+"Quantity 3",
                        "Uom 3",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 150, 150, 250, 60,  
                        
                        //6-10
                        60, 100, 60, 100, 60, 
                        
                        //11-13
                        100, 60, 180
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 11 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 9, 10, 11, 12 }, false);
                Grd1.Cols[13].Move(5);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 9, 10, 11, 12, 13 }, false);
            }
            ShowInventoryUomCode();
            if (Sm.GetLue(LueOptCode) == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            }
            else if (Sm.GetLue(LueOptCode) == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1 }, true);
            }
            else if (Sm.GetLue(LueOptCode) == "3")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1 }, true);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12 }, true);
        }


        override protected void ShowData()
        {
            try
            {
                if (Sm.IsDteEmpty(DteDocDt1, "Date ") || Sm.IsDteEmpty(DteDocDt2, "Document Date") ||Sm.IsLueEmpty(LueOptCode, "Monitoring Option")) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                if (ChkDocDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                }
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName", "ForeignName" });

                SetGrd();
                var a = SetSQL(ChkDocDt.Checked) + Filter;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(ChkDocDt.Checked) + Filter,
                        new string[]
                        {
                            //0
                            "Option",   
                            
                            //1-5
                            "WhsName", "ItCode", "ItName", "Lot", "Bin",   
                            
                            //6-10
                            "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3",  
                            
                            //11-12
                            "InventoryUomCode3","ForeignName"
                 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                        }, true, false, false, false
                    );
               // Grd1.Rows.CollapseAll();
              
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        private string FilterByDocDt(bool IsFilterByDocDt, string TableAlias)
        {
            return (IsFilterByDocDt) ? " And " + TableAlias + ".DocDt Between @DocDt1 And @DocDt2 " : "";
        }

        private void GetValueFromlue()
        {
            if (Sm.GetLue(LueOptCode) == "1")
            {
                val = "Item";
            }
            else if (Sm.GetLue(LueOptCode) == "2")
            {
                val = "CostCenter";
            }
            else if (Sm.GetLue(LueOptCode) == "3")
            {
                val = "Requested By";
            }
            else
            {
                val = string.Empty;
            }
        }

        private void SetLueOptCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Col1, Col2 From (");
                SQL.AppendLine("    Select '1' As Col1, 'Item' As Col2 Union All");
                SQL.AppendLine("    Select '2' As Col1, 'CostCenter' As Col2 Union All");
                SQL.AppendLine("    Select '3' As Col1, 'Requested By' As Col2 ");
                SQL.AppendLine(") T Order By Col1");

                Sm.SetLue2(
                    ref Lue,
                    SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        #endregion

        #region Event
        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkOptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Monitoring Option");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueOptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptCode, new Sm.RefreshLue1(SetLueOptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
