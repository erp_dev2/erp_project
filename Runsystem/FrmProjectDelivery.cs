﻿#region Update
/*
    24/09/2019 [WED/IMS] new apps for IMS
    26/09/2019 [WED/IMS] journal masih ada yang salah
    28/10/2019 [WED/YK] DocType 1 untuk Project System standard
    30/10/2019 [WED/YK] bug salah nama tabel saat save
    04/12/2019 [WED/VIR] COA journal ambil dari SOContractHdr.ProjectCode berdasarkan parameter ProjectAcNoFormula
    10/12/2019 [DITA/YK] Feedback tambah wbs 2
    23/12/2019 [WED/YK] approval
    06/05/2020 [WED/YK] cancel PD belom meng unsettled PRJI
    26/08/2020 [WED/YK] amount journal ambil dari yg tidak kena pajak (Estimated Amount PRJI)
    20/09/2021 [DITA/ALL]  validasi setting journal coa
    21/12/2021 [BRI/YK] Membuat WBS2 supaya bisa ditarik ke Project Delivery berdasarkan param IsWBS2SameAsWBS
    22/12/2021 [ISD/YK] tambah field approval's remark based on param IsPSModuleShowApproverRemarkInfo
    22/03/2022 [VIN/YK] BUG show hdr belum di group by
    28/03/2022 [TRI/YK] Bug detail, kolom estimated price belum berdasar param IsWBS2SameAsWBS
    19/07/2022 [TYO/PRODUCT] Menambah Tab Control Approval Information, merubah source Grd1 ke Grd2
    20/09/2022 [HPH/VIR] Mengubah source settled date jadi mengambil dari document date project delivery
    27/09/2022 [MAU/VIR] Menonaktifkan jurnal Project delivery WBS 2
    30/09/2022 [BRI/VIR] bug settled date
    26/10/2022 [VIN/VIR] bug tampilan detail
    19/12/2022 [RDA/MNET] tambah tab upload file berdasarkan param IsProjectDeliveryAllowToUploadFile
    22/12/2022 [RDA/MNET] benerin validasi pas cancel data
    29/12/2022 [DITA/MNET] saat edit untuk upload file masih save journal, harusnya tidak
    02/02/2023 [TYO/MNET] set upload file mandatory berdasarkan parameter IsProjectDeliveryUploadFileMandatory
    09/02/2023 [BRI/MNET] tambah source CC JT berdasarkan param IsProjectDeliveryJournalUseCostCenter
    10/02/2023 [DITA/MNET] penyesuaian update settled prji based on param = IsWBS2SummarizedWBS1
    16/02/2023 [DITA/MNET] saat settledtask2 estimated cost belum dikali 0.01
    21/02/2023 [DITA/MNET] Penyesuaian menu Project Delivery ( logic menu+ jurnal yang terbentuk saat save+jurnal setelah approval)
    24/02/2023 [DITA/MNET] ada bug saaat settled task2
    27/02/2023 [DITA/MNET] bug: saat edit, settled weight jadi keisi semua
    02/03/2023 [VIN/VIR] bug : show upload file hanya jika IsProjectDeliveryUploadFileMandatory
    03/03/2023 [HAR/GSS] bug : show data general gak muncul (ShowProjectDeliveryDtl)
    04/03/2023 [WED/MNET] upload file tidak muncul ketika parameter IsProjectDeliveryUploadFileMandatory value nya N
    26/04/2023 [BRI/YK] bug : show data general gak muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmProjectDelivery : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string mProjectDeliveryDocType = "1", mProjectAcNoFormula = string.Empty;
        private bool mIsAutoJournalActived = false, mIsCheckCOAJournalNotExists = false, mIsPSModuleShowApproverRemarkInfo = false;
        internal bool mIsFilterBySite = false, mIsPRJIUseWBS2 = false, mIsWBS2SameAsWBS = false, mIsWBS2SummarizedWBS1 = false;
        internal FrmProjectDeliveryFind FrmFind;

        private bool
            mIsProjectDeliveryAllowToUploadFile = false,
            mIsProjectDeliveryUploadFileMandatory = false,
            mIsProjectDeliveryJournalUseCostCenter = false;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mDocTitle = string.Empty,
            mProjectDeliveryJournalFormula = string.Empty;
        private string mStateIndicator = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmProjectDelivery(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectDelivery");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();
                SetGrd();

                if (!mIsProjectDeliveryAllowToUploadFile)
                {
                    tabControl1.TabPages.Remove(TpUploadFile);
                }

                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                if (!mIsPSModuleShowApproverRemarkInfo)
                {
                    int ypoint = 21;
                    label26.Visible = MeeApprovalRemark.Visible = false;
                    label13.Top = label13.Top - ypoint; MeeCancelReason.Top = MeeCancelReason.Top - ypoint; ChkCancelInd.Top = ChkCancelInd.Top - ypoint;
                    label12.Top = label12.Top - ypoint; MeeRemark.Top = MeeRemark.Top - ypoint;
                    //Grd1.Height = Grd1.Height - ypoint;
                    Grd2.Height = Grd2.Height - ypoint;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        
        // digantikan dengan Grd2
        private void SetGrd()
        {
            #region Grd1
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "PRJIDNo",
                    "Stage",
                    "Task",
                    "Bobot (%)",
                    "Remark",
                    
                    //6-10
                    "SO Contract",
                    "SO Revision" + Environment.NewLine + "(Existing)",
                    "SO Revision" + Environment.NewLine + "(Latest)",
                    "Estimated Price",
                    "Type"
                },
                new int[] 
                {
                    //0
                    20,
                    //1-5
                    0, 180, 180, 120, 200, 
                    //6-10
                    150, 150, 150, 150, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
            #endregion

            #region Grd2
            Grd2.Cols.Count = 13;
            Grd2.FrozenArea.ColCount = 4;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "PRJIDNo",
                    "Stage",
                    "Task",
                    "Bobot (%)",
                    "Remark",
                    
                    //6-10
                    "SO Contract",
                    "SO Revision" + Environment.NewLine + "(Existing)",
                    "SO Revision" + Environment.NewLine + "(Latest)",
                    "Estimated Price",
                    "Type",

                    //11-12
                    "SettledRevAmt",
                    "EstimatedCost"

                },
                new int[]
                {
                    //0
                    20,
                    //1-5
                    0, 180, 180, 120, 200, 
                    //6-10
                    150, 150, 150, 150, 100,
                    //11-1
                    0, 0
                }
            );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdFormatDec(Grd2, new int[] { 4, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12  });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 11, 12 });
            Sm.SetGrdProperty(Grd2, false);

            #endregion

            #region Grid3
            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 2, 3, 4 });

            #endregion

            #region Grid4

            if (mIsProjectDeliveryAllowToUploadFile)
            {
                Grd4.Cols.Count = 7;
                Grd4.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd4,
                        new string[]
                        {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                        },
                         new int[]
                        {
                        0,
                        250, 20, 20, 100, 100,
                        100
                        }
                    );

                Sm.GrdColInvisible(Grd4, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd4, new int[] { 5 });
                Sm.GrdFormatTime(Grd4, new int[] { 6 });
                Sm.GrdColButton(Grd4, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                Grd4.Cols[2].Move(1);
            }

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, MeeCancelReason, DteDocDt, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2, TxtAmt,
                        TxtPRJIDocNo, MeeApprovalRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnPRJIDocNo.Enabled = false;
                    if (mIsProjectDeliveryAllowToUploadFile)
                    {
                        Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                            {
                                Grd4.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd4.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd4.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    } 
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    //Grd1.ReadOnly = true;
                    Grd2.ReadOnly = false;
                    BtnPRJIDocNo.Enabled = true;
                    if (mIsProjectDeliveryAllowToUploadFile) Sm.GrdColReadOnly(false, true, Grd4, new int[] { 2, 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, MeeCancelReason, DteDocDt, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2,
                 TxtPRJIDocNo, TxtStatus, MeeApprovalRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            //Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4, 9 });
            Sm.FocusGrd(Grd2, 0, 0);
            Grd3.Rows.Clear();
            if (mIsProjectDeliveryAllowToUploadFile) Sm.ClearGrd(Grd4, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectDeliveryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateIndicator = "I";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);

            mStateIndicator = "E";

            if (TxtStatus.Text != "Approved")
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                    {
                        Grd4.Cells[Row, 2].ReadOnly = iGBool.False;
                        Grd4.Cells[Row, 3].ReadOnly = iGBool.True;
                    }
                    else
                    {
                        Grd4.Cells[Row, 2].ReadOnly = iGBool.True;
                        Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                    }
                }
            }
            else
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                }
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                ComputeAmt();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                ComputeAmt();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) && e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectDeliveryDlg2(this, TxtPRJIDocNo.Text));
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) && e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectDeliveryDlg2(this, TxtPRJIDocNo.Text));
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) && e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmProjectDeliveryDlg2(this, TxtPRJIDocNo.Text));
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) && e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmProjectDeliveryDlg2(this, TxtPRJIDocNo.Text));
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            UpdateLatestSOContractRevision();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectDelivery", "TblProjectDeliveryHdr");
            decimal mTotalBobot = 0m, mTotalBobot2 = 0m, mSettledRevAmt=0m, mEstimatedCost=0m;
            string DocDt = Sm.GetDte(DteDocDt);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProjectDeliveryHdr(DocNo));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                {
                    cml.Add(SaveProjectDeliveryDtl(DocNo, Row));
                    if (Sm.GetGrdStr(Grd2, Row, 10) == "WBS1")
                    {
                        cml.Add(SettledTask(
                            DocNo,
                            TxtPRJIDocNo.Text,
                            Sm.GetGrdStr(Grd2, Row, 1),
                            Sm.GetGrdStr(Grd2, Row, 5),
                            "I",
                            DocDt
                        ));
                        mTotalBobot += Sm.GetGrdDec(Grd2, Row, 4);
                    }
                    if (Sm.GetGrdStr(Grd2, Row, 10) == "WBS2")
                    {
                        cml.Add(SettledTask2(
                            DocNo,
                            TxtPRJIDocNo.Text,
                            Sm.GetGrdStr(Grd2, Row, 1),
                            Sm.GetGrdStr(Grd2, Row, 5),
                            "I",
                            DocDt
                        ));
                        mTotalBobot2 += Sm.GetGrdDec(Grd2, Row, 4);
                        mSettledRevAmt += Sm.GetGrdDec(Grd2, Row, 11);
                        mEstimatedCost += Sm.GetGrdDec(Grd2, Row, 12);
                    }
                }
            }

            if(!mIsWBS2SummarizedWBS1)cml.Add(UpdateAchievement(DocNo, TxtPRJIDocNo.Text, mTotalBobot, "I"));
            cml.Add(UpdateAchievement2(DocNo, TxtPRJIDocNo.Text, mTotalBobot2, "I", mSettledRevAmt, mEstimatedCost));

            if (((!mIsWBS2SameAsWBS && !mIsWBS2SummarizedWBS1) && Sm.CompareStr(Sm.GetGrdStr(Grd2, 0, 10), "WBS1")) || ((mIsWBS2SameAsWBS || mIsWBS2SummarizedWBS1) && Sm.CompareStr(Sm.GetGrdStr(Grd2, 0, 10), "WBS2")))
            {
                if (mIsAutoJournalActived && !IsNeedApproval())
                {
                    string JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1)); 
                    cml.Add(SaveJournal(DocNo, mMenuCode, JNDocNo ));
                }
            }
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsProjectDeliveryAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd4, Row, 1))) return;
                    }
                }
            }

            cml.Add(SaveUploadFile(DocNo));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsProjectDeliveryAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1")
                    {
                        //if (Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 4))
                        //{
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd4, Row, 1));
                        //}
                    }
                }
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) ||
                IsProjectAlreadyCancelled() ||
                IsApprovalOnGoing() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid1() ||
                IsGrdValueNotValid2() ||
                ((mIsWBS2SameAsWBS || mIsWBS2SummarizedWBS1) && IsWBSNotValid()) ||
                (mIsProjectDeliveryAllowToUploadFile && IsUploadFileEmpty()) ||
                IsJournalSettingInvalid();
        }

        private bool IsUploadFileEmpty()
        {
            if (mIsProjectDeliveryUploadFileMandatory && Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (Sm.GetParameter("CustomerAcNoNonInvoice").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }

           

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo4")) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string PRJIDNo = string.Empty, ItCtName = string.Empty;
            string AcName = "(Sales)";

            SQL.AppendLine("Select G.ItCtName  ");
            SQL.AppendLine("From TblProjectImplementationHdr A   ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo And A.DocNo = @PRJIDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl D On A.DocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl E On C.DocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory G On F.ItCtCode = G.ItCtCode ");

            SQL.AppendLine("Where G." + COA + " Is Null ");
            SQL.AppendLine("And D.DNo In (");
            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                PRJIDNo = Sm.GetGrdStr(Grd2, r, 1);
                if (PRJIDNo.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@PRJIDNo_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@PRJIDNo_" + r.ToString(), PRJIDNo);
                    r++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + AcName + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsApprovalOnGoing()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo ");
            SQL.AppendLine("From TblProjectDeliveryHdr A ");
            SQL.AppendLine("Inner Join TblDocApproval B On A.DocNo = B.DocNo And A.Status = 'O' And A.CancelInd = 'N' "); // status document masih O, tapi ada level yang sudah approved
            SQL.AppendLine("Where A.PRJIDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't create this delivery. Another delivery is still on approval process. Make sure this document#" + Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text) + " is approved by all level, or cancelled via document approval.");
                return true;
            }

            return false;
        }

        private bool IsSOContractRevisionOutdated()
        {
            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, i, 7) != Sm.GetGrdStr(Grd2, i, 8))
                    {
                        Sm.StdMsg(mMsgType.Warning, "This document needs revision, due to outdated SO Contract Revision.");
                        Sm.FocusGrd(Grd2, i, 1);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsProjectAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C'); ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This project is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid1()
        {
            string mPRJIDNo = string.Empty, mStageName = string.Empty, mTaskName = string.Empty;
            bool mIsSettled = false;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 10) == "WBS1") 
                {
                    if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                    mPRJIDNo += Sm.GetGrdStr(Grd2, Row, 1);
                }
            }

            if (mPRJIDNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select B.StageName, C.TaskName ");
                SQL.AppendLine("From TblProjectImplementationDtl A ");
                SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode ");
                SQL.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");
                SQL.AppendLine("And Find_In_Set(A.DNo, @DNo) ");
                SQL.AppendLine("And A.SettledInd = 'Y' ");
                SQL.AppendLine("Limit 1; ");

                Sm.CmParam<String>(ref cm, "@DocNo", TxtPRJIDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", mPRJIDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "StageName", "TaskName" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mIsSettled = true;
                            mStageName = Sm.DrStr(dr, c[0]);
                            mTaskName = Sm.DrStr(dr, c[1]);
                        }
                    }
                    dr.Close();
                }

                if (mIsSettled)
                {
                    var mMsg = new StringBuilder();

                    mMsg.AppendLine("Stage : " + mStageName);
                    mMsg.AppendLine("Task : " + mTaskName);
                    mMsg.AppendLine("This data is already settled. ");

                    Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            string mPRJIDNo = string.Empty, mStageName = string.Empty, mTaskName = string.Empty;
            bool mIsSettled = false;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 10) == "WBS2") 
                {
                    if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                    mPRJIDNo += Sm.GetGrdStr(Grd2, Row, 1);
                }
            }

            if (mPRJIDNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select B.StageName, C.TaskName ");
                SQL.AppendLine("From TblProjectImplementationDtl5 A ");
                SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode ");
                SQL.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");
                SQL.AppendLine("And Find_In_Set(A.DNo, @DNo) ");
                SQL.AppendLine("And A.SettledInd = 'Y' ");
                SQL.AppendLine("Limit 1; ");

                Sm.CmParam<String>(ref cm, "@DocNo", TxtPRJIDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", mPRJIDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "StageName", "TaskName" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mIsSettled = true;
                            mStageName = Sm.DrStr(dr, c[0]);
                            mTaskName = Sm.DrStr(dr, c[1]);
                        }
                    }
                    dr.Close();
                }

                if (mIsSettled)
                {
                    var mMsg = new StringBuilder();

                    mMsg.AppendLine("Stage : " + mStageName);
                    mMsg.AppendLine("Task : " + mTaskName);
                    mMsg.AppendLine("This data is already settled. ");

                    Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                    return true;
                }
            }

            return false;
        }

        private bool IsWBSNotValid()
        {
            string mTypeCode = string.Empty;
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (mTypeCode.Length > 0) mTypeCode += ",";
                mTypeCode += Sm.GetGrdStr(Grd2, Row, 10);
            }
            if (Sm.Find_In_Set("WBS1", mTypeCode) && Sm.Find_In_Set("WBS2", mTypeCode))
            {
                Sm.StdMsg(mMsgType.Warning, "Type is invalid");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveProjectDeliveryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectDeliveryHdr(DocNo, DocDt, DocType, Status, CancelInd, PRJIDocNo, Remark, Amt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, 'O', 'N', @PRJIDocNo, @Remark, @Amt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ProjectDelivery' ");
            SQL.AppendLine("And T.SiteCode Is Not Null ");
            SQL.AppendLine("And T.SiteCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X5.SiteCode ");
            SQL.AppendLine("    From TblProjectImplementationHdr X1 ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo And X1.DocNo = @PRJIDocNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr X4 On X3.BOQDocNo = X4.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr X5 On X4.LOPDocNo = X5.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblProjectDeliveryHdr Set ");
            SQL.AppendLine("    Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'ProjectDelivery' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mProjectDeliveryDocType);
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectDeliveryDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectDeliveryDtl(DocNo, DNo, PRJIDocNo, PRJIDNo, Remark, Type, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @PRJIDocNo, @PRJIDNo, @Remark, @Type, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtPRJIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PRJIDNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@Type", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateAchievement(string PDDocNo, string DocNo, decimal Bobot, string StateInd)
        {
            var SQL = new StringBuilder();

            if (StateInd == "I")
            {
                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set Achievement = Achievement + @Bobot, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And CancelInd = 'N' And Status = 'A' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'N' And T.Status = 'A') ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set Achievement = 100 ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And Achievement > 99.9; ");
            }
            else
            {
                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set Achievement = Achievement - @Bobot, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And CancelInd = 'N' And Status = 'A' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'Y' And T.Status = 'A') ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set Achievement = 0 ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And Achievement < 0.00; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PDDocNo", PDDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Bobot);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateAchievement2(string PDDocNo, string DocNo, decimal Bobot, string StateInd, decimal SettledRevAmt, decimal EstimatedCost )
        {
            var SQL = new StringBuilder();

            if (StateInd == "I")
            {
                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set ");
                SQL.AppendLine("    TotalAchievement = TotalAchievement + @Bobot, ");
                SQL.AppendLine("    Achievement = Achievement + @Bobot, ");
                SQL.AppendLine("    TotalRev = TotalRev + @SettledRevAmt , ");
                SQL.AppendLine("    TotalEstimatedCost = TotalEstimatedCost + @EstimatedCost , ");
                SQL.AppendLine("    LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And CancelInd = 'N' And Status = 'A' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'N' And T.Status = 'A') ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set TotalAchievement = 100 ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And TotalAchievement > 99.9; ");
            }
            else
            {
                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set ");
                SQL.AppendLine("    TotalAchievement = TotalAchievement - @Bobot, ");
                SQL.AppendLine("    Achievement = Achievement - @Bobot, ");
                SQL.AppendLine("    TotalRev = TotalRev - @SettledRevAmt , ");
                SQL.AppendLine("    TotalEstimatedCost = TotalEstimatedCost - @EstimatedCost , ");
                SQL.AppendLine("    LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And CancelInd = 'N' And Status = 'A' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'Y' And T.Status = 'A') ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblProjectImplementationHdr ");
                SQL.AppendLine("    Set TotalAchievement = 0 ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And TotalAchievement < 0.00; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PDDocNo", PDDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Bobot);
            Sm.CmParam<Decimal>(ref cm, "@SettledRevAmt", SettledRevAmt);
            Sm.CmParam<Decimal>(ref cm, "@EstimatedCost", EstimatedCost);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SettledTask(string PDDocNo, string DocNo, string DNo, string Remark, string StateInd, string DocDt)
        {
            var SQL = new StringBuilder();

            if (StateInd == "I")
            {
                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledInd='Y', ");
                SQL.AppendLine("    SettledAmt = EstimatedAmt, ");
                //SQL.AppendLine("    SettleDt = Replace(CurDate(), '-', ''), ");
                SQL.AppendLine("    SettleDt = Left(@DocDt, 8)");
                SQL.AppendLine(",");
                SQL.AppendLine("    Remark=@Remark, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo And SettledInd='N' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'N' And T.Status = 'A') ");
                SQL.AppendLine("; ");
            }
            else
            {
                SQL.AppendLine("Update TblProjectImplementationDtl Set ");
                SQL.AppendLine("    SettledInd='N', ");
                SQL.AppendLine("    SettledAmt = 0.00, ");
                SQL.AppendLine("    SettleDt=NULL, ");
                SQL.AppendLine("    Remark=NULL, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo And SettledInd='Y' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'Y' And T.Status = 'A') ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PDDocNo", PDDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            return cm;
        }

        private MySqlCommand SettledTask2(string PDDocNo, string DocNo, string DNo, string Remark, string StateInd, string DocDt)
        {
            var SQL = new StringBuilder();

            if (StateInd == "I")
            {
                SQL.AppendLine("Update TblProjectImplementationDtl5 A ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("Set  ");
                SQL.AppendLine("    SettledInd = 'Y', ");
                SQL.AppendLine("    SettledAmt = A.BobotPercentage, ");
                //SQL.AppendLine("    SettleDt = Replace(CurDate(), '-', ''), ");
                SQL.AppendLine("    SettleDt = Left(@DocDt, 8), ");
                SQL.AppendLine("    SettledRevAmt = A.Amt, ");
                SQL.AppendLine("    EstimatedCost = A.BobotPercentage*B.TotalResource*0.01, ");
                SQL.AppendLine("    A.Remark=@Remark, ");
                SQL.AppendLine("    A.LastUpBy=@UserCode, ");
                SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where A.DocNo=@DocNo And DNo = @DNo And SettledInd='N' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'N' And T.Status = 'A') ");
                SQL.AppendLine("; ");

                if(mIsWBS2SummarizedWBS1)
                {
                    //SQL.AppendLine("Update TblProjectImplementationDtl A ");
                    //SQL.AppendLine("Inner Join ");
                    //SQL.AppendLine("( ");
                    //SQL.AppendLine("    Select T1.StageCode, If((T1.Settled / T2.Pembagi) * 100 = 100, 'Y', 'N') SettledInd, SettledWeightPerc, SettledRevAmt, EstimatedCost ");
                    //SQL.AppendLine("    From( ");
                    //SQL.AppendLine("       Select StageCode, Count(*) Settled, Sum(BobotPercentage) SettledWeightPerc, ");
                    //SQL.AppendLine("       Sum(SettledRevAmt) SettledRevAmt, Sum(EstimatedCost) EstimatedCost ");
                    //SQL.AppendLine("       From TblProjectImplementationDtl5 ");
                    //SQL.AppendLine("       Where SettledInd = 'Y' And DocNo=@DocNo  ");
                    //SQL.AppendLine("       Group By StageCode ");
                    //SQL.AppendLine("    ) T1 ");
                    //SQL.AppendLine("    Left Join ");
                    //SQL.AppendLine("   ( ");
                    //SQL.AppendLine("       Select StageCode, Count(*) Pembagi ");
                    //SQL.AppendLine("        From TblProjectImplementationDtl5 ");
                    //SQL.AppendLine("        Where DocNo=@DocNo ");
                    //SQL.AppendLine("        Group By StageCode ");
                    //SQL.AppendLine("   ) T2 On T1.StageCode = T2.StageCode ");
                    //SQL.AppendLine(") B ON A.StageCode = B.StageCode ");
                    //SQL.AppendLine("Set ");
                    //SQL.AppendLine("    A.SettledInd = B.SettledInd, ");
                    //SQL.AppendLine("    A.SettledWeightPerc = B.SettledWeightPerc, ");
                    //SQL.AppendLine("    A.SettledRevAmt = B.SettledRevAmt, ");
                    //SQL.AppendLine("    A.EstimatedCost = B.EstimatedCost, ");
                    //SQL.AppendLine("    A.LastUpBy=@UserCode, ");
                    //SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
                    //SQL.AppendLine("Where A.DocNo=@DocNo ");
                    //SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                    //SQL.AppendLine("; ");

                    SQL.AppendLine("Update TblProjectImplementationDtl A ");
                    SQL.AppendLine("Inner Join( ");
                    SQL.AppendLine("    Select T1.DocNo, T1.StageCode, If((T1.Settled / T2.Pembagi) * 100 = 100, 'Y', 'N') SettledInd, SettledWeightPerc, SettledRevAmt, EstimatedCost ");
                    SQL.AppendLine("    From( ");
                    SQL.AppendLine("        Select DocNo, StageCode, Count(*) Settled, Sum(BobotPercentage) SettledWeightPerc, ");
                    SQL.AppendLine("        Sum(SettledRevAmt) SettledRevAmt, Sum(EstimatedCost) EstimatedCost ");
                    SQL.AppendLine("        From TblProjectImplementationDtl5 ");
                    SQL.AppendLine("        Where SettledInd = 'Y' ");
                    SQL.AppendLine("        And DocNo In(Select PRJIDocNo From TblProjectDeliveryHdr Where DocNo = @PDDocNo And CancelInd = 'N' And Status = 'A') ");
                    SQL.AppendLine("        Group By DocNo, StageCode ");
                    SQL.AppendLine("    ) T1 ");
                    SQL.AppendLine("    Left Join ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select DocNo, StageCode, Count(*) Pembagi ");
                    SQL.AppendLine("        From TblProjectImplementationDtl5 ");
                    SQL.AppendLine("        Where DocNo IN(Select PRJIDocNo From TblProjectDeliveryHdr where docno = @PDDocNo And CancelInd = 'N' And Status = 'A') ");
                    SQL.AppendLine("        Group By DocNo, StageCode ");
                    SQL.AppendLine("    ) T2 On T1.StageCode = T2.StageCode And T1.DocNo = T2.DocNo ");
                    SQL.AppendLine(") B On A.DocNo = B.DocNo And A.StageCode = B.StageCode ");
                    SQL.AppendLine("Set ");
                    SQL.AppendLine("    A.SettledInd = B.SettledInd,  ");
                    SQL.AppendLine("    A.SettledWeightPerc = B.SettledWeightPerc,  ");
                    SQL.AppendLine("    A.SettledRevAmt = B.SettledRevAmt,  ");
                    SQL.AppendLine("    A.EstimatedCost = B.EstimatedCost,  ");
                    SQL.AppendLine("    A.LastUpBy = @UserCode,  ");
                    SQL.AppendLine("    A.LastUpDt = CurrentDateTime(); ");
                }
            }
            else
            {
                SQL.AppendLine("Update TblProjectImplementationDtl5 Set ");
                SQL.AppendLine("    SettledInd='N', ");
                SQL.AppendLine("    SettledAmt = 0.00, ");
                SQL.AppendLine("    SettleDt=NULL, ");
                SQL.AppendLine("    SettledRevAmt = 0.00, ");
                SQL.AppendLine("    EstimatedCost = 0.00, ");
                SQL.AppendLine("    Remark=NULL, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo And SettledInd='Y' ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                SQL.AppendLine("And Exists (Select 1 From TblProjectDeliveryHdr T Where T.DocNo = @PDDocNo And T.CancelInd = 'Y' ) ");
                SQL.AppendLine("; ");

                if (mIsWBS2SummarizedWBS1)
                {
                    SQL.AppendLine("Update TblProjectImplementationDtl A ");
                    SQL.AppendLine("Inner Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select T1.DocNo, T1.StageCode, T2.SettledInd, SettledWeightPerc, SettledRevAmt, EstimatedCost ");
                    SQL.AppendLine("    From( ");
                    SQL.AppendLine("         Select A.DocNo, StageCode, SettledInd, Sum(ifnull(B.BobotPercentage, 0.00)) SettledWeightPerc, ");
                    SQL.AppendLine("         Sum(SettledRevAmt) SettledRevAmt, Sum(EstimatedCost) EstimatedCost ");
                    SQL.AppendLine("         From TblProjectImplementationDtl5 A ");
                    SQL.AppendLine("         Left Join ");
                    SQL.AppendLine("         ( ");
                    SQL.AppendLine("            Select DocNo, ifnull(BobotPercentage, 0.00) BobotPercentage ");
                    SQL.AppendLine("            From TblProjectImplementationDtl5 Where SettledInd = 'Y' And DocNO = @DocNo ");
                    SQL.AppendLine("          ) B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("         Where SettledInd = 'N' And A.DocNo=@DocNo  ");
                    SQL.AppendLine("         And A.DocNo In(Select PRJIDocNo From TblProjectDeliveryHdr Where DocNo = @PDDocNo And CancelInd = 'Y' ) ");
                    SQL.AppendLine("         Group By A.DocNo, StageCode ");
                    SQL.AppendLine("    ) T1 ");
                    SQL.AppendLine("    Inner Join ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select X1.DocNo, X1.StageCode, If(IfNull(X2.SettledCount, 0) / Count(*) = 1, 'Y', 'N') SettledInd ");
                    SQL.AppendLine("        From TblProjectImplementationDtl5 X1 ");
                    SQL.AppendLine("        Left Join ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select T1.StageCode, Count(*) SettledCount ");
                    SQL.AppendLine("            From TblProjectImplementationDtl5 T1 ");
                    SQL.AppendLine("            Where T1.DocNo = @DocNo ");
                    SQL.AppendLine("            And T1.SettledInd = 'Y' ");
                    SQL.AppendLine("            Group By T1.StageCode ");
                    SQL.AppendLine("        ) X2 On X1.StageCode = X2.StageCode ");
                    SQL.AppendLine("        Where X1.DocNo = @DocNo ");
                    SQL.AppendLine("        And DocNo IN(Select PRJIDocNo From TblProjectDeliveryHdr where docno = @PDDocNo And CancelInd = 'Y' ) "); 
                    SQL.AppendLine("        Group By X1.DocNo, X1.StageCode ");
                    SQL.AppendLine("    ) T2 On T1.StageCode = T2.StageCode And T1.DocNo = T2.DocNo ");
                    SQL.AppendLine(") B ON A.StageCode = B.StageCode And A.StageCode = B.StageCode ");
                    SQL.AppendLine("Set ");
                    SQL.AppendLine("    A.SettledInd = B.SettledInd, ");
                    SQL.AppendLine("    A.SettledWeightPerc = B.SettledWeightPerc, ");
                    SQL.AppendLine("    A.SettledRevAmt = B.SettledRevAmt, ");
                    SQL.AppendLine("    A.EstimatedCost = B.EstimatedCost, ");
                    SQL.AppendLine("    A.LastUpBy=@UserCode, ");
                    SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where A.DocNo=@DocNo ");
                    SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
                    SQL.AppendLine("; ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PDDocNo", PDDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            return cm;
        }

        public MySqlCommand SaveJournal(string DocNo, string MenuCode, string JNDocNo)
        {
            var SQL = new StringBuilder();
            GetParameter();
            SQL.AppendLine("Update TblProjectDeliveryHdr Set ");
            SQL.AppendLine("    JournalDocNo = @JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd = 'N' And JournalDocNo Is Null;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Project Delivery : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            if (mIsProjectDeliveryJournalUseCostCenter)
                SQL.AppendLine("F.CCCode, ");
            else
                SQL.AppendLine("Null, ");
            SQL.AppendLine("A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblProjectDeliveryHdr A ");
            if (mIsProjectDeliveryJournalUseCostCenter)
            {
                SQL.AppendLine("Left Join TblProjectImplementationHdr B ON A.PRJIDocNo = B.DocNo ");
                SQL.AppendLine("Left Join TblSOContractRevisionHdr C ON B.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("Left Join TblSOContractHdr D ON C.SOCDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblBOQHdr E ON D.BOQDocNo = E.DocNo ");
                SQL.AppendLine("Left Join TblLOPHdr F ON E.LOPDocNo = F.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mProjectDeliveryJournalFormula == "1")
            {
                //piutang uninvoice
                if (mProjectAcNoFormula == "2")
                    SQL.AppendLine("        Select Concat(X4.ParValue, X3.ProjectCode2) As AcNo, ");
                else
                    SQL.AppendLine("        Select Concat(X4.ParValue, X3.ProjectCode) As AcNo, ");
                //SQL.AppendLine("        ((X5.BobotPercentage * 0.01) * X3.Amt) As DAmt, ");
                SQL.AppendLine("        X5.EstimatedAmt As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblProjectDeliveryDtl X0 ");
                SQL.AppendLine("        Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo And X0.DocNo = @DocNo And X0.Type = " + (mIsWBS2SameAsWBS ? "'WBS2'" : "'WBS1'") + " ");
                SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblParameter X4 On X4.ParCode = 'CustomerAcNoNonInvoice' And X4.ParValue Is not Null ");
                if (mIsWBS2SameAsWBS)
                    SQL.AppendLine("        Inner Join TblProjectImplementationDtl5 X5 On X0.PRJIDocNo = X5.DocNo And X0.PRJIDNo = X5.DNo ");
                else
                    SQL.AppendLine("        Inner Join TblProjectImplementationDtl X5 On X0.PRJIDocNo = X5.DocNo And X0.PRJIDNo = X5.DNo ");

                //penjualan
                SQL.AppendLine("    Union All ");

                if (mProjectAcNoFormula == "2")
                    SQL.AppendLine("        Select Concat(X8.AcNo4, '.', X3.ProjectCode2) As AcNo, ");
                else
                    SQL.AppendLine("        Select Concat(X8.AcNo4, '.', X3.ProjectCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                //SQL.AppendLine("        (X5.BobotPercentage * 0.01 * X5.Amt) As CAmt ");
                SQL.AppendLine("        X5.EstimatedAmt As CAmt ");
                SQL.AppendLine("        From TblProjectDeliveryDtl X0 ");
                SQL.AppendLine("        Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo And X0.DocNo = @DocNo And X0.Type = " + (mIsWBS2SameAsWBS ? "'WBS2'" : "'WBS1'") + " ");
                SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                if (mIsWBS2SameAsWBS)
                    SQL.AppendLine("        Inner Join TblProjectImplementationDtl5 X5 On X0.PRJIDocNo = X5.DocNo And X0.PRJIDNo = X5.DNo ");
                else
                    SQL.AppendLine("        Inner Join TblProjectImplementationDtl X5 On X0.PRJIDocNo = X5.DocNo And X0.PRJIDNo = X5.DNo ");
                SQL.AppendLine("        Inner Join TblSOContractDtl X6 On X3.DocNo = X6.DocNo ");
                SQL.AppendLine("        Inner Join TblItem X7 On X6.ItCode = X7.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory X8 On X7.ItCtCode = X8.ItCtCode ");
            }

            else
            {
                SQL.AppendLine("        Select Distinct Concat(X5.ParValue, X4.CtCode) As AcNo, ");
                SQL.AppendLine("        X0.Amt As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblProjectDeliveryHdr X0 ");
                SQL.AppendLine("        Inner Join TblProjectDeliveryDtl X0A On X0.DocNo = X0A.DocNo And X0A.Type = " + (mIsWBS2SameAsWBS || mIsWBS2SummarizedWBS1 ? "'WBS2'" : "'WBS1'") + " ");
                SQL.AppendLine("        Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo And X0.DocNo = @DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblBOQHdr X4 On X3.BOQDocNo = X4.DocNo ");
                SQL.AppendLine("        Inner Join TblParameter X5 On X5.ParCode = 'CustomerAcNoNonInvoice' And X5.ParValue Is not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Distinct Concat(X8.AcNo4) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        X0.Amt As CAmt ");
                SQL.AppendLine("        From TblProjectDeliveryHdr X0 ");
                SQL.AppendLine("        Inner Join TblProjectDeliveryDtl X0A On X0.DocNo = X0A.DocNo And X0A.Type = " + (mIsWBS2SameAsWBS || mIsWBS2SummarizedWBS1 ? "'WBS2'" : "'WBS1'") + " ");
                SQL.AppendLine("        Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo And X0.DocNo = @DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractDtl X6 On X3.DocNo = X6.DocNo ");
                SQL.AppendLine("        Inner Join TblItem X7 On X6.ItCode = X7.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory X8 On X7.ItCtCode = X8.ItCtCode  And X8.AcNo4 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select T.AcNo, T.DAmt, T.CAmt From ( ");
                SQL.AppendLine("        Select Distinct X8.AcNo5 AcNo, X5.DNo, X5.EstimatedCost As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblProjectDeliveryDtl X0 ");
                SQL.AppendLine("        Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo And X0.DocNo = @DocNo And X0.Type = " + (mIsWBS2SameAsWBS || mIsWBS2SummarizedWBS1? "'WBS2'" : "'WBS1'") + " ");
                SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblProjectImplementationDtl5 X5 On X0.PRJIDocNo = X5.DocNo And X0.PRJIDNo = X5.DNo ");
                SQL.AppendLine("        Inner Join TblSOContractDtl X6 On X3.DocNo = X6.DocNo ");
                SQL.AppendLine("        Inner Join TblItem X7 On X6.ItCode = X7.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory X8 On X7.ItCtCode = X8.ItCtCode  And X8.AcNo5 Is Not Null ");
                SQL.AppendLine("        )T ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(X4.ParValue, X3.ProjectCode2) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        X5.EstimatedCost As CAmt ");
                SQL.AppendLine("        From TblProjectDeliveryDtl X0 ");
                SQL.AppendLine("        Inner Join TblProjectImplementationHdr X1 On X0.PRJIDocNo = X1.DocNo And X0.DocNo = @DocNo And X0.Type = " + (mIsWBS2SameAsWBS || mIsWBS2SummarizedWBS1 ? "'WBS2'" : "'WBS1'") + " ");
                SQL.AppendLine("        Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblParameter X4 On X4.ParCode = 'ProjectDeliveryAcNoforAccrualPayable' And X4.ParValue Is not Null ");
                SQL.AppendLine("        Inner Join TblProjectImplementationDtl5 X5 On X0.PRJIDocNo = X5.DocNo And X0.PRJIDNo = X5.DNo ");

            }
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblProjectDeliveryHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JNDocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            decimal mTotalBobot = 0m, mTotalBobot2 = 0m, mSettledRevAmt = 0m, mEstimatedCost = 0m;
            string DocDt = Sm.GetDte(DteDocDt);

            if (ChkCancelInd.Checked)
            {
                cml.Add(EditProjectDeliveryHdr());

                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 10) == "WBS1" && !mIsWBS2SummarizedWBS1)
                        {
                            mTotalBobot += Sm.GetGrdDec(Grd2, Row, 4);
                            cml.Add(SettledTask(
                                TxtDocNo.Text,
                                TxtPRJIDocNo.Text,
                                Sm.GetGrdStr(Grd2, Row, 1),
                                Sm.GetGrdStr(Grd2, Row, 5),
                                "E",
                                DocDt
                                ));
                        }
                        if (Sm.GetGrdStr(Grd2, Row, 10) == "WBS2")
                        {
                            mTotalBobot2 += Sm.GetGrdDec(Grd2, Row, 4);
                            mSettledRevAmt += Sm.GetGrdDec(Grd2, Row, 11);
                            mEstimatedCost += Sm.GetGrdDec(Grd2, Row, 12);
                            cml.Add(SettledTask2(
                                TxtDocNo.Text,
                                TxtPRJIDocNo.Text,
                                Sm.GetGrdStr(Grd2, Row, 1),
                                Sm.GetGrdStr(Grd2, Row, 5),
                                "E",
                                DocDt
                                ));
                        }
                    }
                }

                if (!mIsWBS2SummarizedWBS1) cml.Add(UpdateAchievement(TxtDocNo.Text, TxtPRJIDocNo.Text, mTotalBobot, "E"));
                cml.Add(UpdateAchievement2(TxtDocNo.Text, TxtPRJIDocNo.Text, mTotalBobot2, "E", mSettledRevAmt, mEstimatedCost));

                //if (!mIsWBS2SameAsWBS || (mIsWBS2SameAsWBS && Sm.CompareStr(Sm.GetGrdStr(Grd1, 0, 10), "WBS2")))
                if (mIsAutoJournalActived)
                {
                    string JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
                    cml.Add(SaveJournal2(TxtDocNo.Text, JNDocNo, DocDt));
                }
            }

            //EDIT UPLOAD

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsProjectDeliveryAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd4, Row, 1))) return;
                    }
                }
            }

            if (mIsProjectDeliveryAllowToUploadFile)cml.Add(SaveUploadFile(TxtDocNo.Text));

            Sm.ExecCommands(cml);
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (mIsProjectDeliveryAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd4, Row, 1));
                        }
                    }
                }
            
            //EDIT UPLOAD

         

            ShowData(TxtDocNo.Text);
            if (mIsProjectDeliveryAllowToUploadFile) ShowUploadFile(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false) ||
                ((mIsProjectDeliveryAllowToUploadFile && TxtStatus.Text == "Approved") && IsDataNotCancelled() || !mIsProjectDeliveryAllowToUploadFile && IsDataNotCancelled()) ||
                IsDataAlreadyCancelled() ||
                IsProjectAlreadyInvoiced();
        }

        private bool IsProjectAlreadyInvoiced()
        {
            string mPRJIDNo = string.Empty;

            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, i, 10) == "WBS1")
                    {
                        if (mPRJIDNo.Length > 0) mPRJIDNo += ",";
                        mPRJIDNo += Sm.GetGrdStr(Grd2, i, 1);
                    }
                }
            }

            if (mPRJIDNo.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DNo ");
                SQL.AppendLine("From TblProjectImplementationDtl ");
                SQL.AppendLine("Where DocNo = @Param1 ");
                SQL.AppendLine("And InvoicedInd = 'Y' ");
                SQL.AppendLine("And Find_In_Set(DNo, @Param2) ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text, mPRJIDNo, string.Empty))
                {
                    string mDNo = Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text, mPRJIDNo, string.Empty);
                    int mRow = 0;
                    for(int i = 0; i < Grd2.Rows.Count; ++i)
                    {
                        if(mDNo == Sm.GetGrdStr(Grd2, i, 1))
                        {
                            mRow = i;
                            break;
                        }
                    }
                    Sm.StdMsg(mMsgType.Warning, "This data already invoiced.");
                    Sm.FocusGrd(Grd2, mRow, 2);
                    return true;
                }
            }

            return false;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You should cancel this document.");
                ChkCancelInd.Focus();
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblProjectDeliveryHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'Y'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditProjectDeliveryHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectDeliveryHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        public MySqlCommand SaveJournal2(string DocNo, string JNDocNo, string DocDt)
        {
            var SQL = new StringBuilder();
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblProjectDeliveryHdr Set ");
            SQL.AppendLine("    JournalDocNo2 = @JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo2 Is Null ");
            SQL.AppendLine("And JournalDocNo Is Not Null And CancelInd='Y'; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblProjectDeliveryHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, NULL, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblProjectDeliveryHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JNDocNo);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProjectDeliveryHdr(DocNo);
                ShowProjectDeliveryDtl(DocNo);
                Sm.ShowDocApproval(DocNo, "ProjectDelivery", ref Grd3);
                if (mIsProjectDeliveryAllowToUploadFile) ShowUploadFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectDeliveryHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PRJIDocNo, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo, A.JournalDocNo2, A.Amt, ");
            SQL.AppendLine("Case A.Status when 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            if (mIsPSModuleShowApproverRemarkInfo)
                SQL.AppendLine(", C.Remark as ApprovalRemark ");
            else
                SQL.AppendLine(", Null as ApprovalRemark ");
            SQL.AppendLine("From TblProjectDeliveryHdr A ");
            if (mIsPSModuleShowApproverRemarkInfo)
            {
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("	Select Max(T2.`Level`) LV, T1.DocNo   ");
                SQL.AppendLine("	From TblDocApproval T1   ");
                SQL.AppendLine("	Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo   ");
                SQL.AppendLine("	Where T1.DocType = 'ProjectDelivery' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '') ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine("	Group By T1.DocNo ");
                SQL.AppendLine(") B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("   Select T2.`Level` As LV, T1.DocNo, T1.Remark, T3.EmpName, T1.UserCode ");
                SQL.AppendLine("   From TblDocApproval T1   ");
                SQL.AppendLine("   Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo ");
                SQL.AppendLine("	Inner Join TblEmployee T3 On T1.UserCode = T3.UserCode  ");
                SQL.AppendLine("   Where T1.DocType = 'ProjectDelivery' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '')   ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine(") C On B.DocNo = C.DocNo And B.LV = C.LV ");
            }
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "PRJIDocNo", "Remark", 
                    
                    //6-10
                    "JournalDocNo", "JournalDocNo2", "Amt", "StatusDesc", "ApprovalRemark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[9]);
                    MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        private void ShowProjectDeliveryDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.* From ( ");
            SQL.AppendLine("Select A.DNo, A.PRJIDNo, C.StageName, ");
            if (mIsWBS2SummarizedWBS1) SQL.AppendLine("Group_Concat(D.TaskName) TaskName, ");
            else SQL.AppendLine("D.TaskName, ");
            SQL.AppendLine("B.BobotPercentage, A.Remark, F.SOCDocNo, F.DocNo As SOCRDocNo,  ");
            if(mIsWBS2SummarizedWBS1)SQL.AppendLine(" B.Amt EstimatedAmt, ");
            else SQL.AppendLine(" B.EstimatedAmt, ");
            SQL.AppendLine(" A.Type, B.SettledRevAmt, B.EstimatedCost ");
            SQL.AppendLine("From TblProjectDeliveryDtl A ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl B On A.PRJIDocNo = B.DocNo And A.PRJIDNo = B.DNo And A.Type = 'WBS1' ");
            //SQL.AppendLine("Inner Join TblProjectImplementationDtl5 B2 On B.DocNo = B2.DocNo ");
            //if(mIsWBS2SameAsWBS)
            //    SQL.AppendLine("And B.StageCode = B2.StageCode ");
            SQL.AppendLine("Inner Join TblProjectStage C On B.StageCode = C.StageCode ");
            if (mIsWBS2SummarizedWBS1)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select TaskCode, TaskName ");
                SQL.AppendLine("    From TblProjectTask ");
                SQL.AppendLine(" ) D On B2.TaskCode = D.TaskCode ");
            } 
            else SQL.AppendLine("Inner Join TblProjectTask D On B.TaskCode = D.TaskCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr E On B.DocNo = E.DocNo ");
            SQL.AppendLine("Inner JOin TblSOContractRevisionHdr F On E.SOContractDocNo = F.Docno ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            if (mIsWBS2SummarizedWBS1) SQL.AppendLine("Group By A.DocNo, A.DNo, B.StageCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.PRJIDNo, C.StageName, D.TaskName, B.BobotPercentage, A.Remark, ");
            SQL.AppendLine("F.SOCDocNo, F.DocNo As SOCRDocNo,  ");
            if (mIsWBS2SummarizedWBS1) SQL.AppendLine(" B.Amt EstimatedAmt, ");
            else SQL.AppendLine((mIsWBS2SameAsWBS ? "B.EstimatedAmt" : "0") + " estimatedAmt, ");
            SQL.AppendLine(" A.Type, B.Amt As SettledRevAmt, (B.BobotPercentage * E.TotalResource) As EstimatedCost ");
            SQL.AppendLine("From TblProjectDeliveryDtl A ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl5 B On A.PRJIDocNo = B.DocNo And A.PRJIDNo = B.DNo And A.Type = 'WBS2' ");
            SQL.AppendLine("Inner Join TblProjectStage C On B.StageCode = C.StageCode ");
            SQL.AppendLine("Inner Join TblProjectTask D On B.TaskCode = D.TaskCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr E On B.DocNo = E.DocNo ");
            SQL.AppendLine("Inner JOin TblSOContractRevisionHdr F On E.SOContractDocNo = F.Docno ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Order By T.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "PRJIDNo", 
                    //1-5
                    "StageName", "TaskName", "BobotPercentage","Remark", "SOCDocNo", 
                    //6-10
                    "SOCRDocNo", "EstimatedAmt", "Type", "SettledRevAmt", "EstimatedCost"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 9 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                   "select DNo, FileName, CreateBy, CreateDt from  Tblprojectdeliveryfile Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd4, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd4, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblprojectdeliveryfile` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `FileName` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            Sm.ExecQuery(SQL.ToString());
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;

            SQL.AppendLine("Select 1  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ProjectDelivery' ");
            SQL.AppendLine("And T.SiteCode Is Not Null ");
            SQL.AppendLine("And T.SiteCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X5.SiteCode ");
            SQL.AppendLine("    From TblProjectImplementationHdr X1 ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo And X1.DocNo = @Param ");
            SQL.AppendLine("    Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr X4 On X3.BOQDocNo = X4.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr X5 On X4.LOPDocNo = X5.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Limit 1;");
         
            if (Sm.IsDataExist(SQL.ToString(), TxtPRJIDocNo.Text)) return true;

            return false;
        }

        #region FTP

        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Project Delivery - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into tblprojectdeliveryfile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsProjectDeliveryAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsProjectDeliveryAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsProjectDeliveryAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsProjectDeliveryAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsProjectDeliveryAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsProjectDeliveryAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From tblprojectdeliveryfile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update tblprojectdeliveryfile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteEmployeeFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From tblprojectdeliveryfile Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtDocNo.Text);

            return cm;
        }

        #endregion

        internal string GetSelectedPRJIData()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + TxtPRJIDocNo.Text + Sm.GetGrdStr(Grd2, Row, 1) + Sm.GetGrdStr(Grd2, Row, 10) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mProjectAcNoFormula = Sm.GetParameter("ProjectAcNoFormula");
            mIsPRJIUseWBS2 = Sm.GetParameterBoo("IsPRJIUseWBS2");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
            mIsWBS2SameAsWBS = Sm.GetParameterBoo("IsWBS2SameAsWBS");
            mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            mIsProjectDeliveryUploadFileMandatory = Sm.GetParameterBoo("IsProjectDeliveryUploadFileMandatory");
            mIsProjectDeliveryJournalUseCostCenter = Sm.GetParameterBoo("IsProjectDeliveryJournalUseCostCenter");
            mIsWBS2SummarizedWBS1 = Sm.GetParameterBoo("IsWBS2SummarizedWBS1");
            mDocTitle = Sm.GetParameter("Doctitle");
            mProjectDeliveryJournalFormula = Sm.GetParameter("ProjectDeliveryJournalFormula");

            // FTP
            mIsProjectDeliveryAllowToUploadFile = Sm.GetParameterBoo("IsProjectDeliveryAllowToUploadFile");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            //if (mFileSizeMaxUploadFTPClient.Length == 0) mFileSizeMaxUploadFTPClient = "";
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            //if (mPortForFTPClient.Length == 0) mPortForFTPClient = "";
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            //if (mPasswordForFTPClient.Length == 0) mPasswordForFTPClient = "";
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            //if (mUsernameForFTPClient.Length == 0) mUsernameForFTPClient = "";
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            //if (mSharedFolderForFTPClient.Length == 0) mSharedFolderForFTPClient = "";
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            //if (mHostAddrForFTPClient.Length == 0) mHostAddrForFTPClient = "";
        }



        internal void ComputeAmt()
        {
            decimal mAmt = 0m;

            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                {
                    mAmt += Sm.GetGrdDec(Grd2, i, 9);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(mAmt, 0);
        }

        private void UpdateLatestSOContractRevision()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.SOCDocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param; ");

            string mSOCDocNo = Sm.GetValue(SQL.ToString(), TxtPRJIDocNo.Text);
            string mSOCR = Sm.GetValue("Select Max(DocNo) From TblSOContractRevisionHdr Where SOCDocNo = @Param And Status = 'A';", mSOCDocNo);

            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                {
                    Grd2.Cells[i, 8].Value = mSOCR;
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Grid Events

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd4.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd4_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd4.SelectedRows.Count > 0)
            {
                for (int Index = Grd4.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd4.SelectedRows[Index].Index;
            }

            if (mStateIndicator == "E" && TxtStatus.Text != "Approved")
            {
                if (Sm.GetGrdStr(Grd4, SelectedIndex, 4).Length == 0)
                {
                    Sm.GrdRemoveRow(Grd4, e, BtnSave);
                    Sm.GrdEnter(Grd4, e);
                    Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
                }
            }
            else if (mStateIndicator == "I")
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                Sm.GrdEnter(Grd4, e);
                Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Button Click

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProjectDeliveryDlg(this));
            }
        }

        private void BtnPRJIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false))
            {
                if (mDocTitle == "MNET")
                {
                    var f = new FrmProjectImplementation3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = Sm.GetMenuDesc("FrmProjectImplementation3");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtPRJIDocNo.Text;
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmProjectImplementation(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = Sm.GetMenuDesc("FrmProjectImplementation");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtPRJIDocNo.Text;
                    f.ShowDialog();
                }

            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        #endregion

        #endregion

    }
}
