﻿#region Update
/*
    30/05/2017 [HAR] tambah informasi create by dan To
    01/08/2017 [ARI] tambah kolom Location
    15/08/2017 [TKG] Tambah filter TO dan location
    06/09/2017 [WED] khusus IOK, yg di Settlement hanya WOR yang sudah di WO kan
    12/10/2017 [HAR] khusus IOK, yg di Settlement hanya WOR yang sudah di WO kan dan statusnya FIXED
    12/10/2017 [HAR] query saat update tambah settle by
    16/10/2017 [HAR] bug fixing saat milih wor( WO yg munucl yag Fixed) berdasarkan parameter
    16/11/2017 [HAR] tambah function approve dan cancel
    23/11/2017 [HAR] bug fixing saat klik btn WO dtl (WO banyak)
    23/01/2018 [WED] bug fixing, validasi baik approve maupun cancel dokumen WO yang belum FIXED
    25/06/2018 [WED] bug fixing, salah ambil nomor kolom di IsWORAlreadyCancelled()
    14/08/2018 [HAR] bug fixing, location non aktif masih muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWORSettlement : RunSystem.FrmBase5
    {
        #region Field

        private string mMenuCode = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySite = false, mIsSettleOnlyProcessedWOR = false;

        #endregion

        #region Constructor

        public FrmWORSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                GetParameter();
                SetLueLocCode(ref LueLocCode);
                Sl.SetLueOption(ref LueMaintenanceStatus, "MaintenanceStatus");
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsSettleOnlyProcessedWOR = Sm.GetParameter("IsSettleOnlyProcessedWOR") == "Y";
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo, B.DocDt, F.WoDocNo, ");
            SQL.AppendLine("C.OptDesc As MtcStatusDesc, D.OptDesc As MtcTypeDesc, E.OptDesc As SymProblemDesc, I.LocName, ");
            SQL.AppendLine("B.Description, F.Createby, B.TOCode, G.Assetname As TOName, G.Displayname As TODesc ");
            SQL.AppendLine("From TblWOR B  ");
            SQL.AppendLine("Left Join TblOption C On B.MtcStatus=C.OptCode And C.OptCat ='MaintenanceStatus'  ");
            SQL.AppendLine("Left Join TblOption D On B.MtcType=D.OptCode And D.OptCat ='MaintenanceType'  ");
            SQL.AppendLine("Left Join TblOption E On B.SymProblem=E.OptCode And E.OptCat ='SymptomProblem'  ");
            if (mIsSettleOnlyProcessedWOR)
            {
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select WORDocNo, group_concat(DocNo) As WODocNo, group_concat(distinct Createby) As CreateBy From TblWOHdr   ");
                SQL.AppendLine("    Where SettleInd = 'N' And cancelInd = 'N' And FixedInd='Y' ");
                SQL.AppendLine("    Group by WORDocNo  ");
                SQL.AppendLine(")F On F.WorDocNo = B.DocNo  ");
            }
            else
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select WORDocNo, group_concat(DocNo) As WODocNo, group_concat(distinct Createby) As CreateBy From TblWOHdr   ");
                SQL.AppendLine("    Group by WORDocNo  ");
                SQL.AppendLine(")F On F.WorDocNo = B.DocNo  ");
            }
            SQL.AppendLine("Inner Join TblAsset G On B.ToCOde = G.AssetCode ");
            SQL.AppendLine("Inner Join TblTOHdr H On B.TOCode = H.AssetCode ");
            SQL.AppendLine("Left Join TblLocation I On H.LocCode = I.LocCode ");
            SQL.AppendLine("Where (B.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And B.CancelInd='N' And B.Status = 'A' ");
            SQL.AppendLine("And B.WOStatus='O'  ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Approve",
                        //1-5
                        "Cancel",
                        "WO Request", 
                        "",
                        "Date",
                        "WO",

                        //6-10
                        "",
                        "Status",
                        "Type",
                        "Symptom"+Environment.NewLine+"Problem",
                        "Location",
                        
                        //11-15
                        "Description",
                        "TO"+Environment.NewLine+"Code",
                        "TO"+Environment.NewLine+"Name", 
                        "TO"+Environment.NewLine+"Description",
                        "Created By"
                    },
                    new int[] 
                    {
                        //0
                        80,

                        //1-5
                        80, 170, 20, 80, 130, 
                        
                        //6-10
                        20, 160, 180, 180, 150,   
                        
                        //11-15
                        250, 150, 180, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0, 1 });
            Sm.GrdColButton(Grd1, new int[] { 3, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 12, 13, 14, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] {  2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtTOCode.Text, "G.AssetName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "H.LocCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMaintenanceStatus), "B.MtcStatus", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.DocDt Desc, B.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "WODocNo", "MtcStatusDesc", "MtcTypeDesc", "SymProblemDesc", 

                            //6-10
                            "LocName", "Description", "TOCode", "TOName", "TODesc", 
                            
                            //11
                            "Createby"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0))
                    {
                        cml.Add(SaveWORHdr(Sm.GetGrdStr(Grd1, r, 2)));
                    }
                    if (Sm.GetGrdBool(Grd1, r, 1))
                    {
                        cml.Add(SaveWORHdr2(Sm.GetGrdStr(Grd1, r, 2)));
                    }
                }

                Sm.ExecCommands(cml);
                ShowData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return
                IsDataNotExisted() ||
                IsWORAlreadyCancelled() ||
                IsWORAlreadyClosed() ||
                IsWONotFixed();
        }

        private bool IsWONotFixed()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0) || Sm.GetGrdBool(Grd1, r, 1))
                {
                    string mFixedInd = "Select DocNo From TblWOHdr Where WORDocNo = @Param And SettleInd = 'N' And CancelInd = 'N' And FixedInd = 'N' Limit 1;";
                    if (Sm.IsDataExist(mFixedInd, Sm.GetGrdStr(Grd1, r, 2)))
                    {
                        string WODocNo = Sm.GetValue(mFixedInd, Sm.GetGrdStr(Grd1, r, 2));
                        Sm.StdMsg(mMsgType.Warning, "You need to fixed this WO Document : (" + WODocNo + ") to either approve or cancel this WOR Document : (" + Sm.GetGrdStr(Grd1, r, 2) + ").");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDataNotExisted()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 0) || Sm.GetGrdBool(Grd1, r, 1)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to settle or cancel minimum 1 WO Request.");
            return true;
        }

        private bool IsWORAlreadyCancelled()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0) && IsWORAlreadyCancelled(Sm.GetGrdStr(Grd1, r, 2)))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "WO Request : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine + 
                        "This WO Request already cancelled.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWORAlreadyCancelled(string DocNo)
        {
            return Sm.IsDataExist("Select DocNo From TblWOR Where CancelInd='Y' And DocNo=@Param;", DocNo);
        }

        private bool IsWORAlreadyClosed()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0) && IsWORAlreadyClosed(Sm.GetGrdStr(Grd1, r, 1)))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "WO Request : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine + 
                        "This WO Request already closed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWORAlreadyClosed(string DocNo)
        {
            return Sm.IsDataExist("Select DocNo From TblWOR Where WOStatus='C' And DocNo=@Param;", DocNo);
        }

       

        private MySqlCommand SaveWORHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWOR Set WOStatus = 'C', CloseDt = Left(CurrentDateTime(), 8), LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ");
            if (mIsSettleOnlyProcessedWOR)
                SQL.AppendLine("And Exists (Select WORDocNo From TblWOHdr Where WORDocNo = @DocNo And CancelInd = 'N' And SettleInd = 'N' And FixedInd = 'Y' Limit 1) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblWOHdr Set SettleInd='Y', SettleDt = Left(CurrentDateTime(), 8), SettleBy = @UserCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WORDocNo=@DocNo And CancelInd='N' And SettleInd='N' And FixedInd = 'Y'; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveWORHdr2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWOR Set CancelInd = 'Y', cancelReason='Cancel from WOR settlement', CloseDt = Left(CurrentDateTime(), 8), LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblWOHdr Set SettleInd='N', cancelInd = 'Y', cancelReason='Cancel from WOR settlement',  SettleDt = Left(CurrentDateTime(), 8), SettleBy = @UserCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WORDocNo=@DocNo And CancelInd='N' And SettleInd='N' And FixedInd = 'Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                string WODocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                string[] ListWODocNo = WODocNo.Split(',');
                foreach (string DocNo in ListWODocNo)
                {
                    var f = new FrmWO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = DocNo;
                    f.ShowDialog();
                }
            }
        }
 
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                string WODocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                string[] ListWODocNo = WODocNo.Split(',');
                foreach (string DocNo in ListWODocNo)
                {
                    var f = new FrmWO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = DocNo;
                    f.ShowDialog();
                }
            }
        }

        
        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void TxtTOCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTOCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Technical Object");
        }

        private void LueMaintenanceStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMaintenanceStatus, new Sm.RefreshLue2(Sl.SetLueOption), "MaintenanceStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMaintenanceStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Maintenance status");
        }

        #endregion

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if ((e.ColIndex == 0 || e.ColIndex == 1))
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex))
                    Grd1.Cells[e.RowIndex, (e.ColIndex == 0) ? 1 : 0].Value = false;
                else
                    Grd1.Cells[e.RowIndex, 8].Value = null;
            }
        }

        #endregion
    }
}
