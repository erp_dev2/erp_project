﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeRecruitmentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmployeeRecruitment mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmployeeRecruitmentFind(FrmEmployeeRecruitment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" Select A.DocNo, A.DocDt, A.EmpName, A.DeptCode,  C.DeptName, D.PosName,");
            SQL.AppendLine(" A.IdNumber, E.OptDesc As Gender, F.OptDesc As Religion, ");
            SQL.AppendLine(" A.BirthPlace, A.BirthDt, A.Address, G.CityName, A.PostalCode, A.Phone, A.Mobile, A.Email, A.NPWP, A.EmployeeRequestDocNo, ");
            SQL.AppendLine(" A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine(" From TblEmployeeRecruitment A ");
            SQL.AppendLine(" Left Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine(" left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine(" Left Join TblPosition D On A.PosCode=D.PosCode ");
            SQL.AppendLine(" Left Join TblOption E On A.Gender=E.OptCode And E.OptCat='Gender' ");
            SQL.AppendLine(" Left Join TblOption F On A.Religion=F.OptCode And F.OptCat='Religion' ");
            SQL.AppendLine(" Left Join TblCity G On A.CityCode=G.CityCode ");
            if (mFrmParent.mIsFilterBySiteHR || mFrmParent.mIsFilterByLevelHR)
            {
                SQL.AppendLine(" Inner Join TblEmployeeRequest H On A.EmployeeRequestDocNo=H.DocNo ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And H.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(H.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsFilterByLevelHR)
                {
                    SQL.AppendLine("And H.LevelCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupLevel ");
                    SQL.AppendLine("    Where LevelCode=IfNull(H.LevelCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Employee's Name",
                        "Department",
                        "Position",
                        
                        //6-10
                        "Identity#",
                        "Gender",
                        "Religion",
                        "Birth Place",
                        "Birth Date",

                        //11-15
                        "Address",
                        "City",
                        "Postal Code",
                        "Phone",
                        "Mobile",

                        //16-20
                        "Email",
                        "NPWP",
                        "Recruitment",
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date",

                        //21-24
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 200, 150, 150,  
                        
                        //6-10
                        150, 100, 100, 120, 80,  
                        
                        //11-15
                        150,  130, 80, 100, 100,   

                        //16-20
                        100, 100, 150, 100, 100,  

                        //21-25
                        100, 100, 100, 100,

                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 10, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " ",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "EmpName", "DeptName",  "PosName", "IdNumber",

                            //6-10
                            "Gender","Religion","Birthplace","BirthDt", "Address",
                            
                            //11-15
                            "CityName","PostalCode","Phone","Mobile","Email",    
                            
                            //16-20
                             "NPWP", "EmployeeRequestDocNo", "CreateBy", "CreateDt","LastUpBy",
                            //21
                             "LastUpDt"
                                                    },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        #endregion
    }
}
