﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockHistory : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptStockHistory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.WhsCode,C.ItCtCode, B.WhsName, A.Lot, A.Bin, A.ItCode, C.ItName, D.ItCtName, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("    Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("    Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("    Group By A.WhsCode,C.ItCtCode,B.WhsName, A.Lot, A.Bin, A.ItCode, C.ItName, D.ItCtName, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3 ");
            SQL.AppendLine("    Having Sum(A.Qty)>0 Or Sum(A.Qty2)>0 Or Sum(A.Qty3)>0 ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Item Code",
                        "",
                        "Item Name",
                        "Batch Number", 
                        
                        //6
                        "Source"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 20, 200, 200, 
                        
                        //6
                        170
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 9, 10, 11, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        //override protected void ShowData()
        //{
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;

        //        string
        //            Filter = string.Empty,
        //            DocDt = (ChkDocDt.Checked) ? Sm.GetDte(DteDocDt).Substring(0, 8) : "";
        //        var cm = new MySqlCommand();

        //        Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T.WhsCode", true);
        //        Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCatCode), "T.ItCtCode", true);
        //        Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });

        //        if (ChkDocDt.Checked) DocDt = Sm.GetDte(DteDocDt).Substring(0, 8);

        //        Sm.ShowDataInGrid(
        //        ref Grd1, ref cm,
        //        ((ChkDocDt.Checked && decimal.Parse(DocDt) < decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
        //        SetSQL2(DocDt) : mSQL) +
        //        Filter + " Order By WhsName, Lot, Bin, ItCode",
        //        new string[]
        //            {
        //                //0
        //                "WhsName", 

        //                //1-5
        //                "Lot", "Bin", "ItCode", "ItName", "ItCtName", 
                        
        //                //6-10
        //                "Qty", "InventoryUomCode", "Qty2", "InventoryUOMCode2", "Qty3", 
                        
        //                //11
        //                "InventoryUOMCode3",
        //            },
        //        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
        //        {
        //            Grd1.Cells[Row, 0].Value = Row + 1;
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
        //            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
        //            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
        //            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
        //        }, true, true, false, false
        //        );
        //        iGSubtotalManager.BackColor = Color.LightSalmon;
        //        iGSubtotalManager.ShowSubtotalsInCells = true;
        //        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 9, 11 });

        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //    finally
        //    {
        //        if (Grd1.Rows.Count > 1)
        //            Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 5 : 2);
        //        else
        //            Sm.FocusGrd(Grd1, 0, 0);
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        #endregion

        #endregion
    }
}
