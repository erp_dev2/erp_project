﻿#region Update
// 03/01/2018 [HAR] tambah grid training Suggestion
// 08/02/2018 [HAR] tambah mCompetence jika dipanggil dari form lain
// 26/03/2018 [HAR] level name ambil dari master level, tambh combo  potensi dan kompetensi 
// 25/02/2020 [HAR/SIER] tambah length untuk Competence name
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCompetence : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mCompetenceCode = string.Empty;
        internal FrmCompetenceFind FrmFind;


        #endregion

        #region Constructor

        public FrmCompetence(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetGrd();
            SetFormControl(mState.View);
            Sl.SetLueCompetenceLevel(ref LueLvlFrom);
            Sl.SetLueCompetenceLevel(ref LueLvlTo);
            Sl.SetLueCompetenceLevel(ref LueLvlMax);
            Sl.SetLueLevelCode(ref LueCompLevelName);
            SetLueCompetenceLevelType(ref LueCompetenceLevelType);

            if (mCompetenceCode.Length != 0)
            {
                this.Text = "Competence";
                ShowData(mCompetenceCode);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method


        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Training"+Environment.NewLine+"Code",
                        "Training",
                        "Minimum"+Environment.NewLine+"Hour",
                        "Minimum"+Environment.NewLine+"Score",
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 120, 300, 120, 120,
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0,2 }, false);

            #endregion

        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCompetenceCode, TxtCompetenceName, ChkActInd,
                        LueCompLevelName, LueLvlFrom, LueLvlTo, LueLvlMax, LueCompetenceLevelType, MeeRemark
                    }, true);
                    TxtCompetenceCode.Focus();
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCompetenceCode, TxtCompetenceName, 
                        LueCompLevelName, LueLvlFrom, LueLvlTo, LueLvlMax, LueCompetenceLevelType, MeeRemark 
                    }, false);
                    TxtCompetenceCode.Focus();
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 4, 5 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCompetenceName, LueLvlFrom, LueLvlTo, LueLvlMax, MeeRemark, ChkActInd
                    }, false);
                    TxtCompetenceName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCompetenceCode, TxtCompetenceName, 
                LueCompLevelName, LueLvlFrom, LueLvlTo, LueLvlMax, LueCompetenceLevelType, MeeRemark
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCompetenceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCompetenceCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCompetenceCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCompetence Where CompetenceCode=@CompetenceCode" };
                Sm.CmParam<String>(ref cm, "@CompetenceCode", TxtCompetenceCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                   InsertData();
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CompetenceCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CompetenceCode", CompetenceCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select CompetenceCode, CompetenceName, CompetenceLevel,  LevelFrom, LevelTo, LevelMax, ActiveInd, CompetenceLevelType, Remark From TblCompetence Where CompetenceCode=@CompetenceCode",
                        new string[] 
                        {
                            //0
                            "CompetenceCode", 
                            //1-5
                            "CompetenceName", "CompetenceLevel", "LevelFrom", "LevelTo", "LevelMax", 
                            //6-8
                            "ActiveInd", "CompetenceLevelType", "Remark" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCompetenceCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCompetenceName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueCompLevelName, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueLvlFrom, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueLvlTo, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueLvlMax, Sm.DrStr(dr, c[5]));
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y");
                            Sm.SetLue(LueCompetenceLevelType, Sm.DrStr(dr, c[7]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        }, true
                    );

                ShowCompetenceDtl(CompetenceCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowCompetenceDtl(string CompetenceCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CompetenceCode", CompetenceCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.TrainingCode, B.TrainingName, A.MinHour, A.MinScore ");
            SQL.AppendLine("From TblCompetenceDtl A  ");
            SQL.AppendLine("Inner Join TblTraining B On A.TrainingCode = B.TrainingCode  ");
            SQL.AppendLine("Where A.CompetenceCode=@CompetenceCode ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + " Order bY  A.DNo;",
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-4
                    "TrainingCode", "TrainingName", "MinHour", "MinScore" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }


        #endregion

        #region Save Data
  
        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveCompetence());

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveCompetenceDtl(Row));

            Sm.ExecCommands(cml);

            ShowData(TxtCompetenceCode.Text);
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 training suggestion.");
                return true;
            }
            return false;
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCompetenceCode, "Competence code", false) ||
                Sm.IsTxtEmpty(TxtCompetenceName, "Competence name", false) ||
                Sm.IsLueEmpty(LueCompLevelName, "Competence level") ||
                Sm.IsLueEmpty(LueCompetenceLevelType, "Position / Competence ") ||
                IsCompetenceCodeExisted()||
                IsGrdEmpty()||
                IsCompetenceCodeAlreadyNonActive();
        }

        private bool IsCompetenceCodeExisted()
        {
            if (!TxtCompetenceCode.Properties.ReadOnly && Sm.IsDataExist("Select CompetenceCode From TblCompetence Where CompetenceCode='" + TxtCompetenceCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Competence code ( " + TxtCompetenceCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsCompetenceCodeAlreadyNonActive()
        {
            if (Sm.IsDataExist("Select CompetenceCode From TblCompetence Where CompetenceCode='" + TxtCompetenceCode.Text + "' And ActiveInd = 'N' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Competence code ( " + TxtCompetenceCode.Text + " ) already non-active.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCompetence()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCompetence(CompetenceCode, ActiveInd,  CompetenceName, CompetenceLevel, LevelFrom, LevelTo, LevelMax, CompetenceLevelType, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CompetenceCode, @ActiveInd, @CompetenceName,  @CompetenceLevel, @LevelFrom, @LevelTo, @LevelMax, @CompetenceLevelType, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update CompetenceName=@CompetenceName, ActiveInd=@ActiveInd, CompetenceLevel=@CompetenceLevel, LevelFrom=@LevelFrom, LevelTo=@LevelTo, LevelMax=@LevelMax, CompetenceLevelType=@CompetenceLevelType, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblCompetenceDtl Where CompetenceCode =@CompetenceCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CompetenceCode", TxtCompetenceCode.Text);
            Sm.CmParam<String>(ref cm, "@CompetenceName", TxtCompetenceName.Text);
            Sm.CmParam<String>(ref cm, "@CompetenceLevel", Sm.GetLue(LueCompLevelName));
            Sm.CmParam<String>(ref cm, "@LevelFrom", Sm.GetLue(LueLvlFrom));
            Sm.CmParam<String>(ref cm, "@LevelTo", Sm.GetLue(LueLvlTo));
            Sm.CmParam<String>(ref cm, "@LevelMax", Sm.GetLue(LueLvlMax));
            Sm.CmParam<String>(ref cm, "@CompetenceLevelType", Sm.GetLue(LueCompetenceLevelType));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCompetenceDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCompetenceDtl(CompetenceCode, DNo, TrainingCode, MinHour, MinScore, CreateBy, CreateDt) " +
                    "Values(@CompetenceCode, @DNo, @TrainingCode, @MinHour, @MinScore, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@CompetenceCode", TxtCompetenceCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@MinHour", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@MinScore", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion 

        #endregion

        #region Additional Method

        private void SetLueCompetenceLevelType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'CompetenceLevelType' Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        internal string GetSelectedTrainingCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmCompetenceDlg(this));
            }

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled)
               Sm.FormShowDialog(new FrmCompetenceDlg(this));

        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        private void LueCompLevelName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompLevelName, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void TxtCompetenceCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCompetenceCode);
        }

        private void TxtCompetenceName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCompetenceName);
        }

        private void LueLvlFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvlFrom, new Sm.RefreshLue1(Sl.SetLueCompetenceLevel));
        }

        private void LueLvlTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvlTo, new Sm.RefreshLue1(Sl.SetLueCompetenceLevel));
        }

        private void LueLvlMax_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvlMax, new Sm.RefreshLue1(Sl.SetLueCompetenceLevel));
        }


        private void LueCompetenceLevelType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceLevelType, new Sm.RefreshLue1(SetLueCompetenceLevelType));
        }
        
        #endregion

       

        

        #endregion
    }
}
