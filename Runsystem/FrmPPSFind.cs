﻿#region Update
/*
    04/09/2017 [TKG] Ubah filter document date   
    12/09/2017 [TKG] tambah employment status, tambah validasi berdasarkan otorisasi
    30/10/2017 [TKG] otorisasi department dan site berdasarkan group
    31/03/2018 [TKG] filter by level
    23/07/2020 [IBL/SIER] tambah informasi Acting Official (PLT) 
    24/11/2020 [TKG/PHT] tambah level
 *  19/02/2021 [RDH/PHT] tambah info download SK yang sudah diupload sebelumnya
 *  04/03/2021 [ICA/PHT] mengubah validasi ketika attachment kosong dan menambahkan readonly pada grid
 *  16/11/2021 [IBL/SIER] ActingOfficialInd ambil dari master employee. Berdasarkan parameter ActingOfficialIndSource
 *  25/03/2023 [MAU/PHT] memindahkan kolom Department Name New
 *  29/03/2023 [MAU/PHT] penyesuaian seluruh kolom old bersampingan dengan kolom new 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPPSFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPPS mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPPSFind(FrmPPS FrmParent, string FilterBySite)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.StartDt, A.EmpCode, B.EmpName, H3.Optdesc As JobTransfer, A.EmployeeRequestDocNo, ");
            SQL.AppendLine("C.DeptName As Deptnameold, D.Posname As PosNameOld, E.GrdLvlname As GrdLvlNameOld, ");
            SQL.AppendLine("F.OptDesc As EmpStatOld, G.OptDesc As EmpSysTypeOld, H.OptDesc As EmpPaYperOld, I.PGName As PGNameOld, ");
            SQL.AppendLine("C2.DeptName As DeptnameNew, D2.Posname As PosNameNew, E2.GrdLvlname As GrdLvlNameNew, ");
            SQL.AppendLine("F2.OptDesc As EmpStatNew, G2.OptDesc As EmpSysTypeNew, H2.OptDesc As EmpPaYperNew, I2.PGName As PGNameNew, ");
            SQL.AppendLine("J.SiteName As SiteNameOld, J2.SiteName As SiteNameNew, ");
            SQL.AppendLine("K.LevelName As LevelNameOld, K2.LevelName As LevelNameNew, ");
            if (mFrmParent.mActingOfficialIndSource == "1")
                SQL.AppendLine("D.ActingOfficialInd As ActingOfficialIndOld, D2.ActingOfficialInd As ActingOfficialIndNew, ");
            else
                SQL.AppendLine("A.ActingOfficialIndOld, A.ActingOfficialInd As ActingOfficialIndNew, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPPS A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            if (mFrmParent.mIsFilterByLevelHR)
            {
                SQL.AppendLine("And (B.GrdLvlCode Is Null Or (B.GrdLvlCode Is Not Null ");
                SQL.AppendLine("And B.GrdLvlCode In ( ");
                SQL.AppendLine("    Select X.GrdLvlCode From TblGradeLevelHdr X ");
                SQL.AppendLine("    Where X.LevelCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where LevelCode=X.LevelCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Left Join tblDepartment C On A.DeptCodeOld = C.deptCode");
            SQL.AppendLine("Left Join TblPosition D On A.PosCodeOld = D.PosCode");
            SQL.AppendLine("Left Join TblgradeLevelhdr E On A.GrdlvlCodeOld = E.GrdlvlCode");
            SQL.AppendLine("Left Join Tbloption H3 On A.Jobtransfer=H3.OptCode And optcat='EmpJobTransfer' ");
            SQL.AppendLine("left Join ( ");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'EmploymentStatus'");
            SQL.AppendLine(")F On A.EmploymentStatusOld = F.OptCode ");
            SQL.AppendLine("Left join (");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'EmpSystemType' ");
            SQL.AppendLine(")G On A.SystemTypeOld = G.OptCode ");
            SQL.AppendLine("Left Join( ");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'PayrunPeriod' ");
            SQL.AppendLine(")H On A.PayrunPeriodOld = H.OptCode ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On A.PGCodeOld = I.PGCode");
            SQL.AppendLine("Left Join TblSite J On A.SiteCodeOld = J.SiteCode ");
            SQL.AppendLine("Left Join TblLevelHdr K On A.LevelCodeOld=K.LevelCode ");
            SQL.AppendLine("Left Join tblDepartment C2 On A.DeptCodeNew = C2.deptCode ");
            SQL.AppendLine("Left Join TblPosition D2 On A.PosCodeNew = D2.PosCode ");
            SQL.AppendLine("left Join TblgradeLevelhdr E2 On A.GrdlvlCodeNew = E2.GrdlvlCode ");
            SQL.AppendLine("left Join ( ");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'EmploymentStatus' ");
            SQL.AppendLine(")F2 On A.EmploymentStatusNew = F2.OptCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'EmpSystemType' ");
            SQL.AppendLine(")G2 On A.SystemTypeNew = G2.OptCode ");
            SQL.AppendLine("Left Join( ");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'PayrunPeriod' ");
            SQL.AppendLine(")H2 On A.PayrunPeriodNew = H2.OptCode ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I2 On A.PGCodeNew = I2.PGCode");
            SQL.AppendLine("Left Join TblSite J2 On A.SiteCodeNew = J2.SiteCode ");
            SQL.AppendLine("Left Join TblLevelHdr K2 On A.LevelCodeNew=K2.LevelCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (A.SiteCodeOld Is Null Or (A.SiteCodeOld Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCodeOld, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");

                SQL.AppendLine("And (A.SiteCodeNew Is Null Or (A.SiteCodeNew Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCodeNew, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And (A.DeptCodeOld Is Null Or (A.DeptCodeOld Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCodeOld, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");

                SQL.AppendLine("And (A.DeptCodeNew Is Null Or (A.DeptCodeNew Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCodeNew, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            mSQL = SQL.ToString();
        }

      private void SetGrd()
        {
            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "Start"+Environment.NewLine+"Date",

                        //6-10
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Job Transfer",
                        "Employee"+Environment.NewLine+"Request#",
                        "Department Name"+Environment.NewLine+"Old",

                        //11-15
                        "Position Name"+Environment.NewLine+"Old",
                        "Grade Level Name"+Environment.NewLine+"Old",
                        "Employment"+Environment.NewLine+"Status Old",
                        "System Type"+Environment.NewLine+"Old",
                        "Payrun Periode"+Environment.NewLine+"Old",

                        //16-20
                        "Payroll Group"+Environment.NewLine+"Old",
                        "Department Name"+Environment.NewLine+"New",
                        "Position Name"+Environment.NewLine+"New",
                        "Grade Level Name"+Environment.NewLine+"New",
                        "Employment"+Environment.NewLine+"Status New",

                        //21-25
                        "System Type"+Environment.NewLine+"New",
                        "Payrun Periode"+Environment.NewLine+"New",
                        "Payroll Group"+Environment.NewLine+"New",
                        "Site Old",
                        "Site New",
                        
                        //26-30
                        "Level Old",
                        "Level New",
                        "Acting Official"+Environment.NewLine+"Old",
                        "Acting Official"+Environment.NewLine+"New",
                        "",

                        //31-35
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By", 
                        "Last Updated Date",
 
                        //36
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 100, 80, 
                        
                        //6-10
                        100, 200, 130, 130, 150, 

                        //11-15
                        150, 150, 100, 100, 100,

                        //16-20
                        100, 150, 150, 150, 100,
                        
                        //21-25
                        100, 100, 100, 130, 200,
                        
                        //26-30
                        130, 200, 130, 200, 20,
                        
                        //31-35
                        130, 130, 130, 130, 130,

                        //36
                        130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 28, 29 });
            Sm.GrdColButton(Grd1, new int[] { 30 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 5, 32, 35 });
            Sm.GrdFormatTime(Grd1, new int[] { 33, 36 });
            Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33, 34, 35, 36}, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 31, 32, 33, 34, 35, 36 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[17].Move(11);
            Grd1.Cols[18].Move(13);
            Grd1.Cols[19].Move(15);
            Grd1.Cols[20].Move(17);
            Grd1.Cols[21].Move(19);
            Grd1.Cols[22].Move(21);


        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33, 34, 35, 36 }, !ChkHideInfoInGrd.Checked);
           
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCodeOld", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "StartDt", "EmpCode", 

                            //6-10
                            "EmpName", "JobTransfer", "EmployeeRequestDocNo", "DeptnameOld", "PosNameOld", 
                            
                            //11-15
                            "GrdLvlNameOld", "EmpStatOld", "EmpSysTypeOld", "EmpPaYperOld", "PGNameOld", 
                            
                            //16-20
                            "DeptnameNew", "PosNameNew", "GrdLvlNameNew", "EmpStatNew", "EmpSysTypeNew", 
                            
                            //21-25
                            "EmpPaYperNew", "PGNameNew", "SiteNameOld", "SiteNameNew", "LevelNameOld",

                            //26-30
                            "LevelNameNew", "ActingOfficialIndOld", "ActingOfficialIndNew", "CreateBy", "CreateDt", 
                            
                            //31-32
                            "LastUpBy", "LastUpDt", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 33, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 31);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 35, 32);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 36, 32);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 30 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (IsAttachmentNotExists(Sm.GetGrdStr(Grd1, e.RowIndex, 1)))
                {
                    Sm.StdMsg(mMsgType.Warning, "No File was Downloaded Yet!");
                }
                else
                {
                    var f = new FrmPPSDlg4(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.mHostAddrForFTPClient = mFrmParent.mHostAddrForFTPClient;
                    f.mPortForFTPClient = mFrmParent.mPortForFTPClient;
                    f.mUsernameForFTPClient = mFrmParent.mUsernameForFTPClient;
                    f.mPasswordForFTPClient = mFrmParent.mPasswordForFTPClient;
                    f.mSharedFolderForFTPClient = mFrmParent.mSharedFolderForFTPClient;
                    f.ShowDialog();      
                }
                
            }
        }


        public static bool IsAttachmentNotExists(string DocNo)
        {
            var cek = Sm.IsDataExist("Select 1 FROM tblpps where DocNo = @Param and Filename is null and Filename2 is null and filename3 is null", DocNo);
            return cek;
        }
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion 

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

    }
}
