#region Update
    // 06/04/2017 [WED] di Axes Customization, ChartLabelIntersectAction diganti menjadi Wrap (tadinya MultipleRows)
#endregion

#region Copyright Syncfusion Inc. 2001 - 2016
// Copyright Syncfusion Inc. 2001 - 2016. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Text;
using Syncfusion.Windows.Forms.Chart;
using Syncfusion.Drawing;
using System.Drawing;

#endregion

namespace RunSystem
{
    public static class ChartAppearance
    {
        public static void ApplyChartStyles(ChartControl chart)
        {
            #region ApplyCustomPalette
            //chart.Skins = Skins.None;
            #endregion

            #region Chart Appearance Customization

            //chart.ChartArea.PrimaryXAxis.ClearBreaks();
            //chart.ChartArea.PrimaryXAxis.HidePartialLabels = false;
            chart.PrimaryXAxis.EdgeLabelsDrawingMode = ChartAxisEdgeLabelsDrawingMode.Center;

            #endregion

            #region Axes Customization

            chart.PrimaryXAxis.TickLabelsDrawingMode = ChartAxisTickLabelDrawingMode.UserMode;
            chart.PrimaryXAxis.LabelIntersectAction = ChartLabelIntersectAction.Wrap;

            chart.PrimaryXAxis.LabelRotate = false;
            //chart.PrimaryXAxis.LabelRotateAngle = 270;

            #region Axes Customization

            //chart.PrimaryYAxis.RangeType = ChartAxisRangeType.Set;
            //chart.PrimaryYAxis.Range = new MinMaxInfo(0, 80, 20);
            //chart.PrimaryXAxis.RangeType = ChartAxisRangeType.Set;
            //chart.PrimaryXAxis.Range = new MinMaxInfo(1998, 2009, 1);

            #endregion

            #endregion

            #region Legend Customization

            //chart.Legend.RepresentationType = ChartLegendRepresentationType.SeriesType;
            //chart.LegendPosition = ChartDock.Top;
            //chart.LegendsPlacement = ChartPlacement.Outside;
            //chart.LegendAlignment = ChartAlignment.Center;

            #endregion
        }
    }
}
