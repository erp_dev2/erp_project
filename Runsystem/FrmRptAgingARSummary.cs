﻿#region Update
/*
    19/08/2021 [IBL/KIM] New apps
    21/09/2021 [HAR/KIM] bug nilai balace tidak sama dengan nilai TB akun oiutang usaha
    17/12/2021 [HAR/KIM] bug nilai - nilai invoice yang sdh lunas masih muncul- periode perhitungan aging dari docdt ke currentdate
    06/04/2022 [TKG/KIM] menggunakan query ar sub ledger sbg sumbernya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mCustomerAcNoAR = string.Empty,
            mAccountingRptStartFrom = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                if (mAccountingRptStartFrom.Length > 0)
                {
                    Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                    Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                }
                else
                {
                    Sl.SetLueYr(LueStartFrom, string.Empty);
                    Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                }
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'CustomerAcNoAR', 'AccountingRptStartFrom' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "CustomerAcNoAR": mCustomerAcNoAR = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(Left(X1.Period, 4), '-', Right(X1.Period, 2)) As Period, ");
            SQL.AppendLine("X1.SalesInvoiceDocNo, X1.DocDt,  ");
            SQL.AppendLine("X2.CtName, X2.CtCode, X1.SalesInvoiceAmt, X1.TransType,  ");
            SQL.AppendLine("Case X1.TransType ");
            SQL.AppendLine("	When '1' Then 'Opening Balance' ");
            SQL.AppendLine("	When '2' Then ");
            SQL.AppendLine("	    Case SubType ");
            SQL.AppendLine("	        When '1' Then 'Voucher (Incoming Payment)' ");
            SQL.AppendLine("	        When '2' Then 'AR Settlement' ");
            SQL.AppendLine("	        When '3' Then 'Voucher (Incoming Payment for Invoice Without Journal)' ");
            SQL.AppendLine("	        When '4' Then 'Voucher (Incoming Payment With Deposit)' ");
            SQL.AppendLine("	    End ");
            SQL.AppendLine("	When '3' Then Concat('Voucher (Incoming Payment) Before ', @StartFrom) ");
            SQL.AppendLine("	When '4' Then Concat('AR Settlement Before ', @StartFrom) ");
            SQL.AppendLine("	When '5' Then  ");
            SQL.AppendLine("		Case SubType  ");
            //SQL.AppendLine("			When '1' Then 'Deposit (Incoming Payment)'  ");
            SQL.AppendLine("			When '1' Then 'Journal Transaction'  ");
            SQL.AppendLine("			When '2' Then 'Journal Voucher'  ");
            SQL.AppendLine("		End ");
            SQL.AppendLine("	When '6' Then 'AR Settlement Reverse' ");
            SQL.AppendLine("	When '7' Then Concat('Voucher (Incoming Payment With Deposit) Before ', @StartFrom) ");
            SQL.AppendLine("End As TransTypeDesc,  ");
            SQL.AppendLine("X1.DocNo, X1.DocDt2, X1.Amt, X3.DueDt  ");
            SQL.AppendLine("From ( ");

            // Type 1 : coa opening balance
            SQL.AppendLine("Select Concat(@StartFrom, '0101') As Period, '1' As TransType, '0' As SubType, Null As SalesInvoiceDocNo, Null As DocDt, ");
            SQL.AppendLine("Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("A.DocNo, A.DocDt As DocDt2,  ");
            SQL.AppendLine("Case When C.AcType='D' Then 1.00 Else -1.00 End * B.Amt As Amt  ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo And B.Amt<>0.00  ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.Yr=@StartFrom  ");
            SQL.AppendLine("And A.CancelInd='N' ");

            SQL.AppendLine("Union All ");

            // Type 2 : sales invoice (voucher (incoming payment) + ar settlement + incoming payment with deposit)
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '2' As TransType, D.SubType, ");
            SQL.AppendLine("B.DocNo As SalesInvoiceDocNo, A.DocDt, B.CtCode, C.DAmt As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt2, IfNull(D.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo=B.JournalDocNo And B.JournalDocNo is Not Null And B.CancelInd='N'  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "B."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And B.CtCode=@CtCode ");
            SQL.AppendLine("Inner Join TblJournalDtl C On A.DocNo=C.DocNo And Left(C.AcNo, @AcNoLength)=@AcNo And C.DAmt>0.00 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select '1' As SubType, T2.DocNo As SalesInvoiceDocNo, T6.DocNo, T6.DocDt As DocDt2, T4.Amt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo Is Not Null ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblJournalDtl T3 On T1.DocNo=T3.DocNo And Left(T3.AcNo, @AcNoLength)=@AcNo And T3.DAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T4 On T2.DocNo=T4.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T5 On T4.DocNo=T5.DocNo And T5.CancelInd='N' And T5.Status='A' And T5.Amt>0.00 And T5.DepositAmt=0.00 ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T6 On T5.VoucherRequestDocNo=T6.VoucherRequestDocNo And T6.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T7 On T6.JournalDocNo=T7.DocNo And T7.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '2' As SubType, T2.DocNo As SalesInvoiceDocNo, T3.DocNo, T3.DocDt As DocDt2, (-1.00*T4.DAmt)+T4.CAmt As Amt ");
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo is Not Null  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblARSHdr T3 On T2.DocNo=T3.SalesInvoiceDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalDtl T4 On T3.JournalDocNo=T4.DocNo And Left(T4.AcNo, @AcNoLength)=@AcNo  ");
            SQL.AppendLine("	    And T2.CtCode=Right(T4.AcNo, Length(T4.AcNo)-@AcNoLength) ");
            SQL.AppendLine("	Inner Join TblJournalHdr T5 On T4.DocNo=T5.DocNo And T5.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select '4' As SubType, T2.DocNo As SalesInvoiceDocNo, T6.DocNo, T6.DocDt As DocDt2, T4.Amt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr T1 ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr T2 On T1.DocNo=T2.JournalDocNo And T2.JournalDocNo is Not Null ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T2."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T2.CtCode=@CtCode ");
            SQL.AppendLine("	Inner Join TblJournalDtl T3 On T1.DocNo=T3.DocNo And Left(T3.AcNo, @AcNoLength)=@AcNo And T3.DAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T4 On T2.DocNo=T4.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T5 On T4.DocNo=T5.DocNo And T5.CancelInd='N' And T5.Status='A' And T5.DepositAmt>0.00 ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T6 On T5.VoucherRequestDocNo=T6.VoucherRequestDocNo And T6.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T7 On T5.JournalDepositDocNo=T7.DocNo And T7.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine(") D On B.DocNo=D.SalesInvoiceDocNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");

            SQL.AppendLine("Union All ");

            // Type 2 : sales invoice tanpa journal (voucher (incoming payment))
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '2' As TransType, B.SubType, ");
            SQL.AppendLine("A.DocNo As SalesInvoiceDocNo, A.DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("B.DocNo, B.DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select '3' As SubType, T1.DocNo As SalesInvoiceDocNo, T4.DocNo, T4.DocDt As DocDt2, T2.Amt As Amt  ");
            SQL.AppendLine("	From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo ");
            SQL.AppendLine("	Inner Join TblIncomingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status='A' And T3.Amt>0.00 ");
            SQL.AppendLine("	Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo And T4.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalHdr T5 On T4.JournalDocNo=T5.DocNo And T5.DocDt<=@Dt2  ");
            SQL.AppendLine("	Where T1.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("    And T1.JournalDocNo Is Null ");
            SQL.AppendLine("    And T1.CancelInd='N'  ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "T1."));
            if (ChkCtCode.Checked) SQL.AppendLine(" And T1.CtCode=@CtCode ");
            SQL.AppendLine(") B On A.DocNo=B.SalesInvoiceDocNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.JournalDocNo Is Null ");
            SQL.AppendLine("And A.CancelInd='N' ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");
            if (Filter != " ") SQL.AppendLine(Filter.Replace("X.", "A."));

            SQL.AppendLine("Union All ");

            // Type 3 : sales invoice before start date (voucher (incoming payment))
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '3' As TransType, '1' As SubType, ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt As DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo=B.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr C On B.DocNo=C.DocNo And C.CancelInd='N' And C.Status='A' And C.Amt>0.00 ");
            SQL.AppendLine("Inner Join TblVoucherHdr D On C.VoucherRequestDocNo=D.VoucherRequestDocNo And D.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblJournalHdr E On D.JournalDocNo=E.DocNo And E.DocDt>=@Dt1 And E.DocDt<=@Dt2  ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And Left(A.DocDt, 4)<@StartFrom ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");

            //AR Settlement before start date

            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select Left(A.DocDt, 6) As Period, '4' As TransType, '1' As SubType, ");
            SQL.AppendLine("    Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("    B.DocNo, B.DocDt As DocDt2, (-1.00*C.DAmt)+C.CAmt As Amt  ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("	Inner Join TblARSHdr B On A.DocNo=B.SalesInvoiceDocNo And B.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblJournalDtl C On B.JournalDocNo=C.DocNo And Left(C.AcNo, @AcNoLength)=@AcNo  ");
            SQL.AppendLine("	    And A.CtCode=Right(C.AcNo, Length(C.AcNo)-@AcNoLength)  ");
            SQL.AppendLine("	Inner Join TblJournalHdr D On C.DocNo=D.DocNo And D.DocDt>=@Dt1 And D.DocDt<=@Dt2  ");
            SQL.AppendLine("    Where A.CancelInd='N'  ");
            SQL.AppendLine("    And Left(A.DocDt, 4)<@StartFrom ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");

            //Incoming Payment With Deposit before start date
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '7' As TransType, '1' As SubType, ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt, A.CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("D.DocNo, D.DocDt As DocDt2, IfNull(B.Amt, 0.00) As Amt  ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo=B.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr C On B.DocNo=C.DocNo And C.CancelInd='N' And C.Status='A' And C.DepositAmt>0.00 ");
            SQL.AppendLine("Inner Join TblJournalHdr D On C.JournalDepositDocNo=D.DocNo And D.DocDt>=@Dt1 And D.DocDt<=@Dt2  ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And Left(A.DocDt, 4)<@StartFrom ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And A.CtCode=@CtCode ");

            //AR Settlement reverse
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select Left(A.DocDt, 6) As Period, '6' As TransType, '1' As SubType, ");
            SQL.AppendLine("    Null As SalesInvoiceDocNo, Null As DocDt, Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("    A.DocNo, A.DocDt As DocDt2, (-1.00*B.DAmt)+B.CAmt As Amt  ");
            SQL.AppendLine("	From TblJournalHdr A ");
            SQL.AppendLine("	Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo  ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("	Inner Join TblARSHdr C On A.DocNo=C.JournalDocNo And C.CancelInd='N' ");
            SQL.AppendLine("	Inner Join TblSalesInvoiceHdr D On C.SalesInvoiceDocNo=D.DocNo And D.CancelInd='N' ");
            SQL.AppendLine("	    And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)<>D.CtCode  ");
            SQL.AppendLine("    Where A.DocDt Between @Dt1 And @Dt2 ");

            SQL.AppendLine("Union All ");

            //Type 4 : journal + journal voucher
            SQL.AppendLine("Select Left(A.DocDt, 6) As Period, '5' As TransType,  ");
            SQL.AppendLine("Case A.MenuCode  ");
            //SQL.AppendLine("	When '0102023003' Then '1' ");
            SQL.AppendLine("	When '0102010106' Then '1' ");
            SQL.AppendLine("	When '0102010107' Then '2' ");
            SQL.AppendLine("End As SubType,  ");
            SQL.AppendLine("Null As SalesInvoiceDocNo, Null As DocDt,  ");
            SQL.AppendLine("Right(B.AcNo, Length(B.AcNo)-@AcNoLength) As CtCode, 0.00 As SalesInvoiceAmt,  ");
            SQL.AppendLine("A.DocNo, A.DocDt As DocDt2,  ");
            SQL.AppendLine("Case When B.DAmt<>0.00 Then  ");
            SQL.AppendLine("	Case When C.AcType='D' Then 1.00 Else -1.00 End * B.DAmt ");
            SQL.AppendLine("Else  ");
            SQL.AppendLine("	Case When C.AcType='D' Then -1.00 Else 1.00 End * B.CAmt ");
            SQL.AppendLine("End As Amt  ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, @AcNoLength)=@AcNo And (B.DAmt<>0.00 Or B.CAmt<>0.00) ");
            if (ChkCtCode.Checked) SQL.AppendLine(" And Right(B.AcNo, Length(B.AcNo)-@AcNoLength)=@CtCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");
            //SQL.AppendLine("And A.MenuCode In ('0102023003', '0102010106', '0102010107') ");
            SQL.AppendLine("And A.MenuCode In ('0102010106', '0102010107') ");
            SQL.AppendLine(") X1");
            SQL.AppendLine("Inner Join TblCustomer X2 On X1.CtCode=X2.CtCode ");
            SQL.AppendLine("Left Join TblSalesInvoiceHdr X3 On X1.SalesInvoiceDocNo=X3.DocNo ");
            SQL.AppendLine("Order By X2.CtName, X1.CtCode, X1.TransType, X1.DocDt,  X1.SalesInvoiceDocNo, X1.SubType, X1.DocDt2;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "Period",
                        "Customer Code",
                        "Customer Name",
                        "Invoice#",
                        "Invoice Date",

                        //6-10
                        "Due Date",
                        "Invoice Amount",
                        "Paid",
                        "Type",
                        "Type",

                        //11-15
                        "Document#",
                        "Date",
                        "Amount",
                        "Balance",
                        "0-30 Days",

                        //16-20
                        "31-60 Days",
                        "61-90 Days",
                        "91-120 Days",
                        "121-150 Days",
                        "151-180 Days",

                        //21-23
                        "> 180 Days",
                        "Unknown",
                        "Different"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 200, 180, 100, 
                        
                        //6-10
                        100, 180, 180, 100, 280, 

                        //11-15
                        150, 100, 150, 150, 150,

                        //16-20
                        150, 150, 150, 150, 150,

                        //21-23
                        150, 150, 150
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 9, 23 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 9, 23 }, !ChkHideInfoInGrd.Checked);
        }

        #region Old Code
        //override protected void SetSQL()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select T1.Type, T2.CtCode, T2.CtName, T1.DocDt, IfNull(T1.Amount, 0.00) As Amount, ");
        //    SQL.AppendLine("T1.InvoiceDocNo, T1.JournalDocNo, T1.Period, T1.Remark, T1.Range ");
        //    SQL.AppendLine("From ");
        //    SQL.AppendLine("( ");
        //    SQL.AppendLine("    Select '1' As Type, B.CtCode, B.DocDt, (B.TotalAmt+B.TotalTax) As Amount, ");
        //    SQL.AppendLine("    B.DocNo As InvoiceDocNo, A.DocNo As JournalDocNo, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
        //    SQL.AppendLine("    A.Remark, DATEDIFF((LEFT(CurrentDateTime(),8)), B.DocDt) As `Range` ");
        //    SQL.AppendLine("    From TblJournalHdr A ");
        //    SQL.AppendLine("    Inner Join TblSalesInvoiceHdr B On A.DocNo = B.JournalDocNo ");
        //    SQL.AppendLine("        And B.Status = 'A'  AND ProcessInd <> 'F' ");
        //    SQL.AppendLine("    Where Left(B.DocDt, 6) Between concat(@Yr, '01') and  @YrMth ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("    Select '2' As Type, SUBSTRING_INDEX(B.AcNo,'.',-1) As CtCode, A.DocDt, ");
        //    SQL.AppendLine("    Case ");
        // SQL.AppendLine("        When B.DAmt > 0.00 Then B.DAmt ");
        // SQL.AppendLine("        When B.CAmt > 0.00 Then B.CAmt * -1 ");
        //    SQL.AppendLine("    End As Amount, Null As InvoiceDocNo, A.DocNo As JournalDocNo, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
        //    SQL.AppendLine("    A.Remark, 0.00 As `Range` ");
        //    SQL.AppendLine("    From TblJournalHdr A ");
        //    SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
        // SQL.AppendLine("        And B.AcNo Like '1.1.2.1.%' ");
        //    SQL.AppendLine("    Where A.DocNo Not In ");
        //    SQL.AppendLine("    ( ");
        //    SQL.AppendLine("        Select JournalDocNo ");
        //    SQL.AppendLine("        From TblSalesInvoiceHdr ");
        //    SQL.AppendLine("        Where JournalDocNo Is Not Null And Status = 'A'  AND ProcessInd = 'F' ");
        //    SQL.AppendLine("        And Left(DocDt, 6) Between concat(@Yr, '01') and  @YrMth  ");
        //    SQL.AppendLine("    ) ");
        //    SQL.AppendLine("    And Left(A.DocDt, 6) Between concat(@Yr, '01') and  @YrMth ");

        //    SQL.AppendLine("UNION ALL ");
        //    SQL.AppendLine("SELECT ");
        //    SQL.AppendLine("'3' As TYPE, D.CtCode, CONCAT(A.yr, '0101'),   ");
        //    SQL.AppendLine("Amt, Null As InvoiceDocNo, A.DocNo As OBDocNo, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,   ");
        //    SQL.AppendLine("A.Remark, 0.00 As `Range`   ");
        //    SQL.AppendLine("From tblcoaopeningbalanceHdr A   ");
        //    SQL.AppendLine("Inner Join tblcoaOpeningbalanceDtl B On A.DocNo = B.DocNo ");
        //    SQL.AppendLine("INNER JOIN tblcoa C ON B.AcNo = C.AcNo ");
        //    SQL.AppendLine("INNER JOIN tblcustomer D ON Right(C.AcNo, (SELECT Length(MAX(ctCode)) FROM tblcustomer)) = D.CtCode ");   
        //    SQL.AppendLine("And A.CancelInd = 'N'   ");
        //    SQL.AppendLine("WHERE A.YR= LEFT(@YrMth, 4)  AND C.AcNo LIKE '1.1.2.1%' ");
        //    SQL.AppendLine(") T1 ");
        //    SQL.AppendLine("Left Join TblCustomer T2 On T1.CtCode = T2.CtCode ");

        //    mSQL = SQL.ToString();
        //}

        //override protected void ShowData()
        //{
        //    Sm.ClearGrd(Grd1, false);
        //    if (Sm.IsLueEmpty(LueYr, "Year") ||
        //       Sm.IsLueEmpty(LueMth, "Month") ||
        //       Sm.IsLueEmpty(LueStartFrom, "Start From")
        //       ) return;

        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;

        //        var Filter = " ";
        //        var cm = new MySqlCommand();

        //        Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
        //        if (ChkCtCode.Checked) Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

        //        Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
        //        Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
        //        Sm.CmParam<String>(ref cm, "@StartFrom", Sm.GetLue(LueStartFrom));
        //        Sm.CmParam<String>(ref cm, "@AcNo", mCustomerAcNoAR);
        //        Sm.CmParam<String>(ref cm, "@AcNoLength", mCustomerAcNoAR.Length.ToString());
        //        Sm.CmParam<String>(ref cm, "@Dt1", string.Concat(Sm.GetLue(LueStartFrom), "0101"));
        //        Sm.CmParam<String>(ref cm, "@Dt2", Sm.GetValue("Select DATE_FORMAT(Date_Add(Date_Add((Select Str_To_Date(Concat('" + Sm.GetLue(LueYr) + "','" + Sm.GetLue(LueMth) + "', '01'), '%Y%m%d')), Interval 1 Month), Interval -1 Day), '%Y%m%d');"));


        //        Sm.ShowDataInGrid(
        //                ref Grd1, ref cm, GetSQL(Filter),
        //                // mSQL + Filter + " Order By T2.CtName, T1.Period; ",
        //                new string[]
        //                { 
        //                    //0
        //                    "CtName", 

        //                    //1-5
        //                    "DocDt", "Amount", "InvoiceDocNo", "JournalDocNo", "Period",

        //                    //6-7
        //                    "Remark", "Range", "Type"
        //                },
        //                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
        //                {
        //                    Grd.Cells[Row, 0].Value = Row + 1;
        //                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
        //                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
        //                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
        //                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
        //                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
        //                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
        //                    Grd.Cells[Row, 7].Value = 0;
        //                    Grd.Cells[Row, 8].Value = 0;
        //                    Grd.Cells[Row, 9].Value = 0;
        //                    Grd.Cells[Row, 10].Value = 0;
        //                    Grd.Cells[Row, 11].Value = 0;
        //                    Grd.Cells[Row, 12].Value = 0;
        //                    Grd.Cells[Row, 13].Value = 0;
        //                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 2);
        //                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 6);
        //                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 7);
        //                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 8);
        //                }, true, false, false, false
        //            );
        //        ProcessAgingAR();
        //        iGSubtotalManager.BackColor = Color.LightSalmon;
        //        iGSubtotalManager.ShowSubtotalsInCells = true;
        //        iGSubtotalManager.HideSubtotals(Grd1);
        //        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 7, 8, 9, 10, 11, 12, 13, 14 });
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //    finally
        //    {
        //        Sm.FocusGrd(Grd1, 0, 0);
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        #endregion

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") ||
               Sm.IsLueEmpty(LueMth, "Month") ||
               Sm.IsLueEmpty(LueStartFrom, "Start From")
               ) return;

            Cursor.Current = Cursors.WaitCursor;
            var l = new List<Data>();
            var l2 = new List<Data2>();

            try
            {
                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                    Process4(ref l, ref l2);
                    l.Clear();
                    if (l2.Count > 0)
                        Process5(ref l2);
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                l2.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<Data> l)
        {
            var Filter = " ";
            var cm = new MySqlCommand();

            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
            if (ChkCtCode.Checked) Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@StartFrom", Sm.GetLue(LueStartFrom));
            Sm.CmParam<String>(ref cm, "@AcNo", mCustomerAcNoAR);
            Sm.CmParam<String>(ref cm, "@AcNoLength", mCustomerAcNoAR.Length.ToString());
            Sm.CmParam<String>(ref cm, "@Dt1", string.Concat(Sm.GetLue(LueStartFrom), "0101"));
            Sm.CmParam<String>(ref cm, "@Dt2", Sm.GetValue("Select DATE_FORMAT(Date_Add(Date_Add((Select Str_To_Date(Concat('" + Sm.GetLue(LueYr) + "','" + Sm.GetLue(LueMth) + "', '01'), '%Y%m%d')), Interval 1 Month), Interval -1 Day), '%Y%m%d');"));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = GetSQL(Filter);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    "Period",
                    "SalesInvoiceDocNo", "DocDt",  "CtName", "CtCode", "SalesInvoiceAmt",
                    "TransType",  "TransTypeDesc",  "DocNo", "DocDt2", "Amt",
                    "DueDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data()
                        {
                            Period = Sm.DrStr(dr, c[0]),
                            SalesInvoiceDocNo = Sm.DrStr(dr, c[1]),
                            DocDt = Sm.DrStr(dr, c[2]),
                            CtName = Sm.DrStr(dr, c[3]),
                            CtCode = Sm.DrStr(dr, c[4]),
                            SalesInvoiceAmt = Sm.DrDec(dr, c[5]),
                            TransType = Sm.DrStr(dr, c[6]),
                            TransTypeDesc = Sm.DrStr(dr, c[7]),
                            DocNo = Sm.DrStr(dr, c[8]),
                            DocDt2 = Sm.DrStr(dr, c[9]),
                            Amt = Sm.DrDec(dr, c[10]),
                            DueDt = Sm.DrStr(dr, c[11]),
                            Balance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Data> l)
        {
            var SalesInvoiceDocNo = string.Empty;
            var SalesInvoiceAmt = 0m;
            var Amt = 0m;

            foreach (var x in l)
            {
                if (x.TransType == "2" && Sm.CompareStr(SalesInvoiceDocNo, x.SalesInvoiceDocNo))
                    x.SalesInvoiceAmt = SalesInvoiceAmt - Amt;
                SalesInvoiceDocNo = x.SalesInvoiceDocNo;
                Amt = x.Amt;
                SalesInvoiceAmt = x.SalesInvoiceAmt;
            }
        }

        private void Process3(ref List<Data> l)
        {
            foreach (var x in l)
            {
                if (x.TransType == "1") //Opening Balance
                    x.Balance = x.Amt;
                else
                {
                    if (x.TransType == "3" || x.TransType == "4" || x.TransType == "6" || x.TransType == "7")
                        x.Balance = 0m;
                    else
                    {
                        if (x.TransType == "5")
                            x.Balance = x.Amt;
                        else
                            x.Balance = x.SalesInvoiceAmt - x.Amt;
                    }
                }
            }
        }

        private void Process4(ref List<Data> l1, ref List<Data2> l2)
        {
            var SalesInvoiceDocNo = string.Empty;
            var l = new List<Data2>();

            foreach (var x in l1)
            {
                if (x.SalesInvoiceDocNo.Length > 0)
                {
                    if (Sm.CompareStr(SalesInvoiceDocNo, x.SalesInvoiceDocNo))
                    {
                        foreach (var i in l.Where(w => Sm.CompareStr(x.SalesInvoiceDocNo, w.SalesInvoiceDocNo)))
                        {
                            i.Amt = i.Amt + x.Amt;
                        }
                    }
                    else
                        l.Add(new Data2()
                        {
                            Period = x.Period,
                            SalesInvoiceDocNo = x.SalesInvoiceDocNo,
                            DocDt = x.DocDt,
                            CtName = x.CtName,
                            CtCode = x.CtCode,
                            SalesInvoiceAmt = x.SalesInvoiceAmt,
                            TransType = string.Empty,
                            TransTypeDesc = string.Empty,
                            DocNo = string.Empty,
                            DocDt2 = string.Empty,
                            Amt = x.Amt,
                            DueDt = x.DueDt,
                            Balance = 0m,
                            Range0_30 = 0m,
                            Range31_60 = 0m,
                            Range61_90 = 0m,
                            Range91_120 = 0m,
                            Range121_150 = 0m,
                            Range151_180 = 0m,
                            Range181 = 0m,
                            RangeUnknown = 0m,
                            Difference = 0m
                        });
                }
                else
                {
                    l.Add(new Data2()
                    {
                        Period = x.Period,
                        SalesInvoiceDocNo = x.SalesInvoiceDocNo,
                        DocDt = x.DocDt,
                        CtName = x.CtName,
                        CtCode = x.CtCode,
                        SalesInvoiceAmt = x.SalesInvoiceAmt,
                        TransType = x.TransType,
                        TransTypeDesc = x.TransTypeDesc,
                        DocNo = x.DocNo,
                        DocDt2 = x.DocDt2,
                        Amt = x.Amt,
                        DueDt = x.DueDt,
                        Balance = 0m,
                        Range0_30 = 0m,
                        Range31_60 = 0m,
                        Range61_90 = 0m,
                        Range91_120 = 0m,
                        Range121_150 = 0m,
                        Range151_180 = 0m,
                        Range181 = 0m,
                        RangeUnknown = 0m,
                        Difference = 0m
                    });
                }
                SalesInvoiceDocNo = x.SalesInvoiceDocNo;
            }

            foreach (var x in l)
            {
                if (x.SalesInvoiceDocNo.Length > 0)
                    x.Balance = x.SalesInvoiceAmt - x.Amt;
                else
                    x.Balance = x.Amt;
            }

            foreach (var x in l.Where(w => w.Balance != 0m))
            {
                l2.Add(new Data2()
                {
                    Period = x.Period,
                    SalesInvoiceDocNo = x.SalesInvoiceDocNo,
                    DocDt = x.DocDt,
                    CtName = x.CtName,
                    CtCode = x.CtCode,
                    SalesInvoiceAmt = x.SalesInvoiceAmt,
                    TransType = x.TransType,
                    TransTypeDesc = x.TransTypeDesc,
                    DocNo = x.DocNo,
                    DocDt2 = x.DocDt2,
                    Amt = x.Amt,
                    DueDt = x.DueDt,
                    Balance = x.Balance,
                    Range0_30 = 0m,
                    Range31_60 = 0m,
                    Range61_90 = 0m,
                    Range91_120 = 0m,
                    Range121_150 = 0m,
                    Range151_180 = 0m,
                    Range181 = 0m,
                    RangeUnknown = 0m,
                    Difference = 0m
                });
            }

            l.Clear();

            var Dt = Sm.ServerCurrentDate();
            var Dt1 = new DateTime(
                      Int32.Parse(Dt.Substring(0, 4)),
                      Int32.Parse(Dt.Substring(4, 2)),
                      Int32.Parse(Dt.Substring(6, 2)),
                      0, 0, 0
                      );
            DateTime Dt2 = Dt1;
            TimeSpan Aging = Dt1 - Dt2;
            int days = 0;

            foreach (var x in l2.Where(w => w.Balance != 0m))
            {
                if (x.DueDt.Length > 0)
                {
                    Dt2 = new DateTime(
                      Int32.Parse(x.DueDt.Substring(0, 4)),
                      Int32.Parse(x.DueDt.Substring(4, 2)),
                      Int32.Parse(x.DueDt.Substring(6, 2)),
                      0, 0, 0
                      );
                    Aging = Dt1 - Dt2;
                    days = (int)Math.Ceiling(Aging.TotalDays);

                    if (days >= 0 && days <= 30) x.Range0_30 = x.Balance;
                    if (days >= 31 && days <= 60) x.Range31_60 = x.Balance;
                    if (days >= 61 && days <= 90) x.Range61_90 = x.Balance;
                    if (days >= 91 && days <= 120) x.Range91_120 = x.Balance;
                    if (days >= 121 && days <= 150) x.Range121_150 = x.Balance;
                    if (days >= 151 && days <= 180) x.Range151_180 = x.Balance;
                    if (days >= 181) x.Range181 = x.Balance;

                    x.Difference = days;
                }
                else
                    x.RangeUnknown = x.Balance;
            }
        }

        private void Process5(ref List<Data2> l)
        {
            iGRow r;
            int n = 0;

            Grd1.BeginUpdate();
            foreach (var x in l)
            {
                r = Grd1.Rows.Add();
                n++;
                r.Cells[0].Value = n;
                r.Cells[1].Value = x.Period;
                r.Cells[2].Value = x.CtCode;
                r.Cells[3].Value = x.CtName;
                r.Cells[4].Value = x.SalesInvoiceDocNo;
                if (x.DocDt.Length>0) r.Cells[5].Value = Sm.ConvertDate(x.DocDt);
                if (x.DueDt.Length > 0) r.Cells[6].Value = Sm.ConvertDate(x.DueDt);
                r.Cells[7].Value = x.SalesInvoiceAmt;
                r.Cells[8].Value = (x.SalesInvoiceDocNo.Length>0)?x.Amt:0m;
                r.Cells[9].Value = x.TransType;
                r.Cells[10].Value = x.TransTypeDesc;
                r.Cells[11].Value = x.DocNo;
                if (x.DocDt2.Length > 0) r.Cells[12].Value = Sm.ConvertDate(x.DocDt2);
                r.Cells[13].Value = (x.SalesInvoiceDocNo.Length < 0) ? x.Amt : 0m;
                r.Cells[14].Value = x.Balance;
                r.Cells[15].Value = x.Range0_30;
                r.Cells[16].Value = x.Range31_60;
                r.Cells[17].Value = x.Range61_90;
                r.Cells[18].Value = x.Range91_120;
                r.Cells[19].Value = x.Range121_150;
                r.Cells[20].Value = x.Range151_180;
                r.Cells[21].Value = x.Range181;
                r.Cells[22].Value = x.RangeUnknown;
                r.Cells[23].Value = x.Difference;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Grd1.EndUpdate();
        }

        //private void ProcessAgingAR()
        //{
        //    for (int i = 0; i < Grd1.Rows.Count; i++)
        //    {
        //        decimal num = Sm.GetGrdDec(Grd1, i, 16);
        //        if (Sm.GetGrdStr(Grd1, i, 17) == "1") //Sales Invoice Doc
        //        {
        //            if (num >= 0 && num <= 30) Grd1.Cells[i, 7].Value = Sm.GetGrdDec(Grd1, i, 3);
        //            if (num >= 31 && num <= 60) Grd1.Cells[i, 8].Value = Sm.GetGrdDec(Grd1, i, 3);
        //            if (num >= 61 && num <= 90) Grd1.Cells[i, 9].Value = Sm.GetGrdDec(Grd1, i, 3);
        //            if (num >= 91 && num <= 120) Grd1.Cells[i, 10].Value = Sm.GetGrdDec(Grd1, i, 3);
        //            if (num >= 121 && num <= 150) Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 3);
        //            if (num >= 151 && num <= 180) Grd1.Cells[i, 12].Value = Sm.GetGrdDec(Grd1, i, 3);
        //            if (num > 180) Grd1.Cells[i, 13].Value = Sm.GetGrdDec(Grd1, i, 3);
        //        }
        //        else //Journal Doc (Without SLI JN)
        //        {
        //            Grd1.Cells[i, 13].Value = Sm.GetGrdDec(Grd1, i, 3);
        //        }
        //    }
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Invoice#");
        }

        #endregion

        #endregion

        #region Class

        private class Data
        {
            public string Period { get; set; }
            public string SalesInvoiceDocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string CtCode { get; set; }
            public decimal SalesInvoiceAmt { get; set; }
            public string TransType { get; set; }
            public string TransTypeDesc { get; set; }
            public string DocNo { get; set; }
            public string DocDt2 { get; set; }
            public decimal Amt { get; set; }
            public decimal Balance { get; set; }
            public string DueDt { get; set; }
        }

        private class Data2
        {
            public string Period { get; set; }
            public string SalesInvoiceDocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string CtCode { get; set; }
            public decimal SalesInvoiceAmt { get; set; }
            public string TransType { get; set; }
            public string TransTypeDesc { get; set; }
            public string DocNo { get; set; }
            public string DocDt2 { get; set; }
            public decimal Amt { get; set; }
            public decimal Balance { get; set; }
            public string DueDt { get; set; }
            public decimal Range0_30 { get; set; }
            public decimal Range31_60 { get; set; }
            public decimal Range61_90 { get; set; }
            public decimal Range91_120 { get; set; }
            public decimal Range121_150 { get; set; }
            public decimal Range151_180 { get; set; }
            public decimal Range181 { get; set; }
            public decimal RangeUnknown { get; set; }
            public decimal Difference { get; set; }
        }

        #endregion
    }
}
