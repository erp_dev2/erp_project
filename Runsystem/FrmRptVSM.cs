﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptVSM : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptVSM(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                SetSQL();
                SetGrd();

                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                base.FrmLoad(sender, e);
                BtnChart.Visible = (Sm.GetValue("Select IfNull(ParValue, '') from tblparameter where ParCode='ChartAvailable'") == "Yes");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.CreateDt As DocDt, J.OptDesc As ItCatName, K.TTName, ");
	        SQL.AppendLine("Concat(A.DocDateBf, A.DocTimeBf) As BeforeUnloading, ");
	        SQL.AppendLine("Concat(A.DocDateAf, A.DocTimeAf) As AfterUnloading, ");
            SQL.AppendLine("B.CreateDt As LegalDocVerify,  D.Received, D.StartUnloading, D.FinishedUnloading, E.Opname, F.CreateDt As Verify, G.CreateDt As PI, I.CreateDt As Voucher ");
	        SQL.AppendLine("From TblLoadingQueue A ");
	        SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo And B.CancelInd='N' ");
	        SQL.AppendLine("Left Join ( ");
		    SQL.AppendLine("    Select LegalDocVerifyDocNo,  ");
            SQL.AppendLine("    Min(CreateDt) Received, Min(Concat(UnloadStartDt, UnloadStartTm)) StartUnloading, Max(Concat(UnloadEndDt, UnloadEndTm)) FinishedUnloading ");
		    SQL.AppendLine("    From TblRecvRawMaterialHdr ");
		    SQL.AppendLine("    Where CancelInd='N' Group By LegalDocVerifyDocNo ");
	        SQL.AppendLine(") D On B.Docno=D.LegalDocVerifyDocNo ");
	        SQL.AppendLine("Left Join ( ");
		    SQL.AppendLine("    Select LegalDocVerifyDocNo,Min(CreateDt) Opname ");
		    SQL.AppendLine("    From TblRawMaterialOpnameHdr ");
		    SQL.AppendLine("    Where CancelInd='N' Group By LegalDocVerifyDocNo ");
	        SQL.AppendLine(") E On B.Docno=E.LegalDocVerifyDocNo "); 
	        SQL.AppendLine("Left Join TblRawMaterialVerify F On B.Docno=F.LegalDocVerifyDocNo And F.CancelInd='N' ");
	        SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr G On F.Docno=G.RawMaterialVerifyDocNo And G.CancelInd='N' "); 
	        SQL.AppendLine("Left Join TblVoucherRequestHdr H On G.VoucherRequestDocno=H.DocNo And H.CancelInd='N' ");
	        SQL.AppendLine("Left Join TblVoucherHdr I On H.VoucherDocno=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Left Join TblOption J On A.ItCat=J.OptCode And J.OptCat='LoadingItemCat' ");
	        SQL.AppendLine("Left Join TblTransportType K On A.TTCode=K.TTCode ");
            SQL.AppendLine("Where A.ItCat In ('A', 'B', 'C') ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Queue Number",
                        "Queue Date",
                        "Category",
                        "Transport",
                        "Timbang 1 vs"+Environment.NewLine+"Verifikasi"+Environment.NewLine+"Dokumen",
                        
                        //6-10
                        "Timbang 1 vs"+Environment.NewLine+"Mulai"+Environment.NewLine+"Bongkar",
                        "Timbang 1 vs"+Environment.NewLine+"Selesai"+Environment.NewLine+"Bongkar",

                        "Mulai"+Environment.NewLine+"Bongkar vs"+Environment.NewLine+"Selesai"+Environment.NewLine+"Bongkar",

                        "Timbang 1 vs"+Environment.NewLine+"Timbang 2",
                        "Timbang 1 vs"+Environment.NewLine+"Nota"+Environment.NewLine+"Selesai",
                        
                        //11-15
                        "Timbang 1 vs"+Environment.NewLine+"Opname",
                        "Timbang 1 vs"+Environment.NewLine+"Verifikasi"+Environment.NewLine+"Terima",
                        "Timbang 1 vs"+Environment.NewLine+"Purchase"+Environment.NewLine+"Invoice",

                        "Mulai"+Environment.NewLine+"Bongkar vs"+Environment.NewLine+"Verifikasi"+Environment.NewLine+"Terima",

                        "Timbang 1 vs"+Environment.NewLine+"Voucher",
                        
                        //16-17
                        "Verifikasi"+Environment.NewLine+"Terima vs"+Environment.NewLine+"Purchase"+Environment.NewLine+"Invoice",
                        "Verifikasi"+Environment.NewLine+"Terima vs"+Environment.NewLine+"Voucher"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 80, 100, 100, 100, 
                        
                        //6-10
                        100, 100, 100, 100, 100,    
                        
                        //11-15
                        100, 100, 100, 100, 100,

                        //16-17
                        100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.CreateDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt, A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "ItCatName", "TTName", "BeforeUnloading", "LegalDocVerify",     

                            //6-10
                            "StartUnloading", "FinishedUnloading", "AfterUnloading", "Received", "Opname",  
                            
                            //11-13
                            "Verify", "PI", "Voucher"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            SetGrdTimeDiff(Grd, Row, 5, ref dr, ref c, 4, 5);
                            SetGrdTimeDiff(Grd, Row, 6, ref dr, ref c, 4, 6);
                            SetGrdTimeDiff(Grd, Row, 7, ref dr, ref c, 4, 7);
                            SetGrdTimeDiff(Grd, Row, 8, ref dr, ref c, 6, 7);
                            SetGrdTimeDiff(Grd, Row, 9, ref dr, ref c, 4, 8);
                            SetGrdTimeDiff(Grd, Row, 10, ref dr, ref c, 4, 9);
                            SetGrdTimeDiff(Grd, Row, 11, ref dr, ref c, 4, 10);
                            SetGrdTimeDiff(Grd, Row, 12, ref dr, ref c, 4, 11);
                            SetGrdTimeDiff(Grd, Row, 13, ref dr, ref c, 4, 12);
                            SetGrdTimeDiff(Grd, Row, 14, ref dr, ref c, 6, 11);
                            SetGrdTimeDiff(Grd, Row, 15, ref dr, ref c, 4, 13);
                            SetGrdTimeDiff(Grd, Row, 16, ref dr, ref c, 11, 12);
                            SetGrdTimeDiff(Grd, Row, 17, ref dr, ref c, 11, 13);
                        }, true, false, false, false
                    );
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetGrdTimeDiff(iGrid Grd, int Row, int Col, ref MySqlDataReader dr, ref int[] c, int Col1, int Col2)
        { 
            Grd.Cells[Row, Col].Value = ComputeTimeDiff(
                (dr[c[Col1]] == DBNull.Value) ? "" : dr.GetString(c[Col1]), 
                (dr[c[Col2]] == DBNull.Value) ? "" : dr.GetString(c[Col2])
                );
        }

        private string ComputeTimeDiff(string Param1, string Param2)
        {
            string Value = string.Empty;

            if (Param1.Length != 0 && Param2.Length != 0)
            {
                int
                    Yr1 = int.Parse(Param1.Substring(0, 4)),
                    Mth1 = int.Parse(Param1.Substring(4, 2)),
                    Day1 = int.Parse(Param1.Substring(6, 2)),
                    Hr1 = int.Parse(Param1.Substring(8, 2)),
                    Mn1 = int.Parse(Param1.Substring(10, 2)),

                    Yr2 = int.Parse(Param2.Substring(0, 4)),
                    Mth2 = int.Parse(Param2.Substring(4, 2)),
                    Day2 = int.Parse(Param2.Substring(6, 2)),
                    Hr2 = int.Parse(Param2.Substring(8, 2)),
                    Mn2 = int.Parse(Param2.Substring(10, 2));

                System.DateTime
                    Dt1 = new System.DateTime(Yr1, Mth1, Day1, Hr1, Mn1, 0),
                    Dt2 = new System.DateTime(Yr2, Mth2, Day2, Hr2, Mn2, 0);


                System.TimeSpan Diff = Dt2.Subtract(Dt1);

                //double
                //    D = Math.Floor(Diff.TotalDays) % 365,
                //    H = Math.Floor(Diff.TotalHours) % 24,
                //    M = Math.Floor(Diff.TotalMinutes) % 60;

                //if (D != 0) Value = D + " Days";
                //if (H != 0) Value += " " + H + " hours";
                //if (M != 0) Value += " " + M + " minutes";
                
                Value = String.Format("{0:#,##0.00}", (Math.Floor(Diff.TotalMinutes) / 60));
            }

            return Value.Trim();
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Queue date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
