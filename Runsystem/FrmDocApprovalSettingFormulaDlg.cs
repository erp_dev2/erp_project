﻿#region Update
/*
    13/07/2021 [TKG/PHT] New apps
    16/07/2021 [TKG/PHT] judul level diubah menjadi Setting level
    13/08/2021 [TKG/PHT] level tidak menggunakan link ke table level lagi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalSettingFormulaDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDocApprovalSettingFormula mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalSettingFormulaDlg(FrmDocApprovalSettingFormula FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.SetLueDocType(ref LueDocType);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, "N");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",
                        "Setting Level",
                        "Site",
                        "Department",
                        "Employee's Level",
                        
                        //6
                        "User"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 150, 250, 250, 200, 
                        
                        //6-9
                        200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.Level, B.SiteName, C.DeptName, A.LevelCode, A.UserCode, ");
            SQL.AppendLine("A.SiteCode, A.DeptCode ");
            SQL.AppendLine("From TblDocApprovalSetting A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblLevelHdr D On A.LevelCode=D.LevelCode ");
            SQL.AppendLine("Where Find_In_Set(A.DocType, ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='ApprovalTypeForDocApprovalFormula' And ParValue Is Not Null ");
            SQL.AppendLine(")) ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "A.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocType, B.SiteName, C.DeptName;",
                        new string[] 
                        { 
                            //0
                            "DocType",

                            //1-5
                            "Level", 
                            "SiteName",
                            "DeptName", 
                            "LevelCode",
                            "UserCode"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtTypeApproval.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtLevel.Text = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtDASSiteCode.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtDASDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtDASLevelCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(mFrmParent.SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Approval type");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

    }
}
