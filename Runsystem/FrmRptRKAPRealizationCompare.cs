﻿#region Update
/*
    27/07/2022 [DITA/SIER] New Reporting   

 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRKAPRealizationCompare : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mYr = string.Empty,
            mYr2 = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5";
        private int mMth = 0;
        private bool mIsFilterBySite = false,
            mIsPrintRptFicoSettingUseParentheses = false;

        #endregion

        #region Constructor

        public FrmRptRKAPRealizationCompare(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion
        
        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueFicoFormula(ref LueTypeCode);
                SetLuePrintoutFormula(ref LuePrintoutFormatCode);
                mYr = Sm.GetLue(LueYr);
                mMth =Convert.ToInt32(Sm.GetLue(LueMth));
                mYr2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
                SetGrd();
                LueYr.Focus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter', 'IsPLComputeFromFicoSetting','IsRptTBCalculateFicoNetIncome', 'FormulaForComputeProfitLoss', ");
            SQL.AppendLine("'AcNoForCurrentEarning', 'AcNoForCurrentEarning2', 'AcNoForCurrentEarning3','AcNoForIncome', 'AcNoForIncome2', ");
            SQL.AppendLine("'IsFicoUseMultiEntityFilter', 'IsFicoUseCOALevelFilter', 'IsAccountingRptUseJournalPeriod','IsRptTrialBalanceUseProfitCenter', 'PrintFicoSettingLastYearBasedOnFilterMonth', ");
            SQL.AppendLine("'IsReportingFilterByEntity', 'IsEntityMandatory', 'IsCOAAssetUseStartYr','CurrentEarningFormulaType', 'MaxAccountCategory', ");
            SQL.AppendLine("'COAAssetAcNo', 'AccountingRptStartFrom', 'AcNoForActiva','AcNoForPassiva', 'AcNoForCapital', ");
            SQL.AppendLine("'AcNoForCost', 'COAAssetStartYr' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            

                            //string
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            


                        }
                    }
                }
                dr.Close();
            }
           
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            string[] arrMth = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Account",
                        "Description",
                        "Realization"+Environment.NewLine+"Up To "+arrMth[mMth-1]+" "+mYr2+Environment.NewLine+"[1]",
                        "Realization"+Environment.NewLine+mYr2+Environment.NewLine+"[2]",
                        "RKAP"+Environment.NewLine+mYr+Environment.NewLine+"[3]",
                        
                        
                        //6-10
                        "RKAP"+Environment.NewLine+"Up To "+arrMth[mMth-1]+" "+mYr+Environment.NewLine+"[4]",
                        "Realization"+Environment.NewLine+"Up To "+arrMth[mMth-1]+" "+mYr+Environment.NewLine+"[5]",
                        "%"+Environment.NewLine+"[5/1]",
                        "%"+Environment.NewLine+"[5/2]",
                        "%"+Environment.NewLine+"[5/3]",

                        //11
                        "%"+Environment.NewLine+"[5/4]",

                    },
                    new int[]
                    {
                        //0
                        50,
                        //1-5
                        150, 180, 150, 150, 150, 
                        //6-10
                        150, 150, 150, 150, 150, 
                        //11
                        150,
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10 , 11 }, 2);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                (Sm.IsLueEmpty(LueTypeCode, "Type")) 
                ) return;


            Cursor.Current = Cursors.WaitCursor;

            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();
            var lCBPTemp = new List<CBP>();
            string mListAcNo = string.Empty, mUsedAcNo = string.Empty;

            mListAcNo = Sm.GetValue("SET SESSION group_concat_max_len = 1000000;Select Group_Concat(Distinct Replace(Replace(AcNo, '+', ','), '-', ',')) From TblRptFicoSettingDtl Where RptFicoCode = @Param And AcNo Is Not Null; ", Sm.GetLue(LueTypeCode));

            mYr = Sm.GetLue(LueYr);
            mMth = Convert.ToInt32(Sm.GetLue(LueMth));
            mYr2 = Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1);
            var Mth = Sm.GetLue(LueMth);
            SetGrd();
            GetAllCOA(ref lCOATemp, mListAcNo);
            if (lCOATemp.Count > 0)
            {
                foreach (var i in lCOATemp)
                {
                    if (mUsedAcNo.Length > 0) mUsedAcNo += ",";
                    mUsedAcNo += i.AcNo;
                }
            }

            GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, mYr, mYr2);
            GetAllJournal(ref lJournalTemp, mUsedAcNo, Mth, mYr,mYr2);
            GetAllJournalCurrentMonth(ref lJournalCurrentMonthTemp, mUsedAcNo, Mth, mYr, mYr2);
            GetAllCBP(ref lCBPTemp, mUsedAcNo, Mth, mYr);

            try
            {
                #region Grid 1

                Sm.ClearGrd(Grd1, true);
                var lCOA = new List<COA>();

                var lRptFicoSetting = new List<RptFicoSetting>();
                var lRptFicoSetting2 = new List<RptFicoSetting>();
                var lRptFicoSetting3 = new List<COAFicoSetting>();
                var lRptFicoSetting4 = new List<RptFicoSetting2>();


                foreach (var x in lCOATemp)
                {
                    lCOA.Add(new COA()
                    {
                        AcNo = x.AcNo,
                        AcDesc = x.AcDesc,
                        AcType = x.AcType,
                        HasChild = x.HasChild,
                        Level = x.Level,
                        Parent = x.Parent,
                        //This Year
                        CurrentMonthBalance = x.CurrentMonthBalance,
                        CurrentMonthCAmt = x.CurrentMonthCAmt,
                        CurrentMonthDAmt = x.CurrentMonthDAmt,
                        MonthToDateBalance = x.MonthToDateBalance,
                        MonthToDateCAmt = x.MonthToDateCAmt,
                        MonthToDateDAmt = x.MonthToDateDAmt,
                        OpeningBalanceCAmt = x.OpeningBalanceCAmt,
                        OpeningBalanceDAmt = x.OpeningBalanceDAmt,
                        YearToDateBalance = x.YearToDateBalance,
                        YearToDateCAmt = x.YearToDateCAmt,
                        YearToDateDAmt = x.YearToDateDAmt,
                        //Last Year
                        CurrentMonthBalance2 = x.CurrentMonthBalance2,
                        CurrentMonthCAmt2 = x.CurrentMonthCAmt2,
                        CurrentMonthDAmt2 = x.CurrentMonthDAmt2,
                        MonthToDateBalance2 = x.MonthToDateBalance2,
                        MonthToDateCAmt2 = x.MonthToDateCAmt2,
                        MonthToDateDAmt2 = x.MonthToDateDAmt2,
                        OpeningBalanceCAmt2 = x.OpeningBalanceCAmt2,
                        OpeningBalanceDAmt2 = x.OpeningBalanceDAmt2,
                        YearToDateBalance2 = x.YearToDateBalance2,
                        YearToDateCAmt2 = x.YearToDateCAmt2,
                        YearToDateDAmt2 = x.YearToDateDAmt2,
                        //Last Yearly
                        MonthToDateBalance3 = x.MonthToDateBalance3,
                        MonthToDateCAmt3 = x.MonthToDateCAmt3,
                        MonthToDateDAmt3 = x.MonthToDateDAmt3,
                        LastYearBalance = x.LastYearBalance,
                        LastYearCAmt = x.LastYearCAmt,
                        LastYearDAmt = x.LastYearDAmt,
                        //CBP
                        YTDCBPAmt = x.YTDCBPAmt,
                        ThisYearCBPAmt = x.ThisYearCBPAmt
                    });
                }

                if (lCOA.Count > 0)
                {
                    Process2(ref lCOAOpeningBalanceTemp, ref lCOA, "New");
                    Process2(ref lCOAOpeningBalanceTemp, ref lCOA, "Old");

                    Process3(ref lJournalTemp, ref lCOA, "New");
                    Process3(ref lJournalTemp, ref lCOA, "Old");
                    Process3(ref lJournalTemp, ref lCOA, "Yearly");

                    Process4(ref lJournalCurrentMonthTemp, ref lCOA, "New");
                    Process4(ref lJournalCurrentMonthTemp, ref lCOA, "Old");

                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    Process9(ref lCOA);

                    Process10(ref lCBPTemp, ref lCOA, "YTD");
                    Process10(ref lCBPTemp, ref lCOA, "Yearly");

                    ProcessFicoCOA(ref lRptFicoSetting);
                    if (lRptFicoSetting.Count > 0)
                    {
                        ProcessFicoCOA2(ref lRptFicoSetting, ref lRptFicoSetting2);
                    }

                    ProcessFicoCOA3(ref lRptFicoSetting3);

                    if (lCOA.Count > 0)
                    {
                        Grd1.BeginUpdate();

                        foreach (var x in lRptFicoSetting2.OrderBy(o => o.AcNo))
                        {
                            foreach (var y in lCOA.OrderBy(o => o.AcNo).Where(a => a.AcNo == x.AcNo))
                            {
                                lRptFicoSetting4.Add(new RptFicoSetting2()
                                {
                                    FicoCode = x.FicoCode,
                                    SettingCode = x.SettingCode,
                                    SettingDesc = x.SettingDesc,
                                    Sequence = x.Sequence,
                                    Formula = x.Formula,
                                    AcNo = x.AcNo,
                                    ThisYTDBal = y.YearToDateBalance,
                                    LastYTDBal= y.YearToDateBalance2, 
                                    LastYearBal= y.LastYearBalance,
                                    ThisYearCBPAmt = y.ThisYearCBPAmt,
                                    YTDCBPAmt = y.YTDCBPAmt,


                                });
                                break;
                            }
                        }

                        ProcessFicoCOA4(ref lRptFicoSetting3, ref lRptFicoSetting4, Grd1);
                        Grd1.EndUpdate();
                    }

                    lCOA.Clear();
                }
                #endregion

                lCOATemp.Clear();
                lJournalCurrentMonthTemp.Clear();
                lJournalTemp.Clear();
                lCOAOpeningBalanceTemp.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetLueFicoFormula(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RptFicoCode As Col1, T.RptFicoName As Col2 ");
            SQL.AppendLine("From TblRptFicoSettingHdr T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where (T.SiteCode Is Null Or ");
                SQL.AppendLine("(T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser " +
                    "" +
                    "");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        private void SetLuePrintoutFormula(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '01' As Col1, 'Format 1' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '02' As Col1, 'Format 2' As Col2 ");
            SQL.AppendLine("Order By Col1 ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        private void GetAllCOA(ref List<COA2> l, string ListAcNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;
            string[] AcNo = ListAcNo.Split(',');

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And ( ");
                for (int x = 0; x < AcNo.Count(); ++x)
                {
                    SQL.AppendLine("(A.AcNo Like '" + AcNo[x] + "%') ");
                    if (x != AcNo.Count() - 1)
                        SQL.AppendLine(" Or ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
             
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA2()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }
        private void GetAllJournal(ref List<Journal2> l, string ListAcNo,
            string Mth, string Yr,
            string Yr2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");

            SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("    Where 1=1 ");
            SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("    Where 1=1 ");
            SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr2 ");
            SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth2 ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select 'Yearly' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("    Where 1=1 ");
            SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr2 ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);
            Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(Yr2, Mth));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 10000;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournalCurrentMonth(ref List<Journal3> l, string ListAcNo, string Mth, string Yr, string Yr2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");

            SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("    Where Left(A.DocDt, 6) = @YrMth ");
           
            SQL.AppendLine("Union All ");

            SQL.AppendLine("    Select 'Old' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("    Where Left(A.DocDt, 6) = @YrMth2 ");
            
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@YrMth2", string.Concat(Yr2, Mth));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal3()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCOAOpeningBalance(ref List<COAOpeningBalance2> l, string ListAcNo, string Yr, string Yr2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");

          
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'Old' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.Yr = @Yr2 ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
           
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAOpeningBalance2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCBP(ref List<CBP> l, string ListAcNo, string Mth, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 1, mMth = Int32.Parse(Mth); ;

            SQL.AppendLine("Select T.DocType, T.AcNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            while (i <= mMth)
            {
                SQL.AppendLine("Select 'YTD' As DocType, B.AcNo, B.Amt" + Sm.Right(string.Concat("00", i.ToString()), 2) + " As Amt ");
                SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
                SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
                SQL.AppendLine("    And B.Amt" + Sm.Right(string.Concat("00", i.ToString()), 2) + " != 0.00 ");
                SQL.AppendLine("    And A.Yr = @Yr");
                SQL.AppendLine("    And A.CancelInd = 'N' ");

                i += 1;
               

                if (i <= mMth)
                    SQL.AppendLine("Union All ");
            }


            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'Yearly' As DocType, B.AcNo, B.Amt ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    And B.Amt != 0.00 ");
            SQL.AppendLine("    And A.Yr = @Yr");
            SQL.AppendLine("    And A.CancelInd = 'N' ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Having Sum(T.Amt) != 0.00 ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CBP()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COAOpeningBalance2> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            if (DocType == "New")
                            {
                                lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                                lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            }
                            if(DocType == "Old")
                            {
                                lCOA[j].OpeningBalanceDAmt2 += lJournal[i].DAmt;
                                lCOA[j].OpeningBalanceCAmt2 += lJournal[i].CAmt;
                            }
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<Journal2> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            if (DocType == "New")
                            {
                                lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                                lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            }
                            if (DocType == "Old")
                            {
                                lCOA[j].MonthToDateDAmt2 += lJournal[i].DAmt;
                                lCOA[j].MonthToDateCAmt2 += lJournal[i].CAmt;
                            }
                            else if (DocType == "Yearly")
                            {
                                lCOA[j].MonthToDateDAmt3 += lJournal[i].DAmt;
                                lCOA[j].MonthToDateCAmt3 += lJournal[i].CAmt;
                            }

                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<Journal3> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(p => p.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            if (DocType == "New")
                            {
                                lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                                lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            }
                            if (DocType == "Old")
                            {
                                lCOA[j].CurrentMonthDAmt2 += lJournal[i].DAmt;
                                lCOA[j].CurrentMonthCAmt2 += lJournal[i].CAmt;
                            }
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            return;
            if (
                mAcNoForCurrentEarning.Length == 0 ||
                mAcNoForIncome.Length == 0 ||
                mAcNoForCost.Length == 0
                ) return;

            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            //var CurrentProfiLossParentIndex = -1;
            //var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;

            //for (var i = 0; i < lCOA.Count; i++)
            //{
            //    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //    {
            //        CurrentProfiLossParentIndex = i;
            //        break;
            //    }
            //}

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;
            //    (lCOA[IncomeIndex].OpeningBalanceCAmt - lCOA[IncomeIndex].OpeningBalanceDAmt)-
            //    (lCOA[CostIndex].OpeningBalanceDAmt - lCOA[CostIndex].OpeningBalanceCAmt);

            //lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt = Amt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].OpeningBalanceCAmt += Amt;

            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                        lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Year To Date
            //Amt =
            //    (lCOA[IncomeIndex].YearToDateCAmt - lCOA[IncomeIndex].YearToDateDAmt) -
            //    (lCOA[CostIndex].YearToDateDAmt - lCOA[CostIndex].YearToDateCAmt);

            //lCOA[CurrentProfiLossIndex].YearToDateDAmt = 0m;
            //lCOA[CurrentProfiLossIndex].YearToDateCAmt = Amt;
            //CurrentProfiLossParentIndex = -1;
            //CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            //f = true;
            //while (f)
            //{
            //    for (var i = 0; i < lCOA.Count; i++)
            //    {
            //        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
            //        {
            //            CurrentProfiLossParentIndex = i;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateDAmt += 0m;
            //            lCOA[CurrentProfiLossParentIndex].YearToDateCAmt += Amt;

            //            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
            //            f = CurrentProfiLossParentAcNo.Length > 0;
            //            break;
            //        }
            //    }
            //}
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                //This Year
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                //Last Year
                lCOA[i].YearToDateDAmt2 =
                    lCOA[i].OpeningBalanceDAmt2 +
                    lCOA[i].MonthToDateDAmt2 +
                    lCOA[i].CurrentMonthDAmt2;

                lCOA[i].YearToDateCAmt2 =
                    lCOA[i].OpeningBalanceCAmt2 +
                    lCOA[i].MonthToDateCAmt2 +
                    lCOA[i].CurrentMonthCAmt2;

                //Last Yearly
                lCOA[i].LastYearDAmt =
                    lCOA[i].OpeningBalanceDAmt2 +
                    lCOA[i].MonthToDateDAmt3;

                lCOA[i].LastYearCAmt =
                    lCOA[i].OpeningBalanceCAmt2 +
                    lCOA[i].MonthToDateCAmt3;

            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    lCOA[i].YearToDateBalance2 = lCOA[i].YearToDateDAmt2 - lCOA[i].YearToDateCAmt2;
                    lCOA[i].LastYearBalance = lCOA[i].LastYearDAmt - lCOA[i].LastYearCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                    lCOA[i].YearToDateBalance2 = lCOA[i].YearToDateCAmt2 - lCOA[i].YearToDateDAmt2;
                    lCOA[i].LastYearBalance = lCOA[i].LastYearCAmt - lCOA[i].LastYearDAmt;
                }
            }
        }

        private void Process10(ref List<CBP> l, ref List<COA> lCOA, string DocType)
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo).Where(a => a.Year == DocType))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    Amt = x.Amt,
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            if (DocType == "YTD")
                            {
                                lCOA[j].YTDCBPAmt = lJournal[i].Amt;
                            }
                            if (DocType == "Yearly")
                            {
                                lCOA[j].ThisYearCBPAmt = lJournal[i].Amt;
                            }
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void ProcessFicoCOA(ref List<RptFicoSetting> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order by Cast(sequence As UNSIGNED); ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueTypeCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new RptFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            Balance = 0,
                            AcNo = string.Empty,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA2(ref List<RptFicoSetting> l, ref List<RptFicoSetting> lRptFicoSetting)
        {
            lRptFicoSetting.Clear();

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].SettingDesc.Length > 0)
                {
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();

                    SQL.AppendLine("SET SESSION group_concat_max_len = 1000000; ");
                    SQL.AppendLine("Select AcNo From TblCOA ");
                    SQL.AppendLine("Where Find_In_Set(AcNo,( ");
                    SQL.AppendLine("    Select Group_Concat(Acno) As AcNo ");
                    SQL.AppendLine("    From TblRptFicoSettingDtl ");
                    SQL.AppendLine("    Where SettingCode=@SettingCode ");
                    SQL.AppendLine("    And RptFicoCode=@FicoCode ");
                    SQL.AppendLine("    Order by Cast(sequence As UNSIGNED) ");
                    SQL.AppendLine(")) Order By AcNo;");
                    Sm.CmParam<String>(ref cm, "@SettingCode", l[i].SettingCode);
                    Sm.CmParam<String>(ref cm, "@FicoCode", l[i].FicoCode);

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandTimeout = 600;
                        cm.CommandText = SQL.ToString();
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lRptFicoSetting.Add(new RptFicoSetting()
                                {
                                    FicoCode = l[i].FicoCode,
                                    SettingCode = l[i].SettingCode,
                                    SettingDesc = l[i].SettingDesc,
                                    Sequence = l[i].Sequence,
                                    Formula = l[i].Formula,
                                    AcNo = Sm.DrStr(dr, c[0]),
                                    Balance = 0,
                                });
                            }
                        }
                        dr.Close();
                    }
                }
            }
        }

        private void ProcessFicoCOA3(ref List<COAFicoSetting> lCOASet)
        {
            lCOASet.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula ");
            SQL.AppendLine("From TblRptFicoSettinghdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By Cast(B.sequence As UNSIGNED);");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueTypeCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        lCOASet.Add(new COAFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            Balance = 0,
                        });
                }
                dr.Close();
            }
        }

        private void ProcessFicoCOA4(ref List<COAFicoSetting> lCOASet, ref List<RptFicoSetting2> l, iGrid Grdxx)
        {
            Sm.ClearGrd(Grdxx, true);
            iGRow r;


            for (var i = 0; i < lCOASet.Count; i++)
            {
                decimal ThisYTDBal = 0, LastYTDBal = 0m, LastYearBal = 0m, YTDCBPBal = 0m, ThisYearCBPBal = 0m,
                    Perc = 0m, Perc2 = 0m, Perc3 = 0m, Perc4 = 0m;
                r = Grdxx.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = lCOASet[i].SettingCode;
                r.Cells[2].Value = lCOASet[i].SettingDesc;


                if (lCOASet[i].Formula.Length == 0)
                {
                    l.OrderBy(o => o.Sequence).ToList();
                    for (var j = 0; j < l.Count; j++)
                    {
                        if (lCOASet[i].SettingCode == l[j].SettingCode)
                        {
                            r.Cells[3].Value = LastYTDBal + l[j].LastYTDBal;
                            r.Cells[4].Value = LastYearBal + l[j].LastYearBal;
                            r.Cells[5].Value = ThisYearCBPBal + l[j].ThisYearCBPAmt;
                            r.Cells[6].Value = YTDCBPBal + l[j].YTDCBPAmt;
                            r.Cells[7].Value = ThisYTDBal + l[j].ThisYTDBal;


                            LastYTDBal = LastYTDBal + l[j].LastYTDBal;
                            LastYearBal = LastYearBal + l[j].LastYearBal;
                            ThisYearCBPBal = ThisYearCBPBal + l[j].ThisYearCBPAmt;
                            YTDCBPBal = YTDCBPBal + l[j].YTDCBPAmt;
                            ThisYTDBal = ThisYTDBal + l[j].ThisYTDBal;
                        }
                    }
                    if (r.Cells[7].Value != null)
                    {
                        r.Cells[8].Value = LastYTDBal == 0m ? 0m : (ThisYTDBal / LastYTDBal) * 0.01m;
                        r.Cells[9].Value = LastYearBal == 0m ? 0m : (ThisYTDBal / LastYearBal) * 0.01m;
                        r.Cells[10].Value = ThisYearCBPBal == 0m ? 0m : (ThisYTDBal / ThisYearCBPBal) * 0.01m;
                        r.Cells[11].Value = YTDCBPBal == 0m ? 0m : (ThisYTDBal / YTDCBPBal) * 0.01m;
                    }

                }
                else
                {
                    char[] delimiters = { '+', '-' };
                    string SQLFormula = lCOASet[i].Formula;
                    string SQLFormula2 = lCOASet[i].Formula;
                    string SQLFormula3 = lCOASet[i].Formula;
                    string SQLFormula4 = lCOASet[i].Formula;
                    string SQLFormula5 = lCOASet[i].Formula;
                    string[] ArraySQLFormula = lCOASet[i].Formula.Split(delimiters);
                    for (int ind = 0; ind < ArraySQLFormula.Count(); ind++)
                    {
                        for (var h = 0; h < Grdxx.Rows.Count; h++)
                        {
                            if (ArraySQLFormula[ind].ToString() == Sm.GetGrdStr(Grdxx, h, 1))
                            {
                                string oldS = ArraySQLFormula[ind].ToString();
                                string newS = Sm.GetGrdDec(Grdxx, h, 3).ToString();

                                SQLFormula = SQLFormula.Replace(oldS, newS);

                                string oldS2 = ArraySQLFormula[ind].ToString();
                                string newS2 = Sm.GetGrdDec(Grdxx, h, 4).ToString();

                                SQLFormula2 = SQLFormula2.Replace(oldS2, newS2);

                                string oldS3 = ArraySQLFormula[ind].ToString();
                                string newS3 = Sm.GetGrdDec(Grdxx, h, 5).ToString();

                                SQLFormula3 = SQLFormula3.Replace(oldS3, newS3);

                                string oldS4 = ArraySQLFormula[ind].ToString();
                                string newS4 = Sm.GetGrdDec(Grdxx, h, 6).ToString();

                                SQLFormula4 = SQLFormula4.Replace(oldS4, newS4);

                                string oldS5 = ArraySQLFormula[ind].ToString();
                                string newS5 = Sm.GetGrdDec(Grdxx, h, 7).ToString();

                                SQLFormula5 = SQLFormula5.Replace(oldS5, newS5);
                            }
                        }
                    }
                    decimal baltemp = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
                    decimal baltemp2 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula2 + " "));
                    decimal baltemp3 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula3 + " "));
                    decimal baltemp4 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula4 + " "));
                    decimal baltemp5 = Decimal.Parse(Sm.GetValue("Select " + SQLFormula5 + " "));
                    r.Cells[3].Value = baltemp;
                    r.Cells[4].Value = baltemp2;
                    r.Cells[5].Value = baltemp3;
                    r.Cells[6].Value = baltemp4;
                    r.Cells[7].Value = baltemp5;

                    if (r.Cells[7].Value != null)
                    {
                        r.Cells[8].Value = baltemp == 0m ? 0m : (baltemp5 / baltemp) * 0.01m;
                        r.Cells[9].Value = baltemp2 == 0m ? 0m : (baltemp5 / baltemp2) * 0.01m;
                        r.Cells[10].Value = baltemp3 == 0m ? 0m : (baltemp5 / baltemp3) * 0.01m;
                        r.Cells[11].Value = baltemp4 == 0m ? 0m : (baltemp5 / baltemp4) * 0.01m;
                    }

                }

            }
        }



        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

       
        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue1(SetLueFicoFormula));
        }

        private void LuePrintoutFormatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePrintoutFormatCode, new Sm.RefreshLue1(SetLuePrintoutFormula));
        }



        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
            public decimal LastYearDAmt { get; set; }
            public decimal LastYearCAmt { get; set; }
            public decimal LastYearBalance { get; set; }
            public decimal CurrentMonthBalance2 { get; set; }
            public decimal CurrentMonthCAmt2 { get; set; }
            public decimal CurrentMonthDAmt2 { get; set; }
            public decimal MonthToDateBalance2 { get; set; }
            public decimal MonthToDateCAmt2 { get; set; }
            public decimal MonthToDateDAmt2 { get; set; }
            public decimal OpeningBalanceCAmt2 { get; set; }
            public decimal OpeningBalanceDAmt2 { get; set; }
            public decimal YearToDateBalance2 { get; set; }
            public decimal YearToDateCAmt2 { get; set; }
            public decimal YearToDateDAmt2 { get; set; }
            public decimal MonthToDateBalance3 { get; set; }
            public decimal MonthToDateCAmt3 { get; set; }
            public decimal MonthToDateDAmt3 { get; set; }
            public decimal OpeningBalanceCAmt3 { get; set; }
            public decimal OpeningBalanceDAmt3 { get; set; }
            public decimal YTDCBPAmt { get; set; }
            public decimal ThisYearCBPAmt { get; set; }
        }

        private class COA2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
            public decimal LastYearDAmt { get; set; }
            public decimal LastYearCAmt { get; set; }
            public decimal LastYearBalance { get; set; }
            public decimal CurrentMonthBalance2 { get; set; }
            public decimal CurrentMonthCAmt2 { get; set; }
            public decimal CurrentMonthDAmt2 { get; set; }
            public decimal MonthToDateBalance2 { get; set; }
            public decimal MonthToDateCAmt2 { get; set; }
            public decimal MonthToDateDAmt2 { get; set; }
            public decimal OpeningBalanceCAmt2 { get; set; }
            public decimal OpeningBalanceDAmt2 { get; set; }
            public decimal YearToDateBalance2 { get; set; }
            public decimal YearToDateCAmt2 { get; set; }
            public decimal YearToDateDAmt2 { get; set; }
            public decimal MonthToDateBalance3 { get; set; }
            public decimal MonthToDateCAmt3 { get; set; }
            public decimal MonthToDateDAmt3 { get; set; }
            public decimal OpeningBalanceCAmt3 { get; set; }
            public decimal OpeningBalanceDAmt3 { get; set; }
            public decimal YTDCBPAmt { get; set; }
            public decimal ThisYearCBPAmt { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string DocType { get; set; }
            public decimal Amt { get; set; }
        }

        private class Journal2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal3
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAOpeningBalance2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class CBP
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class COAFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
        }

        private class RptFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
        }

        private class RptFicoSetting2
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string Formula { get; set; }
            public decimal Balance { get; set; }
            public decimal LastYTDBal{ get; set; }
            public decimal LastYearBal { get; set; }
            public decimal ThisYearCBPAmt { get; set; }
            public decimal YTDCBPAmt { get; set; }
            public decimal ThisYTDBal { get; set; }
        }

        #endregion
    }
}
