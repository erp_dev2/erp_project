﻿#region Update
/*
    31/01/2018 [HAR] ubah nama kolom
    11/04/2019 [WED] Budget v PO ambil kalkulasi UPrice*Qty saja
    14/05/2019 [WED] validasi Budget v PO, melihat Grand Total PO (minus TaxAmt & CustomsTaxAmt)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptBudgetCashFlow : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsFilterBySite = false;
        private bool mIsFilterByDept = false;
        private string 
            mMainCurCode = string.Empty,
            mBudgetBasedOn = string.Empty; // 1 = Department, 2 = Site

        #endregion

        #region Constructor

        public FrmRptBudgetCashFlow(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                if (mBudgetBasedOn == "1")
                {
                    SetLueDeptCode(ref LueDeptCode);
                }
                if (mBudgetBasedOn == "2")
                {
                   SetLueSiteCode(ref LueDeptCode);
                    label2.Text = "Site";
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Z1.DeptCode, Z2.DeptName, Z1.Yr, Z1.Mth, ");
            SQL.AppendLine("ifnull(Z1.Amt, 0) As BudgetAmt, ");
            //SQL.AppendLine("IfNull(Z5.Amt, 0.00)-IfNull(Z6.Amt, 0.00) As TotalAmtPO, ");
            SQL.AppendLine("IfNull(Z5.Amt, 0.00) As TotalAmtPO, ");
            SQL.AppendLine("IfNull(Z3.Amt, 0.00)-IfNull(Z4.Amt, 0.00) As TotalAmtMr  ");
            SQL.AppendLine("From TblBudget Z1 ");
            SQL.AppendLine("Inner Join TblDepartment Z2 On Z1.DeptCode = Z2.DeptCode ");

            //MR yg sudah dibuat PO

            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select A.DeptCode, Sum(B.UPrice*D.Qty) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status='A' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL.AppendLine("    Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL.AppendLine("    Where A.Status='A' ");
            SQL.AppendLine("    And A.Reqtype='1' ");
            SQL.AppendLine("    And Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Group By A.DeptCode ");
            SQL.AppendLine(") Z3 On Z1.DeptCode=Z3.DeptCode ");

            //PO yg diputihkan dengan menggunakan price MR

            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select A.DeptCode, Sum(B.UPrice*F.Qty) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status='A' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL.AppendLine("    Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL.AppendLine("    Inner Join TblPOQtyCancel F On D.DocNo=F.PODocNo And D.DNo=F.PODNo And F.CancelInd='N' ");
            SQL.AppendLine("    Where A.Status='A' ");
            SQL.AppendLine("    And A.Reqtype='1' ");
            SQL.AppendLine("    And Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            SQL.AppendLine("    Group By A.DeptCode ");
            SQL.AppendLine(") Z4 On Z1.DeptCode=Z4.DeptCode ");

            //MR yg sudah dibuat PO dengan harga PO

            #region Old Code

            //SQL.AppendLine("Left Join (");
            //SQL.AppendLine("    Select A.DeptCode, Sum( ");
            //SQL.AppendLine("    ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue) ");
            //SQL.AppendLine("    * Case When F.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("    IfNull(( ");
            //SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("        Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("    ), 0.00) End ");
            //SQL.AppendLine("    ) As Amt ");
            //SQL.AppendLine("    From TblMaterialRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("        On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        And B.CancelInd='N' ");
            //SQL.AppendLine("        And B.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("    Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("    Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            //SQL.AppendLine("    Where A.Status='A' ");
            //SQL.AppendLine("    And A.Reqtype='1' ");
            //SQL.AppendLine("    And Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            //SQL.AppendLine("    Group By A.DeptCode ");
            //SQL.AppendLine(") Z5 On Z1.DeptCode=Z5.DeptCode ");

            //SQL.AppendLine("Left Join (");
            //SQL.AppendLine("    Select A.DeptCode, Sum( ");
            //SQL.AppendLine("    ((((100.00-D.Discount)*0.01)*(H.Qty*G.UPrice))-((H.Qty/D.Qty)*D.DiscountAmt)+((H.Qty/D.Qty)*D.RoundingValue)) ");
            //SQL.AppendLine("    * Case When F.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("    IfNull(( ");
            //SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("        Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("    ), 0.00) End ");
            //SQL.AppendLine("    ) As Amt ");
            //SQL.AppendLine("    From TblMaterialRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("        On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        And B.CancelInd='N' ");
            //SQL.AppendLine("        And B.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("    Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("    Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            //SQL.AppendLine("    Inner Join TblPOQtyCancel H On D.DocNo=H.PODocNo And D.DNo=H.PODNo And H.CancelInd='N' ");
            //SQL.AppendLine("    Where A.Status='A' ");
            //SQL.AppendLine("    And A.Reqtype='1' ");
            //SQL.AppendLine("    And Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            //SQL.AppendLine("    Group By A.DeptCode ");
            //SQL.AppendLine(") Z6 On Z1.DeptCode=Z6.DeptCode ");

            #endregion

            #region Old Code 2

            //SQL.AppendLine("Left Join (");
            //SQL.AppendLine("    Select A.DeptCode, Sum( ");
            //SQL.AppendLine("    (D.Qty*G.UPrice) ");
            //SQL.AppendLine("    * Case When F.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("    IfNull(( ");
            //SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("        Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("    ), 0.00) End ");
            //SQL.AppendLine("    ) As Amt ");
            //SQL.AppendLine("    From TblMaterialRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("        On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        And B.CancelInd='N' ");
            //SQL.AppendLine("        And B.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("    Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("    Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            //SQL.AppendLine("    Where A.Status='A' ");
            //SQL.AppendLine("    And A.Reqtype='1' ");
            //SQL.AppendLine("    And Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            //SQL.AppendLine("    Group By A.DeptCode ");
            //SQL.AppendLine(") Z5 On Z1.DeptCode=Z5.DeptCode ");

            #endregion

            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select X2.DeptCode, Sum( ");
            SQL.AppendLine("    X2.Amt -  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
            SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
            SQL.AppendLine("          IfNull((  ");
            SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
            SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
            SQL.AppendLine("        ), 0.00) End ");
            SQL.AppendLine("      , 0.00)   ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPOHdr X1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
            SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
            SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
            SQL.AppendLine("        IfNull((  ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
            SQL.AppendLine("        ), 0.00) End  ");
            SQL.AppendLine("        ) As Amt  ");
            SQL.AppendLine("        From TblMaterialRequestHdr A  ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("             And Left(A.DocDt, 6)=Concat(@Yr, @Mth)  ");
            SQL.AppendLine("            And B.CancelInd='N'  ");
            SQL.AppendLine("            And B.Status='A'  ");
            SQL.AppendLine("            And A.Status='A'  ");
            SQL.AppendLine("            And A.Reqtype='1'   ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
            SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
            SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
            SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
            SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
            SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
            SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("    Group By X2.DeptCode ");
            SQL.AppendLine(") Z5 On Z1.DeptCode=Z5.DeptCode ");

            SQL.AppendLine("Where Z1.ApprovalDt Is Not Null ");
            SQL.AppendLine("And Z1.Yr=@Yr ");
            SQL.AppendLine("And Z1.Mth=@Mth ");
            if (mBudgetBasedOn == "1" && mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=Z1.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Department",
                        "Budget",
                        "Amount Actual"+Environment.NewLine+"PO",
                        "Amount Actual"+Environment.NewLine+"MR",
                        "Budget"+Environment.NewLine+"VS"+Environment.NewLine+"Purchase Order",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 200, 180, 180, 180
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);

            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }   
            else
            {
                try
                {
                    SetSQL();
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " ";

                    var cm = new MySqlCommand();
                    Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Z1.DeptCode", true);

                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Z2.DeptName;",
                        new string[]
                        { 
                            //0
                            "DeptName", 

                            //1-3
                            "BudgetAmt", "TotalAmtPO", "TotalAmtMr"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Grd1.Cells[Row, 5].Value = Sm.DrDec(dr, c[1]) - Sm.DrDec(dr, c[2]);
                        }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }
        }


        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                SQL.AppendLine(") ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if(mIsFilterBySite) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mBudgetBasedOn == "1")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
            if (mBudgetBasedOn == "2")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueSiteCode));

            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
