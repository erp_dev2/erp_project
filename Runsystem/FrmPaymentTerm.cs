﻿#region Update
/*
    26/03/2019 [DITA] Tambah remark
    07/06/2021 [VIN/IMS] tambah karaketer YOP name jadi 200 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPaymentTerm : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPaymentTermFind FrmFind;

        #endregion

        #region Constructor

        public FrmPaymentTerm(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPtCode, TxtPtName, TxtPtDay, MeeRemark
                    }, true);
                    TxtPtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPtCode, TxtPtName, TxtPtDay, MeeRemark
                    }, false);
                    TxtPtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPtName, TxtPtDay, MeeRemark
                    }, false);
                    TxtPtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPtCode, TxtPtName, MeeRemark
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtPtDay }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPaymentTermFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPaymentTerm Where PtCode=@PtCode" };
                Sm.CmParam<String>(ref cm, "@PtCode", TxtPtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPaymentTerm(PtCode, PtName, PtDay, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PtCode, @PtName, @PtDay, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update PtName=@PtName, PtDay=@PtDay, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PtCode", TxtPtCode.Text);
                Sm.CmParam<String>(ref cm, "@PtName", TxtPtName.Text);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<Decimal>(ref cm, "@PtDay", decimal.Parse(TxtPtDay.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PtCode", PtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select PtCode, PtName, PtDay, Remark From TblPaymentTerm Where PtCode=@PtCode",
                        new string[] 
                        {
                            "PtCode", "PtName", "PtDay", "Remark" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPtName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtPtDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPtCode, "Term of payment code", false) ||
                Sm.IsTxtEmpty(TxtPtName, "Term of payment name", false) ||
                Sm.IsTxtEmpty(TxtPtDay, "Number of day", false) ||
                IsPtCodeExisted();
        }

        private bool IsPtCodeExisted()
        {
            if (!TxtPtCode.Properties.ReadOnly && Sm.IsDataExist("Select PtCode From TblPaymentTerm Where PtCode='" + TxtPtCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Term of payment code ( " + TxtPtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPtCode);
        }

        private void TxtPtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPtName);
        }

        private void TxtPtDay_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPtDay, 0);
        }

        #endregion

        #endregion
    }
}
