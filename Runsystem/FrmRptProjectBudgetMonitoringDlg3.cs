﻿#region Update
/*
    06/12/2020 [DITA] New Apps
    05/08/2021 [TKG/IMS] tambah informasi recv whs project
    06/08/2021 [TKG/IMS] ubah sumber do#
    10/08/2021 [VIN/IMS] tambah param IsMovingAvgEnabled
    27/10/2021 [IBL/IMS] Tambah source dari journal receiving
    08/11/2021 [VIN/IMS] source dari journal receiving data terduplikat
    02/12/2021 [ICA/IMS] Bug data double, item yg muncul sesuai dengan journalnya, journal yg kepilih yg cancel dan yg tidak cancel
    17/12/2021 [ICA/IMS] Menampilkan data yg tidak memiliki DO
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetMonitoringDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptProjectBudgetMonitoring mFrmParent;
        internal string mJournalDocNo = string.Empty, mAcNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetMonitoringDlg3(FrmRptProjectBudgetMonitoring FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Date",
                    "Month",
                    "",
                    "Journal#",
                    "COA#",

                    //6-10
                    "COA",
                    "Value",
                    "Remark",
                    "DO#",
                    "Item's Code",

                    //11-15
                    "Local Code",
                    "Item's Name",
                    "Specification",
                    "Received",
                    "UoM",

                    //16-17
                    "Price",
                    "Total"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    80, 100, 20, 180, 150,

                    //6-10
                    200, 150, 250, 150, 120,

                    //11-15
                    100, 200, 200, 100, 80,

                    //16-17
                    130, 130
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 14, 16, 17 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocDt, Mth, DocNo, AcNo, AcDesc, Amt, Remark, Group_Concat(Distinct DOWhsDocNo) DOWhsDocNo, Group_Concat(Distinct ItCode) ItCode, Group_Concat(Distinct ItCodeInternal) ItCodeInternal, ");
            SQL.AppendLine("Group_Concat(Distinct ItName) ItName, Group_Concat(Distinct Specification) Specification, Sum(Ifnull(Qty,0.00)) Qty, Group_Concat(distinct InventoryUomCode) InventoryUomCode, Sum(Ifnull(Price, 0.00)) Price, ");
            SQL.AppendLine("Sum(Ifnull(Total,0.00)) Total ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocDt, Substring(A.DocDt, 5, 2) Mth, A.DocNo, B.AcNo, C.AcDesc, ");
            SQL.AppendLine("    Case C.AcType When 'D' Then B.DAmt - B.CAmt Else B.CAmt - B.DAmt End As Amt, ");
            SQL.AppendLine("    A.Remark, ");
            SQL.AppendLine("    G.DocNo As DOWhsDocNo, H.ItCode, H.ItCodeInternal, H.ItName, H.Specification, IfNull(E.Qty, 0.00) As Qty, ");
            SQL.AppendLine("    H.InventoryUomCode,  ");
            if (mFrmParent.mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Case When J.MovingAvgInd='Y' Then IfNull(K.MovingAvgPrice, 0.00) Else I.Uprice End As Price, ");
                SQL.AppendLine("    Case When J.MovingAvgInd='Y' Then IfNull(E.Qty, 0.00)*IfNull(K.MovingAvgPrice, 0.00) Else IfNull(E.Qty, 0.00)*IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) End As Total ");
            }
            else
            {
                SQL.AppendLine("    IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) As Price, ");
                SQL.AppendLine("    IfNull(E.Qty, 0.00)*IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) As Total ");
            }
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo And B.AcNo=@AcNo ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("    Left Join TblRecvWhs4Hdr D On A.DocNo=D.JournalDocNo ");
            SQL.AppendLine("    Left Join TblRecvWhs4Dtl E On D.DocNo=E.DocNo ");
            SQL.AppendLine("    Left Join TblDOWhsDtl F On E.DOWhsDocNo=F.DocNo And E.DOWhsDNo=F.DNo ");
            SQL.AppendLine("    Left Join TblDOWhs4Dtl G On F.DocNo=G.DOWhsDocNo And F.DNo=G.DOWhsDNo ");
            SQL.AppendLine("    Left Join TblItem H On F.ItCode=H.ItCode ");
            SQL.AppendLine("    Left Join TblStockPrice I On F.Source=I.Source ");
            SQL.AppendLine("    Left Join TblItemCategory J On H.ItCtCode = J.ItCtCode  ");
            SQL.AppendLine("    Left Join TblItemMovingAvg K On H.ItCode=K.ItCode ");
            SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DocNo) ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select A.DocDt, Substring(A.DocDt, 5, 2) Mth, A.DocNo, B.AcNo, C.AcDesc, ");
            SQL.AppendLine("    Case C.AcType When 'D' Then B.DAmt - B.CAmt Else B.CAmt - B.DAmt End As Amt, ");
            SQL.AppendLine("    A.Remark, ");
            SQL.AppendLine("    G.DocNo As DOWhsDocNo, H.ItCode, H.ItCodeInternal, H.ItName, H.Specification, IfNull(E.Qty, 0.00) As Qty, ");
            SQL.AppendLine("    H.InventoryUomCode, ");
            if (mFrmParent.mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Case When J.MovingAvgInd = 'Y' Then IfNull(K.MovingAvgPrice, 0.00) Else I.Uprice End As Price, ");
                SQL.AppendLine("    Case When J.MovingAvgInd = 'Y' Then IfNull(E.Qty, 0.00)*IfNull(K.MovingAvgPrice, 0.00) Else IfNull(E.Qty, 0.00)*IfNull(I.UPrice, 0.00) * IfNull(I.ExcRate, 0.00) End As Total ");
            }
            else
            {
                SQL.AppendLine("    IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) As Price, ");
                SQL.AppendLine("    IfNull(E.Qty, 0.00)*IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) As Total ");
            }
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo And B.AcNo=@AcNo ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("    Left Join TblRecvVdJournal D On A.DocNo = D.JournalDocNo ");
            SQL.AppendLine("    Left Join TblRecvVdDtl E On D.RecvVdDocNo = E.DocNo And D.RecvVdDNo = E.DNo ");
            SQL.AppendLine("    Left Join TblDODeptHdr F On D.RecvVdDocNo = F.RecvVdDocNo ");
            SQL.AppendLine("    Left Join TblDODeptDtl G On F.DocNo = G.DocNo And E.DNo = G.DNo ");
            SQL.AppendLine("    Left Join TblItem H On E.ItCode=H.ItCode ");
            SQL.AppendLine("    Left Join TblStockPrice I On E.Source=I.Source ");
            SQL.AppendLine("    Left Join TblItemCategory J On H.ItCtCode = J.ItCtCode ");
            SQL.AppendLine("    Left Join TblItemMovingAvg K On H.ItCode=K.ItCode ");
            SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DocNo) ");

            SQL.AppendLine("    Union All ");
            //for cancell data
            SQL.AppendLine("    Select A.DocDt, Substring(A.DocDt, 5, 2) Mth, A.DocNo, B.AcNo, C.AcDesc, ");
            SQL.AppendLine("    Case C.AcType When 'D' Then B.DAmt - B.CAmt Else B.CAmt - B.DAmt End As Amt, ");
            SQL.AppendLine("    A.Remark, ");
            SQL.AppendLine("    G.DocNo As DOWhsDocNo, H.ItCode, H.ItCodeInternal, H.ItName, H.Specification, IfNull(E.Qty, 0.00) As Qty, ");
            SQL.AppendLine("    H.InventoryUomCode, ");
            if (mFrmParent.mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Case When J.MovingAvgInd = 'Y' Then IfNull(K.MovingAvgPrice, 0.00) Else I.Uprice End As Price, ");
                SQL.AppendLine("    Case When J.MovingAvgInd = 'Y' Then IfNull(E.Qty, 0.00)*IfNull(K.MovingAvgPrice, 0.00) Else IfNull(E.Qty, 0.00)*IfNull(I.UPrice, 0.00) * IfNull(I.ExcRate, 0.00) End As Total ");
            }
            else
            {
                SQL.AppendLine("    IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) As Price, ");
                SQL.AppendLine("    IfNull(E.Qty, 0.00)*IfNull(I.UPrice, 0.00)*IfNull(I.ExcRate, 0.00) As Total ");
            }
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo And B.AcNo=@AcNo ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo=C.AcNo ");
            SQL.AppendLine("    Left Join TblRecvVdJournal D On A.DocNo = D.JournalDocNo2 ");
            SQL.AppendLine("    Left Join TblRecvVdDtl E On D.RecvVdDocNo = E.DocNo And D.RecvVdDNo = E.DNo ");
            SQL.AppendLine("    Left Join TblDODeptHdr F On D.RecvVdDocNo = F.RecvVdDocNo ");
            SQL.AppendLine("    Left Join TblDODeptDtl G On F.DocNo = G.DocNo And E.DNo = G.DNo ");
            SQL.AppendLine("    Left Join TblItem H On E.ItCode=H.ItCode ");
            SQL.AppendLine("    Left Join TblStockPrice I On E.Source=I.Source ");
            SQL.AppendLine("    Left Join TblItemCategory J On H.ItCtCode = J.ItCtCode ");
            SQL.AppendLine("    Left Join TblItemMovingAvg K On H.ItCode=K.ItCode ");
            SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DocNo) ");
            SQL.AppendLine(")T ");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<string>(ref cm, "@DocNo", mJournalDocNo);
                Sm.CmParam<string>(ref cm, "@AcNo", mAcNo);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() +
                    " Group By DocDt, Mth, DocNo, AcNo, AcDesc, Amt, Remark " +
                    " Order By T.DocDt, T.DocNo ",
                    new string[] 
                    { 
                        //0
                        "DocDt", 
                        //1-5
                        "Mth", "DocNo", "AcNo", "AcDesc", "Amt",
                        //6-10
                        "Remark", "DOWhsDocNo", "ItCode", "ItCodeInternal", "ItName", 
                        //11-15
                        "Specification", "Qty", "InventoryUomCode", "Price", "Total"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    }, true, false, false, false
                );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 14, 16, 17 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0)
            {

                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();

            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0)
                {

                    var f = new FrmJournal("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();

                }
          
        }

        #endregion

        #endregion

    }
}
