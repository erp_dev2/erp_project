﻿#region  Update
    // 16/08/2017 [TKG] tetap menampilkan karyawan yg resign sebelum masanya.
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmEmpWorkSchedule : RunSystem.FrmBase7
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mSQL = string.Empty,
            mWSCodeForNationalHoliday = string.Empty,
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty;
        private int mCol = -1;
        private bool 
            mIsFilterBySite = false,
            mIsNationalHolidayAutoUpdateWorkSchedule = false
            ;

        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmEmpWorkSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                DteDocDt1.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueAGCode(ref LueAGCode);
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");

                Sl.SetLueWSCode(ref LueWSCode);
                LueWSCode.Visible = false;

                SetSQL();
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        { 
            mWSCodeForNationalHoliday = Sm.GetParameter("WSCodeForNationalHoliday");
            mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");
            mIsNationalHolidayAutoUpdateWorkSchedule = Sm.GetParameter("IsNationalHolidayAutoUpdateWorkSchedule")=="Y";
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, D.AGName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.AGCode, T1.AGName, T2.EmpCode ");
            SQL.AppendLine("    From TblAttendanceGrpHdr T1 ");
            SQL.AppendLine("    Inner Join TblAttendanceGrpDtl T2 On T1.AGCode=T2.AGCode ");
            SQL.AppendLine("    Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("        And T3.DeptCode=@DeptCode ");
            SQL.AppendLine(Filter1.Replace("A.", "T3."));
            SQL.AppendLine("    Where T1.ActInd='Y' ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine(" Where 0=0 ");
            SQL.AppendLine(Filter1);
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Employee"+Environment.NewLine+"Code",
                        
                        //1-5
                        "Employee Name",
                        "Old Code",
                        "Position",
                        "Department",
                        "Attendance"+Environment.NewLine+"Group"
                    },
                     new int[] 
                    {
                        //0
                        100, 

                        //1-5
                        200, 80, 150, 150, 150
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                ClearData();
                Grd1.Cols.Count = 6;

                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsLueEmpty(LueDeptCode, "Department")
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                ShowEmployee();
                ShowDate();
                ShowHoliday();

                var l = new List<EmpWorkSchedule>();

                GetData(ref l);
                ShowData(ref l);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ClearData()
        {
            LueWSCode.EditValue = null;
            Sm.ClearGrd(Grd1, true);
        }

        override protected void SaveData()
        {
            try
            {
                NationalHolidayAutoUpdateWorkSchedule();

                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsSaveDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var l = new List<EmpWorkSchedule>();
                
                PrepareData(ref l);

                var cml = new List<MySqlCommand>();

                l.ForEach(i => { cml.Add(SaveEmpWorkSchedule(ref i)); });
                
                Sm.ExecCommands(cml);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsSaveDataNotValid()
        {
            return IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No employee selected.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpWorkSchedule(ref EmpWorkSchedule i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpWorkSchedule(EmpCode, Dt, WSCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@EmpCode, @Dt, @WSCode, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update WSCode=@WSCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", i.EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", i.Dt);
            Sm.CmParam<String>(ref cm, "@WSCode", i.WSCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void PrepareData(ref List<EmpWorkSchedule> l)
        {
            int Col = 6;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Grd1.Cols.Count > 5)
                {
                    Col = 6;
                    while (Col < Grd1.Cols.Count)
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.GetGrdStr(Grd1, Row, 0),
                            Dt = Grd1.Header.Cells[0, Col].Value.ToString(),
                            WSCode = Sm.GetGrdStr(Grd1, Row, Col)
                        });
                        Col += 2;
                    }
                }
            }
        }

        #endregion

        #region Additional Method

        private void NationalHolidayAutoUpdateWorkSchedule()
        {
            if (!mIsNationalHolidayAutoUpdateWorkSchedule ||
                mWSCodeForNationalHoliday.Length == 0 ||
                mWSCodesForNationalHoliday.Length == 0 ||
                mWSCodesForHoliday.Length == 0) return;

            NationalHolidayAutoUpdateWorkSchedule(
                mWSCodesForNationalHoliday.Split('#').Union(mWSCodesForHoliday.Split('#')).ToArray());
        }

        private void NationalHolidayAutoUpdateWorkSchedule(string[] AllHolidays)
        {
            bool IsNeedUpdate = true;
            var cm = new MySqlCommand();
            cm.CommandText = "Select WSName from TblWorkSchedule Where WSCode=@WSCode;";
            Sm.CmParam<String>(ref cm, "@WSCode", mWSCodeForNationalHoliday);
            var WSNameForNationalHoliday = Sm.GetValue(cm);

            for (int i = 6; i < Grd1.Cols.Count - 1; i++)
            {
                if (Grd1.Header.Cells[0, i + 1].BackColor == Color.Pink)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                        {
                            IsNeedUpdate = true;
                            foreach (string h in AllHolidays)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, i), h))
                                {
                                    IsNeedUpdate = false;
                                    break;
                                }
                            }
                            if (IsNeedUpdate)
                            {
                                Grd1.Cells[r, i].Value = mWSCodeForNationalHoliday;
                                Grd1.Cells[r, i + 1].Value = WSNameForNationalHoliday;
                                Grd1.Cells[r, i + 1].ForeColor = Color.Red;
                                Grd1.Cells[r, i + 1].BackColor = Color.Pink;
                            }
                        }
                    }
                }
            }
        }

        private void ShowEmployee()
        {
            string Filter = " ", Filter2 = " ";

            var cm = new MySqlCommand();

            //switch (Sm.GetLue(LueStatus))
            //{
            //    case "1":
            //        Filter += " And A.ResignDt Is Null ";
            //        break;
            //    case "2":
            //        Filter += " And A.ResignDt Is Not Null ";
            //        break;
            //}

            switch (Sm.GetLue(LueStatus))
            {
                case "1":
                    Filter += " And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ";
                    break;
                case "2":
                    Filter += " And A.ResignDt Is Not Null ";
                    break;
            }

            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueAGCode), "T1.AGCode", true);
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
             
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(Filter, Filter2) + " Order By C.DeptName, D.AGName, A.EmpName;",
                    new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "AGName"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    }, true, false, false, false
                );
            if (Grd1.Rows.Count>=1 && Sm.GetGrdStr(Grd1, 0, 0).Length>0) Grd1.Rows.Add();
        }

        private void ShowDate()
        {
            var DtMin = Sm.GetDte(DteDocDt1);
            var DtMax = Sm.GetDte(DteDocDt2);

            if (DtMin.Length >= 8 &&
                DtMax.Length >= 8 &&
                decimal.Parse(DtMin) <= decimal.Parse(DtMax))
            {
                DateTime Dt1 = new DateTime(
                       Int32.Parse(DtMin.Substring(0, 4)),
                       Int32.Parse(DtMin.Substring(4, 2)),
                       Int32.Parse(DtMin.Substring(6, 2)),
                       0, 0, 0);

                DateTime Dt2 = new DateTime(
                    Int32.Parse(DtMax.Substring(0, 4)),
                    Int32.Parse(DtMax.Substring(4, 2)),
                    Int32.Parse(DtMax.Substring(6, 2)),
                    0, 0, 0);

                DateTime TempDt = Dt1;

                var TotalDays = (Dt2 - Dt1).Days;

                var s = new List<string>();

                s.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );

                for (int i = 0; i < TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    s.Add(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    );
                }

                Grd1.Cols.Count = 6 + ((TotalDays + 1) * 2);
                int Col = 6;

                Grd1.BeginUpdate();
                s.ForEach(i =>
                {
                    Grd1.Header.Cells[0, Col].Value = i;
                    Grd1.Cols[Col].Width = 100;
                    Grd1.Cols[Col].Visible = false;
                    Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.MiddleCenter;

                    Grd1.Header.Cells[0, Col + 1].Value =
                        Sm.Right(i, 2) + "-" +
                        CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(
                            int.Parse(i.Substring(4, 2))
                            ) + "-" +
                        Sm.Left(i, 4) + Environment.NewLine +
                        TempDt.DayOfWeek.ToString();
                    Grd1.Cols[Col+1].Width = 100;
                    Grd1.Header.Cells[0, Col+1].TextAlign = iGContentAlignment.MiddleCenter;

                    if ((int)TempDt.DayOfWeek==0)
                    {
                        for (int r = 0; r < Grd1.Rows.Count; r++)
                            Grd1.Cells[r, Col + 1].BackColor = Color.LightSalmon;
                    }

                    TempDt = TempDt.AddDays(1);
                    Col += 2;
                });
                Grd1.EndUpdate();
            }
        }

        private void ShowHoliday()
        {
            var SQL = new StringBuilder();
            var l = new List<Holiday>();

            SQL.AppendLine("Select HolDt ");
            SQL.AppendLine("From TblHoliday ");
            SQL.AppendLine("Where HolDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("Order By HolDt;");

            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "HolDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Holiday()
                        {
                            HolDt = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0 && Grd1.Cols.Count>6)
            {
                l.ForEach(h =>
                {
                    for (int i = 6; i < Grd1.Cols.Count; i++)
                    {
                        if (Sm.CompareStr(h.HolDt, Grd1.Header.Cells[0, i].Value.ToString()))
                        {
                            Grd1.Header.Cells[0, i+1].Value += (Environment.NewLine + "HOLIDAY");
                            Grd1.Header.Cells[0, i+1].BackColor = Color.Pink;
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                                Grd1.Cells[r, i + 1].BackColor = Color.Pink;
                        }
                    }
                });
            }
            Grd1.Rows.AutoHeight();
        }

        private void GetData(ref List<EmpWorkSchedule> l)
        {
            string Param = string.Empty;
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                        Param += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";
            }
            if (Param.Length == 0) Param = "##XXX##";

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, B.WSName, B.HolidayInd ");
            SQL.AppendLine("From TblEmpWorkSchedule A, TblWorkSchedule B ");
            SQL.AppendLine("Where Locate(Concat('##', A.EmpCode, '##'), @SelectedEmpCode)>0 ");
            SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.WSCode=B.WSCode Order By A.EmpCode, A.Dt;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SelectedEmpCode", Param);
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "Dt", "WSCode", "WSName", "HolidayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            WSName = Sm.DrStr(dr, c[3]),
                            IsHoliday = Sm.DrStr(dr, c[4])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowData(ref List<EmpWorkSchedule> l)
        {
            if (Grd1.Cols.Count<=6) return; 

            string EmpCode = string.Empty;
            int r = 0;
            int c = 6;
            Grd1.BeginUpdate();
            l.ForEach(i =>
            {
                if (!Sm.CompareStr(EmpCode, i.EmpCode))
                {
                    EmpCode = i.EmpCode;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), i.EmpCode))
                        {
                            r = Row;
                            break;   
                        }
                    }
                }
                c = 6;
                while (c < Grd1.Cols.Count)
                {
                    if (Sm.CompareStr(i.Dt, Grd1.Header.Cells[0, c].Value.ToString()))
                    {
                        Grd1.Cells[r, c].Value = i.WSCode;
                        Grd1.Cells[r, c+1].Value = i.WSName;
                        Grd1.Cells[r, c + 1].ForeColor = i.IsHoliday ? Color.Red : Color.Black;
                        break;
                    }
                    c += 2;    
                }
            });
            Grd1.EndUpdate();
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, mCol-1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueAGCode(ref LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
            {
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            }
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            DteDocDt2.EditValue = DteDocDt1.EditValue; 
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            string DeptCode = Sm.GetLue(LueDeptCode);
            LueAGCode.EditValue = null;
            SetLueAGCode(ref LueAGCode, DeptCode);
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            string DeptCode = Sm.GetLue(LueDeptCode);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), DeptCode);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Attendance group");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void LueWSCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWSCode, new Sm.RefreshLue1(Sl.SetLueWSCode));
        }

        private void LueWSCode_Leave(object sender, EventArgs e)
        {
            if (LueWSCode.Visible && fAccept)
            {
                Grd1.Cells[fCell.RowIndex, mCol].ForeColor = Color.Black;
                if (Sm.GetLue(LueWSCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, mCol-1].Value = null;
                    Grd1.Cells[fCell.RowIndex, mCol].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, mCol-1].Value = Sm.GetLue(LueWSCode);
                    Grd1.Cells[fCell.RowIndex, mCol].Value = LueWSCode.GetColumnValue("Col2");
                    if (LueWSCode.GetColumnValue("Col3").ToString() == "Y")
                        Grd1.Cells[fCell.RowIndex, mCol].ForeColor = Color.Red;
                }
                LueWSCode.Visible = false;
            }
        }

        private void LueWSCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0 && e.ColIndex > 5)
            {
                mCol = e.ColIndex;
                LueRequestEdit(Grd1, LueWSCode, ref fCell, ref fAccept, e);
            }
            else
                e.DoDefault = false;
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Grd1.CurCell != null && Grd1.CurCell.ColIndex >6 && e.KeyCode == Keys.Enter && Grd1.CurCell.RowIndex != Grd1.Rows.Count - 1)
            {
                if (Grd1.CurCell.ColIndex == Grd1.Cols.Count - 1)
                    Sm.FocusGrd(Grd1, Grd1.CurCell.RowIndex+1, 7);
                else
                    Sm.FocusGrd(Grd1, Grd1.CurCell.RowIndex, Grd1.CurCell.ColIndex + 2);
            }
        }

        #endregion

        #region Button Event

        private void BtnClearData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to clear all schedule ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    for (int Col = 6; Col < Grd1.Cols.Count; Col++)
                    {
                        Grd1.Cells[Row, Col].Value = null;
                        Grd1.Cells[Row, Col].ForeColor = Color.Black;
                    }
                }

                Grd1.EndUpdate();

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy data ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1 || Sm.GetGrdStr(Grd1, 0, 0).Length==0) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 1; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        for (int Col = 6; Col < Grd1.Cols.Count; Col++)
                        {
                            Grd1.Cells[Row, Col].Value = Grd1.Cells[0, Col].Value;
                            Grd1.Cells[Row, Col].ForeColor = Grd1.Cells[0, Col].ForeColor;
                        }
                    }
                }

                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }

        }

        private void BtnCopyPattern_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy pattern ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row <= Grd1.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) CopyPattern(Row);
                
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void CopyPattern(int Row)
        {
            int TempCol = -1;
            for (int Col = 6; Col < Grd1.Cols.Count; Col++)
                if (Sm.GetGrdStr(Grd1, Row, Col).Length != 0) TempCol = Col;

            if (TempCol == -1)
            {
                //Sm.StdMsg(mMsgType.Warning, "No pattern existed.");
                return;
            }

            if (TempCol == Grd1.Cols.Count - 1)
            {
                //Sm.StdMsg(mMsgType.Warning, "No pattern copied.");
                return;
            }

            var l = new List<WorkSchedulePattern>();

            for (int Col = 6; Col <= TempCol; Col++)
                l.Add(new WorkSchedulePattern()
                {
                    Pattern = Sm.GetGrdStr(Grd1, Row, Col),
                    PatternColor = Grd1.Cells[Row, Col].ForeColor
                });

            var PatternCount = l.Count;
            int PatternIndex = 0;
            for (int Col = TempCol + 1; Col < Grd1.Cols.Count; Col++)
            {
                Grd1.Cells[Row, Col].Value = l[PatternIndex].Pattern;
                Grd1.Cells[Row, Col].ForeColor = l[PatternIndex].PatternColor;
                if (PatternIndex == PatternCount - 1)
                    PatternIndex = 0;
                else
                    PatternIndex += 1;
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 0].Value = "'" + Sm.GetGrdStr(Grd1, Row, 0);
                Grd1.Cells[Row, 2].Value = "'" + Sm.GetGrdStr(Grd1, Row, 2);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 0].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 0), Sm.GetGrdStr(Grd1, Row, 0).Length - 1);
                Grd1.Cells[Row, 2].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 2).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Class

        private class EmpWorkSchedule
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string WSName { get; set; }
            public bool IsHoliday { get; set; }
        }

        private class WorkSchedulePattern
        {
            public string Pattern { get; set; }
            public Color PatternColor { get; set; }
        }

        private class Holiday
        {
            public string HolDt { get; set; }
        }

        #endregion
    }
}
