﻿#region Update
/*
    17/05/2017 [ARI] tambah kolom MR Date, PORequest Date, PO Date, ReceivVd Date, dan Site
    29/05/2018 [TKG] Data PO yg ditampilkan hanya yang sudah di-approved
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
   
{
    public partial class FrmRptPurchasing1 : RunSystem.FrmBase6
    {
        #region Field
        
        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            DocDt = string.Empty, RefreshTitle = string.Empty;
        private bool 
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = false,
            mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchasing1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();

                panel4.Visible = mIsNoOfDaysPurchasedItemComparisonReportShowDoc;
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt?"Y":"N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = Sm.GetParameterBoo("IsNoOfDaysPurchasedItemComparisonReportShowDoc");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                SQL.AppendLine("Select ItCat.ItCtCode, ItCat.ItCtName, FilterMonth, ");
                SQL.AppendLine("TT.SiteName, TT.MaterialRequestDocNo, TT.MaterialRequestDocDt, TT.PORequestDocNo, TT.PORequestDocDt, TT.PODocNo, TT.PODocDt, TT.RecvVdDocNo, TT.RecvVdDocDt, ");
                SQL.AppendLine("Case when MRDtVsUsageDtCount <> 0 then (MRDtVsUsageDt/MRDtVsUsageDtCount) else 0 end As AvgMRDtVsUsageDt, ");
                SQL.AppendLine("Case when MRDtVsPOReqDt <> 0 then (MRDtVsPOReqDt/MRDtVsPOReqDtCount) else 0 end As AvgMRDtVsPOReqDt, ");
                SQL.AppendLine("Case when MRDtVsPODt <> 0 then (MRDtVsPODt/MRDtVsPODtCount) else 0 end As AvgMRDtVsPODt, ");
                SQL.AppendLine("Case when MRDtVsETADt <> 0 then (MRDtVsETADt/MRDtVsETADtCount) else 0 end As AvgMRDtVsETADt, ");
                SQL.AppendLine("Case when MRDtVsRecvDt <> 0 then (MRDtVsRecvDt/MRDtVsRecvDtCount) else 0 end As AvgMRDtVsRecvDt, ");
                SQL.AppendLine("Case when UsageDtVsPOReqDt <> 0 then (UsageDtVsPOReqDt/UsageDtVsPOReqDtCount) else 0 end As AvgUsageDtVsPOReqDt, ");
                SQL.AppendLine("Case when UsageDtVsPODt <> 0 then (UsageDtVsPODt/UsageDtVsPODtCount) else 0 end As AvgUsageDtVsPODt, ");
                SQL.AppendLine("Case when UsageDtVsETADt <> 0 then (UsageDtVsETADt/UsageDtVsETADtCount) else 0 end As AvgUsageDtVsETADt, ");
                SQL.AppendLine("Case when UsageDtVsRecvDt <> 0 then (UsageDtVsRecvDt/UsageDtVsRecvDtCount) else 0 end As AvgUsageDtVsRecvDt, ");
                SQL.AppendLine("Case when POReqDtVsPODt <> 0 then (POReqDtVsPODt/POReqDtVsPODtCount) else 0 end As AvgPOReqDtVsPODt, ");
                SQL.AppendLine("Case when POReqDtVsETADt <> 0 then (POReqDtVsETADt/POReqDtVsETADtCount) else 0 end As AvgPOReqDtVsETADt, ");
                SQL.AppendLine("Case when POReqDtVsRecvDt <> 0 then (POReqDtVsRecvDt/POReqDtVsRecvDtCount) else 0 end As AvgPOReqDtVsRecvDt, ");
                SQL.AppendLine("Case when RecvDtVsETADt <> 0 then (RecvDtVsETADt/RecvDtVsETADtCount) else 0 end As AvgRecvDtVsETADt, ");
                SQL.AppendLine("Case when PODtVsRecvDt <> 0 then (PODtVsRecvDt/PODtVsRecvDtCount) else 0 end As AvgPODtVsRecvDt ");
                SQL.AppendLine("From TblItemCategory ItCat ");
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select T.ItCtCode, T.FilterMonth,");
                SQL.AppendLine("    T.SiteName, T.MaterialRequestDocNo, T.MaterialRequestDocDt, T.PORequestDocNo, T.PORequestDocDt, T.PODocNo, T.PODocDt, T.RecvVdDocNo, T.RecvVdDocDt, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(UsageDt, MRDt), 0)) As MRDtVsUsageDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(UsageDt, MRDt) is null then 0 else 1 end) As MRDtVsUsageDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(POReqDt, MRDt), 0)) As MRDtVsPOReqDt,  ");
                SQL.AppendLine("    Sum(Case when DateDiff(POReqDt, MRDt) is null then 0 else 1 end) As MRDtVsPOReqDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(PODt, MRDt), 0)) As MRDtVsPODt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(PODt, MRDt) is null then 0 else 1 end) As MRDtVsPODtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, MRDt), 0)) As MRDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt, MRDt) is null then 0 else 1 end) As MRDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, MRDt), 0)) As MRDtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, MRDt) is null then 0 else 1 end) As MRDtVsRecvDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(POReqDt, UsageDt), 0)) As UsageDtVsPOReqDt,  ");
                SQL.AppendLine("    Sum(Case when DateDiff(POReqDt, UsageDt) is null then 0 else 1 end) As UsageDtVsPOReqDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(PODt, UsageDt), 0)) As UsageDtVsPODt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(PODt, UsageDt) is null then 0 else 1 end) As UsageDtVsPODtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, UsageDt), 0)) As UsageDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt, UsageDt) is null then 0 else 1 end) As UsageDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, UsageDt), 0)) As UsageDtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, UsageDt) is null then 0 else 1 end) As UsageDtVsRecvDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(PODt, POReqDt), 0)) As POReqDtVsPODt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(PODt, POReqDt) is null then 0 else 1 end) As POReqDtVsPODtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, POReqDt), 0)) As POReqDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt, POReqDt) is null then 0 else 1 end) As POReqDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, POReqDt), 0)) As POReqDtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, POReqDt) is null then 0 else 1 end) As POReqDtVsRecvDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, RecvDt), 0)) As RecvDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt,RecvDt) is null then 0 else 1 end) As RecvDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, PODt), 0)) As PODtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, PODt) is null then 0 else 1 end) As PODtVsRecvDtCount ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select Distinct T4.ItCtCode, ");

                if(LueMth.EditValue == null)
                    SQL.AppendLine("        Left(T2.DocDt, 4) As FilterMonth, ");
                else
                    SQL.AppendLine("        Left(T2.DocDt, 6) As FilterMonth, ");

                SQL.AppendLine("        T2.DocDt As MRDt, ");
                SQL.AppendLine("        T3.UsageDt, T6.DocDt As POReqDt, ");
                SQL.AppendLine("        T8.DocDt As PODt, T7.EstRecvDt As ETADt, T10.DocDt As RecvDt, ");
                SQL.AppendLine("        IfNull(T10.POInd, 'Y') As NotAutoPO, T11.SiteName, ");
                SQL.AppendLine("        T2.DocNo As MaterialRequestDocNo, T2.DocDt As MaterialRequestDocDt, T6.DocNo As PORequestDocNo, T6.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        T8.DocNo As PODocNo, T8.DocDt As PODocDt, T10.DocNo As RecvVdDocNo, T10.DocDt As RecvVdDocDt ");
                SQL.AppendLine("        From TblMaterialRequestHdr T2 ");
                SQL.AppendLine("        inner join tblmaterialrequestdtl T3 on T3.DocNo=T2.DocNo And T3.CancelInd='N' And T3.Status<>'C' ");
                SQL.AppendLine("        inner join tblitem T4 on T4.ItCode=T3.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T4.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("        left join tblporequestdtl T5 on T5.MaterialRequestDocNo=T3.DocNo AND T5.MaterialRequestDNo=T3.DNo  And T5.CancelInd='N' And T5.Status<>'C' ");
                SQL.AppendLine("        left join tblporequesthdr T6 on T6.DocNo=T5.DocNo ");
                SQL.AppendLine("        Left Join TblPODtl T7 ");
                SQL.AppendLine("            On T7.PORequestDocNo=T5.DocNo ");
                SQL.AppendLine("            And T7.PORequestDNo=T5.DNo ");
                SQL.AppendLine("            And T7.CancelInd='N' ");
                SQL.AppendLine("            And T7.DocNo In (Select DocNo From TblPOHdr Where Status='A') ");
                SQL.AppendLine("        Left Join TblPOHdr T8 On T8.DocNo=T7.DocNo And T7.Status='A' ");
                SQL.AppendLine("        left join tblrecvvddtl T9 on T9.PODocNo=T7.DocNo AND T9.PODNo=T7.DNo And T9.CancelInd='N' ");
                SQL.AppendLine("        left join tblrecvvdhdr T10 on T10.DocNo=T9.DocNo ");
                SQL.AppendLine("        left join tblsite T11 on T2.SiteCode=T11.SiteCode ");
                
                if(LueMth.EditValue == null)
                    SQL.AppendLine("        Where Left(T2.DocDt, 4)=@FilterMonth ");
                else
                    SQL.AppendLine("        Where Left(T2.DocDt, 6)=@FilterMonth ");

                SQL.AppendLine("    ) T  ");
                SQL.AppendLine("    Where T.NotAutoPO = 'Y' ");
                SQL.AppendLine("    Group By T.ItCtCode, T.FilterMonth, ");
                SQL.AppendLine("    T.MaterialRequestDocNo, T.MaterialRequestDocDt, T.PORequestDocNo, T.PORequestDocDt, T.PODocNo, T.PODocDt, T.RecvVdDocNo, T.RecvVdDocDt ");
                SQL.AppendLine(") TT On TT.ItCtCode=ItCat.ItCtCode ");
                SQL.AppendLine("Where TT.FilterMonth = @FilterMonth ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=ItCat.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("Select ItCat.ItCtCode, ItCat.ItCtName, FilterMonth, ");
                SQL.AppendLine("Null As SiteName, Null As MaterialRequestDocNo, Null As MaterialRequestDocDt, Null As PORequestDocNo, Null As PORequestDocDt, ");
                SQL.AppendLine("Null As PODocNo, Null As PODocDt, Null As RecvVdDocNo, Null As RecvVdDocDt,");
                SQL.AppendLine("Case when MRDtVsUsageDtCount <> 0 then (MRDtVsUsageDt/MRDtVsUsageDtCount) else 0 end As AvgMRDtVsUsageDt, ");
                SQL.AppendLine("Case when MRDtVsPOReqDt <> 0 then (MRDtVsPOReqDt/MRDtVsPOReqDtCount) else 0 end As AvgMRDtVsPOReqDt, ");
                SQL.AppendLine("Case when MRDtVsPODt <> 0 then (MRDtVsPODt/MRDtVsPODtCount) else 0 end As AvgMRDtVsPODt, ");
                SQL.AppendLine("Case when MRDtVsETADt <> 0 then (MRDtVsETADt/MRDtVsETADtCount) else 0 end As AvgMRDtVsETADt, ");
                SQL.AppendLine("Case when MRDtVsRecvDt <> 0 then (MRDtVsRecvDt/MRDtVsRecvDtCount) else 0 end As AvgMRDtVsRecvDt, ");
                SQL.AppendLine("Case when UsageDtVsPOReqDt <> 0 then (UsageDtVsPOReqDt/UsageDtVsPOReqDtCount) else 0 end As AvgUsageDtVsPOReqDt, ");
                SQL.AppendLine("Case when UsageDtVsPODt <> 0 then (UsageDtVsPODt/UsageDtVsPODtCount) else 0 end As AvgUsageDtVsPODt, ");
                SQL.AppendLine("Case when UsageDtVsETADt <> 0 then (UsageDtVsETADt/UsageDtVsETADtCount) else 0 end As AvgUsageDtVsETADt, ");
                SQL.AppendLine("Case when UsageDtVsRecvDt <> 0 then (UsageDtVsRecvDt/UsageDtVsRecvDtCount) else 0 end As AvgUsageDtVsRecvDt, ");
                SQL.AppendLine("Case when POReqDtVsPODt <> 0 then (POReqDtVsPODt/POReqDtVsPODtCount) else 0 end As AvgPOReqDtVsPODt, ");
                SQL.AppendLine("Case when POReqDtVsETADt <> 0 then (POReqDtVsETADt/POReqDtVsETADtCount) else 0 end As AvgPOReqDtVsETADt, ");
                SQL.AppendLine("Case when POReqDtVsRecvDt <> 0 then (POReqDtVsRecvDt/POReqDtVsRecvDtCount) else 0 end As AvgPOReqDtVsRecvDt, ");
                SQL.AppendLine("Case when RecvDtVsETADt <> 0 then (RecvDtVsETADt/RecvDtVsETADtCount) else 0 end As AvgRecvDtVsETADt, ");
                SQL.AppendLine("Case when PODtVsRecvDt <> 0 then (PODtVsRecvDt/PODtVsRecvDtCount) else 0 end As AvgPODtVsRecvDt ");
                SQL.AppendLine("From tblItemCategory ItCat ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("    (Select T.ItCtCode, T.FilterMonth, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(UsageDt, MRDt), 0)) As MRDtVsUsageDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(UsageDt, MRDt) is null then 0 else 1 end) As MRDtVsUsageDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(POReqDt, MRDt), 0)) As MRDtVsPOReqDt,  ");
                SQL.AppendLine("    Sum(Case when DateDiff(POReqDt, MRDt) is null then 0 else 1 end) As MRDtVsPOReqDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(PODt, MRDt), 0)) As MRDtVsPODt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(PODt, MRDt) is null then 0 else 1 end) As MRDtVsPODtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, MRDt), 0)) As MRDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt, MRDt) is null then 0 else 1 end) As MRDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, MRDt), 0)) As MRDtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, MRDt) is null then 0 else 1 end) As MRDtVsRecvDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(POReqDt, UsageDt), 0)) As UsageDtVsPOReqDt,  ");
                SQL.AppendLine("    Sum(Case when DateDiff(POReqDt, UsageDt) is null then 0 else 1 end) As UsageDtVsPOReqDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(PODt, UsageDt), 0)) As UsageDtVsPODt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(PODt, UsageDt) is null then 0 else 1 end) As UsageDtVsPODtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, UsageDt), 0)) As UsageDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt, UsageDt) is null then 0 else 1 end) As UsageDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, UsageDt), 0)) As UsageDtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, UsageDt) is null then 0 else 1 end) As UsageDtVsRecvDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(PODt, POReqDt), 0)) As POReqDtVsPODt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(PODt, POReqDt) is null then 0 else 1 end) As POReqDtVsPODtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, POReqDt), 0)) As POReqDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt, POReqDt) is null then 0 else 1 end) As POReqDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, POReqDt), 0)) As POReqDtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, POReqDt) is null then 0 else 1 end) As POReqDtVsRecvDtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(ETADt, RecvDt), 0)) As RecvDtVsETADt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(ETADt,RecvDt) is null then 0 else 1 end) As RecvDtVsETADtCount, ");
                SQL.AppendLine("    Sum(IfNull(DateDiff(RecvDt, PODt), 0)) As PODtVsRecvDt, ");
                SQL.AppendLine("    Sum(Case when DateDiff(RecvDt, PODt) is null then 0 else 1 end) As PODtVsRecvDtCount ");
                SQL.AppendLine("    from (");
                SQL.AppendLine("        select Distinct T4.ItCtCode, T2.DocNo, Left(T2.DocDt, 6) As FilterMonth, T2.DocDt As MRDt, T3.UsageDt, T6.DocDt As POReqDt, ");
                SQL.AppendLine("        T8.DocDt As PODt, T7.EstRecvDt As ETADt, T10.DocDt As RecvDt, IfNull(T10.POInd, 'Y') As NotAutoPO ");
                SQL.AppendLine("        from tblmaterialrequesthdr T2 ");
                SQL.AppendLine("        inner join tblmaterialrequestdtl T3 on T3.DocNo=T2.DocNo And T3.CancelInd='N' And T3.Status<>'C' ");
                SQL.AppendLine("        inner join tblitem T4 on T4.ItCode=T3.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T4.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("        left join tblporequestdtl T5 on T5.MaterialRequestDocNo=T3.DocNo AND T5.MaterialRequestDNo=T3.DNo  And T5.CancelInd='N' And T5.Status<>'C' ");
                SQL.AppendLine("        left join tblporequesthdr T6 on T6.DocNo=T5.DocNo ");
                SQL.AppendLine("        left join tblpodtl T7 on T7.PORequestDocNo=T5.DocNo and T7.PORequestDNo=T5.DNo And T7.CancelInd='N' ");
                SQL.AppendLine("            And T7.DocNo In (Select DocNo From TblPOHdr Where Status='A') ");
                SQL.AppendLine("        left join tblpohdr T8 on T7.DocNo=T8.DocNo And T8.Status='A' ");
                SQL.AppendLine("        left join tblrecvvddtl T9 on T9.PODocNo=T7.DocNo AND T9.PODNo=T7.DNo And T9.CancelInd='N' ");
                SQL.AppendLine("        left join tblrecvvdhdr T10 on T10.DocNo=T9.DocNo ");
                SQL.AppendLine("        Where Left(T2.DocDt, 6)=@FilterMonth )T  ");
                SQL.AppendLine("    where T.NotAutoPO = 'Y' ");
                SQL.AppendLine("    Group By T.ItCtCode, T.FilterMonth) TT On TT.ItCtCode=ItCat.ItCtCode ");
                SQL.AppendLine("Where FilterMonth = @FilterMonth");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=ItCat.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Category"+Environment.NewLine+"Code", 
                        "Category"+Environment.NewLine+"Name",
                        "Month",
                        "MR"+Environment.NewLine+"Vs"+Environment.NewLine+"Usage",   
                        "MR"+Environment.NewLine+"Vs"+Environment.NewLine+"PO Request", 

                        //6-10
                        "MR"+Environment.NewLine+"Vs"+Environment.NewLine+"PO", 
                        "MR"+Environment.NewLine+"Vs"+Environment.NewLine+"ETA", 
                        "MR"+Environment.NewLine+"Vs"+Environment.NewLine+"Receiving", 
                        "Usage"+Environment.NewLine+"Vs"+Environment.NewLine+"PO Request", 
                        "Usage"+Environment.NewLine+"Vs"+Environment.NewLine+"PO", 
                        
                        //11-15
                        "Usage"+Environment.NewLine+"Vs"+Environment.NewLine+"ETA", 
                        "Usage"+Environment.NewLine+"Vs"+Environment.NewLine+"Receiving", 
                        "PO Request"+Environment.NewLine+"Vs"+Environment.NewLine+"PO", 
                        "PO Request"+Environment.NewLine+"Vs"+Environment.NewLine+"ETA", 
                        "PO Request"+Environment.NewLine+"Vs"+Environment.NewLine+"Receiving", 

                        //16-20
                        "Receiving"+Environment.NewLine+"Vs"+Environment.NewLine+"ETA", 
                        "Material Request#",
                        "PO Request#",
                        "PO#",
                        "Received#",

                        //21-25
                        "PO"+Environment.NewLine+"Vs"+Environment.NewLine+"Receiving", 
                        "Material Request"+Environment.NewLine+"Date",
                        "PO Request"+Environment.NewLine+"Date",
                        "PO Date",
                        "Received"+Environment.NewLine+"Date",

                        //26
                        "Site"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 50, 60, 80, 
                        
                        //6-10
                        60, 60, 80, 80, 80, 
                        
                        //11-15
                        80, 80, 80, 80, 80, 

                        //16-20
                        80, 130, 130, 130, 150,

                        //21-25
                        80, 100, 100, 100, 100,

                        //26
                        120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 22, 23, 24, 25 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 21 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 },false);
            Grd1.Cols[21].Move(16);
            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                Grd1.Cols[26].Move(3);
                Grd1.Cols[17].Move(4);
                Grd1.Cols[22].Move(5);
                Grd1.Cols[18].Move(6);
                Grd1.Cols[23].Move(7);
                Grd1.Cols[19].Move(8);
                Grd1.Cols[24].Move(9);
                Grd1.Cols[20].Move(10);
                Grd1.Cols[25].Move(11);

 
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 22, 23, 24, 25, 26 }, false);
            
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year, Month);

            if (Year == string.Empty && Month == string.Empty)
            {
                if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
                    Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                else
                    Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty && (!mIsNoOfDaysPurchasedItemComparisonReportShowDoc))
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " ";

                    var cm = new MySqlCommand();
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.CmParam<String>(ref cm, "@FilterMonth", DocDt);
                    Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "TT.MaterialRequestDocNo", false);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "ItCat.ItCtCode", true);
                    RefreshTitle = "Period : " + Month + "/" + Year + ((LueItCtCode.Text == string.Empty || LueItCtCode.Text == "[Empty]") ? "" : Environment.NewLine + LueItCtCode.Text);

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            GetSQL() + Filter + " Order By ItCat.ItCtName;",
                            new string[]
                            { 
                                //0
                                "ItCtCode", 

                                //1-5
                                "ItCtName", "FilterMonth", "AvgMRDtVsUsageDt", "AvgMRDtVsPOReqDt", "AvgMRDtVsPODt", 
                                
                                //6-10
                                "AvgMRDtVsETADt", "AvgMRDtVsRecvDt", "AvgUsageDtVsPOReqDt", "AvgUsageDtVsPODt", "AvgUsageDtVsETADt", 
     
                                //11-15
                                "AvgUsageDtVsRecvDt", "AvgPOReqDtVsPODt", "AvgPOReqDtVsETADt", "AvgPOReqDtVsRecvDt", "AvgRecvDtVsETADt",

                                //16-20
                                "MaterialRequestDocNo", "PORequestDocNo", "PODocNo", "RecvVdDocNo", "AvgPODtVsRecvDt",

                                //21-24
                                "MaterialRequestDocDt", "PORequestDocDt", "PODocDt", "RecvVdDocDt", "SiteName"
                            },

                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 23);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 24);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mIsFilterByItCt?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Material Request#");
        }

        #endregion
    }
}
