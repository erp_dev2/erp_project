﻿#region update

/*
    31/05/2022 [SET/PRODUCT] Menu dialog baru
    15/06/2022 [SET/PRODUCT] Penyesuaian
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionDebtSecuritiesDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAcquisitionDebtSecurities mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAcquisitionDebtSecuritiesDlg(FrmAcquisitionDebtSecurities FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BankAcCode, BankAcNm ");
            SQL.AppendLine("From TblBankAccount ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("And Find_In_Set(BankAcTp, @BankAccountTypeForInvestment) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.ReadOnly = true;
            //Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Account",
                        "Investment Bank",
                        //"Date",
                        //"Status",
                        //"Down Date",
                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 170
                    }
                );
            //Sm.GrdColButton(Grd1, new int[] { 2 });
            //Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            //Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@BankAccountTypeForInvestment", mFrmParent.mBankAccountTypeForInvestment);
                Sm.FilterStr(ref Filter, ref cm, TxtBankAcc.Text, new string[] { "BankAcCode", "BankAcNm" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt;",
                        new string[]
                        {
                            //0
                            "BankAcCode", 

                            //1
                            "BankAcNm"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int r = Grd1.CurRow.Index;

                mFrmParent.mInvestmentBankAccount = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.TxtInvestmentBankAcc.EditValue = Sm.GetGrdStr(Grd1, r, 2);
                //mFrmParent.ClearGrd();

                this.Close();
            }
        }

        #endregion

        #region Event

        private void ChkBankAcc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank Account");
        }
        private void TxtBankAcc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion
    }
}
