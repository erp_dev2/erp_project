﻿#region Update
/*
    29/07/2022 [MYA/PRODUCT] Penyesuaian reporting aging invoice AP (poin 2 di requirement - 2.1, 2.2)
    25/01/2023 [RDA/MNET] penyesuaian kolom overdue, hide kolom paid date
    14/02/2023 [HAR/TWC] aturan memunculkan data  ketika parameter "IsRptAgingAPPaidItemUsePaymentDate" aktif
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingAPPaidItem2 : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = string.Empty; 
        internal string mAccessInd = string.Empty, mSQL = string.Empty;
        private bool 
            mIsSiteMandatory = false,
            mIsAgingAPPaidItemShowTaxInfo = false,
            mIsRptAgingAPPaidItemShowPOInfo = false,
            mIsFilterByItCt = false,
            mIsRptAgingAPPaidItemUsePaymentDate = false,
            mIsRptShowVoucherLoop = false,
            mIsOutgoingPaymentUsePIWithCOA = false,
            mIsPurchaseInvoiceUseCOATaxInd = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsAllProfitCenterSelected = false,
            mIsFilterByProfitCenter = false
            ;
        private string
            mRptAgingAPPaidItemDlgFormat = string.Empty,
            mQueryProfitCenter = string.Empty;
        private List<Tax> mlTax = null;
        private List<String> mlProfitCenter;
        private int mFirstColumnForTax = -1;
        
        #endregion

        #region Constructor

        public FrmRptAgingAPPaidItem2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                if (mIsAgingAPPaidItemShowTaxInfo)
                {
                    mlTax = new List<Tax>();
                    SetTax();
                }

                if (!mIsRptAgingAPPaidItemUsePaymentDate)
                {
                    LblPaymentDt.Visible = false;
                    label10.Visible = false;
                    DtePaymentDt1.Visible = false;
                    DtePaymentDt2.Visible = false;
                    ChkPaymentDt.Visible = false;
                }
                
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueVdCtCode(ref LueVdCtCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                SetLueType();
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sm.SetPeriod(ref DteDueDt1, ref DteDueDt2, ref ChkDueDt, -30);
                Sm.SetPeriod(ref DtePaymentDt1, ref DtePaymentDt2, ref ChkPaymentDt, -30);
                base.FrmLoad(sender, e);
                if (!mIsFicoUseMultiProfitCenterFilter)
                {
                    label9.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
                mlProfitCenter = new List<String>();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.*, ");
            SQL.AppendLine("Tbl2.VdName, Tbl6.EntName, Tbl4.SiteName, Tbl5.ProfitCenterName, ");
            SQL.AppendLine("Case Tbl1.Status When 'O' Then 'Outstanding' When 'F' Then 'Fulfilled' When 'P' Then 'Partial' End As StatusDesc, ");
            SQL.AppendLine("Case Tbl1.Type When '1' Then Tbl7.DocDt When '2' Then Tbl8.DocDt End As PaidDt ");
            SQL.AppendLine("From (");

            #region Purchase Invoice

            SQL.AppendLine("    Select DocDt, Period, '1' As Type, 'Purchase Invoice' As TypeDesc, ");
            SQL.AppendLine("    CurCode, VdCode, DocNo, VdInvNo, PaymentTypeDesc, BankAccount,");
            SQL.AppendLine("    InvoiceAmt, DownPayment, PaidAmt, VoucherDocNo, Balance, ");
            SQL.AppendLine("    DueDt, EarliestPaidDt, DueToPaidDays, AgingDays, ");
            SQL.AppendLine("    Case When Balance<=0 Then 'F' Else ");
	        SQL.AppendLine("        Case When Balance=InvoiceAmt Then 'O' Else 'P' End ");
            SQL.AppendLine("    End As Status, ");
            SQL.AppendLine("    Case When (AgingDays < 0) then Balance Else 0.00 End As AgingCurrent, ");
            SQL.AppendLine("    Case When (AgingDays > 0 And AgingDays < 31) then Balance Else 0.00 End As Aging1To30,	");
            SQL.AppendLine("    Case When (AgingDays > 30 And AgingDays < 91) then Balance Else 0.00 End As Aging31To90, ");
            SQL.AppendLine("    Case When (AgingDays > 60 And AgingDays < 181) then Balance Else 0.00 End As Aging91To180,  ");
            SQL.AppendLine("    Case When (AgingDays > 90 And AgingDays < 361) then Balance Else 0.00 End As Aging181To360, ");
            SQL.AppendLine("    Case When (AgingDays > 90 And AgingDays < 721) then Balance Else 0.00 End As Aging361To720, ");
            SQL.AppendLine("    Case When (AgingDays > 720) then Balance Else 0.00 End As AgingOver720, ");

            SQL.AppendLine("    DeptName, PtName, SiteCode, PODocNo, PIDocDt,  LPPB ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocDt, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("        A.CurCode, A.VdCode, A.DocNo, A.VdInvNo, F.PaymentTypeDesc, F.BankAccount,");
            if (mIsOutgoingPaymentUsePIWithCOA || mIsPurchaseInvoiceUseCOATaxInd)
            {
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When A.COATaxInd = 'Y' then ((A.Amt+A.TaxAmt)+IfNull(G.Amt, 0.00)) ");
                SQL.AppendLine("    Else (A.Amt+A.TaxAmt) ");
                SQL.AppendLine("End As InvoiceAmt, ");
            }
            else
                SQL.AppendLine("        (A.Amt+A.TaxAmt) As InvoiceAmt, ");
            SQL.AppendLine("        A.DownPayment As DownPayment, IfNull(B.PaidAmt, 0) As PaidAmt, B.VoucherDocNo, ");
            if (mIsOutgoingPaymentUsePIWithCOA || mIsPurchaseInvoiceUseCOATaxInd)
            {
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When A.COATaxInd = 'Y' then (A.Amt + A.TaxAmt + IfNull(G.Amt, 0.00))- A.DownPayment - IfNull(B.PaidAmt, 0) ");
                SQL.AppendLine("    Else (A.Amt + A.TaxAmt - A.DownPayment - IfNull(B.PaidAmt, 0)) ");
                SQL.AppendLine("End As Balance, ");
            }
            else
                SQL.AppendLine("        A.Amt + A.TaxAmt - A.DownPayment - IfNull(B.PaidAmt, 0) As Balance, ");
            SQL.AppendLine("        A.DueDt, B.EarliestPaidDt, ");
            SQL.AppendLine("        IfNull(DateDiff(Date_Format(B.EarliestPaidDt, '%Y%m%d'), Date_Format(A.DueDt, '%Y%m%d')), 0) As DueToPaidDays, ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("        DateDiff(@BalanceDt, Date_Format(A.DueDt, '%Y%m%d')) As AgingDays, ");
            }
            else
                SQL.AppendLine("        DateDiff(D.CurrentDateTime, Date_Format(A.DueDt, '%Y%m%d')) As AgingDays, ");
            SQL.AppendLine("        E.DeptName, E.PtName, A.SiteCode, E.PODocNo, E.PIDocDt,  E.LPPB ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select DocNo, Min(DocDt) As EarliestPaidDt, Sum(Amt) As PaidAMt, Group_Concat(IfNull(VoucherDocNo, '')) VoucherDocNo ");
		    SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select T2.InvoiceDocNo As DocNo, T3.DocDt, T2.Amt, T3.DocNo VoucherDocNo ");
			SQL.AppendLine("                From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("                Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T1.DocDt <= @BalanceDt ");
            }
            SQL.AppendLine("                Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("                Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("                    On T2.InvoiceDocNo=T4.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T4.DocDt <= @BalanceDt ");
            }
            else
            {
                SQL.AppendLine("                    And T4.DocDt Between @DocDt1 And @DocDt2 ");
            }
			SQL.AppendLine("                Where T1.CancelInd='N' ");
            if (mIsRptAgingAPPaidItemUsePaymentDate)
            {
                if (DtePaymentDt1.Text.Length > 0 && DtePaymentDt2.Text.Length > 0)
                {
                    SQL.AppendLine("             And T3.DocDt Between @PaymentDt1 And @PaymentDt2 ");
                }
                else
                {
                    SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
                }
            }
            SQL.AppendLine("                And T1.Status In ('O', 'A') ");
            SQL.AppendLine("                Union All ");
            SQL.AppendLine("                Select T1.PurchaseInvoiceDocNo As DocNo, T1.DocDt, T1.Amt, '' As VoucherDocNo ");
			SQL.AppendLine("                From TblAPSHdr T1 ");
            SQL.AppendLine("                Inner Join TblPurchaseInvoiceHdr T2 ");
            SQL.AppendLine("                    On T1.PurchaseInvoiceDocNo=T2.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T2.DocDt <= @BalanceDt ");
            }
            else
            {
                SQL.AppendLine("                    And T2.DocDt Between @DocDt1 And @DocDt2 ");
            }
            SQL.AppendLine("                Where T1.CancelInd='N' ");
		    SQL.AppendLine("            ) Tbl Group By DocNo ");
	        SQL.AppendLine("        ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Left Join (Select Left(CurrentDateTime(), 8) As  CurrentDateTime) D On 0=0 ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
        	SQL.AppendLine("	            Select T10.DocNo As PIDocNo, Group_Concat(Distinct T7.DeptName Separator ', ') As DeptName,  ");
            SQL.AppendLine("	            Group_Concat(Distinct T9.PtName Separator ', ') As PtName, T10.DocDt PIDocDt, ");
            SQL.AppendLine("	            Group_Concat(Distinct T1.RecvVdDocNo  Separator ', ') As LPPB, ");
            SQL.AppendLine("	            Group_Concat(Distinct T3.DocNo  Separator ', ') As PODocNo, T7.DeptCode ");
			SQL.AppendLine("	            From TblPurchaseInvoiceHdr T10  ");
			SQL.AppendLine("	            Inner Join TblPurchaseInvoiceDtl T1 On T10.DocNo = T1.DocNo ");
			SQL.AppendLine("	            Inner Join TblRecvVdDtl T2 On T1.RecvVdDocNo=T2.DocNo And T1.RecvVdDNo=T2.DNo  ");
			SQL.AppendLine("	            Inner Join TblPODtl T3 On T2.PODocNo=T3.DocNo And T2.PODNo=T3.DNo  ");
			SQL.AppendLine("	            Inner Join TblPORequestDtl T4 On T3.PORequestDocNo=T4.DocNo And T3.PORequestDNo=T4.DNo  ");
            SQL.AppendLine("	            Inner Join TblMaterialRequestDtl T5 On T4.MaterialRequestDocNo=T5.DocNo And T4.MaterialRequestDNo=T5.DNo  ");
			SQL.AppendLine("	            Inner JOin TblMaterialRequestHdr T6 On T4.MaterialRequestDocNo = T6.DocNo ");
            SQL.AppendLine("	            Left Join TblDepartment T7 On T6.DeptCode = T7.DeptCode ");
			SQL.AppendLine("	            Inner Join TblQtHdr T8 On T4.QtDocNo=T8.DocNo  ");
			SQL.AppendLine("	            Left Join TblPaymentTerm T9 On T8.PtCode=T9.PtCode ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            Where T10.DocDt <= @BalanceDt ");
            }
            else
            {
                SQL.AppendLine("	            Where T10.DocDt Between @DocDt1 And @DocDt2 ");
            }
			SQL.AppendLine("	            Group By T10.DocNo ");
            SQL.AppendLine("        )E On A.DocNo = E.PIDocNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            SELECT T1.DocNo, Group_Concat(Distinct T5.OptDesc Separator ', ')  As PaymentTypeDesc, ");
            SQL.AppendLine("            GROUP_CONCAT(DISTINCT T6.BankAcNo,' [',T6.BankAcNm,'] ', T7.BankName SEPARATOR ', ') AS BankAccount ");
            SQL.AppendLine("            From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T2.InvoiceType='1' And T1.DocNo=T2.InvoiceDocNo ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("            Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo And T4.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblOption T5 On T4.PaymentType=T5.OptCode And T5.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("            Inner Join TblBankAccount T6 ON T4.BankAcCode=T6.BankAcCode ");
            SQL.AppendLine("            Inner Join TblBank T7 ON T6.BankCode=T7.BankCode ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T1.DocDt <= @BalanceDt ");
            }
            else
            {
                SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2 ");
            }
            SQL.AppendLine("            Group By T1.DocNo ");
            SQL.AppendLine("        ) F On A.DocNo=F.DocNo ");
            if (mIsOutgoingPaymentUsePIWithCOA || mIsPurchaseInvoiceUseCOATaxInd)
            {
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.DocNo, ");
                SQL.AppendLine("            Case T4.AcType ");
                SQL.AppendLine("                When 'D' Then Sum(T2.DAmt-T2.CAmt) ");
                SQL.AppendLine("                Else Sum(T2.CAmt-T2.DAmt) ");
                SQL.AppendLine("            End As Amt ");
                SQL.AppendLine("            From TblPurchaseInvoiceHdr T1 ");
                SQL.AppendLine("            Inner Join TblPurchaseInvoiceDtl4 T2 On T1.DocNo = T2.DocNo ");
                if (DteBalanceDt.Text.Length > 0)
                {
                    SQL.AppendLine("            And T1.DocDt <= @BalanceDt ");
                }
                else
                {
                    SQL.AppendLine("                And T1.DocDt Between @DocDt1 And @DocDt2 ");
                }
                SQL.AppendLine("            Inner Join TblParameter T3 On T3.ParCode = 'VendorAcNoAP' And T3.ParValue Is Not Null ");
                SQL.AppendLine("                And T2.AcNo = Concat(T3.ParValue, T1.VdCode) ");
                SQL.AppendLine("            Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("            Group By T1.DocNo ");
                SQL.AppendLine("        ) G On A.DocNo=G.DocNo ");
            }
            SQL.AppendLine("        Where A.CancelInd='N' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And A.DocDt <= @BalanceDt ");
            }
            else
            {
                SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            }
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=E.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    ) X ");

            #endregion

            SQL.AppendLine("    Union All ");

            #region Purchase Return Invoice

            SQL.AppendLine("    Select DocDt, Period, '2' As Type, 'Purchase Return Invoice' As TypeDesc, CurCode, VdCode, DocNo, Null As VdInvNo, PaymentTypeDesc, BankAccount, ");
            SQL.AppendLine("    -1.00*InvoiceAmt, DownPayment, -1.00*PaidAmt, VoucherDocNo ,-1.00*Balance, ");
            SQL.AppendLine("    DueDt, EarliestPaidDt, DueToPaidDays, AgingDays, ");
            SQL.AppendLine("    Case When Balance<=0 Then 'F' Else ");
	        SQL.AppendLine("        Case When Balance=InvoiceAmt Then 'O' Else 'P' End ");
            SQL.AppendLine("    End As Status, ");
            SQL.AppendLine("    0.00 As AgingCurrent, ");
            SQL.AppendLine("    0.00 As Aging1To30, ");
            SQL.AppendLine("    0.00 As Aging31To90, ");
            SQL.AppendLine("    0.00 As Aging91To180, ");
            SQL.AppendLine("    0.00 As Aging181To360, ");
            SQL.AppendLine("    0.00 As Aging361To720, ");
            SQL.AppendLine("    0.00 As AgingOver720, ");
            SQL.AppendLine("    DeptName, PtName, SiteCode,  Null As PODocNo,  Null As PIDocDt,  Null As LPPB ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select ");
            SQL.AppendLine("        A.DocDt, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("        A.CurCode, A.VdCode, A.DocNo, D.PaymentTypeDesc, D.BankAccount, "); 
            SQL.AppendLine("        A.Amt As InvoiceAmt, 0.00 As DownPayment, IfNull(B.PaidAmt, 0) As PaidAmt, B.VoucherDocNo,  ");
            SQL.AppendLine("        A.Amt-IfNull(B.PaidAmt, 0.00) As Balance, ");
            SQL.AppendLine("        Null As DueDt, B.EarliestPaidDt, ");
            SQL.AppendLine("        0.00 As DueToPaidDays, 0.00 As AgingDays, C.DeptName, C.PtName, A.SiteCode ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo As DocNo, Min(T3.DocDt) As EarliestPaidDt, Sum(T2.Amt) As PaidAMt, Group_Concat(IfNull(T3.DocNo, '')) VoucherDocNo ");
	        SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
	        SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("        And T1.DocDt <= @BalanceDt ");
            }
            SQL.AppendLine("            Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblPurchaseReturnInvoiceHdr T4 ");
            SQL.AppendLine("                On T2.InvoiceDocNo=T4.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T4.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("                And T4.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("            Where T1.CancelInd='N' ");
            if (mIsRptAgingAPPaidItemUsePaymentDate)
            {
                if (DtePaymentDt1.Text.Length > 0 && DtePaymentDt2.Text.Length > 0)
                {
                    SQL.AppendLine("             And T3.DocDt Between @PaymentDt1 And @PaymentDt2 ");
                }
                else
                {
                    SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
                }
            }
            SQL.AppendLine("            And IfNull(T1.Status, 'O') In ('O', 'A') ");
	        SQL.AppendLine("            Group By T2.InvoiceDocNo ");
	        SQL.AppendLine("        ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
    		SQL.AppendLine("            Select T10.DocNo As PRIDocNo, Group_Concat(Distinct T7.DeptName Separator ', ') As DeptName, ");
			SQL.AppendLine("            Group_Concat(Distinct T9.PtName Separator ', ') As PtName ");
			SQL.AppendLine("            From TblPurchaseReturnInvoiceHdr T10  ");
			SQL.AppendLine("            Inner Join TblPurchaseReturnInvoiceDtl T1 On T10.DocNo = T1.DocNo ");
			SQL.AppendLine("            Left Join TblDOVdDtl T11 On T1.DOVdDocNo = T11.DocNo And T1.DOVdDNo = T11.DNo ");
			SQL.AppendLine("            Left Join TblRecvVdDtl T2 On T11.RecvVdDocNo=T2.DocNo And T11.RecvVdDNo=T2.DNo  ");
			SQL.AppendLine("            Left Join TblPODtl T3 On T2.PODocNo=T3.DocNo And T2.PODNo=T3.DNo  ");
			SQL.AppendLine("            Left Join TblPORequestDtl T4 On T3.PORequestDocNo=T4.DocNo And T3.PORequestDNo=T4.DNo  ");
			SQL.AppendLine("            Left Join TblMaterialRequestDtl T5 On T4.MaterialRequestDocNo=T5.DocNo And T4.MaterialRequestDNo=T5.DNo  ");
			SQL.AppendLine("            Left JOin TblMaterialRequestHdr T6 On T4.MaterialRequestDocNo = T6.DocNo ");
            SQL.AppendLine("            Left Join TblDepartment T7 On T6.DeptCode = T7.DeptCode ");
			SQL.AppendLine("            Left Join TblQtHdr T8 On T4.QtDocNo=T8.DocNo  ");
			SQL.AppendLine("            Left Join TblPaymentTerm T9 On T8.PtCode=T9.PtCode ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T10.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("            Where T10.DocDt Between @DocDt1 And @DocDt2 ");
			SQL.AppendLine("            Group By T10.DocNo ");
            SQL.AppendLine("        ) C On A.DocNo = C.PRIDocNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select T1.DocNo, Group_Concat(Distinct T5.OptDesc Separator ', ')  As PaymentTypeDesc, ");
            SQL.AppendLine("            GROUP_CONCAT(DISTINCT T6.BankAcNo,' [',T6.BankAcNm,'] ', T7.BankName SEPARATOR ', ') AS BankAccount ");
            SQL.AppendLine("            From TblPurchaseReturnInvoiceHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T2.InvoiceType='2' And T1.DocNo=T2.InvoiceDocNo ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("            Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo And T4.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblOption T5 On T4.PaymentType=T5.OptCode And T5.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("            Inner Join TblBankAccount T6 ON T4.BankAcCode=T6.BankAcCode ");
            SQL.AppendLine("            Inner Join Tblbank T7 ON T6.BankCode=T7.BankCode "); 
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T1.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            Group By T1.DocNo ");
            SQL.AppendLine("        ) D On A.DocNo=D.DocNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And A.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) X ");

            #endregion

            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Inner Join TblVendor Tbl2 On Tbl1.VdCode=Tbl2.VdCode ");
            SQL.AppendLine("Left Join TblSite Tbl4 On Tbl1.SiteCode=Tbl4.SiteCode ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("Inner Join TblProfitCenter Tbl5 On Tbl4.ProfitCenterCode=Tbl5.ProfitCenterCode ");
                if (ChkProfitCenterCode.Checked) SQL.AppendLine(mQueryProfitCenter);
                //if (ChkProfitCenterCode.Checked) SQL.AppendLine("And Find_In_Set(Tbl4.ProfitCenterCode, @ProfitCenterCode) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblProfitCenter Tbl5 On Tbl4.ProfitCenterCode=Tbl5.ProfitCenterCode ");
            }
            SQL.AppendLine("Left Join TblEntity Tbl6 On Tbl5.EntCode=Tbl6.EntCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Date_Format(DocDt, '%d/%m/%Y') Order By DocDt ASC Separator ', ') As DocDt, docdt docdt2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, T3.DocDt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("        Inner Join TblVoucherHdr T3 ");
            SQL.AppendLine("            On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
            SQL.AppendLine("            And T3.CancelInd='N' ");
            if (mIsRptAgingAPPaidItemUsePaymentDate)
            {
                if (DtePaymentDt1.Text.Length > 0 && DtePaymentDt2.Text.Length > 0)
                {
                    SQL.AppendLine("             And T3.DocDt Between @PaymentDt1 And @PaymentDt2 ");
                }
                else
                {
                    SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
                }
            }
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T4.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T4.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("            And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T1.PurchaseInvoiceDocNo As DocNo, ");
            SQL.AppendLine("        T1.DocDt ");
            SQL.AppendLine("        From TblAPSHdr T1 ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T2 ");
            SQL.AppendLine("            On T1.PurchaseInvoiceDocNo=T2.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T2.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            if (mIsRptAgingAPPaidItemUsePaymentDate)
            {
                if (DtePaymentDt1.Text.Length > 0 && DtePaymentDt2.Text.Length > 0)
                {
                    SQL.AppendLine("             And T1.DocDt Between @PaymentDt1 And @PaymentDt2 ");
                }
                else
                {
                    SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2 ");
                }
            }
            SQL.AppendLine("    ) Tbl Group By DocNo ");
            SQL.AppendLine(") Tbl7 On Tbl1.DocNo=Tbl7.DocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Date_Format(DocDt, '%d/%m/%Y') Order By DocDt ASC Separator ', ') As DocDt, docdt docdt2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, T3.DocDt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblVoucherHdr T3 ");
            SQL.AppendLine("            On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
            SQL.AppendLine("            And T3.CancelInd='N'");
            if (mIsRptAgingAPPaidItemUsePaymentDate)
            {
                if (DtePaymentDt1.Text.Length > 0 && DtePaymentDt2.Text.Length > 0)
                {
                    SQL.AppendLine("             And T3.DocDt Between @PaymentDt1 And @PaymentDt2 ");
                }
                else
                {
                    SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
                }
            }
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T4.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T4.DocDt <= @BalanceDt ");
            }
            else
                SQL.AppendLine("            And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    ) Tbl Group By DocNo ");
            SQL.AppendLine(") Tbl8 On Tbl1.DocNo=Tbl8.DocNo ");
            SQL.AppendLine("Where 1=1 ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("And Tbl1.Status In ('O','P')");
            }

            //if (mIsRptAgingAPPaidItemUsePaymentDate)
            //{
            //    if (DtePaymentDt1.Text.Length > 0 && DtePaymentDt2.Text.Length > 0)
            //    {
            //        if (DteBalanceDt.Text.Length > 0)
            //        {
            //            SQL.AppendLine("AND (Case Tbl1.Type When '1' Then Tbl7.DocDt2 When '2' Then Tbl8.DocDt2 END <= @BalanceDt) ");
            //        }
            //        else
            //            SQL.AppendLine("AND (Case Tbl1.Type When '1' Then Tbl7.DocDt2 When '2' Then Tbl8.DocDt2 END Between @PaymentDt1 And @PaymentDt2) ");
            //    }
            //}



            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 38;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Period",
                        "Entity",
                        "Department",
                        "Site",
                        "Type",

                        //6-10
                        "Currency", 
                        "Vendor", 
                        "Invoice#",
                        "Type",
                        "",
                        
                        //11-15
                        "",
                        "Vendor's" + Environment.NewLine + "Invoice#",
                        "Payment",
                        "Status",
                        "Invoice Amount",
                        
                        //16-20
                        "Downpayment",
                        "Paid Amount",
                        "Balance",
                        "Due Date",
                        "Paid Date",
                        
                        //21-25
                        "Due To Paid"+Environment.NewLine+"(Days)",
                        "Aging"+Environment.NewLine+"(Days)",
                        "Aging"+Environment.NewLine+"Current AP",
                        "Over Due"+Environment.NewLine+"1-30 Days",
                        "Over Due"+Environment.NewLine+"31-90 Days",

                        //26-30
                        "Over Due"+Environment.NewLine+"91-180 Days",
                        "Over Due"+Environment.NewLine+"181-360 Days",
                        "Over Due"+Environment.NewLine+"361-720 Days",
                        "Over 720 Days",
                        "Term of"+Environment.NewLine+"Payment",
                        

                        //31-35
                        "Bank Account",
                        "PO#",
                        "Date",
                        "LPPB#",
                        "Voucher#",

                        //36-37
                        "",
                        "Profit Center"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 180, 200, 150, 150,  
                        
                        //6-10
                        60, 250, 130, 0, 20,  
                        
                        //11-15
                        20, 150, 120, 80, 120, 
                        
                        //16-20
                        120, 120, 120, 80, 150, 
                        
                        //21-25
                        100, 100, 100, 100, 100,  
                        
                        //26-30
                        100, 100, 100, 100, 150,

                        //31-35
                         300, 150, 100, 150, 0, 

                        //36-37
                        20, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 10, 11 , 36});
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 18, 21, 22, 23, 24, 25, 26, 27, 28, 29 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 19, 33 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 9, 10, 11, 20, 21, 30, 35 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            if (mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, true);
            if (!mIsRptAgingAPPaidItemShowPOInfo) Sm.GrdColInvisible(Grd1, new int[] { 32, 33, 34 }, false);
            if (!mIsRptShowVoucherLoop) Sm.GrdColInvisible(Grd1, new int[] { 36 }, false);
            if (!mIsFicoUseMultiProfitCenterFilter) Sm.GrdColInvisible(Grd1, new int[] { 37 }, false);
            Grd1.Cols[31].Move(22);
            Grd1.Cols[32].Move(9);
            Grd1.Cols[33].Move(10);
            Grd1.Cols[34].Move(11);
            Grd1.Cols[36].Move(21);
            Grd1.Cols[37].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 11, 21, 30 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (mIsFicoUseMultiProfitCenterFilter)
                if (IsProfitCenterInvalid()) return;
            Sm.ClearGrd(Grd1, false);
            if (ChkBalanceDt.Checked == false && ChkDocDt.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "Invoice Date or Balance As Of Date is empty"); return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                SetProfitCenter();
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (Tbl4.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0)
                        mQueryProfitCenter += "    And 1=0 ";
                    else
                        mQueryProfitCenter += "    And (" + Filter_ + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter += "    And Find_In_Set(Tbl4.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter += "    And Tbl4.ProfitCenterCode In ( ";
                        mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "    ) ";
                    }
                }

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@PaymentDt1", Sm.GetDte(DtePaymentDt1));
                Sm.CmParamDt(ref cm, "@PaymentDt2", Sm.GetDte(DtePaymentDt2));
                Sm.CmParamDt(ref cm, "@BalanceDt", Sm.GetDte(DteBalanceDt));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "Tbl1.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCtCode), "Tbl2.VdCtCode", true);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDueDt1), Sm.GetDte(DteDueDt2), "Tbl1.DueDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "Tbl1.Type", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "Tbl4.SiteCode", true);
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

                if (Sm.GetLue(LueStatus) == "OP")
                    Filter += " And Tbl1.Status In ('O', 'P') ";
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "Tbl1.Status", true);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL() + Filter + " Order By Tbl1.Period, Tbl1.Type, Tbl1.CurCode, Tbl2.VdName; ",
                        new string[]
                            { 
                                //0
                                "Period", 

                                //1-5
                                "EntName", "DeptName", "SiteName", "TypeDesc", "CurCode",  
                                
                                //6-10
                                "VdName", "DocNo", "Type", "VdInvNo", "PaymentTypeDesc", 
                                
                                //11-15
                                "StatusDesc", "InvoiceAmt", "Downpayment", "PaidAmt", "Balance", 
                                
                                //16-20
                                "DueDt", "PaidDt", "DueToPaidDays", "AgingDays", "AgingCurrent", 
                                
                                //21-25
                                "Aging1To30", "Aging31To90", "Aging91To180", "Aging181To360", "Aging361To720",
                                
                                //26-30
                                "AgingOver720", "PtName", "BankAccount", "PODocNo", "PIDocDt", 

                                //31
                                "LPPB", "VoucherDocNo", "ProfitCenterName"
                            },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 33, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 33);

                        }, true, false, false, false
                    );

                if (mIsAgingAPPaidItemShowTaxInfo) ProcessTax();

                Grd1.GroupObject.Add(1);
                if (mIsSiteMandatory) Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(5);
                Grd1.GroupObject.Add(6);
                Grd1.GroupObject.Add(7);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 15, 16, 17, 18, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmRptAgingAPPaidItem2Dlg(this, e.RowIndex);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }    
            
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 9) == "1")
                {
                    var f1 = new FrmPurchaseInvoice("XXX");
                    f1.Tag = "XXX";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f1.Text = "Purchase Invoice";
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice("XXX");
                    f2.Tag = "XXX";
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f2.Text = "Purchase Return Invoice";
                    f2.ShowDialog();
                }
            }

            if (mRptAgingAPPaidItemDlgFormat == "2")
            {
                if (e.ColIndex == 35 && Sm.GetGrdDec(Grd1, e.RowIndex, 17) > 0)
                {
                    var f = new FrmRptAgingAPPaidItem2Dlg3(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.PurchaseInvoiceDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.Text = "List of Invoice";
                    f.ShowDialog();
                }
            }
            else
            {
                if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 34).Length != 0)
                {
                    var f = new FrmRptAgingAPPaidItem2Dlg2(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 34);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmRptAgingAPPaidItem2Dlg(this, e.RowIndex);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }    

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 9) == "1")
                {
                    var f1 = new FrmPurchaseInvoice("XXX");
                    f1.Tag = "XXX";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f1.Text = "Purchase Invoice";
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice("XXX");
                    f2.Tag = "XXX";
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f2.Text = "Purchase Return Invoice";
                    f2.ShowDialog();
                }
            }
            
                if (mRptAgingAPPaidItemDlgFormat == "2")
                {
                    if (e.ColIndex == 35 && Sm.GetGrdDec(Grd1, e.RowIndex, 17) > 0)
                    {
                        var f = new FrmRptAgingAPPaidItem2Dlg3(this);
                        f.Tag = "***";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.PurchaseInvoiceDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                        f.Text = "List of Invoice";
                        f.ShowDialog();
                    }
                }
                else
                {
                    if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 34).Length != 0)
                    {
                    var f = new FrmRptAgingAPPaidItem2Dlg2(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 34);
                    f.ShowDialog();
                    }
                }
        }

        #endregion

        #region Misc Control Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 12].Value = "'" + Sm.GetGrdStr(Grd1, Row, 12);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            //Grd1.BeginUpdate();
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    Grd1.Cells[Row, 12].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 12), Sm.GetGrdStr(Grd1, Row, 12).Length - 1);
            //}
            //Grd1.EndUpdate();
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsAgingAPPaidItemShowTaxInfo = Sm.GetParameter("IsAgingAPPaidItemShowTaxInfo") == "Y";
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptAgingAPPaidItemShowPOInfo = Sm.GetParameterBoo("IsRptAgingAPPaidItemShowPOInfo");
            mIsRptAgingAPPaidItemUsePaymentDate = Sm.GetParameterBoo("IsRptAgingAPPaidItemUsePaymentDate");
            mIsRptShowVoucherLoop = Sm.GetParameterBoo("IsRptShowVoucherLoop");
            mIsOutgoingPaymentUsePIWithCOA = Sm.GetParameterBoo("IsOutgoingPaymentUsePIWithCOA");
            mIsPurchaseInvoiceUseCOATaxInd = Sm.GetParameterBoo("IsPurchaseInvoiceUseCOATaxInd");
            mRptAgingAPPaidItemDlgFormat = Sm.GetParameter("RptAgingAPPaidItemDlgFormat");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");

            if (mRptAgingAPPaidItemDlgFormat.Length == 0) mRptAgingAPPaidItemDlgFormat = "1";
        }

        private void SetLueType()
        {
            Sm.SetLue2(
                ref LueType,
                "Select '1' As Col1, 'Purchase Invoice' As Col2 " +
                "Union All Select '2' As Col1, 'Purchase Return Invoice' As Col2;",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetTax()
        {
            mlTax.Clear();
            var LatestColumn = Grd1.Cols.Count - 1; 
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select TaxCode, TaxName From TblTax Order By TaxName;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "TaxCode", "TaxName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LatestColumn += 1;
                        if (mFirstColumnForTax == -1) mFirstColumnForTax = LatestColumn;
                        mlTax.Add(new Tax()
                        {
                            TaxCode = Sm.DrStr(dr, c[0]),
                            TaxName = Sm.DrStr(dr, c[1]),
                            Column = LatestColumn
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.DrStr(dr, c[1]);
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 150;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTax()
        {
            if (mlTax.Count > 0)
            {
                for (int Col = mFirstColumnForTax; Col < Grd1.Cols.Count; Col++)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, Col].Value = 0m;
                }
                var l = new List<InvoiceTax>();
                ProcessTax1(ref l);
                if (l.Count>0) ProcessTax2(ref l);
            }
        }

        private void ProcessTax1(ref List<InvoiceTax> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                var DocNo = string.Empty;
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    DocNo = Sm.GetGrdStr(Grd1, Row, 8);
                    if (DocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), DocNo);
                        No += 1;
                    }
                }
            }

            if (Filter.Length > 0) Filter = " Where (" + Filter + ") ";

            SQL.AppendLine("Select A.DocNo, G.TaxCode, ");
            SQL.AppendLine("((B.Qty*F.UPrice)-((B.Qty*F.UPrice)*D.Discount/100)-D.DiscountAmt+D.RoundingValue)*G.TaxRate*0.01 As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo And B.CancelInd='N' And B.Status='A' ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo And C.TaxCode1 Is Not Null ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo And A.RecvVdDNo=B.DNo And D.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo And E.CancelInd='N' And E.Status='A' ");
            SQL.AppendLine("Inner Join TblQtDtl F On E.QtDocNo=F.DocNo And E.QtDNo=F.DNo  ");
            SQL.AppendLine("Inner Join TblTax G On C.TaxCode1=G.TaxCode " + Filter);
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo, G.TaxCode, ");
            SQL.AppendLine("((B.Qty*F.UPrice)-((B.Qty*F.UPrice)*D.Discount/100)-D.DiscountAmt+D.RoundingValue)*G.TaxRate*0.01 As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo And B.CancelInd='N' And B.Status='A' ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo And C.TaxCode2 Is Not Null ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo And A.RecvVdDNo=B.DNo And D.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo And E.CancelInd='N' And E.Status='A' ");
            SQL.AppendLine("Inner Join TblQtDtl F On E.QtDocNo=F.DocNo And E.QtDNo=F.DNo  ");
            SQL.AppendLine("Inner Join TblTax G On C.TaxCode2=G.TaxCode " + Filter);
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo, G.TaxCode, ");
            SQL.AppendLine("((B.Qty*F.UPrice)-((B.Qty*F.UPrice)*D.Discount/100)-D.DiscountAmt+D.RoundingValue)*G.TaxRate*0.01 As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo And B.CancelInd='N' And B.Status='A' ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo And C.TaxCode3 Is Not Null ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo And A.RecvVdDNo=B.DNo And D.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo And E.CancelInd='N' And E.Status='A' ");
            SQL.AppendLine("Inner Join TblQtDtl F On E.QtDocNo=F.DocNo And E.QtDNo=F.DNo  ");
            SQL.AppendLine("Inner Join TblTax G On C.TaxCode3=G.TaxCode " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "DocNo", "TaxCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new InvoiceTax()
                        {
                            InvoiceDocNo = Sm.DrStr(dr, c[0]),
                            TaxColumn = GetTaxColumn(Sm.DrStr(dr, c[1])),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private int GetTaxColumn(string TaxCode)
        {
            foreach (var x in mlTax.Where(x => string.Compare(x.TaxCode, TaxCode) == 0))
                return x.Column;
            return -1;
        }

        private void ProcessTax2(ref List<InvoiceTax> l)
        {
            for (var i = 0; i < l.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 9)=="1" && 
                        string.Compare(l[i].InvoiceDocNo, Sm.GetGrdStr(Grd1, Row, 8)) == 0)
                    {
                        if (l[i].TaxColumn>=0)
                            Grd1.Cells[Row, l[i].TaxColumn].Value = Sm.GetGrdDec(Grd1, Row, l[i].TaxColumn) + l[i].Amt;
                        break;
                    }
                }
            }
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("    Where ProfitCenterCode In ( ");
                SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
            if (DteDocDt1.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, false);
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDueDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDueDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDueDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void DtePaymentDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ChkBalanceDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSingleSetDateEdit(this, sender, "Balance As Of date");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Received date");
        }

        private void DteBalanceDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterSingleDteSetCheckEdit(this, sender);
            if (DteBalanceDt.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt1, DteDocDt2, ChkDocDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt1, DteDocDt2, ChkDocDt }, false);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (DteDocDt2.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, false);
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueVdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCtCode, new Sm.RefreshLue1(Sl.SetLueVdCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor's Category");
        }

        private void DtePaymentDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkPaymentDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Payment date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueType), "<Refresh>")) LueType.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion

        #region Class

        private class Tax
        {
            public string TaxCode { get; set; }
            public string TaxName { get; set; }
            public int Column { get; set; }
        }

        private class InvoiceTax
        {
            public string InvoiceDocNo { get; set; }
            public int TaxColumn { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion

    }
}
