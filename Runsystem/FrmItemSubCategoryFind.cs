﻿#region Update
    // 12/04/2017 [WED] revisi untuk excel, memunculkan kode item yang berawalan angka 0
    // 28/12/2020 [HAR] tambah informasi parent
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmItemSubCategoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmItemSubCategory mFrmParent;

        #endregion

        #region Constructor

        public FrmItemSubCategoryFind(FrmItemSubCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Item's Category", 
                        "Parent",   
                        
                        //6-10
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //11
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10 });
            Sm.GrdColCheck(Grd1, new int[] {3});
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, false);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItScName.Text, new string[] { "A.ItScCode", "A.ItScName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.ItScCode, A.ItScName, B.ItCtName, A.ActInd, C.ItScName As parentName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt " +
                        "From TblItemSubCategory A " +
                        "Left Join TblItemCategory B On A.ItCtCode=B.ItCtCode " +
                        "Left Join TblItemSubCategory C On A.Parent = C.ItScCode "+
                        Filter + " Order By A.ItScName;",
                        new string[]
                        {
                            //0
                            "ItScCode", 
                                
                            //1-5
                            "ItScName", "ActInd", "ItCtName", "parentName", "CreateBy",   
                            
                            //6-8
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItScName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItScName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's sub category");
        }
        #endregion

        #endregion
    }
}
