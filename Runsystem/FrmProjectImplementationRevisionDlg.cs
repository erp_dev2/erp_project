﻿#region Update
/*
    08/08/2019 [WED] alur nya ambil dari SO Contract Revision
    27/11/2019 [VIN/IMS] tambah kolom project name, project code, customer po#
    05/12/2019 [WED/YK] bug saat refresh
    23/12/2021 [RIS/YK] Grid9 (WBS 2) pada parent tidak readonly ketika choosedata
    20/04/2022 [BRI/PRODUCT] nilai PPN mengikuti PRJI
    25/04/2022 [DITA/PRODUCT] label PPh mengikuti PRJI
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationRevisionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmProjectImplementationRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementationRevisionDlg(FrmProjectImplementationRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CtCode, C.CtName, B.CurCode, B.Amt As SOCAmt, A.SOContractDocNo, ");
            SQL.AppendLine("IFNULL(F.ProjectName, E.ProjectName) ProjectName,  ");
            SQL.AppendLine("IFNULL(F.ProjectCode, B.ProjectCode2) ProjectCode, IFNULL(B.PONo, E2.DocNo) CustomerPO, A.PPNCode, A.PPhCode ");
            SQL.AppendLine("From tblprojectimplementationhdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join tblsocontracthdr B On A1.SOCDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join tblcustomer C On B.CtCode = C.CtCode ");
            SQL.AppendLine("Inner Join TblBOQHdr D On B.BOQDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNo ");
			SQL.AppendLine("left JOIN tblnoticetoproceed E2 ON E.DocNo=E2.LOPDocNo ");
            SQL.AppendLine("LEFT JOIN tblprojectgroup F ON E.PGCode=F.PGCode");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (E.SiteCode Is Null Or ( ");
                SQL.AppendLine("    E.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            SQL.AppendLine("where A.ProcessInd = 'F' ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("And A.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer Code",
                    "Customer",

                    //6-10
                    "Currency",
                    "Amount",
                    "SO Contract#",
                    "Project Name",
                    "Project Code",

                    //11-13
                    "Customer PO#",
                    "PPNCode",
                    "PPhCode"
                
                },
                new int[]
                {
                    40, 
                    130, 20, 80, 0, 180, 
                    100, 150, 0, 150, 150,
                    150, 0, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 4, 8, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
               
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt Desc, A.DocNo; ",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "CtCode", "CtName", "CurCode", "SOCAmt",
                        "SOContractDocNo", "ProjectName", "ProjectCode", "CustomerPo", "PPNCode", 
                        "PPhCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            string ContactPerson = string.Empty;
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtProjectImplementationDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.mPPNCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12);
                mFrmParent.mPPhCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 13);
                mFrmParent.ShowSOContract(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8));
                mFrmParent.BtnImportWBS.Enabled = mFrmParent.BtnImportResource.Enabled = true;
                mFrmParent.Grd5.ReadOnly = mFrmParent.Grd6.ReadOnly = mFrmParent.Grd7.ReadOnly = mFrmParent.Grd8.ReadOnly = mFrmParent.Grd9.ReadOnly = false;
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProjectImplementation(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion
    }
}
