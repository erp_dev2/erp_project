﻿#region Update
/*
    23/11/2021 [WED/RM] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRFQDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRFQ mFrmParent;
        private string mSQL = string.Empty, mVdSectorSubSector = string.Empty;

        #endregion

        #region Constructor

        public FrmRFQDlg3(FrmRFQ FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueVdCtCode(ref LueVdCtCode);
                SetGrd();
                ProcessSectorAndSub(ref mVdSectorSubSector);
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Vendor Code",
                    "",
                    "Vendor",
                    "Category"
                },
                new int[]
                {
                    40,
                    20, 100, 20, 200, 200
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.VdCode, A.VdName, B.VdCtName ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Inner Join TblVendorCategory B On A.VdCtCode = B.VdCtCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.VdCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct VdCode ");
            SQL.AppendLine("    From TblVendorSector ");
            SQL.AppendLine("    Where SubSectorCode Is Not Null ");
            SQL.AppendLine("    And Find_In_Set(Concat(SectorCode, '#', SubSectorCode), @VdSectorSubSector) ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdSectorSubSector", mVdSectorSubSector);

                string SelectedVdCode = mFrmParent.GetSelectedVdCode();
                if (SelectedVdCode.Length > 0)
                {
                    Filter += " And Not Find_In_Set(A.VdCode, @SelectedVdCode) ";
                    Sm.CmParam<String>(ref cm, "@SelectedVdCode", SelectedVdCode);
                }

                Sm.FilterStr(ref Filter, ref cm, TxtVdName.Text, new string[] { "A.VdCode", "A.VdName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCtCode), "A.VdCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.VdName;",
                    new string[]
                    {                         
                        //0
                        "VdCode",  
                            
                        //1-2
                        "VdName", "VdCtName"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 0, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 4);

                        mFrmParent.Grd2.Rows.Add();
                    }
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 vendor.");
            }
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 0), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            }
            return false;
        }

        #region Grid Method        

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                var f = new FrmVendor(string.Empty)
                {
                    Tag = "***",
                    Text = Sm.GetMenuDesc("FrmVendor"),
                    WindowState = FormWindowState.Normal,
                    StartPosition = FormStartPosition.CenterScreen,
                    mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2)
                };
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ProcessSectorAndSub(ref string mVdSectorSubSector)
        {
            if (mFrmParent.mVdSector.Count > 0)
            {
                foreach (var x in mFrmParent.mVdSector)
                {
                    if (mVdSectorSubSector.Length > 0) mVdSectorSubSector += ",";
                    mVdSectorSubSector += string.Concat(x.SectorCode, "#", x.SubSectorCode);
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtVdName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        private void LueVdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCtCode, new Sm.RefreshLue1(Sl.SetLueVdCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor Category");
        }

        #endregion

        #endregion
    }
}
