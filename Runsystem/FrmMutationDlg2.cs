﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMutationDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMutation mFrmParent;
        private string mSQL = string.Empty, mDocDt = string.Empty, mItCode = string.Empty;
        private int mCurRow = 0;

        #endregion

        #region Constructor

        public FrmMutationDlg2(FrmMutation FrmParent, string DocDt, string ItCode, int CurRow)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
            mItCode = ItCode;
            mCurRow = CurRow;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's"+Environment.NewLine+"Code", 
                        "",
                        "Item's Name",
                        "Category",
                        "Item Cost"+Environment.NewLine+"Document#",
                        
                        //6-10
                        "Item Cost"+Environment.NewLine+"D Number",
                        "Currency",
                        "Cost",
                        "UoM" + Environment.NewLine + "(1)",
                        "UoM" + Environment.NewLine + "(2)",
                        
                        //11
                        "UoM" + Environment.NewLine + "(3)"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 9, 10, 11 }, false);
            if (!mFrmParent.mIsShowMutationPriceInfo) Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.ItCode, A.ItName, B.ItCtName, C.DocNo, C.DNo, C.CurCode, C.Cost, ");
            SQL.AppendLine("A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3 ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ItCodeTo, T1.DocNo, T2.DNo, T1.CurCode, T2.Cost ");
            SQL.AppendLine("    From TblItemCostHdr T1 ");
            SQL.AppendLine("    Inner Join TblItemCostDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T3b.ItCodeTo, Max(Concat(T3a.DocDt, Left(T3a.DocNo, 4), T3b.DNo)) As Key1 ");
            SQL.AppendLine("        From TblItemCostHdr T3a ");
            SQL.AppendLine("        Inner Join TblItemCostDtl T3b On T3a.DocNo=T3b.DocNo And T3b.ItCodeFrom=@ItCodeFrom ");
            SQL.AppendLine("        Where T3a.DocDt<=@DocDt ");
            SQL.AppendLine("        Group By T3b.ItCodeTo ");
            SQL.AppendLine("    ) T3 On Concat(T1.DocDt, Left(T1.DocNo, 4), T2.DNo)=Key1 ");
            SQL.AppendLine(") C On A.ItCode=C.ItCodeTo ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cm, "@ItCodeFrom", mItCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode",

                            //1-5
                            "ItName",
                            "ItCtName", 
                            "DocNo",
                            "DNo",
                            "CurCode", 
                            
                            //6-9
                            "Cost", 
                            "InventoryUomCode", 
                            "InventoryUomCode2", 
                            "InventoryUomCode3"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
             if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row1 = mCurRow, Row2 = Grd1.CurRow.Index;
            
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 3);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 5);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 6);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 7);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 8);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 9);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 10);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 11);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
