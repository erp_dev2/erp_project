﻿namespace RunSystem
{
    partial class FrmProductionPenalty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductionPenalty));
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtUomCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueProductionWagesSource = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtProductionShiftCode = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LueWagesFormulationCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtWorkCenterDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.DteShopFloorControlDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnShopFloorControlDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnWorkCenterDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtShopFloorControlDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgBom = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgOthers = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWagesSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionShiftCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWagesFormulationCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteShopFloorControlDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteShopFloorControlDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShopFloorControlDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgBom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgOthers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtWorkCenterDocNo);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.DteShopFloorControlDocDt);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.BtnShopFloorControlDocNo);
            this.panel2.Controls.Add(this.BtnWorkCenterDocNo);
            this.panel2.Controls.Add(this.TxtShopFloorControlDocNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 136);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Controls.Add(this.Grd2);
            this.panel3.Location = new System.Drawing.Point(0, 136);
            this.panel3.Size = new System.Drawing.Size(772, 337);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.Grd2, 0);
            this.panel3.Controls.SetChildIndex(this.tabControl1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 126);
            this.Grd1.TabIndex = 36;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtUomCode);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.LueProductionWagesSource);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.TxtProductionShiftCode);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.LueWagesFormulationCode);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.LueCurCode);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(332, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(440, 136);
            this.panel6.TabIndex = 23;
            // 
            // TxtUomCode
            // 
            this.TxtUomCode.EnterMoveNextControl = true;
            this.TxtUomCode.Location = new System.Drawing.Point(125, 67);
            this.TxtUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUomCode.Name = "TxtUomCode";
            this.TxtUomCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUomCode.Properties.MaxLength = 40;
            this.TxtUomCode.Size = new System.Drawing.Size(305, 20);
            this.TxtUomCode.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(90, 70);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 14);
            this.label9.TabIndex = 30;
            this.label9.Text = "Uom";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProductionWagesSource
            // 
            this.LueProductionWagesSource.EnterMoveNextControl = true;
            this.LueProductionWagesSource.Location = new System.Drawing.Point(125, 25);
            this.LueProductionWagesSource.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWagesSource.Name = "LueProductionWagesSource";
            this.LueProductionWagesSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWagesSource.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWagesSource.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWagesSource.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWagesSource.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWagesSource.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWagesSource.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWagesSource.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWagesSource.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWagesSource.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWagesSource.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWagesSource.Properties.DropDownRows = 20;
            this.LueProductionWagesSource.Properties.NullText = "[Empty]";
            this.LueProductionWagesSource.Properties.PopupWidth = 222;
            this.LueProductionWagesSource.Size = new System.Drawing.Size(305, 20);
            this.LueProductionWagesSource.TabIndex = 27;
            this.LueProductionWagesSource.ToolTip = "F4 : Show/hide list";
            this.LueProductionWagesSource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionWagesSource.EditValueChanged += new System.EventHandler(this.LueProductionWagesSource_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(76, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 14);
            this.label3.TabIndex = 26;
            this.label3.Text = "Source";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProductionShiftCode
            // 
            this.TxtProductionShiftCode.EnterMoveNextControl = true;
            this.TxtProductionShiftCode.Location = new System.Drawing.Point(125, 4);
            this.TxtProductionShiftCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductionShiftCode.Name = "TxtProductionShiftCode";
            this.TxtProductionShiftCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProductionShiftCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductionShiftCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductionShiftCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProductionShiftCode.Properties.MaxLength = 40;
            this.TxtProductionShiftCode.Size = new System.Drawing.Size(305, 20);
            this.TxtProductionShiftCode.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(74, 112);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 34;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(89, 7);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 24;
            this.label12.Text = "Shift";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(125, 109);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(305, 20);
            this.MeeRemark.TabIndex = 35;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LueWagesFormulationCode
            // 
            this.LueWagesFormulationCode.EnterMoveNextControl = true;
            this.LueWagesFormulationCode.Location = new System.Drawing.Point(125, 46);
            this.LueWagesFormulationCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWagesFormulationCode.Name = "LueWagesFormulationCode";
            this.LueWagesFormulationCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.Appearance.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWagesFormulationCode.Properties.DropDownRows = 20;
            this.LueWagesFormulationCode.Properties.NullText = "[Empty]";
            this.LueWagesFormulationCode.Properties.PopupWidth = 222;
            this.LueWagesFormulationCode.Size = new System.Drawing.Size(305, 20);
            this.LueWagesFormulationCode.TabIndex = 29;
            this.LueWagesFormulationCode.ToolTip = "F4 : Show/hide list";
            this.LueWagesFormulationCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWagesFormulationCode.EditValueChanged += new System.EventHandler(this.LueWagesFormulationCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(7, 50);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 14);
            this.label4.TabIndex = 28;
            this.label4.Text = "Penalty Formulation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(66, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "Currency";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(125, 88);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 20;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 222;
            this.LueCurCode.Size = new System.Drawing.Size(305, 20);
            this.LueCurCode.TabIndex = 33;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // TxtWorkCenterDocNo
            // 
            this.TxtWorkCenterDocNo.EnterMoveNextControl = true;
            this.TxtWorkCenterDocNo.Location = new System.Drawing.Point(80, 67);
            this.TxtWorkCenterDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkCenterDocNo.Name = "TxtWorkCenterDocNo";
            this.TxtWorkCenterDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkCenterDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkCenterDocNo.Properties.MaxLength = 40;
            this.TxtWorkCenterDocNo.Size = new System.Drawing.Size(220, 20);
            this.TxtWorkCenterDocNo.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(1, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 15;
            this.label7.Text = "Work Center";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteShopFloorControlDocDt
            // 
            this.DteShopFloorControlDocDt.EditValue = null;
            this.DteShopFloorControlDocDt.EnterMoveNextControl = true;
            this.DteShopFloorControlDocDt.Location = new System.Drawing.Point(80, 109);
            this.DteShopFloorControlDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteShopFloorControlDocDt.Name = "DteShopFloorControlDocDt";
            this.DteShopFloorControlDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteShopFloorControlDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteShopFloorControlDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteShopFloorControlDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteShopFloorControlDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteShopFloorControlDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteShopFloorControlDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteShopFloorControlDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteShopFloorControlDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteShopFloorControlDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteShopFloorControlDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteShopFloorControlDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteShopFloorControlDocDt.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(22, 112);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "SFC Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnShopFloorControlDocNo
            // 
            this.BtnShopFloorControlDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnShopFloorControlDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnShopFloorControlDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnShopFloorControlDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnShopFloorControlDocNo.Appearance.Options.UseBackColor = true;
            this.BtnShopFloorControlDocNo.Appearance.Options.UseFont = true;
            this.BtnShopFloorControlDocNo.Appearance.Options.UseForeColor = true;
            this.BtnShopFloorControlDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnShopFloorControlDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnShopFloorControlDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnShopFloorControlDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnShopFloorControlDocNo.Image")));
            this.BtnShopFloorControlDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnShopFloorControlDocNo.Location = new System.Drawing.Point(303, 88);
            this.BtnShopFloorControlDocNo.Name = "BtnShopFloorControlDocNo";
            this.BtnShopFloorControlDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnShopFloorControlDocNo.TabIndex = 20;
            this.BtnShopFloorControlDocNo.ToolTip = "Show Shop Floor Control Information";
            this.BtnShopFloorControlDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnShopFloorControlDocNo.ToolTipTitle = "Run System";
            this.BtnShopFloorControlDocNo.Click += new System.EventHandler(this.BtnShopFloorControlDocNo_Click);
            // 
            // BtnWorkCenterDocNo
            // 
            this.BtnWorkCenterDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWorkCenterDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWorkCenterDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWorkCenterDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWorkCenterDocNo.Appearance.Options.UseBackColor = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseFont = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseForeColor = true;
            this.BtnWorkCenterDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnWorkCenterDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWorkCenterDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWorkCenterDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnWorkCenterDocNo.Image")));
            this.BtnWorkCenterDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWorkCenterDocNo.Location = new System.Drawing.Point(303, 66);
            this.BtnWorkCenterDocNo.Name = "BtnWorkCenterDocNo";
            this.BtnWorkCenterDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnWorkCenterDocNo.TabIndex = 17;
            this.BtnWorkCenterDocNo.ToolTip = "Find Work Center\'s Shop Floor Control#";
            this.BtnWorkCenterDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWorkCenterDocNo.ToolTipTitle = "Run System";
            this.BtnWorkCenterDocNo.Click += new System.EventHandler(this.BtnWorkCenterDocNo_Click);
            // 
            // TxtShopFloorControlDocNo
            // 
            this.TxtShopFloorControlDocNo.EnterMoveNextControl = true;
            this.TxtShopFloorControlDocNo.Location = new System.Drawing.Point(80, 88);
            this.TxtShopFloorControlDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShopFloorControlDocNo.Name = "TxtShopFloorControlDocNo";
            this.TxtShopFloorControlDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShopFloorControlDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShopFloorControlDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShopFloorControlDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtShopFloorControlDocNo.Properties.MaxLength = 30;
            this.TxtShopFloorControlDocNo.Size = new System.Drawing.Size(220, 20);
            this.TxtShopFloorControlDocNo.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(43, 91);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "SFC#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(80, 24);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(80, 46);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(46, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(80, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(186, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 126);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(772, 103);
            this.Grd2.TabIndex = 37;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgBom);
            this.tabControl1.Controls.Add(this.TpgOthers);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 229);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(772, 108);
            this.tabControl1.TabIndex = 38;
            // 
            // TpgBom
            // 
            this.TpgBom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgBom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgBom.Controls.Add(this.Grd3);
            this.TpgBom.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgBom.Location = new System.Drawing.Point(4, 26);
            this.TpgBom.Name = "TpgBom";
            this.TpgBom.Size = new System.Drawing.Size(764, 78);
            this.TpgBom.TabIndex = 0;
            this.TpgBom.Text = "Direct Labor";
            this.TpgBom.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 20;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(760, 74);
            this.Grd3.TabIndex = 39;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgOthers
            // 
            this.TpgOthers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgOthers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgOthers.Controls.Add(this.Grd4);
            this.TpgOthers.Location = new System.Drawing.Point(4, 26);
            this.TpgOthers.Name = "TpgOthers";
            this.TpgOthers.Size = new System.Drawing.Size(764, 78);
            this.TpgOthers.TabIndex = 1;
            this.TpgOthers.Text = "Coordinator";
            this.TpgOthers.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 20;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.ReadOnly = true;
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(760, 74);
            this.Grd4.TabIndex = 40;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            // 
            // FrmProductionPenalty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmProductionPenalty";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWagesSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionShiftCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWagesFormulationCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteShopFloorControlDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteShopFloorControlDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShopFloorControlDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgBom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgOthers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtProductionShiftCode;
        public DevExpress.XtraEditors.LookUpEdit LueProductionWagesSource;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        public DevExpress.XtraEditors.LookUpEdit LueWagesFormulationCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.DateEdit DteShopFloorControlDocDt;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnShopFloorControlDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnWorkCenterDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtShopFloorControlDocNo;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgBom;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgOthers;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        internal DevExpress.XtraEditors.TextEdit TxtWorkCenterDocNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtUomCode;
        private System.Windows.Forms.Label label9;
    }
}