﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPlantMaintenanceStatus : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySite = false, mIsShowForeignName = false;

        #endregion

        #region Constructor

        public FrmRptPlantMaintenanceStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueMth(LueMth);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "WOR#",
                        "Date",
                        "",
                        "Status",
                        "Level",
                        
                        //6-10
                        "Checked By",
                        "WO#",
                        "Date",
                        "Settled",
                        "RPL#",

                        //11-15
                        "DNo",
                        "Date",
                        "Requested"+Environment.NewLine+"Quantity",
                        "UoM",
                        "",

                        //16-20
                        "Status",
                        "Level",
                        "Checked By",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",

                        //21-25
                        "Local Code",
                        "Foreign Name",
                        "Site",
                        "Department",
                        "DO#",

                        //26-28
                        "DNo",
                        "Date",
                        "DO"+Environment.NewLine+"Quantity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 80, 20, 80, 60, 
                        
                        //6-10
                        200, 180, 80, 80, 180, 
                        
                        //11-15
                        60, 80, 100, 80, 20, 
                        
                        //16-20
                        80, 60, 200, 100, 180, 
                        
                        //21-25
                        100, 200, 150, 200, 180, 
                        
                        //26-28
                        60, 80, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 15 });
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 28 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 12, 27 });
            Sm.GrdColReadOnly(Grd1, new int[] { 
                0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28
            }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 11, 15, 17, 19, 21, 26 }, false);
            if (!mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 22 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 11, 15, 17, 19, 21, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As WORDocNo, A.DocDt As WORDocDt,  ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As WORStatus,  ");
            SQL.AppendLine("E.WORLevel, E.WORCheckedBy, ");
            SQL.AppendLine("F.DocNo As WODocNo, F.DocDt As WODocDt, F.SettleInd, ");
            SQL.AppendLine("G.RPLDocNo, G.RPLDNo, G.RPLDocDt, G.RPLQty, H.InventoryUomCode As Uom, G.RPLStatus, G.RPLLevel, G.RPLCheckedBy, ");
            SQL.AppendLine("G.ItCode, H.ItName, H.ItCodeInternal, H.ForeignName, D.SiteName, I.DeptName, ");
            SQL.AppendLine("J.DODocNo, J.DODNo, J.DODocDt, J.DOQty ");
            SQL.AppendLine("From TblWOR A ");
            SQL.AppendLine("Inner Join TblTOHdr B On A.TOCode = B.AssetCode ");
            SQL.AppendLine("Left Join TblLocation C On B.LocCode = C.LocCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblSite D On C.SiteCode=D.SiteCode  ");
                SQL.AppendLine("    And Exists(  ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite  ");
                SQL.AppendLine("        Where SiteCode=D.SiteCode  ");
                SQL.AppendLine("        And GrpCode In (  ");
                SQL.AppendLine("            Select GrpCode From TblUser  ");
                SQL.AppendLine("            Where UserCode=@UserCode  ");
                SQL.AppendLine("        )  ");
                SQL.AppendLine("    )  ");
            }
            else
                SQL.AppendLine("Left Join TblSite D On C.SiteCode = D.SiteCode ");
            
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, Count(T1.DocNo) As WORLevel,  ");
            SQL.AppendLine("    Group_Concat(T2.UserName, ' on ', DATE_FORMAT(Left(T1.LastUpDt, 8), '%d/%b/%Y'), ' (',  ");
            SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End,  ");
            SQL.AppendLine("    ')' Order By T1.ApprovalDNo)   ");
            SQL.AppendLine("    As WORCheckedBy  ");
            SQL.AppendLine("    From TblDocApproval T1  ");
            SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode = T2.UserCode ");
            SQL.AppendLine("    Where T1.DocType='WORequest'  ");
            //SQL.AppendLine("    And Exists(Select DocNo From TblWOR Where (Left(DocDt, 6) = @YrMth) And DocNo=T1.DocNo)  ");
            SQL.AppendLine("    And Exists(Select DocNo From TblWOR Where DocNo=T1.DocNo)  ");
            SQL.AppendLine("    Group By T1.DocNo, T1.DNo  ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo ");

            SQL.AppendLine("Left Join TblWOHdr F On A.DocNo = F.WORDocNo And F.CancelInd = 'N' "); //And Left(F.DocDt, 6) = @YrMth "); 
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T1.DocNo As RPLDocNo, T1.DocDt As RPLDocDt, T2.DNo As RPLDNo, T1.WODocNo, T2.Qty As RPLQty,  ");
	        SQL.AppendLine("    T1.DeptCode, T2.ItCode,  ");
	        SQL.AppendLine("    Case T2.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As RPLStatus,	 ");
	        SQL.AppendLine("    T3.RPLLevel, T3.RPLCheckedBy ");
	        SQL.AppendLine("    From TblDORequestDeptHdr T1 ");
	        SQL.AppendLine("    Inner Join TblDORequestDeptDtl T2 On T1.DocNo = T2.DocNo ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select X1.DocNo, X1.DNo, Count(X1.DocNo) As RPLLevel,  ");
		    SQL.AppendLine("        Group_Concat(X2.UserName, ' on ', DATE_FORMAT(Left(X1.LastUpDt, 8), '%d/%b/%Y'), ' (',  ");
	        SQL.AppendLine("            Case X1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End,  ");
	        SQL.AppendLine("        ')' Order By X1.ApprovalDNo)   ");
	        SQL.AppendLine("        As RPLCheckedBy  ");
		    SQL.AppendLine("        From TblDocApproval X1 ");
		    SQL.AppendLine("        Inner Join TblUser X2 On X1.UserCode = X2.UserCode ");
		    SQL.AppendLine("        Where X1.DocType = 'DORequestDeptWO' ");
		    SQL.AppendLine("        And Exists ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select A2.DocNo  ");
			SQL.AppendLine("            From TblDORequestDeptHdr A1 ");
			SQL.AppendLine("            Inner Join TblDORequestDeptDtl A2 On A1.DocNo = A2.DocNo ");
			SQL.AppendLine("            Where A2.DocNo=X1.DocNo And A2.DNo = X1.DNo "); // And Left(A1.DocDt, 6) = @YrMth ");
		    SQL.AppendLine("        ) ");
		    SQL.AppendLine("        Group By X1.DocNo, X1.DNo ");
	        SQL.AppendLine("    ) T3 On T2.DocNo = T3.DocNo And T2.DNo = T3.DNo ");
            SQL.AppendLine("    Where T1.WODocNo Is Not Null And T2.CancelInd = 'N' "); // And Left(T1.DocDt, 6) = @YrMth  ");
            SQL.AppendLine(") G On F.DocNo = G.WODocNo ");

            SQL.AppendLine("Left Join TblItem H On G.ItCode = H.ItCode ");
            SQL.AppendLine("Left Join TblDepartment I On G.DeptCode = I.DeptCode ");

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.DocNo As DODocNo, X2.DNo As DODNo, X1.DocDt As DODocDt, X1.DORequestDeptDocNo,  ");
	        SQL.AppendLine("    X3.DORequestDeptDNo As RPLDNo, X2.Qty As DOQty ");
	        SQL.AppendLine("    From TblDODeptHdr X1 ");
	        SQL.AppendLine("    Inner Join TblDODeptDtl X2 On X1.DocNo = X2.DocNo ");
	        SQL.AppendLine("    Inner Join TblDODeptDtl2 X3 On X1.DocNo = X3.DocNo And X2.DNo = X3.DNo ");
            SQL.AppendLine("    Where X1.DORequestDeptDocNo Is Not Null And X2.CancelInd = 'N' "); //And Left(X1.DocDt, 6) = @YrMth ");
            SQL.AppendLine(") J On G.RPLDocNo = J.DORequestDeptDocNo And G.RPLDNo = J.RPLDNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And Left(A.DocDt, 6) = @YrMth ");

            mSQL = SQL.ToString();
            return mSQL;
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Year")
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " And 0=0 ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)) );
                Sm.FilterStr(ref Filter, ref cm, TxtWORDocNo.Text, "A.DocNo", "WORDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWODocNo.Text, "F.DocNo", "WODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRPLDocNo.Text, "G.RPLDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDODocNo.Text, "J.DODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "H.ItCodeInternal", "H.ItName", "H.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "G.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By A.DocNo, J.DODocNo, J.DODNo ",
                    new string[]
                    {
                        //0
                        "WORDocNo",

                        //1-5
                        "WORDocDt", "WORStatus", "WORLevel","WORCheckedBy", "WODocNo",
                        
                        //6-10
                        "WODocDt", "SettleInd", "RPLDocNo", "RPLDNo", "RPLDocDt",  
                         
                        //11-15
                        "RPLQty", "Uom", "RPLStatus", "RPLLevel", "RPLCheckedBy",

                        //16-20
                        "ItCode", "ItName", "ItCodeInternal", "ForeignName", "SiteName",

                        //21-25
                        "DeptName", "DODocNo", "DODNo", "DODocDt", "DOQty"
                    
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdVal("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdVal("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 25);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 28 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void ShowWORApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("DATE_FORMAT(Left(LastUpDt, 8), '%d/%b/%Y')  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='WORequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 1));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private void ShowRPLApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("DATE_FORMAT(Left(LastUpDt, 8), '%d/%b/%Y') ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='DORequestDeptWO' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 11));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0) ShowWORApprovalInfo(e.RowIndex);
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length > 0) ShowRPLApprovalInfo(e.RowIndex);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0) ShowWORApprovalInfo(e.RowIndex);
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length > 0) ShowRPLApprovalInfo(e.RowIndex);
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 28 });
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtWORDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWORDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "WOR#");
        }

        private void TxtWODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "WO#");
        }

        private void TxtRPLDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRPLDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "RPL#");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        #endregion

        #endregion

    }
}
