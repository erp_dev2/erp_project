﻿#region Update
/*
    10/06/2020 [WED/SRN] new apps --> reporting srnpos
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockMovement4 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool
            mIsInventoryShowTotalQty = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockMovementShowProjectInfo = false
            ;

        #endregion

        #region Constructor

        public FrmRptStockMovement4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt ? "Y" : "N");
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            mIsRptStockMovementShowProjectInfo = Sm.GetParameterBoo("IsRptStockMovementShowProjectInfo");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Type",   
                    "Document#",
                    "Date",
                    "Warehouse",
                    "Lot",

                    //6-10
                    "Bin",
                    "Item's Code",
                    "Item's Name",
                    "Foreign Name",
                    "Property",
                    
                    //11-15
                    "Batch#",
                    "Source",
                    "Quantity", 
                    "UoM",
                    "Item Group"+Environment.NewLine+"Code",

                    //16-19
                    "Item Group"+Environment.NewLine+"Name",
                    "Sub-Category",
                    "Remark",
                    "Specification"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    250, 150, 80, 200, 60, 
                    
                    //6-10
                    60, 80, 250, 230, 80,   
                    
                    //11-15
                    200, 180, 120, 100, 100, 

                    //16-19
                    150, 150, 300, 200
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 12, 17 }, false);
            if (!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 9 });
            if (mIsItGrpCode) Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, false);
            Grd1.Cols[15].Move(10);
            Grd1.Cols[16].Move(11);
            Grd1.Cols[17].Move(12);
            if (mIsInvTrnShowItSpec)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, mIsInvTrnShowItSpec);
                Grd1.Cols[19].Move(9);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 12, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var lPOS = PrepareDataPOS();
                if (lPOS.Count > 0)
                {
                    if (lPOS.Count > 0)
                    {
                        var lSS = PrepareDataStockSummary(ref lPOS);
                        if (lSS.Count > 0) Process1(ref lPOS, ref lSS);
                        var lP = PrepareDataProperty();
                        if (lP.Count > 0) Process2(ref lPOS, ref lP);

                        lSS.Clear(); lP.Clear();
                    }

                    int Row = 0;
                    Grd1.BeginUpdate();
                    Grd1.Rows.Count = 0;

                    foreach(var i in lPOS)
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = i.DocType;
                        Grd1.Cells[Row, 2].Value = i.DocNo;
                        Grd1.Cells[Row, 3].Value = Sm.ConvertDate(i.DocDt);
                        Grd1.Cells[Row, 4].Value = i.WhsName;
                        Grd1.Cells[Row, 5].Value = i.Lot;
                        Grd1.Cells[Row, 6].Value = i.Bin;
                        Grd1.Cells[Row, 7].Value = i.ItCode;
                        Grd1.Cells[Row, 8].Value = i.ItName;
                        Grd1.Cells[Row, 9].Value = i.ForeignName;
                        Grd1.Cells[Row, 10].Value = i.PropName;
                        Grd1.Cells[Row, 11].Value = i.BatchNo;
                        Grd1.Cells[Row, 12].Value = i.Source;
                        Grd1.Cells[Row, 13].Value = i.Qty;
                        Grd1.Cells[Row, 14].Value = i.InventoryUomCode;
                        Grd1.Cells[Row, 15].Value = i.ItGrpCode;
                        Grd1.Cells[Row, 16].Value = i.ItGrpName;
                        Grd1.Cells[Row, 17].Value = i.ItScName;
                        Grd1.Cells[Row, 18].Value = i.Remark;
                        Grd1.Cells[Row, 19].Value = i.Specification;
                        
                        Row++;
                    }
                    if (ChkShowTotal.Checked)
                    {
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13 });
                    }
                    Grd1.EndUpdate();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                lPOS.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private List<POS> PrepareDataPOS()
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            if (ChkItCatCode.Checked) Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCatCode));
            if (ChkItName.Checked) Sm.CmParam<String>(ref cm, "@ItName", "%" + TxtItName.Text + "%");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.* FROM ( ");
            SQL.AppendLine("    SELECT 'POS' AS DocType, '1' As HasItem, CONCAT(A.Bsdate, '/', A.TrnNo) DocNo, A.BsDate DocDt, ");
            SQL.AppendLine("    A.WhsCode, B.WhsName, NULL AS Lot, NULL AS BIN, ");
            SQL.AppendLine("    A.ItCode, C.ItName, C.ForeignName, NULL AS PropName, NULL AS BatchNo, NULL AS SOURCE, A.Qty, C.InventoryUomCode, ");
            SQL.AppendLine("    C.ItGrpCode, D.ItGrpName, E.ItScName, NULL AS Remark, C.Specification ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select BsDate, TrnNo, WhsCode, ItCode, Qty ");
            SQL.AppendLine("        From SRNPOS.tblpostrn ");
            SQL.AppendLine("        Where BsDate Between @DocDt1 And @DocDt2 ");
            if (Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("    And WhsCode Is Not Null And WhsCode = @WhsCode ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    LEFT JOIN TblWarehouse B ON A.WhsCode = B.WhsCode ");
            SQL.AppendLine("    INNER JOIN TblItem C ON A.ItCode = C.ItCodeInternal ");
            SQL.AppendLine("        AND C.ItCodeInternal IS NOT NULL ");
            SQL.AppendLine("        AND C.ActInd = 'Y' ");
            if (Sm.GetLue(LueItCatCode).Length > 0)
                SQL.AppendLine("        And C.ItCtCode = @ItCtCode ");
            if (TxtItCode.Text.Length > 0)
                SQL.AppendLine("        And (C.ItCode = @ItCode Or C.ItCodeInternal = @ItCode) ");
            if (TxtItName.Text.Length > 0)
                SQL.AppendLine("        And C.ItName Like @ItName ");
            SQL.AppendLine("    LEFT JOIN TblItemGroup D ON C.ItGrpCode = D.ItGrpCode ");
            SQL.AppendLine("    LEFT JOIN TblItemSubCategory E ON C.ItScCode = E.ItScCode ");
            if (Sm.GetLue(LueItCatCode).Length == 0 && TxtItName.Text.Length == 0)
            {
                SQL.AppendLine("    UNION ALL ");
                SQL.AppendLine("    SELECT 'POS' AS DocType, '0' As HasItem, CONCAT(A.Bsdate, '/', A.TrnNo) DocNo, A.BsDate DocDt, ");
                SQL.AppendLine("    A.WhsCode, B.WhsName, NULL AS Lot, NULL AS BIN, ");
                SQL.AppendLine("    A.ItCode, NULL ItName, NULL ForeignName, NULL AS PropName, NULL AS BatchNo, NULL AS SOURCE, A.Qty, NULL InventoryUomCode, ");
                SQL.AppendLine("    NULL ItGrpCode, NULL ItGrpName, NULL ItScName, NULL AS Remark, NULL Specification ");
                SQL.AppendLine("    FROM ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select BsDate, TrnNo, WhsCode, ItCode, Qty ");
                SQL.AppendLine("        From SRNPOS.tblpostrn ");
                SQL.AppendLine("        Where BsDate Between @DocDt1 And @DocDt2 ");
                if (Sm.GetLue(LueWhsCode).Length > 0)
                    SQL.AppendLine("    And WhsCode Is Not Null And WhsCode = @WhsCode ");
                if (TxtItCode.Text.Length > 0)
                    SQL.AppendLine("    And ItCode = @ItCode ");
                SQL.AppendLine("    ) A ");
                SQL.AppendLine("    LEFT JOIN TblWarehouse B ON A.WhsCode = B.WhsCode ");
                SQL.AppendLine("    WHERE A.ItCode NOT IN (SELECT ItCode FROM TblItem) ");
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("ORDER BY T.DocNo; ");

            var l = new List<POS>();

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "DocType", 

                    //1-5
                    "HasItem", "DocNo", "DocDt", "WhsCode", "WhsName", 

                    //6-10
                    "Lot", "Bin", "ItCode", "ItName", "ForeignName", 

                    //11-15
                    "PropName", "BatchNo", "Source", "Qty", "InventoryUomCode", 

                    //16-20
                    "ItGrpCode", "ItGrpName", "ItScName", "Remark", "Specification"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                    l.Add(new POS()
                    {
                        DocType = Sm.DrStr(dr, 0),
                        HasItem = Sm.DrStr(dr, 1) == "1" ? true : false,
                        DocNo = Sm.DrStr(dr, 2),
                        DocDt = Sm.DrStr(dr, 3),
                        WhsCode = Sm.DrStr(dr, 4),
                        WhsName = Sm.DrStr(dr, 5),
                        Lot = Sm.DrStr(dr, 6),
                        Bin = Sm.DrStr(dr, 7),
                        ItCode = Sm.DrStr(dr, 8),
                        ItName = Sm.DrStr(dr, 9),
                        ForeignName = Sm.DrStr(dr, 10),
                        PropName = Sm.DrStr(dr, 11),
                        BatchNo = Sm.DrStr(dr, 12),
                        Source = Sm.DrStr(dr, 13),
                        Qty = Sm.DrDec(dr, 14),
                        InventoryUomCode = Sm.DrStr(dr, 15),
                        ItGrpCode = Sm.DrStr(dr, 16),
                        ItGrpName = Sm.DrStr(dr, 17),
                        ItScName = Sm.DrStr(dr, 18),
                        Remark = Sm.DrStr(dr, 19),
                        Specification = Sm.DrStr(dr, 20)
                    });
               }, false
            );

            return l;
        }

        private List<StockSummary> PrepareDataStockSummary(ref List<POS> lPOS)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<StockSummary>();

            string
                WhsCode = string.Empty,
                ItCode = string.Empty;

            foreach (var x in lPOS.Where(p =>
                p.WhsCode.Length > 0 &&
                p.HasItem == true))
            {
                if (WhsCode.Length > 0) WhsCode += ",";
                WhsCode += x.WhsCode;

                if (ItCode.Length > 0) ItCode += ",";
                ItCode += x.ItCode;
            }

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            SQL.AppendLine("Select WhsCode, ItCode, Source, BatchNo, Lot, Bin, PropCode ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where Find_In_Set(WhsCode, @WhsCode) ");
            SQL.AppendLine("And Find_In_Set(ItCode, @ItCode) ");
            if (TxtLot.Text.Length > 0)
                SQL.AppendLine("And Lot Like @Lot ");
            if (TxtBatchNo.Text.Length > 0)
                SQL.AppendLine("And BatchNo Like @BatchNo ");
            if (TxtBin.Text.Length > 0)
                SQL.AppendLine("And Bin Like @Bin ");
            SQL.AppendLine("Order By WhsCode, ItCode, Qty Desc; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "WhsCode", 
                    "ItCode", "Source", "BatchNo", "Lot", "Bin", 
                    "PropCode"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new StockSummary()
                        {
                            WhsCode = Sm.DrStr(dr, 0),
                            ItCode = Sm.DrStr(dr, 1),
                            Source = Sm.DrStr(dr, 2),
                            BatchNo = Sm.DrStr(dr, 3),
                            Lot = Sm.DrStr(dr, 4),
                            Bin = Sm.DrStr(dr, 5),
                            PropCode = Sm.DrStr(dr, 6),
                            IsPicked = false
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Property> PrepareDataProperty()
        {
            var cm = new MySqlCommand();
            var l = new List<Property>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select PropCode, PropName From TblProperty Union All Select '-' As PropCode, '-' As PropName;",
                new string[] { "PropCode", "PropName" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Property()
                        {
                            PropCode = Sm.DrStr(dr, 0),
                            PropName = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private void Process1(ref List<POS> lPOS, ref List<StockSummary> lSS)
        {
            foreach(var x in lPOS.Where(p => p.HasItem == true))
            {
                foreach(var y in lSS.Where(a => 
                    a.WhsCode == x.WhsCode && 
                    a.ItCode == x.ItCode &&
                    a.IsPicked == false))
                {
                    x.BatchNo = y.BatchNo;
                    x.PropName = y.PropCode;
                    x.Source = y.Source;
                    x.Lot = y.Lot;
                    x.Bin = y.Bin;

                    y.IsPicked = true;
                }
            }
        }

        private void Process2(ref List<POS> lPOS, ref List<Property> lP)
        {
            foreach(var x in lPOS.Where(p => p.HasItem == true))
            {
                foreach(var y in lP.Where(a =>
                    a.PropCode == x.PropName))
                {
                    x.PropName = y.PropName;
                }
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsInventoryRptFilterByGrpWhs)
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item Name");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        #endregion

        #endregion

        #region Class

        private class POS
        {
            public string DocType { get; set; }
            public bool HasItem { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string PropName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Remark { get; set; }
            public string Specification { get; set; }
        }

        private class StockSummary
        {
            public string WhsCode { get; set; }
            public string ItCode { get; set; }
            public string Source { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string PropCode { get; set; }
            public bool IsPicked { get; set; }
        }

        private class Property
        {
            public string PropCode { get; set; }
            public string PropName { get; set; }
        }

        #endregion
    }
}
