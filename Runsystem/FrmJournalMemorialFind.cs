﻿#region Update
/*
    21/09/2021 [TKG/AMKA] New Apps
    18/11/2021 [DEV/AMKA] Membuat Field Combo Box Cost Center di Journal Memorial hanya memunculkan Cost Center berdasarkan Group User dapat terfilter berdasarkan Group User dengan parameter IsJournalMemorialCCFilteredByGroup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJournalMemorialFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmJournalMemorial mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsReportingFilterByEntity = false;
        internal bool mIsCOAFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmJournalMemorialFind(FrmJournalMemorial FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -3);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.CancelInd, A.JnDesc, ");
            SQL.AppendLine("B.AcNo, C.AcDesc, A.CurCode, B.DAmt, B.CAmt, A.MenuCode, A.MenuDesc, D.EntName, E.CCName, ");
            SQL.AppendLine("A.JournalDocNo, A.Remark, B.Remark As RemarkDtl, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblJournalmemorialHdr A ");
            SQL.AppendLine("Left Join TblJournalMemorialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Left Join TblEntity D On B.EntCode=D.EntCode ");       
            if (mFrmParent.mIsJournalMemorialCCFilteredByGroup)
            {
                SQL.AppendLine("Inner Join TblCostCenter E ON A.CCCode=E.CCCode ");
                SQL.AppendLine("AND EXISTS ( ");
                SQL.AppendLine("    SELECT 1 FROM TblGroupCostCenter ");
                SQL.AppendLine("    WHERE CCCode=E.CCCode ");
                SQL.AppendLine("    AND GrpCode IN (SELECT GrpCode FROM TblUser WHERE ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblCostCenter E On A.CCCode=E.CCCode ");
            }

            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Cancel",
                        "Description",
                        
                        //6-10
                        "Account#",
                        "Account Description",
                        "Currency",
                        "Debit",
                        "Credit",
                        
                        //11-15
                        "Menu Code",
                        "Menu Description",
                        "Entity",
                        "Cost Center",
                        "Journal#",

                        //16-20
                        "Remark",
                        "Remark Detail",
                        "Created By",
                        "Created Date", 
                        "Created Time", 

                        //21-23
                        "Last Updated By", 
                        "Updated Date", 
                        "Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        180, 80, 80, 80, 250, 
                        
                        //6-10
                        150, 250, 80, 150, 150, 

                        //11-15
                        100, 150, 180, 200, 130, 

                        //16-20
                        200, 200, 120, 120, 120, 
                        
                        //21-23
                        120, 120, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkZeroAmt.Checked)
                {
                    Filter =
                        " And A.DocNo In ( " +
                        "   Select T1.DocNo " +
                        "   From TblJournalMemorialHdr T1, TblJournalMemorialDtl T2 " +
                        "   Where T1.DocNo=T2.DocNo " +
                        "   And T1.DocDt Between @DocDt1 And @DocDt2 " +
                        "   Group By T1.DocNo " +
                        "   Having Sum(T2.DAmt)=0.00 And Sum(T2.CAmt)=0.00 " +
                        "   ) ";
                }

                if (ChkUnbalancedJournal.Checked)
                {
                    Filter =
                        " And A.DocNo In ( " +
                        "   Select T1.DocNo " +
                        "   From TblJournalMemorialHdr T1, TblJournalMemorialDtl T2 " +
                        "   Where T1.DocNo=T2.DocNo " +
                        "   And T1.DocDt Between @DocDt1 And @DocDt2 " +
                        "   Group By T1.DocNo " +
                        "   Having Round(Sum(T2.DAmt),2)<>Round(Sum(T2.CAmt),2) " +
                        "   ) ";
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtJnDesc.Text, "A.JnDesc", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",
                            
                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "JnDesc", "AcNo", 
                            
                            //6-10
                            "AcDesc", "CurCode", "DAmt", "CAmt", "MenuCode", 
                            
                            //11-15
                            "MenuDesc", "EntName", "CCName", "JournalDocNo", "Remark", 
                            
                            //16-20
                            "RemarkDtl", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 20);
                          }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtJnDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkJnDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        #endregion

        #endregion
    }
}
