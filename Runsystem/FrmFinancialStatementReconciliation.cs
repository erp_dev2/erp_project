﻿#region Update
/*
   12/07/2022 [DITA/PRODUCT] New Apps
   26/07/2022 [DITA/PRODUCT] perhitungan profit loss dan balance sheet berubah
   08/08/2022 [BRI/PRODUCT] tambah journal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFinancialStatementReconciliation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty;
        internal FrmFinancialStatementReconciliationFind FrmFind;

        #endregion

        #region Constructor

        public FrmFinancialStatementReconciliation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Financial Statement Reconciliation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                
                TcFinancialStatementReconciliation.SelectedTabPage = TpSummary;
                TcFinancialStatementReconciliation.SelectedTabPage = TpDetail;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {

            #region Grid 1 - Detail

            Grd3.Cols.Count = 11;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Account#",
                        "Account Description",
                        "Type",
                        "Local Account#",
                        "Local Account"+Environment.NewLine+"Description",
                        

                        //6-10
                        "Debit",
                        "Credit",
                        "Balance",
                        "Status",
                        "Parent"
                    },
                    new int[]
                    {
                        //0
                        100,
 
                        //1-5
                        100, 150, 80, 100, 150, 
                        
                        //6-10
                        120, 120, 120, 150, 0
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8,9, 10 });
            Sm.GrdColInvisible(Grd3, new int[] { 0,10 }, false);

            #endregion

            #region Grid 2 - Summary

            Grd2.Cols.Count = 4;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "COA Parent#", 
                        
                        //1-3
                        "Account Description",
                        "Type",
                        "Total Amount",
                       
                    },
                    new int[]
                    {
                        //0
                        100,
 
                        //1-3
                        150, 100, 120
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 3 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3 });

            #endregion

          

        }
        #endregion

        override protected void HideInfoInGrd()
        {
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, MeeCancelReason, TxtIFSDocNo, TxtEntName, DteClosingDt, 
                        TxtJournalDocNo, TxtBalanceSheetAmt, TxtProfitLossAmt, TxtSumBalance
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnIFSDocNo.Enabled = false;
                    BtnProcess.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt
                    }, false);
                    BtnIFSDocNo.Enabled = true;
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    MeeCancelReason.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                 TxtDocNo, DteDocDt, MeeCancelReason, TxtIFSDocNo, TxtEntName, DteClosingDt,
                 TxtJournalDocNo, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBalanceSheetAmt, TxtProfitLossAmt, TxtSumBalance }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd3, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 6, 7, 8 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmFinancialStatementReconciliationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "FSReconciliation", "TblFinancialStatementReconciliationHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveFinancialStatementReconciliationHdr(DocNo));
            cml.Add(SaveFinancialStatementReconciliationDtl(DocNo));
            cml.Add(SaveFinancialStatementReconciliationDtl2(DocNo));
            cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtIFSDocNo, "Importing FS#", false) ||
                IsIFSAlreadyProceed() ||
                IsSummaryBalanceInvalid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
               
        }

        private bool IsIFSAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblFinancialStatementReconciliationHdr ");
            SQL.AppendLine("Where IFSDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtIFSDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This Importing FS# data already proceed in FS Reconciliation#" + Sm.GetValue(SQL.ToString(), TxtIFSDocNo.Text));
                return true;
            }

            return false;
        }
        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count ; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Tab List of Detail is empty.")) return true;
                if (Sm.GetGrdStr(Grd3, Row, 9).Contains("Not Found"))
                {
                    Sm.StdMsg(mMsgType.Warning, "There is Account# Not Found in Tab List of Detail.");
                    TcFinancialStatementReconciliation.SelectedTabPage = TpDetail;
                    Sm.FocusGrd(Grd3, Row, 9);
                    return true;
                }
            }
            for (int Row = 0; Row < Grd2.Rows.Count ; Row++)
                if (Sm.IsGrdValueEmpty(Grd2, Row, 0, false, "Tab Summary is empty.")) return true;

            return false;
        }

        private bool IsSummaryBalanceInvalid()
        {
            decimal SumBalanceAmt = decimal.Parse(TxtSumBalance.Text);

            if (SumBalanceAmt != 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Summary Balance should be zero. ");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveFinancialStatementReconciliationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* FSR - Hdr */ ");

            SQL.AppendLine("Insert Into TblFinancialStatementReconciliationHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, IFSDocNo, JournalDocNo, BalanceSheetAmt, ProfitLossAmt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, @IFSDocNo, @JournalDocNo, @BalanceSheetAmt, @ProfitLossAmt, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@IFSDocNo", TxtIFSDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", TxtJournalDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@BalanceSheetAmt", Decimal.Parse(TxtBalanceSheetAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ProfitLossAmt", Decimal.Parse(TxtProfitLossAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveFinancialStatementReconciliationDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* FSR - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblFinancialStatementReconciliationDtl(DocNo, DNo, AcNo, LocalAcNo, Balance, Status, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                    }

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @LocalAcNo_" + r.ToString() +
                        ", @Balance_" + r.ToString() +
                        ", @Status_" + r.ToString() +
                        ", @UserCode, @Dt) ");
                  
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<String>(ref cm, "@LocalAcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@Balance_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 8));
                    Sm.CmParam<String>(ref cm, "@Status_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveFinancialStatementReconciliationDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* FSR - Dtl2 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 0).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblFinancialStatementReconciliationDtl2(DocNo, AcNo, Total, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                    }

                    SQL.AppendLine(
                        " (@DocNo"+
                        ", @AcNo_" + r.ToString() +
                        ", @Total_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 0));
                    Sm.CmParam<Decimal>(ref cm, "@Total_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 3));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString() ;

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var EntCode = Sm.GetValue("Select EntCode From TblFinancialStatementHdr Where DocNo=@Param",TxtIFSDocNo.Text);

            SQL.AppendLine("/* FinancialStatementReconciliation - Journal */ ");
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblFinancialStatementReconciliationHdr Set ");
            SQL.AppendLine("JournalDocNo = @JournalDocNo ");
            SQL.AppendLine("Where DocNo = @DocNo And JournalDocNo is Null And CancelInd = 'N'; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, ");
            SQL.AppendLine("Concat('Financial Statement Reconciliation : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("NULL, NULL, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo as DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From (");
            SQL.AppendLine("        Select B.AcNo AS AcNo, ");
            SQL.AppendLine("        A.DAmt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblFinancialStatementDtl A ");
            SQL.AppendLine("        Inner Join TblFinancialStatementReconciliationDtl B On A.AcNo=B.LocalAcNo And B.DocNo=@DocNo ");
            SQL.AppendLine("        Where A.DocNo = @IFSDocNo And A.DAmt > 0 ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        Select B.AcNo AS AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.CAmt As CAmt ");
            SQL.AppendLine("        From TblFinancialStatementDtl A ");
            SQL.AppendLine("        Inner Join TblFinancialStatementReconciliationDtl B On A.AcNo=B.LocalAcNo And B.DocNo=@DocNo ");
            SQL.AppendLine("        Where A.DocNo = @IFSDocNo And A.CAmt > 0 ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine(") T;  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@IFSDocNo", TxtIFSDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("/* FinancialStatementReconciliation - Cancel Journal */ ");
            SQL.AppendLine("Update TblFinancialStatementReconciliationHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblFinancialStatementReconciliationHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblFinancialStatementReconciliationHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNoFSR", TxtJournalDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelFinancialStatementReconciliationHdr());
            cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() 
                ;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this FSR#.");
                return true;
            }
            return false;
        }
 
        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblFinancialStatementReconciliationHdr ");
            SQL.AppendLine("Where CancelInd='Y' And CancelReason Is Not Null And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }
        private MySqlCommand CancelFinancialStatementReconciliationHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblFinancialStatementReconciliationHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd",  "Y" );
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowAccount(string IFSDocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (TxtDocNo.Text.Length == 0)
                    {
                        SQL.AppendLine("Select D.AcNo, D.AcDesc, Case D.AcType When 'D' Then 'Debit' Else 'Credit' End As AcType, ");
                        SQL.AppendLine("B.AcNo LocalAcNo, B.AcDesc LocalAcDesc, B.DAmt, B.CAmt, Case D.AcType When 'D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End As Balance ");
                        SQL.AppendLine("From TblFinancialStatementHdr A ");
                        SQL.AppendLine("Inner Join TblFinancialStatementDtl B On A.DocNo = B.DocNo And A.DocNo = @IFSDocNo ");
                        SQL.AppendLine("Left Join TblCOADtl2 C On B.AcNo = C.LocalAcNo And A.EntCode = C.EntCode ");
                        SQL.AppendLine("Left Join TblCOA D On C.AcNo = D.AcNo And D.ActInd = 'Y' ");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@IFSDocNo", IFSDocNo);
                    }
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                    {
                        "AcNo",
                        "AcDesc", "AcType", "LocalAcNo", "LocalAcDesc", "DAmt",
                        "CAmt", "Balance",
                    });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd3.Rows.Count = 0;
                        Grd3.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd3, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd3, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd3, dr, c, Row, 8, 7);
                            Grd3.Cells[Row, 9].Value = Sm.GetGrdStr(Grd3, Row, 1).Length > 0 ? "Ready":"Account# Not Found";
                            string[] x = Sm.GetGrdStr(Grd3, Row, 1).Split('.');
                            Grd3.Cells[Row, 10].Value = x[0];

                            Row += 1;
                        }
                    }
                   
                    Grd3.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public void ShowAccountParent()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (TxtDocNo.Text.Length == 0)
                    {
                        SQL.AppendLine("Select AcNo, AcDesc, Case AcType When 'D' Then 'Debit' Else 'Credit' End As AcType ");
                        SQL.AppendLine("From TblCOA  ");
                        SQL.AppendLine("Where ActInd = 'Y' And Level = '1' And Parent Is Null ");
                        SQL.AppendLine("Order By AcNo ");

                        cm.CommandText = SQL.ToString();
                    }
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                    {
                        "AcNo",
                        "AcDesc", "AcType"
                    });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd2.Rows.Count = 0;
                        Grd2.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd2.Rows.Add();
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                            Grd2.Cells[Row, 3].Value = 0m;

                            Row += 1;
                        }
                    }

                    Grd2.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowFinancialStatementReconciliationHdr(DocNo);
                ShowFinancialStatementReconciliationDtl(DocNo);
                ShowFinancialStatementReconciliationDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowFinancialStatementReconciliationHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("B.DocNo IFSDocNo, C.EntName, B.ClosingDt, A.JournalDocNo, A.BalanceSheetAmt, A.ProfitLossAmt ");
            SQL.AppendLine("From TblFinancialStatementReconciliationHdr A  ");
            SQL.AppendLine("Inner Join TblFinancialStatementHdr B On A.IFSDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEntity C On B.EntCode= C.EntCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "CancelReason", "IFSDocNo","EntName",

                        //6-9
                         "ClosingDt", "JournalDocNo", "BalanceSheetAmt", "ProfitLossAmt", 
                        
                       
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        TxtIFSDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEntName.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetDte(DteClosingDt, Sm.DrStr(dr, c[6]));
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtBalanceSheetAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtProfitLossAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtSumBalance.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]) - Sm.DrDec(dr, c[9]), 0);

                    }, true
                );
        }

        private void ShowFinancialStatementReconciliationDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("   Select B.Dno, B.AcNo, D.AcDesc, Case D.AcType When 'D' Then 'Debit' Else 'Credit' End As AcType, B.LocalAcNo, C.AcDesc LocalAcDesc, ");
            SQL.AppendLine("   C.DAmt, C.CAmt, B.Balance, B.Status ");
            SQL.AppendLine("   From TblFinancialStatementReconciliationHdr A    ");
            SQL.AppendLine("   Inner Join TblFinancialStatementReconciliationDtl B On A.DocNo=B.DocNo   ");
            SQL.AppendLine("   Inner Join TblFinancialStatementDtl C On A.IFSDocNo=C.DocNo And B.LocalAcNo = C.AcNo  ");
            SQL.AppendLine("   Inner Join TblCOA D On B.AcNo=D.AcNo  ");
            SQL.AppendLine("   Inner Join TblCOADtl2 E On B.LocalAcNo=E.LocalAcNo  ");
            SQL.AppendLine("   Where A.DocNo=@DocNo  ");
            SQL.AppendLine(") T Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AcNo", "AcDesc", "AcType",  "LocalAcNo", "LocalAcDesc", 
                    //6-9
                    "DAmt", "CAmt", "Balance", "Status",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowFinancialStatementReconciliationDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select  A.AcNo, B.AcDesc, Case B.AcType When 'D' Then 'Debit' Else 'Credit' End As AcType, A.Total ");
            SQL.AppendLine("From TblFinancialStatementReconciliationDtl2 A    ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo  ");
            SQL.AppendLine("Where A.DocNo=@DocNo;  ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[]
                { 
                    //0
                    "AcNo", 

                    //1-3
                    "AcDesc", "AcType", "Total"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3 });


              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);
        }
        #endregion

        #region Additional Method

        private void Process1(ref List<COASummary> lCOASummary, ref List<COASummary> lCOASummary2)
        {
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    lCOASummary.Add(new COASummary()
                    {
                        Parent = Sm.GetGrdStr(Grd3, Row, 10),
                        Balance = Sm.GetGrdDec(Grd3, Row, 8),
                    });
                }
            }

            if (lCOASummary.Count > 0)
            {
                lCOASummary2 = lCOASummary.GroupBy(g => new { g.Parent })
                    .Select(s => new COASummary()
                    {
                        Parent = s.Key.Parent,
                        Balance = s.Sum(t => t.Balance),
                        AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param", s.Key.Parent)
                    }).ToList();
            }
        }

        private void Process2(ref List<COASummary> lCOASummary2)
        {
            decimal BSAmt = 0m, PLAmt = 0m, BSDebitAmt = 0m, BSCreditAmt = 0m, PLDebitAmt = 0m, PLCreditAmt = 0m;
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                foreach (var x in lCOASummary2.OrderBy(y => y.Parent).Where(z => z.Parent == Sm.GetGrdStr(Grd2, Row, 0)))
                    Grd2.Cells[Row, 3].Value = x.Balance;
            }
            foreach (var x in lCOASummary2.OrderBy(y => y.Parent))
            {
                if (x.Parent == "1" || x.Parent == "2" || x.Parent == "3")
                {
                    if(x.AcType == "D")
                        BSDebitAmt += x.Balance;
                    else
                        BSCreditAmt += x.Balance;
                }
                    
                else
                {
                    if (x.AcType == "D")
                        PLDebitAmt += x.Balance;
                    else
                        PLCreditAmt += x.Balance;
                }
                    
            }

            BSAmt = BSDebitAmt - BSCreditAmt;
            PLAmt = PLCreditAmt - PLDebitAmt;
            TxtBalanceSheetAmt.EditValue = Sm.FormatNum(BSAmt, 0);
            TxtProfitLossAmt.EditValue = Sm.FormatNum(PLAmt, 0);
            TxtSumBalance.EditValue = Sm.FormatNum(BSAmt-PLAmt, 0);

        }

        #endregion


        #endregion

        #region Event

        #region Button Event

        private void BtnIFSDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new FrmFinancialStatementReconciliationDlg(this);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            var lCOASummary = new List<COASummary>();
            var lCOASummary2 = new List<COASummary>();

            try
            {
                ShowAccountParent();
                Process1(ref lCOASummary, ref lCOASummary2);
                Process2(ref lCOASummary2);
            }

            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                lCOASummary.Clear();
                lCOASummary2.Clear();

                Sm.FocusGrd(Grd2, 0, 0);
                Cursor.Current = Cursors.Default;
            }

        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if(BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }


        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            //if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

        #region Class

        private class COASummary
        {
            public string Parent { get; set; }
            public decimal Balance { get; set; }
            public string AcType { get; set; }
          
        }

      
        #endregion
    }
}
