﻿#region Update
/*
    07/01/2019 [MEY] Dokumen WOR yang muncul sesuai filter date 
    12/11/2019 [HAR/IOK] bug saat dialog, dialog WOR yang muncul difilter juga berdasrakn maintenanance status di main nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMTTF2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptMTTF2 mFrmParent;
        private string mSQL = string.Empty, mAssetCode = string.Empty, mMtStatus = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMTTF2Dlg(FrmRptMTTF2 FrmParent, string AssetCode, string MtStatus)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mAssetCode = AssetCode;
            mMtStatus = MtStatus;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "WOR Information";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "WOR#",
                    "Down Date",
                    "Down Time",
                    "Document Date",
                    "Status",

                    //6-10
                    "Maintenance Status",
                    "Symptom Problem",
                    "TO",
                    "Asset Name",
                    "Display Name"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    170, 100, 100, 100, 110,
                    
                    //6-10
                    200, 150, 120, 200, 200
                }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DownDt, IfNull(Concat(Left(A.DownTm, 2), ':', Right(A.DownTm, 2)), '') DownTm, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.MtcStatus, B.OptDesc As MtcStatusDesc, A.SymProblem, C.OptDesc As SymProblemDesc, A.TOCode, A1.AssetName, A1.DisplayName ");
            SQL.AppendLine("From TblWOR A ");
            SQL.AppendLine("Left Join TblAsset A1 On A.TOCode = A1.AssetCode ");
            SQL.AppendLine("Left Join TblOption B On A.MtcStatus = B.OptCode And B.OptCat = 'MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.SymProblem = C.OptCode And C.OptCat = 'SymptomProblem' ");
            SQL.AppendLine("Where A.TOCode = @TOCode And A.MtcStatus=@MtStatus ");
            SQL.AppendLine("And (A.DownDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@TOCode", mAssetCode);
                Sm.CmParam<String>(ref cm, "@MtStatus", mMtStatus);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(mFrmParent.DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(mFrmParent.DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DownDt, DownTm, DocNo;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DownDt", 
                             "DownTm", 
                             "DocDt",
                             "StatusDesc",
                             "MtcStatusDesc",

                             //6-9
                             "SymProblemDesc",
                             "TOCode",
                             "AssetName",
                             "DisplayName"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "WOR#");
        }

        #endregion

        #endregion

    }
}
