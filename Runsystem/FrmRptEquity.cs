﻿#region Update
/*
    18/12/2019 [VIN/VIR] new apps
     19/12/2019 [DITA/VIR] isi data reporting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEquity : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mThisYear = string.Empty,
            mOneYearAgo = string.Empty,
            mTwoYearsAgo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEquity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetTimeStampVariable();
                SetGrd();
                GetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT *FROM TblRptDescription where doctype = 'Equity' order by description1 ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Deskripsi",
                    "Modal Disetor " + mTwoYearsAgo, 
                    "Tambahan modal disetor-"+Environment.NewLine+"pengampunan pajak " + mTwoYearsAgo,
                    "Ditentukan penggunaannya-"+Environment.NewLine+"cadangan umum " + mTwoYearsAgo,
                    "Belum ditentukan"+Environment.NewLine+"penggunaannya " + mTwoYearsAgo,

                    //6-10
                    "Surplus revaluasi"+Environment.NewLine+"aset tetap " + mTwoYearsAgo,
                    "Laba (rugi) aktuaria"+Environment.NewLine+"imbalan pasca kerja " + mTwoYearsAgo, 
                    "Jumlah ekuitas " + mTwoYearsAgo, 
                    "Modal Disetor " + mOneYearAgo, 
                    "Tambahan modal disetor-"+Environment.NewLine+"pengampunan pajak " + mOneYearAgo,
                    
                    //11-15
                    "Ditentukan penggunaannya-"+Environment.NewLine+"cadangan umum " + mOneYearAgo,
                    "Belum ditentukan"+Environment.NewLine+"penggunaannya " + mOneYearAgo,
                    "Surplus revaluasi"+Environment.NewLine+"aset tetap " + mOneYearAgo,
                    "Laba (rugi) aktuaria"+Environment.NewLine+"imbalan pasca kerja " + mOneYearAgo, 
                    "Jumlah ekuitas " + mOneYearAgo, 
                    
                    //16-20
                    "Modal Disetor " + mThisYear, 
                    "Tambahan modal disetor-"+Environment.NewLine+"pengampunan pajak " + mThisYear,
                    "Ditentukan penggunaannya-"+Environment.NewLine+"cadangan umum " + mThisYear,
                    "Belum ditentukan"+Environment.NewLine+"penggunaannya " + mThisYear,
                    "Surplus revaluasi"+Environment.NewLine+"aset tetap " + mThisYear,

                    //21-22
                    "Laba (rugi) aktuaria"+Environment.NewLine+"imbalan pasca kerja " + mThisYear,
                    "Jumlah ekuitas " + mThisYear
                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 150, 150, 150, 150,  
                    
                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 150, 150,

                    //16-20
                    150, 150, 150, 150, 150,

                    //21-22
                    150, 150

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            try
            {
                var cm = new MySqlCommand();
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);
                SetTimeStampVariable();
                SetGrd();
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(),
                    new string[]
                    {
                        //0
                        "Description1", 
                       
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Grd.Cells[Row, 2].Value = 0m;
                        Grd.Cells[Row, 3].Value = 0m;
                        Grd.Cells[Row, 4].Value = 0m;
                        Grd.Cells[Row, 5].Value = 0m;
                        Grd.Cells[Row, 6].Value = 0m;
                        Grd.Cells[Row, 7].Value = 0m;
                        Grd.Cells[Row, 8].Value = 0m;
                        Grd.Cells[Row, 9].Value = 0m;
                        Grd.Cells[Row, 10].Value = 0m;
                        Grd.Cells[Row, 11].Value = 0m;
                        Grd.Cells[Row, 12].Value = 0m;
                        Grd.Cells[Row, 13].Value = 0m;
                        Grd.Cells[Row, 14].Value = 0m;
                        Grd.Cells[Row, 15].Value = 0m;
                        Grd.Cells[Row, 16].Value = 0m;
                        Grd.Cells[Row, 17].Value = 0m;
                        Grd.Cells[Row, 18].Value = 0m;
                        Grd.Cells[Row, 19].Value = 0m;
                        Grd.Cells[Row, 20].Value = 0m;
                        Grd.Cells[Row, 21].Value = 0m;
                        Grd.Cells[Row, 22].Value = 0m;
                    }, true, false, false, true
                );
                Grd1.Group();
                AdjustSubtotals();
           

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
            mTwoYearsAgo = (Int32.Parse(mThisYear) - 2).ToString();
        }

        #endregion

        #endregion
    }
}
