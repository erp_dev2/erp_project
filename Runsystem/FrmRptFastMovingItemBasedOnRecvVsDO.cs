﻿#region Update
/*
    14/10/2021 [DEV/ALL] Menambahkan Kolom Batch Number pada Reporting Fast Moving Item (based on Receiving vs DO)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptFastMovingItemBasedOnRecvVsDO : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool mIsShowForeignName = false;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptFastMovingItemBasedOnRecvVsDO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string SetSQL(bool IsFilterByDocDt)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select Distinct B.ItCode, D.ItName, B.BatchNo, C.WhsName, E.DeptName, B.Source, G.DocNo, A.DocDt As DateDo, G.DocDt As DateRecv,");
            SQL.AppendLine("dateDiff(DATE_FORMAT(A.DocDt, '%Y-%m-%d'), DATE_FORMAT(G.DocDt, '%Y-%m-%d')) As Selisih, D.ForeignName ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N'  ");
            //SQL.AppendLine(FilterByDocDt(IsFilterByDocDt, "A"));
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment E On A.DeptCode=E.DeptCode ");
            SQL.AppendLine("Inner Join TblRecvVddtl F On B.Source = F.Source ");
            SQL.AppendLine("Inner Join TblRecvVdHdr G On F.DocNo = G.DocNo ");
            SQL.AppendLine("Where A.DORequestDeptDocNo Is Null  ");
            SQL.AppendLine(FilterByDocDt(IsFilterByDocDt, "A"));
            //SQL.AppendLine("	Group By B.ItCode, B.Source  ");
            SQL.AppendLine(") Y1  ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5  
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Source",
                        "Batch#",
                        "Foreign Name", 
                        
                        //6-10
                        "Warehouse",
                        "Department",
                        "Received#",
                        "DO Date",
                        "Received"+Environment.NewLine+"Date",

                        //11
                        "Received"+Environment.NewLine+"vs DO(Day)"
                    },
                    new int[] 
                    {
                        //0
                        30,

                        //1-5
                        80, 250, 200, 150, 120,  
                        
                        //6-10
                        150, 150, 150, 100, 80,  

                        //11
                        100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 2);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5 }, false);
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName", "ForeignName" });
                
                if (ChkDocDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                }
                SetSQL(ChkDocDt.Checked);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(ChkDocDt.Checked) + Filter + " Order By Selisih;",
                        new string[]
                        {
                            //0
                            "ItCode",   
                            
                            //1-5
                            "ItName", "Source", "BatchNo", "ForeignName", "WhsName", 
                            
                            //6-10
                            "DeptName", "DocNo", "DateDO", "DateRecv", "Selisih"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private string FilterByDocDt(bool IsFilterByDocDt, string TableAlias)
        {
            return (IsFilterByDocDt) ? " And " + TableAlias + ".DocDt Between @DocDt1 And @DocDt2 " : "";
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion     

        #endregion

        #endregion

        #region Event 
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
