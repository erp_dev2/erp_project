﻿#region Update
/*
    15/01/2020 [TKG/IMS] New application
    05/02/2020 [TKG/IMS] tambah cancel, revision
    18/03/2020 [TKG/IMS] tambah document date.
    14/04/2020 [VIN/IMS] tambah attachment di detail
    20/04/2020 [TKG/IMS] quantity komponen bisa 0
    28/04/2020 [WED/IMS] bugu saat upload file
    04/06/2020 [TKG/IMS] bug saat save
    07/06/2020 [TKG/IMS] tambah customer PO#
    24/06/2020 [TKG/IMS] bug show data PO#
    22/07/2020 [TKG/IMS] tambah drawing approval document information
    19/08/2020 [TKG/IMS] menambah validasi apabila so contract item pernah diproses sebelumnya.
    21/10/2020 [VIN/IMS] Penyesuaian Printout
    15/11/2020 [TKG/IMS] tambah bom group, sales bisa memilih component+material.
    06/12/2020 [WED/IMS] index revisi menu bom ditampilkan abjad
    06/12/2020 [WED/IMS] BOM Group tertentu akan mengubah quantity di header nya menjadi 1, berdasarkan parameter BomGroupCodeForGeneralType
    07/12/2020 [WED/IMS] TxtRevision di hide (karna kemungkinan Drawing Approval gak dipakai). ada approval Bom per department
    14/12/2020 [WED/IMS] validasi IsOutstanding dihilangkan
    18/12/2020 [WED/IMS] kalau belum approved, belum nambah RevNo nya
    18/12/2020 [VIN/IMS] penyesuaian Printout
    08/01/2021 [TKG/IMS] nomor bom berdasarkan document date
    21/01/2021 [DITA/IMS] tambah seqno dan order data by seqno untuk tblbom2dtl + tblbom2revisiondtl
    22/03/2021 [WED/IMS] tambah seqno untuk save Bom2Dtl2
    12/04/2021 [VIN/IMS] Penyesuaian Printout
 *  14/06/2021 [ICA/IMS] Generate docno berdasarkan BOM Group nya based on param IsDocNoFormatBasedOnBOMGroup
 *  16/06/2021 [VIN/IMS] Penyesuaian qty -> soc service bisa tick service indicator 
 *  02/07/2021 [VIN/IMS] bisa copy bom for sales saat insert
 *  14/07/2021 [VIN/IMS] Bug: quantity berubah saat pilih bom's group
 *  16/07/2021 [VIN/IMS] feedback Penyesuaian RevNo BOM3 IMS
 *  26/08/2021 [VIN/IMS] feedback Penyesuaian RevNo Printout BOM3 IMS
 *  08/12/2021 [TYO/IMS] menambah gambar ttd pada printout
 *  
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmBom3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal decimal mRevNo = 0;
        private string
            mMenuCodeForBom2FinishedGood = string.Empty, 
            mMenuCodeForBom2ComponentMaterial = string.Empty,
            mMenuCodeForBom2Revision = string.Empty,
            mBomGroupCodeForGeneralType = string.Empty
            ;
        private bool mIsForSales = false,
            IsInsert = false,
            IsBomGroupCodeForGeneralType = false,
            mIsFilterByDeptHR = false, 
            mIsDocNoFormatBasedOnBOMGroup = false
            ;
        internal decimal mInitFGQty = 0m;

        internal bool mIsForRevision = false;
        internal FrmBom3Find FrmFind;
        internal List<Bom2Dtl2> ml;

        private string
         mPortForFTPClient = string.Empty,
         mHostAddrForFTPClient = string.Empty,
         mSharedFolderForFTPClient = string.Empty,
         mUsernameForFTPClient = string.Empty,
         mPasswordForFTPClient = string.Empty,
         mFileSizeMaxUploadFTPClient = string.Empty,
         mFormatFTPClient = string.Empty,
         mInitFileNameHdr = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmBom3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Bill of Material";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueOption(ref LueBomGroupCode, "BomGroup");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                ml = new List<Bom2Dtl2>();
                SetGrd();
                TxtRevision.Visible = false;
                TxtQty1.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mRevNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "",
                    "Component's Code",
                    "Component's Name",
                    "Local Code",
                    "Specification",
                    
                    //6-10
                    "Quantity",
                    "UoM",
                    "Remark",
                    "FileName",
                    "",
                    
                    //11
                    "",
                    "FileName2"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    20, 130, 200, 100, 200, 
                    
                    //6-10
                    80, 100, 200, 150, 20, 

                    //11-12
                    20, 150
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 1, 10, 11});
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 12 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 3, 4, 5, 7 });
            //if (!mIsForSales)
            //    Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11 });

            Grd2.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "No.",

                    //1-3
                    "Revision",
                    "Drawing Approval",
                    ""
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    150, 150, 20
                }
            );
            Sm.GrdColButton(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    BtnSOContractDocNo.Enabled = false;
                    BtnBrowseFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnCopy.Enabled = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeCancelReason, ChkCancelInd, LueBomGroupCode, MeeRemark, LueDeptCode }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 6, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueBomGroupCode, MeeRemark, LueDeptCode }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkServiceItemInd }, true);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 6, 8, 10 });
                    BtnSOContractDocNo.Enabled = true;
                    BtnBrowseFile.Enabled = true;
                    BtnDownload.Enabled = true;
                    BtnCopy.Enabled = true;
                    BtnSOContractDocNo.Focus();
                    break;
                case mState.Edit:
                    if (mIsForSales)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd, MeeRemark, ChkServiceItemInd }, false);
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 6, 8, 10 });
                        BtnSOContractDocNo.Enabled = true;
                        BtnBrowseFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        ChkServiceItemInd.Enabled = true;

                    }
                    else
                    {
                        ChkServiceItemInd.Enabled = true;
                        BtnCopy.Enabled = true;
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 6, 8, 10 });
                    }
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mInitFGQty = 0m;
            mInitFileNameHdr = string.Empty;
            IsInsert = false;
            ml.Clear();
            ChkCancelInd.Checked = false;
            ChkServiceItemInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtSOContractDocNo, TxtProjectCode, TxtProjectName, 
                TxtItCode, TxtItName, TxtItCodeInternal, MeeSpecification, TxtUomCode, 
                MeeRemark, MeeCancelReason, TxtRevNo, TxtRevision, TxtPONo,
                TxtDrawingApprovalDocNo, LueBomGroupCode, LueDeptCode, TxtStatus, TxtQty1
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtQty }, 0);
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);

            Sm.ClearGrd(Grd2, false);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBom3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                if (mIsForRevision)
                {
                    Sm.StdMsg(mMsgType.Warning, "You can't insert revision data.");
                    return;
                }
                if (!mIsForSales)
                {
                    Sm.StdMsg(mMsgType.Warning, "You can't insert new data.");
                    return;
                }
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormShowDialog(new FrmBom3Dlg4(this));
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (mIsForRevision)
            {
                Sm.StdMsg(mMsgType.Warning, "You can't edit revision data.");
                return;
            }
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsForSales) InsertData();
                }
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;

                string[] TableName = { "BOmHdrIMS", "BOMDtlIMS", "BOMSignarureIMS" };
                List<IList> myLists = new List<IList>();

                var l3 = new List<BOMHdrIMS>();
                var ldtl2 = new List<BOMDtlIMS>();
                var l = new List<BOMSignarureIMS>();

                #region BOM Header IMS

                var cm3 = new MySqlCommand();

                var SQL3 = new StringBuilder();

                SQL3.AppendLine("SELECT @CompanyLogo As CompanyLogo, A.DocNo, DATE_FORMAT(A.DocDt, '%d %M %Y') DocDt, B.ItName, A.RevNo, F.ProjectName, B.ItCodeInternal, B.Specification, ");
                SQL3.AppendLine("A.Qty, B.PurchaseUomCode, A.Remark, B.ItCode, G.UserName, I.PosName, "); 
                SQL3.AppendLine("case  when A.RevNo = 0 then  '0' ELSE CONCAT(J.RevisionList, ',', Char(64 + A.RevNo)) END RevisionList, ifnull(H.EmpName,G.UserName) EmpName ");
                SQL3.AppendLine("FROM TblBOM2Hdr A  ");
                SQL3.AppendLine("INNER JOIN TblItem B ON A.ItCode=B.ItCode  ");
                SQL3.AppendLine("INNER JOIN TblSOContractHdr C ON A.SOContractDocNo=C.DocNo  ");
                SQL3.AppendLine("INNER JOIN TblBOQHdr D ON C.BOQDocNo=D.DocNo  ");
                SQL3.AppendLine("INNER JOIN TblLOPHdr E ON D.LOPDocNo=E.DocNo  ");
                SQL3.AppendLine("INNER JOIN TblProjectGroup F ON E.PGCode=F.PGCode  ");
                SQL3.AppendLine("INNER JOIN TblUser G ON G.UserCode=A.CreateBy  ");
                SQL3.AppendLine("Left JOIN TblEmployee H ON H.UserCode=G.UserCode  ");
                SQL3.AppendLine("Left JOIN TblPosition I ON I.PosCode=H.PosCode  ");
                SQL3.AppendLine("Left Join( ");
                SQL3.AppendLine("SELECT Group_concat(Distinct ");
                SQL3.AppendLine("case ");
                SQL3.AppendLine("    when revno !='0' then  Char(64 + T.RevNo) ");
                SQL3.AppendLine("    else '0' ");
                SQL3.AppendLine("end Separator ', ') RevisionList, docno ");
                SQL3.AppendLine("FROM tblbom2revisionhdr T ");
                SQL3.AppendLine("group by T.DocNo ");
                SQL3.AppendLine(") J on A.DocNo=J.DocNo ");

                SQL3.AppendLine(" WHERE A.DocNo=@DocNo ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "RevNo",
                         "ProjectName",
                         "ItCode",
                         "Specification",
                         "Qty",

                         //6-10
                         "PurchaseUomCode",
                         "Remark",
                         "ItName",
                         "DocDt",
                         "CompanyLogo",

                         //11-13
                         "EmpName",
                         "PosName",
                         "RevisionList"
                        
                        });
                    if (dr3.HasRows)
                    {

                        while (dr3.Read())
                        {
                            l3.Add(new BOMHdrIMS()
                            {
                                DocNo = Sm.DrStr(dr3, c3[0]),

                                RevNo = Sm.DrStr(dr3, c3[1]),
                                ProjectName = Sm.DrStr(dr3, c3[2]),
                                ItCode = Sm.DrStr(dr3, c3[3]),
                                Spesification = Sm.DrStr(dr3, c3[4]),
                                Qty = Sm.DrDec(dr3, c3[5]),

                                PurchaseUomCode = Sm.DrStr(dr3, c3[6]),
                                Remark = Sm.DrStr(dr3, c3[7]),
                                FGName = Sm.DrStr(dr3, c3[8]),
                                DocDt = Sm.DrStr(dr3, c3[9]),
                                CompanyLogo = Sm.DrStr(dr3, c3[10]),

                                UserName = Sm.DrStr(dr3, c3[11]),
                                PosName = Sm.DrStr(dr3, c3[12]),
                                BOMGroup = LueBomGroupCode.Text,
                                RevisionList = Sm.DrStr(dr3, c3[13]),
                                Project = TxtProjectCode.Text,

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                            });
                        }
                    }
                    dr3.Close();
                }

                myLists.Add(l3);
                #endregion

                #region BOM Detail IMS

                #region old
                //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++ )
                //{
                //    ldtl2.Add(new BOMDtlIMS()
                //    {
                //        Number = Row + 1,
                //        ItCode = Sm.GetGrdStr(Grd1, Row, 2),
                //        Spesifiation = Sm.GetGrdStr(Grd1, Row, 5),
                //        Qty = Sm.GetGrdDec(Grd1, Row, 6),
                //        UOM = Sm.GetGrdStr(Grd1, Row, 7),
                //        Remark = Sm.GetGrdStr(Grd1, Row, 8),
                //        ItName = Sm.GetGrdStr(Grd1, Row, 3)

                //    });
                //}

                //myLists.Add(ldtl2);
                #endregion

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                decimal Numb = 0;
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select B.ItCode, B.ItCode2, C.ItName, C.ItCodeInternal, C.Specification, "); 
                    SQLDtl2.AppendLine("B.Qty, C.PurchaseUomCode, B.Remark, B.FileName ");
                    SQLDtl2.AppendLine("FROM tblbom2dtl A ");
                    SQLDtl2.AppendLine("INNER JOIN tblbom2dtl2 B ON A.DocNo=B.DocNo AND A.ItCode=B.ItCode ");
                    SQLDtl2.AppendLine("INNER JOIN tblitem C ON C.ItCode=B.ItCode2 ");
                    SQLDtl2.AppendLine("WHERE A.DocNo=@DocNo ORder By B.SeqNo;  ");

                    cmDtl2.CommandTimeout = 600;
                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItCode2", 
                    "ItName", 
                    "ItCodeInternal", 
                    "Specification", 
                    "Qty", 

                    //6-8
                    "PurchaseUomCode", 
                    "Remark", 
                    "FileName"
         
                });

                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            Numb = Numb + 1;
                            ldtl2.Add(new BOMDtlIMS()
                            {
                                Number = Numb,
                                ItCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                ItCode2 = Sm.DrStr(drDtl2, cDtl2[1]),
                                ItName = Sm.DrStr(drDtl2, cDtl2[2]),
                                ItCodeInternal = Sm.DrStr(drDtl2, cDtl2[3]),
                                Specification = Sm.DrStr(drDtl2, cDtl2[4]),
                                Qty = Sm.DrDec(drDtl2, cDtl2[5]),
                                UomCode = Sm.DrStr(drDtl2, cDtl2[6]),
                                Remark = Sm.DrStr(drDtl2, cDtl2[7]),
                                FileName = Sm.DrStr(drDtl2, cDtl2[8]),
                                RevNo = TxtRevNo.Text,

                            });
                        }
                    }

                    drDtl2.Close();
                }

                myLists.Add(ldtl2);

                #endregion

                #region BOM Signature IMS

                var cm = new MySqlCommand();

                var SQL = new StringBuilder();

                SQL.AppendLine("select ");
                SQL.AppendLine("T1.UserName UserName1, DATE_FORMAT(T1.LastUpDt, '%d %M %Y') LastUpDt1, T1.PosName PosName1, ifnull(T1.EmpName, T1.UserName) EmpName1, T1.EmpPict EmpPict1, ");
                SQL.AppendLine("T2.UserName UserName2, DATE_FORMAT(T2.LastUpDt, '%d %M %Y') LastUpDt2, T2.PosName PosName2, ifnull(T2.EmpName, T2.UserName) EmpName2, T2.EmpPict EmpPict2, ");
                SQL.AppendLine("T3.UserName Username3, T3.PosName Posname3, T3.EmpPict3   ");

                SQL.AppendLine("from  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	select Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') AS EmpPict, C.DocNo, D.UserName, left(A.LastUpDt, 8) LastUpDt, PosName, E.EmpName ");
                SQL.AppendLine("	from tbldocapproval A ");
                SQL.AppendLine("	inner join tbldocapprovalsetting B on A.DocType=B.DocType  ");
                SQL.AppendLine("		and A.ApprovalDNo=B.DNo ");
                SQL.AppendLine("		and B.`Level`='1' ");
                SQL.AppendLine("	inner join tblbom2hdr C on C.DocNo=A.DocNo ");
                SQL.AppendLine("	inner join tbluser D on A.LastUpBy=D.UserCode ");
                SQL.AppendLine("	left join tblemployee E on E.UserCode=D.UserCode ");
                SQL.AppendLine("	left join tblposition F on F.PosCode=E.PosCode ");
                SQL.AppendLine("	LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine(")T1  ");
                SQL.AppendLine("left join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	select Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') AS EmpPict, C.DocNo, D.UserName,left(A.LastUpDt, 8) LastUpDt, PosName, E.EmpName ");
                SQL.AppendLine("	from tbldocapproval A ");
                SQL.AppendLine("	inner join tbldocapprovalsetting B on A.DocType=B.DocType ");
                SQL.AppendLine("		and A.ApprovalDNo=B.DNo ");
                SQL.AppendLine("		and B.`Level`='2'  ");
                SQL.AppendLine("	inner join tblbom2hdr C on C.DocNo=A.DocNo ");
                SQL.AppendLine("	inner join tbluser D on A.LastUpBy=D.UserCode ");
                SQL.AppendLine("	left join tblemployee E on E.UserCode=D.UserCode ");
                SQL.AppendLine("	left join tblposition F on F.PosCode=E.PosCode ");
                SQL.AppendLine("	LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine(")T2 on  T1.DocNo=T2.DocNO ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	SELECT A.DocNo, Concat(IfNull(E.ParValue, ''), B.UserCode, '.JPG') AS EmpPict3, B.UserName, D.PosName ");
                SQL.AppendLine("	FROM tblbom2hdr A ");
                SQL.AppendLine("	INNER JOIN tbluser B ON B.UserCode = A.CreateBy ");
                SQL.AppendLine("	INNER JOIN tblemployee C ON C.UserCode = B.UserCode ");
                SQL.AppendLine("	INNER JOIN tblposition D ON D.PosCode = C.PosCode ");
                SQL.AppendLine("	left join TblParameter E On E.ParCode = 'ImgFileSignature'  ");
                SQL.AppendLine(")T3 ON T1.DocNo = T3.DocNo ");
                SQL.AppendLine(" ");
                SQL.AppendLine("where T1.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "UserName1",

                         //1-5
                         "LastUpDt1",
                         "PosName1",
                         "EmpPict1",
                         "UserName2",
                         "LastUpDt2",

                         //6-10
                         "PosName2",
                         "EmpPict2",
                         "UserName3",
                         "PosName3",
                         "EmpPict3",
                         


                        });
                    if (dr.HasRows)
                    {

                        while (dr.Read())
                        {
                            l.Add(new BOMSignarureIMS()
                            {

                                UserName1 = Sm.DrStr(dr, c[0]),
                                LastUpDt1 = Sm.DrStr(dr, c[1]),
                                PosName1 =  Sm.DrStr(dr, c[2]),
                                EmpPict1 =  Sm.DrStr(dr, c[3]),

                                UserName2 = Sm.DrStr(dr, c[4]),
                                LastUpDt2 = Sm.DrStr(dr, c[5]),
                                PosName2 =  Sm.DrStr(dr, c[6]),
                                EmpPict2 =  Sm.DrStr(dr, c[7]),

                                Username3 = Sm.DrStr(dr, c[8]),
                                PosName3 =  Sm.DrStr(dr, c[9]),
                                EmpPict3 =  Sm.DrStr(dr, c[10]),

                            });
                        }
                    }
                    dr.Close();
                }

                myLists.Add(l);
                #endregion

                Sm.PrintReport("BOMIMS", myLists, TableName, false);

            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !mIsForSales)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBom3Dlg(this));
                }

                if (e.ColIndex == 1)
                    Sm.FormShowDialog(new FrmBom3Dlg2(this, e.RowIndex, BtnSave.Enabled));

                if (Sm.IsGrdColSelected(new int[] { 0, 1, 6, 8 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
                }
            }

            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 9), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 8, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ml.Count > 0)
                {
                    var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    if (ItCode.Length>0)
                        ml.RemoveAll(x => Sm.CompareStr(x.ItCode, ItCode));
                }
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 9), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 9, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 10)
                {
                    try
                    {
                        OD.InitialDirectory = "c:";
                        OD.Filter = "Pdf files (*.pdf)|*.pdf|Office Files|*.xls;*.xlsx";
                        OD.FilterIndex = 2;
                        OD.ShowDialog();
                        Grd1.Cells[e.RowIndex, 9].Value = OD.FileName;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
            }

            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmBom3Dlg(this));
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmBom3Dlg2(this, e.RowIndex, BtnSave.Enabled));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 8 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var DocNo = string.Empty;
            if (mIsDocNoFormatBasedOnBOMGroup) 
                DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "BOM2", Sm.GetLue(LueBomGroupCode), "TblBOM2Hdr");
            else 
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOM2", "TblBOM2Hdr");

            var cml = new List<MySqlCommand>();

            if (TxtFileName.Text.Length > 0 && TxtFileName.Text != "openFileDialog1")
            {
                UploadFile(DocNo, 0, TxtFileName.Text, true);
            }

            cml.Add(SaveBom(DocNo));
            cml.Add(UpdateFileNameHdr(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo, 0m);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document's date") ||
                Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false) ||
                Sm.IsLueEmpty(LueBomGroupCode, "Bom's group") ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true)||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsSOContractItemAlreadyProcessed() ||
                IsUploadFileNotValid();
        }

        private bool IsSOContractItemAlreadyProcessed()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblBom2Hdr Where SOContractDocNo=@SOContractDocNo And BomGroupCode=@BomGroupCode And ItCode=@ItCode And CancelInd='N';"
            };
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BomGroupCode", Sm.GetLue(LueBomGroupCode));
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            return Sm.IsDataExist(cm, "This SO Contract's item already processed.");
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0 && Sm.GetGrdStr(Grd1, Row, 9) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd1, Row, 9), false)) return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveBom(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblBom2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, DeptCode, RevNo, SOContractDocNo, BomGroupCode, ItCode, ServiceItemInd, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, Null, 'N', 'O', @DeptCode, 0, @SOContractDocNo, @BomGroupCode, @ItCode, @ServiceItemInd, @Qty, @Remark, @UserCode, CurrentDateTime()); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    SQL.AppendLine("Insert Into TblBom2Dtl(DocNo, ItCode, RevNo, SeqNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine("Select DocNo, @ItCode_1_" + r + ", 0, @SeqNo_1_" + r + ", @Qty_1_" + r + ", @Remark_1_" + r + ", @FileName_1_" + r + ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                    SQL.AppendLine("From TblBom2Hdr ");
                    SQL.AppendLine("Where DocNo=@DocNo; ");

                    Sm.CmParam<String>(ref cm, "@ItCode_1_" + r, Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_1_" + r, Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@Remark_1_" + r, Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@FileName_1_" + r, Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@SeqNo_1_" + r, Sm.Right("0000000000" + (r + 1).ToString(), 10));
                }
            }

            int i = 0;
            foreach (var x in ml)
            {
                SQL.AppendLine("Insert Into TblBom2Dtl2(DocNo, ItCode, ItCode2, SeqNo, RevNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                SQL.AppendLine("Select DocNo, @ItCode_2_" + i + ", @ItCode2_2_" + i + ", @SeqNo_2_" + i + ", 0, @Qty_2_" + i + ", @Remark_2_" + i + ", @FileName_2_" + i + ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                SQL.AppendLine("From TblBom2Hdr ");
                SQL.AppendLine("Where DocNo=@DocNo; ");

                Sm.CmParam<String>(ref cm, "@ItCode_2_" + i, x.ItCode);
                Sm.CmParam<String>(ref cm, "@ItCode2_2_" + i, x.ItCode2);
                Sm.CmParam<String>(ref cm, "@SeqNo_2_" + i, Sm.Right(string.Concat("0000000000", (i + 1).ToString()), 10));
                Sm.CmParam<Decimal>(ref cm, "@Qty_2_" + i, x.Qty);
                Sm.CmParam<String>(ref cm, "@Remark_2_" + i, x.Remark);
                Sm.CmParam<String>(ref cm, "@FileName_2_" + i, x.FileName);

                ++i;
            }

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='BOM3' ");
            SQL.AppendLine("And DeptCode = @DeptCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblBom2Hdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BomGroupCode", Sm.GetLue(LueBomGroupCode));
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@ServiceItemInd", ChkServiceItemInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateFileNameHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBom2Hdr Set ");
            SQL.AppendLine("    FileName = @FileName ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@FileName", TxtFileName.Text);

            cm.CommandText = SQL.ToString();

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                (!mIsForSales && IsEditedDataNotValid()) ||
                IsDataAlreadyCancelled() //||
                //IsDataOutstanding()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 9) != Sm.GetGrdStr(Grd1, Row, 12))
                {
                    if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0 && Sm.GetGrdStr(Grd1, Row, 9) != "openFileDialog1")
                        UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd1, Row, 9), false);
                }
            }

            var cml = new List<MySqlCommand>();
            cml.Add(SaveBom());

            if (TxtFileName.Text.Length > 0 && TxtFileName.Text != "openFileDialog1" && TxtFileName.Text != mInitFileNameHdr)
            {
                UploadFile(TxtDocNo.Text, 0, TxtFileName.Text, true);
                cml.Add(UpdateFileNameHdr(TxtDocNo.Text));
            }

            Sm.ExecCommands(cml);

            decimal mRevNoShow = Decimal.Parse(Sm.GetValue("Select RevNo From TblBOM2Hdr Where DocNo = @Param Limit 1;", TxtDocNo.Text));

            ShowData(TxtDocNo.Text, mRevNoShow);

            //if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
            //    (!mIsForSales && IsEditedDataNotValid()) ||
            //    IsDataAlreadyCancelled()
            //    ) return;

            //Cursor.Current = Cursors.WaitCursor;

            //var cml = new List<MySqlCommand>();

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 9) != Sm.GetGrdStr(Grd1, Row, 12))
            //    {
            //        if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0 && Sm.GetGrdStr(Grd1, Row, 9) != "openFileDialog1")
            //        {
            //            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd1, Row, 9));
            //        }
            //    }
            //}

            //cml.Add(SaveBom());

            //cml.Add(DeleteBOMDtl());

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
            //    {
            //        cml.Add(SaveBom());
            //    }
            //}
            //Sm.ExecCommands(cml);
            
            //ShowData(TxtDocNo.Text, 0m);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Bill of material#", false) ||
                //Sm.IsTxtEmpty(TxtQty, "Quantity", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsDataOutstanding()
        {
            if (ChkCancelInd.Checked) return false;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblBOM2Hdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And Status = 'O'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document need to be approved.");
                return true;
            }

            return false;
        }
        
        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBom2Hdr Where (CancelInd='Y' Or Status = 'C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data is already cancelled."
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No material.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r< Grd1.Rows.Count - 1; r++)
                if (
                    Sm.IsGrdValueEmpty(Grd1, r, 3, false, "Material is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, r, 6, true,
                        "Component's Code : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                        "Component's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                        "Quantity should be greater than 0.")
                    ) return true;
            return false;
        }

        private MySqlCommand SaveBom()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @RevNo:=(Select RevNo From TblBom2Hdr Where DocNo=@DocNo Limit 1); ");

            SQL.AppendLine("Delete From TblBom2RevisionHdr Where DocNo=@DocNo And RevNo=@RevNo; ");
            SQL.AppendLine("Delete From TblBom2RevisionDtl Where DocNo=@DocNo And RevNo=@RevNo; ");
            SQL.AppendLine("Delete From TblBom2RevisionDtl2 Where DocNo=@DocNo And RevNo=@RevNo; ");
            SQL.AppendLine("Delete From TblBom2RevisionDtl3 Where DocNo=@DocNo; ");

            SQL.AppendLine("Delete From TblDocApproval Where DocNo = @DocNo; ");

            SQL.AppendLine("Update TblBOM2Hdr Set Status = 'O' Where Docno = @DocNo And RevNo = @RevNo; ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='BOM3' ");
            SQL.AppendLine("And DeptCode = @DeptCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblBom2Hdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And RevNo = @RevNo ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblBom2RevisionHdr ");
            SQL.AppendLine("(DocNo, CancelReason, CancelInd, Status, DeptCode, RevNo, Revision, SOContractDocNo, ItCode, ServiceItemInd, BomGroupCode, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, CancelReason, CancelInd, Status, DeptCode, RevNo, Revision, SOContractDocNo, ItCode, ServiceItemInd, BomGroupCode, Qty, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblBom2Hdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblBom2RevisionDtl ");
            SQL.AppendLine("(DocNo, ItCode, RevNo, SeqNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select DocNo, ItCode, RevNo, '9999999999', Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblBom2Dtl Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblBom2RevisionDtl2 ");
            SQL.AppendLine("(DocNo, ItCode, ItCode2, SeqNo, RevNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select DocNo, ItCode, ItCode2, SeqNo, RevNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblBom2Dtl2 ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblBom2RevisionDtl3 ");
            SQL.AppendLine("(DocNo, Revision, RevNo, DrawingApprovalDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select DocNo, Revision, RevNo, DrawingApprovalDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblBom2Dtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblBom2Hdr Set ");
            if (mIsForSales) SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, Remark=@Remark, FileName=@FileName, ");
            if (TxtDrawingApprovalDocNo.Text.Length > 0)
                SQL.AppendLine("    Revision=@Revision, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Set @RevNo2 :=(Select Case Status When 'A' Then RevNo+1 Else RevNo End As RevNo From TblBom2Hdr Where DocNo = @DocNo Limit 1); ");

            SQL.AppendLine("Update TblBom2Hdr Set ");
            SQL.AppendLine("RevNo = @RevNo2 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("; ");

            //if (!mIsForSales)
            //{
                if (TxtDrawingApprovalDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Insert Into TblBom2Dtl3(DocNo, Revision, RevNo, DrawingApprovalDocNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo, @Revision, @RevNo2, @DrawingApprovalDocNo, @UserCode, CurrentDateTime()); ");
                }

                SQL.AppendLine("Delete From TblBom2Dtl Where DocNo=@DocNo; ");
                SQL.AppendLine("Delete From TblBom2Dtl2 Where DocNo=@DocNo; ");
                
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        SQL.AppendLine("Insert Into TblBom2Dtl(DocNo, ItCode, RevNo, SeqNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                        SQL.AppendLine("Select DocNo, @ItCode_1_" + r + ", @RevNo2, @SeqNo_1_" + r + ", @Qty_1_" + r + ", @Remark_1_" + r + ", @FileName_1_" + r + ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                        SQL.AppendLine("From TblBom2Hdr ");
                        SQL.AppendLine("Where DocNo=@DocNo; ");

                        Sm.CmParam<String>(ref cm, "@ItCode_1_" + r, Sm.GetGrdStr(Grd1, r, 2));
                        Sm.CmParam<Decimal>(ref cm, "@Qty_1_" + r, Sm.GetGrdDec(Grd1, r, 6));
                        Sm.CmParam<String>(ref cm, "@Remark_1_" + r, Sm.GetGrdStr(Grd1, r, 8));
                        Sm.CmParam<String>(ref cm, "@FileName_1_" + r, Sm.GetGrdStr(Grd1, r, 9));
                        Sm.CmParam<String>(ref cm, "@SeqNo_1_" + r, Sm.Right("0000000000" + (r + 1).ToString(), 10));
                    }
                }

                int i = 0;
                foreach (var x in ml)
                {
                    SQL.AppendLine("Insert Into TblBom2Dtl2(DocNo, ItCode, ItCode2, SeqNo, RevNo, Qty, Remark, FileName, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine("Select DocNo, @ItCode_2_" + i + ", @ItCode2_2_" + i + ", @SeqNo_2_" + i + ",  @RevNo2, @Qty_2_" + i + ", @Remark_2_" + i + ", @FileName_2_" + i + ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                    SQL.AppendLine("From TblBom2Hdr ");
                    SQL.AppendLine("Where DocNo=@DocNo; ");

                    Sm.CmParam<String>(ref cm, "@ItCode_2_" + i, x.ItCode);
                    Sm.CmParam<String>(ref cm, "@ItCode2_2_" + i, x.ItCode2);
                    Sm.CmParam<String>(ref cm, "@SeqNo_2_" + i, Sm.Right("0000000000" + (i + 1).ToString(), 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_2_" + i, x.Qty);
                    Sm.CmParam<String>(ref cm, "@Remark_2_" + i, x.Remark);
                    Sm.CmParam<String>(ref cm, "@FileName_2_" + i, x.FileName);

                    ++i;
                }
            //}

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@DrawingApprovalDocNo", TxtDrawingApprovalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Revision", (TxtDrawingApprovalDocNo.Text.Length > 0)?GetRevision():string.Empty);
            Sm.CmParam<String>(ref cm, "@FileName", TxtFileName.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GetRevision()
        {
            var r = Sm.GetValue("Select Revision From TblBom2Hdr Where DocNo=@Param;", TxtDocNo.Text);
            if (r.Length == 0)
                return "A";
            else
            {
                var v = Sm.Left(r, 1);
                if (v == "Z") return Sm.RepeatString("A", r.Length);
                byte x = 0;
                foreach (var i in Encoding.ASCII.GetBytes(v))
                {
                    x = i;
                    x++;
                    return Sm.RepeatString(Encoding.ASCII.GetString(new byte[] { x }), r.Length);
                }
            }
            return "*";
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, decimal RevNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                decimal mRevNoShow = Sm.GetValueDec("Select RevNo From TblBOM2Hdr Where DocNo = @Param Limit 1;", DocNo);
                if (RevNo != mRevNoShow) RevNo = mRevNoShow;
                ShowBomHdr(DocNo, RevNo);
                ShowBomDtl(DocNo, RevNo);
                ShowBomDtl2(DocNo, RevNo);
                ShowBomDtl3(DocNo, RevNo);
                //TxtRevNo.EditValue = NumberToString(Decimal.ToInt32(RevNo), true);
                TxtRevNo.EditValue = GetColumnName(Decimal.ToInt32(RevNo));
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBomHdr(string DocNo, decimal RevNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<decimal>(ref cm, "@RevNo", RevNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.SOContractDocNo, F.ProjectCode, F.ProjectName, ");
            SQL.AppendLine("C.ItCode, C.ItName, C.ItCodeInternal, C.Specification, A.Qty, C.PurchaseUomCode, ");
            SQL.AppendLine("A.Remark, A.FileName, A.RevNo, A.CancelReason, A.CancelInd, B.PONo, A.Revision, ");
            SQL.AppendLine("A.ServiceItemInd, A.BomGroupCode, A.DeptCode, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc ");
            SQL.AppendLine("From TblBom2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblBOQHdr D On B.BOQDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNO ");
            SQL.AppendLine("Inner Join TblProjectGroup F On E.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            if (mIsForRevision)
            {
                SQL.AppendLine("And A.RevNo=@RevNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.SOContractDocNo, F.ProjectCode, F.ProjectName, ");
                SQL.AppendLine("C.ItCode, C.ItName, C.ItCodeInternal, C.Specification, A.Qty, C.PurchaseUomCode, ");
                SQL.AppendLine("A.Remark, A.FileName, A.RevNo, A.CancelReason, A.CancelInd, B.PONo, A.Revision, ");
                SQL.AppendLine("A.ServiceItemInd, A.BomGroupCode, A.DeptCode, ");
                SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc ");
                SQL.AppendLine("From TblBom2RevisionHdr A ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Inner Join TblBOQHdr D On B.BOQDocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNO ");
                SQL.AppendLine("Inner Join TblProjectGroup F On E.PGCode=F.PGCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And A.RevNo=@RevNo ");
            }
            SQL.AppendLine(") T Limit 1;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "SOContractDocNo", "ProjectCode", "ProjectName", "ItCode", "ItName", 

                        //6-10
                        "ItCodeInternal", "Specification", "Qty", "PurchaseUomCode", "Remark", 

                        //11-15
                        "FileName", "RevNo", "CancelReason", "CancelInd", "DocDt",

                        //16-20
                        "PONo", "Revision", "ServiceItemInd", "BomGroupCode", "DeptCode",

                        //21
                        "StatusDesc"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[3]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[6]);
                        MeeSpecification.EditValue = Sm.DrStr(dr, c[7]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        mInitFGQty = Sm.DrDec(dr, c[8]);
                        TxtQty1.EditValue = Sm.DrDec(dr, c[8]);
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[9]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                        TxtFileName.EditValue = Sm.DrStr(dr, c[11]);
                        mInitFileNameHdr = Sm.DrStr(dr, c[11]);
                        TxtRevNo.EditValue = Sm.DrDec(dr, c[12]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[13]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[14])=="Y";
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[15]));
                        TxtPONo.EditValue = Sm.DrStr(dr, c[16]);
                        TxtRevision.EditValue = Sm.DrStr(dr, c[17]);
                        ChkServiceItemInd.Checked = Sm.DrStr(dr, c[18]) == "Y";
                        Sm.SetLue(LueBomGroupCode, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[20]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[21]);
                    }, true
                );
        }

        private void ShowBomDtl(string DocNo, decimal RevNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select A.SeqNo, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, A.Qty, B.PurchaseUomCode, A.Remark, A.FileName ");
            SQL.AppendLine("From TblBOM2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            if (mIsForRevision)
            {
                SQL.AppendLine("And A.RevNo=@RevNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.SeqNo, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, A.Qty, B.PurchaseUomCode, A.Remark, A.FileName ");
                SQL.AppendLine("From TblBOM2RevisionDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And A.RevNo=@RevNo ");
            }
            SQL.AppendLine(") T Order By T.SeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<decimal>(ref cm, "@RevNo", RevNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", "ItCodeInternal", "Specification", "Qty", "PurchaseUomCode", 
                    
                    //6-7
                    "Remark", "FileName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowBomDtl2(string DocNo, decimal RevNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItCode2, A.SeqNo, B.ItName, B.ItCodeInternal, B.Specification, A.Qty, B.PurchaseUomCode, A.Remark, A.FileName ");
            SQL.AppendLine("From TblBom2Dtl2 A, TblItem B ");
            SQL.AppendLine("Where A.ItCode2=B.ItCode ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            if (mIsForRevision)
            {
                SQL.AppendLine("And A.RevNo=@RevNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.ItCode, A.ItCode2, A.SeqNo, B.ItName, B.ItCodeInternal, B.Specification, A.Qty, B.PurchaseUomCode, A.Remark, A.FileName ");
                SQL.AppendLine("From TblBom2RevisionDtl2 A, TblItem B ");
                SQL.AppendLine("Where A.ItCode2=B.ItCode ");
                SQL.AppendLine("And A.DocNo=@DocNo ");
                SQL.AppendLine("And A.RevNo=@RevNo ");
            }

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<decimal>(ref cm, "@RevNo", RevNo);
            cm.CommandText=SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "ItCode", 
                    "ItCode2", "ItName", "ItCodeInternal", "Specification", "Qty", 
                    "PurchaseUomCode", "Remark", "FileName", "SeqNo"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ml.Add(new Bom2Dtl2()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItCode2 = Sm.DrStr(dr, c[1]),
                            ItName = Sm.DrStr(dr, c[2]),
                            ItCodeInternal = Sm.DrStr(dr, c[3]),
                            Specification = Sm.DrStr(dr, c[4]),
                            Qty = Sm.DrDec(dr, c[5]),
                            UomCode = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            FileName = Sm.DrStr(dr, c[8]),
                            SeqNo = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowBomDtl3(string DocNo, decimal RevNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Revision, DrawingApprovalDocNo, RevNo From ( ");
            SQL.AppendLine("    Select Revision, DrawingApprovalDocNo, RevNo ");
            SQL.AppendLine("    From TblBOM2Dtl3 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            if (mIsForRevision)
            {
                SQL.AppendLine("    And RevNo=@RevNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Revision, DrawingApprovalDocNo, RevNo  ");
                SQL.AppendLine("    From TblBOM2RevisionDtl3 ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And RevNo=@RevNo ");
            }
            SQL.AppendLine(") T Order By RevNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<decimal>(ref cm, "@RevNo", RevNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { "Revision", "DrawingApprovalDocNo" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        public static string GenerateDocNo(string DocDt, string DocType, string BOMGroup, string Tbl)
        {
            var SQL = new StringBuilder();
            bool
                IsDocNoFormatUseFullYear = Sm.GetParameterBoo("IsDocNoFormatUseFullYear"),
                IsDocNoResetByDocAbbr = Sm.GetParameterBoo("IsDocNoResetByDocAbbr"),
                IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                SQL.Append("       From " + Tbl);
                //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                if (IsDocNoResetByDocAbbr)
                    SQL.Append("       And DocNo Like '%/" + DocAbbr + "-" + BOMGroup + "/%' ");
                SQL.Append("       And BOMGroupCode = '" + BOMGroup + "' ");
                SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                //SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "','-', '"+ BOMGroup +"', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
                SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
                SQL.Append("       From " + Tbl);
                //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                if (IsDocNoResetByDocAbbr)
                    SQL.Append("       And DocNo Like '%/" + DocAbbr + "-" + BOMGroup + "/%' ");
                SQL.Append("       And BOMGroupCode = '" + BOMGroup + "' ");
                SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
                //SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '-', '" + BOMGroup + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }

        internal void GeneralTypeForQty()
        {
            //if (!ChkServiceItemInd.Checked)
            //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkServiceItemInd }, false);

            IsBomGroupCodeForGeneralType = IsSelectedBomGroupGeneralType();
            if (IsBomGroupCodeForGeneralType)
            {
                if (TxtSOContractDocNo.Text.Length > 0)
                {
                    TxtQty.EditValue = Sm.FormatNum(1m, 0);
                }
            }
            else
            {
                TxtQty.EditValue = Sm.FormatNum(mInitFGQty, 0);
            }
        }
        internal void BOMQtyByChkServiceInd()
        {
            if (!ChkServiceItemInd.Checked)
                TxtQty.EditValue = Sm.FormatNum(TxtQty1.Text, 0);
            else
                TxtQty.EditValue = Sm.FormatNum(1m, 0);

        }
        private bool IsSelectedBomGroupGeneralType()
        {
            if (mBomGroupCodeForGeneralType.Length == 0) return false;

            if(Sm.GetLue(LueBomGroupCode).Length == 0) return false;

            string[] Groups = mBomGroupCodeForGeneralType.Split(',');
            foreach(var x in Groups)
            {
                if (Sm.GetLue(LueBomGroupCode) == x)
                {
                    return true;
                }
            }

            return false;
        }

        private string GetColumnName(int index)
        {
            if (index == 0 || index > 702) return index.ToString();
            else
            {
                index = index - 1;
                const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                var value = "";

                if (index >= letters.Length)
                    value += letters[index / letters.Length - 1];

                value += letters[index % letters.Length];

                return value;
            }
        }

        private void GetParameter()
        {
            mMenuCodeForBom2FinishedGood = Sm.GetParameter("MenuCodeForBom2FinishedGood");
            mMenuCodeForBom2ComponentMaterial = Sm.GetParameter("MenuCodeForBom2ComponentMaterial");
            mMenuCodeForBom2Revision = Sm.GetParameter("MenuCodeForBom2Revision");
            mIsForSales = Sm.CompareStr(mMenuCodeForBom2FinishedGood, mMenuCode);
            mIsForRevision = Sm.CompareStr(mMenuCodeForBom2Revision, mMenuCode);
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mBomGroupCodeForGeneralType = Sm.GetParameter("BomGroupCodeForGeneralType");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsDocNoFormatBasedOnBOMGroup = Sm.GetParameterBoo("IsDocNoFormatBasedOnBOMGroup");
        }

        internal void ShowBomComponentAndMaterial(string DocNo)
        {
            ClearGrd();
            ml.Clear();

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                ShowBomDtl(DocNo, 0m);
                ShowBomDtl2(DocNo, 0m);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName, bool IsHeader)
        {
            if (IsUploadFileNotValid(Row, FileName, IsHeader)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);

            file.Close();
            ftpStream.Close();

            //if (!IsInsert) 
            if (IsHeader) TxtFileName.EditValue = toUpload.Name;
            else Grd1.Cells[Row, 9].Value = toUpload.Name;
            //if (IsInsert)
            //{
            //    var cml = new List<MySqlCommand>();
            //    cml.Add(UpdateFile(DocNo, Row, toUpload.Name));
            //    Sm.ExecCommands(cml);
            //}
        }

        private bool IsUploadFileNotValid(int Row, string FileName, bool IsHeader)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName, IsHeader) ||
                IsFileNameAlreadyExisted(Row, FileName, IsHeader)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if ( FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if ( FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if ( FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if ( FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName, bool IsHeader)
        {
            if ( FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    if (IsHeader)
                    {
                        Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                        TxtFileName.Focus();
                    }
                    else Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));

                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName, bool IsHeader)
        {
            if ( FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                if (IsHeader)
                {
                    SQL.AppendLine("Select DocNo ");
                    SQL.AppendLine("From TblBom2Hdr ");
                    SQL.AppendLine("Where FileName = @FileName ");
                    SQL.AppendLine("And FileName Is not Null ");
                    SQL.AppendLine("And CancelInd = 'N' ");
                    SQL.AppendLine("Limit 1; ");
                }
                else
                {
                    SQL.AppendLine("Select DocNo From TblBom2Dtl ");
                    SQL.AppendLine("Where FileName=@FileName ");
                    SQL.AppendLine("Limit 1; ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }


        #endregion

        #endregion

        #region Event

        #region Button events

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtFileName, "FileName", false))
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFileName.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = TxtFileName.Text;
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (!Sm.IsTxtEmpty(TxtFileName, "File", false) && downloadedData != null && downloadedData.Length != 0)
                {
                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        #endregion

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            }
        }

        private void LueBomGroupCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBomGroupCode, new Sm.RefreshLue2(Sl.SetLueOption), "BomGroup");

                GeneralTypeForQty();
                BOMQtyByChkServiceInd();
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBom3Dlg4(this));
        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBom3Dlg5(this));
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                try
                {
                    var f = new FrmSOContract2("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOContractDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void BtnDrawingApprovalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDocNo, "Document#", false))
                Sm.FormShowDialog(new FrmBom3Dlg6(this, TxtDocNo.Text));
        }

        private void BtnDrawingApprovalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDrawingApprovalDocNo, "Drawing Approval#", false))
            {
                try
                {
                    var f = new FrmDrawingApproval("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDrawingApprovalDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDrawingApproval("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDrawingApproval("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }
        
        #endregion

        #endregion

        #region Class

        internal class Bom2Dtl2
        {
            public string ItCode { set; get; }
            public string ItCode2 { set; get; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string UomCode { get; set; }
            public string Remark { get; set; }
            public string FileName { get; set; }
            public string SeqNo { get; set; }
        }

        private class BOMHdrIMS
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }

            public string ProjectName { get; set; }
            public string Project { get; set; }
            public string RevNo { get; set; }
            public string LocalCode { get; set; }
            public string FGName { get; set; }
            public string PrintBy { get; set; }
            public string Spesification { get; set; }
            public decimal Qty { get; set; }
            public string ItCode { get; set; }
            public string PurchaseUomCode { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string BOMGroup { get; set; }
            public string RevisionList { get; set; }

        }

        private class BOMDtlIMS
        {
            public string ItCode { get; set; }
            public string ItCode2 { get; set; }
            public string ItCodeInternal { get; set; }
            public string UomCode { get; set; }
            public string Specification { get; set; }
            public string FileName { get; set; }
            public string UOM { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }
            public decimal Number { get; set; }
            public string ItName { get; set; }
            public string RevNo { get; set; }

        }
        private class BOMSignarureIMS
        {
            public string UserName1 { get; set; }
            public string LastUpDt1 { get; set; }
            public string PosName1 { get; set; }
            public string EmpPict1 { get; set; }
            public string UserName2 { get; set; }
            public string LastUpDt2 { get; set; }
            public string PosName2 { get; set; }
            public string EmpPict2 { get; set; }
            public string Username3 { get; set; }
            public string PosName3 { get; set; }
            public string EmpPict3 { get; set; }
        }

        #endregion        

        private void ChkServiceItemInd_CheckedChanged(object sender, EventArgs e)
        {
            BOMQtyByChkServiceInd();
        }
    }
}
