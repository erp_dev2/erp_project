﻿#region Update

/*
    24/09/2018 [MEY] Menambah Kolom Tender Name di List of Quotation, di menu Tender Quotation Request
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTenderQuotationRequestDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmTenderQuotationRequest mFrmParent;
        private string mTenderDocNo = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTenderQuotationRequestDlg2(FrmTenderQuotationRequest FrmParent, string TenderDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTenderDocNo = TenderDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Quotation";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                var mStartDt = Sm.GetValue("Select Concat(DocDt, '000000') DocDt From TblTender Where DocNo = @Param Limit 1;", mTenderDocNo);
                var mExpiredDt = Sm.GetValue("Select Concat(ExpiredDt, '000000') ExpiredDt From TblTender Where DocNo = @Param Limit 1;", mTenderDocNo);
                DteDocDt1.DateTime = Sm.ConvertDate(mStartDt);
                DteDocDt2.DateTime = Sm.ConvertDate(mExpiredDt);
                TxtTenderName.Text = string.Empty;
                string mTenderName = string.Empty;
                mTenderName = Sm.GetValue("Select TenderName from TblTender Where DocNo = @Param;", mTenderDocNo);
                TxtTenderName.Text = mTenderName;
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Vendor",
                    "Term of Payment",
                    
                    //6-10
                    "Item's Code", 
                    "Item's Name",
                    "Currency",
                    "Price",
                    "QtDNo"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    160, 20, 80, 200, 120, 
                    
                    //6-10
                    100, 180, 80, 120, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 10 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.VdName, E.PtName, B.ItCode, D.ItName, ");
            SQL.AppendLine("A.CurCode, B.UPrice, B.DNo ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo = B.DocNo And B.ActInd = 'Y' And A.TenderDocNo Is Not Null And A.Status = 'A' And A.CancelInd = 'N' And A.TenderDocNo = @TenderDocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Left Join TblPaymentTerm E On A.PtCode = E.PtCode ");
            SQL.AppendLine("Where Concat(@TenderDocNo, '#', A.DocNo) Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(TenderDocNo, '#', QtDocNo) ");
            SQL.AppendLine("    From TblTenderQuotationRequest ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And TenderDocNo = @TenderDocNo ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start Date") ||
                Sm.IsDteEmpty(DteDocDt2, "End Date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@TenderDocNo", mTenderDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter +
                    " Order By A.CreateDt Desc;",
                    new string[] 
                    {                     
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "VdName", "PtName", "ItCode", "ItName", 
                        
                        //6-8
                        "CurCode", "UPrice", "DNo"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearData3();

                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtQtDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.mQtDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.mQtDNo = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtQtCurCode.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                mFrmParent.TxtUPrice.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                ChooseData();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmQt(mFrmParent.mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.Text = Sm.GetMenuDesc("FrmQt");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmQt(mFrmParent.mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.Text = Sm.GetMenuDesc("FrmQt");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region events

        #region Misc Control events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
