﻿#region Update
/*
    28/06/2020 [TKG/IMS] New Application
    27/10/2020 [TKG/IMS] durasi 0 bisa disave
    05/11/2020 [TKG/IMS] amount 0 bisa disave
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTVerification : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmOTVerificationFind FrmFind;
        internal decimal
            mOTVerificationRateWorkingday = 0m,
            mOTVerificationRateHoliday = 0m;

        #endregion

        #region Constructor

        public FrmOTVerification(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        private void GetParameter()
        {
            mOTVerificationRateWorkingday = Sm.GetParameterDec("OTVerificationRateWorkingday");
            mOTVerificationRateHoliday = Sm.GetParameterDec("OTVerificationRateHoliday");
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "OT Verification";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, TxtStatus }, true);
                GetParameter();
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Document#", 
                        "DNo", 
                        "Employee's Code",
                        "Employee's Name",

                        //6-10
                        "Old Code",
                        "Position",
                        "Department", 
                        "OT Date",
                        "Requested"+Environment.NewLine+"Start",

                        //11-15
                        "Requested"+Environment.NewLine+"End",
                        "Log"+Environment.NewLine+"Start",
                        "Log"+Environment.NewLine+"End",
                        "Verified"+Environment.NewLine+"Start",
                        "Verified"+Environment.NewLine+"End",

                        //16-20
                        "Duration"+Environment.NewLine+"Recess",
                        "Duration"+Environment.NewLine+"Log",
                        "Duration"+Environment.NewLine+"Verification",
                        "Duration",
                        "Rate",
                        
                        //21-24
                        "Amount",
                        "Remark",
                        "DeptCode",
                        "WSCode"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 150, 0, 130, 200,
                        
                        //6-10
                        130, 200, 200, 100, 100,
                        
                        //11-15
                        100, 100, 100, 100, 100,

                        //16-20
                        130, 130, 130, 130, 130,
                        
                        //21-23
                        130, 300, 0, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 23, 24 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 11, 12, 13, 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LuePGCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 19, 22 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LuePGCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 19, 22 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, LuePGCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 16, 17, 18, 19, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOTVerificationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (e.KeyCode == Keys.Enter)
            {
                if (Grd1.CurCell.Col.Index != Grd1.Cols.Count - 1)
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index, Grd1.CurCell.Col.Index + 1);
                else
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, 6);
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LuePGCode, "Payroll's group")) 
                        Sm.FormShowDialog(new FrmOTVerificationDlg(this, Sm.GetLue(LuePGCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 19, 22 }, e.ColIndex))
                    //if (e.ColIndex == 6 || e.ColIndex == 7) TmeRequestEdit(Grd1, TmeOTTm, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LuePGCode, "Payroll's group"))
                Sm.FormShowDialog(new FrmOTVerificationDlg(this, Sm.GetLue(LuePGCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 19 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 22 }, e);

            if (e.ColIndex == 19)
                Grd1.Cells[e.RowIndex, 21].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 19) * Sm.GetGrdDec(Grd1, e.RowIndex, 20);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OTVerification", "TblOTVerificationHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOTVerificationHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) 
                    cml.Add(SaveOTVerificationDtl(DocNo, r));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePGCode, "Payroll's group") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsOTRequestAlreadyProcessed();
        }

        private bool IsOTRequestAlreadyProcessed()
        {
            var SQL = 
                "Select 1 From TblOTVerificationHdr A, TblOTVerificationDtl B " +
                "Where A.CancelInd='N' And A.Status In ('O', 'A') And A.DocNo=B.DocNo " +
                "And B.OTRequestDocNo=@Param1 And B.OTRequestDNo=@Param2;";

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.IsDataExist(SQL, Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 3), string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "OT Request# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                        "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                        "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                        "Row : " + r.ToString() + Environment.NewLine + Environment.NewLine +
                        "This document already processed."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var Msg = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                Msg =
                    "OT# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine + Environment.NewLine;

                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, Msg+"OT# is empty.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveOTVerificationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblOTVerificationHdr(DocNo, DocDt, Status, CancelReason, CancelInd, PGCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', Null, 'N', @PGCode, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='OTVerification'; ");

            SQL.AppendLine("Update TblOTVerificationHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblDocApproval Where DocType='OTVerification' And DocNo=@DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOTVerificationDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOTVerificationDtl ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, DeptCode, OTRequestDocNo, OTRequestDNo, OTDt, ");
            SQL.AppendLine("OTRequestStartTm, OTRequestEndTm, LogStartTm, LogEndTm, OTVerificationStartTm, OTVerificationEndTm, ");
            SQL.AppendLine("DurationRecess, DurationLog, DurationVerification, Duration, Rate, Amt, Remark, WSCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @EmpCode, @DeptCode, ");
            SQL.AppendLine("@OTRequestDocNo, @OTRequestDNo, @OTDt, ");
            SQL.AppendLine("@OTRequestStartTm, @OTRequestEndTm, @LogStartTm, @LogEndTm, @OTVerificationStartTm, @OTVerificationEndTm, ");
            SQL.AppendLine("@DurationRecess, @DurationLog, @DurationVerification, @Duration, @Rate, @Amt, @Remark, @WSCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@OTDt", Sm.GetGrdDate(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@OTRequestDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@OTRequestDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@OTRequestStartTm", Sm.GetGrdStr(Grd1, Row, 10).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@OTRequestEndTm", Sm.GetGrdStr(Grd1, Row, 11).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@LogStartTm", Sm.GetGrdStr(Grd1, Row, 12).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@LogEndTm", Sm.GetGrdStr(Grd1, Row, 13).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@OTVerificationStartTm", Sm.GetGrdStr(Grd1, Row, 14).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@OTVerificationEndTm", Sm.GetGrdStr(Grd1, Row, 15).Replace(":", ""));
            Sm.CmParam<Decimal>(ref cm, "@DurationRecess", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@DurationLog", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@DurationVerification", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Duration", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@WSCode", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditOTRequestDtl());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return IsDataAlreadyCancelled();
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblOTVerificationHdr ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And (CancelInd='Y' Or Status='C'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditOTRequestDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOTVerificationHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOTVerificationHdr(DocNo);
                ShowOTVerificationDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowOTVerificationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ");
            SQL.AppendLine("Case Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("CancelReason, CancelInd, PGCode, Remark ");
            SQL.AppendLine("From TblOTVerificationHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                       "DocNo", 
                       
                       //1-5
                       "DocDt", "StatusDesc", "CancelReason", "CancelInd", "PGCode", 
                       
                       //6
                       "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[5]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
            );
        }

        private void ShowOTVerificationDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, ");
            SQL.AppendLine("A.OTRequestDocNo, A.OTRequestDNo, A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, A.OTDt, ");
            SQL.AppendLine("A.OTRequestStartTm, A.OTRequestEndTm, A.LogStartTm, A.LogEndTm, A.OTVerificationStartTm, A.OTVerificationEndTm, ");
            SQL.AppendLine("A.DurationRecess, A.DurationLog, A.DurationVerification, A.Duration, A.Rate, A.Amt, A.Remark, A.DeptCode, A.WSCode ");
            SQL.AppendLine("From TblOTVerificationDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "OTRequestDocNo", "OTRequestDNo", "EmpCode", "EmpName", "EmpCodeOld", 
                    
                    //6-10
                    "PosName", "DeptName", "OTDt", "OTRequestStartTm", "OTRequestEndTm", 
                    
                    //11-15
                    "LogStartTm", "LogEndTm", "OTVerificationStartTm", "OTVerificationEndTm", "DurationRecess", 
                    
                    //16-20
                    "DurationLog", "DurationVerification", "Duration", "Rate", "Amt", 
                    
                    //21-23
                    "Remark", "DeptCode", "WSCode" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        //private void TmeRequestEdit(iGrid Grd, DevExpress.XtraEditors.TimeEdit Tme, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        //{
        //    e.DoDefault = false;

        //    fCell = Grd.Cells[e.RowIndex, e.ColIndex];
        //    fCell.EnsureVisible();
        //    Rectangle myBounds = fCell.Bounds;
        //    myBounds.Width -= Grd.GridLines.Vertical.Width;
        //    myBounds.Height -= Grd.GridLines.Horizontal.Width;
        //    if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

        //    Rectangle myCellsArea = Grd.CellsAreaBounds;
        //    if (myBounds.Right > myCellsArea.Right)
        //        myBounds.Width -= myBounds.Right - myCellsArea.Right;
        //    if (myBounds.Bottom > myCellsArea.Bottom)
        //        myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

        //    myBounds.Offset(Grd.Location);

        //    Tme.Bounds = myBounds;

        //    if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
        //        Tme.EditValue = null;
        //    else
        //        Sm.SetTme(Tme, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Replace(":", ""));

        //    Tme.Visible = true;
        //    Tme.Focus();

        //    fAccept = true;
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        #endregion

        #endregion
    }
}
