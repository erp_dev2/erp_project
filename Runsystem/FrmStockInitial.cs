﻿#region Update
/* 
    18/04/2017 [WED] Save DNo ke tabel TblStockInitialDtl ditambah jadi 5 digit
    18/04/2017 [WED] Validasi max item menjadi 99.999 item
    18/05/2017 [WED] Lot dan Bin dibuat isian combobox
    20/06/2017 [TKG] Update remark di journal
    06/07/2017 [HAR] Tambah entity di journal berdasarkan parameter
    07/08/2017 [WED] Tambah kolom ForeignName, berdasarkan parameter IsShowForeignName
    17/07/2018 [TKG] tambah cost center saat journal
    26/07/2018 [TKG] konversi antar uom.
    01/10/2018 [TKG] untuk KIM menggunakan document date sebagai batch# kalau kosong
    16/12/2019 [TKG/IMS] journal untuk moving average
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    25/08/2020 [IBL/MGI] menambahkan field group berdasarkan parameter IsUseProductionWorkGroup
    01/10/2020 [TKG/IMS] tambah local code
    27/05/2021 [TKG/SRN] tambah validasi setting journal
    23/03/2022 [VIN/YK] BUG save journal group By
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmStockInitial : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmStockInitialFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string 
            mMainCurCode = string.Empty,
            mAcNoForInitialStockOpeningBalance = string.Empty,
            mDocType = "04",
            mEntCode = string.Empty;
        internal bool 
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsInvTrnShowItSpec = false,
            mIsUseProductionWorkGroup = false;
        private bool 
            mIsAutoJournalActived = false, 
            mIsBatchNoUseDocDtIfEmpty = false, 
            mIsMovingAvgEnabled = false,
            mIsJournalValidationStockInitialEnabled = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmStockInitial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                SetLueLot(ref LueLot);
                LueLot.Visible = false;
                SetLueBin(ref LueBin);
                LueBin.Visible = false;
                if (!mIsUseProductionWorkGroup)
                {
                    LueProductionWorkGroup.Visible = false;
                    panel2.Height = 71;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "Item's Name",
                        
                        //6-10
                        "Local Code",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",

                        //11-15
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Price", 
                        "Remark",
                        "Group",
                        "Foreign Name",

                        //21
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        50, 0, 20, 100, 200, 
                        //6-10
                        130, 200, 180, 100, 100, 
                        //11-15
                        80, 60, 80, 60, 80, 
                        //16-20
                        60, 100, 300, 100, 150,
                        //21
                        200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 17 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 8, 9, 13, 14, 15, 16, 18, 19, 21 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 20 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 8, 12, 14, 16, 19, 20, 21 });
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[19].Visible = true;
                Grd1.Cols[19].Move(7);
            }
            Grd1.Cols[20].Move(8);
            if (mIsInvTrnShowItSpec)
            {
                Grd1.Cols[21].Visible = true;
                Grd1.Cols[21].Move(9);
            }

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        "Total Quantity 1", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[] 
                    {
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 18 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1 }, true);
            }
            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, true);

            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueCurCode, TxtExcRate, MeeRemark, LueProductionWorkGroup
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 7, 9, 10, 11, 13, 15, 17, 18 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCurCode, TxtExcRate, MeeRemark, LueProductionWorkGroup
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 7, 9, 10, 11, 13, 15, 17, 18 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueCurCode, TxtExcRate, MeeRemark, LueProductionWorkGroup
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 13, 15, 17 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmStockInitialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueCurCode, mMainCurCode);
                TxtExcRate.EditValue = Sm.FormatNum(1, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue("Select C.EntCode "+
                            "From TblWarehouse A "+
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  "+
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '"+Sm.GetLue(LueWhsCode)+"' limit 1");

                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text, (int)mNumberOfInventoryUomCode);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmStockInitialDlg(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 7, 9, 10, 11, 13, 15, 17, 18 }, e.ColIndex))
                    {
                        if (Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
                        {
                            LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                            //SetLueLot(ref LueLot);
                        }

                        if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
                        {
                            LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                            //SetLueBin(ref LueBin);
                        }

                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 13, 15, 17 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2)||Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length==0)) 
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalQty();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmStockInitialDlg(this));                
            
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 13, 15, 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7, 9, 10, 18 }, e);

            if (e.ColIndex == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 11, 13, 15, 12, 14, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 11, 15, 13, 12, 16, 14);
            }

            if (e.ColIndex == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 13, 11, 15, 14, 12, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 13, 15, 11, 14, 16, 12);
            }

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 15, 11, 13, 16, 12, 14);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 15, 13, 11, 16, 14, 12);
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 14)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 13, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 13);

            if (Sm.IsGrdColSelected(new int[] { 11, 13, 15 }, e.ColIndex)) ComputeTotalQty();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11, 13, 15 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockInitial", "TblStockInitialHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveStockInitialHdr(DocNo));
            cml.Add(SaveStockInitialDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
            //        cml.Add(SaveStockInitialDtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
                ParPrint(DocNo, (int)mNumberOfInventoryUomCode);
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) ||
                IsJournalSettingInvalid();
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationStockInitialEnabled) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (mAcNoForInitialStockOpeningBalance.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForInitialStockOpeningBalance is empty.");
                return true;
            }

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# ("+ItCtName+") is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count >100000)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item data entered (" + (Grd1.Rows.Count-1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsInventoryQtyMandatory = Sm.GetParameter("IsInventoryQtyMandatory")=="Y";
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg = 
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine 
                    ;
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 4 , false, "Item is empty.") ||
                    (!IsInventoryQtyMandatory && 
                    (
                        Sm.IsGrdValueEmpty(Grd1, Row, 11, true, Msg + "Quantity (1) is empty.") ||
                        (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Quantity (2) is empty.")) ||
                        (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 && mNumberOfInventoryUomCode == 3 && Sm.IsGrdValueEmpty(Grd1, Row, 15, true, Msg + "Quantity (3) is empty."))
                    ))
                    ) 
                    return true;

                for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                {
                    if (Row != Row2 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4) + Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row2, 4) + Sm.GetGrdStr(Grd1, Row2, 7)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                            "Duplicate item in the list."
                            );
                        Sm.FocusGrd(Grd1, Row, 1);
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveStockInitialHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblStockInitialHdr(DocNo, DocDt, WhsCode, CurCode, ExcRate, ProductionWorkGroup, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @CurCode, @ExcRate, @ProductionWorkGroup, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveStockInitialDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* StockInitial - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblStockInitialDtl(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() + 
                        ", 'N', @ItCode_" + r.ToString() + 
                        ", ");
                    SQL.AppendLine("IfNull(@BatchNo_" + r.ToString() + ", ");
                    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
                    SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                        "), IfNull(@Lot_" + r.ToString() + 
                        ", '-'), IfNull(@Bin_" + r.ToString() + 
                        ", '-'), ");
                    SQL.AppendLine("@Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @Remark_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 18));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        private MySqlCommand SaveStockInitialDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInitialDtl(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, ");
            SQL.AppendLine("IfNull(@BatchNo, ");
            SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty?"@DocDt":"'-'");
            SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-') , ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @UPrice, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, B.UPrice, A.ExcRate, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockInitialHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Stock Initial : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, Remark As RemarkJournal From (");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select D.AcNo, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0.00 As DAmt, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo, Remark ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@Remark", DocNo);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "'XXXXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    cml.Add(EditStockInitialDtl(Row));
            }
            if (mIsAutoJournalActived) cml.Add(SaveJournal2(DNo));
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblStockInitialDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsStockNotEnough() ||
                IsJournalSettingInvalid();
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXXXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false; 
        }

        private bool IsStockNotEnough()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                    if (IsStockNotEnough(r)) return true;
            }
            return false;
        }

        private bool IsStockNotEnough(int r)
        {
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;
            decimal Stock = 0m, Stock2 = 0m, Stock3 = 0m;

            if (Sm.GetGrdStr(Grd1, r, 11).Length > 0) Qty = Sm.GetGrdDec(Grd1, r, 11);
            if (Sm.GetGrdStr(Grd1, r, 13).Length > 0) Qty2 = Sm.GetGrdDec(Grd1, r, 13);
            if (Sm.GetGrdStr(Grd1, r, 15).Length > 0) Qty3 = Sm.GetGrdDec(Grd1, r, 15);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Qty, Qty2, Qty3 From TblStockSummary ");
            SQL.AppendLine("Where Source=@Source ");
            SQL.AppendLine("And WhsCode=@WhsCode ");
            SQL.AppendLine("And Source=@Lot ");
            SQL.AppendLine("And Bin=@Bin Limit 1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 8));
                Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 9));
                Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 10));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Qty", "Qty2", "Qty3" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Stock = Sm.DrDec(dr, 0);
                        Stock2 = Sm.DrDec(dr, 1);
                        Stock3 = Sm.DrDec(dr, 2);
                    }
                }
                dr.Close();
            }

            if (Qty < Stock)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine + 
                     "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + 
                     "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine + 
                     "Stock : " + Sm.FormatNum(Stock, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            if (mNumberOfInventoryUomCode>=2 && Qty2 < Stock2)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock2, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty2, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            if (mNumberOfInventoryUomCode >= 3 && Qty3 < Stock3)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock3, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty3, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditStockInitialDtl(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockInitialDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo=@DNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary T Set ");
            SQL.AppendLine("    T.Qty=0, T.Qty2=0, T.Qty3=0, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string DNo)
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockInitialDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo In (" + DNo + "); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling Stock Initial : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("(Select CCCode From TblWarehouse Where WhsCode=@WhsCode), ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");
            
            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, RemarkJournal From (");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark As RemarkJournal");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt, B.Remark As RemarkJournal");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark As RemarkJournal");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo And B.DNo In (" + DNo + ") ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo, RemarkJournal ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowStockInitialHdr(DocNo);
                ShowStockInitialDtl(DocNo);
                ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowStockInitialHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, CurCode, ExcRate, ProductionWorkGroup, Remark From TblStockInitialHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "CurCode", "ExcRate", "Remark",
                        "ProductionWorkGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[3]));
                        TxtExcRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[6]));
                    }, true
                );
        }

        private void ShowStockInitialDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.ItCode, B.ItName, B.ItCodeInternal, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, ");
            SQL.AppendLine("A.UPrice, A.Remark, B.ItGrpCode, B.ForeignName, B.Specification ");
            SQL.AppendLine("From TblStockInitialDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "ItCode", "ItName", "ItCodeInternal", "BatchNo", 
                    
                    //6-10
                    "Source", "Lot", "Bin", "Qty", "InventoryUomCode", 

                    //11-15
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "UPrice", 
 
                    //16-19
                    "Remark", "ItGrpCode", "ForeignName", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13); 
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 13, 15, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += ((SQL.Length != 0?",":"") + "'" + Sm.GetGrdStr(Grd1, Row, 4) + "'");
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ParPrint(string DocNo, int parValue)
        {
            var l = new List<IS>();
            var ldtl = new List<ISDtl>();

            string[] TableName = { "IS", "ISDtl" };
            List<IList> myLists = new List<IList>();

            #region Header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, A.Remark ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         "WhsName",
                         "Remark",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new IS()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            HRemark = Sm.DrStr(dr, c[7]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.BatchNo,  A.Source, A.Lot, A.Bin, A.Qty, B.InventoryUomCode, ");
                SQLDtl.AppendLine("A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, ");
                SQLDtl.AppendLine("A.UPrice, C.CurCode, A.Remark, B.ItGrpCode ");
                SQLDtl.AppendLine("From TblStockInitialDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblStockInitialHdr C On A.DocNo = C.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "BatchNo",
                         "Source",
                         "Lot",
                         "Bin",

                         //6-10
                         "Qty" ,
                         "InventoryUomCode",
                         "Qty2" ,
                         "InventoryUomCode2",
                         "Qty3" ,

                         //11-15
                         "InventoryUomCode3",
                         "UPrice",
                         "CurCode",
                         "Remark",
                         "ItGrpCode"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new ISDtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),

                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[7]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[8]),

                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[9]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            UPrice = Sm.DrDec(drDtl, cDtl[12]),
                            CurCode = Sm.DrStr(drDtl, cDtl[13]),

                            DRemark = Sm.DrStr(drDtl, cDtl[14]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[15]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("InitialStock", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("InitialStock2", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("InitialStock3", myLists, TableName, false);
                    break;
            }
        }

        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 11;
            int col2 = 0;
            while (col <= 15)
            {
                Total = 0m;
                for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, col).Length != 0) Total += Sm.GetGrdDec(Grd1, row, col);
                
                if (col == 11) col2 = 0;
                if (col == 13) col2 = 1;
                if (col == 15) col2 = 2;
                Grd2.Cells[0, col2].Value = Total;
                col += 2;
            }
        }

        private void GetParameter()
        {
            //SetNumberOfInventoryUomCode();
            //mMainCurCode = Sm.GetParameter("MainCurCode");
            //mAcNoForInitialStockOpeningBalance = Sm.GetParameter("AcNoForInitialStockOpeningBalance");
            //mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            //mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            //mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            //mIsBatchNoUseDocDtIfEmpty = Sm.GetParameterBoo("IsBatchNoUseDocDtIfEmpty");
            //mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            //mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            //mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
            //mIsJournalValidationStockInitialEnabled = Sm.GetParameterBoo("IsJournalValidationStockInitialEnabled");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsItGrpCodeShow', 'MainCurCode', 'IsAutoJournalActived', 'AcNoForInitialStockOpeningBalance', 'IsShowForeignName', ");
            SQL.AppendLine("'IsBatchNoUseDocDtIfEmpty', 'IsMovingAvgEnabled', 'IsInvTrnShowItSpec', 'IsUseProductionWorkGroup', 'IsJournalValidationStockInitialEnabled', ");
            SQL.AppendLine("'NumberOfInventoryUomCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsInvTrnShowItSpec": mIsInvTrnShowItSpec = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsJournalValidationStockInitialEnabled": mIsJournalValidationStockInitialEnabled = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "AcNoForInitialStockOpeningBalance": mAcNoForInitialStockOpeningBalance = ParValue; break;
                            
                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 10].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
            
            //if (LueBin.Visible && fAccept && fCell.ColIndex == 10)
            //{
            //    if (Sm.GetLue(LueBin).Length == 0)
            //        Grd1.Cells[fCell.RowIndex, 21].Value =
            //        Grd1.Cells[fCell.RowIndex, 10].Value = null;
            //    else
            //    {
            //        Grd1.Cells[fCell.RowIndex, 21].Value = Sm.GetLue(LueBin);
            //        Grd1.Cells[fCell.RowIndex, 10].Value = LueBin.GetColumnValue("Col2");
            //    }
            //    LueBin.Visible = false;
            //    Sm.SetGrdNumValueZero(ref Grd1, (fCell.RowIndex + 1), new int[] { 11, 13, 15, 17 });
            //}
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 9].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
            
            //if (LueLot.Visible && fAccept && fCell.ColIndex == 9)
            //{
            //    if (Sm.GetLue(LueLot).Length == 0)
            //        Grd1.Cells[fCell.RowIndex, 20].Value =
            //        Grd1.Cells[fCell.RowIndex, 9].Value = null;
            //    else
            //    {
            //        Grd1.Cells[fCell.RowIndex, 20].Value = Sm.GetLue(LueLot);
            //        Grd1.Cells[fCell.RowIndex, 9].Value = LueLot.GetColumnValue("Col2");
            //    }
            //    LueLot.Visible = false;
            //    Sm.SetGrdNumValueZero(ref Grd1, (fCell.RowIndex + 1), new int[] { 11, 13, 15, 17 });
            //}
        }

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }

        #endregion

        #endregion

        #region Class

        private class IS
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string HRemark { get; set; }
            public string PrintBy { get; set; }
        }

        private class ISDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public decimal UPrice { get; set; }
            public string CurCode { get; set; }
            public string DRemark { get; set; }
            public string ItGrpCode { get; set; }
        }

        #endregion

    }
}
