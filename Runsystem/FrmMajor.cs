﻿#region Update 

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMajor : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmMajorFind FrmFind;

        #endregion

        #region Constructor

        public FrmMajor(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtMajorCode, TxtMajorName, ChkActInd
                    }, true);
                    TxtMajorCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtMajorCode, TxtMajorName, 
                    }, false);
                    TxtMajorCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtMajorName, ChkActInd
                    }, false);
                    TxtMajorName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtMajorCode, TxtMajorName, 
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMajorFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtMajorCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtMajorCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblMajor Where MajorCode=@MajorCode" };
                Sm.CmParam<String>(ref cm, "@MajorCode", TxtMajorCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblMajor(MajorCode, MajorName, ActiveInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@MajorCode, @MajorName,  @ActiveInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update MajorName=@MajorName, ActiveInd=@ActiveInd,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@MajorCode", TxtMajorCode.Text);
                Sm.CmParam<String>(ref cm, "@MajorName", TxtMajorName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActInd.Checked ? "Y" : "N");
                Sm.ExecCommand(cm);

                ShowData(TxtMajorCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string MajorCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@MajorCode", MajorCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select MajorCode, MajorName, ActiveInd From TblMajor Where majorCode=@MajorCode",
                        new string[] 
                        {
                            "MajorCode", 
                            "MajorName", "ActiveInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtMajorCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtMajorName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtMajorCode, "Major code", false) ||
                Sm.IsTxtEmpty(TxtMajorName, "Major name", false) ||
                IsMajorCodeExisted()||
                IsMajorAlreadyNonActive();
        }

        private bool IsMajorCodeExisted()
        {
            if (!TxtMajorCode.Properties.ReadOnly && Sm.IsDataExist("Select MajorCode From Tblmajor Where MajorCode='" + TxtMajorCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "major code ( " + TxtMajorCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsMajorAlreadyNonActive()
        {
            if (Sm.IsDataExist("Select MajorCode From TblMajor Where MajorCode='" + TxtMajorCode.Text + "' And ActiveInd='N' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Major code ( " + TxtMajorCode.Text + " ) already Non Active.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtMajorCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMajorCode);
        }
        private void TxtMajorName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMajorName);
        }

        #endregion

        
     

        #endregion

       
    }
}
