#region Update
/*
    09/06/2017 [WED] tambah kolom ProposeCandidateDocNo dan ProposeCandidateDNo
    07/08/2017 [HAR] tambah kolom remark dan tambah informasi entity
    22/08/2017 [HAR] saat cancel PPS jika resign date new ada isinya, maka di master employee resign dt kembali null
    28/08/2017 [TKG] merubah entity di master employee
    30/08/2017 [TKG] tambah indikator untuk merubah secara otomatis leave start date di master employee.
    30/10/2017 [TKG] otorisasi department dan site berdasarkan group
    02/01/2018 [ARI] emp job transfer berdasarkan group
    19/03/2018 [ARI] tambah printout
    27/03/2018 [TKG] tambah employee request document 
    29/03/2018 [ARI] revisi printout (tambah logo, dll)
    31/03/2018 [TKG] filter by level
    05/07/2018 [HAR] posisi employee request dibawah employeecode
    20/09/2018 [TKG] tambah position status
    17/12/2018 [WED] tambah fitur untuk open form EmpSalary berdasarkan parameter IsPPSAutoShowEmpSalary
    11/11/2018 [TKG/SRN] update division di master employee berdasarkan department
    11/12/2019 [WED/IMS] tambah informasi Contract Date, berdasarkan parameter IsEmpContractDtMandatory
    23/07/2020 [IBL/SIER] tambah informasi Acting Official (PLT) 
    24/07/2020 [WED/SIER] tambah Section, berdasarkan parameter IsPPSUseSection
    03/11/2020 [VIN/PHT] Generate Employee Code Old
    22/11/2020 [TKG/PHT] tambah level
    01/01/2021 [TKG/PHT] tambah work period
    01/01/2021 [TKG/PHT] update allowance based on parameter IsPPSAutoUpdEmpADBasedOnLevel
    18/02/2021 [IBL/PHT] tambah multi attachment file berdasarkan parameter IsPPSAllowToUploadFile
    28/05/2021 [RDA/SIER] mengatur check box Acting Official (PLT) berdasarkan parameter IsPltCheckboxNotBasedOnPosition
    02/06/2021 [RDA/SIER] update table employee jika informasi Acting Official (PLT) dicentang user
    30/07/2021 [ICA/PHT] Update Allowance based on parameter PPSAutoUpdEmpADBasedOnLevelSource -> update nya di event db
    30/09/2021 [ICA/PHT] Allowance deduction update saat save pps jika startdate <= currentdate
    16/11/2021 [ICA/PHT] menambah panjang karakter saat input lue position
    16/11/2021 [IBL/SIER] menambah parameter untuk mengambil sumber data ActingOfficialInd -> ActingOfficialIndSource
    18/11/2021 [ICA/SIER] ketika cancel pps ActingOfficialInd dikembalikan ke data ActingOfficialIndOld
    27/01/2022 [VIN/PHT] Saat cancel mengembalikan level seperti sebelumnya 
    04/03/2022 [RDA/PHT] saat insert, tunjangan akan terupdate secara otomatis dan tergenerate di ESADA ketika karyawan mutasi, promosi dan demosi
    31/03/2022 [RDA/PHT] tunjangan terupdate secara otomatis di ESADA melihat position hanya ketika position code menu pps = position di level
    04/07/2022 [MYA/PHT] Work periode di pps menjadi mandatory 
    06/07/2022 [MYA/PHT] Work periode dan remark di pps menjadi editable, di sebelah work periode ada loop yang mengcapture history edit dari work periode 
    13/07/2022 [ICA/PHT] update enddate untuk allowance yg sudah tidak di pakai 
    21/07/2022 [ICA/PHT] masih ada bug saat update enddate, ada tunjangan yg seharusnya tidak di non active kan
    22/07/2022 [ICA/PHT] otomatisasi update smk statis based on param SMKStatisAmtSource
    05/08/2022 [ICA/PHT] update enddate allowance berdasarkan parameter, jobtransfer(M,P,D), harus ada salah satu perubahan (grade, pos, level, site)
    15/08/2022 [ICA/PHT] bug DNO null di ketika input smk statis
    16/12/2022 [ICA/PHT] update endate SMK Statis tidak melihat job transfer nya
    21/03/2023 [WED/PHT] pengerucutan isian combo Department, Level, Position Status berdasarkan parameter IsLevelUsePositionStatus, IsPPSDepartmentBasedOnSite, IsDepartmentPositionEnabled, IsPositionUseLevel
    03/04/2023 [WED/PHT] Position Status list combobox melihat SetLuePositionStatus(ref Lue, string Code)
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPPS : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmPPSFind FrmFind;
        internal string 
            mProposeCandidateDocNo = string.Empty,
            mProposeCandidateDNo = string.Empty,
            mEmpCodePPs = string.Empty;
        internal bool 
            mIsNotFilterByAuthorization = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsFilterByLevelHR = false,
            mIsEmpContractDtMandatory = false,
            mIsPPSUseSection = false,
            mIsOldCodeAutoFillAfterPromotion = false,
            mIsPPSUseLevel = false,
            mIsPPSDepartmentBasedOnSite = false,
            mIsLevelUsePositionStatus = false,
            mIsDepartmentPositionEnabled = false,
            mIsPositionUseLevel = false,
            IsFromDlg = false
            ;
        internal string
            DeptCodeOld = string.Empty, 
            PosCodeOld = string.Empty, 
            GrdLvlCodeOld = string.Empty, 
            EmploymentStatusOld = string.Empty, 
            SystemTypeOld = string.Empty, 
            PayrunPeriodOld = string.Empty, 
            PGCodeOld = string.Empty, 
            SiteCodeOld = string.Empty,
            mPositionStatusCodeOld = string.Empty,
            mSectionCodeOld = string.Empty,
            mEmploymentStatusTetap = string.Empty,
            mLevelCodeOld = string.Empty;
        private string
            mPPSAutoUpdEmpADBasedOnLevelSource = string.Empty;
        private bool 
            mIsPPSApprovalBasedOnDept = false,
            mIsAnnualLeaveUseStartDt = false,
            mIsPPSShowJobTransferByGrp = false,
            mIsPPSAutoShowEmpSalary = false,
            mIsPPSUpdateEmpDivBasedOnDept = false,
            mIsPPSShowEmpPerformanceAllowance = false,
            mIsPPSAutoUpdEmpADBasedOnLevel = false,
            mIsPPSAllowToUploadFile = false,
            mIsPltCheckboxNotBasedOnPosition = false,
            mIsWorkPeriodePPSMandatory = false,
            mIsWorkPeriodePPSEditable = false;

        internal string
             mHostAddrForFTPClient = string.Empty,
             mPortForFTPClient = string.Empty,
             mUsernameForFTPClient = string.Empty,
             mPasswordForFTPClient = string.Empty,
             mSharedFolderForFTPClient = string.Empty,
             mFileSizeMaxUploadFTPClient = string.Empty,
             mActingOfficialIndSource = string.Empty;

        #endregion

        #region Constructor

        public FrmPPS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Payrol Process Setting";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();
                SetFormControl(mState.View);
                ChkActingOfficialOld.Properties.ReadOnly = ChkActingOfficialNew.Properties.ReadOnly = true;
                base.FrmLoad(sender, e);
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                if (mIsPPSShowJobTransferByGrp)
                    SetLueEmpJobTransfer(ref LueType);
                else
                Sl.SetLueOption(ref LueType, "EmpJobTransfer");

                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLuePositionStatusCode(ref LuePositionStatusCode);
                SetLueSection(ref LueSectionCode, string.Empty);
                if (!mIsPPSUseSection)
                {
                    LblSectionOld.Visible = TxtSectionCodeOld.Visible = LblSectionNew.Visible = LueSectionCode.Visible = false;
                }

                if (mIsWorkPeriodePPSMandatory)
                {
                    label12.ForeColor = Color.Red;
                }
                if (!mIsWorkPeriodePPSEditable)
                {
                    BtnLogWorkPeriod.Visible = false;
                }

                if (!mIsPPSAllowToUploadFile)
                    tabControl1.TabPages.RemoveAt(1);
                
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteStartDt, LueType, TxtEmpCode, 
                        TxtEmpName, TxtDeptName, TxtPosition, TxtGrade, TxtEmploymentStatus, 
                        TxtPayrunPeriod, TxtSystemType, TxtPGCode, LueDeptCode, LueGrdLvlCode, 
                        LuePosCode, LueEmploymentStatus, LuePayrunPeriod, LueSystemType, LuePGCode, 
                        ChkCancelInd, TxtSiteCode, LueSiteCode, DteResignDtOld, DteResignDtNew,
                        TxtEntityName, LuePositionStatusCode, MeeRemark, LueSectionCode, TxtSectionCodeOld,
                        TxtLevelCode, LueLevelCode, TxtWorkPeriodYr, TxtWorkPeriodMth, TxtFile, TxtFile2,
                        TxtFile3,
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnEmpCode2.Enabled = true;
                    BtnEmployeeRequestDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    ChkUpdLeaveStartDtInd.Properties.ReadOnly = true;

                    BtnFile.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnFile3.Enabled = false;
                    ChkFile.Enabled = false;
                    ChkFile2.Enabled = false;
                    ChkFile3.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteStartDt, LueType, LueDeptCode, LueGrdLvlCode, 
                        LuePosCode, LueEmploymentStatus, LueSystemType, LueType, LuePayrunPeriod, 
                        LuePGCode, DteResignDtNew, LuePositionStatusCode, MeeRemark, TxtWorkPeriodYr, 
                        TxtWorkPeriodMth
                    }, false);
                    if (mIsPPSUseSection) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueSectionCode }, false);
                    if(mIsFilterBySiteHR)Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueSiteCode }, false);
                    if (mIsAnnualLeaveUseStartDt) ChkUpdLeaveStartDtInd.Properties.ReadOnly = false;
                    if (mIsPPSUseLevel) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueLevelCode }, false);
                    BtnEmpCode.Enabled = true;
                    BtnEmpCode2.Enabled = true;
                    BtnEmployeeRequestDocNo.Enabled = true;

                    if (mIsPPSAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnFile3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    if(mIsPltCheckboxNotBasedOnPosition) ChkActingOfficialNew.Properties.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    if (mIsWorkPeriodePPSEditable)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWorkPeriodYr, TxtWorkPeriodMth, MeeRemark }, false);
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
              TxtDocNo, LueType, DteDocDt, TxtEmpCode, TxtEmpName, 
              TxtDeptName, TxtPosition, DteStartDt, TxtGrade, TxtSystemType, 
              TxtPayrunPeriod, TxtEmploymentStatus, TxtPGCode, LueEmploymentStatus, LueDeptCode, 
              LueGrdLvlCode, LuePosCode, LueSystemType, LuePosCode, LuePayrunPeriod, 
              LuePGCode, TxtSiteCode, LueSiteCode, DteResignDtOld, DteResignDtNew, 
              TxtEntityName, TxtEmployeeRequestDocNo, TxtPositionStatusCode, LuePositionStatusCode, MeeRemark,
              TxtSectionCodeOld, LueSectionCode, TxtLevelCode, LueLevelCode, TxtFile, TxtFile2, TxtFile3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtWorkPeriodYr, TxtWorkPeriodMth }, 21);
            ChkCancelInd.Checked = false;
            ChkActingOfficialOld.Checked = false;
            ChkActingOfficialNew.Checked = false;
            ChkUpdLeaveStartDtInd.Checked = false;
            mProposeCandidateDocNo = string.Empty;
            mProposeCandidateDNo = string.Empty;

            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPPSFind(this, mIsFilterBySiteHR?"Y":"N");
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueGrdLvlCode(ref LueGrdLvlCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                if (mIsPPSUseLevel) Sl.SetLueLevelCode(ref LueLevelCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                    
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            string[] TableName = { "PPs", "PPs2" };

            var l = new List<PPs>();
            var l2 = new List<PPs2>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine(" A.DocNo, date_format(A.docdt, '%d %M %Y')As DocDt, date_format(A.StartDt, '%d %M %Y')As StartDt, A.EmpCode, B.EmpName, C.SiteName, ");
            SQL.AppendLine(" B.Gender, date_format(B.ResignDt, '%d %M %Y')As ResignDt, D.PosName, ");
            SQL.AppendLine(" IfNull(Timestampdiff(Year, B.JoinDt, B.ResignDt),'0')As Yr, Ifnull(Timestampdiff(Month, B.JoinDt, B.ResignDt) % 12, '0') As Mth ");
            SQL.AppendLine(" From TblPPS A ");
            SQL.AppendLine(" Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine(" Left Join TblSite C On B.SiteCode=C.SiteCode ");
            SQL.AppendLine(" Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                         

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 

                         //6-10
                         "DocDt", 
                         "StartDt",
                         "EmpCode",
                         "EmpName",
                         "SiteName",

                         //11-15
                         "Gender",
                         "ResignDt",
                         "PosName",
                         "Yr",
                         "Mth"
                        
                       
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PPs()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            StartDt = Sm.DrStr(dr, c[7]),
                            EmpCode = Sm.DrStr(dr, c[8]),
                            EmpName = Sm.DrStr(dr, c[9]),
                            SiteName = Sm.DrStr(dr, c[10]),

                            Gender = Sm.DrStr(dr, c[11]),
                            ResignDt = Sm.DrStr(dr, c[12]),
                            PosName = Sm.DrStr(dr, c[13]),
                            Yr = Sm.DrStr(dr, c[14]),
                            Mth = Sm.DrStr(dr, c[15]),
                           
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Employee 2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.EmpCode, A.EmpName, B.PosName  ");
            SQL2.AppendLine("From tblemployee A ");
            SQL2.AppendLine("Left join tblposition B On A.PosCode=B.PosCode ");
            SQL2.AppendLine("where A.EmpCode=@EmpCode ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@EmpCode", mEmpCodePPs);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "EmpCode",
                         

                         //1-5
                         "EmpName",
                         "PosName",
                       
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new PPs2()
                        {
                            EmpCode = Sm.DrStr(dr2, c2[0]),

                            EmpName = Sm.DrStr(dr2, c2[1]),
                            PosName = Sm.DrStr(dr2, c2[2]),

                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            #endregion

            if (Sm.GetLue(LueType) == "D" || Sm.GetLue(LueType) == "P" || Sm.GetLue(LueType) == "R")
                Sm.PrintReport("PPs", myLists, TableName, false);
            if (Sm.GetLue(LueType) == "Q" || Sm.GetLue(LueType) == "T" )
                Sm.PrintReport("PPs2", myLists, TableName, false);

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PPS", "TblPPS");
            bool NeedApproval = IsDocApprovalSettingExisted();
            string EmpCode = TxtEmpCode.Text;
            var cml = new List<MySqlCommand>();

            cml.Add(SavePPS(DocNo, NeedApproval, mProposeCandidateDocNo, mProposeCandidateDNo));
            if (mProposeCandidateDocNo.Length > 0 && mProposeCandidateDNo.Length > 0)
                cml.Add(UpdateProposeCandidate(DocNo, mProposeCandidateDocNo, mProposeCandidateDNo));

            if (NeedApproval == false)
            {
                if (Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8)) <= Decimal.Parse(Sm.GetValue("Select concat(replace(curdate(), '-', ''))")))
                    cml.Add(UpdateEmployee(EmpCode));
                cml.Add(UpdateEmployee2(EmpCode));
            }
            if (mEmploymentStatusTetap.Length != 0 && mEmploymentStatusTetap == Sm.GetLue(LueEmploymentStatus) && mIsOldCodeAutoFillAfterPromotion)
                cml.Add(SetEmpCodeOld(EmpCode));
                
            Sm.ExecCommands(cml);

            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);

            ShowData(DocNo);
            if (mIsPPSAutoShowEmpSalary)
            {
                var f = new FrmEmpSalary(mMenuCode);
                f.Tag = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmEmpSalary' Limit 1;");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPPSEmpCode = EmpCode;
                f.ShowDialog();
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsLueEmpty(LueType, "Job Transfer") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false)||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsPPSUseLevel && Sm.IsLueEmpty(LueLevelCode, "Level")) ||
                //IsEmpStartDtAlreadyExist() ||
                IsStartDtNotValid() ||
                IsOutstandingEmpPPSExisted() ||
                IsInvalidADExisted() ||
                IsWorkPeriodeNotValid();
        }

        private bool IsWorkPeriodeNotValid()
        {
            if (!mIsWorkPeriodePPSMandatory) return false;
            if (TxtWorkPeriodYr.Text == "0" && TxtWorkPeriodMth.Text == "0")
            {
                Sm.StdMsg(mMsgType.Warning, "Work Period Is Empty.");
                return true;
            }
            return false;
        }

        private bool IsOutstandingEmpPPSExisted()
        {
            return Sm.IsDataExist(
                "Select 1 from TblPPS Where CancelInd='N' And Status='O' And EmpCode=@Param Limit 1;",
                TxtEmpCode.Text,
                "Outstanding PPS for this employee existed."
                );
        }

        private bool IsInvalidADExisted()
        {
            if (!mIsPPSAutoUpdEmpADBasedOnLevel) return false;
            if (Sm.IsDataExist(
                "Select 1 from TblEmployeeAllowanceDeduction Where EmpCode=@Param1 And StartDt Is Not Null And StartDt>=Left(@Param2, 8) Limit 1;",
                TxtEmpCode.Text,
                Sm.GetDte(DteStartDt),
                string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid allowance/deduction for this employee existed.");
                return true;
            }
            return false;
                
        }

        private bool IsStartDtNotValid()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select StartDt From TblEmployeePPS ");
            SQL.AppendLine("Where StartDt>=@StartDt ");
            SQL.AppendLine("And EmpCode=@EmpCode ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            var StartDt = Sm.GetValue(cm);
            if (StartDt.Length>0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Invalid start date. " +Environment.NewLine + 
                    "Start date should not be earlier than /equal as " + Sm.DateValue(StartDt) + ".");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePPS(string DocNo, bool NeedApproval, string ProposeCandidateDocNo, string ProposeCandidateDNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPPS(DocNo, DocDt, CancelInd, Status, Jobtransfer, StartDt, EmployeeRequestDocNo, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, ");
            SQL.AppendLine("DeptCodeOld, PosCodeOld, PositionStatusCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, PGCodeOld, SiteCodeOld, ResignDtOld, SectionCodeOld, LevelCodeOld, ");
            SQL.AppendLine("DeptCodeNew, PosCodeNew, PositionStatusCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, SiteCodeNew, ResignDtNew, SectionCodeNew, LevelCodeNew, UpdLeaveStartDtInd, Remark,");
            if(mActingOfficialIndSource == "2")
                SQL.AppendLine("ActingOfficialIndOld, ");
            SQL.AppendLine("ActingOfficialInd, WorkPeriodYr, WorkPeriodMth, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @Jobtransfer, @StartDt, @EmployeeRequestDocNo, @EmpCode, @ProposeCandidateDocNo, @ProposeCandidateDNo, ");
            SQL.AppendLine("@DeptCodeOld, @PosCodeOld, @PositionStatusCodeOld, @GrdLvlCodeOld, @EmploymentStatusOld, @SystemTypeOld, @PayrunPeriodOld, @PGCodeOld, @SiteCodeOld, @ResignDtOld, @SectionCodeOld, @LevelCodeOld, ");
            SQL.AppendLine("@DeptCodeNew, @PosCodeNew, @PositionStatusCodeNew, @GrdLvlCodeNew, @EmploymentStatusNew, @SystemTypeNew, @PayrunPeriodNew, @PGCodeNew, @SiteCodeNew, @ResignDtNew, @SectionCodeNew, @LevelCodeNew, @UpdLeaveStartDtInd, @Remark, ");
            if (mActingOfficialIndSource == "2")
                SQL.AppendLine("@ActingOfficialIndOld, ");
            SQL.AppendLine("@ActingOfficialInd, @WorkPeriodYr, @WorkPeriodMth, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PPS' ");
            if (mIsPPSApprovalBasedOnDept)
                SQL.AppendLine("And T.DeptCode In (Select DeptCodeNew From TblPPS Where DocNo=@DocNo) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPPS Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PPS' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            if (mIsAnnualLeaveUseStartDt && ChkUpdLeaveStartDtInd.Checked)
            {
                SQL.AppendLine("Update TblEmployee Set LeaveStartDt=@StartDt ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='PPS' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ); ");
            }

            //ini katanya dikeluarin dari yg butuh approval
            if (!Sm.CompareStr(Sm.GetDte(DteResignDtOld), Sm.GetDte(DteResignDtNew)))
            {
                SQL.AppendLine("Update TblEmployee Set ");
                SQL.AppendLine("    ResignDt=@ResignDtNew, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where EmpCode=@EmpCode; ");
            }

            if (NeedApproval==false) //kalau nggak butuh approval
            {
                SQL.AppendLine("Update TblEmployeePPS Set ");
                SQL.AppendLine("    EndDt=@EndDt ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And StartDt=( ");
                SQL.AppendLine("    Select StartDt From (");
                SQL.AppendLine("        Select Max(StartDt) StartDt ");
                SQL.AppendLine("        From TblEmployeePPS ");
                SQL.AppendLine("        Where EmpCode=@EmpCode ");
                SQL.AppendLine("        And StartDt<@StartDt ");
                SQL.AppendLine("    ) T );");

                SQL.AppendLine("Insert Into TblEmployeePPS(EmpCode, StartDt, EndDt, ");
                SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, SectionCode, LevelCode, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@EmpCode, @StartDt, Null, ");
                SQL.AppendLine("@DeptCodeNew, @PosCodeNew, @GrdLvlCodeNew, @EmploymentStatusNew, @SystemTypeNew, @PayrunPeriodNew, @PGCodeNew, @SiteCodeNew, @SectionCodeNew, @LevelCodeNew, ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
            }

            if (mIsPPSAutoUpdEmpADBasedOnLevel)
            {
                //untuk kalau dicancel, data allowance/deductionnya akan dibalikin seperti semula.
                SQL.AppendLine("Insert Into TblPPSAD(DocNo, EmpCode, DNo, ADCode, StartDt, EndDt, SiteCode, Amt, CreateBy, Createdt, LastUpBy, LastUpDt) ");
                SQL.AppendLine("Select B.DocNo, A.EmpCode, A.DNo, A.ADCode, A.StartDt, A.EndDt, A.SiteCode, A.Amt, A.CreateBy, A.Createdt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
                SQL.AppendLine("Inner Join TblPPS B  ");
                SQL.AppendLine("    On A.EmpCode=B.EmpCode  ");
                SQL.AppendLine("    And B.CancelInd='N'  ");
                SQL.AppendLine("    And B.Status='A'  ");
                SQL.AppendLine("    And B.DocNo=@DocNo ");
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 from TblParameter  ");
                SQL.AppendLine("    Where ParCode='IsPPSAutoUpdEmpADBasedOnLevel'  ");
                SQL.AppendLine("    And ParValue Is Not Null  ");
                SQL.AppendLine("    And ParValue='Y' ");
                SQL.AppendLine("    ); ");

                if(mPPSAutoUpdEmpADBasedOnLevelSource == "2")
                {
                    decimal mStartDt = Decimal.Parse(Sm.Left(Sm.GetDte(DteStartDt), 8));
                    decimal mCurrentDt = Decimal.Parse(Sm.Left(Sm.ServerCurrentDate(), 8));
                    bool IsFirst = true;
                    string ADCode = string.Empty;
                    var l = new List<AllowanceDeduction>();

                    if (mStartDt <= mCurrentDt)
                    {
                        //insert tblemployee allowance deduction
                        SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction ");
                        SQL.AppendLine("(EmpCode, DNo, ADCode, StartDt, EndDt, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Select EmpCode, DNo, ADCode, StartDt, EndDt, Amt, CreateBy, CreateDt ");
                        SQL.AppendLine("From ( ");
                        SQL.AppendLine("    Select A.EmpCode, Right(Concat('00', Cast((@DNoEAD:=@DNoEAD+1) As Char(3))), 3) As DNo, ");
                        SQL.AppendLine("    E.ADCode, A.StartDt, IfNull(G.ParValue, '20991231') As EndDt, E.Amt, A.CreateBy, A.CreateDt ");
                        SQL.AppendLine("    From TblPPS A ");
                        SQL.AppendLine("    INNER JOIN tblsite B ON A.SiteCodeNew = B.SiteCode ");
                        SQL.AppendLine("    INNER JOIN tblcity C ON B.CityCode = C.CityCode ");
                        SQL.AppendLine("    INNER JOIN tblposition D ON A.PosCodeNew = D.PosCode ");
                        SQL.AppendLine("    Inner Join TblLevelDtl2 E On A.LevelCodeNew=E.LevelCode ");
                        SQL.AppendLine("        AND C.LocationCode = E.LocationCode ");
                        SQL.AppendLine("        AND A.JobTransfer IN ('M', 'P', 'D') ");
                        SQL.AppendLine("        AND( ");
                        SQL.AppendLine("            if (E.GrdLvlCode IS NULL, true, If(A.GrdLvlCodeNew = E.GrdLvlCode, TRUE, FALSE)) ");
                        SQL.AppendLine("            AND if (E.PosCode IS NULL, true, if (A.PosCodeNew = E.PosCode, TRUE, FALSE)) ");
                        SQL.AppendLine("            AND if (E.WorkingCode IS NULL, true, if (D.TeritoryCode = E.WorkingCode, TRUE, FALSE)) ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine("    Inner Join ( ");
                        SQL.AppendLine("        Select @DNoEAD:=Cast(IfNull(DNo, 0) As Unsigned) ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("           Select Max(DNo) As DNo ");
                        SQL.AppendLine("           From TblPPS T1 ");
                        SQL.AppendLine("           Inner Join TblEmployeeAllowanceDeduction T2 On T1.EmpCode=T2.EmpCode ");
                        SQL.AppendLine("           Where T1.DocNo=@DocNo ");
                        SQL.AppendLine("        ) T ");
                        SQL.AppendLine("    ) F On 1=1 ");
                        SQL.AppendLine("    INNER JOIN TblParameter G On G.ParCode='ADEndDt' And G.ParValue Is Not NULL ");
                        SQL.AppendLine("    INNER JOIN tblparameter H ON H.parcode = 'IsPPSAutoUpdEmpADBasedOnLevel' AND H.Parvalue = 'Y' ");
                        SQL.AppendLine("    INNER JOIN tblparameter I ON I.parcode = 'PPSAutoUpdEmpADBasedOnLevelSource' AND I.parvalue = '2' ");
                        SQL.AppendLine("    WHERE A.EmpCode = @EmpCode ");
                        SQL.AppendLine("    And A.DocNo = @DocNo ");
                        SQL.AppendLine("    AND A.CancelInd='N' ");
                        SQL.AppendLine("    AND STATUS = 'A' ");
                        SQL.AppendLine("    AND NOT EXISTS (SELECT 1 FROM tblemployeeallowancededuction WHERE empcode = A.EmpCode AND AdCode = E.AdCode AND Amt = E.Amt And EndDt > IfNull(G.ParValue, '20991231')) ");
                        SQL.AppendLine("    AND (A.LevelCodeOld <> A.LevelCodeNew ");
                        SQL.AppendLine("        OR A.SiteCodeOld <> A.SiteCodeNew ");
                        SQL.AppendLine("        OR A.GrdLvlCodeOld <> A.GrdLvlCodeNew ");
                        SQL.AppendLine("        OR A.PosCodeOld <> A.PosCodeNew) ");

                        SQL.AppendLine("    Union ALL ");

                        SQL.AppendLine("    Select A.EmpCode, IfNull(Right(Concat('00', Cast((@DNoEAD:=@DNoEAD+1) As Char(3))), 3), C.DNo+1) As DNo, ");
                        SQL.AppendLine("    Ifnull(F.ParValue, 'AL025') ADCode, A.StartDt, IfNull(E.ParValue, '20991231') As EndDt, B.Amt, A.CreateBy, A.CreateDt ");
                        SQL.AppendLine("    From TblPPS A ");
                        SQL.AppendLine("    Inner Join TblLevelDtl3 B On A.LevelCodeNew = B.LevelCode And B.DNo = '001' ");
                        SQL.AppendLine("    Inner JOIN ( ");
                        SQL.AppendLine("       SELECT ");
                        SQL.AppendLine("       EmpCode, MAX(DNo) AS DNo ");
                        SQL.AppendLine("       FROM tblemployeeallowancededuction ");
                        SQL.AppendLine("       Group by EmpCode ");
                        SQL.AppendLine("    ) C ON A.EmpCode = C.EmpCode ");
                        SQL.AppendLine("    Inner Join TblParameter D On D.ParCode = 'SMKStatisAmtSource' And D.ParValue = '2' ");
                        SQL.AppendLine("    INNER JOIN TblParameter E On E.ParCode='ADEndDt' And E.ParValue Is Not NULL ");
                        SQL.AppendLine("    Left Join TblParameter F On F.ParCode = 'ADCodeSMKStatis' And F.ParValue Is Not Null ");
                        SQL.AppendLine("    INNER JOIN tblparameter G ON G.parcode = 'IsPPSAutoUpdEmpADBasedOnLevel' AND G.Parvalue = 'Y' ");
                        SQL.AppendLine("    INNER JOIN tblparameter H ON H.parcode = 'PPSAutoUpdEmpADBasedOnLevelSource' AND H.parvalue = '2' ");
                        SQL.AppendLine("    Where A.EmpCode = @EmpCode ");
                        SQL.AppendLine("    And A.DocNo = @DocNo ");
                        SQL.AppendLine("    And A.CancelInd='N' ");
                        SQL.AppendLine("    AND STATUS = 'A' ");
                        SQL.AppendLine("    AND NOT EXISTS (SELECT 1 FROM tblemployeeallowancededuction WHERE empcode = A.EmpCode AND AdCode = IfNull(F.ParValue, 'AL025') AND Amt = B.Amt And EndDt > IfNull(G.ParValue, '20991231')) ");
                        SQL.AppendLine("    AND (A.LevelCodeOld <> A.LevelCodeNew ");
                        SQL.AppendLine("       OR A.SiteCodeOld <> A.SiteCodeNew ");
                        SQL.AppendLine("       OR A.GrdLvlCodeOld <> A.GrdLvlCodeNew ");
                        SQL.AppendLine("       OR A.PosCodeOld <> A.PosCodeNew) ");
                        SQL.AppendLine(") Tbl; ");

                        //update endate data lama, jika ada perubahan amt 
                        SQL.AppendLine("Update TblEmployeeAllowanceDeduction X1 ");
                        SQL.AppendLine("Left join ( ");
                        SQL.AppendLine("    SELECT EmpCode, AdCode, StartDt, CreateBy, CreateDt ");
                        SQL.AppendLine("    FROM tblemployeeallowancededuction ");
                        SQL.AppendLine("    WHERE startdt = @StartDt ");
                        SQL.AppendLine(")X2 On X1.EmpCode = X2.EmpCode And X1.AdCode = X2.AdCode ");
                        SQL.AppendLine("Left Join TblParameter X7 On X7.ParCode = 'ADCodeSMKStatis' And X7.ParValue Is Not Null ");
                        SQL.AppendLine("Inner Join TblPPS X3 On X1.EmpCode = X3.EmpCode ");
                        SQL.AppendLine("    And X3.DocNo = @DocNo ");
                        SQL.AppendLine("    And X3.CancelInd = 'N' ");
                        SQL.AppendLine("    And X3.Status = 'A' ");
                        //SQL.AppendLine("    And X3.JobTransfer IN ('M', 'P', 'D') ");
                        SQL.AppendLine("    AND (If(X1.ADCode = IfNull(X7.Parvalue, 'AL025'), TRUE, If(X3.JobTransfer IN ('M', 'P', 'D'), TRUE, False))) ");
                        SQL.AppendLine("    AND (X3.LevelCodeOld <> X3.LevelCodeNew ");
                        SQL.AppendLine("        OR X3.SiteCodeOld <> X3.SiteCodeNew ");
                        SQL.AppendLine("        OR X3.GrdLvlCodeOld <> X3.GrdLvlCodeNew ");
                        SQL.AppendLine("        OR X3.PosCodeOld <> X3.PosCodeNew) ");
                        SQL.AppendLine("INNER JOIN TblParameter X4 On X4.ParCode='ADEndDt' And X4.ParValue Is Not NULL ");
                        SQL.AppendLine("INNER JOIN tblparameter X5 ON X5.parcode = 'IsPPSAutoUpdEmpADBasedOnLevel' AND X5.Parvalue = 'Y' ");
                        SQL.AppendLine("INNER JOIN tblparameter X6 ON X6.parcode = 'PPSAutoUpdEmpADBasedOnLevelSource' AND X6.parvalue = '2' ");
                        SQL.AppendLine("SET ");
                        SQL.AppendLine("    X1.EndDt = Date_Format(Date_Add(Str_To_Date(@StartDt, '%Y%m%d'), Interval -1 Day), '%Y%m%d'), ");
                        SQL.AppendLine("    X1.LastUpBy=@CreateBy, ");
                        SQL.AppendLine("    X1.LastUpDt= CurrentDateTime() ");
                        SQL.AppendLine("Where X1.EndDt > @StartDt ");
                        SQL.AppendLine("And ( ");
                        SQL.AppendLine("    (X2.AdCode Is Not Null And X1.startdt < @StartDt) ");
                        SQL.AppendLine("    Or ( ");
                        SQL.AppendLine("        X2.AdCode Is Null ");
                        SQL.AppendLine("        And Not Exists( ");
                        SQL.AppendLine("            Select 1 ");
                        SQL.AppendLine("            From TblLevelDtl2 A ");
                        SQL.AppendLine("            INNER JOIN TblParameter B On B.ParCode='ADEndDt' And B.ParValue Is Not NULL ");
                        SQL.AppendLine("            INNER JOIN tblparameter C ON C.parcode = 'IsPPSAutoUpdEmpADBasedOnLevel' AND C.Parvalue = 'Y' ");
                        SQL.AppendLine("            INNER JOIN tblparameter D ON D.parcode = 'PPSAutoUpdEmpADBasedOnLevelSource' AND D.parvalue = '2' ");
                        SQL.AppendLine("            Inner Join (  ");
                        SQL.AppendLine("                Select @DNoEAD:=Cast(IfNull(DNo, 0) As Unsigned)  ");
                        SQL.AppendLine("                From (  ");
                        SQL.AppendLine("                    Select Max(DNo) As DNo  ");
                        SQL.AppendLine("                    From TblEmployeeAllowanceDeduction ");
                        SQL.AppendLine("                    Where EmpCode = @EmpCode ");
                        SQL.AppendLine("                ) T ");
                        SQL.AppendLine("            )E On 1=1  ");
                        SQL.AppendLine("            Inner Join TblEmployee F On F.EmpCode = @EmpCode ");
                        SQL.AppendLine("            Where A.LevelCode = @LevelCodeNew  ");
                        SQL.AppendLine("                And A.ADCode = X1.ADCode ");
                        SQL.AppendLine("            	And A.LocationCode = @LocationCode  ");
                        SQL.AppendLine("            	And @JobTransfer in ('M', 'P', 'D') ");
                        SQL.AppendLine("                AND ( ");
                        SQL.AppendLine("                   if(A.GrdLvlCode IS NULL, 0 = 0, @GrdLvlCodeNew = A.GrdLvlCode)  ");
                        SQL.AppendLine("                   AND if (A.PosCode IS NULL, 0 = 0, @PosCodeNew = A.PosCode) ");
                        SQL.AppendLine("                   AND if (A.WorkingCode IS NULL, 0 = 0, @TeritoryCode = A.WorkingCode) ");
                        SQL.AppendLine("                ) ");
                        //SQL.AppendLine("                AND NOT EXISTS (SELECT 1 FROM tblemployeeallowancededuction WHERE empcode = @EmpCode AND AdCode = A.ADCode AND Amt = A.Amt) ");
                        SQL.AppendLine("                AND (F.LevelCode <> @LevelCodeNew ");
                        SQL.AppendLine("                    OR F.SiteCode <> @SiteCodeNew ");
                        SQL.AppendLine("                    OR F.GrdLvlCode <> @GrdLvlCodeNew  ");
                        SQL.AppendLine("                    OR F.PosCode <> @PosCodeNew) ");
                        SQL.AppendLine("        )");
                        SQL.AppendLine("        And Exists( ");
                        SQL.AppendLine("            Select 1 ");
                        SQL.AppendLine("            From TblLevelDtl2 ");
                        SQL.AppendLine("            Where AdCode = X1.AdCode ");
                        SQL.AppendLine("            Limit 1 ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine("); ");

                        #region Old
                        //SQL.AppendLine("Update TblEmployeeAllowancededuction A ");
                        //SQL.AppendLine("INNER JOIN ( ");
                        //SQL.AppendLine("    SELECT EmpCode, AdCode, StartDt, CreateBy, CreateDt ");
                        //SQL.AppendLine("    FROM tblemployeeallowancededuction ");
                        //SQL.AppendLine("    WHERE startdt = @StartDt ");
                        //SQL.AppendLine(")B ON A.EmpCode = B.EmpCode AND A.AdCode = B.AdCode ");
                        //SQL.AppendLine("SET ");
                        //SQL.AppendLine("    A.EndDt = Date_Format(Date_Add(Str_To_Date(B.StartDt, '%Y%m%d'), Interval -1 Day), '%Y%m%d'), ");
                        //SQL.AppendLine("    A.LastUpBy=B.CreateBy, ");
                        //SQL.AppendLine("    A.LastUpDt=B.Createdt ");
                        //SQL.AppendLine("WHERE A.StartDt Is Not Null And A.StartDt < @StartDt; ");
                        #endregion
                    }
                }
                else 
                {
                    SQL.AppendLine("Update TblEmployeeAllowanceDeduction A ");
                    SQL.AppendLine("Inner Join TblPPS B  ");
                    SQL.AppendLine("    On A.EmpCode=B.EmpCode  ");
                    SQL.AppendLine("    And A.StartDt<B.StartDt ");
                    SQL.AppendLine("    And A.EndDt>=B.StartDt ");
                    SQL.AppendLine("    And B.CancelInd='N'  ");
                    SQL.AppendLine("    And B.Status='A'  ");
                    SQL.AppendLine("    And B.DocNo=@DocNo ");
                    SQL.AppendLine("Set  ");
                    SQL.AppendLine("    A.EndDt=Date_Format(Date_Add(Str_To_Date(B.StartDt, '%Y%m%d'), Interval -1 Day), '%Y%m%d'), ");
                    SQL.AppendLine("    A.LastUpBy=B.CreateBy, ");
                    SQL.AppendLine("    A.LastUpDt=B.Createdt ");
                    SQL.AppendLine("Where A.StartDt Is Not Null ");
                    SQL.AppendLine("And A.EndDt Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 from TblParameter  ");
                    SQL.AppendLine("    Where ParCode='IsPPSAutoUpdEmpADBasedOnLevel'  ");
                    SQL.AppendLine("    And ParValue Is Not Null  ");
                    SQL.AppendLine("    And ParValue='Y' ");
                    SQL.AppendLine("    ); ");

                    SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction ");
                    SQL.AppendLine("(EmpCode, DNo, ADCode, StartDt, EndDt, Amt, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.EmpCode,  ");
                    SQL.AppendLine("Right(Concat('00', Cast((@DNoEAD:=@DNoEAD+1) As Char(3))), 3) As DNo, ");
                    SQL.AppendLine("B.ADCode, A.StartDt, IfNull(D.ParValue, '20991231') As EndDt, B.Amt, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblPPS A ");
                    SQL.AppendLine("Inner Join TblLevelDtl B On A.LevelCodeNew=B.LevelCode ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select @DNoEAD:=Cast(IfNull(DNo, 0) As Unsigned)  ");
                    SQL.AppendLine("    From (  ");
                    SQL.AppendLine("        Select Max(DNo) As DNo ");
                    SQL.AppendLine("        From TblPPS T1  ");
                    SQL.AppendLine("        Inner Join TblEmployeeAllowanceDeduction T2 On T1.EmpCode=T2.EmpCode ");
                    SQL.AppendLine("        Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine(") C On 1=1 ");
                    SQL.AppendLine("Left join TblParameter D On D.ParCode='ADEndDt' And D.ParValue Is Not Null ");
                    SQL.AppendLine("Where A.CancelInd='N' ");
                    SQL.AppendLine("And A.Status='A' ");
                    SQL.AppendLine("And IfNull(A.LevelCodeOld, '')<>IfNull(A.LevelCodeNew, '') ");
                    SQL.AppendLine("And A.LevelCodeNew Is Not Null ");
                    SQL.AppendLine("And A.DocNo=@DocNo ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 from TblParameter  ");
                    SQL.AppendLine("    Where ParCode='IsPPSAutoUpdEmpADBasedOnLevel'  ");
                    SQL.AppendLine("    And ParValue Is Not Null  ");
                    SQL.AppendLine("    And ParValue='Y' ");
                    SQL.AppendLine("    ); ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@StartEmpCode", Sm.GetValue("Select Concat(StartDt,EmpCode) From TblEmployeepps Where EmpCode = '" + TxtEmpCode.Text + "' And "+
                "CreateDt in (Select max(CreateDt) From TblEmployeepps Where EmpCode = '" + TxtEmpCode.Text + "' )"));
            
            string CurrentDate = Sm.GetDte(DteStartDt);
            DateTime DateMin = Sm.ConvertDate(CurrentDate).AddDays(-1);
            Sm.CmParam<String>(ref cm, "@EndDt", DateMin.ToString("yyyyMMdd"));

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDocNo", ProposeCandidateDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDNo", ProposeCandidateDNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@JobTransfer", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DeptCodeOld", DeptCodeOld);
            Sm.CmParam<String>(ref cm, "@PosCodeOld", PosCodeOld);
            Sm.CmParam<String>(ref cm, "@PositionStatusCodeOld", mPositionStatusCodeOld);
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeOld", GrdLvlCodeOld);
            Sm.CmParam<String>(ref cm, "@EmploymentStatusOld", EmploymentStatusOld); 
            Sm.CmParam<String>(ref cm, "@SystemTypeOld", SystemTypeOld);
            Sm.CmParam<String>(ref cm, "@PayrunPeriodOld", PayrunPeriodOld);
            Sm.CmParam<String>(ref cm, "@PGCodeOld", PGCodeOld);
            Sm.CmParam<String>(ref cm, "@SiteCodeOld", SiteCodeOld);
            Sm.CmParam<String>(ref cm, "@DeptCodeNew", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCodeNew", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PositionStatusCodeNew", Sm.GetLue(LuePositionStatusCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeNew", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@EmploymentStatusNew", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@SystemTypeNew", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriodNew", Sm.GetLue(LuePayrunPeriod));
            Sm.CmParam<String>(ref cm, "@PGCodeNew", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@SiteCodeNew", Sm.GetLue(LueSiteCode));
            Sm.CmParamDt(ref cm, "@ResignDtOld", Sm.GetDte(DteResignDtOld));
            Sm.CmParamDt(ref cm, "@ResignDtNew", Sm.GetDte(DteResignDtNew));
            Sm.CmParam<String>(ref cm, "@UpdLeaveStartDtInd", ChkUpdLeaveStartDtInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@EmployeeRequestDocNo", TxtEmployeeRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SectionCodeOld", mSectionCodeOld);
            Sm.CmParam<String>(ref cm, "@SectionCodeNew", Sm.GetLue(LueSectionCode));
            Sm.CmParam<String>(ref cm, "@LevelCodeOld", mLevelCodeOld);
            Sm.CmParam<String>(ref cm, "@LevelCodeNew", Sm.GetLue(LueLevelCode));
            Sm.CmParam<decimal>(ref cm, "@WorkPeriodYr", decimal.Parse(TxtWorkPeriodYr.Text));
            Sm.CmParam<decimal>(ref cm, "@WorkPeriodMth", decimal.Parse(TxtWorkPeriodMth.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ActingOfficialInd", ChkActingOfficialNew.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActingOfficialIndOld", ChkActingOfficialOld.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateProposeCandidate(string PPSDocNo, string ProposeCandidateDocNo, string ProposeCandidateDNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Update TblProposeCandidateDtl2 ");
            SQL.AppendLine("Set PPSDocNo = @PPSDocNo, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @ProposeCandidateDocNo And DNo = @ProposeCandidateDNo ");
            SQL.AppendLine("And Exists ( ");
            SQL.AppendLine("  Select A.DocNo ");
            SQL.AppendLine("  From TblPPS A ");
            SQL.AppendLine("  Where A.DocNo = @PPSDocNo ");
            SQL.AppendLine("  And A.Status = 'A' ");
            SQL.AppendLine("); ");
            
            SQL.AppendLine("Update TblProposeCandidateHdr ");
            SQL.AppendLine("Set ProcessInd = 'F', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @ProposeCandidateDocNo ");
            SQL.AppendLine("And Exists ( ");
            SQL.AppendLine("  Select A.DocNo ");
            SQL.AppendLine("  From TblPPS A ");
            SQL.AppendLine("  Where A.DocNo = @PPSDocNo ");
            SQL.AppendLine("  And A.Status = 'A' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("  Select T.DocNo ");
            SQL.AppendLine("  From TblProposeCandidateDtl2 T ");
            SQL.AppendLine("  Where T.DocNo = @ProposeCandidateDocNo ");
            SQL.AppendLine("  And T.PPSDocNo Is Null ");
            SQL.AppendLine("); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PPSDocNo", PPSDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDocNo", ProposeCandidateDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDNo", ProposeCandidateDNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateEmployee(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployee Set ");
            SQL.AppendLine("    DeptCode=@DeptCode, ");
            if (mIsPPSUpdateEmpDivBasedOnDept)
                SQL.AppendLine("    DivisionCode=(Select DivisionCode From TblDepartment Where DeptCode=@DeptCode), ");
            
            SQL.AppendLine("    PosCode=@PosCode, ");
            SQL.AppendLine("    GrdLvlCode=@GrdLvlCode, ");
            SQL.AppendLine("    EmploymentStatus=@EmploymentStatus, ");
            SQL.AppendLine("    SystemType=@SystemType, ");
            SQL.AppendLine("    PayrunPeriod=@PayrunPeriod, ");
            SQL.AppendLine("    PGCode=@PGCode, ");
            SQL.AppendLine("    SiteCode=@SiteCode, ");
            if (mIsPPSUseSection)
                SQL.AppendLine("    SectionCode = @SectionCode, ");
            if (mIsPPSUseLevel)
                SQL.AppendLine("    LevelCode = @LevelCode, ");
            if (mIsPltCheckboxNotBasedOnPosition)
                SQL.AppendLine("    ActingOfficialInd=@ActingOfficialInd, ");
            SQL.AppendLine("    EntCode=( ");
            SQL.AppendLine("        Select T2.EntCode ");
            SQL.AppendLine("        From TblSite T1 ");
            SQL.AppendLine("        Inner Join TblProfitCenter T2 On T1.ProfitCenterCode=T2.ProfitCenterCode ");
            SQL.AppendLine("        Where T1.SiteCode=@SiteCode ");
            SQL.AppendLine("    ), ");

            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode; ");

            SQL.AppendLine("Update TblEmployeePPS P Set P.ProcessInd = 'Y' ");
            SQL.AppendLine("Where P.EmpCode = @EmpCode And P.ProcessInd = 'N' ");
            SQL.AppendLine("And P.EndDt Is Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetLue(LuePositionStatusCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSectionCode));
            if (mIsPPSUseLevel) Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevelCode));
            Sm.CmParamDt(ref cm, "@ResignDtNew", Sm.GetDte(DteResignDtNew));
            //
            Sm.CmParam<String>(ref cm, "@ActingOfficialInd", ChkActingOfficialNew.Checked ? "Y" : "N");
            //
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmployee2(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployee Set ");
            SQL.AppendLine("    PositionStatusCode=@PositionStatusCode, ");
            if (mIsPltCheckboxNotBasedOnPosition)
                SQL.AppendLine("    ActingOfficialInd=@ActingOfficialInd, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            //
            Sm.CmParam<String>(ref cm, "@ActingOfficialInd", ChkActingOfficialNew.Checked ? "Y" : "N");
            //
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", Sm.GetLue(LuePositionStatusCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SetEmpCodeOld( string EmpCode)
        {
            //Customized for PHT

                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblEmployee Set ");
                SQL.AppendLine("    EmpCodeOld=@EmpCodeOld, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where EmpCode=@EmpCode; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                string BirthDt = string.Empty,
                       Date = string.Empty,
                       Gender = string.Empty,
                       Text = string.Empty,
                       EmpCodeOld = string.Empty;

                BirthDt = Sm.GetValue("Select ifnull(BirthDt, '00000000') From TblEmployee where EmpCode = @Param;", TxtEmpCode.Text);
                Date = Sm.Left(Sm.GetDte(DteStartDt), 6);
                Text = "PHT";
                Gender = Sm.GetValue(
                        "SELECT case  when gender ='M' then '1' " +
                        "when gender ='F' then '2' ELSE '0' " +
                        "END AS Gender " +
                        "FROM tblemployee where EmpCode = @Param;", TxtEmpCode.Text);

                EmpCodeOld = Sm.GetValue(
                        "Select Concat('" + Text + "','" + BirthDt + "','" + Date + "','" + Gender + "', " +
                        "   IfNull((Select Right(Concat('0', Convert(EmpCodeTemp+1, CHAR)), 2) From ( " +
                        "   Select Convert(Right(EmpCodeOld, 2), Decimal) As EmpCodeTemp " +
                        "   From TblEmployee Where Left(EmpCodeOld, 18)=CONCAT('" + Text + "','" + BirthDt + "','" + Date + "','" + Gender + "')" +
                        "   Order By Right(EmpCodeOld, 2) Desc Limit 1" +
                        "   ) As TblEmployeeTemp), '01')) As EmpCodeOld");

                Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                Sm.CmParam<String>(ref cm, "@EmpCodeOld", EmpCodeOld);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                return cm;
           
        }
        
        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            if(mIsWorkPeriodePPSEditable)
                cml.Add(SavePPSDtl());
            cml.Add(EditPPS());

            if (mProposeCandidateDocNo.Length > 0 && mProposeCandidateDNo.Length > 0)
                cml.Add(EditProposeCandidate(mProposeCandidateDocNo, mProposeCandidateDNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
            if (mIsPPSAutoShowEmpSalary)
            {
                var f = new FrmEmpSalary(mMenuCode);
                f.Tag = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmEmpSalary' Limit 1;");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPPSEmpCode = TxtEmpCode.Text;
                f.ShowDialog();
            }
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataAlreadyCancelled()||
                IsSettingHasEndDt()
                ;
        }

        private bool IsSettingHasEndDt()
        {   
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Select EmpCode From TblEmployeePPS " +
                    "Where EmpCode=@EmpCode And StartDt=@StartDt And EndDt Is Not Null;"
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "You can't cancel this document." + Environment.NewLine +
                    "Employee's Setting already has end date.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblPPS Where DocNo=@Param And CancelInd='Y';", 
                TxtDocNo.Text, 
                "This document already cancelled.");
        }

        private MySqlCommand SavePPSDtl()
        {

            var Row = Sm.GetValue("SELECT IfNull(Max(DNo),0)+1 From  tblppsdtl Where DocNo='" + TxtDocNo.Text + "'");
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPPSDtl");
            SQL.AppendLine("(DocNo, DNo, WorkPeriodYr, WorkPeriodMth, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select DocNo, @DNo, WorkPeriodYr, WorkPeriodMth, Remark, CreateBy, CreateDt, @LastUpBy, CurrentDateTime() ");
            SQL.AppendLine("From TblPPS Where DocNo=@DocNo ;");
                    
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + Row, 3));
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditPPS()
        {
            var SQL = new StringBuilder();
            if (mIsWorkPeriodePPSEditable)
            {
                SQL.AppendLine("Update TblPPS Set WorkPeriodYr=@WorkPeriodYr, WorkPeriodMth=@WorkPeriodMth, Remark=@Remark, ProposeCandidateDocNo = null, ProposeCandidateDNo = null, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            }
            else
            {
                SQL.AppendLine("Update TblPPS Set CancelInd='Y', ProposeCandidateDocNo = null, ProposeCandidateDNo = null, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            }
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N';");

            SQL.AppendLine("Update TblEmployee A ");
            SQL.AppendLine("Inner Join TblPPS B On A.EmpCode=B.EmpCode And B.DocNo=@DocNo ");
            SQL.AppendLine("Set A.PositionStatusCode=B.PositionStatusCodeOld, ");
            SQL.AppendLine("A.ActingOfficialInd=B.ActingOfficialIndOld; ");

            SQL.AppendLine("Update TblEmployee A ");
            SQL.AppendLine("Inner Join TblEmployeePPS B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And StartDt In ( ");
            SQL.AppendLine("        Select Max(StartDt) StartDt ");
            SQL.AppendLine("        From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=@EmpCode ");
            SQL.AppendLine("        And StartDt<@StartDt ");
            SQL.AppendLine("    ) ");
            if (mIsPPSUpdateEmpDivBasedOnDept)
                SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DeptCode=B.DeptCode, ");
            if (mIsPPSUpdateEmpDivBasedOnDept)
                SQL.AppendLine("    A.DivisionCode=C.DivisionCode, ");
            SQL.AppendLine("    A.PosCode=B.PosCode, ");
            SQL.AppendLine("    A.GrdLvlCode=B.GrdLvlCode, ");
            SQL.AppendLine("    A.EmploymentStatus=B.EmploymentStatus, ");
            SQL.AppendLine("    A.SystemType=B.SystemType, ");
            SQL.AppendLine("    A.PayrunPeriod=B.PayrunPeriod, ");
            SQL.AppendLine("    A.PGCode=B.PGCode, ");
            SQL.AppendLine("    A.SiteCode=B.SiteCode, ");
            if (mIsPPSUseSection)
                SQL.AppendLine("    A.SectionCode = B.SectionCode, ");
            if (mIsPPSUseLevel)
                SQL.AppendLine("A.LevelCode=B.LevelCode, ");
            SQL.AppendLine("    A.EntCode=( ");
            SQL.AppendLine("        Select T2.EntCode ");
            SQL.AppendLine("        From TblSite T1 ");
            SQL.AppendLine("        Inner Join TblProfitCenter T2 On T1.ProfitCenterCode=T2.ProfitCenterCode ");
            SQL.AppendLine("        Where T1.SiteCode=B.SiteCode ");
            SQL.AppendLine("    ), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            if (!Sm.CompareStr(Sm.GetDte(DteResignDtOld), Sm.GetDte(DteResignDtNew)) && Sm.GetDte(DteResignDtNew).Length>0)
                SQL.AppendLine(", A.ResignDt=null ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode; ");

            SQL.AppendLine("Delete From TblEmployeePPS Where StartDt=@StartDt And EmpCode=@EmpCode;");

            SQL.AppendLine("Update TblEmployeePPS Set ");
            SQL.AppendLine("    EndDt=Null, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And StartDt In ( ");
            SQL.AppendLine("    Select StartDt From (");
            SQL.AppendLine("        Select Max(StartDt) StartDt ");
            SQL.AppendLine("        From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=@EmpCode ");
            SQL.AppendLine("        And StartDt<@StartDt ");
            SQL.AppendLine("    ) T );");

            if (mIsPPSAutoUpdEmpADBasedOnLevel)
            {
                SQL.AppendLine("Delete A From TblEmployeeAllowanceDeduction A ");
                SQL.AppendLine("Inner Join TblPPS B  ");
                SQL.AppendLine("    On A.EmpCode=B.EmpCode  ");
                SQL.AppendLine("    And B.CancelInd='Y'  ");
                SQL.AppendLine("    And B.DocNo=@DocNo ");
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 from TblParameter  ");
                SQL.AppendLine("    Where ParCode='IsPPSAutoUpdEmpADBasedOnLevel'  ");
                SQL.AppendLine("    And ParValue Is Not Null  ");
                SQL.AppendLine("    And ParValue='Y' ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 from TblPPSAD ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction(EmpCode, DNo, ADCode, StartDt, EndDt, SiteCode, Amt, CreateBy, Createdt, LastUpBy, LastUpDt) ");
                SQL.AppendLine("Select A.EmpCode, A.DNo, A.ADCode, A.StartDt, A.EndDt, A.SiteCode, A.Amt, A.CreateBy, A.Createdt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblPPSAD A ");
                SQL.AppendLine("Inner Join TblPPS B  ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='Y'  ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 from TblParameter  ");
                SQL.AppendLine("    Where ParCode='IsPPSAutoUpdEmpADBasedOnLevel'  ");
                SQL.AppendLine("    And ParValue Is Not Null  ");
                SQL.AppendLine("    And ParValue='Y' ");
                SQL.AppendLine("    ); ");
            }


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@WorkPeriodYr", TxtWorkPeriodYr.Text);
            Sm.CmParam<String>(ref cm, "@WorkPeriodMth", TxtWorkPeriodMth.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditProposeCandidate(string ProposeCandidateDocNo, string ProposeCandidateDNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProposeCandidateDtl2 Set PPSDocNo = null, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@ProposeCandidateDocNo And DNo = @ProposeCandidateDNo; ");

            SQL.AppendLine("Update TblProposeCandidateHdr ");
            SQL.AppendLine("Set ProcessInd = 'O', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @ProposeCandidateDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ProposeCandidateDocNo", ProposeCandidateDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDNo", ProposeCandidateDNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPPS(DocNo);
                ChkActingOfficialNew.Properties.ReadOnly = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPPS(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.StartDt, A.EmpCode, B.EmpName, A.JobTransfer, A.ProposeCandidateDocNo, A.ProposeCandidateDNo, ");
            SQL.AppendLine("C.DeptName As Deptnameold, D.Posname As PosNameOld, E.GrdLvlname As GrdLvlNameOld, ");
            SQL.AppendLine("F.OptDesc As EmpStatOld, G.OptDesc As EmpSysTypeOld, H.OptDesc As EmpPaYperOld, I.PGName, J.SiteName, ");
            SQL.AppendLine("A.DeptCodeNew, A.PosCodeNew, A.GrdLvlCodeNew, A.EmploymentStatusNew, A.SystemTypeNew, A.PayrunPeriodNew, A.PGCodeNew, A.SiteCodeNew, ");
            SQL.AppendLine("M.PositionStatusName, A.PositionStatusCodeNew, A.ResignDtOld, A.ResignDtNew, L.EntName, ");
            SQL.AppendLine("A.UpdLeaveStartDtInd, A.EmployeeRequestDocNo, A.Remark,  ");
            if (mActingOfficialIndSource == "2")
                SQL.AppendLine("A.ActingOfficialIndOld,");
            else
                SQL.AppendLine("D.ActingOfficialInd As ActingOfficialIndOld,");
            SQL.AppendLine("N.SectionName As SectionCodeOld, A.SectionCodeNew, O.LevelName As LevelCodeOld, A.LevelCodeNew, ");
            SQL.AppendLine("A.WorkPeriodYr, A.WorkPeriodMth, A.FileName, A.FileName2, A.FileName3, A.ActingOfficialInd As ActingOfficialIndNew ");
            SQL.AppendLine("From TblPPS A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join tblDepartment C On A.DeptCodeOld = C.deptCode");
            SQL.AppendLine("Left Join TblPosition D On A.PosCodeOld = D.PosCode");
            SQL.AppendLine("left Join TblgradeLevelhdr E On A.GrdlvlCodeOld = E.GrdlvlCode");
            SQL.AppendLine("left Join (");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'EmploymentStatus'");
            SQL.AppendLine(")F On A.EmploymentStatusOld = F.OptCode");
            SQL.AppendLine("Left join (");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'EmpSystemType'");
            SQL.AppendLine(")G On A.SystemTypeOld = G.OptCode");
            SQL.AppendLine("Left Join(");
            SQL.AppendLine("    Select OptCode, OptDesc From TblOption Where OptCat = 'PayrunPeriod'");
            SQL.AppendLine(")H On A.PayrunPeriodOld = H.OptCode");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On A.PGCodeOld = I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCodeOld = J.SiteCode");
            SQL.AppendLine("Left Join TblProfitCenter K On J.ProfitCenterCode = K.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity L On K.EntCode = L.EntCode ");
            SQL.AppendLine("Left Join TblPositionStatus M On A.PositionStatusCodeOld = M.PositionStatusCode ");
            SQL.AppendLine("Left Join TblSection N On A.SectionCodeOld=N.SectionCode ");
            SQL.AppendLine("Left Join TblLevelHdr O On A.LevelCodeOld=O.LevelCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "DocNo", 
                    //1-5
                    "DocDt", "CancelInd", "StartDt", "EmpCode", "EmpName", 
                    //6-10
                    "JobTransfer","Deptnameold", "PosNameOld", "GrdLvlNameOld", "EmpStatOld", 
                    //11-15
                    "EmpSysTypeOld", "EmpPaYperOld", "PGName", "DeptCodeNew", "PosCodeNew", 
                    //16-20
                    "GrdLvlCodeNew", "EmploymentStatusNew", "SystemTypeNew", "PayrunPeriodNew", "PGCodeNew",
                    //21-25
                    "SiteName", "SiteCodeNew", "ResignDtOld", "ResignDtNew", "ProposeCandidateDocNo",
                    //26-30
                    "ProposeCandidateDNo", "EntName", "UpdLeaveStartDtInd", "EmployeeRequestDocNo", "Remark",
                    //31-35
                    "PositionStatusName", "PositionStatusCodeNew", "ActingOfficialIndOld", "SectionCodeOld", "SectionCodeNew",
                    //36-40
                    "LevelCodeOld", "LevelCodeNew", "WorkPeriodYr", "WorkPeriodMth", "FileName",
                    //41-42
                    "FileName2", "FileName3", "ActingOfficialIndNew"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueType, Sm.DrStr(dr, c[6]));
                    TxtDeptName.EditValue = Sm.DrStr(dr, c[7]);
                    TxtPosition.EditValue = Sm.DrStr(dr, c[8]);
                    TxtGrade.EditValue = Sm.DrStr(dr, c[9]);
                    TxtEmploymentStatus.EditValue = Sm.DrStr(dr, c[10]);
                    TxtSystemType.EditValue = Sm.DrStr(dr, c[11]);
                    TxtPayrunPeriod.EditValue = Sm.DrStr(dr, c[12]);
                    TxtPGCode.EditValue = Sm.DrStr(dr, c[13]);
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[14]), string.Empty);
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[15]));
                    SetLueGrdLvlCode(ref LueGrdLvlCode, Sm.DrStr(dr, c[16]));
                    Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[16]));
                    Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[17]));
                    Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[18]));
                    Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[19]));
                    Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[20]));
                    TxtSiteCode.EditValue = Sm.DrStr(dr, c[21]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[22]), string.Empty);
                    Sm.SetDte(DteResignDtOld, Sm.DrStr(dr, c[23]));
                    Sm.SetDte(DteResignDtNew, Sm.DrStr(dr, c[24]));
                    mProposeCandidateDocNo = Sm.DrStr(dr, c[25]);
                    mProposeCandidateDNo = Sm.DrStr(dr, c[26]);
                    TxtEntityName.EditValue = Sm.DrStr(dr, c[27]);
                    ChkUpdLeaveStartDtInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[28]), "Y");
                    TxtEmployeeRequestDocNo.EditValue = Sm.DrStr(dr, c[29]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[30]);
                    TxtPositionStatusCode.EditValue = Sm.DrStr(dr, c[31]);
                    Sm.SetLue(LuePositionStatusCode, Sm.DrStr(dr, c[32]));
                    ChkActingOfficialOld.Checked = Sm.CompareStr(Sm.DrStr(dr, c[33]), "Y");
                    //
                    if (mIsPltCheckboxNotBasedOnPosition)
                    {
                        ChkActingOfficialNew.Checked = Sm.CompareStr(Sm.DrStr(dr, c[43]), "Y");
                    }
                    else
                    {
                        ChkActingOfficialNew.Checked = Sm.CompareStr(Sm.GetValue("Select ActingOfficialInd From TblPosition " +
                                                   "Where PosCode = @Param", Sm.DrStr(dr, c[15])), "Y");
                    }
                    //
                    TxtSectionCodeOld.EditValue = Sm.DrStr(dr, c[34]);
                    SetLueSection(ref LueSectionCode, Sm.DrStr(dr, c[35]));
                    Sm.SetLue(LueSectionCode, Sm.DrStr(dr, c[35]));
                    TxtLevelCode.EditValue = Sm.DrStr(dr, c[36]);
                    if (mIsPPSUseLevel) Sl.SetLueLevelCode(ref LueLevelCode, Sm.DrStr(dr, c[37]));
                    TxtWorkPeriodYr.EditValue = Sm.DrDec(dr, c[38]);
                    TxtWorkPeriodMth.EditValue = Sm.DrDec(dr, c[39]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[40]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[41]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[42]);
                }, true
        );

        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPPSDepartmentBasedOnSite', 'Apakah Department di PPS berdasarkan Site ? [Y = Ya, N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsLevelUsePositionStatus', 'Apakah master Level menggunakan Position Status ? [Y = Ya; N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPositionStatusMandatory', 'Apakah Position Status mandatory? [Y = Ya; N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPositionUseLevel', 'Apakah master Position menggunakan Level ? [Y = Ya, N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPositionLevelMandatory', 'Apakah tab Level di master Position mandatory? [Y = Ya, N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");

            SQL.AppendLine("ALTER TABLE `tbllevelhdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `PositionStatusCode` VARCHAR(16) NULL DEFAULT NULL AFTER `Remark`, ");
            SQL.AppendLine("    DROP INDEX IF EXISTS `LevelCode`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `LevelCode` (`LevelCode`, `LevelName`, `PositionStatusCode`) USING BTREE; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblpositionlevel` ( ");
            SQL.AppendLine("    `PosCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LevelCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(255) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`PosCode`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `PosCode` (`PosCode`, `DNo`, `LevelCode`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            Sm.ExecQuery(SQL.ToString());
        }

        internal void SetLueSection(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SectionCode As Col1, SectionName As Col2 ");
                SQL.AppendLine("From TblSection ");
                if (Code.Length == 0)
                    SQL.AppendLine("Where ActInd='Y' ");
                else
                    SQL.AppendLine("Where SectionCode = '" + Code + "' ");
                SQL.AppendLine("Order By SectionName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ( ");
            SQL.AppendLine("'IsPayrollDataFilterByAuthorization', 'IsFilterByDeptHR', 'IsFilterBySiteHR', 'IsFilterByLevelHR', 'IsPPSApprovalBasedOnDept', 'IsAnnualLeaveUseStartDt', 'IsPPSShowJobTransferByGrp',  ");
            SQL.AppendLine("'EmpCodePPs', 'IsPPSAutoShowEmpSalary', 'IsPPSUpdateEmpDivBasedOnDept', 'IsEmpContractDtMandatory', 'IsOldCodeAutoFillAfterPromotion', 'EmploymentStatusTetap', 'IsPPSUseSection',  ");
            SQL.AppendLine("'IsPPSUseLevel', 'IsPPSAutoUpdEmpADBasedOnLevel', 'IsPPSAllowToUploadFile', 'HostAddrForFTPClient', 'PortForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient',  ");
            SQL.AppendLine("'SharedFolderForFTPClient', 'FileSizeMaxUploadFTPClient', 'IsPltCheckboxNotBasedOnPosition', 'PPSAutoUpdEmpADBasedOnLevelSource', 'ActingOfficialIndSource',  ");
            SQL.AppendLine("'IsWorkPeriodePPSMandatory', 'IsWorkPeriodePPSEditable', 'IsPPSDepartmentBasedOnSite', 'IsLevelUsePositionStatus', 'IsDepartmentPositionEnabled', 'IsPositionUseLevel' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsPayrollDataFilterByAuthorization": mIsNotFilterByAuthorization = ParValue == "N"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsFilterByLevelHR": mIsFilterByLevelHR = ParValue == "Y"; break;
                            case "IsPPSApprovalBasedOnDept": mIsPPSApprovalBasedOnDept = ParValue == "Y"; break;
                            case "IsAnnualLeaveUseStartDt": mIsAnnualLeaveUseStartDt = ParValue == "Y"; break;
                            case "IsPPSShowJobTransferByGrp": mIsPPSShowJobTransferByGrp = ParValue == "Y"; break;                            
                            case "IsPPSAutoShowEmpSalary": mIsPPSAutoShowEmpSalary = ParValue == "Y"; break;
                            case "IsPPSUpdateEmpDivBasedOnDept": mIsPPSUpdateEmpDivBasedOnDept = ParValue == "Y"; break;
                            case "IsEmpContractDtMandatory": mIsEmpContractDtMandatory = ParValue == "Y"; break;
                            case "IsOldCodeAutoFillAfterPromotion": mIsOldCodeAutoFillAfterPromotion = ParValue == "Y"; break;                            
                            case "IsPPSUseSection": mIsPPSUseSection = ParValue == "Y"; break;
                            case "IsPPSUseLevel": mIsPPSUseLevel = ParValue == "Y"; break;
                            case "IsPPSAutoUpdEmpADBasedOnLevel": mIsPPSAutoUpdEmpADBasedOnLevel = ParValue == "Y"; break;
                            case "IsPPSAllowToUploadFile": mIsPPSAllowToUploadFile = ParValue == "Y"; break;                            
                            case "IsPltCheckboxNotBasedOnPosition": mIsPltCheckboxNotBasedOnPosition = ParValue == "Y"; break;                            
                            case "IsWorkPeriodePPSMandatory": mIsWorkPeriodePPSMandatory = ParValue == "Y"; break;
                            case "IsWorkPeriodePPSEditable": mIsWorkPeriodePPSEditable = ParValue == "Y"; break;
                            case "IsPPSDepartmentBasedOnSite": mIsPPSDepartmentBasedOnSite = ParValue == "Y"; break;
                            case "IsLevelUsePositionStatus": mIsLevelUsePositionStatus = ParValue == "Y"; break;
                            case "IsDepartmentPositionEnabled": mIsDepartmentPositionEnabled = ParValue == "Y"; break;
                            case "IsPositionUseLevel": mIsPositionUseLevel = ParValue == "Y"; break;

                            //string
                            case "EmpCodePPs": mEmpCodePPs = ParValue; break;
                            case "EmploymentStatusTetap": mEmploymentStatusTetap = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PPSAutoUpdEmpADBasedOnLevelSource": mPPSAutoUpdEmpADBasedOnLevelSource = ParValue; break;
                            case "ActingOfficialIndSource": mActingOfficialIndSource = ParValue; break;

                        }
                    }
                }
                dr.Close();
            }

            if (mPPSAutoUpdEmpADBasedOnLevelSource.Length == 0) mPPSAutoUpdEmpADBasedOnLevelSource = "1";
            if (mActingOfficialIndSource.Length == 0) mActingOfficialIndSource = "1";


            //mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            //mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            //mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            //mIsFilterByLevelHR = Sm.GetParameterBoo("IsFilterByLevelHR");
            //mIsPPSApprovalBasedOnDept = Sm.GetParameterBoo("IsPPSApprovalBasedOnDept");
            //mIsAnnualLeaveUseStartDt = Sm.GetParameterBoo("IsAnnualLeaveUseStartDt");
            //mIsPPSShowJobTransferByGrp = Sm.GetParameterBoo("IsPPSShowJobTransferByGrp");
            //mEmpCodePPs = Sm.GetParameter("EmpCodePPs");
            //mIsPPSAutoShowEmpSalary = Sm.GetParameterBoo("IsPPSAutoShowEmpSalary");
            //mIsPPSUpdateEmpDivBasedOnDept = Sm.GetParameterBoo("IsPPSUpdateEmpDivBasedOnDept");
            //mIsEmpContractDtMandatory = Sm.GetParameterBoo("IsEmpContractDtMandatory");
            //mIsOldCodeAutoFillAfterPromotion = Sm.GetParameterBoo("IsOldCodeAutoFillAfterPromotion");
            //mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");   
            //mIsPPSUseSection = Sm.GetParameterBoo("IsPPSUseSection");
            //mIsPPSUseLevel = Sm.GetParameterBoo("IsPPSUseSection");
            //mIsPPSAutoUpdEmpADBasedOnLevel = Sm.GetParameterBoo("IsPPSAutoUpdEmpADBasedOnLevel");
            //mIsPPSAllowToUploadFile = Sm.GetParameterBoo("IsPPSAllowToUploadFile");
            //mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            //mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            //mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            //mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            //mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            //mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            //mIsPltCheckboxNotBasedOnPosition = Sm.GetParameterBoo("IsPltCheckboxNotBasedOnPosition");
            //mPPSAutoUpdEmpADBasedOnLevelSource = Sm.GetParameter("PPSAutoUpdEmpADBasedOnLevelSource");
            //mActingOfficialIndSource = Sm.GetParameter("ActingOfficialIndSource");
            //mIsWorkPeriodePPSMandatory = Sm.GetParameterBoo("IsWorkPeriodePPSMandatory");
            //mIsWorkPeriodePPSEditable = Sm.GetParameterBoo("IsWorkPeriodePPSEditable");
            //mIsPPSDepartmentBasedOnSite = Sm.GetParameterBoo("IsPPSDepartmentBasedOnSite");
            //mIsLevelUsePositionStatus = Sm.GetParameterBoo("IsLevelUsePositionStatus");
            //mIsDepartmentPositionEnabled = Sm.GetParameterBoo("IsDepartmentPositionEnabled");
            //mIsPositionUseLevel = Sm.GetParameterBoo("IsPositionUseLevel");
        }

        private bool IsDocApprovalSettingExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting Where DocType = 'PPS' " +
                (mIsPPSApprovalBasedOnDept?" And DeptCode Is Not Null ":string.Empty) +
                "Limit 1; "
                );
        }

        private void SetLueGrdLvlCode(ref DXE.LookUpEdit Lue, string GrdLvlCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.GrdLvlCode As Col1, T.GrdLvlName As Col2 ");
                SQL.AppendLine("From TblGradeLevelHdr T ");
                if (GrdLvlCode.Length != 0)
                    SQL.AppendLine("Where T.GrdLvlCode='" + GrdLvlCode + "' ");
                else
                {
                    if (!mIsNotFilterByAuthorization)
                    {
                        SQL.AppendLine("Where GrdLvlCode In ( ");
                        SQL.AppendLine("    Select T2.GrdLvlCode ");
                        SQL.AppendLine("    From TblPPAHdr T1 ");
                        SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                        SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Order By GrdLvlName; ");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(
                    ref Lue, ref cm,
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueEmpJobTransfer(ref LookUpEdit Lue)
        {
             try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.JobCode As Col1, B.OptDesc As Col2 from TblGroupJobTransfer A ");
                SQL.AppendLine("Inner Join TblOption B On B.OptCat='empjobtransfer' and A.JobCode=B.OptCode ");
                SQL.AppendLine("Where A.GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode)");
                SQL.AppendLine("Order By B.OptDesc; ");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(
                    ref Lue, ref cm,
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
              Sm.ShowErrorMsg(Exc);
            }

        }

        private MySqlCommand UpdatePPSFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPPS Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdatePPSFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPPS Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdatePPSFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPPS Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePPSFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePPSFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePPSFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsPPSAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPPS ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsPPSAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPPS ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsPPSAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPPS ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnEmpCode_Click_1(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPPSDlg(this));
        }

        private void BtnEmpCode2_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
            {
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = TxtEmpCode.Text;
                f.ShowDialog();
            }
        }

        private void BtnLogWorkPeriod_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
            {
                var f = new FrmPPSDlg5(this, TxtDocNo.Text);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        private void BtnEmployeeRequestDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPPSDlg2(this));
        }

        private void BtnEmployeeRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmployeeRequestDocNo, "Employee request document#", false))
            {
                var f = new FrmEmployeeRequest("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtEmployeeRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Misc Control Event

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsPPSShowJobTransferByGrp)
                    Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueEmpJobTransfer));
                else
                    Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpJobTransfer");
            }
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsPPSDepartmentBasedOnSite && !IsFromDlg)
                {
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue4(Sl.SetLueDeptCodeBasedOnSiteCode), string.Empty, Sm.GetLue(LueSiteCode), mIsFilterByDeptHR ? "Y" : "N");
                    if (mIsDepartmentPositionEnabled && !IsFromDlg) Sl.SetLuePosCodeBasedOnDeptCode(ref LuePosCode, string.Empty, Sm.GetLue(LueDeptCode));
                }
                else Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                if (mIsPPSDepartmentBasedOnSite && !IsFromDlg) Sl.SetLueDeptCodeBasedOnSiteCode(ref LueDeptCode, string.Empty, Sm.GetLue(LueSiteCode), mIsFilterByDeptHR ? "Y" : "N");
            }
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsDepartmentPositionEnabled && !IsFromDlg)
                {
                    Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue3(Sl.SetLuePosCodeBasedOnDeptCode), string.Empty, Sm.GetLue(LueDeptCode));
                    if (mIsPositionUseLevel && !IsFromDlg) Sl.SetLueLevelCodeBasedOnPosCode(ref LueLevelCode, string.Empty, Sm.GetLue(LuePosCode));
                }
                else Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
                
                string ActingOfficialInd = Sm.GetValue("Select ActingOfficialInd From TblPosition Where PosCode = @Param", Sm.GetLue(LuePosCode));
                ChkActingOfficialNew.Checked = Sm.CompareStr(ActingOfficialInd, "Y");
            }
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue2(SetLueGrdLvlCode), string.Empty);
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        private void LueSystemType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSystemType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpSystemType");
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        private void LuePositionStatusCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsLevelUsePositionStatus && !IsFromDlg)
                {
                    string PositionStatusCode = Sm.GetValue("Select PositionStatusCode From TblLevelHdr Where LevelCode = @Param", Sm.GetLue(LueLevelCode));
                    Sm.RefreshLookUpEdit(LuePositionStatusCode, new Sm.RefreshLue2(Sl.SetLuePositionStatusCode), PositionStatusCode);
                }
                else Sm.RefreshLookUpEdit(LuePositionStatusCode, new Sm.RefreshLue1(Sl.SetLuePositionStatusCode));
            }
        }

        private void LueSectionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSectionCode, new Sm.RefreshLue2(SetLueSection), string.Empty);
        }

        private void LueLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsPositionUseLevel && !IsFromDlg) Sm.RefreshLookUpEdit(LueLevelCode, new Sm.RefreshLue3(Sl.SetLueLevelCodeBasedOnPosCode), string.Empty, Sm.GetLue(LuePosCode));
                else Sm.RefreshLookUpEdit(LueLevelCode, new Sm.RefreshLue2(Sl.SetLueLevelCode), string.Empty);

                if (mIsLevelUsePositionStatus && !IsFromDlg)
                {
                    string PositionStatusCode = Sm.GetValue("Select PositionStatusCode From TblLevelHdr Where LevelCode = @Param", Sm.GetLue(LueLevelCode));
                    Sl.SetLuePositionStatusCode(ref LuePositionStatusCode, string.Empty);
                    Sm.SetLue(LuePositionStatusCode, string.Empty);
                    if (PositionStatusCode.Length > 0)
                    {
                        Sl.SetLuePositionStatusCode(ref LuePositionStatusCode, PositionStatusCode);
                        Sm.SetLue(LuePositionStatusCode, PositionStatusCode);
                    }
                }
            }
        }

        private void TxtWorkPeriodYr_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWorkPeriodYr, 21);
        }

        private void TxtWorkPeriodMth_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWorkPeriodMth, 21);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        #endregion

        #endregion

        #region Report Class

        private class PPs
        {
            public string CompanyLogo { get; set; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }

            public string DocDt { get; set; }
            public string StartDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string SiteName { get; set; }

            public string Gender { get; set; }
            public string ResignDt { get; set; }
            public string PosName { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
           
        }

        private class PPs2
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }


        }

        private class AllowanceDeduction
        {
            public string EmpCode { set; get; }
            public string DNo { set; get; }
            public string ADCode { set; get; }
            public string StartDt { set; get; }
            public string EndDt { set; get; }
            public decimal Amt { set; get; }
            public string CreateBy { set; get; }
            public string CreateDt { set; get; }
        }
       
        #endregion
    }
}
