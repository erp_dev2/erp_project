﻿#region Update
/*
    13/07/2017 [TKG] tambah entity dan remark pada saat update journal.
    28/08/2017 [TKG] tambah filter berdasarkan voucher#
    24/08/2018 [WED] Tambahan fitur Excel berdasarkan FrmBase5
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherCheckPosting : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherCheckPosting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                base.FrmLoad(sender, e);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.Description, A.AcType, ");
            SQL.AppendLine("Case When A.AcType='D' Then D.COAAcNo Else A.AcNo End As DAcNo, ");
            SQL.AppendLine("Case When A.AcType='D' Then E.AcDesc Else C.AcDesc End As DAcDesc, ");
            SQL.AppendLine("Case When A.AcType='C' Then D.COAAcNo Else A.AcNo End As CAcNo, ");
            SQL.AppendLine("Case When A.AcType='C' Then E.AcDesc Else C.AcDesc End As CAcDesc, ");
            SQL.AppendLine("A.CurCode, A.Amt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherDtl B On A.DocNo=B.DocNo And B.DNo='001' ");
            SQL.AppendLine("Inner Join TblCOA C On A.AcNo=C.AcNo ");
            SQL.AppendLine("Inner Join TblBankAccount D On A.BankAcCode=D.BankAcCode And D.COAAcNo Is Not Null ");
            SQL.AppendLine("Left Join TblCOA E On D.COAAcNo=E.AcNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.AcNo Is Not Null ");
            SQL.AppendLine("And A.VoucherJournalDocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter + "Order By A.CreateDt;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Voucher#",
                    "", 
                    "Date",
                    "Voucher"+Environment.NewLine+"Description",
                    "Type",

                    //6-10
                    "",
                    "Debit Account#",
                    "Debit Account"+Environment.NewLine+"Description",
                    "Credit Account#",
                    "Credit Account"+Environment.NewLine+"Description",
                    
                    //11-12
                    "Currency",
                    "Amount"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    100, 20, 80, 250, 0, 
                    
                    //6-10
                    20, 150, 250, 150, 250, 
                    
                    //11-12
                    60, 120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 2, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 4, 5, 7, 8, 9, 10, 11, 12 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();
                var Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBankAcCode), "A.BankAcCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "Description", "AcType", "DAcNo", "DAcDesc", 
                        
                        //6-9
                        "CAcNo", "CAcDesc", "CurCode", "Amt" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    }, true, false, true, false
                );
                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdBool(Grd1, Row, 0)) 
                        ProcessData1(
                            Sm.GetGrdStr(Grd1, Row, 1),
                            Sm.GetGrdDate(Grd1, Row, 3),
                            Sm.GetGrdStr(Grd1, Row, 5)=="D"?
                                Sm.GetGrdStr(Grd1, Row, 9):
                                Sm.GetGrdStr(Grd1, Row, 7)
                            );

                Sm.ExecCommands(cml);
                Sm.ClearGrd(Grd1, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessData1(string DocNo, string DocDt, string AcNo)
        {
            string
                VoucherJournalDocNo = Sm.GenerateDocNo(DocDt, "VoucherJournal", "TblVoucherJournalHdr"),
                JournalDocNo = Sm.GenerateDocNoJournal(DocDt, 1);

            var cml = new List<MySqlCommand>();

            cml.Add(ProcessData2(DocNo, VoucherJournalDocNo, JournalDocNo, AcNo));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand ProcessData2(string VoucherDocNo, string VoucherJournalDocNo, string JournalDocNo, string AcNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    VoucherJournalDocNo=@VoucherJournalDocNo, AcNo=@AcNo ");
            SQL.AppendLine("Where DocNo=@VoucherDocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DocDt, Concat('Voucher : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherHdr Where DocNo=@VoucherDocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, C.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select T.AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(T.Amt, 0) * ");
            SQL.AppendLine("        Case When T.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=T.DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End As DAmt, ");
            SQL.AppendLine("        0 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr T ");
            SQL.AppendLine("        Where T.DocNo=@VoucherDocNo And T.AcType='C' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, ");
            SQL.AppendLine("        IfNull(A.Amt, 0) * ");
            SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo And A.AcType='C' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T.AcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(T.Amt, 0) * ");
            SQL.AppendLine("        Case When T.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=T.DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr T ");
            SQL.AppendLine("        Where T.DocNo=@VoucherDocNo And T.AcType='D' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        IfNull(A.Amt, 0) * ");
            SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo And A.AcType='D' ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Left Join TblVoucherHdr C On C.DocNo=@VoucherDocNo ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            SQL.AppendLine("Insert Into TblVoucherJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, VoucherDocNo, CurCode, Amt, ExcRate, JournalDocNo, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VoucherJournalDocNo, T.DocDt, 'N', T.DocNo, T.CurCode, T.Amt, ");
            SQL.AppendLine("Case When T.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=T.DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End, ");
            SQL.AppendLine("@JournalDocNo, T.EntCode, T.Remark, T.CreateBy, T.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr T ");
            SQL.AppendLine("Where T.DocNo=@VoucherDocNo; ");

            //SQL.AppendLine("Insert Into TblVoucherJournalDtl ");
            //SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @VoucherJournalDocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt ");
            //SQL.AppendLine("From TblJournalDtl Where DocNo=@JournalDocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblVoucherJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VoucherJournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(Amt, 0) As DAmt, ");
            SQL.AppendLine("        0 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr ");
            SQL.AppendLine("        Where DocNo=@VoucherDocNo And AcType='C' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, ");
            SQL.AppendLine("        IfNull(A.Amt, 0) As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo And A.AcType='C' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select AcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(Amt, 0) As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr ");
            SQL.AppendLine("        Where DocNo=@VoucherDocNo And AcType='D' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, ");
            SQL.AppendLine("        IfNull(A.Amt, 0) As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@VoucherDocNo And A.AcType='D' ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherJournalDocNo", VoucherJournalDocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private bool IsEditedDataNotValid()
        {
            return
                IsDataInvalid() ||
                IsClosingJournalInvalid();
        }

        private bool IsClosingJournalInvalid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 0) && 
                    Sm.IsClosingJournalInvalid(Sm.GetGrdDate(Grd1, r, 3)))
                    return true;
            return false;
        }

        private bool IsEditedDataNotExisted()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to edit minimum 1 record.");
            return true;
        }

        private bool IsDataInvalid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0) && IsDataInvalid2(Row))
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to edit minimum 1 record.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDataInvalid2(int r)
        {
            if (Sm.IsDataExist(
                "Select DocNo From TblVoucherHdr " +
                "Where DocNo=@Param " +
                "And CancelInd='N' " +
                "And VoucherJournalDocNo Is Null;",
                Sm.GetGrdStr(Grd1, r, 1)))
                return false;
            else
                return true;
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
                e.DoDefault = false;
            else
            {
                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                    Sm.FormShowDialog(new FrmVoucherCheckPostingDlg(this, e.RowIndex));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                Sm.FormShowDialog(new FrmVoucherCheckPostingDlg(this, e.RowIndex));
        }

        #endregion

        #endregion

        #region Event

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bank account");
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion

        #endregion
    }
}
