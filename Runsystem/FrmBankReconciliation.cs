﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

using System.IO;
using System.Text.RegularExpressions;
#endregion

namespace RunSystem
{
    public partial class FrmBankReconciliation : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        private bool mIsFilterByBankAccount = false;
        internal FrmBankReconciliationFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmBankReconciliation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Bank Reconciliation Process";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No.",
                    
                    //1-5
                    "",
                    "Account#",
                    "Date",
                    "Description",
                    "Debit",

                    //6-7
                    "Credit",
                    "Balance"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 180, 100, 250, 120,

                    //6-7
                    120, 120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "No.",
                    
                    //1-5
                    "",
                    "Document#",
                    "Voucher's Date",
                    "Description",
                    "Debit",

                    //6-8
                    "Credit",
                    "Balance",
                    "DNo"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 180, 100, 250, 120,

                    //6-8
                    120, 120, 0
                }
            );
            Sm.GrdColCheck(Grd2, new int[] { 1 });
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd2, new int[] { 8 });

            #endregion
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select T.BankAcCode, Concat(T2.BankAcNm, Case When T2.BankAcNo Is Null Then '' Else Concat('-', T2.BankAcNo) End) As BankAcNm, T.DocNo, T.DNo, T.VcYear, T.VcDate, T.VcMonth, ");
            SQL.AppendLine("Case T.VCMonth When '01' Then 'January' ");
            SQL.AppendLine("        When '02' Then 'February' ");
            SQL.AppendLine("        When '03' Then 'March'  ");
            SQL.AppendLine("        When '04' Then 'April' ");
            SQL.AppendLine("        When '05' Then 'May' ");
            SQL.AppendLine("        When '06' Then 'June' ");
            SQL.AppendLine("        When '07' Then 'July' ");
            SQL.AppendLine("        When '08' Then 'August' ");
            SQL.AppendLine("        When '09' Then 'September' ");
            SQL.AppendLine("        When '10' Then 'October' ");
            SQL.AppendLine("        When '11' Then 'November' ");
            SQL.AppendLine("        When '12' Then 'December' End As Month, ");
            SQL.AppendLine("T.Description,  ");
            SQL.AppendLine("T.Debet, T.Credit, T.Opening, ");
            SQL.AppendLine("if(@prev != T.BankAcCode, @bal:=((T.Opening+T.Debet)-T.Credit), @bal:= @bal+(Debet-Credit)) As Balanced, ");
            SQL.AppendLine("@prev:=T.BankAcCode, T1.Username, T.CreateDt ");
            SQL.AppendLine("From(  ");

            SQL.AppendLine("      Select A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("      B.Description, B.Amt As Credit, 0 As Debet, 0 As Opening, A.PIC, A.CreateDt  ");
            SQL.AppendLine("      From TblVoucherHdr A  ");
            SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            
            // only Mandiri Bank
            SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("          And C.BankCode Is Not Null And C.BankCode In (Select ParValue From TblParameter Where Parcode = 'MandiriBankCode') ");
            // end

            SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4) = @Year And A.CancelInd='N' ");
            SQL.AppendLine("      And A.BankAcCode Is Not Null ");
            SQL.AppendLine("      And A.AcType='C' ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And A.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth  ");

            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("      B.Description, 0 As Credit, B.Amt As Debet, 0, A.PIC, A.CreateDt ");
            SQL.AppendLine("      From TblVoucherHdr A ");
            SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4)=@Year And A.CancelInd='N' ");
            SQL.AppendLine("      And A.BankAcCode Is Not Null ");
            SQL.AppendLine("      And A.AcType='D' ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And A.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth  ");


            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("      B.Description, (B.Amt*A.ExcRate) As Credit, 0 As Debet, 0 As Opening, A.PIC, A.CreateDt  ");
            SQL.AppendLine("      From TblVoucherHdr A  ");
            SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4) = @Year And A.CancelInd='N' ");
            SQL.AppendLine("      And A.Actype2 Is Not Null ");
            SQL.AppendLine("      And A.Actype2='C' ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth  ");

            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("      B.Description, 0 As Credit, (B.Amt*A.ExcRate) As Debet, 0, A.PIC, A.CreateDt ");
            SQL.AppendLine("      From TblVoucherHdr A ");
            SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter ");
            SQL.AppendLine("      And Left(A.DocDt, 4)=@Year ");
            SQL.AppendLine("      And A.CancelInd='N' ");
            SQL.AppendLine("      And A.Actype2 Is Not Null ");
            SQL.AppendLine("      And A.Actype2='D' ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Group By A.BankAcCode, A.DocNo, B.Dno, VcDate, VcMonth  ");

            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,  ");
            SQL.AppendLine("      'Opening Balance', 0, 0, ifnull(B.Amt, 0) As Opening, A.Createby, concat(@Year,@MonthFilter,'01','0002') ");
            SQL.AppendLine("      From tblClosingBalanceIncashHdr A  ");
            SQL.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And B.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(B.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      where ");
            if (Sm.GetLue(LueMth) == "01")
            {
                int Yr = Convert.ToInt32(Sm.GetLue(LueYr)) - 1;
                SQL.AppendLine(" A.yr = '" + Yr + "' ");
                SQL.AppendLine(" And A.Mth = '12' ");
            }
            else
            {
                SQL.AppendLine(" A.yr = @Year and A.Mth =   Cast(@MonthFilter As Decimal(10,2)) -1 ");
            }
            SQL.AppendLine("      Order By BankAcCode, VcDate, VcMonth, CreateDt ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Left Join TblUser T1 On T.Pic = T1.UserCode ");
            SQL.AppendLine("Inner Join TblBankAccount T2 On T.BankAcCode=T2.BankAcCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("       Select @bal := 0, @prev:='' ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where Concat(T.DocNo, T.DNo) Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct Concat(X2.VCDocNo, X2.VCDNo) ");
            SQL.AppendLine("    From TblBankReconciliationHdr X1 ");
            SQL.AppendLine("    Inner Join TblBankReconciliationDtl2 X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        And X1.CancelInd = 'N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(") Tbl ");

            return SQL.ToString();
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueYr, LueMth, MeeRemark 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueYr, LueMth, MeeRemark }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueYr, LueMth, MeeRemark
            });
            ClearData2();
        }

        private void ClearData2()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt1, TxtAmt2 }, 0);

            Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd2, false);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBankReconciliationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                var Dt = Sm.GetDte(DteDocDt);
                Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                Sm.SetLue(LueMth, Dt.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BankReconciliation", "TblBankReconciliationHdr");

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveBankReconciliationHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 &&
                    Sm.GetGrdBool(Grd1, r, 1)) 
                    cml.Add(SaveBankReconciliationDtl(DocNo, r));

            for (int r = 0; r < Grd2.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0 &&
                    Sm.GetGrdBool(Grd2, r, 1)) 
                    cml.Add(SaveBankReconciliationDtl2(DocNo, r));


            Sm.ExecCommands(cml);

            ShowData(DocNo);  
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsGrdEmpty() ||
                IsGrdUnchecked() ||
                IsGrdValueNotValid() ||
                IsAmtNotValid();
        }

        private bool IsAmtNotValid()
        {
            if (Decimal.Parse(TxtAmt1.Text) != Decimal.Parse(TxtAmt2.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "CSV File's Amount should be equal to System's Amount.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 csv file's record.");
                return true;
            }
            if (Grd2.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 system's record.");
                return true;
            }
            return false;
        }

        private bool IsGrdUnchecked()
        {
            bool mChecked1 = false, mChecked2 = false;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdBool(Grd1, i, 1))
                {
                    mChecked1 = true;
                    break;
                }
            }

            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdBool(Grd2, i, 1))
                {
                    mChecked2 = true;
                    break;
                }
            }

            if (!mChecked1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 csv file's record.");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }

            if (!mChecked2)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 system's record.");
                Sm.FocusGrd(Grd2, 0, 1);
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" ");

            return false;
        }

        private MySqlCommand SaveBankReconciliationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBankReconciliationHdr(DocNo, DocDt, CancelReason, CancelInd, Yr, Mth, Amt1, Amt2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @Yr, @Mth, @Amt1, @Amt2, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBankReconciliationDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBankReconciliationDtl(DocNo, DNo, AcNo, Dt, AcDesc, Debit, Credit, Balance, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @AcNo, @Dt, @AcDesc, @Debit, @Credit, @Balance, @CreateBy, CurrentDateTime()); ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@AcDesc", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Debit", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Credit", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Balance", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBankReconciliationDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBankReconciliationDtl2(DocNo, DNo, VCDocNo, VCDNo, VCDocDt, VCDesc, Debit, Credit, Balance, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @VCDocNo, @VCDNo, @VCDocDt, @VCDesc, @Debit, @Credit, @Balance, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@VCDocNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@VCDNo", Sm.GetGrdStr(Grd2, Row, 8));
            Sm.CmParamDt(ref cm, "@VCDocDt", Sm.GetGrdDate(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@VCDesc", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Debit", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Credit", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Balance", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBankReconciliationHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBankReconciliationHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand EditBankReconciliationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBankReconciliationHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBankReconciliationHdr(DocNo);
                ShowBankReconciliationDtl(DocNo);
                ShowBankReconciliationDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBankReconciliationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, CancelInd, Yr, Mth, Amt1, Amt2, Remark ");
            SQL.AppendLine("From TblBankReconciliationHdr Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "Yr", "Mth", 
                    
                    //6-8
                    "Amt1", "Amt2", "Remark" 
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueMth, Sm.DrStr(dr, c[5]));
                    TxtAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                }, true
            );
        }

        private void ShowBankReconciliationDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, AcNo, Dt, AcDesc, Debit, Credit, Balance ");
            SQL.AppendLine("From TblBankReconciliationDtl ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("Order By DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo",
                    "Dt", "AcDesc", "Debit", "Credit", "Balance"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowBankReconciliationDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, VCDocNo, VCDocDt, VCDesc, Debit, Credit, Balance, VCDNo ");
            SQL.AppendLine("From TblBankReconciliationDtl2 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("Order By DNo; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "VCDocNo",
                    "VCDocDt", "VCDesc", "Debit", "Credit", "Balance",
                    "VCDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsFilterByBankAccount = Sm.GetParameterBoo("IsFilterByBankAccount");
        }

        private void Process1(ref List<CSV> l)
        {
            string FileName = string.Empty;

            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            if (OD.ShowDialog() == DialogResult.Cancel) return;

            FileName = OD.FileName;

            using (var rd = new StreamReader(FileName))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    var arr = splits.ToArray();

                    if (arr[0].Trim().Length > 0)
                    {
                        l.Add(new CSV()
                        {
                            AccountNo = splits[0].Trim(),
                            Date = splits[2].Trim(),
                            Description = splits[4].Trim(),
                            Debit = Decimal.Parse(splits[5].Trim()),
                            Credit = Decimal.Parse(splits[6].Trim()),
                            Balance = Decimal.Parse(splits[7].Trim()),
                        });
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.NoData, "");
                        ClearData();
                        return;
                    }

                }
            }
        }

        private bool IsCSVDataInvalid(ref List<CSV> l)
        {
            return
                IsDateInvalid(ref l);
        }

        private bool IsDateInvalid(ref List<CSV> l)
        {
            bool mFlag = false;
            int x = 0;

            for (int i = 0; i < l.Count(); i++)
            {
                if (l[i].Date.Contains("/") || l[i].Date.Contains("-") || l[i].Date.Contains(".") || l[i].Date.Contains(" "))
                {
                    mFlag = true;
                    x = i + 1;
                    break;
                }
            }

            if (mFlag)
            {
                Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (x));
                return true;
            }

            return false;
        }

        private void RemoveExistingCSVData(ref List<CSV> l, ref List<CSVExists> l3)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Dt, AcDesc, Balance ");
            SQL.AppendLine("From TblBankReconciliationDtl A ");
            SQL.AppendLine("Inner Join TblBankReconciliationHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Order By A.DocNo, A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Dt", "AcDesc", "Balance" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new CSVExists()
                        {
                            Date = Sm.DrStr(dr, c[0]),
                            Description = Sm.DrStr(dr, c[1]),
                            Balance = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }

            for (int i = 0; i < l3.Count; i++)
            {
                for (int j = 0; j < l.Count; j++)
                {
                    if (l3[i].Date == l[j].Date &&
                        l3[i].Description == l[j].Description &&
                        l3[i].Balance == l[j].Balance)
                    {
                        l.RemoveAt(j); // hapus data CSV dari list, karena : tanggal, deskripsi, dan balance nya, sama dengan data bank reconciliation yang sudah tersimpan
                        if (j != 0) j -= 1; // index j di kurangi 1, agar ngulang looping dari index j sebelumnya
                    }
                }
            }
        }

        private void Process2(ref List<Cashbook> l2)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = GetSQL() + " Order By BankAcCode, VcDate, DocNo, Dno, VcMonth, CreateDt; ";
                Sm.CmParam<String>(ref cm, "@MonthFilter", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Year", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "VcYear", "VcMonth", "VcDate", "Description", "Debet", 
                    "Credit", "Opening", "Balanced", "DNo", "BankAcNm"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Cashbook()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Date = string.Concat(Sm.DrStr(dr, c[1]), Sm.DrStr(dr, c[2]), Sm.DrStr(dr, c[3])),
                            Description = Sm.DrStr(dr, c[4]),
                            Debit = Sm.DrDec(dr, c[5]),
                            Credit = Sm.DrDec(dr, c[6]),
                            Opening = Sm.DrDec(dr, c[7]),
                            Balance = Sm.DrDec(dr, c[8]),
                            DNo = Sm.DrStr(dr, c[9]),
                            BankAcNm = Sm.DrStr(dr, c[10]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Bal(ref List<Cashbook> l2)
        {
            string b = string.Empty;
            decimal balance = 0m;
            for (int row = 0; row < l2.Count - 1; row++)
            {
                //if (Grd2.Rows[row].Type == iGRowType.Normal)
                //{
                    if (b != l2[row].BankAcNm)
                    {
                        balance = 0m;
                        b = l2[row].BankAcNm;
                    }
                    l2[row].Balance = balance + l2[row].Debit + l2[row].Opening - l2[row].Credit;
                    balance = l2[row].Balance;
                //}
            }
        }

        private void Process3(ref List<CSV> l, ref List<Cashbook> l2)
        {
            if (l.Count > 0)
            {
                int No = 0;
                for (int i = 0; i < l.Count; i++)
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[i, 0].Value = No + 1;
                    Grd1.Cells[i, 1].Value = false;
                    Grd1.Cells[i, 2].Value = l[i].AccountNo;
                    if (l[i].Date.Length <= 0) Grd1.Cells[i, 3].Value = string.Empty;
                    else Grd1.Cells[i, 3].Value = Sm.ConvertDate(l[i].Date);
                    Grd1.Cells[i, 4].Value = l[i].Description;
                    Grd1.Cells[i, 5].Value = l[i].Debit;
                    Grd1.Cells[i, 6].Value = l[i].Credit;
                    Grd1.Cells[i, 7].Value = l[i].Balance;
                    No++;
                }
            }

            if (l2.Count > 0)
            {
                int No = 0;
                for (int i = 0; i < l2.Count; i++)
                {
                    Grd2.Rows.Add();
                    Grd2.Cells[i, 0].Value = No + 1;
                    Grd2.Cells[i, 1].Value = false;
                    Grd2.Cells[i, 2].Value = l2[i].DocNo;
                    if (l2[i].Date.Length <= 0) Grd2.Cells[i, 3].Value = string.Empty;
                    else Grd2.Cells[i, 3].Value = Sm.ConvertDate(l2[i].Date);
                    Grd2.Cells[i, 4].Value = l2[i].Description;
                    Grd2.Cells[i, 5].Value = l2[i].Debit;
                    Grd2.Cells[i, 6].Value = l2[i].Credit;
                    Grd2.Cells[i, 7].Value = l2[i].Balance;
                    Grd2.Cells[i, 8].Value = l2[i].DNo;
                    No++;
                }
            }
        }

        private void CalculateBalanceCSV()
        {
            decimal mCSVAmt = 0m;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdBool(Grd1, i, 1))
                    {
                        mCSVAmt += Sm.GetGrdDec(Grd1, i, 7);
                    }
                }
            }

            TxtAmt1.EditValue = Sm.FormatNum(mCSVAmt, 0);
        }

        private void CalculateBalanceCashbook()
        {
            decimal mCashbookAmt = 0m;

            if (Grd2.Rows.Count > 0)
            {
                for (int i = 0; i < Grd2.Rows.Count; i++)
                {
                    if (Sm.GetGrdBool(Grd2, i, 1))
                    {
                        mCashbookAmt += Sm.GetGrdDec(Grd2, i, 7);
                    }
                }
            }

            TxtAmt2.EditValue = Sm.FormatNum(mCashbookAmt, 0);
        }

        #endregion

        #region Grid Methods

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Grd1.Cells[Row, 1].Value = !IsSelected;

                CalculateBalanceCSV();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
                if (e.ColIndex == 1)
                    CalculateBalanceCSV();
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                bool IsSelected = Sm.GetGrdBool(Grd2, 0, 1);
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                        Grd2.Cells[Row, 1].Value = !IsSelected;

                CalculateBalanceCashbook();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
                if (e.ColIndex == 1)
                    CalculateBalanceCashbook();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearData2();
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearData2();
        }

        #endregion

        #region Button Click

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(!Sm.IsLueEmpty(LueMth, "Month") &&
                    !Sm.IsLueEmpty(LueYr, "Year"))
                {
                    ClearData2();

                    var l = new List<CSV>();
                    var l2 = new List<Cashbook>();
                    var l3 = new List<CSVExists>();
                    
                    Process1(ref l);
                    if (l.Count > 0)
                    {
                        if (IsCSVDataInvalid(ref l)) return;
                        RemoveExistingCSVData(ref l, ref l3);
                    }
                    Process2(ref l2);
                    if (l2.Count > 0) Bal(ref l2);
                    Process3(ref l, ref l2);

                    l.Clear();
                    l2.Clear();
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class CSV
        {
            public string AccountNo { get; set; }
            public string Date { get; set; }
            public string Description { get; set; }
            public decimal Debit { get; set; }
            public decimal Credit { get; set; }
            public decimal Balance { get; set; }
        }

        private class CSVExists
        {
            public string Date { get; set; }
            public string Description { get; set; }
            public decimal Balance { get; set; }
        }

        private class Cashbook
        {
            public string DocNo { get; set; }
            public string Date { get; set; }
            public string Description { get; set; }
            public decimal Debit { get; set; }
            public decimal Credit { get; set; }
            public decimal Opening { get; set; }
            public decimal Balance { get; set; }
            public string DNo { get; set; }
            public string BankAcNm { get; set; }
        }

        #endregion

    }
}
