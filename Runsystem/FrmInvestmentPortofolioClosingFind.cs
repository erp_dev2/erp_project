﻿#region Update
/*
    10/05/2022 [IBL/PRODUCT] : New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentPortofolioClosingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmInvestmentPortofolioClosing mFrmParent;

        #endregion

        #region Constructor

        public FrmInvestmentPortofolioClosingFind(FrmInvestmentPortofolioClosing FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ClosingDt, 0.00 As InterestRecv, Sum(IfNull(B.UnrealizedGL, 0.00)) As UnrealizedGL, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblInvestmentPortofolioClosingHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("	Select DocNo, (IfNull(Qty, 0.00) * IfNull(MarketPrice, 0.00)) - (IfNull(Qty, 0.00) * IfNull(MovingAvgPrice, 0.00)) As UnrealizedGL ");
            SQL.AppendLine("	From TblInvestmentPortofolioClosingDtl ");
            SQL.AppendLine(")B On A.DocNo = B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By A.DocNo ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Closing Date",
                    "Interest"+Environment.NewLine+"Receivable",
                    "Unrealized"+Environment.NewLine+"Gain/Loss", 

                    //6-10
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date",
                    "Created"+Environment.NewLine+"Time",
                    "Last"+Environment.NewLine+"Updated By",
                    "Last"+Environment.NewLine+"Updated Date",

                    //11
                    "Last"+Environment.NewLine+"Updated Time"
                }
            );

            Sm.GrdFormatDec(Grd1, new int[] { 4, 5 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 3, 7, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsFilterByDateInvalid(ref DteClosingDt1, ref DteClosingDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteClosingDt1), Sm.GetDte(DteClosingDt2), "A.ClosingDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter) + " Order By A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "ClosingDt", "InterestRecv", "UnrealizedGL", "CreateBy",

                            //6-8
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 11, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteClosingDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteClosingDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkClosingDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Closing Date");
        }

        #endregion

        #region Grid Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

    }
}
