﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBinTransferRequestDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBinTransferRequest mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty;

        #endregion

        #region Constructor

        public FrmBinTransferRequestDlg(FrmBinTransferRequest FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.Bin, A.Lot ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblBin B On A.Bin=B.Bin And B.Status='3' ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And (A.Qty>0) ");
            SQL.AppendLine("And IfNull(A.Bin, '')<>'' ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select Bin ");
            SQL.AppendLine("    From TblBinTransferRequestDtl ");
            SQL.AppendLine("    Where ProcessInd='O' And CancelInd='N' ");
            SQL.AppendLine("    And Bin=IfNull(A.Bin, '') ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Locate(Concat('##', A.Bin, '##'), @SelectedBin)<1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(Grd1, 
                new string[]{ "No", "", "Bin", "Lot" }, 
                new int[]{ 50, 20, 130, 130 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@SelectedBin", mFrmParent.GetSelectedBin());

                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Bin;",
                        new string[]{ "Bin", "Lot" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            bool IsChoose = false;
            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                mFrmParent.Grd2.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !IsBinAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Sm.CopyGrdValue(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, 4, Grd1, Row, 2);
                        mFrmParent.InsertItem(Sm.GetGrdStr(Grd1, Row, 2));
                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 5, 7, 9 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
                mFrmParent.Grd2.EndUpdate();
                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 bin.");
            }
        }

        private bool IsBinAlreadyChosen(int Row)
        {
            var Bin = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 4), Bin) )
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        #endregion

        #endregion
    }
}
