﻿
#region Update
/*
    29/09/2017 [TKG] mempercepat proses copy ke layar utama. 
 *  30/07/2019 [DITA] tambah customer's PO dan Sales Contract#
 *  20/04/2020 [IBL/KSM] memunculkan foreign name berdasarkan parameter IsShowForeignName
 *  23/07/2020 [ICA/SIER] hide kolom packaging UoM, Quantity Packaging, Outstanding Quantity Packanging dengan parameter IsSO2NotUsePackagingLabel
 *  31/03/2021 [VIN/SIER] copy remark dan delivery date SO 
 *  15/06/2021 [MYA/SIER] Kolom item category pada menu Delivery Request terfilter berdasarkan grup log in.

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDRDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDR mFrmParent;
        string mSQL = string.Empty, mCtCode = "", mAgtCode = "";
        bool mCBDInd = false;
        

        #endregion

        #region Constructor

        public FrmDRDlg(FrmDR FrmParent, string CtCode, string AgtCode, bool CBDInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mAgtCode = AgtCode;
            mCBDInd = CBDInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mCBDInd)
            {
                SQL.AppendLine("Select A.SAName, E.ItCode, F.ItCodeInternal, F.ItName, F.ForeignName, B.PackagingUnitUomCode, ");
                SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, D.PriceUomCode, F.InventoryUomCode, B.DeliveryDt, ");
                SQL.AppendLine("B.QtyPackagingUnit, 0 As OutstandingQtyPackagingUnit,  ");
                SQL.AppendLine("B.Qty, 0 As OutstandingQty, K.DocNo As VoucherDocNo, B.Remark, ");
                SQL.AppendLine("((C.UPrice-(C.UPrice*0.01*IfNull(L.DiscRate, 0)))+ ((C.UPrice-(C.UPrice*0.01*IfNull(L.DiscRate, 0)))*0.01*B.TaxRate)) As PriceAfterTax, A.LocalDocNo, A.CtPONo ");
                SQL.AppendLine("From TblSOHdr A ");
                SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd5 <> 'F' ");
                SQL.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo");
                SQL.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DOcNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DOcNo And C.ItemPriceDNo = E.DNo ");
                SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode");
                SQL.AppendLine("Inner Join TblsalesInvoiceHdr G On A.DocNo = G.SODocNo");
                SQL.AppendLine("Inner Join TblincomingpaymentDtl H On G.DocNo = H.InvoiceDocNo");
                SQL.AppendLine("Inner Join TblincomingpaymentHdr I On H.DocNo = I.DocNo And I.CancelInd = 'N'");
                SQL.AppendLine("Inner Join TblVoucherRequestHdr J On I.VoucherRequestDocNo = J.Docno");
                SQL.AppendLine("Inner Join TblVoucherhdr K On J.DocNo = K.VoucherRequestDocNo  And K.CancelInd = 'N'");
                SQL.AppendLine("Left Join TblSOQuotPromoItem L On A.SOQuotPromoDocNo=L.DocNo And E.ItCode=L.ItCode ");
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("And A.CancelInd='N'  ");
                SQL.AppendLine("And A.Status <> 'F'");
                SQL.AppendLine("And A.CtCode=@CtCode ");
            }
            else
            {
                SQL.AppendLine("Select A.SAName, E.ItCode, F.ItCodeInternal, F.ItName, F.ForeignName, B.PackagingUnitUomCode, ");
                SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, D.PriceUomCode, F.InventoryUomCode, B.DeliveryDt, ");
                SQL.AppendLine("B.QtyPackagingUnit, (B.QtyPackagingUnit-IfNull(G.QtyPackagingUnit, 0)) As OutstandingQtyPackagingUnit, B.Remark, ");
                SQL.AppendLine("B.Qty, (B.Qty-IfNull(G.Qty, 0)) As OutstandingQty, null As VoucherDocNo, ");
                SQL.AppendLine("((C.UPrice-(C.UPrice*0.01*IfNull(H.DiscRate, 0)))+ ((C.UPrice-(C.UPrice*0.01*IfNull(H.DiscRate, 0)))*0.01*B.TaxRate)) As PriceAfterTax, A.LocalDocNo, A.CtPONo ");
                SQL.AppendLine("From TblSOHdr A ");
                SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd<>'F' ");
                SQL.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DOcNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DOcNo And C.ItemPriceDNo = E.DNo ");
                SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.SODocNo As DocNo, T2.SODNo As DNo,  ");
                SQL.AppendLine("    Sum(IfNull(T2.QtyPackagingUnit, 0)) As QtyPackagingUnit, Sum(IfNull(T2.Qty, 0)) As Qty  ");
                SQL.AppendLine("    From TblDRHdr T1  ");
                SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo And T3.CancelInd='N' And T3.Status Not In ('M', 'F') And T3.CtCode=@CtCode ");
                SQL.AppendLine("    Inner Join TblSODtl T4 On T2.SODocNo=T4.DocNo And T2.SODNo=T4.DNo and T4.ProcessInd<>'F' ");
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
                SQL.AppendLine(") G On B.DocNo=G.DocNo And B.DNo=G.DNo ");
                SQL.AppendLine("Left Join TblSOQuotPromoItem H On A.SOQuotPromoDocNo=H.DocNo And E.ItCode=H.ItCode ");
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("And A.CancelInd='N' And A.OverSeaInd = 'N' ");
                SQL.AppendLine("And A.Status Not In ('M', 'F') ");
                SQL.AppendLine("And A.CtCode=@CtCode And A.DocNo Not In (Select SODocNo From tblsalesInvoiceHdr Where CancelInd = 'N' and SODocno Is not null ) ");
                if (mFrmParent.mIsItCtFilteredByGroup)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                //SQL.AppendLine("And A.DocNo Not In (Select SODocNo From tblsalesInvoiceHdr Where CancelInd = 'N' and SODocno Is not null ) ");
                if (mAgtCode.Length > 0)
                    SQL.AppendLine("And B.AgtCode =@AgtCode ");
            }


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Shipping Name",
                        "Item's"+Environment.NewLine+"Code", 
                        "",
                        "Item's Name",

                        //6-10
                        "Local Code",
                        "Packaging"+Environment.NewLine+"UoM", 
                        "Quantity"+Environment.NewLine+"(Packaging)",
                        "Oustanding Quantity"+Environment.NewLine+"(Packaging)",
                        "",
                        
                        //11-15
                        "Sales Order#",
                        "SO"+Environment.NewLine+"Date",
                        "SO DNo",
                        "Quantity"+Environment.NewLine+"(Sales)",
                        "Outstanding Quantity"+Environment.NewLine+"(Sales)",
                        
                        //16-20
                        "UoM"+Environment.NewLine+"(Sales)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Delivery"+Environment.NewLine+"Date",
                        "Voucher",
                        "Price"+Environment.NewLine+"After Tax",

                        //21-23
                         mFrmParent.mLocalDocument=="1"?"Sales"+Environment.NewLine+"Contract#":"Local"+Environment.NewLine+"Document#",
                        "Customer's PO#",
                        "Foreign Name",
                        "Remark",

                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 200, 80, 20, 250,  
                        
                        //6-10
                        80, 100, 100, 100, 20, 
                        
                        //11-15
                        150, 130, 80, 100, 100,   
                        
                        //16-20
                        80, 80, 100, 100, 100 ,

                        //21-24
                        130, 100, 120, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 14, 15, 20 }, 0 );
            Sm.GrdColButton(Grd1, new int[] { 4, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 12, 18 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            Grd1.Cols[21].Move(14);
            Grd1.Cols[22].Move(15);
            Grd1.Cols[23].Move(6);
            if (mCBDInd == false)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 13, 19, 20, 24 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 13, 19, 20, 24 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);

            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 23 });

            if (mFrmParent.mIsSO2NotUsePackagingLabel)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 13 }, !ChkHideInfoInGrd.Checked);
        }
    

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@AgtCode", mAgtCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "F.ItCodeInternal", "F.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "SAName", 
                            //1-5
                            "ItCode", "ItName", "ItCodeInternal", "PackagingUnitUomCode", "QtyPackagingUnit",  
                            //6-10
                            "OutstandingQtyPackagingUnit", "DocNo", "DocDt", "DNo", "Qty",
                            //11-15
                            "OutstandingQty", "PriceUomCode", "InventoryUomCode", "DeliveryDt", "VoucherDocNo",
                            //16-20
                            "PriceAfterTax", "LocalDocNo", "CtPONo", "ForeignName", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void ChooseData()
        {

            if (mCBDInd == false)
            {
                int Row1 = 0, Row2 = 0;
                bool IsChoose = false;

                if (Grd1.Rows.Count != 0)
                {
                    mFrmParent.Grd1.BeginUpdate();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItemAlreadyChosen(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 15);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 15);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 17);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 18);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 20);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 23);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 24);

                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 13, 14, 21 });
                        }
                    }
                    mFrmParent.ComputeUom();
                    mFrmParent.Grd1.EndUpdate();
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            }
            else
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.ShowDataSO(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19));
                    this.Hide();
                }
            }

        }

        private bool IsItemAlreadyChosen(int Row)
        {
            var Item =
                Sm.GetGrdStr(Grd1, Row, 3) +
                Sm.GetGrdStr(Grd1, Row, 11) +
                Sm.GetGrdStr(Grd1, Row, 13) +
                Sm.GetGrdStr(Grd1, Row, 7) +
                Sm.GetGrdStr(Grd1, Row, 16);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 1) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 5) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 7) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 8) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 15),
                    Item
                    )) return true;
            return false;
        }
       

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }


        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
