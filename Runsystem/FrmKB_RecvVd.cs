﻿#region Update
/*
    13/02/2019 [TKG] New application for Kawasan Berikat
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmKB_RecvVd : RunSystem.FrmBase5
    {
        #region Field

        private string  
            mMenuCode = string.Empty, mSQL = string.Empty,
            mKB_Server = string.Empty,
            mKB_Database = string.Empty,
            mKB_DBUser = string.Empty,
            mKB_DBPwd = string.Empty,
            mKB_Port = string.Empty,
            mMainCurCode = string.Empty;
        private bool mIsAutoJournalActived = false;
        
        #endregion

        #region Constructor

        public FrmKB_RecvVd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Local#",
                        "Date", 
                        "PO#",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 100, 200, 20
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            var l = new List<KB_Data>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    l.RemoveAll(w => w.PODocNo.Length==0);
                    if (l.Count > 0) Process3(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();    
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "Nomor_Dokumen", false);

            SQL.AppendLine("Select Distinct Trim(Nomor_Dokumen) As LocalDocNo, Date_Format(Tanggal_Dokumen, '%Y%m%d') As DocDt ");
            SQL.AppendLine("From Tpb_Dokumen ");
            SQL.AppendLine("Where Tipe_Dokumen='02' ");
            SQL.AppendLine("And Kode_Jenis_Dokumen='315' ");
            SQL.AppendLine("And Date_Format(Tanggal_Dokumen, '%Y%m%d') Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By Tanggal_Dokumen, Nomor_Dokumen;");

            using (var cn = new MySqlConnection(ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "LocalDocNo", "DocDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new KB_Data()
                        {
                            LocalDocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            PODocNo = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var l2 = new List<PO>();

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.LocalDocNo=@LocalDocNo0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@LocalDocNo0" + i.ToString(), l[i].LocalDocNo);
            }

            if (Filter.Length != 0) Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select Distinct A.LocalDocNo, A.DocNo ");
            SQL.AppendLine("From TblPOHdr A, TblPODtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.LocalDocNo Is Not Null ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And B.CancelInd='N' ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblRecvVdDtl ");
            SQL.AppendLine("    Where Status In ('O', 'A') ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And PODocNo=B.DocNo ");
            SQL.AppendLine("    And PODNo=B.DNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LocalDocNo", "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new PO()
                        {
                            LocalDocNo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l2.Count>0)
            {
                foreach (var i in l)
                {
                    foreach (var j in l2.Where(w=>Sm.CompareStr(w.LocalDocNo, i.LocalDocNo)))
                    {
                        i.PODocNo = j.DocNo;
                        break;
                    }
                }
                l2.Clear();
            }
        }

        private void Process3(ref List<KB_Data> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i+1;
                r.Cells[1].Value = true;
                r.Cells[2].Value = l[i].LocalDocNo;
                if (l[i].DocDt.Length > 0) r.Cells[3].Value = Sm.ConvertDate(l[i].DocDt);
                r.Cells[4].Value = l[i].PODocNo;
            }
            //r = Grd1.Rows.Add();
            Grd1.EndUpdate();
        }

        #region Save Data

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No ||
                    IsProcessDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string LocalDocNo = string.Empty, DocNo = string.Empty, DocDt = string.Empty, WhsCode = Sm.GetLue(LueWhsCode);
                int c = 0;
                if (Grd1.Rows.Count>0)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        LocalDocNo = Sm.GetGrdStr(Grd1, r, 2);
                        DocDt = Sm.GetGrdDate(Grd1, r, 3);
                        DocNo = Sm.GetGrdStr(Grd1, r, 4);
                        if (Sm.GetGrdBool(Grd1, r, 1) && DocNo.Length > 0)
                            SaveData(DocNo, DocDt.Substring(0, 8), LocalDocNo, WhsCode, ref c);
                    }
                }
                if (c<=1)
                    Sm.StdMsg(mMsgType.Info, c.ToString() + " record already processed.");
                else
                    Sm.StdMsg(mMsgType.Info, c.ToString() + " records already processed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsProcessDataNotValid()
        {
            return 
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length>0) 
                    return false;
            Sm.StdMsg(mMsgType.Warning, "You need to process minimum 1 document.");
            return true;
        }

        private bool IsPOInvalid(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblPOHdr A, TblPODtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.LocalDocNo Is Not Null ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And B.CancelInd='N' ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblRecvVdDtl ");
            SQL.AppendLine("    Where Status In ('O', 'A') ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And PODocNo=B.DocNo ");
            SQL.AppendLine("    And PODNo=B.DNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return !Sm.IsDataExist(cm);
        }

        private void SaveData(string PODocNo, string DocDt, string LocalDocNo, string WhsCode, ref int c)
        {
            if (IsPOInvalid(PODocNo)) return;

            string DocNo = Sm.GenerateDocNo(DocDt, "RecvVd", "TblRecvVdHdr");

            var cml = new List<MySqlCommand>();
            cml.Add(SaveData(DocNo, PODocNo, DocDt, LocalDocNo, WhsCode));
            Sm.ExecCommands(cml);

            c += 1;
        }

        private MySqlCommand SaveData(string DocNo, string PODocNo, string DocDt, string LocalDocNo, string WhsCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvVdHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, ");
            SQL.AppendLine("POInd, WhsCode, VdCode, KawasanBerikatInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, @LocalDocNo, 'Y', @WhsCode, VdCode, 'Y', 'AUTOMATIC GENERATED BY KAWASAN BERIKAT APPS.', @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPOHdr Where DocNo=@PODocNo; ");

            SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, D.DNo, 'N', 'A', @PODocNo, D.DNo, I.ItCode, ");
            SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo) As BatchNo, ");
            SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', D.DNo), '-' As Lot, '-' As Bin, ");
            SQL.AppendLine("(D.Qty-IfNull(E.Qty2, 0.00)-IfNull(F.Qty3, 0.00)+IfNull(G.Qty4, 0.00)) As QtyPurchase, ");
            SQL.AppendLine("(D.Qty-IfNull(E.Qty2, 0.00)-IfNull(F.Qty3, 0.00)+IfNull(G.Qty4, 0.00)) As Qty, ");
            SQL.AppendLine("Case When J.InventoryUomCode=IfNull(J.InventoryUomCode2, '') Then ");
            SQL.AppendLine("    (D.Qty-IfNull(E.Qty2, 0.00)-IfNull(F.Qty3, 0.00)+IfNull(G.Qty4, 0.00)) ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    InventoryUomCodeConvert12*(D.Qty-IfNull(E.Qty2, 0.00)-IfNull(F.Qty3, 0.00)+IfNull(G.Qty4, 0.00)) ");
            SQL.AppendLine("End As Qty2,");
            SQL.AppendLine("Case When J.InventoryUomCode=IfNull(J.InventoryUomCode2, '') Then ");
            SQL.AppendLine("    (D.Qty-IfNull(E.Qty2, 0.00)-IfNull(F.Qty3, 0.00)+IfNull(G.Qty4, 0.00)) ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    InventoryUomCodeConvert12*(D.Qty-IfNull(E.Qty2, 0.00)-IfNull(F.Qty3, 0.00)+IfNull(G.Qty4, 0.00)) ");
            SQL.AppendLine("End As Qty3,");
            SQL.AppendLine("'AUTOMATIC GENERATED BY KAWASAN BERIKAT APPS.', ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Inner Join TblPOHdr C On C.DocNo=@PODocNo And C.Status='A' ");
            SQL.AppendLine("Inner Join TblPODtl D On C.DocNo=D.DocNo And D.CancelInd='N' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.PODocNo=@PODocNo ");
            SQL.AppendLine("    Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") E On D.DocNo=E.DocNo And D.DNo=E.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("    From TblPOQtyCancel T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.PODocNo=@PODocNo ");
            SQL.AppendLine("    Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") F On D.DocNo=F.DocNo And D.DNo=F.DNo  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.PODocNo=T4.DocNo And T3.PODNo=T4.DNo And T4.CancelInd='N' And T4.ProcessInd<>'F' ");
            SQL.AppendLine("        And T4.DocNo=@PODocNo ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine(") G On D.DocNo=G.DocNo And D.DNo=G.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl H On D.PORequestDocNo=H.DocNo And D.PORequestDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl I On H.MaterialRequestDocNo=I.DocNo And H.MaterialRequestDNo=I.DNo ");
            SQL.AppendLine("Inner Join TblItem J On I.ItCode=J.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.ItCode, A.BatchNo, A.Source, E.CurCode, ");
            SQL.AppendLine("((A.QtyPurchase/A.Qty)*F.UPrice)-");
            SQL.AppendLine("(((A.QtyPurchase/A.Qty)*F.UPrice)*C.Discount/100)-");
            SQL.AppendLine("((1/A.Qty)*(A.QtyPurchase/C.Qty)*C.DiscountAmt)+");
            SQL.AppendLine("((1/A.Qty)*(A.QtyPurchase/C.Qty)*C.RoundingValue) ");
            SQL.AppendLine("As UPrice, ");
            SQL.AppendLine("Case When E.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=G.DocDt And CurCode1=E.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl C On A.PODocNo=C.DocNo And A.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl F On D.QtDocNo=F.DocNo And D.QtDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdHdr G On A.DocNo=G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.Status='A' And A.CancelInd='N'; ");

            SQL.AppendLine("Update TblPODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
            SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl ");
            SQL.AppendLine("        Where PODocNo=@PODocNo ");
            SQL.AppendLine("        And CancelInd='N' And IfNull(Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel ");
            SQL.AppendLine("        Where PODocNo=@PODocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("        Group By PODocNo, PODNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
            SQL.AppendLine("        From TblDOVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("            On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("            And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("            And T3.CancelInd='N' And IfNull(T3.Status, 'O')<>'C' ");
            SQL.AppendLine("            And T3.PODocNo=@PODocNo ");
            SQL.AppendLine("        Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("        Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Where A.DocNo=@PODocNo ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End; ");

            if (mIsAutoJournalActived)
            {
                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Update TblRecvVdHdr Set ");
                SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@DocNo; ");

                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Receiving Item From Vendor : ', DocNo) As JnDesc, ");
                SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
                SQL.AppendLine("CreateBy, CreateDt ");
                SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
                SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvVdDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine("    Where AcNo Is Not Null ");
                SQL.AppendLine("    Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.CmParam<String>(ref cm, "@DocType", "01");
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mKB_Server = Sm.GetParameter("KB_Server");
            mKB_Database = Sm.GetParameter("KB_Database");
            mKB_DBUser = Sm.GetParameter("KB_DBUser");
            mKB_DBPwd = Sm.GetParameter("KB_DBPwd");
            mKB_Port = Sm.GetParameter("KB_Port");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private string ConnectionString
        {
            get
            {
                return
                    string.Concat(
                    @"Server=", mKB_Server, ";Database=", mKB_Database, ";Uid=", mKB_DBUser, ";Password=", mKB_DBPwd ,";Allow User Variables=True;Connection Timeout=1200;",
                    mKB_Port.Length==0?string.Empty:string.Concat("Port=", mKB_Port, ";"));                
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        #endregion

        #endregion

        #region Class

        private class KB_Data
        {
            public string LocalDocNo { get; set; }
            public string DocDt { get; set; }
            public string PODocNo { get; set; }
        }

        private class PO
        {
            public string LocalDocNo { get; set; }
            public string DocNo { get; set; }
        }

        #endregion
    }
}
