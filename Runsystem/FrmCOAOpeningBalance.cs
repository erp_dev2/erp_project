﻿#region Update
/*
    06/07/2017 [WED] tambah filter Entity, berdasarkan parameter IsEntityMandatory
    18/12/2017 [HAR] bug Fixing validasi consolidate
    23/07/2018 [TKG] ubah proses show nomor rekening coa untuk kasus mai panjang nomor rekening beda2.
    10/08/2018 [TKG] bug fixing saat ubah amount.
    16/01/2019 [TKG] tambah fasilitas untuk memproses secara otomatis nilai opening balance
    08/02/2019 [TKG] nilai yg lebih kecil dari 0 tetap bisa disimpan.
    06/04/2020 [WED/YK] tambah fasilitas import dari CSV
    02/02/2021 [TKG/PHT] tambah profit center
    02/09/2021 [RDA/ALL] tambah fitur checkbox auto group berdasarkan parameter IsCOAOpeningBalanceUseGroupInd
    06/09/2021 [VIN/ALL] Profit Center belum di show di OBHdr
    13/09/2021 [VIN/ALL] IsDocAlreadyCreated tambah IsCOAOpeningBalanceUseProfitCenter
    19/01/2022 [RDA/ALL] checkbox auto group berdasar param IsCOAOpeningBalanceUseGroupInd sudah berfungsi ketika edit data
    21/01/2022 [MYA/PHT] Menambah nilai dari akun 9.01.01 dan 9.05 agar tergenerate saat mengimport Opening Balance
    19/04/2022 [DITA/PHT] ubah compute parent, karena sebelumnya tidak melihat natura setiap akunnya
    09/01/2023 [WED/PHT] bug fix ProcessData() disamakan dengan Trial Balance
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmCOAOpeningBalance : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmCOAOpeningBalanceFind FrmFind;
        private bool
            mIsEntityMandatory = false,
            mIsCOAOpeningBalanceUseProfitCenter = false,
            mIsCOAOpeningBalanceUseGroupInd = false,
            mIsAccountingRptUseJournalPeriod = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptTBCalculateFicoNetIncome = false,
            mIsReportingFilterByEntity = false
            ;
        private string
            mAcNoForCurrentEarning = "3.3",
            mAcNoForCurrentEarning2 = "9",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mOpeningBalanceImportFormat = string.Empty,
            mFormulaForComputeProfitLoss = string.Empty,
            mCurrentEarningFormulaType = string.Empty,
            mMaxAccountCategory = string.Empty,
            mAcNoForActiva = string.Empty,
            mAcNoForPassiva = string.Empty,
            mAcNoForCapital = string.Empty,
            mAcNoForCurrentEarning3 = string.Empty,
            mAcNoForIncome2 = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmCOAOpeningBalance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                SetFormControl(mState.View);
                GetParameter();
                if (mIsEntityMandatory) LblEntity.ForeColor = Color.Red;
                SetLueEntCode(ref LueEntCode);
                Sl.SetLueYr(LueYr, "");
                if (mIsCOAOpeningBalanceUseProfitCenter)
                {
                    LblProfitCenterCode.ForeColor = Color.Red;
                    Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);    
                }
                if (!mIsCOAOpeningBalanceUseGroupInd)
                {
                    ChkGroupInd.Visible = false;
                    BtnProcess.Dock = DockStyle.Bottom;
                    BtnImport.Dock = DockStyle.Bottom;
                }
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Account",

                        //1-5
                        "Account#",
                        "Opening Balance Amount",
                        "Allow",
                        "Amount",
                        "Previous Amount",

                        //6
                        "AcType"
                    },
                    new int[] 
                    {
                        600,
                        150, 150, 20, 150, 150, 
                        50
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 4, 5 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueYr, LueEntCode, LueProfitCenterCode, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnProcess.Enabled = BtnImport.Enabled = false;
                    Grd1.ReadOnly = true;
                    ChkGroupInd.Properties.ReadOnly = true;
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueYr, MeeRemark }, false);
                    if (mIsEntityMandatory) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueEntCode }, false);
                    if (mIsCOAOpeningBalanceUseProfitCenter) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProfitCenterCode }, false);
                    BtnProcess.Enabled = BtnImport.Enabled = true;
                    Grd1.ReadOnly = false;
                    ChkGroupInd.Properties.ReadOnly = false;
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    BtnProcess.Enabled = true;
                    Grd1.ReadOnly = false;
                    ChkGroupInd.Properties.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueYr, LueEntCode, LueProfitCenterCode, 
               MeeRemark
            });
            ChkCancelInd.Checked = false;
            ChkGroupInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 4, 5 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
           if (e.ColIndex == 2)
           {
               if(Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
               {
                   int r = e.RowIndex;
                   var Amt = Sm.GetGrdDec(Grd1, r, 2);
                   string AcType = Sm.GetGrdStr(Grd1, r, 6);
                   ComputeParent(r, Amt, AcType);

                   #region Old
                   //string Ac = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                   //string ccc = string.Empty;
                   //for (int i = 0; i < Ac.Length; i++)
                   //{
                   //    if (Ac[i] == '.')
                   //    {
                   //        ccc = Ac.Substring(0, i);
                   //        for (int y = 0; y < e.RowIndex; y++)
                   //        {
                   //            if (Sm.GetGrdStr(Grd1, y, 1) == ccc)
                   //            {
                   //                decimal QtyRow = (Sm.GetGrdDec(Grd1, e.RowIndex, 2));
                   //                decimal QtyRow2 = (Sm.GetGrdDec(Grd1, y, 2));
                   //                decimal Qty = QtyRow + QtyRow2 - Amt;
                   //                Grd1.Cells[y, 2].Value = Qty;
                   //                Grd1.Cells[y, 4].Value = Qty;
                   //                Grd1.Cells[e.RowIndex, 4].Value = QtyRow;
                   //            }
                   //        }
                   //    }
                   //}
                   #endregion
               }
               else
               {
                   Sm.StdMsg(mMsgType.Info, "You can't input amount for this account");
               }
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCOAOpeningBalanceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "You can't edit this data." +Environment.NewLine + 
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    UpdateData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsDataNotValid()) 
                return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "COAOpeningBalance", "TblCOAOpeningBalanceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOBHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 && 
                    Sm.GetGrdDec(Grd1, Row, 4)!=0m
                    ) 
                    cml.Add(SaveOBDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                (mIsCOAOpeningBalanceUseProfitCenter && Sm.IsLueEmpty(LueProfitCenterCode, "Profit center")) ||
                IsGrdEmpty() ||
                IsDocAlreadyCreated()
                ;
        }


        private bool IsDocAlreadyCreated()
        {
            var EntCode = Sm.GetLue(LueEntCode);
            var ProfitCenterCode = Sm.GetLue(LueProfitCenterCode);
            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 From TblCOAOpeningBalanceHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And CancelInd='N' ");
            if (mIsEntityMandatory) 
                SQL.AppendLine(" And EntCode Is Not Null And EntCode=@EntCode ");
            if (mIsCOAOpeningBalanceUseProfitCenter)
                SQL.AppendLine(" And ProfitCenterCode Is Not Null And ProfitCenterCode=@ProfitCenterCode ");

            SQL.AppendLine(";");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            if (mIsEntityMandatory) Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            if (mIsCOAOpeningBalanceUseProfitCenter) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);

            if (Sm.IsDataExist(cm))
            {
                var Msg = string.Empty;

                Msg = ("Year : " + Sm.GetLue(LueYr) + Environment.NewLine);
                if (EntCode.Length>0) Msg+= ("Entity : " + LueEntCode.GetColumnValue("Col2") + Environment.NewLine);
                Msg += (Environment.NewLine + "Opening balance has been created.");

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }
       
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveOBHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Insert Into TblCOAOpeningBalanceHdr(DocNo, DocDt, Yr, CancelInd, EntCode, ProfitCenterCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, 'N', @EntCode, @ProfitCenterCode, @Remark, @CreateBy, @CreateDt); ");

            SQL.AppendLine("Insert Into TblCOAOpeningBalanceDtl(DocNo, AcNo, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, AcNo, 0, @CreateBy, @CreateDt ");
            SQL.AppendLine("From TblCOA ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("Where AcNo In ( ");
                SQL.AppendLine("    Select AcNo From TblCOADtl ");
                SQL.AppendLine("    Where EntCode Is Not Null ");
                SQL.AppendLine("    And EntCode=@EntCode ");
                SQL.AppendLine(") ");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        private MySqlCommand SaveOBDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            { CommandText = "Update TblCOAOpeningBalanceDtl Set Amt=@Amt Where DocNo=@DocNo And AcNo=@AcNo;" };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditCOAOpeningBalanceHdr());
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 2) != Sm.GetGrdDec(Grd1, Row, 5)
                    )
                    cml.Add(EditCOAOpeningBalanceDtl(Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblCOAOpeningBalanceHdr Where DocNo=@DocNo And CancelInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditCOAOpeningBalanceHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblCOAOpeningBalanceHdr Set " +
                    "   CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditCOAOpeningBalanceDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblCOAOpeningBalanceDtl Set " +
                    "   Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And AcNo=@AcNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowOBHdr(DocNo);
                ShowAccount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOBHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Yr, A.CancelInd, A.EntCode, A.ProfitCenterCode, A.Remark " +
                    "From TblCOAOpeningBalanceHdr A " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Yr", "CancelInd", "EntCode", "Remark", 
 
                        //6
                        "ProfitCenterCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[6]));

                    }, true
                );
        }

        #endregion

        #region Additional Method

        private void ComputeParent(int r, decimal Amt, string AcType)
        {
            var AcNo = Sm.GetGrdStr(Grd1, r, 1);
            var AmtPrev = Sm.GetGrdDec(Grd1, r, 4);
            Grd1.Cells[r, 4].Value = Amt;
            var l = new List<string>();
            var Pos = 1;

            while (Pos > 0)
            {
                Pos = AcNo.LastIndexOf(".");
                if (Pos > 0)
                {
                    AcNo = AcNo.Substring(0, Pos);
                    l.Add(AcNo);
                }
            }

            foreach (var x in l)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 1), x))
                    {
                        if (Sm.GetGrdStr(Grd1, i, 6) != AcType)
                        {
                            Grd1.Cells[i, 2].Value = Sm.GetGrdDec(Grd1, i, 2) - (AmtPrev*-1) + (Amt*-1);
                            Grd1.Cells[i, 4].Value = Sm.GetGrdDec(Grd1, i, 2);
                            break;
                        }
                        else
                        {
                            Grd1.Cells[i, 2].Value = Sm.GetGrdDec(Grd1, i, 2) - AmtPrev + Amt;
                            Grd1.Cells[i, 4].Value = Sm.GetGrdDec(Grd1, i, 2);
                            break;
                        }
                    }
                }
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter', 'IsPLComputeFromFicoSetting','IsRptTBCalculateFicoNetIncome', 'FormulaForComputeProfitLoss', ");
            SQL.AppendLine("'AcNoForCurrentEarning', 'AcNoForCurrentEarning2', 'AcNoForCurrentEarning3','AcNoForIncome', 'AcNoForIncome2', ");
            SQL.AppendLine("'IsFicoUseMultiEntityFilter', 'IsFicoUseCOALevelFilter', 'IsAccountingRptUseJournalPeriod','IsCOAOpeningBalanceUseProfitCenter', 'PrintFicoSettingLastYearBasedOnFilterMonth', ");
            SQL.AppendLine("'IsReportingFilterByEntity', 'IsEntityMandatory', 'IsCOAAssetUseStartYr','CurrentEarningFormulaType', 'MaxAccountCategory', ");
            SQL.AppendLine("'COAAssetAcNo', 'AccountingRptStartFrom', 'AcNoForActiva','AcNoForPassiva', 'AcNoForCapital', ");
            SQL.AppendLine("'AcNoForCost', 'COAAssetStartYr', 'IsCOAOpeningBalanceUseProfitCenter', 'IsCOAOpeningBalanceUseGroupInd', 'OpeningBalanceImportFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            //case "IsFicoUseMultiEntityFilter": mIsFicoUseMultiEntityFilter = ParValue == "Y"; break;
                            //case "IsFicoUseCOALevelFilter": mIsFicoUseCOALevelFilter = ParValue == "Y"; break;
                            case "IsAccountingRptUseJournalPeriod": mIsAccountingRptUseJournalPeriod = ParValue == "Y"; break;
                            //case "IsCOAOpeningBalanceUseProfitCenter": mIsCOAOpeningBalanceUseProfitCenter = ParValue == "Y"; break;
                            //case "PrintFicoSettingLastYearBasedOnFilterMonth": mPrintFicoSettingLastYearBasedOnFilterMonth = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            //case "IsCOAAssetUseStartYr": mIsCOAAssetUseStartYr = ParValue == "Y"; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            //case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                            //case "IsPLComputeFromFicoSetting": mIsPLComputeFromFicoSetting = ParValue == "Y"; break;
                            case "IsRptTBCalculateFicoNetIncome": mIsRptTBCalculateFicoNetIncome = ParValue == "Y"; break;
                            case "IsCOAOpeningBalanceUseGroupInd": mIsCOAOpeningBalanceUseGroupInd = ParValue == "Y"; break;
                            case "IsCOAOpeningBalanceUseProfitCenter":mIsCOAOpeningBalanceUseProfitCenter = ParValue == "Y"; break;
                                
                            //string
                            case "COAAssetAcNo": mCOAAssetAcNo = ParValue; break;
                            //case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "AcNoForActiva": mAcNoForActiva = ParValue; break;
                            case "AcNoForPassiva": mAcNoForPassiva = ParValue; break;
                            case "AcNoForCapital": mAcNoForCapital = ParValue; break;
                            case "COAAssetStartYr": mCOAAssetStartYr = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "FormulaForComputeProfitLoss": mFormulaForComputeProfitLoss = ParValue; break;
                            case "CurrentEarningFormulaType": mCurrentEarningFormulaType = ParValue; break;
                            case "MaxAccountCategory": mMaxAccountCategory = ParValue; break;
                            case "AcNoForIncome2": mAcNoForIncome2 = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            case "AcNoForCurrentEarning2": mAcNoForCurrentEarning2 = ParValue; break;
                            case "AcNoForCurrentEarning3": mAcNoForCurrentEarning3 = ParValue; break;
                            case "OpeningBalanceImportFormat": mOpeningBalanceImportFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mFormulaForComputeProfitLoss.Length == 0) mFormulaForComputeProfitLoss = "1";
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'CONSOLIDATE' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 ");
            SQL.AppendLine("From TblEntity T Where ActInd='Y' ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowAccount()
        {
            int Level = 0;
            int PrevLevel = -1;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EntCode = Sm.GetLue(LueEntCode);

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (TxtDocNo.Text.Length == 0)
                    {
                        SQL.AppendLine("Select A.Level, Concat(A.AcNo, ' ', A.AcDesc) As Account, ");
                        SQL.AppendLine("A.AcNo, 0.00 As Amt, If(B.AcNo Is Null, 'N', 'Y') As Allow, A.AcType ");
                        SQL.AppendLine("From TblCOA A ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("    Select AcNo From TblCOA ");
                        SQL.AppendLine("    Where AcNo Not In (");
                        SQL.AppendLine("        Select Parent From TblCoa ");
                        SQL.AppendLine("        Where Parent Is Not Null ");
                        SQL.AppendLine("        ) ");
                        if (EntCode.Length > 0 && (mIsEntityMandatory && EntCode != "Consolidate"))
                        {
                            SQL.AppendLine("    And AcNo In ( ");
                            SQL.AppendLine("        Select AcNo From TblCOADtl ");
                            SQL.AppendLine("        Where EntCode Is Not Null ");
                            SQL.AppendLine("        And EntCode=@EntCode ");
                            SQL.AppendLine("        ) ");
                        }
                        SQL.AppendLine(") B On A.AcNo=B.AcNo ");
                        if (mOpeningBalanceImportFormat == "2")
                        {
                            SQL.AppendLine("Where Left(A.AcNo, 1) In ('1', '2', '3', '9') AND Left(A.AcNo, 2) NOT IN ('99') ");
                        }
                        else
                        {
                            SQL.AppendLine("Where Left(A.AcNo, 1) In ('1', '2', '3') ");
                        }
                        SQL.AppendLine("And A.ActInd='Y' ");
                        if (EntCode.Length > 0 && EntCode != "Consolidate") //&& (mIsEntityMandatory && EntCode != "Consolidate"))
                        {
                            SQL.AppendLine("And A.AcNo In ( ");
                            SQL.AppendLine("    Select AcNo From TblCOADtl ");
                            SQL.AppendLine("    Where EntCode Is Not Null ");
                            SQL.AppendLine("    And EntCode=@EntCode) ");
                        }
                        SQL.AppendLine("Order by A.AcNo;");

                        cm.CommandText =SQL.ToString();
                        if (EntCode.Length>0) 
                            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
                    }
                    else
                    {
                        SQL.AppendLine("Select B.Level, Concat(A.AcNo, ' ', B.AcDesc) As Account, ");
                        SQL.AppendLine("A.AcNo, A.Amt, If(C.AcNo Is Null, 'N', 'Y') As Allow, B.AcType ");
                        SQL.AppendLine("From TblCOAOpeningBalanceDtl A ");
                        SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("   Select AcNo From TblCoa ");
                        SQL.AppendLine("   Where Acno Not In (Select Parent From TblCoa Where Parent Is Not Null) ");
                        SQL.AppendLine(") C On B.Acno=C.AcNo ");
                        SQL.AppendLine("Where DocNo=@DocNo ");
                        SQL.AppendLine("Order By AcNo;");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    }
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "Level", 
                        "Account", "AcNo", "Allow", "Amt", "AcType"
                    });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = int.Parse(Sm.DrStr(dr, 0));
                            //Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            if (TxtDocNo.Text.Length == 0)
                            {
                                Grd1.Cells[Row, 2].Value = 0m;
                                Grd1.Cells[Row, 4].Value = 0m;
                                Grd1.Cells[Row, 5].Value = 0m;
                            }
                            else
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                            }
                            
                            if (Sm.GetGrdStr(Grd1, Row, 3) == "N")
                            {
                                Grd1.Rows[Row].ReadOnly = iGBool.True;
                                Grd1.Rows[Row].BackColor = Color.FromArgb(224, 224, 224);
                            }
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevLevel >= Level) ? //(PrevAcNo.Length >= Sm.DrStr(dr, 2).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevLevel = Level; //Sm.DrStr(dr, 2);
                            Row++;
                        }
                    }
                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Process Data

        private void ProcessData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") ||
              (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity"))) return;

            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 4, 5 });

            var lCOA = new List<COA>();
            var lEntityCOA = new List<EntityCOA>();
            var lCOAPLSettingHdr = new List<COAProfitLossSettingHdr>();
            var lCOAPLSettingDtl = new List<COAProfitLossSettingDtl>();
            string AcNoPL = string.Empty;
            var Yr = (int.Parse(Sm.GetLue(LueYr))-1).ToString();
            var Mth = "12";
            var EntCode = Sm.GetLue(LueEntCode);
            int Index = 0;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var StartFrom =
                    Sm.GetValue(
                    "Select Max(Yr) From TblCOAOpeningBalanceHdr Where CancelInd='N' " +
                    ((EntCode.Length > 0 && EntCode != "Consolidate") ? "And EntCode=@Param " : string.Empty) + ";",
                    EntCode);

                if (StartFrom != Yr)
                {
                    StartFrom = Yr;
                    Sm.StdMsg(mMsgType.Info, "This process will exclude the current opening balance data due to unpresence last year's opening balance.");
                }

                ShowAccount();

                Process1(ref lCOA);

                if (mFormulaForComputeProfitLoss == "2")
                {
                    GetCOAProfitLossSettingHdr(ref lCOAPLSettingHdr, Yr);
                    GetCOAProfitLossSettingDtl(ref lCOAPLSettingDtl, Yr);
                    AcNoPL = GetSelectedCOAPLSetting(ref lCOAPLSettingHdr);
                }

                if (mIsEntityMandatory && EntCode != "Consolidate")
                    Process8(ref lEntityCOA);

                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, StartFrom);
                        else
                            Process3(ref lCOA, Yr, Mth, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, string.Empty);
                        else
                            Process3(ref lCOA, Yr, Mth, string.Empty);
                    }
                    Process4(ref lCOA, Yr, Mth);
                    if (mFormulaForComputeProfitLoss != "2") Process5(ref lCOA);
                    if (mIsRptTBCalculateFicoNetIncome) Process5e(ref lCOA);
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process9(ref lCOA);

                    if (mFormulaForComputeProfitLoss == "2")
                    {
                        ComputeProfitLoss(ref lCOAPLSettingHdr, ref lCOAPLSettingDtl, ref lCOA);
                        ComputeProfitLoss2(ref lCOAPLSettingHdr, ref lCOA);
                        Process12(ref lCOA, ref lCOAPLSettingHdr, AcNoPL);
                    }

                    #region Filtered By Entity

                    if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                        if (lEntityCOA.Count > 0 && lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            
                            Index = 0;
                            if(mOpeningBalanceImportFormat == "2")
                            {
                                foreach (var x in lCOA
                                .Where(w =>
                                    lEntityCOA.Any(z => w.AcNo.Contains(z.AcNo))
                                    && (Sm.Left(w.AcNo, 1) == "1" || Sm.Left(w.AcNo, 1) == "2" || Sm.Left(w.AcNo, 1) == "3" || Sm.Left(w.AcNo, 1) == "9")
                                    )
                                .OrderBy(o => o.AcNo))
                                {
                                    for (int r = Index; r < Grd1.Rows.Count; r++)
                                    {
                                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), x.AcNo))
                                        {
                                            Grd1.Cells[r, 2].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 4].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 5].Value = x.YearToDateBalance;
                                            Index = r;
                                            break;
                                        }
                                    }
                                }
                            }
                            
                            else
                            {
                                foreach (var x in lCOA
                                .Where(w =>
                                    lEntityCOA.Any(z => w.AcNo.Contains(z.AcNo))
                                    && (Sm.Left(w.AcNo, 1) == "1" || Sm.Left(w.AcNo, 1) == "2" || Sm.Left(w.AcNo, 1) == "3")
                                    )
                                .OrderBy(o => o.AcNo))
                                {
                                    for (int r = Index; r < Grd1.Rows.Count; r++)
                                    {
                                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), x.AcNo))
                                        {
                                            Grd1.Cells[r, 2].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 4].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 5].Value = x.YearToDateBalance;
                                            Index = r;
                                            break;
                                        }
                                    }
                                }
                            }
                            

                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                    #region Not Filtered By Entity

                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();

                            Index=0;
                            if (mOpeningBalanceImportFormat == "2")
                            {
                                foreach (var x in lCOA.Where(w => Sm.Left(w.AcNo, 1) == "1" || Sm.Left(w.AcNo, 1) == "2" || Sm.Left(w.AcNo, 1) == "3" || Sm.Left(w.AcNo, 1) == "9").OrderBy(o => o.AcNo))
                                {
                                    for (int r = Index; r < Grd1.Rows.Count; r++)
                                    {
                                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), x.AcNo))
                                        {
                                            Grd1.Cells[r, 2].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 4].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 5].Value = x.YearToDateBalance;
                                            Index = r;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (var x in lCOA.Where(w => Sm.Left(w.AcNo, 1) == "1" || Sm.Left(w.AcNo, 1) == "2" || Sm.Left(w.AcNo, 1) == "3").OrderBy(o => o.AcNo))
                                {
                                    for (int r = Index; r < Grd1.Rows.Count; r++)
                                    {
                                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), x.AcNo))
                                        {
                                            Grd1.Cells[r, 2].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 4].Value = x.YearToDateBalance;
                                            Grd1.Cells[r, 5].Value = x.YearToDateBalance;
                                            Index = r;
                                            break;
                                        }
                                    }
                                }
                            }
                            

                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lCOA.Clear();
                lEntityCOA.Clear();
                lCOAPLSettingHdr.Clear();
                lCOAPLSettingDtl.Clear();

                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private string GetSelectedCOAPLSetting(ref List<COAProfitLossSettingHdr> lCOAProfitLossSettingHdr)
        {
            string AcNo = string.Empty;

            foreach (var x in lCOAProfitLossSettingHdr)
            {
                if (AcNo.Length > 0)
                    AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        private void GetCOAProfitLossSettingDtl(ref List<COAProfitLossSettingDtl> lCOAProfitLossSettingDtl, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    Select Tb1.SettingCode, Tb1.AcNo, Tb1.ProfitLossSetting, tb2.Level, tb2.parent, tb1.ProcessInd From ( ");
            SQL.AppendLine("    SELECT A.SettingCode, B.AcNo, 'plus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    INNER JOIN TblProfitLossSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' AND processind = 'Y' ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT A.SettingCode, B.AcNo, 'minus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    INNER JOIN tblprofitlosssettingdtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' AND processind = 'Y'  ");
            SQL.AppendLine("    )tb1 ");
            SQL.AppendLine("    Inner Join TblCoa tb2 On Tb1.Acno = tb2.acNo ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr",Yr);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "ProfitLossSetting", "ProcessInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSettingDtl.Add(new COAProfitLossSettingDtl()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            ProfitLossSetting = Sm.DrStr(dr, c[2]),
                            ProcessInd = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetCOAProfitLossSettingHdr(ref List<COAProfitLossSettingHdr> lCOAProfitLossSettingHdr, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    SELECT A.SettingCode, A.CurrentEarningAcno As AcNo, B.AcType");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblCoa B On A.CurrentEarningAcno = B.Acno");
            SQL.AppendLine("    WHERE A.Yr = @Yr AND A.ActInd = 'Y' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSettingHdr.Add(new COAProfitLossSettingHdr()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            ProfitLossSetting = "Y",
                            ProcessInd = "Y",
                            OpeningBalanceCAmt = 0,
                            OpeningBalanceDAmt = 0,
                            MonthToDateDAmt = 0,
                            MonthToDateCAmt = 0,
                            MonthToDateBalance = 0,
                            CurrentMonthDAmt = 0,
                            CurrentMonthCAmt = 0,
                            CurrentMonthBalance = 0,
                            YearToDateDAmt = 0,
                            YearToDateCAmt = 0,
                            YearToDateBalance = 0,
                        });
                    }
                }
                dr.Close();
            }
        }


        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                {
                    SQL.AppendLine("Left Join TblCOADtl B On A.AcNo=B.AcNo ");
                    SQL.AppendLine("And B.EntCode=@EntCode ");
                }
                SQL.AppendLine("Where A.ActInd='Y'  ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then SUM(B.Amt) Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then SUM(B.Amt) Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y'  ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(A.EntCode, '') = IfNull(@EntCode, '') ");
            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.ProfitCenterCode = @ProfitCenterCode ");
                }
                else
                {
                    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Group By C.AcNo ");
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                    SQL.AppendLine("And Left(A.DocDt, 6)<@YrMth ");
                }
            }

            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
            }
            
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("From ( ");

            #region Journal

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (Sm.GetLue(LueEntCode).Length > 0)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (Sm.GetLue(LueEntCode).Length > 0)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (Sm.GetLue(LueEntCode).Length > 0) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
            }
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("And IfNull(B.EntCode, '') = IfNull(@EntCode, '') ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
            }
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            if (mCurrentEarningFormulaType == "2")
            {
                if (
                    mAcNoForCurrentEarning.Length == 0 ||
                    mAcNoForCurrentEarning2.Length == 0 ||
                    mAcNoForActiva.Length == 0 ||
                    mAcNoForPassiva.Length == 0 ||
                    mAcNoForCapital.Length == 0
                    ) return;

                Process5c(ref lCOA);
            }
            else
            {
                if (mCurrentEarningFormulaType == "3")
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForActiva.Length == 0 ||
                        mAcNoForPassiva.Length == 0 ||
                        mAcNoForCapital.Length == 0
                        ) return;

                    Process5d(ref lCOA);
                }
                else
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForCurrentEarning2.Length == 0 ||
                        mAcNoForIncome.Length == 0 ||
                        mAcNoForCost.Length == 0
                        ) return;

                    Process5a(ref lCOA);
                    Process5b(ref lCOA);
                }
            }
        }

        private void Process5a(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5b(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
                lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0m;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    if (CurrentProfiLossParentAcNo.Length == 0) break;
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
                lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    if (CurrentProfiLossParentAcNo.Length == 0) break;
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5c(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                CurrentProfiLossIndex2 = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory > 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory > 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory > 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            var CurrentProfiLossParentIndex2 = -1;
            var CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            var f2 = true;

            //Month To Date
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].MonthToDateDAmt - lCOA[IncomeIndex].MonthToDateCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].MonthToDateCAmt - lCOA[CostIndex1].MonthToDateDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].MonthToDateCAmt - lCOA[CostIndex2].MonthToDateDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].MonthToDateCAmt - lCOA[CostIndex3].MonthToDateDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].MonthToDateCAmt - lCOA[CostIndex4].MonthToDateDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].MonthToDateDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].MonthToDateCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].CurrentMonthDAmt - lCOA[IncomeIndex].CurrentMonthCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].CurrentMonthCAmt - lCOA[CostIndex1].CurrentMonthDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].CurrentMonthCAmt - lCOA[CostIndex2].CurrentMonthDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].CurrentMonthCAmt - lCOA[CostIndex3].CurrentMonthDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].CurrentMonthCAmt - lCOA[CostIndex4].CurrentMonthDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].CurrentMonthDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].CurrentMonthCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5d(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory >= 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }


                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory >= 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory >= 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }


            //Current Month
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5e(ref List<COA> lCOA)
        {
            var lCOATemp2 = new List<COATemp>();
            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();

            //SetProfitCenter();
            string mListAcNo = "4,5,6,7,8,9.04.01.01.01.01.01.01.01.01.03,9.02,9.03";
            string mUsedAcNo = string.Empty;
            decimal mYearToDateBalance = 0m;

            var Yr = Sm.GetLue(LueYr);
            var Mth = "01";
            var StartFrom = Sm.GetLue(LueYr);

            GetAllCOA(ref lCOATemp, mListAcNo);
            if (lCOATemp.Count > 0)
            {
                foreach (var i in lCOATemp)
                {
                    if (mUsedAcNo.Length > 0) mUsedAcNo += ",";
                    mUsedAcNo += i.AcNo;
                }
            }

            if (StartFrom.Length > 0)
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, StartFrom);
                GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, StartFrom);
            }
            else
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, Yr);
                GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, string.Empty);
            }

            GetAllJournalCurrentMonth(ref lJournalCurrentMonthTemp, mUsedAcNo, Yr, Mth);

            Process5f(ref lCOATemp, ref lCOATemp2);
            Process5g(ref lCOAOpeningBalanceTemp, ref lCOATemp2);
            Process5h(ref lJournalTemp, ref lCOATemp2);
            Process5i(ref lJournalCurrentMonthTemp, ref lCOATemp2);
            Process5j(ref lCOATemp2);
            Process5k(ref lCOATemp2);
            Process5l(ref lCOATemp2, ref mYearToDateBalance);
            Process5m(ref lCOA, ref mYearToDateBalance);
        }

        private void Process5f(ref List<COA2> lCOATemp, ref List<COATemp> lCOATemp2)
        {
            foreach (var y in lCOATemp)
            {
                lCOATemp2.Add(new COATemp()
                {
                    AcNo = y.AcNo,
                    AcDesc = y.AcDesc,
                    AcType = y.AcType,
                    HasChild = y.HasChild,
                    Level = y.Level,
                    Parent = y.Parent,
                    CurrentMonthBalance = y.CurrentMonthBalance,
                    CurrentMonthCAmt = y.CurrentMonthCAmt,
                    CurrentMonthDAmt = y.CurrentMonthDAmt,
                    MonthToDateBalance = y.MonthToDateBalance,
                    MonthToDateCAmt = y.MonthToDateCAmt,
                    MonthToDateDAmt = y.MonthToDateDAmt,
                    OpeningBalanceCAmt = y.OpeningBalanceCAmt,
                    OpeningBalanceDAmt = y.OpeningBalanceDAmt,
                    YearToDateBalance = y.YearToDateBalance,
                    YearToDateCAmt = y.YearToDateCAmt,
                    YearToDateDAmt = y.YearToDateDAmt
                });
            }
        }

        private void Process5g(ref List<COAOpeningBalance2> l, ref List<COATemp> lCOATemp2) //Opening Balance
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5h(ref List<Journal2> l, ref List<COATemp> lCOATemp2) //Journal Month To Date
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5i(ref List<Journal3> l, ref List<COATemp> lCOATemp2) //Journal Current Month
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5j(ref List<COATemp> lCOATemp2)
        {
            for (var i = 0; i < lCOATemp2.Count; i++)
            {
                lCOATemp2[i].YearToDateDAmt =
                    lCOATemp2[i].OpeningBalanceDAmt +
                    lCOATemp2[i].MonthToDateDAmt +
                    lCOATemp2[i].CurrentMonthDAmt;

                lCOATemp2[i].YearToDateCAmt =
                    lCOATemp2[i].OpeningBalanceCAmt +
                    lCOATemp2[i].MonthToDateCAmt +
                    lCOATemp2[i].CurrentMonthCAmt;
            }
        }

        private void Process5k(ref List<COATemp> lCOATemp2)
        {
            for (var i = 0; i < lCOATemp2.Count; i++)
            {
                if (lCOATemp2[i].AcType == "D")
                {
                    lCOATemp2[i].MonthToDateBalance =
                        lCOATemp2[i].OpeningBalanceDAmt +
                        lCOATemp2[i].MonthToDateDAmt -
                        lCOATemp2[i].MonthToDateCAmt;

                    lCOATemp2[i].CurrentMonthBalance =
                        lCOATemp2[i].MonthToDateBalance +
                        lCOATemp2[i].CurrentMonthDAmt -
                        lCOATemp2[i].CurrentMonthCAmt;

                    lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateDAmt - lCOATemp2[i].YearToDateCAmt;
                }
                if (lCOATemp2[i].AcType == "C")
                {
                    lCOATemp2[i].MonthToDateBalance =
                        lCOATemp2[i].OpeningBalanceCAmt +
                        lCOATemp2[i].MonthToDateCAmt -
                        lCOATemp2[i].MonthToDateDAmt;

                    lCOATemp2[i].CurrentMonthBalance =
                        lCOATemp2[i].MonthToDateBalance +
                        lCOATemp2[i].CurrentMonthCAmt -
                        lCOATemp2[i].CurrentMonthDAmt;

                    lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateCAmt - lCOATemp2[i].YearToDateDAmt;
                }
            }
        }

        private void Process5l(ref List<COATemp> lCOATemp2, ref decimal YearToDateBalance)
        {
            char[] delimiters = { '+', '-', '(', ')' };
            string SQLFormula = "#4#-#5#-#6#-#7#+#8#-#9.04.01.01.01.01.01.01.01.01.03#-#9.02#-#9.03#";
            string[] ArraySQLFormula = SQLFormula.Split(delimiters);
            var lFormula = new List<Formula>();

            ArraySQLFormula = ArraySQLFormula.Where(val => val != "").ToArray();
            for (int i = 0; i < ArraySQLFormula.Count(); i++)
                lFormula.Add(new Formula() { AcNo = ArraySQLFormula[i], Amt = 0m });

            for (int ind = 0; ind < lFormula.Count; ind++)
                for (var h = 0; h < lCOATemp2.Count; h++)
                    if (lFormula[ind].AcNo == string.Concat("#", lCOATemp2[h].AcNo, "#"))
                        lFormula[ind].Amt = lCOATemp2[h].YearToDateBalance;

            for (int j = 0; j < ArraySQLFormula.Count(); j++)
            {
                for (int k = 0; k < lFormula.Count; k++)
                {
                    if (ArraySQLFormula[j].ToString() == lFormula[k].AcNo)
                    {
                        string oldS = ArraySQLFormula[j].ToString();
                        string newS = lFormula[k].Amt.ToString();
                        SQLFormula = SQLFormula.Replace(oldS, newS);
                    }
                }
            }
            decimal baltemp = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
            YearToDateBalance = baltemp;
        }

        private void Process5m(ref List<COA> lCOA, ref decimal YearToDateBalance)
        {
            if (mAcNoForCurrentEarning3.Length == 0 || mAcNoForIncome2.Length == 0) return;

            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning3) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome2) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 2) break;
                }
            }
            decimal Amt = 0m;
            Amt = YearToDateBalance;

            //Laba rugi tahun berjalan
            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            if (lCOA[CurrentProfiLossIndex].AcType == "D")
            {
                if (Amt < 0)
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt * -1;
                }
                else
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
                }
            }
            else
            {
                if (Amt < 0)
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
                }
                else
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
                }
            }

            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        if (lCOA[CurrentProfiLossIndex].AcType == "D")
                        {
                            if (Amt < 0)
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = Amt * -1;
                            }
                            else
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = 0m;
                            }
                        }
                        else
                        {
                            if (Amt < 0)
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = 0;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = 0m;
                            }
                            else
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = Amt;
                            }
                        }

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Laba rugi - penghasilan
            var IncomeParentIndex = -1;
            var IncomeParentAcNo = lCOA[IncomeIndex].Parent;
            f = true;

            if (lCOA[IncomeIndex].AcType == "D")
            {
                if (Amt < 0)
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = 0m;
                    lCOA[IncomeIndex].MonthToDateCAmt = Amt * -1;
                    lCOA[IncomeIndex].CurrentMonthDAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthCAmt = Amt * -1;
                }
                else
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = Amt;
                    lCOA[IncomeIndex].MonthToDateCAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthDAmt = Amt;
                    lCOA[IncomeIndex].CurrentMonthCAmt = 0m;
                }
            }
            else
            {
                if (Amt < 0)
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = Amt * -1;
                    lCOA[IncomeIndex].MonthToDateCAmt = 0;
                    lCOA[IncomeIndex].CurrentMonthDAmt = Amt * -1;
                    lCOA[IncomeIndex].CurrentMonthCAmt = 0m;
                }
                else
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = 0m;
                    lCOA[IncomeIndex].MonthToDateCAmt = Amt;
                    lCOA[IncomeIndex].CurrentMonthDAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthCAmt = Amt;
                }
            }

            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, IncomeParentAcNo) == 0)
                    {
                        IncomeParentIndex = i;
                        if (lCOA[IncomeIndex].AcType == "D")
                        {
                            if (Amt < 0)
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = 0m;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = Amt * -1;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = Amt * -1;
                            }
                            else
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = Amt;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = Amt;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = 0m;
                            }
                        }
                        else
                        {
                            if (Amt < 0)
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = Amt * -1;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = 0;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = Amt * -1;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = 0m;
                            }
                            else
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = 0m;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = Amt;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = Amt;
                            }
                        }

                        IncomeParentAcNo = lCOA[IncomeParentIndex].Parent;
                        f = IncomeParentAcNo.Length > 0;
                        break;
                    }
                }
            }
        }

        private void GetAllCOA(ref List<COA2> l, string ListAcNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;
            string[] AcNo = ListAcNo.Split(',');

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                {
                    SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo ");
                    SQL.AppendLine("And B.EntCode=@EntCode ");

                    if (mIsReportingFilterByEntity)
                    {
                        SQL.AppendLine("And B.EntCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And ( ");
                for (int x = 0; x < AcNo.Count(); ++x)
                {
                    SQL.AppendLine("(A.AcNo Like '" + AcNo[x] + "%') ");
                    if (x != AcNo.Count() - 1)
                        SQL.AppendLine(" Or ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA2()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournal(ref List<Journal2> l, string ListAcNo, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And B.EntCode=@EntCode ");

                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }

            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCOAOpeningBalance(ref List<COAOpeningBalance2> l, string ListAcNo, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");

            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And A.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And A.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.ProfitCenterCode = @ProfitCenterCode ");
                }
                else
                {
                    SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAOpeningBalance2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournalCurrentMonth(ref List<Journal3> l, string ListAcNo, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And B.EntCode=@EntCode ");

                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth ");
            if (mIsCOAOpeningBalanceUseProfitCenter)
            {
                if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                {
                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode = @ProfitCenterCode ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In (");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )))) ");
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal3()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                {
                    int MaxAccountCategory = int.Parse(mMaxAccountCategory);

                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And U.EntCode = @EntCode ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt -
                        //tambahan berdasarkan OpeningBalanceCAmt bit.ly/31WjiQS
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt -
                        //tambahan MonthToDateCAmt berdasarkan bit.ly/31WjiQS

                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateCAmt -
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }

                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                {
                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
            }
        }

        private void ComputeProfitLoss(ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, ref List<COAProfitLossSettingDtl> lCOAPLSettingDtl, ref List<COA> lCOA)
        {

            foreach (var h in lCOAPLSettingHdr)
            {
                decimal penambahMTD = 0;
                decimal pengurangMTD = 0;
                decimal penambahCMD = 0;
                decimal pengurangCMD = 0;
                decimal penambahYTD = 0;
                decimal pengurangYTD = 0;

                foreach (var d in lCOAPLSettingDtl.OrderBy(d2 => d2.SettingCode).ThenBy(d3 => d3.ProfitLossSetting))
                {
                    if (h.SettingCode == d.SettingCode)
                    {
                        foreach (var c in lCOA)
                        {
                            if (d.AcNo == c.AcNo)
                            {
                                if (d.ProfitLossSetting == "plus")
                                {
                                    penambahMTD += c.MonthToDateBalance;
                                    penambahCMD += c.CurrentMonthBalance;
                                    penambahYTD += c.YearToDateBalance;
                                }
                                else
                                {
                                    pengurangMTD += c.MonthToDateBalance;
                                    pengurangCMD += c.CurrentMonthBalance;
                                    pengurangYTD += c.YearToDateBalance;
                                }
                            }
                        }
                    }
                }
                h.OpeningBalanceCAmt = 0m;
                h.OpeningBalanceDAmt = 0m;
                h.MonthToDateBalance = penambahMTD - pengurangMTD;
                h.CurrentMonthBalance = penambahCMD - pengurangCMD;
                h.YearToDateBalance = penambahYTD - pengurangYTD;

                if (h.AcType == "D")
                {
                    h.CurrentMonthDAmt = h.CurrentMonthBalance - h.MonthToDateBalance > 0m ? h.CurrentMonthBalance - h.MonthToDateBalance : 0m;
                    h.CurrentMonthCAmt = h.CurrentMonthBalance - h.MonthToDateBalance < 0m ? (h.CurrentMonthBalance - h.MonthToDateBalance) * -1 : 0m;
                    h.MonthToDateDAmt = h.MonthToDateBalance > 0m ? h.MonthToDateBalance : 0m;
                    h.MonthToDateCAmt = h.MonthToDateBalance < 0m ? h.MonthToDateBalance * -1 : 0m;
                    h.YearToDateDAmt = h.YearToDateBalance > 0m ? h.YearToDateBalance : 0m;
                    h.YearToDateCAmt = h.YearToDateBalance < 0m ? h.YearToDateBalance * -1 : 0m;
                }
                else
                {
                    h.CurrentMonthCAmt = h.CurrentMonthBalance - h.MonthToDateBalance > 0m ? h.CurrentMonthBalance - h.MonthToDateBalance : 0m;
                    h.CurrentMonthDAmt = h.CurrentMonthBalance - h.MonthToDateBalance < 0m ? (h.CurrentMonthBalance - h.MonthToDateBalance) * -1 : 0m;
                    h.MonthToDateCAmt = h.MonthToDateBalance > 0m ? h.MonthToDateBalance : 0m;
                    h.MonthToDateDAmt = h.MonthToDateBalance < 0m ? h.MonthToDateBalance * -1 : 0m;
                    h.YearToDateCAmt = h.YearToDateBalance > 0m ? h.YearToDateBalance : 0m;
                    h.YearToDateDAmt = h.YearToDateBalance < 0m ? h.YearToDateBalance * -1 : 0m;
                }


            }
        }

        private void ComputeProfitLoss2(ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, ref List<COA> lCOA)
        {

            foreach (var h in lCOAPLSettingHdr.OrderBy(h2 => h2.SettingCode).ThenBy(h3 => h3.AcNo))
            {
                foreach (var c in lCOA.OrderBy(c2 => c2.AcNo))
                {
                    if (h.AcNo == c.AcNo)
                    {
                        //yg ditarik hanya year to date saja
                        //c.OpeningBalanceCAmt = h.OpeningBalanceCAmt;
                        //c.OpeningBalanceDAmt = h.OpeningBalanceDAmt;
                        c.MonthToDateDAmt = h.MonthToDateDAmt;
                        c.MonthToDateCAmt = h.MonthToDateCAmt;
                        c.MonthToDateBalance = h.MonthToDateBalance;
                        c.CurrentMonthDAmt = h.CurrentMonthDAmt;
                        c.CurrentMonthCAmt = h.CurrentMonthCAmt;
                        c.CurrentMonthBalance = h.CurrentMonthBalance;
                        c.YearToDateDAmt = h.YearToDateDAmt;
                        c.YearToDateCAmt = h.YearToDateCAmt;
                        c.YearToDateBalance = h.YearToDateBalance;
                    }
                }
            }

        }

        private void Process11(ref List<COA> lCOA, ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr)
        {
            if (lCOAPLSettingHdr.Count > 0)
            {
                lCOAPLSettingHdr.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lCOAPLSettingHdr.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lCOAPLSettingHdr[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lCOAPLSettingHdr[i].AcNo, lCOA[j].AcNo) ||
                            lCOAPLSettingHdr[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lCOAPLSettingHdr[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            //lCOA[j].OpeningBalanceDAmt += lCOAPLSettingHdr[i].OpeningBalanceDAmt;
                            //lCOA[j].OpeningBalanceCAmt += lCOAPLSettingHdr[i].OpeningBalanceCAmt;


                            //lCOA[j].MonthToDateDAmt += lCOAPLSettingHdr[i].MonthToDateDAmt;
                            //lCOA[j].MonthToDateCAmt += lCOAPLSettingHdr[i].MonthToDateCAmt;
                            //lCOA[j].MonthToDateBalance += lCOAPLSettingHdr[i].MonthToDateBalance;

                            //lCOA[j].CurrentMonthDAmt += lCOAPLSettingHdr[i].CurrentMonthDAmt;
                            //lCOA[j].CurrentMonthCAmt += lCOAPLSettingHdr[i].CurrentMonthCAmt;
                            //lCOA[j].CurrentMonthBalance += lCOAPLSettingHdr[i].CurrentMonthBalance;


                            lCOA[j].YearToDateDAmt += lCOAPLSettingHdr[i].YearToDateDAmt;
                            lCOA[j].YearToDateCAmt += lCOAPLSettingHdr[i].YearToDateCAmt;
                            lCOA[j].YearToDateBalance += lCOAPLSettingHdr[i].YearToDateBalance;

                            if (string.Compare(lCOA[j].AcNo, lCOAPLSettingHdr[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
        }

        private void Process12(ref List<COA> lCOA, ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, string GetSelectedCOAPLSetting)
        {
            foreach (var x in lCOAPLSettingHdr.OrderBy(w => w.AcNo))
            {
                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, GetSelectedCOAPLSetting))))
                {
                    if (x.AcNo.Count(y => y == '.') != i.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(i.AcNo))
                    {
                        //i.OpeningBalanceDAmt = 0m;
                        //i.OpeningBalanceCAmt = 0m;

                        i.MonthToDateDAmt = 0m;
                        i.MonthToDateCAmt = 0m;
                        i.MonthToDateBalance = 0m;

                        i.CurrentMonthDAmt = 0m;
                        i.CurrentMonthCAmt = 0m;
                        i.CurrentMonthBalance = 0m;


                        i.YearToDateDAmt = 0m;
                        i.YearToDateCAmt = 0m;
                        i.YearToDateBalance = 0m;


                    }
                }

                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && !w.HasChild))
                {
                    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, GetSelectedCOAPLSetting))))
                    {
                        if (x.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(j.AcNo))
                        {
                            if (
                                i.AcNo.Count(y => y == '.') == j.AcNo.Count(y => y == '.') && Sm.CompareStr(i.AcNo, j.AcNo) ||
                                i.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && i.AcNo.StartsWith(j.AcNo)
                            )
                            {
                                //j.OpeningBalanceDAmt += i.OpeningBalanceDAmt; 
                                //j.OpeningBalanceCAmt += i.OpeningBalanceCAmt;

                                j.MonthToDateDAmt += i.MonthToDateDAmt;
                                j.MonthToDateCAmt += i.MonthToDateCAmt;
                                j.MonthToDateBalance += i.MonthToDateBalance;

                                j.CurrentMonthDAmt += i.CurrentMonthDAmt;
                                j.CurrentMonthCAmt += i.CurrentMonthCAmt;
                                j.CurrentMonthBalance += i.CurrentMonthBalance;

                                j.YearToDateDAmt += i.YearToDateDAmt;
                                j.YearToDateCAmt += i.YearToDateCAmt;

                                if (j.AcType == i.AcType) j.YearToDateBalance += i.YearToDateBalance;
                                else j.YearToDateBalance -= i.YearToDateBalance;

                                if (string.Compare(j.AcNo, i.AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }


            }
        }

        #endregion

        #region Import CSV

        private void ProcessImport()
        {
            string FileName = string.Empty, AcType = string.Empty;
            var l = new List<COACSV>();

            FileName = OpenFileDialog();
            if (FileName.Length == 0 || FileName == "openFileDialog1") return;
            Sm.ClearGrd(Grd1, true);
            ShowAccount();
            ReadFileToList(ref l, FileName);

            if (l.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 3) == "Y")
                    {
                        foreach (var x in l
                            .OrderBy(y => y.AcNo)
                            .Where(z => z.AcNo == Sm.GetGrdStr(Grd1, i, 1))
                            )
                        {
                            AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param", x.AcNo);
                            Grd1.Cells[i, 2].Value = x.Amt;
                            ComputeParent(i, x.Amt, AcType);
                            break;
                        }
                    }
                }
            }

            l.Clear();
        }

        private string OpenFileDialog()
        {
            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            OD.ShowDialog();
            
            return OD.FileName;
        }

        private void ReadFileToList(ref List<COACSV> l, string FileName)
        {
            bool IsFirst = true;

            using (var rd = new StreamReader(FileName))
            {
                int mRow = 0;

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 2)
                    {
                        if (splits.Count() > 2)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                        }
                        else if (splits.Count() < 2)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data.");
                        }

                        l.Clear();
                        return;
                    }
                    else
                    {
                        if (IsFirst) IsFirst = false;
                        else
                        {
                            mRow += 1;

                            var arr = splits.ToArray();
                            if (arr[0].Trim().Length > 0)
                            {
                                l.Add(new COACSV()
                                {
                                    AcNo = splits[0].Trim(),
                                    Amt = Decimal.Parse(splits[1].Trim())
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.Warning, "No data at row#" + mRow.ToString());
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ProcessData();
            if (mIsCOAOpeningBalanceUseGroupInd && ChkGroupInd.Checked)
                Grd1.Rows.CollapseAll();
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (IsGrdEmpty()) return;
                ProcessImport();
            }
        }

        private void BtnShowAc_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueYr, "Year") && !(mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity"))) 
                ShowAccount();
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.ClearGrd(Grd1, true);
        }

        private void ChkGroupInd_CheckedChanged(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count == 1 && ChkGroupInd.Checked == true)
            {
                ChkGroupInd.Checked = false;
                return;
            }

            if (ChkGroupInd.Checked)
                Grd1.Rows.CollapseAll();
            else
                Grd1.Rows.ExpandAll();
        }

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COACSV
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class COAParent
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
        }

        private class COAProfitLossSettingDtl
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }

        }

        private class COAProfitLossSettingHdr
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }
            public string AcType { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }

        }

        private class COATemp
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class COA2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Formula
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class Journal2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal3
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAOpeningBalance2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion


    }
}
