﻿namespace RunSystem
{
    partial class FrmGoalsSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGoalsSetting));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtTotalGoalsSetting = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.LueDirectorate = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnTopEvaluator = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEvaluator = new DevExpress.XtraEditors.SimpleButton();
            this.LblDirectorate = new System.Windows.Forms.Label();
            this.TxtTopEvaluator = new DevExpress.XtraEditors.TextEdit();
            this.LblTopEvaluator = new System.Windows.Forms.Label();
            this.TxtEvaluator = new DevExpress.XtraEditors.TextEdit();
            this.LblEvaluator = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.LblStatus = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtPICName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtPICCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnPIC = new DevExpress.XtraEditors.SimpleButton();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtGoalSetting = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.Tpg = new System.Windows.Forms.TabControl();
            this.TpgFinancialPerpective = new System.Windows.Forms.TabPage();
            this.LuePosition1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtTotalFinancial = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TpgCustomerPerspective = new System.Windows.Forms.TabPage();
            this.DteDeadLine = new DevExpress.XtraEditors.DateEdit();
            this.LuePosition2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtTotalCustomer = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TpgInternalPerspective = new System.Windows.Forms.TabPage();
            this.DteDeadLine2 = new DevExpress.XtraEditors.DateEdit();
            this.LuePosition3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtTotalInternalProcess = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TpgLearningPerspective = new System.Windows.Forms.TabPage();
            this.DteDeadLine3 = new DevExpress.XtraEditors.DateEdit();
            this.LuePosition4 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtTotalLearning = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DteDeadLine0 = new DevExpress.XtraEditors.DateEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalGoalsSetting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDirectorate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTopEvaluator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEvaluator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoalSetting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.Tpg.SuspendLayout();
            this.TpgFinancialPerpective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalFinancial.Properties)).BeginInit();
            this.TpgCustomerPerspective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalCustomer.Properties)).BeginInit();
            this.TpgInternalPerspective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalInternalProcess.Properties)).BeginInit();
            this.TpgLearningPerspective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLearning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine0.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine0.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(826, 0);
            this.panel1.Size = new System.Drawing.Size(70, 427);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tpg);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(826, 427);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.LblStatus);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.TxtPICName);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.TxtPICCode);
            this.panel3.Controls.Add(this.BtnPIC);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtGoalSetting);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.ChkActiveInd);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(826, 168);
            this.panel3.TabIndex = 9;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtTotalGoalsSetting);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.LueDirectorate);
            this.panel4.Controls.Add(this.BtnTopEvaluator);
            this.panel4.Controls.Add(this.BtnEvaluator);
            this.panel4.Controls.Add(this.LblDirectorate);
            this.panel4.Controls.Add(this.TxtTopEvaluator);
            this.panel4.Controls.Add(this.LblTopEvaluator);
            this.panel4.Controls.Add(this.TxtEvaluator);
            this.panel4.Controls.Add(this.LblEvaluator);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(473, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(349, 164);
            this.panel4.TabIndex = 83;
            // 
            // TxtTotalGoalsSetting
            // 
            this.TxtTotalGoalsSetting.EnterMoveNextControl = true;
            this.TxtTotalGoalsSetting.Location = new System.Drawing.Point(89, 131);
            this.TxtTotalGoalsSetting.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalGoalsSetting.Name = "TxtTotalGoalsSetting";
            this.TxtTotalGoalsSetting.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalGoalsSetting.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalGoalsSetting.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalGoalsSetting.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalGoalsSetting.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalGoalsSetting.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalGoalsSetting.Properties.MaxLength = 16;
            this.TxtTotalGoalsSetting.Properties.ReadOnly = true;
            this.TxtTotalGoalsSetting.Size = new System.Drawing.Size(206, 20);
            this.TxtTotalGoalsSetting.TabIndex = 81;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(51, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 14);
            this.label11.TabIndex = 80;
            this.label11.Text = "Total";
            // 
            // LueDirectorate
            // 
            this.LueDirectorate.EnterMoveNextControl = true;
            this.LueDirectorate.Location = new System.Drawing.Point(89, 46);
            this.LueDirectorate.Margin = new System.Windows.Forms.Padding(5);
            this.LueDirectorate.Name = "LueDirectorate";
            this.LueDirectorate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirectorate.Properties.Appearance.Options.UseFont = true;
            this.LueDirectorate.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirectorate.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDirectorate.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirectorate.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDirectorate.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirectorate.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDirectorate.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirectorate.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDirectorate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDirectorate.Properties.DropDownRows = 30;
            this.LueDirectorate.Properties.NullText = "[Empty]";
            this.LueDirectorate.Properties.PopupWidth = 300;
            this.LueDirectorate.Size = new System.Drawing.Size(206, 20);
            this.LueDirectorate.TabIndex = 79;
            this.LueDirectorate.ToolTip = "F4 : Show/hide list";
            this.LueDirectorate.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // BtnTopEvaluator
            // 
            this.BtnTopEvaluator.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnTopEvaluator.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnTopEvaluator.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTopEvaluator.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnTopEvaluator.Appearance.Options.UseBackColor = true;
            this.BtnTopEvaluator.Appearance.Options.UseFont = true;
            this.BtnTopEvaluator.Appearance.Options.UseForeColor = true;
            this.BtnTopEvaluator.Appearance.Options.UseTextOptions = true;
            this.BtnTopEvaluator.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnTopEvaluator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnTopEvaluator.Image = ((System.Drawing.Image)(resources.GetObject("BtnTopEvaluator.Image")));
            this.BtnTopEvaluator.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnTopEvaluator.Location = new System.Drawing.Point(298, 27);
            this.BtnTopEvaluator.Name = "BtnTopEvaluator";
            this.BtnTopEvaluator.Size = new System.Drawing.Size(24, 21);
            this.BtnTopEvaluator.TabIndex = 76;
            this.BtnTopEvaluator.ToolTip = "Add Top Evaluator";
            this.BtnTopEvaluator.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnTopEvaluator.ToolTipTitle = "Run System";
            this.BtnTopEvaluator.Click += new System.EventHandler(this.BtnTopEvaluator_Click);
            // 
            // BtnEvaluator
            // 
            this.BtnEvaluator.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEvaluator.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEvaluator.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEvaluator.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEvaluator.Appearance.Options.UseBackColor = true;
            this.BtnEvaluator.Appearance.Options.UseFont = true;
            this.BtnEvaluator.Appearance.Options.UseForeColor = true;
            this.BtnEvaluator.Appearance.Options.UseTextOptions = true;
            this.BtnEvaluator.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEvaluator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEvaluator.Image = ((System.Drawing.Image)(resources.GetObject("BtnEvaluator.Image")));
            this.BtnEvaluator.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEvaluator.Location = new System.Drawing.Point(298, 3);
            this.BtnEvaluator.Name = "BtnEvaluator";
            this.BtnEvaluator.Size = new System.Drawing.Size(24, 21);
            this.BtnEvaluator.TabIndex = 73;
            this.BtnEvaluator.ToolTip = "Add Evaluator";
            this.BtnEvaluator.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEvaluator.ToolTipTitle = "Run System";
            this.BtnEvaluator.Click += new System.EventHandler(this.BtnEvaluator_Click);
            // 
            // LblDirectorate
            // 
            this.LblDirectorate.AutoSize = true;
            this.LblDirectorate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDirectorate.ForeColor = System.Drawing.Color.Red;
            this.LblDirectorate.Location = new System.Drawing.Point(18, 48);
            this.LblDirectorate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDirectorate.Name = "LblDirectorate";
            this.LblDirectorate.Size = new System.Drawing.Size(68, 14);
            this.LblDirectorate.TabIndex = 78;
            this.LblDirectorate.Text = "Directorate";
            this.LblDirectorate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTopEvaluator
            // 
            this.TxtTopEvaluator.EnterMoveNextControl = true;
            this.TxtTopEvaluator.Location = new System.Drawing.Point(89, 25);
            this.TxtTopEvaluator.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTopEvaluator.Name = "TxtTopEvaluator";
            this.TxtTopEvaluator.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTopEvaluator.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTopEvaluator.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTopEvaluator.Properties.Appearance.Options.UseFont = true;
            this.TxtTopEvaluator.Properties.MaxLength = 250;
            this.TxtTopEvaluator.Size = new System.Drawing.Size(206, 20);
            this.TxtTopEvaluator.TabIndex = 77;
            // 
            // LblTopEvaluator
            // 
            this.LblTopEvaluator.AutoSize = true;
            this.LblTopEvaluator.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTopEvaluator.ForeColor = System.Drawing.Color.Black;
            this.LblTopEvaluator.Location = new System.Drawing.Point(19, 28);
            this.LblTopEvaluator.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTopEvaluator.Name = "LblTopEvaluator";
            this.LblTopEvaluator.Size = new System.Drawing.Size(68, 14);
            this.LblTopEvaluator.TabIndex = 75;
            this.LblTopEvaluator.Text = "Appraiser 2";
            this.LblTopEvaluator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEvaluator
            // 
            this.TxtEvaluator.EnterMoveNextControl = true;
            this.TxtEvaluator.Location = new System.Drawing.Point(89, 4);
            this.TxtEvaluator.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEvaluator.Name = "TxtEvaluator";
            this.TxtEvaluator.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEvaluator.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEvaluator.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEvaluator.Properties.Appearance.Options.UseFont = true;
            this.TxtEvaluator.Properties.MaxLength = 250;
            this.TxtEvaluator.Size = new System.Drawing.Size(206, 20);
            this.TxtEvaluator.TabIndex = 74;
            // 
            // LblEvaluator
            // 
            this.LblEvaluator.AutoSize = true;
            this.LblEvaluator.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEvaluator.ForeColor = System.Drawing.Color.Red;
            this.LblEvaluator.Location = new System.Drawing.Point(19, 7);
            this.LblEvaluator.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEvaluator.Name = "LblEvaluator";
            this.LblEvaluator.Size = new System.Drawing.Size(68, 14);
            this.LblEvaluator.TabIndex = 72;
            this.LblEvaluator.Text = "Appraiser 1";
            this.LblEvaluator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(126, 67);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 250;
            this.TxtStatus.Size = new System.Drawing.Size(286, 20);
            this.TxtStatus.TabIndex = 64;
            // 
            // LblStatus
            // 
            this.LblStatus.AutoSize = true;
            this.LblStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblStatus.ForeColor = System.Drawing.Color.Black;
            this.LblStatus.Location = new System.Drawing.Point(75, 70);
            this.LblStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(42, 14);
            this.LblStatus.TabIndex = 63;
            this.LblStatus.Text = "Status";
            this.LblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(75, 134);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 70;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(126, 131);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(286, 20);
            this.MeeRemark.TabIndex = 71;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtPICName
            // 
            this.TxtPICName.EnterMoveNextControl = true;
            this.TxtPICName.Location = new System.Drawing.Point(126, 109);
            this.TxtPICName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICName.Name = "TxtPICName";
            this.TxtPICName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICName.Properties.Appearance.Options.UseFont = true;
            this.TxtPICName.Properties.MaxLength = 250;
            this.TxtPICName.Size = new System.Drawing.Size(286, 20);
            this.TxtPICName.TabIndex = 69;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(29, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 14);
            this.label3.TabIndex = 68;
            this.label3.Text = "Employee Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPICCode
            // 
            this.TxtPICCode.EnterMoveNextControl = true;
            this.TxtPICCode.Location = new System.Drawing.Point(126, 88);
            this.TxtPICCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICCode.Name = "TxtPICCode";
            this.TxtPICCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPICCode.Properties.MaxLength = 250;
            this.TxtPICCode.Size = new System.Drawing.Size(286, 20);
            this.TxtPICCode.TabIndex = 67;
            // 
            // BtnPIC
            // 
            this.BtnPIC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPIC.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPIC.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPIC.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPIC.Appearance.Options.UseBackColor = true;
            this.BtnPIC.Appearance.Options.UseFont = true;
            this.BtnPIC.Appearance.Options.UseForeColor = true;
            this.BtnPIC.Appearance.Options.UseTextOptions = true;
            this.BtnPIC.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPIC.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPIC.Image = ((System.Drawing.Image)(resources.GetObject("BtnPIC.Image")));
            this.BtnPIC.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPIC.Location = new System.Drawing.Point(417, 88);
            this.BtnPIC.Name = "BtnPIC";
            this.BtnPIC.Size = new System.Drawing.Size(24, 21);
            this.BtnPIC.TabIndex = 66;
            this.BtnPIC.ToolTip = "Add PIC";
            this.BtnPIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPIC.ToolTipTitle = "Run System";
            this.BtnPIC.Click += new System.EventHandler(this.BtnPIC_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(32, 91);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 14);
            this.label2.TabIndex = 65;
            this.label2.Text = "Employee Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGoalSetting
            // 
            this.TxtGoalSetting.EnterMoveNextControl = true;
            this.TxtGoalSetting.Location = new System.Drawing.Point(126, 46);
            this.TxtGoalSetting.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGoalSetting.Name = "TxtGoalSetting";
            this.TxtGoalSetting.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGoalSetting.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGoalSetting.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGoalSetting.Properties.Appearance.Options.UseFont = true;
            this.TxtGoalSetting.Properties.MaxLength = 250;
            this.TxtGoalSetting.Size = new System.Drawing.Size(286, 20);
            this.TxtGoalSetting.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(48, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 14);
            this.label4.TabIndex = 61;
            this.label4.Text = "Goal Setting";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(416, 4);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(73, 22);
            this.ChkActiveInd.TabIndex = 58;
            this.ChkActiveInd.CheckedChanged += new System.EventHandler(this.ChkActiveInd_CheckedChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(126, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 60;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(84, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 14);
            this.label5.TabIndex = 59;
            this.label5.Text = "Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(126, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(286, 20);
            this.TxtDocNo.TabIndex = 57;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(48, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 14);
            this.label8.TabIndex = 56;
            this.label8.Text = "Document#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tpg
            // 
            this.Tpg.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.Tpg.Controls.Add(this.TpgFinancialPerpective);
            this.Tpg.Controls.Add(this.TpgCustomerPerspective);
            this.Tpg.Controls.Add(this.TpgInternalPerspective);
            this.Tpg.Controls.Add(this.TpgLearningPerspective);
            this.Tpg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tpg.Location = new System.Drawing.Point(0, 168);
            this.Tpg.Multiline = true;
            this.Tpg.Name = "Tpg";
            this.Tpg.SelectedIndex = 0;
            this.Tpg.ShowToolTips = true;
            this.Tpg.Size = new System.Drawing.Size(826, 259);
            this.Tpg.TabIndex = 26;
            this.Tpg.Leave += new System.EventHandler(this.LuePosition3_Leave);
            // 
            // TpgFinancialPerpective
            // 
            this.TpgFinancialPerpective.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgFinancialPerpective.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgFinancialPerpective.Controls.Add(this.DteDeadLine0);
            this.TpgFinancialPerpective.Controls.Add(this.LuePosition1);
            this.TpgFinancialPerpective.Controls.Add(this.Grd1);
            this.TpgFinancialPerpective.Controls.Add(this.panel10);
            this.TpgFinancialPerpective.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgFinancialPerpective.Location = new System.Drawing.Point(4, 26);
            this.TpgFinancialPerpective.Name = "TpgFinancialPerpective";
            this.TpgFinancialPerpective.Size = new System.Drawing.Size(818, 229);
            this.TpgFinancialPerpective.TabIndex = 0;
            this.TpgFinancialPerpective.Text = "Financial Perspective";
            this.TpgFinancialPerpective.UseVisualStyleBackColor = true;
            // 
            // LuePosition1
            // 
            this.LuePosition1.EnterMoveNextControl = true;
            this.LuePosition1.Location = new System.Drawing.Point(237, 116);
            this.LuePosition1.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosition1.Name = "LuePosition1";
            this.LuePosition1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition1.Properties.Appearance.Options.UseFont = true;
            this.LuePosition1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosition1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosition1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosition1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosition1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosition1.Properties.DropDownRows = 20;
            this.LuePosition1.Properties.NullText = "[Empty]";
            this.LuePosition1.Properties.PopupWidth = 500;
            this.LuePosition1.Size = new System.Drawing.Size(286, 20);
            this.LuePosition1.TabIndex = 32;
            this.LuePosition1.ToolTip = "F4 : Show/hide list";
            this.LuePosition1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosition1.EditValueChanged += new System.EventHandler(this.LuePosition1_EditValueChanged);
            this.LuePosition1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePosition1_KeyDown);
            this.LuePosition1.Leave += new System.EventHandler(this.LuePosition1_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 32);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(814, 193);
            this.Grd1.TabIndex = 26;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.TxtTotalFinancial);
            this.panel10.Controls.Add(this.label1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(814, 32);
            this.panel10.TabIndex = 48;
            // 
            // TxtTotalFinancial
            // 
            this.TxtTotalFinancial.EnterMoveNextControl = true;
            this.TxtTotalFinancial.Location = new System.Drawing.Point(47, 6);
            this.TxtTotalFinancial.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalFinancial.Name = "TxtTotalFinancial";
            this.TxtTotalFinancial.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalFinancial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalFinancial.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalFinancial.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalFinancial.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalFinancial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalFinancial.Properties.MaxLength = 16;
            this.TxtTotalFinancial.Properties.ReadOnly = true;
            this.TxtTotalFinancial.Size = new System.Drawing.Size(120, 20);
            this.TxtTotalFinancial.TabIndex = 83;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 82;
            this.label1.Text = "Total";
            // 
            // TpgCustomerPerspective
            // 
            this.TpgCustomerPerspective.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgCustomerPerspective.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgCustomerPerspective.Controls.Add(this.DteDeadLine);
            this.TpgCustomerPerspective.Controls.Add(this.LuePosition2);
            this.TpgCustomerPerspective.Controls.Add(this.Grd2);
            this.TpgCustomerPerspective.Controls.Add(this.panel5);
            this.TpgCustomerPerspective.Location = new System.Drawing.Point(4, 26);
            this.TpgCustomerPerspective.Name = "TpgCustomerPerspective";
            this.TpgCustomerPerspective.Size = new System.Drawing.Size(818, 229);
            this.TpgCustomerPerspective.TabIndex = 1;
            this.TpgCustomerPerspective.Text = "Customer Perspective";
            this.TpgCustomerPerspective.UseVisualStyleBackColor = true;
            // 
            // DteDeadLine
            // 
            this.DteDeadLine.EditValue = null;
            this.DteDeadLine.EnterMoveNextControl = true;
            this.DteDeadLine.Location = new System.Drawing.Point(345, 102);
            this.DteDeadLine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeadLine.Name = "DteDeadLine";
            this.DteDeadLine.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine.Properties.Appearance.Options.UseFont = true;
            this.DteDeadLine.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeadLine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeadLine.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeadLine.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeadLine.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeadLine.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeadLine.Size = new System.Drawing.Size(125, 20);
            this.DteDeadLine.TabIndex = 60;
            this.DteDeadLine.Visible = false;
            this.DteDeadLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeadLine_KeyDown);
            this.DteDeadLine.Leave += new System.EventHandler(this.DteDeadLine_Leave);
            // 
            // LuePosition2
            // 
            this.LuePosition2.EnterMoveNextControl = true;
            this.LuePosition2.Location = new System.Drawing.Point(237, 116);
            this.LuePosition2.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosition2.Name = "LuePosition2";
            this.LuePosition2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition2.Properties.Appearance.Options.UseFont = true;
            this.LuePosition2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosition2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosition2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosition2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosition2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosition2.Properties.DropDownRows = 20;
            this.LuePosition2.Properties.NullText = "[Empty]";
            this.LuePosition2.Properties.PopupWidth = 500;
            this.LuePosition2.Size = new System.Drawing.Size(286, 20);
            this.LuePosition2.TabIndex = 32;
            this.LuePosition2.ToolTip = "F4 : Show/hide list";
            this.LuePosition2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosition2.EditValueChanged += new System.EventHandler(this.LuePosition2_EditValueChanged);
            this.LuePosition2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePosition2_KeyDown);
            this.LuePosition2.Leave += new System.EventHandler(this.LuePosition2_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 32);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(814, 193);
            this.Grd2.TabIndex = 26;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtTotalCustomer);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(814, 32);
            this.panel5.TabIndex = 49;
            // 
            // TxtTotalCustomer
            // 
            this.TxtTotalCustomer.EnterMoveNextControl = true;
            this.TxtTotalCustomer.Location = new System.Drawing.Point(47, 6);
            this.TxtTotalCustomer.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalCustomer.Name = "TxtTotalCustomer";
            this.TxtTotalCustomer.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalCustomer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalCustomer.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalCustomer.Properties.MaxLength = 16;
            this.TxtTotalCustomer.Properties.ReadOnly = true;
            this.TxtTotalCustomer.Size = new System.Drawing.Size(120, 20);
            this.TxtTotalCustomer.TabIndex = 83;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(9, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 82;
            this.label6.Text = "Total";
            // 
            // TpgInternalPerspective
            // 
            this.TpgInternalPerspective.Controls.Add(this.DteDeadLine2);
            this.TpgInternalPerspective.Controls.Add(this.LuePosition3);
            this.TpgInternalPerspective.Controls.Add(this.Grd3);
            this.TpgInternalPerspective.Controls.Add(this.panel6);
            this.TpgInternalPerspective.Location = new System.Drawing.Point(4, 26);
            this.TpgInternalPerspective.Name = "TpgInternalPerspective";
            this.TpgInternalPerspective.Padding = new System.Windows.Forms.Padding(3);
            this.TpgInternalPerspective.Size = new System.Drawing.Size(764, 35);
            this.TpgInternalPerspective.TabIndex = 3;
            this.TpgInternalPerspective.Text = "Internal Process Perspective";
            this.TpgInternalPerspective.UseVisualStyleBackColor = true;
            // 
            // DteDeadLine2
            // 
            this.DteDeadLine2.EditValue = null;
            this.DteDeadLine2.EnterMoveNextControl = true;
            this.DteDeadLine2.Location = new System.Drawing.Point(347, 104);
            this.DteDeadLine2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeadLine2.Name = "DteDeadLine2";
            this.DteDeadLine2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine2.Properties.Appearance.Options.UseFont = true;
            this.DteDeadLine2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeadLine2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeadLine2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeadLine2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeadLine2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeadLine2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeadLine2.Size = new System.Drawing.Size(125, 20);
            this.DteDeadLine2.TabIndex = 61;
            this.DteDeadLine2.Visible = false;
            this.DteDeadLine2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeadLine2_KeyDown);
            this.DteDeadLine2.Leave += new System.EventHandler(this.DteDeadLine2_Leave);
            // 
            // LuePosition3
            // 
            this.LuePosition3.EnterMoveNextControl = true;
            this.LuePosition3.Location = new System.Drawing.Point(239, 118);
            this.LuePosition3.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosition3.Name = "LuePosition3";
            this.LuePosition3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition3.Properties.Appearance.Options.UseFont = true;
            this.LuePosition3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosition3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosition3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosition3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosition3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosition3.Properties.DropDownRows = 20;
            this.LuePosition3.Properties.NullText = "[Empty]";
            this.LuePosition3.Properties.PopupWidth = 500;
            this.LuePosition3.Size = new System.Drawing.Size(286, 20);
            this.LuePosition3.TabIndex = 32;
            this.LuePosition3.ToolTip = "F4 : Show/hide list";
            this.LuePosition3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosition3.EditValueChanged += new System.EventHandler(this.LuePosition3_EditValueChanged);
            this.LuePosition3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePosition3_KeyDown);
            this.LuePosition3.Leave += new System.EventHandler(this.LuePosition3_Leave);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 35);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 0);
            this.Grd3.TabIndex = 27;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtTotalInternalProcess);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(758, 32);
            this.panel6.TabIndex = 49;
            // 
            // TxtTotalInternalProcess
            // 
            this.TxtTotalInternalProcess.EnterMoveNextControl = true;
            this.TxtTotalInternalProcess.Location = new System.Drawing.Point(47, 6);
            this.TxtTotalInternalProcess.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalInternalProcess.Name = "TxtTotalInternalProcess";
            this.TxtTotalInternalProcess.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalInternalProcess.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalInternalProcess.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalInternalProcess.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalInternalProcess.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalInternalProcess.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalInternalProcess.Properties.MaxLength = 16;
            this.TxtTotalInternalProcess.Properties.ReadOnly = true;
            this.TxtTotalInternalProcess.Size = new System.Drawing.Size(120, 20);
            this.TxtTotalInternalProcess.TabIndex = 83;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(9, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 82;
            this.label7.Text = "Total";
            // 
            // TpgLearningPerspective
            // 
            this.TpgLearningPerspective.Controls.Add(this.DteDeadLine3);
            this.TpgLearningPerspective.Controls.Add(this.LuePosition4);
            this.TpgLearningPerspective.Controls.Add(this.Grd4);
            this.TpgLearningPerspective.Controls.Add(this.panel7);
            this.TpgLearningPerspective.Location = new System.Drawing.Point(4, 26);
            this.TpgLearningPerspective.Name = "TpgLearningPerspective";
            this.TpgLearningPerspective.Padding = new System.Windows.Forms.Padding(3);
            this.TpgLearningPerspective.Size = new System.Drawing.Size(764, 35);
            this.TpgLearningPerspective.TabIndex = 4;
            this.TpgLearningPerspective.Text = "Learning & Growth Perspective";
            this.TpgLearningPerspective.UseVisualStyleBackColor = true;
            // 
            // DteDeadLine3
            // 
            this.DteDeadLine3.EditValue = null;
            this.DteDeadLine3.EnterMoveNextControl = true;
            this.DteDeadLine3.Location = new System.Drawing.Point(347, 104);
            this.DteDeadLine3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeadLine3.Name = "DteDeadLine3";
            this.DteDeadLine3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine3.Properties.Appearance.Options.UseFont = true;
            this.DteDeadLine3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeadLine3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeadLine3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeadLine3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeadLine3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeadLine3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeadLine3.Size = new System.Drawing.Size(125, 20);
            this.DteDeadLine3.TabIndex = 61;
            this.DteDeadLine3.Visible = false;
            this.DteDeadLine3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeadLine3_KeyDown);
            this.DteDeadLine3.Leave += new System.EventHandler(this.DteDeadLine3_Leave);
            // 
            // LuePosition4
            // 
            this.LuePosition4.EnterMoveNextControl = true;
            this.LuePosition4.Location = new System.Drawing.Point(239, 118);
            this.LuePosition4.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosition4.Name = "LuePosition4";
            this.LuePosition4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition4.Properties.Appearance.Options.UseFont = true;
            this.LuePosition4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosition4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosition4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosition4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosition4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosition4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosition4.Properties.DropDownRows = 20;
            this.LuePosition4.Properties.NullText = "[Empty]";
            this.LuePosition4.Properties.PopupWidth = 500;
            this.LuePosition4.Size = new System.Drawing.Size(286, 20);
            this.LuePosition4.TabIndex = 32;
            this.LuePosition4.ToolTip = "F4 : Show/hide list";
            this.LuePosition4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosition4.EditValueChanged += new System.EventHandler(this.LuePosition4_EditValueChanged);
            this.LuePosition4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePosition4_KeyDown);
            this.LuePosition4.Leave += new System.EventHandler(this.LuePosition4_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(3, 35);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(758, 0);
            this.Grd4.TabIndex = 27;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd4_AfterCommitEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtTotalLearning);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 32);
            this.panel7.TabIndex = 49;
            // 
            // TxtTotalLearning
            // 
            this.TxtTotalLearning.EnterMoveNextControl = true;
            this.TxtTotalLearning.Location = new System.Drawing.Point(47, 6);
            this.TxtTotalLearning.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalLearning.Name = "TxtTotalLearning";
            this.TxtTotalLearning.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalLearning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalLearning.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalLearning.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalLearning.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalLearning.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalLearning.Properties.MaxLength = 16;
            this.TxtTotalLearning.Properties.ReadOnly = true;
            this.TxtTotalLearning.Size = new System.Drawing.Size(120, 20);
            this.TxtTotalLearning.TabIndex = 83;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(9, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 82;
            this.label9.Text = "Total";
            // 
            // DteDeadLine0
            // 
            this.DteDeadLine0.EditValue = null;
            this.DteDeadLine0.EnterMoveNextControl = true;
            this.DteDeadLine0.Location = new System.Drawing.Point(345, 102);
            this.DteDeadLine0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeadLine0.Name = "DteDeadLine0";
            this.DteDeadLine0.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine0.Properties.Appearance.Options.UseFont = true;
            this.DteDeadLine0.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeadLine0.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeadLine0.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeadLine0.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeadLine0.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine0.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeadLine0.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeadLine0.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeadLine0.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeadLine0.Size = new System.Drawing.Size(125, 20);
            this.DteDeadLine0.TabIndex = 61;
            this.DteDeadLine0.Visible = false;
            this.DteDeadLine0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeadLine0_KeyDown);
            this.DteDeadLine0.Leave += new System.EventHandler(this.DteDeadLine0_Leave);
            // 
            // FrmGoalsSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 427);
            this.Name = "FrmGoalsSetting";
            this.Text = "Goal Setting";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalGoalsSetting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDirectorate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTopEvaluator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEvaluator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoalSetting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.Tpg.ResumeLayout(false);
            this.TpgFinancialPerpective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalFinancial.Properties)).EndInit();
            this.TpgCustomerPerspective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalCustomer.Properties)).EndInit();
            this.TpgInternalPerspective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalInternalProcess.Properties)).EndInit();
            this.TpgLearningPerspective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosition4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLearning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine0.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeadLine0.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl Tpg;
        private System.Windows.Forms.TabPage TpgFinancialPerpective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgCustomerPerspective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        public DevExpress.XtraEditors.TextEdit TxtPICName;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.TextEdit TxtPICCode;
        public DevExpress.XtraEditors.SimpleButton BtnPIC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage TpgInternalPerspective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgLearningPerspective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LuePosition1;
        private DevExpress.XtraEditors.LookUpEdit LuePosition2;
        private DevExpress.XtraEditors.LookUpEdit LuePosition3;
        private DevExpress.XtraEditors.LookUpEdit LuePosition4;
        private System.Windows.Forms.Label LblStatus;
        private DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LueDirectorate;
        public DevExpress.XtraEditors.SimpleButton BtnTopEvaluator;
        public DevExpress.XtraEditors.SimpleButton BtnEvaluator;
        private System.Windows.Forms.Label LblDirectorate;
        public DevExpress.XtraEditors.TextEdit TxtTopEvaluator;
        private System.Windows.Forms.Label LblTopEvaluator;
        public DevExpress.XtraEditors.TextEdit TxtEvaluator;
        private System.Windows.Forms.Label LblEvaluator;
        internal DevExpress.XtraEditors.TextEdit TxtGoalSetting;
        internal DevExpress.XtraEditors.TextEdit TxtTotalGoalsSetting;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtTotalFinancial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtTotalCustomer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit TxtTotalInternalProcess;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtTotalLearning;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteDeadLine;
        internal DevExpress.XtraEditors.DateEdit DteDeadLine2;
        internal DevExpress.XtraEditors.DateEdit DteDeadLine3;
        internal DevExpress.XtraEditors.DateEdit DteDeadLine0;
    }
}