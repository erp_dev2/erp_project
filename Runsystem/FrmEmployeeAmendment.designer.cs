﻿namespace RunSystem
{
    partial class FrmEmployeeAmendment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeAmendment));
            this.TxtEmpNameOld = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDeptCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtPosCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtGrdLvlCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.LueGrdLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.DteResignDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSiteCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtRTRW = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtVillage = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtSubDistrict = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LueCity = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPostalCode = new DevExpress.XtraEditors.TextEdit();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtVillageOld = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtSubDistrictOld = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtCityOld = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtPostalCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.TxtRTRWOld = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.MeeAddressOld = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.label63 = new System.Windows.Forms.Label();
            this.MeeDomicileOld = new DevExpress.XtraEditors.MemoExEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.MeeDomicile = new DevExpress.XtraEditors.MemoExEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.LueBloodTypeOld = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMotherOld = new DevExpress.XtraEditors.TextEdit();
            this.LblMotherOld = new System.Windows.Forms.Label();
            this.DteWeddingDtOld = new DevExpress.XtraEditors.DateEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.LueMaritalStatusOld = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtBirthPlaceOld = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueReligionOld = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtIdNumberOld = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.LueGenderOld = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.LueBloodType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMother = new DevExpress.XtraEditors.TextEdit();
            this.LblMotherNew = new System.Windows.Forms.Label();
            this.DteWeddingDt = new DevExpress.XtraEditors.DateEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.LueMaritalStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtBirthPlace = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.LueReligion = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtIdNumber = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.LuePTKPOld = new DevExpress.XtraEditors.LookUpEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtNPWPOld = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.LuePTKP = new DevExpress.XtraEditors.LookUpEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.TxtBankAcNameOld = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.TxtEmailOld = new DevExpress.XtraEditors.TextEdit();
            this.TxtBankBranchOld = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtBankAcNoOld = new DevExpress.XtraEditors.TextEdit();
            this.TxtMobileOld = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.LueBankCodeOld = new DevExpress.XtraEditors.LookUpEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtPhoneOld = new DevExpress.XtraEditors.TextEdit();
            this.TxtBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.TxtBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.TxtBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.DteBirthDtOld = new DevExpress.XtraEditors.DateEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOldOld = new DevExpress.XtraEditors.TextEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtDisplayNameOld = new DevExpress.XtraEditors.TextEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.DteTGDtOld = new DevExpress.XtraEditors.DateEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.DteTGDt = new DevExpress.XtraEditors.DateEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.LueSection = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSectionOld = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpNameOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLvlCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillageOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrictOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRWOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddressOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicileOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodTypeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMotherOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDtOld.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatusOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlaceOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligionOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumberOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGenderOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKPOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWPOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNameOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmailOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranchOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNoOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobileOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhoneOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDtOld.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOldOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayNameOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDtOld.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSectionOld.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1241, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Size = new System.Drawing.Size(100, 768);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(100, 31);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.TxtSectionOld);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.LueSection);
            this.panel2.Controls.Add(this.DteTGDt);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.DteTGDtOld);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TxtEmpName);
            this.panel2.Controls.Add(this.label69);
            this.panel2.Controls.Add(this.TxtDisplayNameOld);
            this.panel2.Controls.Add(this.label67);
            this.panel2.Controls.Add(this.TxtDisplayName);
            this.panel2.Controls.Add(this.label68);
            this.panel2.Controls.Add(this.TxtEmpCodeOldOld);
            this.panel2.Controls.Add(this.label65);
            this.panel2.Controls.Add(this.TxtEmpCodeOld);
            this.panel2.Controls.Add(this.label66);
            this.panel2.Controls.Add(this.DteBirthDt);
            this.panel2.Controls.Add(this.label64);
            this.panel2.Controls.Add(this.DteBirthDtOld);
            this.panel2.Controls.Add(this.label61);
            this.panel2.Controls.Add(this.TxtBankAcName);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.TxtEmail);
            this.panel2.Controls.Add(this.TxtBankBranch);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.TxtBankAcNo);
            this.panel2.Controls.Add(this.TxtMobile);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.LueBankCode);
            this.panel2.Controls.Add(this.label53);
            this.panel2.Controls.Add(this.label60);
            this.panel2.Controls.Add(this.TxtPhone);
            this.panel2.Controls.Add(this.TxtBankAcNameOld);
            this.panel2.Controls.Add(this.label49);
            this.panel2.Controls.Add(this.TxtEmailOld);
            this.panel2.Controls.Add(this.TxtBankBranchOld);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.label41);
            this.panel2.Controls.Add(this.TxtBankAcNoOld);
            this.panel2.Controls.Add(this.TxtMobileOld);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.LueBankCodeOld);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.TxtPhoneOld);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.LuePTKP);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.TxtNPWP);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.LuePTKPOld);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.TxtNPWPOld);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.LueBloodType);
            this.panel2.Controls.Add(this.TxtMother);
            this.panel2.Controls.Add(this.LblMotherNew);
            this.panel2.Controls.Add(this.DteWeddingDt);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.LueMaritalStatus);
            this.panel2.Controls.Add(this.TxtBirthPlace);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.LueReligion);
            this.panel2.Controls.Add(this.TxtIdNumber);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.LueGender);
            this.panel2.Controls.Add(this.label62);
            this.panel2.Controls.Add(this.LueBloodTypeOld);
            this.panel2.Controls.Add(this.TxtMotherOld);
            this.panel2.Controls.Add(this.LblMotherOld);
            this.panel2.Controls.Add(this.DteWeddingDtOld);
            this.panel2.Controls.Add(this.label59);
            this.panel2.Controls.Add(this.label58);
            this.panel2.Controls.Add(this.LueMaritalStatusOld);
            this.panel2.Controls.Add(this.TxtBirthPlaceOld);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.LueReligionOld);
            this.panel2.Controls.Add(this.TxtIdNumberOld);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.LueGenderOld);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.MeeDomicile);
            this.panel2.Controls.Add(this.label63);
            this.panel2.Controls.Add(this.MeeDomicileOld);
            this.panel2.Controls.Add(this.BtnEmpCode2);
            this.panel2.Controls.Add(this.MeeAddressOld);
            this.panel2.Controls.Add(this.TxtPostalCodeOld);
            this.panel2.Controls.Add(this.TxtRTRWOld);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.TxtVillageOld);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtSubDistrictOld);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.TxtCityOld);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.TxtRTRW);
            this.panel2.Controls.Add(this.label56);
            this.panel2.Controls.Add(this.TxtVillage);
            this.panel2.Controls.Add(this.label55);
            this.panel2.Controls.Add(this.TxtSubDistrict);
            this.panel2.Controls.Add(this.label54);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.LueCity);
            this.panel2.Controls.Add(this.TxtPostalCode);
            this.panel2.Controls.Add(this.MeeAddress);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.TxtSiteCodeOld);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.DteResignDt);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LuePosCode);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.LueGrdLvlCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.TxtGrdLvlCodeOld);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtPosCodeOld);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtDeptCodeOld);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtEmpNameOld);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.BtnEmpCode);
            this.panel2.Controls.Add(this.TxtEmpCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Size = new System.Drawing.Size(1241, 768);
            // 
            // TxtEmpNameOld
            // 
            this.TxtEmpNameOld.EnterMoveNextControl = true;
            this.TxtEmpNameOld.Location = new System.Drawing.Point(233, 74);
            this.TxtEmpNameOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmpNameOld.Name = "TxtEmpNameOld";
            this.TxtEmpNameOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpNameOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpNameOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpNameOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpNameOld.Properties.MaxLength = 16;
            this.TxtEmpNameOld.Properties.ReadOnly = true;
            this.TxtEmpNameOld.Size = new System.Drawing.Size(361, 28);
            this.TxtEmpNameOld.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(87, 79);
            this.label15.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 22);
            this.label15.TabIndex = 17;
            this.label15.Text = "Employee Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(827, 8);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(287, 28);
            this.TxtEmpCode.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(686, 13);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 22);
            this.label4.TabIndex = 13;
            this.label4.Text = "Employee Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(233, 41);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(187, 28);
            this.DteDocDt.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(177, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 22);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(233, 8);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(361, 28);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(120, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 22);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptCodeOld
            // 
            this.TxtDeptCodeOld.EnterMoveNextControl = true;
            this.TxtDeptCodeOld.Location = new System.Drawing.Point(233, 171);
            this.TxtDeptCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtDeptCodeOld.Name = "TxtDeptCodeOld";
            this.TxtDeptCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCodeOld.Properties.MaxLength = 40;
            this.TxtDeptCodeOld.Properties.ReadOnly = true;
            this.TxtDeptCodeOld.Size = new System.Drawing.Size(361, 28);
            this.TxtDeptCodeOld.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(50, 176);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 22);
            this.label3.TabIndex = 23;
            this.label3.Text = "Previous Department";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosCodeOld
            // 
            this.TxtPosCodeOld.EnterMoveNextControl = true;
            this.TxtPosCodeOld.Location = new System.Drawing.Point(233, 204);
            this.TxtPosCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtPosCodeOld.Name = "TxtPosCodeOld";
            this.TxtPosCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCodeOld.Properties.MaxLength = 40;
            this.TxtPosCodeOld.Properties.ReadOnly = true;
            this.TxtPosCodeOld.Size = new System.Drawing.Size(361, 28);
            this.TxtPosCodeOld.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(84, 209);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 22);
            this.label5.TabIndex = 25;
            this.label5.Text = "Previous Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrdLvlCodeOld
            // 
            this.TxtGrdLvlCodeOld.EnterMoveNextControl = true;
            this.TxtGrdLvlCodeOld.Location = new System.Drawing.Point(233, 237);
            this.TxtGrdLvlCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtGrdLvlCodeOld.Name = "TxtGrdLvlCodeOld";
            this.TxtGrdLvlCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrdLvlCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrdLvlCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrdLvlCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtGrdLvlCodeOld.Properties.MaxLength = 40;
            this.TxtGrdLvlCodeOld.Properties.ReadOnly = true;
            this.TxtGrdLvlCodeOld.Size = new System.Drawing.Size(361, 28);
            this.TxtGrdLvlCodeOld.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(53, 240);
            this.label6.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 22);
            this.label6.TabIndex = 27;
            this.label6.Text = "Previous Grade Level";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(669, 176);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 22);
            this.label7.TabIndex = 93;
            this.label7.Text = "New Department";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(827, 171);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 25;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Properties.ReadOnly = true;
            this.LueDeptCode.Size = new System.Drawing.Size(361, 28);
            this.LueDeptCode.TabIndex = 94;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(671, 240);
            this.label47.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(142, 22);
            this.label47.TabIndex = 97;
            this.label47.Text = "New Grade Level";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGrdLvlCode
            // 
            this.LueGrdLvlCode.EnterMoveNextControl = true;
            this.LueGrdLvlCode.Location = new System.Drawing.Point(827, 237);
            this.LueGrdLvlCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueGrdLvlCode.Name = "LueGrdLvlCode";
            this.LueGrdLvlCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCode.Properties.DropDownRows = 25;
            this.LueGrdLvlCode.Properties.NullText = "[Empty]";
            this.LueGrdLvlCode.Properties.PopupWidth = 500;
            this.LueGrdLvlCode.Properties.ReadOnly = true;
            this.LueGrdLvlCode.Size = new System.Drawing.Size(361, 28);
            this.LueGrdLvlCode.TabIndex = 98;
            this.LueGrdLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlCode.EditValueChanged += new System.EventHandler(this.LueGrdLvlCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(703, 207);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 22);
            this.label8.TabIndex = 95;
            this.label8.Text = "New Position";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(827, 204);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseBackColor = true;
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 25;
            this.LuePosCode.Properties.MaxLength = 16;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 500;
            this.LuePosCode.Properties.ReadOnly = true;
            this.LuePosCode.Size = new System.Drawing.Size(361, 28);
            this.LuePosCode.TabIndex = 96;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(1116, 6);
            this.BtnEmpCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(34, 33);
            this.BtnEmpCode.TabIndex = 15;
            this.BtnEmpCode.ToolTip = "Search Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(233, 1128);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(616, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(880, 28);
            this.MeeRemark.TabIndex = 153;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(161, 1130);
            this.label11.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 22);
            this.label11.TabIndex = 83;
            this.label11.Text = "Remark";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteResignDt
            // 
            this.DteResignDt.EditValue = null;
            this.DteResignDt.EnterMoveNextControl = true;
            this.DteResignDt.Location = new System.Drawing.Point(233, 1095);
            this.DteResignDt.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteResignDt.Name = "DteResignDt";
            this.DteResignDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.Appearance.Options.UseFont = true;
            this.DteResignDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDt.Size = new System.Drawing.Size(174, 28);
            this.DteResignDt.TabIndex = 86;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(126, 1100);
            this.label9.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 22);
            this.label9.TabIndex = 85;
            this.label9.Text = "Resign Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(733, 275);
            this.label10.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 22);
            this.label10.TabIndex = 99;
            this.label10.Text = "New Site";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(827, 270);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 25;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 500;
            this.LueSiteCode.Properties.ReadOnly = true;
            this.LueSiteCode.Size = new System.Drawing.Size(361, 28);
            this.LueSiteCode.TabIndex = 100;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // TxtSiteCodeOld
            // 
            this.TxtSiteCodeOld.EnterMoveNextControl = true;
            this.TxtSiteCodeOld.Location = new System.Drawing.Point(233, 270);
            this.TxtSiteCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtSiteCodeOld.Name = "TxtSiteCodeOld";
            this.TxtSiteCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCodeOld.Properties.MaxLength = 100;
            this.TxtSiteCodeOld.Properties.ReadOnly = true;
            this.TxtSiteCodeOld.Size = new System.Drawing.Size(361, 28);
            this.TxtSiteCodeOld.TabIndex = 30;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(119, 273);
            this.label12.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 22);
            this.label12.TabIndex = 29;
            this.label12.Text = "Previous Site";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRTRW
            // 
            this.TxtRTRW.EnterMoveNextControl = true;
            this.TxtRTRW.Location = new System.Drawing.Point(827, 435);
            this.TxtRTRW.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtRTRW.Name = "TxtRTRW";
            this.TxtRTRW.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRTRW.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRTRW.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRTRW.Properties.Appearance.Options.UseFont = true;
            this.TxtRTRW.Properties.MaxLength = 10;
            this.TxtRTRW.Size = new System.Drawing.Size(174, 28);
            this.TxtRTRW.TabIndex = 110;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(613, 440);
            this.label56.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(210, 22);
            this.label56.TabIndex = 109;
            this.label56.Text = "New RT/RW, Postal Code";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVillage
            // 
            this.TxtVillage.EnterMoveNextControl = true;
            this.TxtVillage.Location = new System.Drawing.Point(827, 402);
            this.TxtVillage.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtVillage.Name = "TxtVillage";
            this.TxtVillage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVillage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVillage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVillage.Properties.Appearance.Options.UseFont = true;
            this.TxtVillage.Properties.MaxLength = 40;
            this.TxtVillage.Size = new System.Drawing.Size(361, 28);
            this.TxtVillage.TabIndex = 108;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(714, 407);
            this.label55.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(101, 22);
            this.label55.TabIndex = 107;
            this.label55.Text = "New Village";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubDistrict
            // 
            this.TxtSubDistrict.EnterMoveNextControl = true;
            this.TxtSubDistrict.Location = new System.Drawing.Point(827, 369);
            this.TxtSubDistrict.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtSubDistrict.Name = "TxtSubDistrict";
            this.TxtSubDistrict.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSubDistrict.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubDistrict.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubDistrict.Properties.Appearance.Options.UseFont = true;
            this.TxtSubDistrict.Properties.MaxLength = 40;
            this.TxtSubDistrict.Size = new System.Drawing.Size(361, 28);
            this.TxtSubDistrict.TabIndex = 106;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(674, 374);
            this.label54.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(140, 22);
            this.label54.TabIndex = 105;
            this.label54.Text = "New Sub District";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(734, 341);
            this.label13.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 22);
            this.label13.TabIndex = 103;
            this.label13.Text = "New City";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCity
            // 
            this.LueCity.EnterMoveNextControl = true;
            this.LueCity.Location = new System.Drawing.Point(827, 336);
            this.LueCity.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueCity.Name = "LueCity";
            this.LueCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.Appearance.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity.Properties.DropDownRows = 25;
            this.LueCity.Properties.MaxLength = 16;
            this.LueCity.Properties.NullText = "[Empty]";
            this.LueCity.Properties.PopupWidth = 500;
            this.LueCity.Size = new System.Drawing.Size(361, 28);
            this.LueCity.TabIndex = 104;
            this.LueCity.ToolTip = "F4 : Show/hide list";
            this.LueCity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCity.EditValueChanged += new System.EventHandler(this.LueCity_EditValueChanged);
            // 
            // TxtPostalCode
            // 
            this.TxtPostalCode.EnterMoveNextControl = true;
            this.TxtPostalCode.Location = new System.Drawing.Point(1021, 435);
            this.TxtPostalCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtPostalCode.Name = "TxtPostalCode";
            this.TxtPostalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCode.Properties.MaxLength = 16;
            this.TxtPostalCode.Size = new System.Drawing.Size(166, 28);
            this.TxtPostalCode.TabIndex = 111;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(827, 303);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 80;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(361, 28);
            this.MeeAddress.TabIndex = 102;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(701, 308);
            this.label16.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 22);
            this.label16.TabIndex = 101;
            this.label16.Text = "New Address";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVillageOld
            // 
            this.TxtVillageOld.EnterMoveNextControl = true;
            this.TxtVillageOld.Location = new System.Drawing.Point(233, 402);
            this.TxtVillageOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtVillageOld.Name = "TxtVillageOld";
            this.TxtVillageOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVillageOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVillageOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVillageOld.Properties.Appearance.Options.UseFont = true;
            this.TxtVillageOld.Properties.MaxLength = 100;
            this.TxtVillageOld.Properties.ReadOnly = true;
            this.TxtVillageOld.Size = new System.Drawing.Size(361, 28);
            this.TxtVillageOld.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(100, 405);
            this.label17.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 22);
            this.label17.TabIndex = 37;
            this.label17.Text = "Previous Village";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubDistrictOld
            // 
            this.TxtSubDistrictOld.EnterMoveNextControl = true;
            this.TxtSubDistrictOld.Location = new System.Drawing.Point(233, 369);
            this.TxtSubDistrictOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtSubDistrictOld.Name = "TxtSubDistrictOld";
            this.TxtSubDistrictOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubDistrictOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubDistrictOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubDistrictOld.Properties.Appearance.Options.UseFont = true;
            this.TxtSubDistrictOld.Properties.MaxLength = 40;
            this.TxtSubDistrictOld.Properties.ReadOnly = true;
            this.TxtSubDistrictOld.Size = new System.Drawing.Size(361, 28);
            this.TxtSubDistrictOld.TabIndex = 36;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(60, 374);
            this.label18.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(172, 22);
            this.label18.TabIndex = 35;
            this.label18.Text = "Previous Sub District";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCityOld
            // 
            this.TxtCityOld.EnterMoveNextControl = true;
            this.TxtCityOld.Location = new System.Drawing.Point(233, 336);
            this.TxtCityOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtCityOld.Name = "TxtCityOld";
            this.TxtCityOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCityOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCityOld.Properties.Appearance.Options.UseFont = true;
            this.TxtCityOld.Properties.MaxLength = 40;
            this.TxtCityOld.Properties.ReadOnly = true;
            this.TxtCityOld.Size = new System.Drawing.Size(361, 28);
            this.TxtCityOld.TabIndex = 34;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(120, 339);
            this.label19.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 22);
            this.label19.TabIndex = 33;
            this.label19.Text = "Previous City";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(87, 308);
            this.label20.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(144, 22);
            this.label20.TabIndex = 31;
            this.label20.Text = "Previous Address";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCodeOld
            // 
            this.TxtPostalCodeOld.EnterMoveNextControl = true;
            this.TxtPostalCodeOld.Location = new System.Drawing.Point(427, 435);
            this.TxtPostalCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtPostalCodeOld.Name = "TxtPostalCodeOld";
            this.TxtPostalCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPostalCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCodeOld.Properties.MaxLength = 100;
            this.TxtPostalCodeOld.Properties.ReadOnly = true;
            this.TxtPostalCodeOld.Size = new System.Drawing.Size(167, 28);
            this.TxtPostalCodeOld.TabIndex = 42;
            // 
            // TxtRTRWOld
            // 
            this.TxtRTRWOld.EnterMoveNextControl = true;
            this.TxtRTRWOld.Location = new System.Drawing.Point(233, 435);
            this.TxtRTRWOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtRTRWOld.Name = "TxtRTRWOld";
            this.TxtRTRWOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRTRWOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRTRWOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRTRWOld.Properties.Appearance.Options.UseFont = true;
            this.TxtRTRWOld.Properties.MaxLength = 40;
            this.TxtRTRWOld.Properties.ReadOnly = true;
            this.TxtRTRWOld.Size = new System.Drawing.Size(174, 28);
            this.TxtRTRWOld.TabIndex = 40;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(20, 438);
            this.label22.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(210, 22);
            this.label22.TabIndex = 39;
            this.label22.Text = "Prev RT/RW, Postal Code";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddressOld
            // 
            this.MeeAddressOld.EnterMoveNextControl = true;
            this.MeeAddressOld.Location = new System.Drawing.Point(233, 303);
            this.MeeAddressOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeAddressOld.Name = "MeeAddressOld";
            this.MeeAddressOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAddressOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddressOld.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAddressOld.Properties.Appearance.Options.UseFont = true;
            this.MeeAddressOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddressOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddressOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddressOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddressOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddressOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddressOld.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddressOld.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddressOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddressOld.Properties.MaxLength = 80;
            this.MeeAddressOld.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddressOld.Properties.ReadOnly = true;
            this.MeeAddressOld.Properties.ShowIcon = false;
            this.MeeAddressOld.Size = new System.Drawing.Size(361, 28);
            this.MeeAddressOld.TabIndex = 32;
            this.MeeAddressOld.ToolTip = "F4 : Show/hide text";
            this.MeeAddressOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddressOld.ToolTipTitle = "Run System";
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(1159, 8);
            this.BtnEmpCode2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(34, 33);
            this.BtnEmpCode2.TabIndex = 16;
            this.BtnEmpCode2.ToolTip = "Show Data Employee";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(86, 471);
            this.label63.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(148, 22);
            this.label63.TabIndex = 43;
            this.label63.Text = "Previous Domicile";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDomicileOld
            // 
            this.MeeDomicileOld.EnterMoveNextControl = true;
            this.MeeDomicileOld.Location = new System.Drawing.Point(233, 468);
            this.MeeDomicileOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeDomicileOld.Name = "MeeDomicileOld";
            this.MeeDomicileOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeDomicileOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicileOld.Properties.Appearance.Options.UseBackColor = true;
            this.MeeDomicileOld.Properties.Appearance.Options.UseFont = true;
            this.MeeDomicileOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicileOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDomicileOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicileOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDomicileOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicileOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDomicileOld.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicileOld.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDomicileOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDomicileOld.Properties.MaxLength = 400;
            this.MeeDomicileOld.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDomicileOld.Properties.ShowIcon = false;
            this.MeeDomicileOld.Size = new System.Drawing.Size(361, 28);
            this.MeeDomicileOld.TabIndex = 44;
            this.MeeDomicileOld.ToolTip = "F4 : Show/hide text";
            this.MeeDomicileOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDomicileOld.ToolTipTitle = "Run System";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(707, 473);
            this.label23.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(116, 22);
            this.label23.TabIndex = 112;
            this.label23.Text = "New Domicile";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDomicile
            // 
            this.MeeDomicile.EnterMoveNextControl = true;
            this.MeeDomicile.Location = new System.Drawing.Point(827, 468);
            this.MeeDomicile.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeDomicile.Name = "MeeDomicile";
            this.MeeDomicile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.Appearance.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDomicile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDomicile.Properties.MaxLength = 400;
            this.MeeDomicile.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDomicile.Properties.ShowIcon = false;
            this.MeeDomicile.Size = new System.Drawing.Size(361, 28);
            this.MeeDomicile.TabIndex = 113;
            this.MeeDomicile.ToolTip = "F4 : Show/hide text";
            this.MeeDomicile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDomicile.ToolTipTitle = "Run System";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(60, 671);
            this.label62.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(172, 22);
            this.label62.TabIndex = 59;
            this.label62.Text = "Previous Blood Type";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBloodTypeOld
            // 
            this.LueBloodTypeOld.EnterMoveNextControl = true;
            this.LueBloodTypeOld.Location = new System.Drawing.Point(233, 666);
            this.LueBloodTypeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueBloodTypeOld.Name = "LueBloodTypeOld";
            this.LueBloodTypeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueBloodTypeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodTypeOld.Properties.Appearance.Options.UseBackColor = true;
            this.LueBloodTypeOld.Properties.Appearance.Options.UseFont = true;
            this.LueBloodTypeOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodTypeOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBloodTypeOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodTypeOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBloodTypeOld.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodTypeOld.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBloodTypeOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodTypeOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBloodTypeOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBloodTypeOld.Properties.DropDownRows = 25;
            this.LueBloodTypeOld.Properties.MaxLength = 2;
            this.LueBloodTypeOld.Properties.NullText = "[Empty]";
            this.LueBloodTypeOld.Properties.PopupWidth = 500;
            this.LueBloodTypeOld.Size = new System.Drawing.Size(361, 28);
            this.LueBloodTypeOld.TabIndex = 60;
            this.LueBloodTypeOld.ToolTip = "F4 : Show/hide list";
            this.LueBloodTypeOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBloodTypeOld.EditValueChanged += new System.EventHandler(this.LueBloodTypeOld_EditValueChanged);
            // 
            // TxtMotherOld
            // 
            this.TxtMotherOld.EnterMoveNextControl = true;
            this.TxtMotherOld.Location = new System.Drawing.Point(233, 699);
            this.TxtMotherOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtMotherOld.Name = "TxtMotherOld";
            this.TxtMotherOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMotherOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMotherOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMotherOld.Properties.Appearance.Options.UseFont = true;
            this.TxtMotherOld.Properties.MaxLength = 40;
            this.TxtMotherOld.Size = new System.Drawing.Size(361, 28);
            this.TxtMotherOld.TabIndex = 62;
            this.TxtMotherOld.Validated += new System.EventHandler(this.TxtMotherOld_Validated);
            // 
            // LblMotherOld
            // 
            this.LblMotherOld.AutoSize = true;
            this.LblMotherOld.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMotherOld.ForeColor = System.Drawing.Color.Black;
            this.LblMotherOld.Location = new System.Drawing.Point(19, 704);
            this.LblMotherOld.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblMotherOld.Name = "LblMotherOld";
            this.LblMotherOld.Size = new System.Drawing.Size(217, 22);
            this.LblMotherOld.TabIndex = 61;
            this.LblMotherOld.Text = "Previous Biological Mother";
            this.LblMotherOld.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteWeddingDtOld
            // 
            this.DteWeddingDtOld.EditValue = null;
            this.DteWeddingDtOld.EnterMoveNextControl = true;
            this.DteWeddingDtOld.Location = new System.Drawing.Point(233, 567);
            this.DteWeddingDtOld.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteWeddingDtOld.Name = "DteWeddingDtOld";
            this.DteWeddingDtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteWeddingDtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDtOld.Properties.Appearance.Options.UseBackColor = true;
            this.DteWeddingDtOld.Properties.Appearance.Options.UseFont = true;
            this.DteWeddingDtOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDtOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteWeddingDtOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteWeddingDtOld.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteWeddingDtOld.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDtOld.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteWeddingDtOld.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDtOld.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteWeddingDtOld.Properties.MaxLength = 16;
            this.DteWeddingDtOld.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteWeddingDtOld.Size = new System.Drawing.Size(174, 28);
            this.DteWeddingDtOld.TabIndex = 50;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(36, 572);
            this.label59.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(193, 22);
            this.label59.TabIndex = 49;
            this.label59.Text = "Previous Wedding Date";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(44, 539);
            this.label58.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(189, 22);
            this.label58.TabIndex = 47;
            this.label58.Text = "Previous Marital Status";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueMaritalStatusOld
            // 
            this.LueMaritalStatusOld.EnterMoveNextControl = true;
            this.LueMaritalStatusOld.Location = new System.Drawing.Point(233, 534);
            this.LueMaritalStatusOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueMaritalStatusOld.Name = "LueMaritalStatusOld";
            this.LueMaritalStatusOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueMaritalStatusOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatusOld.Properties.Appearance.Options.UseBackColor = true;
            this.LueMaritalStatusOld.Properties.Appearance.Options.UseFont = true;
            this.LueMaritalStatusOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatusOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaritalStatusOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatusOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaritalStatusOld.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatusOld.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaritalStatusOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatusOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaritalStatusOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaritalStatusOld.Properties.DropDownRows = 30;
            this.LueMaritalStatusOld.Properties.MaxLength = 1;
            this.LueMaritalStatusOld.Properties.NullText = "[Empty]";
            this.LueMaritalStatusOld.Properties.PopupWidth = 300;
            this.LueMaritalStatusOld.Size = new System.Drawing.Size(361, 28);
            this.LueMaritalStatusOld.TabIndex = 48;
            this.LueMaritalStatusOld.ToolTip = "F4 : Show/hide list";
            this.LueMaritalStatusOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMaritalStatusOld.EditValueChanged += new System.EventHandler(this.LueMaritalStatusOld_EditValueChanged);
            // 
            // TxtBirthPlaceOld
            // 
            this.TxtBirthPlaceOld.EnterMoveNextControl = true;
            this.TxtBirthPlaceOld.Location = new System.Drawing.Point(233, 633);
            this.TxtBirthPlaceOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBirthPlaceOld.Name = "TxtBirthPlaceOld";
            this.TxtBirthPlaceOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBirthPlaceOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirthPlaceOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBirthPlaceOld.Properties.Appearance.Options.UseFont = true;
            this.TxtBirthPlaceOld.Properties.MaxLength = 80;
            this.TxtBirthPlaceOld.Size = new System.Drawing.Size(174, 28);
            this.TxtBirthPlaceOld.TabIndex = 56;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(23, 638);
            this.label24.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(208, 22);
            this.label24.TabIndex = 55;
            this.label24.Text = "Previous Birth Place/Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReligionOld
            // 
            this.LueReligionOld.EnterMoveNextControl = true;
            this.LueReligionOld.Location = new System.Drawing.Point(427, 600);
            this.LueReligionOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueReligionOld.Name = "LueReligionOld";
            this.LueReligionOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueReligionOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligionOld.Properties.Appearance.Options.UseBackColor = true;
            this.LueReligionOld.Properties.Appearance.Options.UseFont = true;
            this.LueReligionOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligionOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReligionOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligionOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReligionOld.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligionOld.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReligionOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligionOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReligionOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReligionOld.Properties.DropDownRows = 30;
            this.LueReligionOld.Properties.MaxLength = 2;
            this.LueReligionOld.Properties.NullText = "[Empty]";
            this.LueReligionOld.Properties.PopupWidth = 300;
            this.LueReligionOld.Size = new System.Drawing.Size(167, 28);
            this.LueReligionOld.TabIndex = 54;
            this.LueReligionOld.ToolTip = "F4 : Show/hide list";
            this.LueReligionOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReligionOld.EditValueChanged += new System.EventHandler(this.LueReligionOld_EditValueChanged);
            // 
            // TxtIdNumberOld
            // 
            this.TxtIdNumberOld.EnterMoveNextControl = true;
            this.TxtIdNumberOld.Location = new System.Drawing.Point(233, 501);
            this.TxtIdNumberOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtIdNumberOld.Name = "TxtIdNumberOld";
            this.TxtIdNumberOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIdNumberOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdNumberOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdNumberOld.Properties.Appearance.Options.UseFont = true;
            this.TxtIdNumberOld.Properties.MaxLength = 40;
            this.TxtIdNumberOld.Size = new System.Drawing.Size(361, 28);
            this.TxtIdNumberOld.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(74, 506);
            this.label26.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(156, 22);
            this.label26.TabIndex = 45;
            this.label26.Text = "Previous Identity#";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(20, 605);
            this.label27.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(211, 22);
            this.label27.TabIndex = 51;
            this.label27.Text = "Previous Gender, Religion";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGenderOld
            // 
            this.LueGenderOld.EnterMoveNextControl = true;
            this.LueGenderOld.Location = new System.Drawing.Point(233, 600);
            this.LueGenderOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueGenderOld.Name = "LueGenderOld";
            this.LueGenderOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueGenderOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGenderOld.Properties.Appearance.Options.UseBackColor = true;
            this.LueGenderOld.Properties.Appearance.Options.UseFont = true;
            this.LueGenderOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGenderOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGenderOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGenderOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGenderOld.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGenderOld.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGenderOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGenderOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGenderOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGenderOld.Properties.DropDownRows = 30;
            this.LueGenderOld.Properties.MaxLength = 1;
            this.LueGenderOld.Properties.NullText = "[Empty]";
            this.LueGenderOld.Properties.PopupWidth = 300;
            this.LueGenderOld.Size = new System.Drawing.Size(174, 28);
            this.LueGenderOld.TabIndex = 52;
            this.LueGenderOld.ToolTip = "F4 : Show/hide list";
            this.LueGenderOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGenderOld.EditValueChanged += new System.EventHandler(this.LueGenderOld_EditValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(681, 673);
            this.label28.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(140, 22);
            this.label28.TabIndex = 127;
            this.label28.Text = "New Blood Type";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBloodType
            // 
            this.LueBloodType.EnterMoveNextControl = true;
            this.LueBloodType.Location = new System.Drawing.Point(827, 668);
            this.LueBloodType.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueBloodType.Name = "LueBloodType";
            this.LueBloodType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueBloodType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.Appearance.Options.UseBackColor = true;
            this.LueBloodType.Properties.Appearance.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBloodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBloodType.Properties.DropDownRows = 25;
            this.LueBloodType.Properties.MaxLength = 2;
            this.LueBloodType.Properties.NullText = "[Empty]";
            this.LueBloodType.Properties.PopupWidth = 500;
            this.LueBloodType.Size = new System.Drawing.Size(361, 28);
            this.LueBloodType.TabIndex = 128;
            this.LueBloodType.ToolTip = "F4 : Show/hide list";
            this.LueBloodType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBloodType.EditValueChanged += new System.EventHandler(this.LueBloodType_EditValueChanged);
            // 
            // TxtMother
            // 
            this.TxtMother.EnterMoveNextControl = true;
            this.TxtMother.Location = new System.Drawing.Point(827, 701);
            this.TxtMother.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtMother.Name = "TxtMother";
            this.TxtMother.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMother.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMother.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMother.Properties.Appearance.Options.UseFont = true;
            this.TxtMother.Properties.MaxLength = 40;
            this.TxtMother.Size = new System.Drawing.Size(361, 28);
            this.TxtMother.TabIndex = 130;
            this.TxtMother.Validated += new System.EventHandler(this.TxtMother_Validated);
            // 
            // LblMotherNew
            // 
            this.LblMotherNew.AutoSize = true;
            this.LblMotherNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMotherNew.ForeColor = System.Drawing.Color.Black;
            this.LblMotherNew.Location = new System.Drawing.Point(640, 706);
            this.LblMotherNew.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblMotherNew.Name = "LblMotherNew";
            this.LblMotherNew.Size = new System.Drawing.Size(185, 22);
            this.LblMotherNew.TabIndex = 129;
            this.LblMotherNew.Text = "New Biological Mother";
            this.LblMotherNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteWeddingDt
            // 
            this.DteWeddingDt.EditValue = null;
            this.DteWeddingDt.EnterMoveNextControl = true;
            this.DteWeddingDt.Location = new System.Drawing.Point(827, 567);
            this.DteWeddingDt.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteWeddingDt.Name = "DteWeddingDt";
            this.DteWeddingDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteWeddingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteWeddingDt.Properties.Appearance.Options.UseFont = true;
            this.DteWeddingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteWeddingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteWeddingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteWeddingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.MaxLength = 16;
            this.DteWeddingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteWeddingDt.Size = new System.Drawing.Size(174, 28);
            this.DteWeddingDt.TabIndex = 119;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(657, 572);
            this.label30.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(161, 22);
            this.label30.TabIndex = 118;
            this.label30.Text = "New Wedding Date";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(666, 539);
            this.label31.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(157, 22);
            this.label31.TabIndex = 116;
            this.label31.Text = "New Marital Status";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueMaritalStatus
            // 
            this.LueMaritalStatus.EnterMoveNextControl = true;
            this.LueMaritalStatus.Location = new System.Drawing.Point(827, 534);
            this.LueMaritalStatus.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueMaritalStatus.Name = "LueMaritalStatus";
            this.LueMaritalStatus.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueMaritalStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.Appearance.Options.UseBackColor = true;
            this.LueMaritalStatus.Properties.Appearance.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaritalStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaritalStatus.Properties.DropDownRows = 30;
            this.LueMaritalStatus.Properties.MaxLength = 1;
            this.LueMaritalStatus.Properties.NullText = "[Empty]";
            this.LueMaritalStatus.Properties.PopupWidth = 300;
            this.LueMaritalStatus.Size = new System.Drawing.Size(361, 28);
            this.LueMaritalStatus.TabIndex = 117;
            this.LueMaritalStatus.ToolTip = "F4 : Show/hide list";
            this.LueMaritalStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMaritalStatus.EditValueChanged += new System.EventHandler(this.LueMaritalStatus_EditValueChanged);
            // 
            // TxtBirthPlace
            // 
            this.TxtBirthPlace.EnterMoveNextControl = true;
            this.TxtBirthPlace.Location = new System.Drawing.Point(827, 635);
            this.TxtBirthPlace.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBirthPlace.Name = "TxtBirthPlace";
            this.TxtBirthPlace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBirthPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirthPlace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBirthPlace.Properties.Appearance.Options.UseFont = true;
            this.TxtBirthPlace.Properties.MaxLength = 80;
            this.TxtBirthPlace.Size = new System.Drawing.Size(174, 28);
            this.TxtBirthPlace.TabIndex = 124;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(644, 640);
            this.label32.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(176, 22);
            this.label32.TabIndex = 123;
            this.label32.Text = "New Birth Place/Date";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReligion
            // 
            this.LueReligion.EnterMoveNextControl = true;
            this.LueReligion.Location = new System.Drawing.Point(1021, 602);
            this.LueReligion.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueReligion.Name = "LueReligion";
            this.LueReligion.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueReligion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.Appearance.Options.UseBackColor = true;
            this.LueReligion.Properties.Appearance.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReligion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReligion.Properties.DropDownRows = 30;
            this.LueReligion.Properties.MaxLength = 2;
            this.LueReligion.Properties.NullText = "[Empty]";
            this.LueReligion.Properties.PopupWidth = 300;
            this.LueReligion.Size = new System.Drawing.Size(164, 28);
            this.LueReligion.TabIndex = 122;
            this.LueReligion.ToolTip = "F4 : Show/hide list";
            this.LueReligion.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReligion.EditValueChanged += new System.EventHandler(this.LueReligion_EditValueChanged);
            // 
            // TxtIdNumber
            // 
            this.TxtIdNumber.EnterMoveNextControl = true;
            this.TxtIdNumber.Location = new System.Drawing.Point(827, 501);
            this.TxtIdNumber.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtIdNumber.Name = "TxtIdNumber";
            this.TxtIdNumber.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdNumber.Properties.Appearance.Options.UseFont = true;
            this.TxtIdNumber.Properties.MaxLength = 40;
            this.TxtIdNumber.Size = new System.Drawing.Size(361, 28);
            this.TxtIdNumber.TabIndex = 115;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(696, 506);
            this.label34.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(124, 22);
            this.label34.TabIndex = 114;
            this.label34.Text = "New Identity#";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(644, 605);
            this.label35.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(179, 22);
            this.label35.TabIndex = 120;
            this.label35.Text = "New Gender, Religion";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(827, 602);
            this.LueGender.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseBackColor = true;
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 30;
            this.LueGender.Properties.MaxLength = 1;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 300;
            this.LueGender.Size = new System.Drawing.Size(174, 28);
            this.LueGender.TabIndex = 121;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGender.EditValueChanged += new System.EventHandler(this.LueGender_EditValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(6, 772);
            this.label36.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(230, 22);
            this.label36.TabIndex = 65;
            this.label36.Text = "Previous Non-Incoming Tax";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePTKPOld
            // 
            this.LuePTKPOld.EnterMoveNextControl = true;
            this.LuePTKPOld.Location = new System.Drawing.Point(233, 765);
            this.LuePTKPOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LuePTKPOld.Name = "LuePTKPOld";
            this.LuePTKPOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LuePTKPOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKPOld.Properties.Appearance.Options.UseBackColor = true;
            this.LuePTKPOld.Properties.Appearance.Options.UseFont = true;
            this.LuePTKPOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKPOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePTKPOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKPOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePTKPOld.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKPOld.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePTKPOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKPOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePTKPOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePTKPOld.Properties.DropDownRows = 30;
            this.LuePTKPOld.Properties.NullText = "[Empty]";
            this.LuePTKPOld.Properties.PopupWidth = 300;
            this.LuePTKPOld.Size = new System.Drawing.Size(361, 28);
            this.LuePTKPOld.TabIndex = 66;
            this.LuePTKPOld.ToolTip = "F4 : Show/hide list";
            this.LuePTKPOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePTKPOld.EditValueChanged += new System.EventHandler(this.LuePTKPOld_EditValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(100, 739);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(130, 22);
            this.label37.TabIndex = 63;
            this.label37.Text = "Previous NPWP";
            // 
            // TxtNPWPOld
            // 
            this.TxtNPWPOld.EnterMoveNextControl = true;
            this.TxtNPWPOld.Location = new System.Drawing.Point(233, 732);
            this.TxtNPWPOld.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtNPWPOld.Name = "TxtNPWPOld";
            this.TxtNPWPOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNPWPOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWPOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNPWPOld.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWPOld.Properties.MaxLength = 40;
            this.TxtNPWPOld.Size = new System.Drawing.Size(361, 28);
            this.TxtNPWPOld.TabIndex = 64;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(627, 773);
            this.label38.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(198, 22);
            this.label38.TabIndex = 133;
            this.label38.Text = "New Non-Incoming Tax";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePTKP
            // 
            this.LuePTKP.EnterMoveNextControl = true;
            this.LuePTKP.Location = new System.Drawing.Point(827, 767);
            this.LuePTKP.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LuePTKP.Name = "LuePTKP";
            this.LuePTKP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LuePTKP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.Appearance.Options.UseBackColor = true;
            this.LuePTKP.Properties.Appearance.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePTKP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePTKP.Properties.DropDownRows = 30;
            this.LuePTKP.Properties.NullText = "[Empty]";
            this.LuePTKP.Properties.PopupWidth = 300;
            this.LuePTKP.Size = new System.Drawing.Size(361, 28);
            this.LuePTKP.TabIndex = 134;
            this.LuePTKP.ToolTip = "F4 : Show/hide list";
            this.LuePTKP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePTKP.EditValueChanged += new System.EventHandler(this.LuePTKP_EditValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(721, 740);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(98, 22);
            this.label39.TabIndex = 131;
            this.label39.Text = "New NPWP";
            // 
            // TxtNPWP
            // 
            this.TxtNPWP.EnterMoveNextControl = true;
            this.TxtNPWP.Location = new System.Drawing.Point(827, 734);
            this.TxtNPWP.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtNPWP.Name = "TxtNPWP";
            this.TxtNPWP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWP.Properties.MaxLength = 40;
            this.TxtNPWP.Size = new System.Drawing.Size(361, 28);
            this.TxtNPWP.TabIndex = 132;
            // 
            // TxtBankAcNameOld
            // 
            this.TxtBankAcNameOld.EnterMoveNextControl = true;
            this.TxtBankAcNameOld.Location = new System.Drawing.Point(233, 864);
            this.TxtBankAcNameOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBankAcNameOld.Name = "TxtBankAcNameOld";
            this.TxtBankAcNameOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBankAcNameOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNameOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNameOld.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNameOld.Properties.MaxLength = 80;
            this.TxtBankAcNameOld.Size = new System.Drawing.Size(361, 28);
            this.TxtBankAcNameOld.TabIndex = 72;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(33, 869);
            this.label49.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(196, 22);
            this.label49.TabIndex = 71;
            this.label49.Text = "Previous Account Name";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmailOld
            // 
            this.TxtEmailOld.EnterMoveNextControl = true;
            this.TxtEmailOld.Location = new System.Drawing.Point(233, 996);
            this.TxtEmailOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmailOld.Name = "TxtEmailOld";
            this.TxtEmailOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmailOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmailOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmailOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmailOld.Properties.MaxLength = 80;
            this.TxtEmailOld.Size = new System.Drawing.Size(361, 28);
            this.TxtEmailOld.TabIndex = 80;
            // 
            // TxtBankBranchOld
            // 
            this.TxtBankBranchOld.EnterMoveNextControl = true;
            this.TxtBankBranchOld.Location = new System.Drawing.Point(233, 831);
            this.TxtBankBranchOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBankBranchOld.Name = "TxtBankBranchOld";
            this.TxtBankBranchOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBankBranchOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankBranchOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankBranchOld.Properties.Appearance.Options.UseFont = true;
            this.TxtBankBranchOld.Properties.MaxLength = 80;
            this.TxtBankBranchOld.Size = new System.Drawing.Size(361, 28);
            this.TxtBankBranchOld.TabIndex = 70;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(110, 1001);
            this.label40.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(124, 22);
            this.label40.TabIndex = 79;
            this.label40.Text = "Previous Email";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(46, 836);
            this.label41.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(187, 22);
            this.label41.TabIndex = 69;
            this.label41.Text = "Previous Branch Name";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNoOld
            // 
            this.TxtBankAcNoOld.EnterMoveNextControl = true;
            this.TxtBankAcNoOld.Location = new System.Drawing.Point(233, 897);
            this.TxtBankAcNoOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBankAcNoOld.Name = "TxtBankAcNoOld";
            this.TxtBankAcNoOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBankAcNoOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNoOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNoOld.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNoOld.Properties.MaxLength = 80;
            this.TxtBankAcNoOld.Size = new System.Drawing.Size(361, 28);
            this.TxtBankAcNoOld.TabIndex = 74;
            // 
            // TxtMobileOld
            // 
            this.TxtMobileOld.EnterMoveNextControl = true;
            this.TxtMobileOld.Location = new System.Drawing.Point(233, 963);
            this.TxtMobileOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtMobileOld.Name = "TxtMobileOld";
            this.TxtMobileOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobileOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobileOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobileOld.Properties.Appearance.Options.UseFont = true;
            this.TxtMobileOld.Properties.MaxLength = 80;
            this.TxtMobileOld.Size = new System.Drawing.Size(361, 28);
            this.TxtMobileOld.TabIndex = 78;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(70, 902);
            this.label42.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(158, 22);
            this.label42.TabIndex = 73;
            this.label42.Text = "Previous Account#";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(61, 803);
            this.label43.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(172, 22);
            this.label43.TabIndex = 67;
            this.label43.Text = "Previous Bank Name";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCodeOld
            // 
            this.LueBankCodeOld.EnterMoveNextControl = true;
            this.LueBankCodeOld.Location = new System.Drawing.Point(233, 798);
            this.LueBankCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueBankCodeOld.Name = "LueBankCodeOld";
            this.LueBankCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueBankCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.LueBankCodeOld.Properties.Appearance.Options.UseFont = true;
            this.LueBankCodeOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCodeOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCodeOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCodeOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCodeOld.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCodeOld.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCodeOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCodeOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCodeOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCodeOld.Properties.DropDownRows = 30;
            this.LueBankCodeOld.Properties.NullText = "[Empty]";
            this.LueBankCodeOld.Properties.PopupWidth = 300;
            this.LueBankCodeOld.Size = new System.Drawing.Size(361, 28);
            this.LueBankCodeOld.TabIndex = 68;
            this.LueBankCodeOld.ToolTip = "F4 : Show/hide list";
            this.LueBankCodeOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(100, 966);
            this.label44.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(133, 22);
            this.label44.TabIndex = 77;
            this.label44.Text = "Previous Mobile";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(99, 935);
            this.label45.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(131, 22);
            this.label45.TabIndex = 75;
            this.label45.Text = "Previous Phone";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhoneOld
            // 
            this.TxtPhoneOld.EnterMoveNextControl = true;
            this.TxtPhoneOld.Location = new System.Drawing.Point(233, 930);
            this.TxtPhoneOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtPhoneOld.Name = "TxtPhoneOld";
            this.TxtPhoneOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhoneOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhoneOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhoneOld.Properties.Appearance.Options.UseFont = true;
            this.TxtPhoneOld.Properties.MaxLength = 80;
            this.TxtPhoneOld.Size = new System.Drawing.Size(361, 28);
            this.TxtPhoneOld.TabIndex = 76;
            // 
            // TxtBankAcName
            // 
            this.TxtBankAcName.EnterMoveNextControl = true;
            this.TxtBankAcName.Location = new System.Drawing.Point(827, 864);
            this.TxtBankAcName.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBankAcName.Name = "TxtBankAcName";
            this.TxtBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcName.Properties.MaxLength = 80;
            this.TxtBankAcName.Size = new System.Drawing.Size(361, 28);
            this.TxtBankAcName.TabIndex = 140;
            this.TxtBankAcName.Validated += new System.EventHandler(this.TxtBankAcName_Validated);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(654, 869);
            this.label46.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(164, 22);
            this.label46.TabIndex = 139;
            this.label46.Text = "New Account Name";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(827, 996);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 80;
            this.TxtEmail.Size = new System.Drawing.Size(361, 28);
            this.TxtEmail.TabIndex = 148;
            this.TxtEmail.Validated += new System.EventHandler(this.TxtEmail_Validated);
            // 
            // TxtBankBranch
            // 
            this.TxtBankBranch.EnterMoveNextControl = true;
            this.TxtBankBranch.Location = new System.Drawing.Point(827, 831);
            this.TxtBankBranch.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBankBranch.Name = "TxtBankBranch";
            this.TxtBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtBankBranch.Properties.MaxLength = 80;
            this.TxtBankBranch.Size = new System.Drawing.Size(361, 28);
            this.TxtBankBranch.TabIndex = 138;
            this.TxtBankBranch.Validated += new System.EventHandler(this.TxtBankBranch_Validated);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(731, 1001);
            this.label48.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(92, 22);
            this.label48.TabIndex = 147;
            this.label48.Text = "New Email";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(667, 836);
            this.label50.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(155, 22);
            this.label50.TabIndex = 137;
            this.label50.Text = "New Branch Name";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNo
            // 
            this.TxtBankAcNo.EnterMoveNextControl = true;
            this.TxtBankAcNo.Location = new System.Drawing.Point(827, 897);
            this.TxtBankAcNo.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtBankAcNo.Name = "TxtBankAcNo";
            this.TxtBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNo.Properties.MaxLength = 80;
            this.TxtBankAcNo.Size = new System.Drawing.Size(361, 28);
            this.TxtBankAcNo.TabIndex = 142;
            this.TxtBankAcNo.Validated += new System.EventHandler(this.TxtBankAcNo_Validated);
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(827, 963);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 80;
            this.TxtMobile.Size = new System.Drawing.Size(361, 28);
            this.TxtMobile.TabIndex = 146;
            this.TxtMobile.Validated += new System.EventHandler(this.TxtMobile_Validated);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(691, 902);
            this.label51.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(126, 22);
            this.label51.TabIndex = 141;
            this.label51.Text = "New Account#";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(683, 803);
            this.label52.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(140, 22);
            this.label52.TabIndex = 135;
            this.label52.Text = "New Bank Name";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(827, 798);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(361, 28);
            this.LueBankCode.TabIndex = 136;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(721, 966);
            this.label53.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(101, 22);
            this.label53.TabIndex = 145;
            this.label53.Text = "New Mobile";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(720, 935);
            this.label60.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(99, 22);
            this.label60.TabIndex = 143;
            this.label60.Text = "New Phone";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(827, 930);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 80;
            this.TxtPhone.Size = new System.Drawing.Size(361, 28);
            this.TxtPhone.TabIndex = 144;
            this.TxtPhone.Validated += new System.EventHandler(this.TxtPhone_Validated);
            // 
            // DteBirthDtOld
            // 
            this.DteBirthDtOld.EditValue = null;
            this.DteBirthDtOld.EnterMoveNextControl = true;
            this.DteBirthDtOld.Location = new System.Drawing.Point(427, 633);
            this.DteBirthDtOld.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteBirthDtOld.Name = "DteBirthDtOld";
            this.DteBirthDtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteBirthDtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDtOld.Properties.Appearance.Options.UseBackColor = true;
            this.DteBirthDtOld.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDtOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDtOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDtOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDtOld.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDtOld.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDtOld.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDtOld.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDtOld.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDtOld.Properties.MaxLength = 8;
            this.DteBirthDtOld.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDtOld.Size = new System.Drawing.Size(167, 28);
            this.DteBirthDtOld.TabIndex = 58;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(407, 636);
            this.label61.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(17, 22);
            this.label61.TabIndex = 57;
            this.label61.Text = "/";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(1021, 635);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(166, 28);
            this.DteBirthDt.TabIndex = 126;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(1003, 640);
            this.label64.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(17, 22);
            this.label64.TabIndex = 125;
            this.label64.Text = "/";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOldOld
            // 
            this.TxtEmpCodeOldOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOldOld.Location = new System.Drawing.Point(233, 105);
            this.TxtEmpCodeOldOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmpCodeOldOld.Name = "TxtEmpCodeOldOld";
            this.TxtEmpCodeOldOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCodeOldOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOldOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOldOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOldOld.Properties.MaxLength = 40;
            this.TxtEmpCodeOldOld.Properties.ReadOnly = true;
            this.TxtEmpCodeOldOld.Size = new System.Drawing.Size(361, 28);
            this.TxtEmpCodeOldOld.TabIndex = 20;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(143, 110);
            this.label65.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(83, 22);
            this.label65.TabIndex = 19;
            this.label65.Text = "Old Code";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(827, 105);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 40;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(361, 28);
            this.TxtEmpCodeOld.TabIndex = 90;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(691, 110);
            this.label66.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(123, 22);
            this.label66.TabIndex = 89;
            this.label66.Text = "New Old Code";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDisplayNameOld
            // 
            this.TxtDisplayNameOld.EnterMoveNextControl = true;
            this.TxtDisplayNameOld.Location = new System.Drawing.Point(233, 138);
            this.TxtDisplayNameOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtDisplayNameOld.Name = "TxtDisplayNameOld";
            this.TxtDisplayNameOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDisplayNameOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayNameOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayNameOld.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayNameOld.Properties.MaxLength = 40;
            this.TxtDisplayNameOld.Properties.ReadOnly = true;
            this.TxtDisplayNameOld.Size = new System.Drawing.Size(361, 28);
            this.TxtDisplayNameOld.TabIndex = 22;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(113, 143);
            this.label67.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(117, 22);
            this.label67.TabIndex = 21;
            this.label67.Text = "Display Name";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(827, 138);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 40;
            this.TxtDisplayName.Size = new System.Drawing.Size(361, 28);
            this.TxtDisplayName.TabIndex = 92;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(661, 143);
            this.label68.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(157, 22);
            this.label68.TabIndex = 91;
            this.label68.Text = "New Display Name";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(827, 72);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 40;
            this.TxtEmpName.Size = new System.Drawing.Size(361, 28);
            this.TxtEmpName.TabIndex = 88;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(637, 75);
            this.label69.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(177, 22);
            this.label69.TabIndex = 87;
            this.label69.Text = "New Employee Name";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTGDtOld
            // 
            this.DteTGDtOld.EditValue = null;
            this.DteTGDtOld.EnterMoveNextControl = true;
            this.DteTGDtOld.Location = new System.Drawing.Point(233, 1029);
            this.DteTGDtOld.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteTGDtOld.Name = "DteTGDtOld";
            this.DteTGDtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteTGDtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDtOld.Properties.Appearance.Options.UseBackColor = true;
            this.DteTGDtOld.Properties.Appearance.Options.UseFont = true;
            this.DteTGDtOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDtOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTGDtOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTGDtOld.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTGDtOld.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDtOld.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTGDtOld.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDtOld.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTGDtOld.Properties.MaxLength = 16;
            this.DteTGDtOld.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTGDtOld.Size = new System.Drawing.Size(174, 28);
            this.DteTGDtOld.TabIndex = 82;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(29, 1034);
            this.label14.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(206, 22);
            this.label14.TabIndex = 81;
            this.label14.Text = "Prev Training Graduation";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTGDt
            // 
            this.DteTGDt.EditValue = null;
            this.DteTGDt.EnterMoveNextControl = true;
            this.DteTGDt.Location = new System.Drawing.Point(827, 1029);
            this.DteTGDt.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DteTGDt.Name = "DteTGDt";
            this.DteTGDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteTGDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteTGDt.Properties.Appearance.Options.UseFont = true;
            this.DteTGDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTGDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTGDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTGDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTGDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTGDt.Properties.MaxLength = 16;
            this.DteTGDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTGDt.Size = new System.Drawing.Size(174, 28);
            this.DteTGDt.TabIndex = 150;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(623, 1034);
            this.label21.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(206, 22);
            this.label21.TabIndex = 149;
            this.label21.Text = "New Training Graduation";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(753, 1067);
            this.label25.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 22);
            this.label25.TabIndex = 151;
            this.label25.Text = "Section";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSection
            // 
            this.LueSection.EnterMoveNextControl = true;
            this.LueSection.Location = new System.Drawing.Point(827, 1062);
            this.LueSection.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueSection.Name = "LueSection";
            this.LueSection.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.Appearance.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSection.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSection.Properties.DropDownRows = 30;
            this.LueSection.Properties.MaxLength = 16;
            this.LueSection.Properties.NullText = "[Empty]";
            this.LueSection.Properties.PopupWidth = 300;
            this.LueSection.Size = new System.Drawing.Size(367, 28);
            this.LueSection.TabIndex = 152;
            this.LueSection.ToolTip = "F4 : Show/hide list";
            this.LueSection.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSection.EditValueChanged += new System.EventHandler(this.LueSection_EditValueChanged);
            // 
            // TxtSectionOld
            // 
            this.TxtSectionOld.EnterMoveNextControl = true;
            this.TxtSectionOld.Location = new System.Drawing.Point(233, 1062);
            this.TxtSectionOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtSectionOld.Name = "TxtSectionOld";
            this.TxtSectionOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSectionOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSectionOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSectionOld.Properties.Appearance.Options.UseFont = true;
            this.TxtSectionOld.Properties.MaxLength = 80;
            this.TxtSectionOld.Size = new System.Drawing.Size(361, 28);
            this.TxtSectionOld.TabIndex = 84;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(87, 1067);
            this.label33.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(139, 22);
            this.label33.TabIndex = 83;
            this.label33.Text = "Previous Section";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmEmployeeAmendment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1341, 768);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmEmployeeAmendment";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpNameOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLvlCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillageOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrictOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRWOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddressOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicileOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodTypeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMotherOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDtOld.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatusOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlaceOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligionOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumberOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGenderOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKPOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWPOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNameOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmailOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranchOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNoOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobileOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhoneOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDtOld.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOldOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayNameOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDtOld.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSectionOld.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.TextEdit TxtPosCodeOld;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.TextEdit TxtDeptCodeOld;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.TextEdit TxtEmpNameOld;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.TextEdit TxtGrdLvlCodeOld;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label11;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        public DevExpress.XtraEditors.LookUpEdit LueGrdLvlCode;
        public DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.DateEdit DteResignDt;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        public DevExpress.XtraEditors.TextEdit TxtSiteCodeOld;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtRTRW;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.TextEdit TxtVillage;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.TextEdit TxtSubDistrict;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.LookUpEdit LueCity;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCode;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.TextEdit TxtPostalCodeOld;
        public DevExpress.XtraEditors.TextEdit TxtRTRWOld;
        private System.Windows.Forms.Label label22;
        public DevExpress.XtraEditors.TextEdit TxtVillageOld;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.TextEdit TxtSubDistrictOld;
        private System.Windows.Forms.Label label18;
        public DevExpress.XtraEditors.TextEdit TxtCityOld;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.MemoExEdit MeeAddressOld;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.MemoExEdit MeeDomicileOld;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.MemoExEdit MeeDomicile;
        private System.Windows.Forms.Label label62;
        private DevExpress.XtraEditors.LookUpEdit LueBloodTypeOld;
        private DevExpress.XtraEditors.TextEdit TxtMotherOld;
        private System.Windows.Forms.Label LblMotherOld;
        internal DevExpress.XtraEditors.DateEdit DteWeddingDtOld;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private DevExpress.XtraEditors.LookUpEdit LueMaritalStatusOld;
        private DevExpress.XtraEditors.TextEdit TxtBirthPlaceOld;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.LookUpEdit LueReligionOld;
        internal DevExpress.XtraEditors.TextEdit TxtIdNumberOld;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit LueGenderOld;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.LookUpEdit LueBloodType;
        private DevExpress.XtraEditors.TextEdit TxtMother;
        private System.Windows.Forms.Label LblMotherNew;
        internal DevExpress.XtraEditors.DateEdit DteWeddingDt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit LueMaritalStatus;
        private DevExpress.XtraEditors.TextEdit TxtBirthPlace;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraEditors.LookUpEdit LueReligion;
        internal DevExpress.XtraEditors.TextEdit TxtIdNumber;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.LookUpEdit LuePTKPOld;
        private System.Windows.Forms.Label label37;
        private DevExpress.XtraEditors.TextEdit TxtNPWPOld;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LookUpEdit LuePTKP;
        private System.Windows.Forms.Label label39;
        private DevExpress.XtraEditors.TextEdit TxtNPWP;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNameOld;
        private System.Windows.Forms.Label label49;
        internal DevExpress.XtraEditors.TextEdit TxtEmailOld;
        internal DevExpress.XtraEditors.TextEdit TxtBankBranchOld;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNoOld;
        internal DevExpress.XtraEditors.TextEdit TxtMobileOld;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private DevExpress.XtraEditors.LookUpEdit LueBankCodeOld;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtPhoneOld;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcName;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        internal DevExpress.XtraEditors.TextEdit TxtBankBranch;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label60;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        internal DevExpress.XtraEditors.DateEdit DteBirthDtOld;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        private System.Windows.Forms.Label label64;
        public DevExpress.XtraEditors.TextEdit TxtEmpCodeOldOld;
        private System.Windows.Forms.Label label65;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label66;
        public DevExpress.XtraEditors.TextEdit TxtDisplayNameOld;
        private System.Windows.Forms.Label label67;
        internal DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label68;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label69;
        internal DevExpress.XtraEditors.DateEdit DteTGDtOld;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteTGDt;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtSectionOld;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.LookUpEdit LueSection;
    }
}