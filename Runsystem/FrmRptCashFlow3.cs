﻿#region update
/*
    27/11/2020 [WED/VIR] new apps
    05/01/2021 [WED/VIR] ngelanjutin
    18/01/2021 [WED/VIR] bug saat di baris Inflow : Uang Muka
    19/01/2021 [WED/VIR] tambah ambil dari journal untuk yg Beban PRJI di realisasi
    20/01/2021 [WED/VIR] Remuneration+Direct Cost diganti dengan TotalResource
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashFlow3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string[] mMths = { "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agt", "Sep", "Okt", "Nov", "Des" };
        private int[] mMthsCols = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int mStartRowPRJI = 12, mEndRowPRJI = 20;

        #endregion

        #region Constructor

        public FrmRptCashFlow3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueProjectName(ref LueProjectName);
                GetParameter();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'PENJUALAN' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AKUMULASI PENJUALAN' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PERSENTASE/AKUMULASI' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'INFLOW' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'UANG MUKA' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PENERIMAAN TAGIHAN' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'POTONGAN' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'UANG MUKA' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PPN' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PPh' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'JUMLAH INFLOW' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'OUTFLOW' as Col1 ");
            SQL.AppendLine("Union All ");
            #region Old
            //SQL.AppendLine("Select Distinct H.CCtName as Col1 ");
            //SQL.AppendLine("From TblProjectImplementationDtl2 A ");
            //SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.DOcnO = B.DOcNo And B.CancelInd = 'N' And B.Status = 'A' ");
            //SQL.AppendLine("Inner Join TblSOContractREvisionHdr C On B.SOContractDocNo = C.DocNo ");
            //SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
            //SQL.AppendLine("Inner Join TblBOQHDr E On D.BOQDocNo = E.DOcnO And E.LOPDocNo = @DocNo ");
            //SQL.AppendLine("Inner join tBLItem F On A.ResourceItCode = F.ItCOde ");
            //SQL.AppendLine("Inner Join TblItemCostCategory G On F.ItCode = G.ItCode ");
            //SQL.AppendLine("Inner Join TblCostCategory H On G.CCtCode = H.CCtCode ");
            #endregion

            SQL.AppendLine("Select 'Beban Upah Langsung' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Mobilisasi dan Demolisasi' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Akomodasi' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Transportasi' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Perjalanan Dinas' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Survey, Investigasi dan Training' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Kantor dan Pelaporan' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Beban Administrasi Bank' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Beban Overhead' As Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'TOTAL PERTANGGUNGJAWABAN (TOTAL RAP/RESOURCE)' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PENERIMAAN PERSEKOT' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PERTANGGUNGJAWABAN PERSEKOT (SPJ)' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'SALDO PERSEKOT' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'BIAYA DIBAYAR DIMUKA' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PEMBEBANAN BIAYA DI BAYAR DI MUKA' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'SISA BIAYA DI BAYAR DI MUKA' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'JUMLAH PERSEKOT DINAS DAN BOD' as Col1 ");
            SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select 'JUMLAH OUTFLOW' as Col1 ");
            //SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'TOTAL OUTFLOW' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PLUS (MINUS)' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AKUMULASI PLUS (MINUS)' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'PORSI LABA' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'BUNGA BANK' as Col1 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AKUMULASI BUNGA BANK' as Col1 ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Uraian",
                    "Cash Budget",
                    "Realisasi"+Environment.NewLine+"S.d Tahun Lalu", 
                    "Realisasi"+Environment.NewLine + (Int32.Parse(Sm.GetLue(LueYr)) - 1).ToString(),
                    "Total", 
                },
                new int[] 
                {
                    //0
                    60, 

                    //1-5
                    300, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtProjectName, TxtProjectCode, TxtSiteName, TxtSOContractDocNo, TxtSOContractRevisionDocNo, 
                TxtResource
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtDeliveryDays, TxtInclPPN, TxtExclPPN,
                TxtNetto, TxtRAP, TxtRAPToNetto, TxtProfitToNetto
            }, 0);
            
            Sm.ClearGrd(Grd1, false);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueProjectName, "Project")) return;
            ClearData();

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var lCols = new List<Column>();
                var lCOA = new List<COA>();
                var lPRJI = new List<PRJI>();
                var lVCDrop = new List<VCDropping>();
                var lPRDV = new List<ProjectDelivery>();
                var lDRQ = new List<DroppingRequest>();

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetLue(LueProjectName));

                GetHeaderData();
                SetGrd();
                GenerateColumn(ref lCols);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " ",
                    new string[]
                    {
                        "Col1"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        for (int i = 2; i < Grd1.Cols.Count - 1; ++i)
                        {
                            Grd.Cells[Row, i].Value = 0m;
                        }
                    }, true, false, false, false
                );

                ProcessData(ref lCOA, ref lPRJI, ref lVCDrop, ref lPRDV, ref lDRQ, ref lCols);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ProcessData(ref List<COA> lCOA, ref List<PRJI> lPRJI, ref List<VCDropping> lVCDrop, ref List<ProjectDelivery> lPRDV, ref List<DroppingRequest> lDRQ, ref List<Column> lCols)
        {
            string Yr = Sm.GetLue(LueYr);
            string Yr2 = (Int32.Parse(Yr) - 1).ToString();

            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "2.060.", Yr, string.Empty, 3, true); // untuk cash budget
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "2.060.", Yr2, string.Empty, 1, true); // untuk s.d tahun lalu, inflow uang muka
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "2.060.", Yr2, "ARDownpayment", 1, false); // untuk s.d tahun lalu, inflow potongan uang muka
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "2.060.", Yr2, "ARDownpayment", 2, false); // untuk tahun lalu, inflow potongan uang muka
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "2.070.", Yr2, "SalesInvoice5", 1, false); // untuk s.d tahun lalu, inflow potongan PPN
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "8.200.", Yr2, "SalesInvoice5", 1, false); // untuk s.d tahun lalu, inflow potongan PPh
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "2.060.", Yr, string.Empty, 2, true); // untuk tahun berjalan
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "8.1", Yr2, string.Empty, 1, true); // untuk s.d tahun lalu, bunga bank
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "8.1", Yr2, string.Empty, 2, true); // untuk tahun lalu, bunga bank
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "8.1", Yr, string.Empty, 2, true); // untuk tahun berjalan, bunga bank

            /*
            // Beban PRJI
            // Cash Budget
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.001.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.002.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.003.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.004.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.005.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.006.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.007.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.009.", Yr, string.Empty, 3, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.010.", Yr, string.Empty, 3, true);

            // s.d tahun lalu
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.001.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.002.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.003.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.004.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.005.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.006.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.007.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.009.", Yr2, string.Empty, 1, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.010.", Yr2, string.Empty, 1, true);

            // tahun lalu
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.001.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.002.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.003.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.004.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.005.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.006.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.007.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.009.", Yr2, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.010.", Yr2, string.Empty, 2, true);

             */
            // tahun berjalan
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.001.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.002.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.003.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.004.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.005.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.006.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.007.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.009.", Yr, string.Empty, 2, true);
            ComputeTrialBalance(ref lCOA, TxtSOContractDocNo.Text, "5.010.", Yr, string.Empty, 2, true);

            // end Beban PRJI

            ComputePRJI(ref lPRJI, TxtSOContractDocNo.Text, string.Empty, 1, 1); // untuk cash budget PRJI
            ComputePRJI(ref lPRJI, TxtSOContractDocNo.Text, Yr2, 2, 3); // untuk s.d tahun lalu PRJI
            ComputePRJI(ref lPRJI, TxtSOContractDocNo.Text, Yr2, 3, 3); // untuk tahun lalu PRJI
            ComputePRJI(ref lPRJI, TxtSOContractDocNo.Text, Yr, 3, 3); // untuk tahun ini PRJI (realisasi)
            ComputePRJI(ref lPRJI, TxtSOContractDocNo.Text, Yr, 3, 2); // untuk tahun ini PRJI (rencana)
            ComputeVCDropping(ref lVCDrop, TxtSOContractDocNo.Text, Yr2, 2); // untuk s.d tahun lalu penerimaan persekot
            ComputeVCDropping(ref lVCDrop, TxtSOContractDocNo.Text, Yr2, 3); // untuk tahun lalu penerimaan persekot
            ComputeVCDropping(ref lVCDrop, TxtSOContractDocNo.Text, Yr, 3); // untuk tahun ini penerimaan persekot
            ComputeProjectDelivery(ref lPRDV, TxtSOContractDocNo.Text, Yr, Yr2); // untuk s.d tahun lalu, tahun lalu, dan tahun filter penjualan
            ComputeDroppingRequest(ref lDRQ, TxtSOContractDocNo.Text, Yr); // untuk tahun ini, bulan setelah filter

            // ceplokin data dari list ke grid
            ProcessCashBudgetPenjualan();
            ProcessPenjualan(ref lPRDV, ref lCols, Yr, Yr2);
            ProcessInflowUangMuka(ref lCOA, ref lCols, Yr, Yr2);
            ProcessPenerimaanTagihan();
            ProcessPotonganUangMuka(ref lCOA, ref lCols, Yr, Yr2);
            ProcessPotonganPPNPPH(ref lCOA, Yr, Yr2);
            ProcessPRJI(ref lPRJI, ref lCols, Yr, Yr2);
            ProcessVCDropping(ref lVCDrop, ref lCols, Yr, Yr2);
            ProcessDroppingRequest(ref lDRQ, ref lCols, Yr);

            // ceplokin rumus
            CalculateCashBudget();
            CalculateUntilLastYear();
            ComputeLastYear();
            
            ComputeAkumulasiPenjualan(ref lCols);
            ComputePersentaseAkumulasi(ref lCols);
            ComputeInflowUangMuka(ref lCOA, ref lCols);
            ComputeInflowPenerimaanTagihan(ref lCols);
            ComputePotonganUangMuka(ref lCOA, ref lCols);
            ComputePotonganPPnPPH(ref lCols);
            ComputePotongan(ref lCols);
            ComputeJumlahInflow(ref lCols);
            ComputePRJIRencana(ref lPRJI, ref lCols);
            ComputePRJIRealisasi(ref lPRJI, ref lCols);
            ComputePRJIJournal(ref lCOA, ref lCols, Yr2);
            ComputePRJITotalDanPertanggungjawabanPersekot(ref lCols);
            ComputePersekot(ref lCols, ref lVCDrop, ref lDRQ, ref lCOA);
            //ComputePenerimaanPersekotRealisasiDanBiayaDibayarDimukaDanPembebananBiayaDibayarDimuka(ref lVCDrop, ref lCols);
            //ComputePenerimaanPersekotRencanaDanBiayaDibayarDimukaDanPembebananBiayaDibayarDimuka(ref lDRQ, ref lCols);
            //ComputeSaldoPersekot(ref lCols);
            //ComputeSisaBiayaDibayarDimuka(ref lCols);
            ComputeTotal1();
            ComputeRealisasiSampaiBulanLaluDanBulanIni(ref lCols);
            ComputeJumlah(ref lCols);
        }

        private void ComputePRJIJournal(ref List<COA> lCOA, ref List<Column> lCols, string Yr2)
        {
            decimal 
                AmtCB1 = 0m,
                AmtCB2 = 0m,
                AmtCB3 = 0m,
                AmtCB4 = 0m,
                AmtCB5 = 0m,
                AmtCB6 = 0m,
                AmtCB7 = 0m,
                AmtCB9 = 0m,
                AmtCB10 = 0m, 
                AmtCBToLastYear1 = 0m,
                AmtCBToLastYear2 = 0m,
                AmtCBToLastYear3 = 0m,
                AmtCBToLastYear4 = 0m,
                AmtCBToLastYear5 = 0m,
                AmtCBToLastYear6 = 0m,
                AmtCBToLastYear7 = 0m,
                AmtCBToLastYear9 = 0m,
                AmtCBToLastYear10 = 0m,
                AmtCBLastYear1 = 0m,
                AmtCBLastYear2 = 0m,
                AmtCBLastYear3 = 0m,
                AmtCBLastYear4 = 0m,
                AmtCBLastYear5 = 0m,
                AmtCBLastYear6 = 0m,
                AmtCBLastYear7 = 0m,
                AmtCBLastYear9 = 0m,
                AmtCBLastYear10 = 0m,
                AmtMth1_1 = 0m,
                AmtMth1_2 = 0m,
                AmtMth1_3 = 0m,
                AmtMth1_4 = 0m,
                AmtMth1_5 = 0m,
                AmtMth1_6 = 0m,
                AmtMth1_7 = 0m,
                AmtMth1_9 = 0m,
                AmtMth1_10 = 0m,
                AmtMth2_1 = 0m,
                AmtMth2_2 = 0m,
                AmtMth2_3 = 0m,
                AmtMth2_4 = 0m,
                AmtMth2_5 = 0m,
                AmtMth2_6 = 0m,
                AmtMth2_7 = 0m,
                AmtMth2_9 = 0m,
                AmtMth2_10 = 0m,
                AmtMth3_1 = 0m,
                AmtMth3_2 = 0m,
                AmtMth3_3 = 0m,
                AmtMth3_4 = 0m,
                AmtMth3_5 = 0m,
                AmtMth3_6 = 0m,
                AmtMth3_7 = 0m,
                AmtMth3_9 = 0m,
                AmtMth3_10 = 0m,
                AmtMth4_1 = 0m,
                AmtMth4_2 = 0m,
                AmtMth4_3 = 0m,
                AmtMth4_4 = 0m,
                AmtMth4_5 = 0m,
                AmtMth4_6 = 0m,
                AmtMth4_7 = 0m,
                AmtMth4_9 = 0m,
                AmtMth4_10 = 0m,
                AmtMth5_1 = 0m,
                AmtMth5_2 = 0m,
                AmtMth5_3 = 0m,
                AmtMth5_4 = 0m,
                AmtMth5_5 = 0m,
                AmtMth5_6 = 0m,
                AmtMth5_7 = 0m,
                AmtMth5_9 = 0m,
                AmtMth5_10 = 0m,
                AmtMth6_1 = 0m,
                AmtMth6_2 = 0m,
                AmtMth6_3 = 0m,
                AmtMth6_4 = 0m,
                AmtMth6_5 = 0m,
                AmtMth6_6 = 0m,
                AmtMth6_7 = 0m,
                AmtMth6_9 = 0m,
                AmtMth6_10 = 0m,
                AmtMth7_1 = 0m,
                AmtMth7_2 = 0m,
                AmtMth7_3 = 0m,
                AmtMth7_4 = 0m,
                AmtMth7_5 = 0m,
                AmtMth7_6 = 0m,
                AmtMth7_7 = 0m,
                AmtMth7_9 = 0m,
                AmtMth7_10 = 0m,
                AmtMth8_1 = 0m,
                AmtMth8_2 = 0m,
                AmtMth8_3 = 0m,
                AmtMth8_4 = 0m,
                AmtMth8_5 = 0m,
                AmtMth8_6 = 0m,
                AmtMth8_7 = 0m,
                AmtMth8_9 = 0m,
                AmtMth8_10 = 0m,
                AmtMth9_1 = 0m,
                AmtMth9_2 = 0m,
                AmtMth9_3 = 0m,
                AmtMth9_4 = 0m,
                AmtMth9_5 = 0m,
                AmtMth9_6 = 0m,
                AmtMth9_7 = 0m,
                AmtMth9_9 = 0m,
                AmtMth9_10 = 0m,
                AmtMth10_1 = 0m,
                AmtMth10_2 = 0m,
                AmtMth10_3 = 0m,
                AmtMth10_4 = 0m,
                AmtMth10_5 = 0m,
                AmtMth10_6 = 0m,
                AmtMth10_7 = 0m,
                AmtMth10_9 = 0m,
                AmtMth10_10 = 0m,
                AmtMth11_1 = 0m,
                AmtMth11_2 = 0m,
                AmtMth11_3 = 0m,
                AmtMth11_4 = 0m,
                AmtMth11_5 = 0m,
                AmtMth11_6 = 0m,
                AmtMth11_7 = 0m,
                AmtMth11_9 = 0m,
                AmtMth11_10 = 0m,
                AmtMth12_1 = 0m,
                AmtMth12_2 = 0m,
                AmtMth12_3 = 0m,
                AmtMth12_4 = 0m,
                AmtMth12_5 = 0m,
                AmtMth12_6 = 0m,
                AmtMth12_7 = 0m,
                AmtMth12_9 = 0m,
                AmtMth12_10 = 0m
                ;

            foreach (var x in lCOA.Where(w => w.MenuParam == string.Empty))
            {
                if (x.Mth == "00")
                {
                    if (x.AcNo.Contains("5.001")) AmtCB1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.002")) AmtCB2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.003")) AmtCB3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.004")) AmtCB4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.005")) AmtCB5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.006")) AmtCB6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.007")) AmtCB7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.009")) AmtCB9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.010")) AmtCB10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                }

                if (Int32.Parse(x.Yr) < Int32.Parse(Yr2))
                {
                    if (x.AcNo.Contains("5.001")) AmtCBToLastYear1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.002")) AmtCBToLastYear2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.003")) AmtCBToLastYear3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.004")) AmtCBToLastYear4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.005")) AmtCBToLastYear5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.006")) AmtCBToLastYear6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.007")) AmtCBToLastYear7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.009")) AmtCBToLastYear9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.010")) AmtCBToLastYear10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                }

                if (Int32.Parse(x.Yr) == Int32.Parse(Yr2))
                {
                    if (x.AcNo.Contains("5.001")) AmtCBLastYear1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.002")) AmtCBLastYear2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.003")) AmtCBLastYear3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.004")) AmtCBLastYear4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.005")) AmtCBLastYear5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.006")) AmtCBLastYear6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.007")) AmtCBLastYear7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.009")) AmtCBLastYear9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (x.AcNo.Contains("5.010")) AmtCBLastYear10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                }

                if (x.Yr == Sm.GetLue(LueYr) && x.Mth != "00")
                {
                    if (x.Mth == "01")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth1_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth1_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth1_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth1_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth1_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth1_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth1_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth1_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth1_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "02")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth2_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth2_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth2_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth2_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth2_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth2_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth2_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth2_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth2_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "03")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth3_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth3_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth3_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth3_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth3_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth3_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth3_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth3_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth3_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "04")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth4_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth4_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth4_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth4_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth4_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth4_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth4_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth4_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth4_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "05")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth5_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth5_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth5_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth5_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth5_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth5_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth5_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth5_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth5_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "06")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth6_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth6_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth6_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth6_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth6_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth6_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth6_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth6_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth6_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "07")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth7_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth7_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth7_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth7_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth7_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth7_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth7_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth7_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth7_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "08")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth8_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth8_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth8_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth8_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth8_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth8_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth8_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth8_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth8_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "09")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth9_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth9_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth9_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth9_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth9_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth9_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth9_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth9_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth9_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "10")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth10_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth10_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth10_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth10_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth10_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth10_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth10_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth10_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth10_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "11")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth11_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth11_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth11_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth11_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth11_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth11_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth11_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth11_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth11_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }

                    if (x.Mth == "12")
                    {
                        if (x.AcNo.Contains("5.001")) AmtMth12_1 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.002")) AmtMth12_2 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.003")) AmtMth12_3 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.004")) AmtMth12_4 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.005")) AmtMth12_5 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.006")) AmtMth12_6 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.007")) AmtMth12_7 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.009")) AmtMth12_9 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                        if (x.AcNo.Contains("5.010")) AmtMth12_10 += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }
                }
            }

            // ceplok grid
            foreach (var x in lCols.Where(w => w.MthInd && Int32.Parse(w.Mth) <= Int32.Parse(Sm.GetLue(LueMth))))
            {
                if (x.Mth == "01")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth1_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth1_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth1_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth1_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth1_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth1_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth1_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth1_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth1_10;
                }

                if (x.Mth == "02")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth2_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth2_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth2_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth2_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth2_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth2_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth2_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth2_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth2_10;
                }

                if (x.Mth == "03")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth3_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth3_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth3_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth3_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth3_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth3_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth3_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth3_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth3_10;
                }

                if (x.Mth == "04")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth4_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth4_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth4_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth4_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth4_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth4_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth4_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth4_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth4_10;
                }

                if (x.Mth == "05")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth5_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth5_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth5_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth5_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth5_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth5_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth5_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth5_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth5_10;
                }

                if (x.Mth == "06")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth6_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth6_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth6_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth6_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth6_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth6_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth6_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth6_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth6_10;
                }

                if (x.Mth == "07")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth7_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth7_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth7_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth7_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth7_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth7_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth7_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth7_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth7_10;
                }

                if (x.Mth == "08")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth8_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth8_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth8_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth8_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth8_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth8_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth8_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth8_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth8_10;
                }

                if (x.Mth == "09")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth9_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth9_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth9_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth9_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth9_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth9_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth9_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth9_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth9_10;
                }

                if (x.Mth == "10")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth10_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth10_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth10_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth10_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth10_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth10_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth10_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth10_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth10_10;
                }

                if (x.Mth == "11")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth11_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth11_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth11_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth11_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth11_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth11_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth11_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth11_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth11_10;
                }

                if (x.Mth == "12")
                {
                    Grd1.Cells[12, x.Cols].Value = Sm.GetGrdDec(Grd1, 12, x.Cols) + AmtMth12_1;
                    Grd1.Cells[13, x.Cols].Value = Sm.GetGrdDec(Grd1, 13, x.Cols) + AmtMth12_2;
                    Grd1.Cells[14, x.Cols].Value = Sm.GetGrdDec(Grd1, 14, x.Cols) + AmtMth12_3;
                    Grd1.Cells[15, x.Cols].Value = Sm.GetGrdDec(Grd1, 15, x.Cols) + AmtMth12_4;
                    Grd1.Cells[16, x.Cols].Value = Sm.GetGrdDec(Grd1, 16, x.Cols) + AmtMth12_5;
                    Grd1.Cells[17, x.Cols].Value = Sm.GetGrdDec(Grd1, 17, x.Cols) + AmtMth12_6;
                    Grd1.Cells[18, x.Cols].Value = Sm.GetGrdDec(Grd1, 18, x.Cols) + AmtMth12_7;
                    Grd1.Cells[19, x.Cols].Value = Sm.GetGrdDec(Grd1, 19, x.Cols) + AmtMth12_9;
                    Grd1.Cells[20, x.Cols].Value = Sm.GetGrdDec(Grd1, 20, x.Cols) + AmtMth12_10;
                }
            }
        }

        private void ComputeRealisasiSampaiBulanLaluDanBulanIni(ref List<Column> lCols)
        {
            int index = 6;
            int mCols1 = 0, mCols2 = 0;
            foreach (var x in lCols.Where(w => w.Mth == Sm.GetLue(LueMth)))
            {
                mCols1 = x.Cols - 1;
                mCols2 = x.Cols + 1;
            }

            for (int i = 0; i < 35; ++i)
            {
                decimal Amt1 = 0m;

                for (int j = index; j < mCols1; ++j)
                {
                    Amt1 += Sm.GetGrdDec(Grd1, i, j);
                }
                Grd1.Cells[i, mCols1].Value = Amt1;

                Grd1.Cells[i, mCols2].Value = Amt1 + Sm.GetGrdDec(Grd1, i, mCols2 - 1);
            }
        }

        private void ComputeJumlah(ref List<Column> lCols)
        {
            int mCols = 0, mColsJumlah = 0, mColsPrognosa = 0, mColsTotal = 0, mColsSisa = 0;

            foreach (var x in lCols.Where(w => w.Mth == Sm.GetLue(LueMth)))
            {
                if (x.Mth != "12")
                    mCols = x.Cols + 2;
                else
                    mCols = x.Cols + 1;
            }
            foreach(var x in lCols.Where(w => w.Mth == "13")) mColsJumlah = x.Cols;
            foreach(var x in lCols.Where(w => w.Mth == "14")) mColsPrognosa = x.Cols;
            foreach(var x in lCols.Where(w => w.Mth == "15")) mColsTotal = x.Cols;
            foreach(var x in lCols.Where(w => w.Mth == "16")) mColsSisa = x.Cols;

            for (int i = 0; i < 35; ++i) // rows
            {
                decimal Amt = 0m;
                for (int j = mCols; j < 21; ++j)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, j);
                }

                Grd1.Cells[i, mColsJumlah].Value = Amt;
                Grd1.Cells[i, mColsPrognosa].Value = Amt + Sm.GetGrdDec(Grd1, i, mCols - 1);
                Grd1.Cells[i, mColsTotal].Value = Sm.GetGrdDec(Grd1, i, mColsPrognosa) + Sm.GetGrdDec(Grd1, i, 5);
                Grd1.Cells[i, mColsSisa].Value = Sm.GetGrdDec(Grd1, i, 2) - Sm.GetGrdDec(Grd1, i, mColsTotal);
            }
        }

        private void ComputePersekot(ref List<Column> lCols, ref List<VCDropping> lVCDrop, ref List<DroppingRequest> lDRQ, ref List<COA> lCOA)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                #region penerimaan persekot realisasi, biaya dibayar dimuka, pembebanan biaya dibayar dimuka
                foreach (var x in lCols
                    .Where(w =>
                        Int32.Parse(w.Mth) <= Int32.Parse(Sm.GetLue(LueMth)) &&
                        w.Cols == i &&
                        w.MthInd))
                {
                    foreach (var y in lVCDrop.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth))
                    {
                        Grd1.Cells[22, x.Cols].Value = y.Amt;
                        Grd1.Cells[25, x.Cols].Value = y.Amt;
                        Grd1.Cells[26, x.Cols].Value = y.Amt;

                        if (x.Mth == Sm.GetLue(LueMth))
                        {
                            decimal Amt = 0m;
                            for (int j = index; j < x.Cols - 1; ++j)
                            {
                                Amt += Sm.GetGrdDec(Grd1, 22, j);
                            }

                            Grd1.Cells[22, x.Cols - 1].Value = Amt;
                            Grd1.Cells[25, x.Cols - 1].Value = Amt;
                            Grd1.Cells[26, x.Cols - 1].Value = Amt;
                            Amt = 0m;

                            for (int j = index; j <= x.Cols; ++j)
                            {
                                if (j != x.Cols - 1)
                                {
                                    Amt += Sm.GetGrdDec(Grd1, 22, j);
                                }
                            }

                            Grd1.Cells[22, x.Cols + 1].Value = Amt;
                            Grd1.Cells[25, x.Cols + 1].Value = Amt;
                            Grd1.Cells[26, x.Cols + 1].Value = Amt;
                        }
                    }
                }
                #endregion

                #region penerimaan persekot rencana, biaya dibayar dimuka, pembebanan biaya dibayar dimuka
                foreach (var x in lCols
                    .Where(w =>
                        Int32.Parse(w.Mth) > Int32.Parse(Sm.GetLue(LueMth)) &&
                        w.Cols == i &&
                        w.MthInd))
                {
                    foreach (var y in lDRQ.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth))
                    {
                        Grd1.Cells[22, x.Cols].Value = y.Amt;
                        Grd1.Cells[25, x.Cols].Value = y.Amt;
                        Grd1.Cells[26, x.Cols].Value = y.Amt;
                    }
                }
                #endregion

                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    #region saldo persekot
                    Grd1.Cells[24, x.Cols].Value = Sm.GetGrdDec(Grd1, 22, x.Cols) - Sm.GetGrdDec(Grd1, 23, x.Cols);
                    #endregion

                    #region sisa biaya dibayar dimuka
                    Grd1.Cells[27, x.Cols].Value = Sm.GetGrdDec(Grd1, 24, x.Cols) + Sm.GetGrdDec(Grd1, 25, x.Cols);
                    #endregion

                    #region jumlah persekotan dinas dan bdd
                    Grd1.Cells[28, x.Cols].Value = Sm.GetGrdDec(Grd1, 22, x.Cols) + Sm.GetGrdDec(Grd1, 23, x.Cols);
                    #endregion

                    #region total outflow
                    Grd1.Cells[29, x.Cols].Value = Sm.GetGrdDec(Grd1, 21, x.Cols) + Sm.GetGrdDec(Grd1, 28, x.Cols);
                    #endregion

                    #region plus minus
                    Grd1.Cells[30, x.Cols].Value = Sm.GetGrdDec(Grd1, 10, x.Cols) - Sm.GetGrdDec(Grd1, 29, x.Cols);
                    #endregion

                    #region akumulasi plus minus
                    if (x.Mth == "01")
                    {
                        Grd1.Cells[31, x.Cols - 1].Value = Sm.GetGrdDec(Grd1, 10, x.Cols) - Sm.GetGrdDec(Grd1, 29, x.Cols);
                        Grd1.Cells[31, x.Cols].Value = Sm.GetGrdDec(Grd1, 10, x.Cols) - Sm.GetGrdDec(Grd1, 29, x.Cols);
                        Grd1.Cells[31, x.Cols + 1].Value = Sm.GetGrdDec(Grd1, 10, x.Cols) - Sm.GetGrdDec(Grd1, 29, x.Cols);
                    }
                    else
                    {
                        if (x.MthInd)
                            Grd1.Cells[31, x.Cols].Value = Sm.GetGrdDec(Grd1, 30, x.Cols) + Sm.GetGrdDec(Grd1, 31, x.Cols - 1);
                    }
                    #endregion

                    #region porsi laba
                    Grd1.Cells[32, x.Cols].Value = Sm.GetGrdDec(Grd1, 0, x.Cols) - Sm.GetGrdDec(Grd1, 21, x.Cols);
                    #endregion

                    #region bunga bank & akumulasi bunga bank
                    foreach (var y in lCOA.Where(w => 
                        w.Yr == Sm.GetLue(LueYr) && 
                        w.Mth == x.Mth &&
                        w.AcNo.Contains("8.1") &&
                        w.MenuParam == string.Empty
                        ))
                    {
                        Grd1.Cells[33, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                        Grd1.Cells[34, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }
                    #endregion
                }
            }
        }

        private void ComputeSisaBiayaDibayarDimuka(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    Grd1.Cells[27, x.Cols].Value = Sm.GetGrdDec(Grd1, 24, x.Cols) + Sm.GetGrdDec(Grd1, 25, x.Cols);
                }
            }
        }

        private void ComputeSaldoPersekot(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    Grd1.Cells[24, x.Cols].Value = Sm.GetGrdDec(Grd1, 22, x.Cols) - Sm.GetGrdDec(Grd1, 23, x.Cols);
                }
            }
        }

        private void ComputePenerimaanPersekotRencanaDanBiayaDibayarDimukaDanPembebananBiayaDibayarDimuka(ref List<DroppingRequest> lDRQ, ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols
                    .Where(w =>
                        Int32.Parse(w.Mth) > Int32.Parse(Sm.GetLue(LueMth)) &&
                        w.Cols == i &&
                        w.MthInd))
                {
                    foreach (var y in lDRQ.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth))
                    {
                        Grd1.Cells[22, x.Cols].Value = y.Amt;
                        Grd1.Cells[25, x.Cols].Value = y.Amt;
                        Grd1.Cells[26, x.Cols].Value = y.Amt;
                    }
                }
            }
        }

        private void ComputePenerimaanPersekotRealisasiDanBiayaDibayarDimukaDanPembebananBiayaDibayarDimuka(ref List<VCDropping> lVCDrop, ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols
                    .Where(w =>
                        Int32.Parse(w.Mth) <= Int32.Parse(Sm.GetLue(LueMth)) &&
                        w.Cols == i &&
                        w.MthInd))
                {
                    foreach (var y in lVCDrop.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth))
                    {
                        Grd1.Cells[22, x.Cols].Value = y.Amt;
                        Grd1.Cells[25, x.Cols].Value = y.Amt;
                        Grd1.Cells[26, x.Cols].Value = y.Amt;

                        if (x.Mth == Sm.GetLue(LueMth))
                        {
                            decimal Amt = 0m;
                            for (int j = index; j < x.Cols - 1; ++j)
                            {
                                Amt += Sm.GetGrdDec(Grd1, 22, j);
                            }

                            Grd1.Cells[22, x.Cols - 1].Value = Amt;
                            Grd1.Cells[25, x.Cols - 1].Value = Amt;
                            Grd1.Cells[26, x.Cols - 1].Value = Amt;
                            Amt = 0m;

                            for (int j = index; j <= x.Cols; ++j)
                            {
                                if (j != x.Cols - 1)
                                {
                                    Amt += Sm.GetGrdDec(Grd1, 22, j);
                                }
                            }

                            Grd1.Cells[22, x.Cols + 1].Value = Amt;
                            Grd1.Cells[25, x.Cols + 1].Value = Amt;
                            Grd1.Cells[26, x.Cols + 1].Value = Amt;
                        }
                    }
                }
            }
        }

        private void ComputePRJITotalDanPertanggungjawabanPersekot(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    decimal Amt = 0m;
                    for (int Row = mStartRowPRJI; Row <= mEndRowPRJI; ++Row)
                    {
                        Amt += Sm.GetGrdDec(Grd1, Row, x.Cols);
                    }
                    Grd1.Cells[21, x.Cols].Value = Amt;
                    Grd1.Cells[23, x.Cols].Value = Amt;
                }
            }
        }

        private void ComputePRJIRealisasi(ref List<PRJI> lPRJI, ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols
                    .Where(w =>
                        Int32.Parse(w.Mth) <= Int32.Parse(Sm.GetLue(LueMth)) &&
                        w.Cols == i &&
                        w.MthInd))
                {
                    foreach (var y in lPRJI.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth && w.DocTypeTrans == "3"))
                    {
                        for (int Row = mStartRowPRJI; Row <= mEndRowPRJI; ++Row)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 1) == y.CCtName)
                            {
                                Grd1.Cells[Row, x.Cols].Value = y.Amt;

                                if (x.Mth == Sm.GetLue(LueMth))
                                {
                                    decimal Amt = 0m;
                                    for (int j = index; j < x.Cols - 1; ++j)
                                    {
                                        Amt += Sm.GetGrdDec(Grd1, Row, j);
                                    }

                                    Grd1.Cells[Row, x.Cols - 1].Value = Amt;
                                    Amt = 0m;

                                    for (int j = index; j <= x.Cols; ++j)
                                    {
                                        if (j != x.Cols - 1)
                                        {
                                            Amt += Sm.GetGrdDec(Grd1, Row, j);
                                        }
                                    }

                                    Grd1.Cells[Row, x.Cols + 1].Value = Amt;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ComputePRJIRencana(ref List<PRJI> lPRJI, ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols
                    .Where(w => 
                        Int32.Parse(w.Mth) > Int32.Parse(Sm.GetLue(LueMth)) && 
                        w.Cols == i && 
                        w.MthInd))
                {
                    foreach (var y in lPRJI.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth && w.DocTypeTrans == "2"))
                    {
                        for (int Row = mStartRowPRJI; Row <= mEndRowPRJI; ++Row)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 1) == y.CCtName)
                            {
                                Grd1.Cells[Row, x.Cols].Value = y.Amt;
                            }
                        }
                    }
                }
            }
        }

        private void ComputeJumlahInflow(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    Grd1.Cells[10, x.Cols].Value = Sm.GetGrdDec(Grd1, 4, x.Cols) + Sm.GetGrdDec(Grd1, 5, x.Cols) - Sm.GetGrdDec(Grd1, 6, x.Cols);
                }
            }
        }

        private void ComputePotongan(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    Grd1.Cells[6, x.Cols].Value = Sm.GetGrdDec(Grd1, 7, x.Cols) + Sm.GetGrdDec(Grd1, 8, x.Cols) + Sm.GetGrdDec(Grd1, 9, x.Cols);
                }
            }
        }

        private void ComputePotonganPPnPPH(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = index; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.Cols == i))
                {
                    Grd1.Cells[8, x.Cols].Value = ((Sm.GetGrdDec(Grd1, 5, x.Cols) - Sm.GetGrdDec(Grd1, 7, x.Cols)) / 1.1m) * 0.1m;
                    Grd1.Cells[9, x.Cols].Value = ((Sm.GetGrdDec(Grd1, 5, x.Cols) - Sm.GetGrdDec(Grd1, 7, x.Cols)) / 1.1m) * 0.04m;
                }
            }
        }

        private void ComputePotonganUangMuka(ref List<COA> lCOA, ref List<Column> lCols)
        {
            int index = 6;
            for (int i = 6; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.MthInd && w.Cols == i))
                {
                    foreach (var y in lCOA.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth && w.AcNo.Contains("2.060") && w.MenuParam == "ARDownpayment"))
                    {
                        if (x.Mth == Sm.GetLue(LueMth) && x.Mth == "01")
                        {
                            Grd1.Cells[7, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                            Grd1.Cells[7, x.Cols + 1].Value = Sm.GetGrdDec(Grd1, 7, x.Cols);
                        }
                        else
                        {
                            if (x.Mth == Sm.GetLue(LueMth))
                            {
                                decimal Amt = 0m;
                                for (int j = index; j < x.Cols - 1; ++j)
                                {
                                    Amt += Sm.GetGrdDec(Grd1, 7, j);
                                }

                                Grd1.Cells[7, x.Cols - 1].Value = Amt;

                                Grd1.Cells[7, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                                Amt = 0m;

                                for (int j = index; j <= x.Cols; ++j)
                                {
                                    if (j != x.Cols - 1)
                                    {
                                        Amt += Sm.GetGrdDec(Grd1, 7, j);
                                    }
                                }

                                Grd1.Cells[7, x.Cols + 1].Value = Amt;
                            }
                            else
                            {
                                Grd1.Cells[7, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                            }
                        }
                    }
                }
            }
        }

        private void ComputeInflowPenerimaanTagihan(ref List<Column> lCols)
        {
            // karna dia ambil dari nilai kontrak, maka dibuat nilainya itu muncul sesuai bulan dan tahun terakhir revisi terakhir
            // jika tahun dan bulannya tidak ada di list (tahun di saat filter lebih besar dari tahun terakhir revisi), maka akan dimasukkan di 'sampai dengan bulan lalu'
            // jika tahun dan bulannya tidak ada di list (tahun di saat filter lebih kecil dari tahun terakhir revisi), maka data tidak akan muncul karena belum masuk masa revisi terakhir)

            int index = 6;
            decimal mInclPPN = Decimal.Parse(TxtInclPPN.Text);
            string MaxRevDt = Sm.GetValue("Select Left(DocDt, 6) From TblSOContractRevisionHdr Where DocNo = @Param; ", TxtSOContractRevisionDocNo.Text);

            string MaxRevYr = Sm.Left(MaxRevDt, 4);
            string MaxRevMth = Sm.Right(MaxRevDt, 2);
            bool IsThisYear = MaxRevYr == Sm.GetLue(LueYr);

            if (Int32.Parse(MaxRevYr) <= Int32.Parse(Sm.GetLue(LueYr)))
            {
                if (IsThisYear)
                {
                    for (int i = index; i < 21; ++i)
                    {
                        foreach (var x in lCols.Where(w => w.MthInd && w.Cols == i))
                        {
                            if (x.Mth == MaxRevMth) Grd1.Cells[5, x.Cols].Value = mInclPPN;
                            if (x.Mth == Sm.GetLue(LueMth) && x.Mth == "01")
                            {
                                Grd1.Cells[5, x.Cols].Value = mInclPPN;
                                Grd1.Cells[5, x.Cols + 1].Value = mInclPPN;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = index; i < 21; ++i)
                    {
                        foreach (var x in lCols.Where(w => w.MthInd && w.Cols == i))
                        {
                            if (x.Mth == Sm.GetLue(LueMth))
                            {
                                Grd1.Cells[5, x.Cols - 1].Value = mInclPPN;
                                Grd1.Cells[5, x.Cols + 1].Value = mInclPPN;
                            }
                        }
                    }
                }
            }
        }

        private void ComputeInflowUangMuka(ref List<COA> lCOA, ref List<Column> lCols)
        {
            int index = 6;
            for (int i = 6; i < 21; ++i)
            {
                foreach (var x in lCols.Where(w => w.MthInd && w.Cols == i))
                {
                    foreach (var y in lCOA.Where(w => w.Yr == Sm.GetLue(LueYr) && w.Mth == x.Mth && w.AcNo.Contains("2.060")))
                    {
                        if (x.Mth == Sm.GetLue(LueMth) && x.Mth == "01")
                        {
                            Grd1.Cells[4, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                            Grd1.Cells[4, x.Cols + 1].Value = Sm.GetGrdDec(Grd1, 4, x.Cols);
                        }
                        else
                        {
                            if (x.Mth == Sm.GetLue(LueMth))
                            {
                                decimal Amt = 0m;
                                for (int j = index; j < x.Cols - 1; ++j)
                                {
                                    Amt += Sm.GetGrdDec(Grd1, 4, j);
                                }

                                Grd1.Cells[4, x.Cols - 1].Value = Amt;

                                Grd1.Cells[4, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                                Amt = 0m;

                                for (int j = index; j <= x.Cols; ++j)
                                {
                                    if (j != x.Cols - 1)
                                    {
                                        Amt += Sm.GetGrdDec(Grd1, 4, j);
                                    }
                                }

                                Grd1.Cells[4, x.Cols + 1].Value = Amt;
                            }
                            else
                            {
                                Grd1.Cells[4, x.Cols].Value = (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                            }
                        }
                    }
                }
            }
        }

        private void ComputePersentaseAkumulasi(ref List<Column> lCols)
        {
            int index = 6;
            decimal mExclPPN = Decimal.Parse(TxtExclPPN.Text);
            if (mExclPPN != 0m)
            {
                for (int i = index; i < 21; ++i)
                {
                    foreach (var x in lCols.Where(w => w.Cols == i))
                    {
                        Grd1.Cells[2, x.Cols].Value = (Sm.GetGrdDec(Grd1, 1, x.Cols) / mExclPPN) * 100m;
                        #region Old
                        //if (x.Mth == Sm.GetLue(LueMth) && x.Mth == "01")
                        //{
                        //    Grd1.Cells[2, x.Cols].Value = Sm.GetGrdDec(Grd1, 1, x.Cols) / mExclPPN;
                        //    Grd1.Cells[2, x.Cols + 1].Value = Sm.GetGrdDec(Grd1, 2, x.Cols);
                        //}
                        //else
                        //{
                        //    if (x.Mth == Sm.GetLue(LueMth))
                        //    {
                        //        decimal Amt = 0m;
                        //        for (int j = index; j < x.Cols - 1; ++j)
                        //        {
                        //            Amt += Sm.GetGrdDec(Grd1, 2, j);
                        //        }

                        //        Grd1.Cells[2, x.Cols - 1].Value = Amt;

                        //        Grd1.Cells[2, x.Cols].Value = (Sm.GetGrdDec(Grd1, 1, x.Cols) / mExclPPN) * 100m;
                        //        Amt = 0m;

                        //        for (int j = index; j <= x.Cols; ++j)
                        //        {
                        //            if (j != x.Cols - 1)
                        //            {
                        //                Amt += Sm.GetGrdDec(Grd1, 2, j);
                        //            }
                        //        }

                        //        Grd1.Cells[2, x.Cols + 1].Value = Amt;
                        //    }
                        //    else
                        //    {
                        //        Grd1.Cells[2, x.Cols].Value = (Sm.GetGrdDec(Grd1, 1, x.Cols) / mExclPPN) * 100m;
                        //    }
                        //}
                        #endregion
                    }
                }
            }
        }

        private void ComputeAkumulasiPenjualan(ref List<Column> lCols)
        {
            int index = 6;
            for (int i = 6; i < 21; ++i)
            {
                foreach(var x in lCols.Where(w => w.MthInd && w.Cols == i))
                {
                    if (x.Mth == Sm.GetLue(LueMth) && x.Mth == "01")
                    {
                        Grd1.Cells[1, x.Cols].Value = Sm.GetGrdDec(Grd1, 1, 5) + Sm.GetGrdDec(Grd1, 0, x.Cols);
                        Grd1.Cells[1, x.Cols + 1].Value = Sm.GetGrdDec(Grd1, 1, x.Cols);
                    }
                    else
                    {
                        if (x.Mth == Sm.GetLue(LueMth))
                        {
                            decimal Amt = 0m;
                            for (int j = index; j < x.Cols - 1; ++j)
                            {
                                Amt += Sm.GetGrdDec(Grd1, 1, j);
                            }

                            Grd1.Cells[1, x.Cols - 1].Value = Amt;

                            Grd1.Cells[1, x.Cols].Value = Sm.GetGrdDec(Grd1, 1, x.Cols - 2) + Sm.GetGrdDec(Grd1, 0, x.Cols);
                            Amt = 0m;

                            for (int j = index; j <= x.Cols; ++j)
                            {
                                if (j != x.Cols - 1)
                                {
                                    Amt += Sm.GetGrdDec(Grd1, 1, j);
                                }
                            }

                            Grd1.Cells[1, x.Cols + 1].Value = Amt;
                        }
                        else
                        {
                            if (Int32.Parse(x.Mth) - Int32.Parse(Sm.GetLue(LueMth)) == 1)
                            {
                                Grd1.Cells[1, x.Cols].Value = Sm.GetGrdDec(Grd1, 1, x.Cols - 2) + Sm.GetGrdDec(Grd1, 0, x.Cols);
                            }
                            else
                            {
                                Grd1.Cells[1, x.Cols].Value = Sm.GetGrdDec(Grd1, 1, x.Cols - 1) + Sm.GetGrdDec(Grd1, 0, x.Cols);
                            }
                        }
                    }
                }
            }
        }

        private void ComputeTotal1()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i, 3) + Sm.GetGrdDec(Grd1, i, 4);
            }
        }

        private void CalculateCashBudget()
        {
            if (Decimal.Parse(TxtInclPPN.Text) != 0m)
            {
                Grd1.Cells[8, 2].Value = (Decimal.Parse(TxtInclPPN.Text) / 1.1m) * 0.1m;
                Grd1.Cells[9, 2].Value = (Decimal.Parse(TxtInclPPN.Text) / 1.1m) * 0.04m;
                Grd1.Cells[6, 2].Value = Sm.GetGrdDec(Grd1, 7, 2) + Sm.GetGrdDec(Grd1, 8, 2) + Sm.GetGrdDec(Grd1, 9, 2);
                Grd1.Cells[10, 2].Value = Sm.GetGrdDec(Grd1, 4, 2) + Sm.GetGrdDec(Grd1, 5, 2) - Sm.GetGrdDec(Grd1, 6, 2);
            }

            if(Decimal.Parse(TxtExclPPN.Text) != 0m)
            {
                Grd1.Cells[11, 2].Value = Sm.GetGrdDec(Grd1, 10, 2) / Decimal.Parse(TxtExclPPN.Text);
            }

            decimal Amt = 0;

            if (mEndRowPRJI - mStartRowPRJI > -1)
            {
                for (int i = mStartRowPRJI; i <= mEndRowPRJI; ++i)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, 2);
                }
            }

            Grd1.Cells[mEndRowPRJI + 1, 2].Value = Amt;

            Grd1.Cells[mEndRowPRJI + 9, 2].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 1, 2) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 8, 2);
            Grd1.Cells[mEndRowPRJI + 10, 2].Value = Grd1.Cells[mEndRowPRJI + 11, 2].Value = Sm.GetGrdDec(Grd1, 10, 2) - Sm.GetGrdDec(Grd1, mEndRowPRJI + 9, 2);
            Grd1.Cells[mEndRowPRJI + 12, 2].Value = Decimal.Parse(TxtExclPPN.Text) - Sm.GetGrdDec(Grd1, mEndRowPRJI + 1, 2);
        }

        private void CalculateUntilLastYear()
        {
            if (Decimal.Parse(TxtExclPPN.Text) != 0m) Grd1.Cells[2, 3].Value = Sm.GetGrdDec(Grd1, 1, 3) / Decimal.Parse(TxtExclPPN.Text);
            Grd1.Cells[6, 3].Value = Sm.GetGrdDec(Grd1, 7, 3) + Sm.GetGrdDec(Grd1, 8, 3) + Sm.GetGrdDec(Grd1, 9, 3);
            Grd1.Cells[10, 3].Value = Sm.GetGrdDec(Grd1, 4, 3) + Sm.GetGrdDec(Grd1, 5, 3) - Sm.GetGrdDec(Grd1, 6, 3);

            decimal Amt = 0;

            if (mEndRowPRJI - mStartRowPRJI > -1)
            {
                for (int i = mStartRowPRJI; i <= mEndRowPRJI; ++i)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, 3);
                }
            }

            Grd1.Cells[mEndRowPRJI + 1, 3].Value = Amt;

            Grd1.Cells[mEndRowPRJI + 3, 3].Value = Amt;
            Grd1.Cells[mEndRowPRJI + 4, 3].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 3) - Amt;
            Grd1.Cells[mEndRowPRJI + 5, 3].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 3);
            Grd1.Cells[mEndRowPRJI + 6, 3].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 3);
            Grd1.Cells[mEndRowPRJI + 7, 3].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 4, 3) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 5, 3);
            Grd1.Cells[mEndRowPRJI + 8, 3].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 4, 3) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 5, 3);
            Grd1.Cells[mEndRowPRJI + 9, 3].Value = Amt + Sm.GetGrdDec(Grd1, mEndRowPRJI + 8, 3);
            Grd1.Cells[mEndRowPRJI + 10, 3].Value = Sm.GetGrdDec(Grd1, 10, 3) - Sm.GetGrdDec(Grd1, mEndRowPRJI + 9, 3);
            Grd1.Cells[mEndRowPRJI + 11, 3].Value = Sm.GetGrdDec(Grd1, 10, 3) - Sm.GetGrdDec(Grd1, mEndRowPRJI + 9, 3);
            Grd1.Cells[mEndRowPRJI + 12, 3].Value = Sm.GetGrdDec(Grd1, 0, 3) - Amt;
        }

        private void ComputeLastYear()
        {
            Grd1.Cells[1, 4].Value = Sm.GetGrdDec(Grd1, 1, 4);
            if (Decimal.Parse(TxtExclPPN.Text) != 0m) Grd1.Cells[2, 4].Value = Sm.GetGrdDec(Grd1, 1, 4) / Decimal.Parse(TxtExclPPN.Text);
            Grd1.Cells[5, 4].Value = Decimal.Parse(TxtInclPPN.Text);
            Grd1.Cells[8, 4].Value = ((Decimal.Parse(TxtInclPPN.Text) - Sm.GetGrdDec(Grd1, 7, 4)) / 1.1m) * 0.1m + ((Sm.GetGrdDec(Grd1, 4, 4) / 1.1m) * 0.1m);
            Grd1.Cells[9, 4].Value = ((Decimal.Parse(TxtInclPPN.Text) - Sm.GetGrdDec(Grd1, 7, 4)) / 1.1m) * 0.04m + ((Sm.GetGrdDec(Grd1, 4, 4) / 1.1m) * 0.04m);
            Grd1.Cells[6, 4].Value = Sm.GetGrdDec(Grd1, 7, 4) + Sm.GetGrdDec(Grd1, 8, 4) + Sm.GetGrdDec(Grd1, 9, 4);
            Grd1.Cells[10, 4].Value = Sm.GetGrdDec(Grd1, 4, 4) + Sm.GetGrdDec(Grd1, 5, 4) - Sm.GetGrdDec(Grd1, 6, 4);

            decimal Amt = 0;

            if (mEndRowPRJI - mStartRowPRJI > -1)
            {
                for (int i = mStartRowPRJI; i <= mEndRowPRJI; ++i)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, 4);
                }
            }

            Grd1.Cells[mEndRowPRJI + 1, 4].Value = Amt;
            Grd1.Cells[mEndRowPRJI + 3, 4].Value = Amt;
            Grd1.Cells[mEndRowPRJI + 4, 4].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 4) - Amt;
            Grd1.Cells[mEndRowPRJI + 5, 4].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 4);
            Grd1.Cells[mEndRowPRJI + 6, 4].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 4);
            Grd1.Cells[mEndRowPRJI + 7, 4].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 4, 4) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 5, 4);
            Grd1.Cells[mEndRowPRJI + 8, 4].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 4) + Amt;
            Grd1.Cells[mEndRowPRJI + 9, 4].Value = Amt + Sm.GetGrdDec(Grd1, mEndRowPRJI + 8, 4);
            Grd1.Cells[mEndRowPRJI + 10, 4].Value = Sm.GetGrdDec(Grd1, 10, 4) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 9, 4);
            Grd1.Cells[mEndRowPRJI + 11, 4].Value = Sm.GetGrdDec(Grd1, 11, 3) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 10, 4);
            Grd1.Cells[mEndRowPRJI + 12, 4].Value = Sm.GetGrdDec(Grd1, 0, 4) + Amt;
        }

        private void ProcessCashBudgetPenjualan()
        {
            Grd1.Cells[0, 2].Value = Grd1.Cells[1, 2].Value = Sm.FormatNum(TxtExclPPN.Text, 0);
            Grd1.Cells[2, 2].Value = (Sm.GetGrdDec(Grd1, 1, 2) == 0m) ? 0m : Sm.GetGrdDec(Grd1, 0, 2) / Sm.GetGrdDec(Grd1, 1, 2);
        }

        private void ProcessDroppingRequest(ref List<DroppingRequest> lDRQ, ref List<Column> lCols, string Yr)
        {
            if(lDRQ.Count > 0)
            {
                foreach(var x in lDRQ.Where(w => w.Yr == Yr))
                {
                    for (int j = 1; j < 13; ++j)
                    {
                        if (j > Int32.Parse(Sm.GetLue(LueMth)))
                        {
                            if (x.Mth == Sm.Right(string.Concat("00", j.ToString()), 2))
                                Grd1.Cells[mEndRowPRJI + 2, mMthsCols[j - 1]].Value = x.Amt;
                        }
                    }
                }
            }
        }

        private void ProcessVCDropping(ref List<VCDropping> lVCDrop, ref List<Column> lCols, string Yr, string Yr2)
        {
            if (lVCDrop.Count > 0)
            {
                foreach(var x in lVCDrop.Where(w => Int32.Parse(w.Yr) < Int32.Parse(Yr2)))
                {
                    Grd1.Cells[mEndRowPRJI + 2, 3].Value = x.Amt;
                }

                foreach(var x in lVCDrop.Where(w => w.Yr == Yr2))
                {
                    Grd1.Cells[mEndRowPRJI + 2, 4].Value = x.Amt;
                }

                Grd1.Cells[mEndRowPRJI + 2, 5].Value = Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 3) + Sm.GetGrdDec(Grd1, mEndRowPRJI + 2, 4);

                foreach(var x in lVCDrop.Where(w => w.Yr == Yr))
                {
                    for (int j = 1; j < 13; ++j)
                    {
                        if (j <= Int32.Parse(Sm.GetLue(LueMth)))
                        {
                            if (x.Mth == Sm.Right(string.Concat("00", j.ToString()), 2))
                                Grd1.Cells[mEndRowPRJI + 2, mMthsCols[j - 1]].Value = x.Amt;
                        }
                    }
                }
            }
        }

        private void ProcessPRJI(ref List<PRJI> lPRJI, ref List<Column> lCols, string Yr, string Yr2)
        {
            if (lPRJI.Count > 0)
            {
                for (int i = mStartRowPRJI; i <= mEndRowPRJI; ++i)
                {
                    foreach (var x in lPRJI.Where(w => w.DocType == "1" && w.CCtName == Sm.GetGrdStr(Grd1, i, 1))) // Cash Budget
                    {
                        Grd1.Cells[i, 2].Value = x.Amt;
                    }

                    foreach (var x in lPRJI.Where(w => w.DocType == "2" && w.CCtName == Sm.GetGrdStr(Grd1, i, 1))) // s.d tahun lalu
                    {
                        Grd1.Cells[i, 3].Value = x.Amt;
                    }

                    foreach (var x in lPRJI.Where(w => w.DocType == "3" && w.CCtName == Sm.GetGrdStr(Grd1, i, 1) && w.Yr == Yr2)) // tahun lalu
                    {
                        Grd1.Cells[i, 4].Value = x.Amt;
                    }

                    foreach (var x in lPRJI.Where(w => w.DocType == "3" && w.CCtName == Sm.GetGrdStr(Grd1, i, 1) && w.Yr == Yr && w.DocTypeTrans == "3")) // tahun filter, realisasi
                    {
                        for (int j = 1; j < 13; ++j)
                        {
                            if (j <= Int32.Parse(Sm.GetLue(LueMth)))
                            {
                                if (x.Mth == Sm.Right(string.Concat("00", j.ToString()), 2))
                                    Grd1.Cells[i, mMthsCols[j - 1]].Value = x.Amt;
                            }
                        }
                    }

                    foreach (var x in lPRJI.Where(w => w.DocType == "3" && w.CCtName == Sm.GetGrdStr(Grd1, i, 1) && w.Yr == Yr && w.DocTypeTrans == "2")) // tahun filter, rencana
                    {
                        for (int j = 1; j < 13; ++j)
                        {
                            if (j > Int32.Parse(Sm.GetLue(LueMth)))
                            {
                                if (x.Mth == Sm.Right(string.Concat("00", j.ToString()), 2))
                                    Grd1.Cells[i, mMthsCols[j - 1]].Value = x.Amt;
                            }
                        }
                    }
                }
            }
        }

        private void ProcessPenerimaanTagihan()
        {
            Grd1.Cells[5, 2].Value = Grd1.Cells[5, 3].Value = Grd1.Cells[5, 4].Value = Sm.FormatNum(Decimal.Parse(TxtInclPPN.Text), 0);
            Grd1.Cells[5, 5].Value = Sm.GetGrdDec(Grd1, 5, 3) + Sm.GetGrdDec(Grd1, 5, 4);
            //for (int i = 1; i < 13; ++i)
            //{
            //    Grd1.Cells[5, mMthsCols[i - 1]].Value = Sm.FormatNum(Decimal.Parse(TxtInclPPN.Text), 0);
            //}
        }

        private void ProcessPenjualan(ref List<ProjectDelivery> lPRDV, ref List<Column> lCols, string Yr, string Yr2)
        {
            if (lPRDV.Count > 0)
            {
                string mMth = Sm.GetLue(LueMth);
                foreach(var x in lPRDV.OrderBy(o => o.Yr).ThenBy(t => t.Mth))
                {
                    if (Int32.Parse(x.Yr) < Int32.Parse(Yr2)) Grd1.Cells[0, 3].Value = Grd1.Cells[1, 3].Value = x.Amt;
                    if (Int32.Parse(x.Yr) == Int32.Parse(Yr2)) Grd1.Cells[0, 4].Value = Grd1.Cells[1, 4].Value = x.Amt;
                    Grd1.Cells[0, 5].Value = Sm.GetGrdDec(Grd1, 0, 3) + Sm.GetGrdDec(Grd1, 0, 4);
                    Grd1.Cells[1, 5].Value = Sm.GetGrdDec(Grd1, 1, 3) + Sm.GetGrdDec(Grd1, 1, 4);

                    if (x.Yr == Yr)
                    {
                        for (int i = 1; i < 13; ++i)
                        {
                            if (i == Int32.Parse(mMth))
                            {
                                if (i == Int32.Parse(x.Mth))
                                {
                                    decimal Amt1 = 0m;
                                    if (i != 1)
                                    {
                                        for (int j = 0; j < i - 1; ++j)
                                        {
                                            Amt1 += Sm.GetGrdDec(Grd1, 0, mMthsCols[j]);
                                        }
                                    }
                                    Grd1.Cells[0, mMthsCols[i - 1]-1].Value = Amt1;
                                    Grd1.Cells[0, mMthsCols[i - 1]].Value = x.Amt;
                                    Grd1.Cells[0, mMthsCols[i - 1] + 1].Value = Sm.GetGrdDec(Grd1, 0, mMthsCols[i - 1] - 1) + Sm.GetGrdDec(Grd1, 0, mMthsCols[i - 1]);
                                }
                            }
                            else
                            {
                                if(i == Int32.Parse(x.Mth))
                                    Grd1.Cells[0, mMthsCols[i - 1]].Value = x.Amt;
                            }
                        }
                    }
                }
            }
        }

        private void ProcessPotonganPPNPPH(ref List<COA> lCOA, string Yr, string Yr2)
        {
            if (lCOA.Count > 0)
            {
                foreach(var x in lCOA.Where(w => w.AcNo.Contains("2.070.") && Int32.Parse(w.Yr) < Int32.Parse(Yr2)))
                {
                    Grd1.Cells[8, 3].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                }

                foreach (var x in lCOA.Where(w => w.AcNo.Contains("8.200.") && Int32.Parse(w.Yr) < Int32.Parse(Yr2)))
                {
                    Grd1.Cells[9, 3].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                }
            }
        }

        private void ProcessInflowUangMuka(ref List<COA> lCOA, ref List<Column> lCols, string Yr, string Yr2)
        {
            if (lCOA.Count > 0)
            {
                string mMth = Sm.GetLue(LueMth);
                decimal AmtCB = 0m, AmtToCBLastYear = 0m, AmtCBLastYear = 0m;
                foreach (var x in lCOA.Where(w => w.AcNo.Contains("2.060.")).OrderBy(o => o.Yr).ThenBy(t => t.Mth))
                {
                    if (x.Mth == "00") AmtCB += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (Int32.Parse(x.Yr) < Int32.Parse(Yr2)) AmtToCBLastYear += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (Int32.Parse(x.Yr) == Int32.Parse(Yr2)) AmtCBLastYear += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);

                    //Grd1.Cells[4, 2].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    //if (Int32.Parse(x.Yr) < Int32.Parse(Yr2)) Grd1.Cells[4, 3].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    //if (Int32.Parse(x.Yr) == Int32.Parse(Yr2)) Grd1.Cells[4, 4].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    //Grd1.Cells[4, 5].Value = Sm.GetGrdDec(Grd1, 4, 3) + Sm.GetGrdDec(Grd1, 4, 4);

                    if (x.Yr == Yr)
                    {
                        for (int i = 1; i < 13; ++i)
                        {
                            if (i == Int32.Parse(mMth))
                            {
                                if (i == Int32.Parse(x.Mth))
                                {
                                    decimal Amt1 = 0m;
                                    if (i != 1)
                                    {
                                        for (int j = 0; j < i - 1; ++j)
                                        {
                                            Amt1 += Sm.GetGrdDec(Grd1, 4, mMthsCols[j]);
                                        }
                                    }
                                    Grd1.Cells[4, mMthsCols[i - 1] - 1].Value = Amt1;
                                    Grd1.Cells[4, mMthsCols[i - 1]].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                                    Grd1.Cells[4, mMthsCols[i - 1] + 1].Value = Sm.GetGrdDec(Grd1, 0, mMthsCols[i - 1] - 1) + Sm.GetGrdDec(Grd1, 0, mMthsCols[i - 1]);
                                }
                            }
                            else
                            {
                                if (i == Int32.Parse(x.Mth))
                                    Grd1.Cells[4, mMthsCols[i - 1]].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                            }
                        }
                    }
                }

                Grd1.Cells[4, 2].Value = AmtCB;
                Grd1.Cells[4, 3].Value = AmtToCBLastYear;
                Grd1.Cells[4, 4].Value = AmtCBLastYear;
                Grd1.Cells[4, 5].Value = Sm.GetGrdDec(Grd1, 4, 3) + Sm.GetGrdDec(Grd1, 4, 4);
            }
        }

        private void ProcessPotonganUangMuka(ref List<COA> lCOA, ref List<Column> lCols, string Yr, string Yr2)
        {
            if (lCOA.Count > 0)
            {
                string mMth = Sm.GetLue(LueMth);
                foreach (var x in lCOA.Where(w => w.AcNo.Contains("2.060.")).OrderBy(o => o.Yr).ThenBy(t => t.Mth))
                {
                    Grd1.Cells[7, 2].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (Int32.Parse(x.Yr) < Int32.Parse(Yr2)) Grd1.Cells[7, 3].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    if (Int32.Parse(x.Yr) == Int32.Parse(Yr2)) Grd1.Cells[7, 4].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    Grd1.Cells[7, 5].Value = Sm.GetGrdDec(Grd1, 7, 3) + Sm.GetGrdDec(Grd1, 7, 4);

                    if (x.Yr == Yr)
                    {
                        for (int i = 1; i < 13; ++i)
                        {
                            if (i == Int32.Parse(mMth))
                            {
                                if (i == Int32.Parse(x.Mth))
                                {
                                    decimal Amt1 = 0m;
                                    if (i != 1)
                                    {
                                        for (int j = 0; j < i - 1; ++j)
                                        {
                                            Amt1 += Sm.GetGrdDec(Grd1, 7, mMthsCols[j]);
                                        }
                                    }
                                    Grd1.Cells[7, mMthsCols[i - 1] - 1].Value = Amt1;
                                    Grd1.Cells[7, mMthsCols[i - 1]].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                                    Grd1.Cells[7, mMthsCols[i - 1] + 1].Value = Sm.GetGrdDec(Grd1, 0, mMthsCols[i - 1] - 1) + Sm.GetGrdDec(Grd1, 0, mMthsCols[i - 1]);
                                }
                            }
                            else
                            {
                                if (i == Int32.Parse(x.Mth))
                                    Grd1.Cells[7, mMthsCols[i - 1]].Value = (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                            }
                        }
                    }
                }
            }
        }

        private void ComputeDroppingRequest(ref List<DroppingRequest> lDRQ, string SOCDocNo, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select D.Yr, D.Mth, Sum(D.Amt) Amt ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNO And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr C On B.DocNo = C.SOContractDocNo ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr D On C.DocNo = D.PRJIDocNo And D.PRJIDocNo Is Not Null ");
            SQL.AppendLine("    And D.DocNo Not In (Select Distinct DRQDocNo From TblDroppingPaymentHdr Where CancelInd = 'N' And Status In ('O', 'A')) ");
            SQL.AppendLine("    And D.CancelInd = 'N' And D.Status = 'A' ");
            SQL.AppendLine("    And D.Yr = @Yr ");
            SQL.AppendLine("Group By D.Yr, D.Mth; ");

            Sm.CmParam<String>(ref cm, "@SOCDocNo", SOCDocNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lDRQ.Add(new DroppingRequest()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeProjectDelivery(ref List<ProjectDelivery> lPRDV, string SOCDocNo, string Yr, string Yr2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.Yr, T.Mth, Sum(T.Amt) Amt From ( ");
            SQL.AppendLine("    Select Left(D.SettleDt, 4) Yr, '00' Mth, D.SettledAmt Amt ");
            SQL.AppendLine("    From TblSOContractHdr A ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationHdr C On B.Docno = C.SOContractDocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblProjectImplementationDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("        And D.SettleDt Is Not Null ");
            SQL.AppendLine("        And D.SettledInd = 'Y' ");
            SQL.AppendLine("        And Left(D.SettleDt, 4) < @Yr2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Left(D.SettleDt, 4) Yr, '00' Mth, D.SettledAmt Amt ");
            SQL.AppendLine("    From TblSOContractHdr A ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationHdr C On B.Docno = C.SOContractDocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblProjectImplementationDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("        And D.SettleDt Is Not Null ");
            SQL.AppendLine("        And D.SettledInd = 'Y' ");
            SQL.AppendLine("        And Left(D.SettleDt, 4) = @Yr2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Left(D.SettleDt, 4) Yr, Substring(D.SettleDt, 5, 2) Mth, D.SettledAmt Amt ");
            SQL.AppendLine("    From TblSOContractHdr A ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationHdr C On B.Docno = C.SOContractDocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblProjectImplementationDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("        And D.SettleDt Is Not Null ");
            SQL.AppendLine("        And D.SettledInd = 'Y' ");
            SQL.AppendLine("        And Left(D.SettleDt, 4) = @Yr ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.Yr, T.Mth; ");

            Sm.CmParam<String>(ref cm, "@SOCDocNo", SOCDocNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Yr2", Yr2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lPRDV.Add(new ProjectDelivery()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeVCDropping(ref List<VCDropping> lVCDrop, string SOCDocNo, string Yr, byte DocType)
        {
            // Doctype : 2 kurang dari, 3 sama dengan
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Left(H.DocDt, 4) Yr, ");
            if (DocType == 3)
                SQL.AppendLine("Substring(H.DocDt, 5, 2) Mth, ");
            else
                SQL.AppendLine("'00' As Mth, ");
            SQL.AppendLine("Sum(H.Amt) Amt ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo ");
            SQL.AppendLine("    And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr C On B.DocNo = C.SOContractDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl2 D On C.DocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr E On D.DocNo = E.PRJIDocNo And D.ResourceItCode = E.ResourceItCode ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr F On E.PRJIDocNo = F.PRJIDocNo ");
            SQL.AppendLine("Inner Join TblDroppingPaymentHdr G On F.DocNO = G.DRQDocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr H On G.VoucherRequestDocNo = H.VoucherRequestDocNo And H.CancelInd = 'N' ");
            if (DocType == 2)
                SQL.AppendLine("    And Left(H.DocDt, 4) < @Yr ");
            else
                SQL.AppendLine("    And Left(H.DocDt, 4) = @Yr ");
            SQL.AppendLine("Group By Left(H.DocDt, 4) ");
            if (DocType == 3)
                SQL.AppendLine(", Substring(H.DocDt, 5, 2) ");
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@SOCDocNo", SOCDocNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lVCDrop.Add(new VCDropping()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputePRJI(ref List<PRJI> lPRJI, string SOCDocNo, string Yr, byte DocType, byte DocTypeTrans)
        {
            //DocType : 1 cash budget, 2 kurang dari, 3 sama dengan
            //DocTypeTrans : 1 Resource, 2 RBP, 3 VC OP-PI-RecvVd-PO-POR-MR-DroppingRequest-RBP-PRJI Resource
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (DocTypeTrans == 1) SQL = GetResource();
            if (DocTypeTrans == 2) SQL = GetRBP();
            if (DocTypeTrans == 3) SQL = GetVCOP(DocType);

            Sm.CmParam<String>(ref cm, "@SOCDocNo", SOCDocNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@DocType", DocType.ToString());

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "CCtName", "Yr", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lPRJI.Add(new PRJI()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            CCtName = Sm.DrStr(dr, c[1]),
                            Yr = Sm.DrStr(dr, c[2]),
                            Mth = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4]),
                            DocTypeTrans = DocTypeTrans.ToString()
                        });
                    }
                }
                dr.Close();
            }
        }

        private StringBuilder GetResource()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @DocType As DocType, H.OptDesc CCtName, @Yr As Yr, '00' Mth, Sum(A.Amt) Amt ");
            SQL.AppendLine("From TblProjectImplementationDtl2 A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNO ");
            SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo And D.DocNo = @SOCDocNo ");
            SQL.AppendLine("Inner Join TblITem E On A.ResourceItCode = E.ItCOde ");
            SQL.AppendLine("Inner Join TblItemCostCategory F On E.ItCOde = F.ItCOde ");
            SQL.AppendLine("Inner Join TblCostCategory G On F.CCtCode = G.CCtCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat = 'RptCashFlow3CCtName' And G.AcNo Like Concat(H.OptCode, '.%') ");
            SQL.AppendLine("Group By H.OptDesc; ");

            return SQL;
        }

        private StringBuilder GetRBP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @DocType As DocType, J.Optdesc CCtName, F.Yr, F.Mth, Sum(F.TotalAmt) Amt ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr C On B.DocNo = C.SOContractDocNo ");
            SQL.AppendLine("Inner JOin TblProjectImplementationDtl2 D On C.DocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr E On D.DocNo = E.PRJIDocNo And D.ResourceItCode = E.ResourceItCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPDtl F On E.DocNo = F.DocNo And F.Yr = @Yr ");
            SQL.AppendLine("    And Concat(E.PRJIDocNo, F.Yr, F.Mth) Not In  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(PRJIDocNo, Yr, Mth) ");
            SQL.AppendLine("        From TblDroppingRequestHdr ");
            SQL.AppendLine("        Where PRJIDocNo In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select T1.DocNo ");
            SQL.AppendLine("            From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("            Inner Join TblSOContractRevisionHdr T2 On T1.SOContractDocNo = T2.DOcno And T2.SOCDocNo = @SOCDoNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And CancelInd = 'N' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem G On D.ResourceItCode = G.ItCode ");
            SQL.AppendLine("Inner Join TblItemCostCategory H On G.ItCode = H.ItCode ");
            SQL.AppendLine("Inner Join TblCostCategory I On H.CCtCode = I.CCtCode ");
            SQL.AppendLine("Inner Join TblOption J On J.OptCat = 'RptCashFlow3CCtName' And I.AcNo Like Concat(J.OptCode, '.%') ");
            SQL.AppendLine("Group By J.OptDesc, F.Yr, F.Mth; ");

            return SQL;
        }

        private StringBuilder GetVCOP(byte DocType)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @DocType AS DocType, S.OptDesc CCtName, Left(O.DocDt, 4) Yr, '00' As Mth, Sum(O.Amt) Amt ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo ");
            SQL.AppendLine("    And A.DocNo = @SOCDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr C On B.DocNo = C.SOContractDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl2 D On C.DocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr E On D.DocNo = E.PRJIDocNo And D.ResourceItCode = E.ResourceItCode ");
            SQL.AppendLine("Inner Join TblDroppingRequestHdr F On E.PRJIDocNo = F.PRJIDocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr G On F.DocNo = G.DroppingRequestDocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl H On G.DocNo = H.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl I On H.DocNo = I.MaterialRequestDocNo And H.DNo = I.MaterialRequestDNo ");
            SQL.AppendLine("Inner Join TblPODtl J On I.DocNo = J.PORequestDocNo And I.DNo = J.PORequestDNo ");
            SQL.AppendLine("Inner Join TblRecvVdDtl K On J.DocNo = K.PODocNo And J.DNo = K.PODNo ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl L On K.DocNo = L.RecvVdDocNo And K.DNo = L.RecvVdDNo ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl M On L.DOcNo = M.InvoiceDocNo ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr N On M.DocNo = N.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr O On N.VoucherRequestDocNo =O.VoucherRequestDocNo And O.CancelInd = 'N' ");
            if (DocType == 2)
                SQL.AppendLine("    And Left(O.DocDt, 4) < @Yr ");
            else
                SQL.AppendLine("    And Left(O.DocDt, 4) = @Yr ");
            SQL.AppendLine("Inner Join TblItem P On D.ResourceItCode = P.ItCode ");
            SQL.AppendLine("Inner Join TblItemCostCategory Q On P.ItCode = Q.ItCode ");
            SQL.AppendLine("Inner Join TblCostCategory R On Q.CCtCode = R.CCtCode ");
            SQL.AppendLine("Inner Join TblOption S On S.OptCat = 'RptCashFlow3CCtName' And R.AcNo Like Concat(S.OptCode, '.%') ");
            SQL.AppendLine("Group By S.OptDesc, Left(O.DocDt, 4); ");

            return SQL;
        }

        private void ComputeTrialBalance(ref List<COA> lCOA, string SOCDocNo, string AcNo, string Yr, string MenuParam, byte DocType, bool IsNeedOB)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct T.AcNo) ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select AcNo ");
            SQL.AppendLine("    From TblCOA ");
            SQL.AppendLine("    Where ActInd = 'Y' ");
            SQL.AppendLine("    And SOCDocNo Is Not Null ");
            SQL.AppendLine("    And SOCDocNo = @Param1 ");
            SQL.AppendLine("    And AcNo Like @Param2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select AcNo ");
            SQL.AppendLine("    From TblJournalDtl ");
            SQL.AppendLine("    Where SOContractDocNo Is Not Null ");
            SQL.AppendLine("    And SOContractDocNo = @Param1 ");
            SQL.AppendLine("    And AcNo Like @Param2 ");
            SQL.AppendLine(") T ");

            string AcNos = Sm.GetValue(SQL.ToString(), SOCDocNo, string.Concat(AcNo, "%"), string.Empty);

            if (AcNos.Length > 0)
            {
                if (IsNeedOB) ProcessOB(AcNos, Yr, ref lCOA, MenuParam);
                ProcessJournal(AcNos, Yr, ref lCOA, IsNeedOB, DocType, MenuParam);
            }
        }

        private void ProcessOB(string AcNo, string Yr, ref List<COA> lCOA, string MenuParam)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.AcNo, T.Yr, T.Mth, T.AcType, Sum(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select B.AcNo, A.Yr, '00' As Mth, C.AcType, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("Inner Join TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And C.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.AcNo, T.Yr, T.Mth, T.AcType; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Yr", "Mth", "AcType", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA() 
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Yr = Sm.DrStr(dr, c[1]),
                            Mth = Sm.DrStr(dr, c[2]),
                            AcType = Sm.DrStr(dr, c[3]),
                            DAmt = Sm.DrDec(dr, c[4]),
                            CAmt = Sm.DrDec(dr, c[5]),
                            MenuParam = MenuParam
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessJournal(string AcNo, string Yr, ref List<COA> lCOA, bool IsNeedOB, byte DocType, string MenuParam)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.AcNo, Left(B.DocDt, 4) Yr, ");
            if (DocType == 2)
                SQL.AppendLine("Substr(B.DocDt, 5, 2) Mth, ");
            else
                SQL.AppendLine("'00' As Mth, ");
            SQL.AppendLine("C.AcType, ");
            SQL.AppendLine("Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("From TblJournalDtl A ");
            SQL.AppendLine("Inner Join TblJournalHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("    And Find_In_Set(A.AcNo, @AcNo) ");
            if (DocType == 1)
            {
                SQL.AppendLine("    And Left(B.DocDt, 4) < @Yr ");
            }
            else if (DocType == 2)
            {
                SQL.AppendLine("    And Left(B.DocDt, 4) = @Yr ");
            }
            else
            {
                SQL.AppendLine("    And Left(B.DocDt, 4) <= @Yr ");
            }

            SQL.AppendLine("Inner Join TblCOA C On A.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And C.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");

            if (MenuParam == "ARDownpayment")
            {
                SQL.AppendLine("Inner Join TblVoucherHdr D On B.DocNo = D.DocNo ");
                SQL.AppendLine("    And D.CancelInd = 'N' ");
                SQL.AppendLine("    And D.DocType = '05' ");
            }

            if (MenuParam == "SalesInvoice5")
            {
                SQL.AppendLine("Inner Join TblVoucherHdr D On B.DocNo = D.DocNo ");
                SQL.AppendLine("    And D.CancelInd = 'N' ");
                SQL.AppendLine("    And D.DocType = '02' ");
                SQL.AppendLine("Inner Join TblIncomingPaymentHdr E On D.VoucherRequestDocNo = E.VoucherRequestDocNo ");
                SQL.AppendLine("    And E.DocNo In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct DocNo ");
                SQL.AppendLine("        From TblIncomingPaymentDtl ");
                SQL.AppendLine("        Where DocType = '5' ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Group By A.AcNo, Left(B.DocDt, 4), C.AcType ");

            if (DocType == 2 && Sm.GetLue(LueYr) == Yr)
                SQL.AppendLine(", Substr(B.DocDt, 5, 2) ");

            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Yr", "Mth", "AcType", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsNeedOB)
                        {
                            bool IsExists = false;
                            foreach(var x in lCOA.Where(w => 
                                w.AcNo == Sm.DrStr(dr, c[0]) &&
                                w.Yr == Sm.DrStr(dr, c[1]) &&
                                w.Mth == Sm.DrStr(dr, c[2])
                                ))
                            {
                                x.DAmt += Sm.DrDec(dr, c[4]);
                                x.CAmt += Sm.DrDec(dr, c[5]);
                                IsExists = true;
                            }

                            if (!IsExists)
                            {
                                lCOA.Add(new COA()
                                {
                                    AcNo = Sm.DrStr(dr, c[0]),
                                    Yr = Sm.DrStr(dr, c[1]),
                                    Mth = Sm.DrStr(dr, c[2]),
                                    AcType = Sm.DrStr(dr, c[3]),
                                    DAmt = Sm.DrDec(dr, c[4]),
                                    CAmt = Sm.DrDec(dr, c[5]),
                                    MenuParam = MenuParam
                                });
                            }
                        }
                        else
                        {
                            lCOA.Add(new COA() 
                            {
                                AcNo = Sm.DrStr(dr, c[0]),
                                Yr = Sm.DrStr(dr, c[1]),
                                Mth = Sm.DrStr(dr, c[2]),
                                AcType = Sm.DrStr(dr, c[3]),
                                DAmt = Sm.DrDec(dr, c[4]),
                                CAmt = Sm.DrDec(dr, c[5]),
                                MenuParam = MenuParam
                            });
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetHeaderData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select E.ProjectName, IfNull(C.ProjectCode2, C.ProjectCode) ProjectCode, F.SiteName, ");
            SQL.AppendLine("C.DocNo SOContractDocNo, B.DocNo SOContractRevisionDocNo, IfNull(DateDiff(C1.DeliveryDt, C.DocDt), 0) DeliveryDays, ");
            SQL.AppendLine("G.OptDesc ProjectResource, C.Amt InclPPN, C.Amt2 ExclPPN, A.ExclPPNPPhAmt Netto, (A.TotalResource) RAP, ");
            SQL.AppendLine("If(A.ExclPPNPPhAmt = 0, 0, ((A.TotalResource) / A.ExclPPNPPhAmt) * 100) RAPToNetto, ");
            SQL.AppendLine("(100 - If(A.ExclPPNPPhAmt = 0, 0, ((A.TotalResource) / A.ExclPPNPPhAmt) * 100)) ProfitToNetto ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOCOntractDocNO = B.DOcNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl C1 On C.DocNo = C1.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("INner JOin TblLOPHdr E On D.LOPDocNo = E.DocNo And E.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblSite F On E.SiteCode = F.SiteCode ");
            SQL.AppendLine("Left Join TblOption G On E.ProjectResource = G.OptCode And G.OptCat = 'ProjectResource' ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetLue(LueProjectName));

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ProjectName",

                    //1-5
                    "ProjectCode", "SiteName", "SOContractDocNo", "SOContractRevisionDocNo", "DeliveryDays",  
                    
                    //6-10
                    "ProjectResource", "InclPPN", "ExclPPN", "Netto", "RAP",   
                    
                    //11-12
                    "RAPToNetto", "ProfitToNetto"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[0]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[1]);
                    TxtSiteName.EditValue = Sm.DrStr(dr, c[2]);
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    TxtSOContractRevisionDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtDeliveryDays.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    TxtResource.EditValue = Sm.DrStr(dr, c[6]);
                    TxtInclPPN.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtExclPPN.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtNetto.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    TxtRAP.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    TxtRAPToNetto.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtProfitToNetto.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                }, true
            );
        }

        private void GenerateColumn(ref List<Column> lCols)
        {
            int mMth = 0;
            int LatestColumn = Grd1.Cols.Count - 1;
            string mYr = Sm.GetLue(LueYr);
            string mReal = "Realisasi", mRen = "Rencana";
            lCols.Clear();

            if (Sm.GetLue(LueMth).Length > 0 && Sm.GetLue(LueYr).Length > 0)
            {
                mMth = Int32.Parse(Sm.GetLue(LueMth));

                for (int i = 1; i < 13; ++i)
                {
                    if (i == mMth)
                    {
                        LatestColumn += 1;
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = mReal + Environment.NewLine + mYr + Environment.NewLine + "S.d Bulan Lalu";
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 150;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                        lCols.Add(new Column() 
                        {
                            ColName = mReal + Environment.NewLine + mYr + Environment.NewLine + "S.d Bulan Lalu",
                            Cols = LatestColumn,
                            Mth = string.Concat(Sm.Right(string.Concat("00", i.ToString()), 2), "1"),
                            MthInd = false
                        });

                        LatestColumn += 1;
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = mReal + Environment.NewLine + mYr + Environment.NewLine + "Bulan Ini";
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 150;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                        lCols.Add(new Column()
                        {
                            ColName = mReal + Environment.NewLine + mYr + Environment.NewLine + "Bulan Ini",
                            Cols = LatestColumn,
                            Mth = Sm.Right(string.Concat("00", i.ToString()), 2),
                            MthInd = true
                        });

                        LatestColumn += 1;
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = mReal + Environment.NewLine + mYr + Environment.NewLine + "S.d Bulan Ini";
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 150;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                        lCols.Add(new Column()
                        {
                            ColName = mReal + Environment.NewLine + mYr + Environment.NewLine + "S.d Bulan Ini",
                            Cols = LatestColumn,
                            Mth = string.Concat(Sm.Right(string.Concat("00", i.ToString()), 2), "3"),
                            MthInd = false
                        });
                    }
                    else
                    {
                        LatestColumn += 1;
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = ((i < mMth) ? mReal : mRen) + Environment.NewLine + mYr + Environment.NewLine + mMths[i-1];
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 150;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                        lCols.Add(new Column()
                        {
                            ColName = ((i < mMth) ? mReal : mRen) + Environment.NewLine + mYr + Environment.NewLine + mMths[i - 1],
                            Cols = LatestColumn,
                            Mth = Sm.Right(string.Concat("00", i.ToString()), 2),
                            MthInd = true
                        });
                    }
                }

                LatestColumn += 1;
                Grd1.Cols.Count += 1;
                Grd1.Header.Cells[0, LatestColumn].Value = "Jumlah";
                Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[LatestColumn].Width = 150;
                Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                lCols.Add(new Column()
                {
                    ColName = "Jumlah",
                    Cols = LatestColumn,
                    Mth = "13",
                    MthInd = false
                });

                LatestColumn += 1;
                Grd1.Cols.Count += 1;
                Grd1.Header.Cells[0, LatestColumn].Value = "Prognosa " + mYr;
                Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[LatestColumn].Width = 150;
                Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                lCols.Add(new Column()
                {
                    ColName = "Prognosa " + mYr,
                    Cols = LatestColumn,
                    Mth = "14",
                    MthInd = false
                });

                LatestColumn += 1;
                Grd1.Cols.Count += 1;
                Grd1.Header.Cells[0, LatestColumn].Value = "Total";
                Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[LatestColumn].Width = 150;
                Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                lCols.Add(new Column()
                {
                    ColName = "Total",
                    Cols = LatestColumn,
                    Mth = "15",
                    MthInd = false
                });

                LatestColumn += 1;
                Grd1.Cols.Count += 1;
                Grd1.Header.Cells[0, LatestColumn].Value = "Sisa";
                Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[LatestColumn].Width = 150;
                Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);

                lCols.Add(new Column()
                {
                    ColName = "Sisa",
                    Cols = LatestColumn,
                    Mth = "16",
                    MthInd = false
                });

                LatestColumn += 1;
                Grd1.Cols.Count += 1;
                Grd1.Header.Cells[0, LatestColumn].Value = "Keterangan";
                Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[LatestColumn].Width = 150;

                lCols.Add(new Column()
                {
                    ColName = "Keterangan",
                    Cols = LatestColumn,
                    Mth = "17",
                    MthInd = false
                });

                int mCols = 0;
                foreach(var x in lCols.Where(w => w.MthInd).OrderBy(o => o.Mth))
                {
                    mMthsCols[mCols] = x.Cols;

                    mCols += 1;
                }
            }
        }

        private void SetLueProjectName(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As Col1, A.ProjectName As Col2 ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNO = B.LOPDocNO ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.DocNO = C.BOQDocNo ");
            SQL.AppendLine("Inner JOin TblSOContractRevisionHdr D On C.DocNO = D.SOCDocNo ");
            SQL.AppendLine("INner Join TblProjectImplementationHdr E On D.DocNo = E.SOContractDocNo ");
            SQL.AppendLine("    And E.CancelInd = 'N' ");
            SQL.AppendLine("    And E.Status = 'A' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("Order By A.ProjectName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueProjectName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProjectName, new Sm.RefreshLue1(SetLueProjectName));
        }

        private void TxtDeliveryDays_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDeliveryDays, 0);
        }

        private void TxtInclPPN_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtInclPPN, 0);
        }

        private void TxtExclPPN_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtExclPPN, 0);
        }

        private void TxtNetto_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtNetto, 0);
        }

        private void TxtRAP_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRAP, 0);
        }

        private void TxtRAPToNetto_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRAPToNetto, 0);
        }

        private void TxtProfitToNetto_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtProfitToNetto, 0);
        }

        #endregion

        #endregion

        #region Class

        private class Column
        {
            public string ColName { get; set; }
            public string Mth { get; set; }
            public int Cols { get; set; }
            public bool MthInd { get; set; }
        }

        private class PRJI
        {
            public string DocType { get; set; }
            public string DocTypeTrans { get; set; }
            public string CCtName { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string MenuParam { get; set; }
        }

        private class VCDropping
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        private class DroppingRequest
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        private class ProjectDelivery
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
