﻿#region Update
/* 
    26/01/2021 [BRI] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalSettingEmpUpdate : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSiteCode = string.Empty;
        internal FrmDocApprovalSettingEmpUpdateFind FrmFind;
        private bool mIsInsert = false;

        #endregion

        #region Constructor

        public FrmDocApprovalSettingEmpUpdate(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnDelete.Visible = false;
                BtnPrint.Visible = false;
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserCode, LueSiteCode
                    }, true);
                    TxtUserCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserCode, LueSiteCode
                    }, false);
                    mIsInsert = true;
                    TxtUserCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserCode, LueSiteCode
                    }, false);
                    mIsInsert = false;
                    TxtUserCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtUserCode, LueSiteCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDocApprovalSettingEmpUpdateFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtUserCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mIsInsert)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SiteCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select * ");
                SQL.AppendLine("From TblDocApprovalSettingEmpUpdate ");
                SQL.AppendLine("Where SiteCode=@SiteCode; ");
                Sm.CmParam<String>(ref cm, "@SiteCOde", SiteCode);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "UserCode", 

                        //1-5
                        "SiteCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtUserCode.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[1]));
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (IsDataNotValid()) return;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmployeeApprovalSetting());

            Sm.ExecCommands(cml);

            mSiteCode = Sm.GetLue(LueSiteCode);

            ShowData(Sm.GetLue(LueSiteCode));
        }

        private MySqlCommand SaveEmployeeApprovalSetting()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApprovalSettingEmpUpdate(UserCode, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@UserCode, @SiteCode, @UserCode2, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtUserCode, "User Code", false) ||
                Sm.IsTxtEmpty(LueSiteCode, "Site", false) ||
                IsDocApprovalSettingEmpUpdateExisted();
        }

        private bool IsDocApprovalSettingEmpUpdateExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSettingEmpUpdate ");
            SQL.AppendLine("Where SiteCode = '" + Sm.GetLue(LueSiteCode) + "' ");
            if(!mIsInsert)
                SQL.AppendLine("And SiteCode <> '" + mSiteCode + "' ");
            SQL.AppendLine("Limit 1; ");

            if (BtnSave.Enabled && Sm.IsDataExist(SQL.ToString()))
            {
                Sm.StdMsg(mMsgType.Warning, "The setting for this site already exists.");
                return true;
            }
            return false;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsDataNotValid()) return;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateEmployeeApprovalSetting(mSiteCode));

            Sm.ExecCommands(cml);

            mSiteCode = Sm.GetLue(LueSiteCode);

            ShowData(Sm.GetLue(LueSiteCode));
        }

        private MySqlCommand UpdateEmployeeApprovalSetting(string SiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDocApprovalSettingEmpUpdate ");
            SQL.AppendLine("Set UserCode=@UserCode, SiteCode=@SiteCode, LastUpBy=@UserCode2, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where SiteCode='"+SiteCode+"';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtUserCode);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        #endregion

        #endregion
    }
}
