﻿#region Update
/* 
    20171206 [HAR] bug : reset password ambil dari employeecode 
    20180117 [HAR] bug : reset password 123456 jika employee blm diset usernya
    08/01/2020 [DITA/ALL] sesuai parameter : IsPasswordForLoginNeedToEncode dapat memilih login berdasarkan password yg di encode atau tidak
    09/04/2021 [TKG/PHT] tambah update PwdLastUpDt
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmResetPassword : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmResetPasswordFind FrmFind;

        #endregion

        #region Constructor

        public FrmResetPassword(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtUserCode, MeeRemark
                    }, true);
                    TxtUserCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtUserCode, MeeRemark
                    }, false);
                    TxtUserCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtUserCode, MeeRemark
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmResetPasswordFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                string mEmpCode = Sm.GetValue("Select EmpCode from TblEmployee Where UserCode=@Param ", TxtUserCode.Text),
                    mDefaultPwd = "123456",
                    mEmpCodeEncode = (mEmpCode.Length > 0) ? Sm.GetHashBase64(Sm.GetHashSha256(mEmpCode)) : "",
                    mDefaultPwdEncode = (mDefaultPwd.Length > 0) ? Sm.GetHashBase64(Sm.GetHashSha256(mDefaultPwd)) : "";
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

                SQL.AppendLine("Insert Into TblResetPassword(UserCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@UserCode, @Remark,  @CreateBy, @Dt); ");

                SQL.AppendLine("Update TblUser A  ");
                SQL.AppendLine("Left Join TblEmployee B On A.UserCode = B.UserCode ");
                SQL.AppendLine("Set A.Pwd=Ifnull(@EmpCode, @DefaultPwd), A.PwdLastUpDt=Left(@Dt, 8), A.LastUpBy=@CreateBy, A.LastUpDt=@Dt ");
                SQL.AppendLine("Where A.UserCode=@UserCode ; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (Sm.GetParameter("IsPasswordForLoginNeedToEncode") == "Y")
                {
                    Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCodeEncode);
                    Sm.CmParam<String>(ref cm, "@DefaultPwd", mDefaultPwdEncode);
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
                    Sm.CmParam<String>(ref cm, "@DefaultPwd", mDefaultPwd);
                }

                Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                Sm.StdMsg(mMsgType.Info, "Updating process is completed.");

                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string Id)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@Id", Id);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblResetPassword Where id = @Id",
                        new string[] 
                        {
                            "UserCode", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtUserCode.EditValue = Sm.DrStr(dr, c[0]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtUserCode, "User Code", false)||
                 IsUserCodeNotExisted();
        }


        private bool IsUserCodeNotExisted()
        {
            if (!Sm.IsDataExist("Select UserCode From TblUser Where UserCode ='" + TxtUserCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "User code ( " + TxtUserCode.Text + " ) not found.");
                return true;
            }
            return false;
        }


        #endregion

        #endregion

    }
}
