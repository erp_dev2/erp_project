﻿#region Update
/*
    12/11/2019 [TKG/SIER] New Application
    06/01/2020 [TKG/SIER] tambah panjang, lebar, tinggi, berat. tampilkan lot/bin. sembungikan qty 2 dan qty 3.
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    16/07/2020 [TKG/SIER] outstanding quantity dikurangi quantity outbound yg masih dalam status draft
    16/07/2020 [TKG/SIER] expired date dan stock status diambil dari source info
    22/07/2020 [IBL/SRN] List Of Item filter by vendor berdasarkan parameter StockOutboundFormat
 *  04/08/2020 [HAR/SIER] BUG stock outbond error query 
 *  24/08/2020 [ICA/SIER] Tambah Kolom Remark Inbound
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockOutboundDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmStockOutbound mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty;
        internal bool
            mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmStockOutboundDlg(FrmStockOutbound FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, B.Length, B.LengthUomCode, C.ItCtName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty-IfNull(F.Qty, 0.00) As Qty, B.InventoryUomCode, ");
            SQL.AppendLine("A.Qty2-IfNull(F.Qty2, 0.00) As Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("A.Qty3-IfNull(F.Qty3, 0.00) As Qty3, B.InventoryUomCode3, ");
            SQL.AppendLine("E.OptDesc, D.Value1 As ExpiredDt, D.RemarkInbound, ");
            SQL.AppendLine("B.Width, B.WidthUomCode, B.Height, B.HeightUomCode, B.Weight, B.WeightUomCode ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select t1.Value1, t2.remark As RemarkInbound, t1.Value2, t1.Source ");
            SQL.AppendLine("From TblSourceInfo t1 ");
            SQL.AppendLine("Inner join TblStockInboundDtl t2 On t1.Source=t2.Source And t2.CancelInd='N' ");
            SQL.AppendLine(")D On A.Source=D.Source ");
            SQL.AppendLine("Left Join TblOption E On D.Value2=E.OptCode And E.OptCat='StockStatus' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.Lot, A.Bin, A.Source, Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            SQL.AppendLine("    From TblStockOutboundHdr C ");
            SQL.AppendLine("    Inner Join TblStockOutboundDtl A On C.DocNo=A.DocNo ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Where C.Status In ('O', 'A') ");
            SQL.AppendLine("    And C.ProcessInd='D' ");
            SQL.AppendLine("    And C.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") F On A.Lot=F.Lot And A.Bin=F.Bin And A.Source=F.Source ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And B.ActInd='Y' ");
            if(mFrmParent.mStockOutboundFormat == "2")
                SQL.AppendLine("And (B.VdCode Is Null Or (B.VdCode Is Not Null And B.VdCode=@VdCode)) ");
            SQL.AppendLine("And A.Qty-IfNull(F.Qty, 0.00)>0.00 ");
            SQL.AppendLine(Filter1);
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By B.ItName, A.BatchNo;");
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "Item's Name", 
                        "Category",
                        "Batch#",

                        //6-10
                        "Source",
                        "Lot",
                        "Bin", 
                        "Stock",
                        "UoM",

                        //11-15
                        "Stock",
                        "UoM",
                        "Stock",
                        "UoM",
                        "Status",

                        //16-20
                        "Expired",
                        "Length",
                        "UoM",
                        "Width",
                        "UoM",

                        //21-25
                        "Height",
                        "UoM",
                        "Weight",
                        "UoM",
                        "Remark Inbound"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 200, 180, 
                        
                        //6-10
                        180, 60, 60, 100, 60, 
                        
                        //11-15
                        0, 0, 0, 0, 100,

                        //16-20
                        100, 80, 80, 80, 80,

                        //21-24
                        80, 80, 80, 80, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13, 17, 19, 21, 23 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 11, 12, 13, 14 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[25].Move(17);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter2 = string.Empty;

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "A", ref mFrmParent.Grd1, 7, 8, 9);
                var Filter = (Filter2.Length > 0) ? " And (" + Filter2 + ") " : " And 0=0 ";
                
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                if (mFrmParent.mStockOutboundFormat == "2")
                    Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(mFrmParent.LueCtCode));

                Filter2 = " ";
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "B.ItName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "A.Source", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter, Filter2),
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName", "ItCtName", "BatchNo", "Source",  "Lot", 
                            
                            //6-10
                            "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //11-15
                            "Qty3", "InventoryUomCode3", "OptDesc", "ExpiredDt", "Length", 
                            
                            //16-20
                            "LengthUomCode", "Width", "WidthUomCode", "Height", "HeightUomCode", 
                            
                            //21-23
                            "Weight", "WeightUomCode", "RemarkInbound"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 11, 13 });
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 25);


                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, Row1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 11, 14, 17 });
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }
            Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17, 22, 24, 26, 28 });
            mFrmParent.Grd1.EndUpdate();
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            else
            {
                if (mFrmParent.Grd1.Rows.Count>100000)
                    Sm.StdMsg(mMsgType.Warning,
                        "Number of rows : " + (mFrmParent.Grd1.Rows.Count-1).ToString() + Environment.NewLine +
                        "You've already choose data more than maximum limit ( 99.999 ).");
            }
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            string Key = string.Concat(
                Sm.GetGrdStr(Grd1, Row, 6),
                Sm.GetGrdStr(Grd1, Row, 7),
                Sm.GetGrdStr(Grd1, Row, 8));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key, string.Concat(
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 7), 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 9))
                    ))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

      
        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method 
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
           
        }
        #endregion 
        #endregion

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
