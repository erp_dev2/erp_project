﻿#region Update
/*
    09/01/2020 [HAR/IMS] Menu baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave4Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpLeave4 mFrmParent;
        private string 
            mLeaveCode = string.Empty,
            mAnnualLeaveCode = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty,
            mTrainingPosCode = string.Empty;
        private bool mIsLeaveUseLeaveSummary = false;

        #endregion

        #region Constructor

        public FrmEmpLeave4Dlg(FrmEmpLeave4 FrmParent, string LeaveCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mLeaveCode = LeaveCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
                mIsLeaveUseLeaveSummary = IsLeaveUseLeaveSummary();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                 Grd1,
                 new string[] 
                {
                    //0
                    "No.", 
                    
                    //1-5
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",
                    "Old Code",
                    "Position", 
                    "Department",

                    //6-10
                    "Site",
                    "Join",
                    "Resign",
                    "Permanent", 
                    "Period",

                    //11-13
                    "Start",
                    "End",
                    "Remaining"+Environment.NewLine+"Days"
                },
                  new int[] 
                {
                    //0
                    50, 

                    //1-5
                    100, 200, 100, 180, 180,  
                    
                    //6-10
                    180, 100, 100, 100, 100, 

                    //11-13
                    100, 100, 100
                }
             );
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7, 8, 9, 10, 11, 12 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                string Filter = " ";

                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(mFrmParent.DteStartDt));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@LeaveCode", mLeaveCode);
                Sm.CmParam<String>(ref cm, "@PosCode", mTrainingPosCode);
                
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                if (mIsLeaveUseLeaveSummary)
                {
                    SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, ");
                    SQL.AppendLine("B.PosName, C.DeptCode, C.DeptName, D.SiteCode, D.SiteName, ");
                    SQL.AppendLine("A.JoinDt, A.ResignDt, E.InitialDt As LeaveStartDt, ");
                    SQL.AppendLine("E.Yr, E.StartDt, E.EndDt, E.Balance ");
                    SQL.AppendLine("From TblEmployee A  ");
                    SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                    SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                    SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select EmpCode, Yr, StartDt, EndDt, InitialDt, Balance ");
                    SQL.AppendLine("    From TblLeaveSummary ");
                    SQL.AppendLine("    Where LeaveCode=@LeaveCode ");
                    SQL.AppendLine("    And @StartDt>=StartDt ");
                    SQL.AppendLine("    And @EndDt<=EndDt ");
                    SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");
                    SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                    SQL.AppendLine(Filter);
                    if (mTrainingPosCode.Length > 0)
                    {
                        SQL.AppendLine("And A.EmpCode Not In ( ");
                        SQL.AppendLine("    Select EmpCode From TblEmpPd ");
                        SQL.AppendLine("    Where JobTransfer = 'P' ");
                        SQL.AppendLine("    And PosCodeOld=@TrainingPosCode  ");
                        SQL.AppendLine("    )  ");
                    }
                    if (mFrmParent.mIsFilterBySiteHR)
                    {
                        SQL.AppendLine("And A.SiteCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupSite ");
                        SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine("    ) ");
                    }
                    if (mFrmParent.mIsFilterByDeptHR)
                    {
                        SQL.AppendLine("And A.DeptCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                }
                else
                {
                    SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, ");
                    SQL.AppendLine("B.PosName, C.DeptCode, C.DeptName, D.SiteCode, D.SiteName, ");
                    SQL.AppendLine("A.JoinDt, A.ResignDt, A.LeaveStartDt, ");
                    SQL.AppendLine("Null As Yr, Null As StartDt, Null As EndDt, Null As InitialDt, 0.00 As Balance ");
                    SQL.AppendLine("From TblEmployee A  ");
                    SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                    SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                    SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
                    SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                    SQL.AppendLine(Filter);
                    if (mFrmParent.mIsFilterBySiteHR)
                    {
                        SQL.AppendLine("And A.SiteCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupSite ");
                        SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("        ) ");
                        SQL.AppendLine("    ) ");
                    }
                    if (mFrmParent.mIsFilterByDeptHR)
                    {
                        SQL.AppendLine("And A.DeptCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine(" Order By D.SiteName, C.DeptName, A.EmpName; ");

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName", 

                    //6-10
                    "JoinDt", "ResignDt", "LeaveStartDt", "Yr", "StartDt", 
                    
                    //11-12
                    "EndDt", "Balance"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    }, true, false, false, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
             Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
             Sm.FocusGrd(Grd1, 0, 1);
             Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
         {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtEmpCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtEmpCodeOld.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtPosCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                Sm.SetDte(mFrmParent.DteJoinDt, Sm.GetGrdDate(Grd1, Row, 7).Substring(0, 8));
                if (Sm.GetGrdDate(Grd1, Row, 8).Length >= 8)
                    Sm.SetDte(mFrmParent.DteResignDt, Sm.GetGrdDate(Grd1, Row, 8).Substring(0, 8));
                if (Sm.GetGrdDate(Grd1, Row, 9).Length >= 8)
                    Sm.SetDte(mFrmParent.DteResignDt, Sm.GetGrdDate(Grd1, Row, 9).Substring(0, 8));
                mFrmParent.ComputeDurationDay();
                mFrmParent.ComputeDurationHour();
                this.Close();
             }
         }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
         {
             if (Sm.GetLue(LueFontSize).Length != 0)
             {
                 Grd1.Font = new Font(
                     Grd1.Font.FontFamily.Name.ToString(),
                     int.Parse(Sm.GetLue(LueFontSize))
                     );
             }
         }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            mTrainingPosCode = Sm.GetParameter("TrainingPosCode");
        }

        private bool IsLeaveUseLeaveSummary()
        { 
            if (mAnnualLeaveCode.Length >0 && Sm.CompareStr(mAnnualLeaveCode, mLeaveCode))
                return true;
            
            if (mLongServiceLeaveCode.Length>0 && Sm.CompareStr(mLongServiceLeaveCode, mLeaveCode))
                return true;
                
            if (mLongServiceLeaveCode2.Length >0 && Sm.CompareStr(mLongServiceLeaveCode2, mLeaveCode))
                return true;
                
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
        Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class EmpLeaveDtl
        {
            public string DNo { get; set; }
            public string LeaveDt { get; set; }
        }

        #endregion
    }
}
