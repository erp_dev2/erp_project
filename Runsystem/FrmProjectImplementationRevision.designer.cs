﻿namespace RunSystem
{
    partial class FrmProjectImplementationRevision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjectImplementationRevision));
            this.panel4 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.MeeApprovalRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtSOCDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtProjectImplementationDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtCustomerContactperson = new DevExpress.XtraEditors.TextEdit();
            this.SOCChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.SOCMeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.SOCLueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnProjectImplementationDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.SOCDteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueShpMCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.MeeProjectDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtSAName = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtAddress = new DevExpress.XtraEditors.TextEdit();
            this.TxtCity = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtCountry = new DevExpress.XtraEditors.TextEdit();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtCOAAmt = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpAdditional = new DevExpress.XtraTab.XtraTabPage();
            this.TcOutgoingPayment = new DevExpress.XtraTab.XtraTabControl();
            this.TpBOQ = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpItem = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpWBS = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.TxtTotalPersentase = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtTotalBobot = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtTotalPrice = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.BtnImportWBS = new DevExpress.XtraEditors.SimpleButton();
            this.DtePlanEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DtePlanStartDt = new DevExpress.XtraEditors.DateEdit();
            this.LueTaskCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStageCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpWBS2 = new DevExpress.XtraTab.XtraTabPage();
            this.LueTaskCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStageCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.DtePlanEndDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DtePlanStartDt2 = new DevExpress.XtraEditors.DateEdit();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.TxtTotalPrice2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalPersentase2 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.TxtTotalBobot2 = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.TxtTotalAchievement = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.TpResource = new DevExpress.XtraTab.XtraTabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.LblPPh = new System.Windows.Forms.Label();
            this.LblPPN = new System.Windows.Forms.Label();
            this.TxtPPhAmt = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtContractAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtNetCash = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtExclPPN = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtPPNAmt = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtTotalResource = new DevExpress.XtraEditors.TextEdit();
            this.LblTotalResource = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtPPNPPhPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtPPNPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TxtDirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtRemunerationCost = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.BtnImportResource = new DevExpress.XtraEditors.SimpleButton();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpIssue = new DevExpress.XtraTab.XtraTabPage();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpDocument = new DevExpress.XtraTab.XtraTabPage();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.TpApproval = new DevExpress.XtraTab.XtraTabPage();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectImplementationDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomerContactperson.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCMeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCLueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCDteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCDteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcOutgoingPayment)).BeginInit();
            this.TcOutgoingPayment.SuspendLayout();
            this.TpBOQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpWBS.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPersentase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpWBS2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties)).BeginInit();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPersentase2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAchievement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            this.TpResource.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPhAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExclPPN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPPhPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpIssue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpDocument.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.TpApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(932, 0);
            this.panel1.Size = new System.Drawing.Size(70, 701);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcOutgoingPayment);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(932, 593);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 593);
            this.panel3.Size = new System.Drawing.Size(932, 108);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 679);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(932, 108);
            this.Grd1.TabIndex = 100;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label53);
            this.panel4.Controls.Add(this.MeeApprovalRemark);
            this.panel4.Controls.Add(this.TxtSOCDocNo);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.TxtStatus);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.TxtProjectImplementationDocNo);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.DteDocDt);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.TxtCustomerContactperson);
            this.panel4.Controls.Add(this.SOCChkCancelInd);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.SOCMeeCancelReason);
            this.panel4.Controls.Add(this.TxtLocalDocNo);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.SOCLueStatus);
            this.panel4.Controls.Add(this.BtnProjectImplementationDocNo);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.TxtBOQDocNo);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.LueCtCode);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.SOCDteDocDt);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtDocNo);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(932, 310);
            this.panel4.TabIndex = 11;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(54, 91);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(98, 14);
            this.label53.TabIndex = 21;
            this.label53.Text = "Approval Remark";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeApprovalRemark
            // 
            this.MeeApprovalRemark.EnterMoveNextControl = true;
            this.MeeApprovalRemark.Location = new System.Drawing.Point(155, 89);
            this.MeeApprovalRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeApprovalRemark.Name = "MeeApprovalRemark";
            this.MeeApprovalRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeApprovalRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeApprovalRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeApprovalRemark.Properties.MaxLength = 5000;
            this.MeeApprovalRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeApprovalRemark.Properties.ReadOnly = true;
            this.MeeApprovalRemark.Properties.ShowIcon = false;
            this.MeeApprovalRemark.Size = new System.Drawing.Size(264, 20);
            this.MeeApprovalRemark.TabIndex = 22;
            this.MeeApprovalRemark.ToolTip = "F4 : Show/hide text";
            this.MeeApprovalRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeApprovalRemark.ToolTipTitle = "Run System";
            // 
            // TxtSOCDocNo
            // 
            this.TxtSOCDocNo.EnterMoveNextControl = true;
            this.TxtSOCDocNo.Location = new System.Drawing.Point(155, 135);
            this.TxtSOCDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOCDocNo.Name = "TxtSOCDocNo";
            this.TxtSOCDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOCDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOCDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOCDocNo.Properties.MaxLength = 16;
            this.TxtSOCDocNo.Properties.ReadOnly = true;
            this.TxtSOCDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtSOCDocNo.TabIndex = 25;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(52, 137);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(100, 14);
            this.label24.TabIndex = 24;
            this.label24.Text = "SOC Document#";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(155, 68);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(264, 20);
            this.TxtStatus.TabIndex = 20;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(155, 116);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(111, 14);
            this.label30.TabIndex = 23;
            this.label30.Text = "SO Contract\'s Data";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectImplementationDocNo
            // 
            this.TxtProjectImplementationDocNo.EnterMoveNextControl = true;
            this.TxtProjectImplementationDocNo.Location = new System.Drawing.Point(155, 26);
            this.TxtProjectImplementationDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectImplementationDocNo.Name = "TxtProjectImplementationDocNo";
            this.TxtProjectImplementationDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectImplementationDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectImplementationDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectImplementationDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectImplementationDocNo.Properties.MaxLength = 16;
            this.TxtProjectImplementationDocNo.Properties.ReadOnly = true;
            this.TxtProjectImplementationDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtProjectImplementationDocNo.TabIndex = 15;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(79, 8);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 14);
            this.label29.TabIndex = 12;
            this.label29.Text = "Document#";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(59, 70);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 14);
            this.label25.TabIndex = 19;
            this.label25.Text = "Approval Status";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(155, 47);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(125, 20);
            this.DteDocDt.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(119, 50);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 14);
            this.label26.TabIndex = 17;
            this.label26.Text = "Date";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCustomerContactperson
            // 
            this.TxtCustomerContactperson.EnterMoveNextControl = true;
            this.TxtCustomerContactperson.Location = new System.Drawing.Point(155, 282);
            this.TxtCustomerContactperson.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCustomerContactperson.Name = "TxtCustomerContactperson";
            this.TxtCustomerContactperson.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCustomerContactperson.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCustomerContactperson.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCustomerContactperson.Properties.Appearance.Options.UseFont = true;
            this.TxtCustomerContactperson.Properties.MaxLength = 20;
            this.TxtCustomerContactperson.Properties.ReadOnly = true;
            this.TxtCustomerContactperson.Size = new System.Drawing.Size(264, 20);
            this.TxtCustomerContactperson.TabIndex = 40;
            // 
            // SOCChkCancelInd
            // 
            this.SOCChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SOCChkCancelInd.Location = new System.Drawing.Point(361, 197);
            this.SOCChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SOCChkCancelInd.Name = "SOCChkCancelInd";
            this.SOCChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.SOCChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.SOCChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.SOCChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.SOCChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.SOCChkCancelInd.Properties.Caption = "Cancel";
            this.SOCChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.SOCChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.SOCChkCancelInd.TabIndex = 32;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(67, 200);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 14);
            this.label27.TabIndex = 30;
            this.label27.Text = "Cancel Reason";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SOCMeeCancelReason
            // 
            this.SOCMeeCancelReason.EnterMoveNextControl = true;
            this.SOCMeeCancelReason.Location = new System.Drawing.Point(155, 198);
            this.SOCMeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.SOCMeeCancelReason.Name = "SOCMeeCancelReason";
            this.SOCMeeCancelReason.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.SOCMeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCMeeCancelReason.Properties.Appearance.Options.UseBackColor = true;
            this.SOCMeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.SOCMeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCMeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.SOCMeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCMeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.SOCMeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCMeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.SOCMeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCMeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.SOCMeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SOCMeeCancelReason.Properties.MaxLength = 250;
            this.SOCMeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.SOCMeeCancelReason.Properties.ReadOnly = true;
            this.SOCMeeCancelReason.Properties.ShowIcon = false;
            this.SOCMeeCancelReason.Size = new System.Drawing.Size(202, 20);
            this.SOCMeeCancelReason.TabIndex = 31;
            this.SOCMeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.SOCMeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.SOCMeeCancelReason.ToolTipTitle = "Run System";
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(155, 219);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Properties.ReadOnly = true;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtLocalDocNo.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(57, 222);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 14);
            this.label20.TabIndex = 33;
            this.label20.Text = "Local Document";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SOCLueStatus
            // 
            this.SOCLueStatus.EnterMoveNextControl = true;
            this.SOCLueStatus.Location = new System.Drawing.Point(155, 177);
            this.SOCLueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.SOCLueStatus.Name = "SOCLueStatus";
            this.SOCLueStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.SOCLueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCLueStatus.Properties.Appearance.Options.UseBackColor = true;
            this.SOCLueStatus.Properties.Appearance.Options.UseFont = true;
            this.SOCLueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCLueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.SOCLueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCLueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.SOCLueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCLueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.SOCLueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCLueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.SOCLueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SOCLueStatus.Properties.DropDownRows = 20;
            this.SOCLueStatus.Properties.NullText = "[Empty]";
            this.SOCLueStatus.Properties.PopupWidth = 500;
            this.SOCLueStatus.Properties.ReadOnly = true;
            this.SOCLueStatus.Size = new System.Drawing.Size(202, 20);
            this.SOCLueStatus.TabIndex = 29;
            this.SOCLueStatus.ToolTip = "F4 : Show/hide list";
            this.SOCLueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.SOCLueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged_1);
            // 
            // BtnProjectImplementationDocNo
            // 
            this.BtnProjectImplementationDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProjectImplementationDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProjectImplementationDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProjectImplementationDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProjectImplementationDocNo.Appearance.Options.UseBackColor = true;
            this.BtnProjectImplementationDocNo.Appearance.Options.UseFont = true;
            this.BtnProjectImplementationDocNo.Appearance.Options.UseForeColor = true;
            this.BtnProjectImplementationDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnProjectImplementationDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProjectImplementationDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProjectImplementationDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnProjectImplementationDocNo.Image")));
            this.BtnProjectImplementationDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProjectImplementationDocNo.Location = new System.Drawing.Point(420, 25);
            this.BtnProjectImplementationDocNo.Name = "BtnProjectImplementationDocNo";
            this.BtnProjectImplementationDocNo.Size = new System.Drawing.Size(20, 21);
            this.BtnProjectImplementationDocNo.TabIndex = 16;
            this.BtnProjectImplementationDocNo.ToolTip = "Show Project Implementation";
            this.BtnProjectImplementationDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProjectImplementationDocNo.ToolTipTitle = "Run System";
            this.BtnProjectImplementationDocNo.Click += new System.EventHandler(this.BtnProjectImplementationDocNo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(61, 284);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 14);
            this.label4.TabIndex = 39;
            this.label4.Text = "Contact Person";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(155, 261);
            this.TxtBOQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 16;
            this.TxtBOQDocNo.Properties.ReadOnly = true;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtBOQDocNo.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(66, 263);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 14);
            this.label7.TabIndex = 37;
            this.label7.Text = "Bill of Quantity";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(93, 243);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 35;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(155, 240);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Properties.ReadOnly = true;
            this.LueCtCode.Size = new System.Drawing.Size(264, 20);
            this.LueCtCode.TabIndex = 36;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(59, 179);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 14);
            this.label12.TabIndex = 28;
            this.label12.Text = "Approval Status";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SOCDteDocDt
            // 
            this.SOCDteDocDt.EditValue = null;
            this.SOCDteDocDt.EnterMoveNextControl = true;
            this.SOCDteDocDt.Location = new System.Drawing.Point(155, 156);
            this.SOCDteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SOCDteDocDt.Name = "SOCDteDocDt";
            this.SOCDteDocDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.SOCDteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCDteDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.SOCDteDocDt.Properties.Appearance.Options.UseFont = true;
            this.SOCDteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SOCDteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.SOCDteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SOCDteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.SOCDteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.SOCDteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.SOCDteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.SOCDteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.SOCDteDocDt.Properties.ReadOnly = true;
            this.SOCDteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SOCDteDocDt.Size = new System.Drawing.Size(125, 20);
            this.SOCDteDocDt.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(119, 159);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 26;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(155, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(7, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "Project Implementation#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.TxtProjectName);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.LueShpMCode);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.MeeProjectDesc);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.LueSPCode);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.TxtMobile);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.TxtSAName);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtEmail);
            this.panel6.Controls.Add(this.TxtFax);
            this.panel6.Controls.Add(this.TxtAmt);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtAddress);
            this.panel6.Controls.Add(this.TxtCity);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.TxtPostalCd);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.TxtPhone);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.TxtCountry);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(543, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(389, 310);
            this.panel6.TabIndex = 41;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(119, 4);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 20;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(264, 20);
            this.TxtProjectName.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(33, 7);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 14);
            this.label16.TabIndex = 42;
            this.label16.Text = "Project Name";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueShpMCode
            // 
            this.LueShpMCode.EnterMoveNextControl = true;
            this.LueShpMCode.Location = new System.Drawing.Point(119, 235);
            this.LueShpMCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueShpMCode.Name = "LueShpMCode";
            this.LueShpMCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueShpMCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueShpMCode.Properties.Appearance.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShpMCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShpMCode.Properties.DropDownRows = 30;
            this.LueShpMCode.Properties.NullText = "[Empty]";
            this.LueShpMCode.Properties.PopupWidth = 300;
            this.LueShpMCode.Properties.ReadOnly = true;
            this.LueShpMCode.Size = new System.Drawing.Size(264, 20);
            this.LueShpMCode.TabIndex = 66;
            this.LueShpMCode.ToolTip = "F4 : Show/hide list";
            this.LueShpMCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShpMCode.EditValueChanged += new System.EventHandler(this.LueShpMCode_EditValueChanged_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(15, 238);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 14);
            this.label22.TabIndex = 65;
            this.label22.Text = "Shipping Method";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeProjectDesc
            // 
            this.MeeProjectDesc.EnterMoveNextControl = true;
            this.MeeProjectDesc.Location = new System.Drawing.Point(119, 25);
            this.MeeProjectDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeProjectDesc.Name = "MeeProjectDesc";
            this.MeeProjectDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeProjectDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.Appearance.Options.UseBackColor = true;
            this.MeeProjectDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeProjectDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeProjectDesc.Properties.MaxLength = 5000;
            this.MeeProjectDesc.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeProjectDesc.Properties.ReadOnly = true;
            this.MeeProjectDesc.Properties.ShowIcon = false;
            this.MeeProjectDesc.Size = new System.Drawing.Size(264, 20);
            this.MeeProjectDesc.TabIndex = 45;
            this.MeeProjectDesc.ToolTip = "F4 : Show/hide text";
            this.MeeProjectDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeProjectDesc.ToolTipTitle = "Run System";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(39, 217);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 14);
            this.label21.TabIndex = 63;
            this.label21.Text = "Sales Person";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(119, 214);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 300;
            this.LueSPCode.Properties.ReadOnly = true;
            this.LueSPCode.Size = new System.Drawing.Size(264, 20);
            this.LueSPCode.TabIndex = 64;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged_1);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(4, 27);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 14);
            this.label23.TabIndex = 44;
            this.label23.Text = "Project Description";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(119, 193);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Properties.ReadOnly = true;
            this.TxtMobile.Size = new System.Drawing.Size(264, 20);
            this.TxtMobile.TabIndex = 62;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(73, 195);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 14);
            this.label17.TabIndex = 61;
            this.label17.Text = "Mobile";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(80, 174);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 14);
            this.label5.TabIndex = 59;
            this.label5.Text = "Email";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSAName
            // 
            this.TxtSAName.EnterMoveNextControl = true;
            this.TxtSAName.Location = new System.Drawing.Point(119, 46);
            this.TxtSAName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSAName.Name = "TxtSAName";
            this.TxtSAName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSAName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSAName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSAName.Properties.Appearance.Options.UseFont = true;
            this.TxtSAName.Properties.MaxLength = 20;
            this.TxtSAName.Properties.ReadOnly = true;
            this.TxtSAName.Size = new System.Drawing.Size(264, 20);
            this.TxtSAName.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(89, 153);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 14);
            this.label6.TabIndex = 57;
            this.label6.Text = "Fax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(119, 172);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Properties.ReadOnly = true;
            this.TxtEmail.Size = new System.Drawing.Size(264, 20);
            this.TxtEmail.TabIndex = 60;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(119, 151);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 40;
            this.TxtFax.Properties.ReadOnly = true;
            this.TxtFax.Size = new System.Drawing.Size(264, 20);
            this.TxtFax.TabIndex = 58;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(119, 256);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(264, 20);
            this.TxtAmt.TabIndex = 68;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(72, 133);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 55;
            this.label14.Text = "Phone";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(43, 111);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 14);
            this.label13.TabIndex = 53;
            this.label13.Text = "Postal Code";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAddress
            // 
            this.TxtAddress.EnterMoveNextControl = true;
            this.TxtAddress.Location = new System.Drawing.Point(119, 67);
            this.TxtAddress.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAddress.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAddress.Properties.Appearance.Options.UseFont = true;
            this.TxtAddress.Properties.MaxLength = 20;
            this.TxtAddress.Properties.ReadOnly = true;
            this.TxtAddress.Size = new System.Drawing.Size(264, 20);
            this.TxtAddress.TabIndex = 49;
            // 
            // TxtCity
            // 
            this.TxtCity.EnterMoveNextControl = true;
            this.TxtCity.Location = new System.Drawing.Point(248, 88);
            this.TxtCity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCity.Properties.Appearance.Options.UseFont = true;
            this.TxtCity.Properties.MaxLength = 20;
            this.TxtCity.Properties.ReadOnly = true;
            this.TxtCity.Size = new System.Drawing.Size(135, 20);
            this.TxtCity.TabIndex = 52;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(26, 49);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 14);
            this.label11.TabIndex = 46;
            this.label11.Text = "Shipping Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(63, 258);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 14);
            this.label9.TabIndex = 67;
            this.label9.Text = "Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(119, 109);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Properties.ReadOnly = true;
            this.TxtPostalCd.Size = new System.Drawing.Size(264, 20);
            this.TxtPostalCd.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(65, 70);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 48;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(119, 277);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ReadOnly = true;
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(264, 20);
            this.MeeRemark.TabIndex = 70;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(37, 91);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 14);
            this.label10.TabIndex = 50;
            this.label10.Text = "Country, City";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(119, 130);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Properties.ReadOnly = true;
            this.TxtPhone.Size = new System.Drawing.Size(264, 20);
            this.TxtPhone.TabIndex = 56;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(67, 279);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 69;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCountry
            // 
            this.TxtCountry.EnterMoveNextControl = true;
            this.TxtCountry.Location = new System.Drawing.Point(119, 88);
            this.TxtCountry.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCountry.Name = "TxtCountry";
            this.TxtCountry.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCountry.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCountry.Properties.Appearance.Options.UseFont = true;
            this.TxtCountry.Properties.MaxLength = 20;
            this.TxtCountry.Properties.ReadOnly = true;
            this.TxtCountry.Size = new System.Drawing.Size(125, 20);
            this.TxtCountry.TabIndex = 51;
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(766, 289);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 238);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(766, 51);
            this.panel7.TabIndex = 25;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(536, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(230, 51);
            this.panel8.TabIndex = 29;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(6, 8);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 14);
            this.label28.TabIndex = 30;
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCOAAmt
            // 
            this.TxtCOAAmt.EnterMoveNextControl = true;
            this.TxtCOAAmt.Location = new System.Drawing.Point(63, 5);
            this.TxtCOAAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCOAAmt.Name = "TxtCOAAmt";
            this.TxtCOAAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCOAAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCOAAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCOAAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCOAAmt.Properties.ReadOnly = true;
            this.TxtCOAAmt.Size = new System.Drawing.Size(160, 20);
            this.TxtCOAAmt.TabIndex = 31;
            this.TxtCOAAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(8, 8);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 14);
            this.label18.TabIndex = 26;
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo2
            // 
            this.TxtVoucherDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherDocNo2.Location = new System.Drawing.Point(124, 26);
            this.TxtVoucherDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo2.Name = "TxtVoucherDocNo2";
            this.TxtVoucherDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherDocNo2.TabIndex = 49;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(57, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 28;
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherRequestDocNo2
            // 
            this.TxtVoucherRequestDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo2.Location = new System.Drawing.Point(124, 5);
            this.TxtVoucherRequestDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo2.Name = "TxtVoucherRequestDocNo2";
            this.TxtVoucherRequestDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherRequestDocNo2.TabIndex = 27;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 19;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 238);
            this.Grd4.TabIndex = 24;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpAdditional
            // 
            this.TpAdditional.Appearance.Header.Options.UseTextOptions = true;
            this.TpAdditional.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAdditional.Name = "TpAdditional";
            this.TpAdditional.Size = new System.Drawing.Size(766, 289);
            // 
            // TcOutgoingPayment
            // 
            this.TcOutgoingPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcOutgoingPayment.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcOutgoingPayment.Location = new System.Drawing.Point(0, 310);
            this.TcOutgoingPayment.Name = "TcOutgoingPayment";
            this.TcOutgoingPayment.SelectedTabPage = this.TpBOQ;
            this.TcOutgoingPayment.Size = new System.Drawing.Size(932, 283);
            this.TcOutgoingPayment.TabIndex = 71;
            this.TcOutgoingPayment.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpBOQ,
            this.TpItem,
            this.TpWBS,
            this.TpWBS2,
            this.TpResource,
            this.TpIssue,
            this.TpDocument,
            this.TpApproval});
            // 
            // TpBOQ
            // 
            this.TpBOQ.Appearance.Header.Options.UseTextOptions = true;
            this.TpBOQ.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBOQ.Controls.Add(this.Grd2);
            this.TpBOQ.Name = "TpBOQ";
            this.TpBOQ.Size = new System.Drawing.Size(926, 255);
            this.TpBOQ.Text = "List of Item Bill of Quantity";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(926, 255);
            this.Grd2.TabIndex = 70;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            // 
            // TpItem
            // 
            this.TpItem.Appearance.Header.Options.UseTextOptions = true;
            this.TpItem.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpItem.Controls.Add(this.Grd3);
            this.TpItem.Name = "TpItem";
            this.TpItem.Size = new System.Drawing.Size(766, 255);
            this.TpItem.Text = "List of Item";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 255);
            this.Grd3.TabIndex = 70;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpWBS
            // 
            this.TpWBS.Appearance.Header.Options.UseTextOptions = true;
            this.TpWBS.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpWBS.Controls.Add(this.panel5);
            this.TpWBS.Controls.Add(this.DtePlanEndDt);
            this.TpWBS.Controls.Add(this.DtePlanStartDt);
            this.TpWBS.Controls.Add(this.LueTaskCode);
            this.TpWBS.Controls.Add(this.LueStageCode);
            this.TpWBS.Controls.Add(this.Grd5);
            this.TpWBS.Name = "TpWBS";
            this.TpWBS.Size = new System.Drawing.Size(766, 255);
            this.TpWBS.Text = "WBS";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.BtnImportWBS);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 34);
            this.panel5.TabIndex = 72;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.TxtTotalPersentase);
            this.panel11.Controls.Add(this.label34);
            this.panel11.Controls.Add(this.TxtTotalBobot);
            this.panel11.Controls.Add(this.label32);
            this.panel11.Controls.Add(this.TxtTotalPrice);
            this.panel11.Controls.Add(this.label33);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(79, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(687, 34);
            this.panel11.TabIndex = 72;
            // 
            // TxtTotalPersentase
            // 
            this.TxtTotalPersentase.EnterMoveNextControl = true;
            this.TxtTotalPersentase.Location = new System.Drawing.Point(550, 6);
            this.TxtTotalPersentase.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPersentase.Name = "TxtTotalPersentase";
            this.TxtTotalPersentase.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPersentase.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPersentase.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPersentase.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPersentase.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPersentase.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPersentase.Properties.ReadOnly = true;
            this.TxtTotalPersentase.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalPersentase.TabIndex = 79;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(445, 8);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 14);
            this.label34.TabIndex = 78;
            this.label34.Text = "Total Percentage";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalBobot
            // 
            this.TxtTotalBobot.EnterMoveNextControl = true;
            this.TxtTotalBobot.Location = new System.Drawing.Point(303, 6);
            this.TxtTotalBobot.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBobot.Name = "TxtTotalBobot";
            this.TxtTotalBobot.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalBobot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBobot.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBobot.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBobot.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalBobot.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalBobot.Properties.ReadOnly = true;
            this.TxtTotalBobot.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalBobot.TabIndex = 77;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(13, 8);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 14);
            this.label32.TabIndex = 74;
            this.label32.Text = "Total Price";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalPrice
            // 
            this.TxtTotalPrice.EnterMoveNextControl = true;
            this.TxtTotalPrice.Location = new System.Drawing.Point(82, 6);
            this.TxtTotalPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPrice.Name = "TxtTotalPrice";
            this.TxtTotalPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPrice.Properties.ReadOnly = true;
            this.TxtTotalPrice.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalPrice.TabIndex = 75;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(227, 8);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 14);
            this.label33.TabIndex = 76;
            this.label33.Text = "Total Bobot";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnImportWBS
            // 
            this.BtnImportWBS.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImportWBS.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImportWBS.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImportWBS.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImportWBS.Appearance.Options.UseBackColor = true;
            this.BtnImportWBS.Appearance.Options.UseFont = true;
            this.BtnImportWBS.Appearance.Options.UseForeColor = true;
            this.BtnImportWBS.Appearance.Options.UseTextOptions = true;
            this.BtnImportWBS.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImportWBS.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImportWBS.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImportWBS.Location = new System.Drawing.Point(3, 5);
            this.BtnImportWBS.Name = "BtnImportWBS";
            this.BtnImportWBS.Size = new System.Drawing.Size(86, 21);
            this.BtnImportWBS.TabIndex = 73;
            this.BtnImportWBS.Text = "Import Data";
            this.BtnImportWBS.ToolTip = "Import Data";
            this.BtnImportWBS.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImportWBS.ToolTipTitle = "Run System";
            this.BtnImportWBS.Click += new System.EventHandler(this.BtnImportWBS_Click);
            // 
            // DtePlanEndDt
            // 
            this.DtePlanEndDt.EditValue = null;
            this.DtePlanEndDt.EnterMoveNextControl = true;
            this.DtePlanEndDt.Location = new System.Drawing.Point(619, 54);
            this.DtePlanEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanEndDt.Name = "DtePlanEndDt";
            this.DtePlanEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt.Properties.Appearance.Options.UseFont = true;
            this.DtePlanEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanEndDt.Size = new System.Drawing.Size(138, 20);
            this.DtePlanEndDt.TabIndex = 76;
            this.DtePlanEndDt.Visible = false;
            this.DtePlanEndDt.Leave += new System.EventHandler(this.DtePlanEndDt_Leave);
            // 
            // DtePlanStartDt
            // 
            this.DtePlanStartDt.EditValue = null;
            this.DtePlanStartDt.EnterMoveNextControl = true;
            this.DtePlanStartDt.Location = new System.Drawing.Point(479, 49);
            this.DtePlanStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanStartDt.Name = "DtePlanStartDt";
            this.DtePlanStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt.Properties.Appearance.Options.UseFont = true;
            this.DtePlanStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanStartDt.Size = new System.Drawing.Size(138, 20);
            this.DtePlanStartDt.TabIndex = 75;
            this.DtePlanStartDt.Visible = false;
            this.DtePlanStartDt.Leave += new System.EventHandler(this.DtePlanStartDt_Leave);
            // 
            // LueTaskCode
            // 
            this.LueTaskCode.EnterMoveNextControl = true;
            this.LueTaskCode.Location = new System.Drawing.Point(313, 51);
            this.LueTaskCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaskCode.Name = "LueTaskCode";
            this.LueTaskCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaskCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaskCode.Properties.DropDownRows = 30;
            this.LueTaskCode.Properties.NullText = "[Empty]";
            this.LueTaskCode.Properties.PopupWidth = 150;
            this.LueTaskCode.Size = new System.Drawing.Size(152, 20);
            this.LueTaskCode.TabIndex = 74;
            this.LueTaskCode.ToolTip = "F4 : Show/hide list";
            this.LueTaskCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaskCode.EditValueChanged += new System.EventHandler(this.LueTaskCode_EditValueChanged);
            this.LueTaskCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTaskCode_KeyDown);
            this.LueTaskCode.Leave += new System.EventHandler(this.LueTaskCode_Leave);
            // 
            // LueStageCode
            // 
            this.LueStageCode.EnterMoveNextControl = true;
            this.LueStageCode.Location = new System.Drawing.Point(82, 49);
            this.LueStageCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueStageCode.Name = "LueStageCode";
            this.LueStageCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.Appearance.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStageCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStageCode.Properties.DropDownRows = 30;
            this.LueStageCode.Properties.NullText = "[Empty]";
            this.LueStageCode.Properties.PopupWidth = 150;
            this.LueStageCode.Size = new System.Drawing.Size(224, 20);
            this.LueStageCode.TabIndex = 73;
            this.LueStageCode.ToolTip = "F4 : Show/hide list";
            this.LueStageCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStageCode.EditValueChanged += new System.EventHandler(this.LueStageCode_EditValueChanged);
            this.LueStageCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStageCode_KeyDown);
            this.LueStageCode.Leave += new System.EventHandler(this.LueStageCode_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 34);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 221);
            this.Grd5.TabIndex = 72;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpWBS2
            // 
            this.TpWBS2.Appearance.Header.Options.UseTextOptions = true;
            this.TpWBS2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpWBS2.Controls.Add(this.LueTaskCode2);
            this.TpWBS2.Controls.Add(this.LueStageCode2);
            this.TpWBS2.Controls.Add(this.DtePlanEndDt2);
            this.TpWBS2.Controls.Add(this.DtePlanStartDt2);
            this.TpWBS2.Controls.Add(this.panel13);
            this.TpWBS2.Controls.Add(this.Grd9);
            this.TpWBS2.Name = "TpWBS2";
            this.TpWBS2.Size = new System.Drawing.Size(766, 255);
            this.TpWBS2.Text = "WBS 2";
            // 
            // LueTaskCode2
            // 
            this.LueTaskCode2.EnterMoveNextControl = true;
            this.LueTaskCode2.Location = new System.Drawing.Point(391, 61);
            this.LueTaskCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaskCode2.Name = "LueTaskCode2";
            this.LueTaskCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaskCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaskCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaskCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaskCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaskCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaskCode2.Properties.DropDownRows = 30;
            this.LueTaskCode2.Properties.NullText = "[Empty]";
            this.LueTaskCode2.Properties.PopupWidth = 150;
            this.LueTaskCode2.Size = new System.Drawing.Size(224, 20);
            this.LueTaskCode2.TabIndex = 88;
            this.LueTaskCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaskCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaskCode2.EditValueChanged += new System.EventHandler(this.LueTaskCode2_EditValueChanged);
            this.LueTaskCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTaskCode2_KeyDown);
            this.LueTaskCode2.Leave += new System.EventHandler(this.LueTaskCode2_Leave);
            // 
            // LueStageCode2
            // 
            this.LueStageCode2.EnterMoveNextControl = true;
            this.LueStageCode2.Location = new System.Drawing.Point(157, 61);
            this.LueStageCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueStageCode2.Name = "LueStageCode2";
            this.LueStageCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.Appearance.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStageCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStageCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStageCode2.Properties.DropDownRows = 30;
            this.LueStageCode2.Properties.NullText = "[Empty]";
            this.LueStageCode2.Properties.PopupWidth = 150;
            this.LueStageCode2.Size = new System.Drawing.Size(224, 20);
            this.LueStageCode2.TabIndex = 87;
            this.LueStageCode2.ToolTip = "F4 : Show/hide list";
            this.LueStageCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStageCode2.EditValueChanged += new System.EventHandler(this.LueStageCode2_EditValueChanged);
            this.LueStageCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStageCode2_KeyDown);
            this.LueStageCode2.Leave += new System.EventHandler(this.LueStageCode2_Leave);
            // 
            // DtePlanEndDt2
            // 
            this.DtePlanEndDt2.EditValue = null;
            this.DtePlanEndDt2.EnterMoveNextControl = true;
            this.DtePlanEndDt2.Location = new System.Drawing.Point(625, 115);
            this.DtePlanEndDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanEndDt2.Name = "DtePlanEndDt2";
            this.DtePlanEndDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt2.Properties.Appearance.Options.UseFont = true;
            this.DtePlanEndDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanEndDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanEndDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanEndDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanEndDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanEndDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanEndDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanEndDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanEndDt2.Size = new System.Drawing.Size(138, 20);
            this.DtePlanEndDt2.TabIndex = 86;
            this.DtePlanEndDt2.Visible = false;
            this.DtePlanEndDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanStartDt2_KeyDown);
            this.DtePlanEndDt2.Leave += new System.EventHandler(this.DtePlanEndDt2_Leave);
            // 
            // DtePlanStartDt2
            // 
            this.DtePlanStartDt2.EditValue = null;
            this.DtePlanStartDt2.EnterMoveNextControl = true;
            this.DtePlanStartDt2.Location = new System.Drawing.Point(474, 115);
            this.DtePlanStartDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePlanStartDt2.Name = "DtePlanStartDt2";
            this.DtePlanStartDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt2.Properties.Appearance.Options.UseFont = true;
            this.DtePlanStartDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePlanStartDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePlanStartDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePlanStartDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePlanStartDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePlanStartDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePlanStartDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePlanStartDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePlanStartDt2.Size = new System.Drawing.Size(138, 20);
            this.DtePlanStartDt2.TabIndex = 85;
            this.DtePlanStartDt2.Visible = false;
            this.DtePlanStartDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DtePlanStartDt2_KeyDown);
            this.DtePlanStartDt2.Leave += new System.EventHandler(this.DtePlanStartDt2_Leave);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(766, 34);
            this.panel13.TabIndex = 74;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.label54);
            this.panel14.Controls.Add(this.TxtTotalPrice2);
            this.panel14.Controls.Add(this.TxtTotalPersentase2);
            this.panel14.Controls.Add(this.label50);
            this.panel14.Controls.Add(this.TxtTotalBobot2);
            this.panel14.Controls.Add(this.label51);
            this.panel14.Controls.Add(this.TxtTotalAchievement);
            this.panel14.Controls.Add(this.label52);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(-160, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(926, 34);
            this.panel14.TabIndex = 75;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(15, 8);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(65, 14);
            this.label54.TabIndex = 74;
            this.label54.Text = "Total Price";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalPrice2
            // 
            this.TxtTotalPrice2.EnterMoveNextControl = true;
            this.TxtTotalPrice2.Location = new System.Drawing.Point(84, 6);
            this.TxtTotalPrice2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPrice2.Name = "TxtTotalPrice2";
            this.TxtTotalPrice2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPrice2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPrice2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPrice2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPrice2.Properties.ReadOnly = true;
            this.TxtTotalPrice2.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalPrice2.TabIndex = 75;
            // 
            // TxtTotalPersentase2
            // 
            this.TxtTotalPersentase2.EnterMoveNextControl = true;
            this.TxtTotalPersentase2.Location = new System.Drawing.Point(788, 6);
            this.TxtTotalPersentase2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPersentase2.Name = "TxtTotalPersentase2";
            this.TxtTotalPersentase2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPersentase2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPersentase2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPersentase2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPersentase2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPersentase2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPersentase2.Properties.ReadOnly = true;
            this.TxtTotalPersentase2.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalPersentase2.TabIndex = 81;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(683, 8);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(102, 14);
            this.label50.TabIndex = 80;
            this.label50.Text = "Total Percentage";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalBobot2
            // 
            this.TxtTotalBobot2.EnterMoveNextControl = true;
            this.TxtTotalBobot2.Location = new System.Drawing.Point(547, 6);
            this.TxtTotalBobot2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBobot2.Name = "TxtTotalBobot2";
            this.TxtTotalBobot2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalBobot2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBobot2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBobot2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBobot2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalBobot2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalBobot2.Properties.ReadOnly = true;
            this.TxtTotalBobot2.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalBobot2.TabIndex = 79;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(219, 8);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(111, 14);
            this.label51.TabIndex = 76;
            this.label51.Text = "Total Achievement";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalAchievement
            // 
            this.TxtTotalAchievement.EnterMoveNextControl = true;
            this.TxtTotalAchievement.Location = new System.Drawing.Point(336, 6);
            this.TxtTotalAchievement.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAchievement.Name = "TxtTotalAchievement";
            this.TxtTotalAchievement.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalAchievement.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAchievement.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAchievement.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAchievement.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAchievement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAchievement.Properties.ReadOnly = true;
            this.TxtTotalAchievement.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalAchievement.TabIndex = 77;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(471, 8);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(72, 14);
            this.label52.TabIndex = 78;
            this.label52.Text = "Total Bobot";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 34);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(766, 221);
            this.Grd9.TabIndex = 82;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            this.Grd9.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd9_AfterCommitEdit);
            this.Grd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd9_KeyDown);
            // 
            // TpResource
            // 
            this.TpResource.Appearance.Header.Options.UseTextOptions = true;
            this.TpResource.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpResource.Controls.Add(this.panel9);
            this.TpResource.Controls.Add(this.Grd6);
            this.TpResource.Name = "TpResource";
            this.TpResource.Size = new System.Drawing.Size(926, 255);
            this.TpResource.Text = "Resource";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.TxtTotalResource);
            this.panel9.Controls.Add(this.LblTotalResource);
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Controls.Add(this.TxtTotal);
            this.panel9.Controls.Add(this.label44);
            this.panel9.Controls.Add(this.TxtDirectCost);
            this.panel9.Controls.Add(this.label45);
            this.panel9.Controls.Add(this.TxtRemunerationCost);
            this.panel9.Controls.Add(this.label46);
            this.panel9.Controls.Add(this.BtnImportResource);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(926, 119);
            this.panel9.TabIndex = 70;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.LblPPh);
            this.panel10.Controls.Add(this.LblPPN);
            this.panel10.Controls.Add(this.TxtPPhAmt);
            this.panel10.Controls.Add(this.label31);
            this.panel10.Controls.Add(this.TxtContractAmt);
            this.panel10.Controls.Add(this.TxtNetCash);
            this.panel10.Controls.Add(this.label35);
            this.panel10.Controls.Add(this.TxtExclPPN);
            this.panel10.Controls.Add(this.label36);
            this.panel10.Controls.Add(this.TxtPPNAmt);
            this.panel10.Controls.Add(this.label37);
            this.panel10.Controls.Add(this.label38);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(302, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(326, 119);
            this.panel10.TabIndex = 80;
            // 
            // LblPPh
            // 
            this.LblPPh.AutoSize = true;
            this.LblPPh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPPh.ForeColor = System.Drawing.Color.Black;
            this.LblPPh.Location = new System.Drawing.Point(155, 75);
            this.LblPPh.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPPh.Name = "LblPPh";
            this.LblPPh.Size = new System.Drawing.Size(26, 14);
            this.LblPPh.TabIndex = 89;
            this.LblPPh.Text = "4%";
            this.LblPPh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPPN
            // 
            this.LblPPN.AutoSize = true;
            this.LblPPN.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPPN.ForeColor = System.Drawing.Color.Black;
            this.LblPPN.Location = new System.Drawing.Point(148, 33);
            this.LblPPN.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPPN.Name = "LblPPN";
            this.LblPPN.Size = new System.Drawing.Size(33, 14);
            this.LblPPN.TabIndex = 84;
            this.LblPPN.Text = "10%";
            this.LblPPN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPPhAmt
            // 
            this.TxtPPhAmt.EnterMoveNextControl = true;
            this.TxtPPhAmt.Location = new System.Drawing.Point(186, 72);
            this.TxtPPhAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPhAmt.Name = "TxtPPhAmt";
            this.TxtPPhAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPhAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPhAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPhAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPPhAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPhAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPhAmt.Properties.MaxLength = 30;
            this.TxtPPhAmt.Properties.ReadOnly = true;
            this.TxtPPhAmt.Size = new System.Drawing.Size(131, 20);
            this.TxtPPhAmt.TabIndex = 40;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(79, 12);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(102, 14);
            this.label31.TabIndex = 81;
            this.label31.Text = "Contract Amount";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtContractAmt
            // 
            this.TxtContractAmt.EnterMoveNextControl = true;
            this.TxtContractAmt.Location = new System.Drawing.Point(186, 9);
            this.TxtContractAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmt.Name = "TxtContractAmt";
            this.TxtContractAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmt.Properties.MaxLength = 30;
            this.TxtContractAmt.Properties.ReadOnly = true;
            this.TxtContractAmt.Size = new System.Drawing.Size(131, 20);
            this.TxtContractAmt.TabIndex = 82;
            // 
            // TxtNetCash
            // 
            this.TxtNetCash.EnterMoveNextControl = true;
            this.TxtNetCash.Location = new System.Drawing.Point(186, 93);
            this.TxtNetCash.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNetCash.Name = "TxtNetCash";
            this.TxtNetCash.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNetCash.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetCash.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNetCash.Properties.Appearance.Options.UseFont = true;
            this.TxtNetCash.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNetCash.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNetCash.Properties.MaxLength = 30;
            this.TxtNetCash.Properties.ReadOnly = true;
            this.TxtNetCash.Size = new System.Drawing.Size(131, 20);
            this.TxtNetCash.TabIndex = 42;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(32, 96);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(149, 14);
            this.label35.TabIndex = 41;
            this.label35.Text = "NET Cash (Excl PPN+PPh)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtExclPPN
            // 
            this.TxtExclPPN.EnterMoveNextControl = true;
            this.TxtExclPPN.Location = new System.Drawing.Point(186, 51);
            this.TxtExclPPN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExclPPN.Name = "TxtExclPPN";
            this.TxtExclPPN.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtExclPPN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExclPPN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExclPPN.Properties.Appearance.Options.UseFont = true;
            this.TxtExclPPN.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtExclPPN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtExclPPN.Properties.MaxLength = 30;
            this.TxtExclPPN.Properties.ReadOnly = true;
            this.TxtExclPPN.Size = new System.Drawing.Size(131, 20);
            this.TxtExclPPN.TabIndex = 87;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(128, 75);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 14);
            this.label36.TabIndex = 88;
            this.label36.Text = "PPh";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPPNAmt
            // 
            this.TxtPPNAmt.EnterMoveNextControl = true;
            this.TxtPPNAmt.Location = new System.Drawing.Point(186, 30);
            this.TxtPPNAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPNAmt.Name = "TxtPPNAmt";
            this.TxtPPNAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPNAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPNAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPNAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPPNAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPNAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPNAmt.Properties.MaxLength = 30;
            this.TxtPPNAmt.Properties.ReadOnly = true;
            this.TxtPPNAmt.Size = new System.Drawing.Size(131, 20);
            this.TxtPPNAmt.TabIndex = 85;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(6, 55);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(175, 14);
            this.label37.TabIndex = 86;
            this.label37.Text = "Production Amount(Excl. PPN)";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(122, 33);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 14);
            this.label38.TabIndex = 83;
            this.label38.Text = "PPN";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalResource
            // 
            this.TxtTotalResource.EnterMoveNextControl = true;
            this.TxtTotalResource.Location = new System.Drawing.Point(137, 27);
            this.TxtTotalResource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalResource.Name = "TxtTotalResource";
            this.TxtTotalResource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalResource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalResource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalResource.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalResource.Properties.ReadOnly = true;
            this.TxtTotalResource.Size = new System.Drawing.Size(133, 20);
            this.TxtTotalResource.TabIndex = 73;
            // 
            // LblTotalResource
            // 
            this.LblTotalResource.AutoSize = true;
            this.LblTotalResource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalResource.ForeColor = System.Drawing.Color.Black;
            this.LblTotalResource.Location = new System.Drawing.Point(35, 30);
            this.LblTotalResource.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalResource.Name = "LblTotalResource";
            this.LblTotalResource.Size = new System.Drawing.Size(89, 14);
            this.LblTotalResource.TabIndex = 72;
            this.LblTotalResource.Text = "Total Resource";
            this.LblTotalResource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.label39);
            this.panel12.Controls.Add(this.label40);
            this.panel12.Controls.Add(this.label41);
            this.panel12.Controls.Add(this.TxtPPNPPhPercentage);
            this.panel12.Controls.Add(this.label42);
            this.panel12.Controls.Add(this.TxtPPNPercentage);
            this.panel12.Controls.Add(this.label43);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(628, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(298, 119);
            this.panel12.TabIndex = 43;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(270, 50);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 14);
            this.label39.TabIndex = 50;
            this.label39.Text = "%";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(270, 28);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(19, 14);
            this.label40.TabIndex = 47;
            this.label40.Text = "%";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(3, 9);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(116, 14);
            this.label41.TabIndex = 44;
            this.label41.Text = "Cost Percentage :";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtPPNPPhPercentage
            // 
            this.TxtPPNPPhPercentage.EnterMoveNextControl = true;
            this.TxtPPNPPhPercentage.Location = new System.Drawing.Point(134, 25);
            this.TxtPPNPPhPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPNPPhPercentage.Name = "TxtPPNPPhPercentage";
            this.TxtPPNPPhPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPNPPhPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPNPPhPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPNPPhPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPPNPPhPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPNPPhPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPNPPhPercentage.Properties.MaxLength = 30;
            this.TxtPPNPPhPercentage.Properties.ReadOnly = true;
            this.TxtPPNPPhPercentage.Size = new System.Drawing.Size(131, 20);
            this.TxtPPNPPhPercentage.TabIndex = 46;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(44, 50);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(75, 14);
            this.label42.TabIndex = 48;
            this.label42.Text = "Exclude PPN";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPPNPercentage
            // 
            this.TxtPPNPercentage.EnterMoveNextControl = true;
            this.TxtPPNPercentage.Location = new System.Drawing.Point(134, 47);
            this.TxtPPNPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPPNPercentage.Name = "TxtPPNPercentage";
            this.TxtPPNPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPPNPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPPNPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPPNPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPPNPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPPNPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPPNPercentage.Properties.MaxLength = 30;
            this.TxtPPNPercentage.Properties.ReadOnly = true;
            this.TxtPPNPercentage.Size = new System.Drawing.Size(131, 20);
            this.TxtPPNPercentage.TabIndex = 49;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(15, 30);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(104, 14);
            this.label43.TabIndex = 45;
            this.label43.Text = "Exclude PPN+PPh";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(137, 90);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.MaxLength = 30;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(133, 20);
            this.TxtTotal.TabIndex = 79;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(89, 92);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 14);
            this.label44.TabIndex = 78;
            this.label44.Text = "Total";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectCost
            // 
            this.TxtDirectCost.EnterMoveNextControl = true;
            this.TxtDirectCost.Location = new System.Drawing.Point(137, 69);
            this.TxtDirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectCost.Name = "TxtDirectCost";
            this.TxtDirectCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectCost.Properties.MaxLength = 30;
            this.TxtDirectCost.Properties.ReadOnly = true;
            this.TxtDirectCost.Size = new System.Drawing.Size(133, 20);
            this.TxtDirectCost.TabIndex = 77;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(57, 70);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 14);
            this.label45.TabIndex = 76;
            this.label45.Text = "Direct Cost";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemunerationCost
            // 
            this.TxtRemunerationCost.EnterMoveNextControl = true;
            this.TxtRemunerationCost.Location = new System.Drawing.Point(137, 48);
            this.TxtRemunerationCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemunerationCost.Name = "TxtRemunerationCost";
            this.TxtRemunerationCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemunerationCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemunerationCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseFont = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemunerationCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemunerationCost.Properties.MaxLength = 30;
            this.TxtRemunerationCost.Properties.ReadOnly = true;
            this.TxtRemunerationCost.Size = new System.Drawing.Size(133, 20);
            this.TxtRemunerationCost.TabIndex = 75;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(13, 49);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(111, 14);
            this.label46.TabIndex = 74;
            this.label46.Text = "Remuneration Cost";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnImportResource
            // 
            this.BtnImportResource.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImportResource.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImportResource.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImportResource.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImportResource.Appearance.Options.UseBackColor = true;
            this.BtnImportResource.Appearance.Options.UseFont = true;
            this.BtnImportResource.Appearance.Options.UseForeColor = true;
            this.BtnImportResource.Appearance.Options.UseTextOptions = true;
            this.BtnImportResource.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImportResource.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImportResource.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImportResource.Location = new System.Drawing.Point(1, 5);
            this.BtnImportResource.Name = "BtnImportResource";
            this.BtnImportResource.Size = new System.Drawing.Size(86, 21);
            this.BtnImportResource.TabIndex = 71;
            this.BtnImportResource.Text = "Import Data";
            this.BtnImportResource.ToolTip = "Import Data";
            this.BtnImportResource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImportResource.ToolTipTitle = "Run System";
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 112);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(926, 143);
            this.Grd6.TabIndex = 51;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // TpIssue
            // 
            this.TpIssue.Appearance.Header.Options.UseTextOptions = true;
            this.TpIssue.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpIssue.Controls.Add(this.Grd7);
            this.TpIssue.Name = "TpIssue";
            this.TpIssue.Size = new System.Drawing.Size(766, 255);
            this.TpIssue.Text = "Issue";
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(766, 255);
            this.Grd7.TabIndex = 70;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // TpDocument
            // 
            this.TpDocument.Appearance.Header.Options.UseTextOptions = true;
            this.TpDocument.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpDocument.Controls.Add(this.DteDocDt2);
            this.TpDocument.Controls.Add(this.Grd8);
            this.TpDocument.Name = "TpDocument";
            this.TpDocument.Size = new System.Drawing.Size(766, 255);
            this.TpDocument.Text = "Document";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(82, 23);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(138, 20);
            this.DteDocDt2.TabIndex = 71;
            this.DteDocDt2.Visible = false;
            this.DteDocDt2.Leave += new System.EventHandler(this.DteDocDt2_Leave);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(766, 255);
            this.Grd8.TabIndex = 70;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd8_EllipsisButtonClick);
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // TpApproval
            // 
            this.TpApproval.Controls.Add(this.Grd10);
            this.TpApproval.Name = "TpApproval";
            this.TpApproval.Size = new System.Drawing.Size(766, 255);
            this.TpApproval.Text = "Approval Information";
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(0, 0);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(766, 255);
            this.Grd10.TabIndex = 71;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(550, 6);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(133, 20);
            this.textEdit1.TabIndex = 77;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(445, 8);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(102, 14);
            this.label47.TabIndex = 76;
            this.label47.Text = "Total Percentage";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(303, 6);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(133, 20);
            this.textEdit2.TabIndex = 75;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(13, 8);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(65, 14);
            this.label48.TabIndex = 72;
            this.label48.Text = "Total Price";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(82, 6);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit3.Properties.ReadOnly = true;
            this.textEdit3.Size = new System.Drawing.Size(133, 20);
            this.textEdit3.TabIndex = 73;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(227, 8);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 14);
            this.label49.TabIndex = 74;
            this.label49.Text = "Total Bobot";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmProjectImplementationRevision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 701);
            this.Name = "FrmProjectImplementationRevision";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectImplementationDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomerContactperson.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCMeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCLueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCDteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SOCDteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcOutgoingPayment)).EndInit();
            this.TcOutgoingPayment.ResumeLayout(false);
            this.TpBOQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpWBS.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPersentase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpWBS2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanEndDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePlanStartDt2.Properties)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPrice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPersentase2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBobot2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAchievement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            this.TpResource.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPhAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExclPPN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPPhPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPPNPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpIssue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpDocument.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.TpApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtCOAAmt;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo2;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraTab.XtraTabPage TpAdditional;
        private DevExpress.XtraTab.XtraTabControl TcOutgoingPayment;
        private DevExpress.XtraTab.XtraTabPage TpBOQ;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage TpItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        internal DevExpress.XtraEditors.TextEdit TxtSAName;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.LookUpEdit SOCLueStatus;
        internal DevExpress.XtraEditors.TextEdit TxtAddress;
        internal DevExpress.XtraEditors.TextEdit TxtCity;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtCountry;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.DateEdit SOCDteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueShpMCode;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        internal DevExpress.XtraEditors.TextEdit TxtFax;
        public DevExpress.XtraEditors.SimpleButton BtnProjectImplementationDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit SOCMeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit SOCChkCancelInd;
        public DevExpress.XtraEditors.TextEdit TxtCustomerContactperson;
        private DevExpress.XtraEditors.MemoExEdit MeeProjectDesc;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtProjectImplementationDocNo;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private DevExpress.XtraTab.XtraTabPage TpWBS;
        private DevExpress.XtraTab.XtraTabPage TpResource;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private DevExpress.XtraTab.XtraTabPage TpIssue;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private DevExpress.XtraTab.XtraTabPage TpDocument;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        internal DevExpress.XtraEditors.TextEdit TxtSOCDocNo;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        private DevExpress.XtraEditors.LookUpEdit LueStageCode;
        private DevExpress.XtraEditors.LookUpEdit LueTaskCode;
        internal DevExpress.XtraEditors.DateEdit DtePlanStartDt;
        internal DevExpress.XtraEditors.DateEdit DtePlanEndDt;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel9;
        protected internal DevExpress.XtraEditors.SimpleButton BtnImportWBS;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPrice;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBobot;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPersentase;
        private System.Windows.Forms.Label label34;
        protected System.Windows.Forms.Panel panel11;
        protected System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtPPhAmt;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmt;
        internal DevExpress.XtraEditors.TextEdit TxtNetCash;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtExclPPN;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtPPNAmt;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtTotalResource;
        private System.Windows.Forms.Label LblTotalResource;
        protected System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtPPNPPhPercentage;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtPPNPercentage;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtDirectCost;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtRemunerationCost;
        private System.Windows.Forms.Label label46;
        public DevExpress.XtraEditors.SimpleButton BtnImportResource;
        private DevExpress.XtraTab.XtraTabPage TpWBS2;
        internal DevExpress.XtraEditors.DateEdit DtePlanEndDt2;
        internal DevExpress.XtraEditors.DateEdit DtePlanStartDt2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        protected System.Windows.Forms.Panel panel13;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label49;
        protected System.Windows.Forms.Panel panel14;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPersentase2;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBobot2;
        private System.Windows.Forms.Label label51;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAchievement;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private DevExpress.XtraEditors.MemoExEdit MeeApprovalRemark;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPrice2;
        private DevExpress.XtraEditors.LookUpEdit LueStageCode2;
        private DevExpress.XtraEditors.LookUpEdit LueTaskCode2;
        private System.Windows.Forms.Label LblPPN;
        private System.Windows.Forms.Label LblPPh;
        private DevExpress.XtraTab.XtraTabPage TpApproval;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
    }
}