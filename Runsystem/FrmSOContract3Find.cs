﻿#region Update
/*
    26/01/2023 [DITA/MNET] New Apps
    29/03/2023 [RDA/MNET] tambah field start date pada menu SO Contract (include perubahan find dan printout)
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmSOContract3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract3Find(FrmSOContract3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("A.LocalDocNo, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approve' End As StatusDesc, A.CancelInd, ");
            SQL.AppendLine("A.SAAddress, B.CtName, A.BOQDocNo, A.Amt AmtBefTax, A.Amt2 AmtAfterTax, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine(", A.ProjectCode2, A4.SiteName,  ");
            SQL.AppendLine("G.ProjectName, A2.OptDesc AS ProjectScope,  ");
            SQL.AppendLine("A3.OptDesc AS ProjectResource, ");
            SQL.AppendLine("A1.OptDesc AS ProjectType, H.CCName, A.StartDt, A.EndDt, I.UserCode PICSales, A.TotalResource, A.ProfitMarginPerc ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join tblCustomer B on A.CtCode = B.CtCode ");
            
            SQL.AppendLine("Inner Join TblBOQHdr F On A.BOQDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblLOphdr G On F.LOPDocNo=G.DocNO ");
            SQL.AppendLine("Left Join TblCostCenter H On G.CCCode = H.CCCode ");
            SQL.AppendLine("Left Join TblUser I On G.PICCode = I.UserCode ");
            SQL.AppendLine("Left JOIN tbloption A1 ON G.ProjectType = A1.OptCode AND A1.OptCat = 'ProjectType' ");
            SQL.AppendLine("Left JOIN tbloption A2 ON G.ProjectScope = A2.OptCode AND A2.OptCat = 'ProjectScope' ");
            SQL.AppendLine("Left JOIN tbloption A3 ON G.ProjectResource = A3.OptCode AND A3.OptCat = 'ProjectResource' ");
            if (mFrmParent.mIsFilterBySite)
                SQL.AppendLine("Inner  JOIN tblsite A4 ON G.SiteCode = A4.SiteCode ");
            else
                SQL.AppendLine("Left  JOIN tblsite A4 ON G.SiteCode = A4.SiteCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Cancel",
                        "Start Date",
                        
                        
                        //6-10
                        "End Date",
                        "Local#",
                        "Customer",
                        "Bill Of Quantity",
                        "Shipping"+Environment.NewLine+"Address",
                        

                        //11-15
                        "PIC Sales",
                        "Project Code",
                        "Cost Center",
                        "Project Name",
                        "Scope of "+Environment.NewLine+"Work",
                        
                        
                        //16-20
                        "Type",
                        "Resource",
                        "Contract Amount",
                        "Total Resource",
                        "Profit Margin"+Environment.NewLine+"(%)",

                        //21-25
                        "Created By",
                        "Created Date", 
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",

                        //26
                        "Last Updated Time",


                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 60, 80,  
                        
                        //6-10
                        80, 100, 200, 130, 200, 

                        //11-15
                        130, 100, 150, 150, 150, 

                        //16-20
                        150, 150, 150, 150, 150, 

                        //21-25
                        150, 150, 150, 150, 150, 

                        //26
                        150,

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 19, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 5, 6, 22, 25});
            Sm.GrdFormatTime(Grd1, new int[] { 23, 26 });
            Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24, 25, 26}, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);Sm.FilterStr(ref Filter, ref cm, TxtBOQDocNo.Text, new string[] { "A.BOQDocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "StartDt", "EndDt",
                            
                            //6-10
                            "LocalDocNo", "CtName", "BOQDocNo", "SAAddress", "PICSales", 

                            //11-15
                            "ProjectCode2", "CCName", "ProjectName", "ProjectScope","ProjectType", 

                            //16-20
                            "ProjectResource", "AmtBefTax", "TotalResource", "ProfitMarginPerc", "CreateBy", 
                            
                            //21-23
                            "CreateDt", "LastUpBy", "LastUpDt", 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 23);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);

        }
        #endregion

        private void ChkBOQDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "BOQ#");
        }

        private void TxtBOQDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
    }
}
