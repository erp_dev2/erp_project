﻿#region Update
/*
    19/11/2021 [MYA/SIER] New Apps
    24/11/2021 [MYA/SIER] FEEDBACK : Penyesuaian menu BOQ pada Detail (0103990104) Material Request SPPJB saat di loop
    04/02/2021 [VIN/SIER] BUG : hrs nya tidak bisa save jika uom dan currency kosong -> ngaruh di main form
    07/04/2022 [TRI/SIER] BUG belum compute remaining budget ketika di main form tidak ada action klik di grid, langsung disave
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Imaging;

using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest5Dlg7 : RunSystem.FrmBase17
    {
        #region Field
        bool fAccept;
        iGCell fCell;
        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mMainCurCode = string.Empty,
            mItemCode = string.Empty;
        private int mRow = 0;
        private FrmMaterialRequest5 mFrmParent;
        private bool mIsInsert;

        #endregion

        #region Constructor

        public FrmMaterialRequest5Dlg7(FrmMaterialRequest5 FrmParent, int r, bool IsInsert)
        {
            InitializeComponent();
            mRow = r;
            mFrmParent = FrmParent;
            mIsInsert = IsInsert;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();

                Sl.SetLueUomCode(ref LueUOMCode1);
                SetLueCurCode(ref LueCurCode1);
                Tc1.SelectedTabPage = B;
                Sl.SetLueUomCode(ref LueUOMCode2);
                SetLueCurCode(ref LueCurCode2);
                Tc1.SelectedTabPage = C;
                Sl.SetLueUomCode(ref LueUOMCode3);
                SetLueCurCode(ref LueCurCode3);
                Tc1.SelectedTabPage = D;
                Sl.SetLueUomCode(ref LueUOMCode4);
                SetLueCurCode(ref LueCurCode4);
                Tc1.SelectedTabPage = E;
                Sl.SetLueUomCode(ref LueUOMCode5);
                SetLueCurCode(ref LueCurCode5);
                Tc1.SelectedTabPage = F;
                Sl.SetLueUomCode(ref LueUOMCode6);
                SetLueCurCode(ref LueCurCode6);
                Tc1.SelectedTabPage = G;
                Sl.SetLueUomCode(ref LueUOMCode7);
                SetLueCurCode(ref LueCurCode7);
                Tc1.SelectedTabPage = H;
                Sl.SetLueUomCode(ref LueUOMCode8);
                SetLueCurCode(ref LueCurCode8);
                Tc1.SelectedTabPage = I;
                Sl.SetLueUomCode(ref LueUOMCode9);
                SetLueCurCode(ref LueCurCode9);
                Tc1.SelectedTabPage = J;
                Sl.SetLueUomCode(ref LueUOMCode10);
                SetLueCurCode(ref LueCurCode10);
                Tc1.SelectedTabPage = K;
                Sl.SetLueUomCode(ref LueUOMCode11);
                SetLueCurCode(ref LueCurCode11);
                Tc1.SelectedTabPage = L;
                Sl.SetLueUomCode(ref LueUOMCode12);
                SetLueCurCode(ref LueCurCode12);
                Tc1.SelectedTabPage = M;
                Sl.SetLueUomCode(ref LueUOMCode13);
                SetLueCurCode(ref LueCurCode13);
                Tc1.SelectedTabPage = N;
                Sl.SetLueUomCode(ref LueUOMCode14);
                SetLueCurCode(ref LueCurCode14);
                Tc1.SelectedTabPage = O;
                Sl.SetLueUomCode(ref LueUOMCode15);
                SetLueCurCode(ref LueCurCode15);
                Tc1.SelectedTabPage = P;
                Sl.SetLueUomCode(ref LueUOMCode16);
                SetLueCurCode(ref LueCurCode16);
                Tc1.SelectedTabPage = Q;
                Sl.SetLueUomCode(ref LueUOMCode17);
                SetLueCurCode(ref LueCurCode17);
                Tc1.SelectedTabPage = R;
                Sl.SetLueUomCode(ref LueUOMCode18);
                SetLueCurCode(ref LueCurCode18);
                Tc1.SelectedTabPage = S;
                Sl.SetLueUomCode(ref LueUOMCode19);
                SetLueCurCode(ref LueCurCode19);
                Tc1.SelectedTabPage = T;
                Sl.SetLueUomCode(ref LueUOMCode20);
                SetLueCurCode(ref LueCurCode20);
                Tc1.SelectedTabPage = U;
                Sl.SetLueUomCode(ref LueUOMCode21);
                SetLueCurCode(ref LueCurCode21);
                Tc1.SelectedTabPage = V;
                Sl.SetLueUomCode(ref LueUOMCode22);
                SetLueCurCode(ref LueCurCode22);
                Tc1.SelectedTabPage = W;
                Sl.SetLueUomCode(ref LueUOMCode23);
                SetLueCurCode(ref LueCurCode23);
                Tc1.SelectedTabPage = X;
                Sl.SetLueUomCode(ref LueUOMCode24);
                SetLueCurCode(ref LueCurCode24);
                Tc1.SelectedTabPage = Y;
                Sl.SetLueUomCode(ref LueUOMCode25);
                SetLueCurCode(ref LueCurCode25);
                Tc1.SelectedTabPage = Z;
                Sl.SetLueUomCode(ref LueUOMCode26);
                SetLueCurCode(ref LueCurCode26);
                Tc1.SelectedTabPage = A;

                SetGrd();
                ShowData();
                if (!mIsInsert)
                {
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd9, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd10, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd21, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd22, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd23, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd24, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd25, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd26, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    BtnSave.Enabled = false;
                }

                // Lue Uom
                LueUOMCode1.Visible = false;
                LueUOMCode2.Visible = false;
                LueUOMCode3.Visible = false;
                LueUOMCode4.Visible = false;
                LueUOMCode5.Visible = false;
                LueUOMCode6.Visible = false;
                LueUOMCode7.Visible = false;
                LueUOMCode8.Visible = false;
                LueUOMCode9.Visible = false;
                LueUOMCode10.Visible = false;
                LueUOMCode11.Visible = false;
                LueUOMCode12.Visible = false;
                LueUOMCode13.Visible = false;
                LueUOMCode14.Visible = false;
                LueUOMCode15.Visible = false;
                LueUOMCode16.Visible = false;
                LueUOMCode17.Visible = false;
                LueUOMCode18.Visible = false;
                LueUOMCode19.Visible = false;
                LueUOMCode20.Visible = false;
                LueUOMCode21.Visible = false;
                LueUOMCode22.Visible = false;
                LueUOMCode23.Visible = false;
                LueUOMCode24.Visible = false;
                LueUOMCode25.Visible = false;
                LueUOMCode26.Visible = false;

                // Lue Curenccy
                LueCurCode1.Visible = false;
                LueCurCode2.Visible = false;
                LueCurCode3.Visible = false;
                LueCurCode4.Visible = false;
                LueCurCode5.Visible = false;
                LueCurCode6.Visible = false;
                LueCurCode7.Visible = false;
                LueCurCode8.Visible = false;
                LueCurCode9.Visible = false;
                LueCurCode10.Visible = false;
                LueCurCode11.Visible = false;
                LueCurCode12.Visible = false;
                LueCurCode13.Visible = false;
                LueCurCode14.Visible = false;
                LueCurCode15.Visible = false;
                LueCurCode16.Visible = false;
                LueCurCode17.Visible = false;
                LueCurCode18.Visible = false;
                LueCurCode19.Visible = false;
                LueCurCode20.Visible = false;
                LueCurCode21.Visible = false;
                LueCurCode22.Visible = false;
                LueCurCode23.Visible = false;
                LueCurCode24.Visible = false;
                LueCurCode25.Visible = false;
                LueCurCode26.Visible = false;

                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    TxtGrpName1, TxtGrpName2, TxtGrpName3, TxtGrpName4, TxtGrpName5,
                    TxtGrpName6, TxtGrpName7, TxtGrpName8, TxtGrpName9, TxtGrpName10,
                    TxtGrpName11, TxtGrpName12, TxtGrpName13, TxtGrpName14, TxtGrpName15,
                    TxtGrpName16, TxtGrpName17, TxtGrpName18, TxtGrpName19, TxtGrpName20,
                    TxtGrpName21, TxtGrpName22, TxtGrpName23, TxtGrpName24, TxtGrpName25,
                    TxtGrpName26
                }, false);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private void SetGrd()
        {
            SetGrdItem(new List<iGrid>
            {
                Grd1, Grd2, Grd3, Grd4, Grd5,
                Grd6, Grd7, Grd8, Grd9, Grd10,
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25,
                Grd26
            });
        }

        private void SetGrdItem(List<iGrid> ListofGrd)
        {
            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 10;
                Grd.FrozenArea.ColCount = 4;

                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[]
                        {
                         //0
                        "",

                        //1-5
                        "Job's Category Code",
                        "Category",
                        "Job's Name",
                        "Quantity",
                        "UoM",
                        
                        //6-9
                        "Currency",
                        "Price",
                        "Total Price",
                        "Remark"
                        },
                         new int[]
                        {
                        //0
                        20, 

                        //1-5
                        80, 200, 200, 80, 100,
                        
                        //6-8
                        150, 150, 150, 300
                        }
                    );

                Sm.GrdColButton(Grd, new int[] { 0 });
                Sm.GrdColReadOnly(true, true, Grd, new int[] { 1, 2, 8 });
                Sm.GrdFormatDec(Grd, new int[] { 4, 7, 8 }, 0);
                Sm.GrdColInvisible(Grd, new int[] { 1 }, false);

                //Grd.Rows.Add();
            });
        }

        private void ClearGrd(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 4, 7, 8 });
                Sm.FocusGrd(Grd, 0, 0);
            });
        }

        private void ShowData()
        {
            var CurCode = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 28);
            if (CurCode.Length == 0) CurCode = mMainCurCode;
            TxtItCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 8);
            TxtItName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 11);
            TxtLocation.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 40);
            TxtCurCode.EditValue = CurCode;
            TxtEstPrice.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 29), 0);
            ShowGrd(Grd1, "1", TxtGrpName1);
            ShowGrd(Grd2, "2", TxtGrpName2);
            ShowGrd(Grd3, "3", TxtGrpName3);
            ShowGrd(Grd4, "4", TxtGrpName4);
            ShowGrd(Grd5, "5", TxtGrpName5);
            ShowGrd(Grd6, "6", TxtGrpName6);
            ShowGrd(Grd7, "7", TxtGrpName7);
            ShowGrd(Grd8, "8", TxtGrpName8);
            ShowGrd(Grd9, "9", TxtGrpName9);
            ShowGrd(Grd10, "10", TxtGrpName10);
            ShowGrd(Grd11, "11", TxtGrpName11);
            ShowGrd(Grd12, "12", TxtGrpName12);
            ShowGrd(Grd13, "13", TxtGrpName13);
            ShowGrd(Grd14, "14", TxtGrpName14);
            ShowGrd(Grd15, "15", TxtGrpName15);
            ShowGrd(Grd16, "16", TxtGrpName16);
            ShowGrd(Grd17, "17", TxtGrpName17);
            ShowGrd(Grd18, "18", TxtGrpName18);
            ShowGrd(Grd19, "19", TxtGrpName19);
            ShowGrd(Grd20, "20", TxtGrpName20);
            ShowGrd(Grd21, "21", TxtGrpName21);
            ShowGrd(Grd22, "22", TxtGrpName22);
            ShowGrd(Grd23, "23", TxtGrpName23);
            ShowGrd(Grd24, "24", TxtGrpName24);
            ShowGrd(Grd25, "25", TxtGrpName25);
            ShowGrd(Grd26, "26", TxtGrpName26);
            ComputeEstPrice(Grd1, TxtSubTotal1);
            ComputeEstPrice(Grd2, TxtSubTotal2);
            ComputeEstPrice(Grd3, TxtSubTotal3);
            ComputeEstPrice(Grd4, TxtSubTotal4);
            ComputeEstPrice(Grd5, TxtSubTotal5);
            ComputeEstPrice(Grd6, TxtSubTotal6);
            ComputeEstPrice(Grd7, TxtSubTotal7);
            ComputeEstPrice(Grd8, TxtSubTotal8);
            ComputeEstPrice(Grd9, TxtSubTotal9);
            ComputeEstPrice(Grd10, TxtSubTotal10);
            ComputeEstPrice(Grd11, TxtSubTotal11);
            ComputeEstPrice(Grd12, TxtSubTotal12);
            ComputeEstPrice(Grd13, TxtSubTotal13);
            ComputeEstPrice(Grd14, TxtSubTotal14);
            ComputeEstPrice(Grd15, TxtSubTotal15);
            ComputeEstPrice(Grd16, TxtSubTotal16);
            ComputeEstPrice(Grd17, TxtSubTotal17);
            ComputeEstPrice(Grd18, TxtSubTotal18);
            ComputeEstPrice(Grd19, TxtSubTotal19);
            ComputeEstPrice(Grd20, TxtSubTotal20);
            ComputeEstPrice(Grd21, TxtSubTotal21);
            ComputeEstPrice(Grd22, TxtSubTotal22);
            ComputeEstPrice(Grd23, TxtSubTotal23);
            ComputeEstPrice(Grd24, TxtSubTotal24);
            ComputeEstPrice(Grd25, TxtSubTotal25);
            ComputeEstPrice(Grd26, TxtSubTotal26);
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (IsGrdValueNotValid(Grd1) || IsCurCodeInvalid(Grd1)) return;
            if (IsGrdValueNotValid(Grd2) || IsCurCodeInvalid(Grd2)) return;
            if (IsGrdValueNotValid(Grd3) || IsCurCodeInvalid(Grd3)) return;
            if (IsGrdValueNotValid(Grd4) || IsCurCodeInvalid(Grd4)) return;
            if (IsGrdValueNotValid(Grd5) || IsCurCodeInvalid(Grd5)) return;
            if (IsGrdValueNotValid(Grd6) || IsCurCodeInvalid(Grd6)) return;
            if (IsGrdValueNotValid(Grd7) || IsCurCodeInvalid(Grd7)) return;
            if (IsGrdValueNotValid(Grd8) || IsCurCodeInvalid(Grd8)) return;
            if (IsGrdValueNotValid(Grd9) || IsCurCodeInvalid(Grd9)) return;
            if (IsGrdValueNotValid(Grd10) || IsCurCodeInvalid(Grd10)) return;
            if (IsGrdValueNotValid(Grd11) || IsCurCodeInvalid(Grd11)) return;
            if (IsGrdValueNotValid(Grd12) || IsCurCodeInvalid(Grd12)) return;
            if (IsGrdValueNotValid(Grd13) || IsCurCodeInvalid(Grd13)) return;
            if (IsGrdValueNotValid(Grd14) || IsCurCodeInvalid(Grd14)) return;
            if (IsGrdValueNotValid(Grd15) || IsCurCodeInvalid(Grd15)) return;
            if (IsGrdValueNotValid(Grd16) || IsCurCodeInvalid(Grd16)) return;
            if (IsGrdValueNotValid(Grd17) || IsCurCodeInvalid(Grd17)) return;
            if (IsGrdValueNotValid(Grd18) || IsCurCodeInvalid(Grd18)) return;
            if (IsGrdValueNotValid(Grd19) || IsCurCodeInvalid(Grd19)) return;
            if (IsGrdValueNotValid(Grd20) || IsCurCodeInvalid(Grd20)) return;
            if (IsGrdValueNotValid(Grd21) || IsCurCodeInvalid(Grd21)) return;
            if (IsGrdValueNotValid(Grd22) || IsCurCodeInvalid(Grd22)) return;
            if (IsGrdValueNotValid(Grd23) || IsCurCodeInvalid(Grd23)) return;
            if (IsGrdValueNotValid(Grd24) || IsCurCodeInvalid(Grd24)) return;
            if (IsGrdValueNotValid(Grd25) || IsCurCodeInvalid(Grd25)) return;
            if (IsGrdValueNotValid(Grd26) || IsCurCodeInvalid(Grd26)) return;
            try
            {
                if (mFrmParent.mlJob2.Count > 0)
                {
                    mFrmParent.mlJob2.RemoveAll(x => Sm.CompareStr(x.ItCode, TxtItCode.Text));
                }
                SaveGrd(Grd1, "1", TxtGrpName1.Text);
                SaveGrd(Grd2, "2", TxtGrpName2.Text);
                SaveGrd(Grd3, "3", TxtGrpName3.Text);
                SaveGrd(Grd4, "4", TxtGrpName4.Text);
                SaveGrd(Grd5, "5", TxtGrpName5.Text);
                SaveGrd(Grd6, "6", TxtGrpName6.Text);
                SaveGrd(Grd7, "7", TxtGrpName7.Text);
                SaveGrd(Grd8, "8", TxtGrpName8.Text);
                SaveGrd(Grd9, "9", TxtGrpName9.Text);
                SaveGrd(Grd10, "10", TxtGrpName10.Text);
                SaveGrd(Grd11, "11", TxtGrpName11.Text);
                SaveGrd(Grd12, "12", TxtGrpName12.Text);
                SaveGrd(Grd13, "13", TxtGrpName13.Text);
                SaveGrd(Grd14, "14", TxtGrpName14.Text);
                SaveGrd(Grd15, "15", TxtGrpName15.Text);
                SaveGrd(Grd16, "16", TxtGrpName16.Text);
                SaveGrd(Grd17, "17", TxtGrpName17.Text);
                SaveGrd(Grd18, "18", TxtGrpName18.Text);
                SaveGrd(Grd19, "19", TxtGrpName19.Text);
                SaveGrd(Grd20, "20", TxtGrpName20.Text);
                SaveGrd(Grd21, "21", TxtGrpName21.Text);
                SaveGrd(Grd22, "22", TxtGrpName22.Text);
                SaveGrd(Grd23, "23", TxtGrpName23.Text);
                SaveGrd(Grd24, "24", TxtGrpName24.Text);
                SaveGrd(Grd25, "25", TxtGrpName25.Text);
                SaveGrd(Grd26, "26", TxtGrpName26.Text);
                mFrmParent.Grd1.Cells[mRow, 28].Value = TxtCurCode.Text;
                mFrmParent.Grd1.Cells[mRow, 29].Value = decimal.Parse(TxtEstPrice.Text);
                mFrmParent.ComputeTotalPrice(mRow);
                mFrmParent.ComputeTotalEstPrice();

                mFrmParent.ComputeRemainingBudget();
                mFrmParent.ComputeDR_Balance();

                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeEstPrice(iGrid Grd, DevExpress.XtraEditors.TextEdit SubTotal)
        {
            DevExpress.XtraEditors.TextEdit[] Total = new DevExpress.XtraEditors.TextEdit[26] { TxtSubTotal1, TxtSubTotal2, TxtSubTotal3, TxtSubTotal4, TxtSubTotal5,
                                                                                                TxtSubTotal6, TxtSubTotal7, TxtSubTotal8, TxtSubTotal9, TxtSubTotal10,
                                                                                                TxtSubTotal11, TxtSubTotal12, TxtSubTotal13, TxtSubTotal14, TxtSubTotal15,
                                                                                                TxtSubTotal16, TxtSubTotal17, TxtSubTotal18, TxtSubTotal19, TxtSubTotal20,
                                                                                                TxtSubTotal21, TxtSubTotal22, TxtSubTotal23, TxtSubTotal24, TxtSubTotal25,
                                                                                                TxtSubTotal26 };
            decimal TotalPrice = 0m;
            decimal TotalEstPrice = 0m;
            for (int r = 0; r < Grd.Rows.Count; r++)
            {

                if (Sm.GetGrdStr(Grd, r, 7).Length > 0)
                    Grd.Cells[r, 8].Value = Sm.FormatNum(Sm.GetGrdDec(Grd, r, 7) * Sm.GetGrdDec(Grd, r, 4), 0);

                TotalPrice += Sm.GetGrdDec(Grd, r, 7) * Sm.GetGrdDec(Grd, r, 4);
            }

            if (Sm.GetGrdStr(Grd, 0, 6).Length > 0) TxtCurCode.EditValue = Sm.GetGrdStr(Grd, 0, 6);

            SubTotal.EditValue = Sm.FormatNum(TotalPrice, 0);

            for (int i = 0; i < Total.Length; i++)
            {
                TotalEstPrice += Convert.ToDecimal(Sm.FormatNum(Total[i].Text, 0));
            }
            TxtEstPrice.EditValue = Sm.FormatNum(TotalEstPrice, 0);

        }

        private bool IsGrdValueNotValid(iGrid Grd)
        {
            if (Grd.Rows.Count > 1)
            {
                for (int r = 0; r < Grd.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd, r, 2, false, "Job Category is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, r, 3, false, "Job is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, r, 5, false, "UOM is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, r, 6, false, "Currency is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, r, 7, true, "Price should be bigger than 0.00.")) return true;
                }
            }
            return false;
        }

        private bool IsCurCodeInvalid(iGrid Grd)
        {
            var CurCode = Sm.GetGrdStr(Grd, 0, 6);
            if (Grd.Rows.Count > 1)
            {
                for (int r = 0; r < Grd.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd, r, 6).Length > 0)
                    {
                        if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd, r, 6)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Job's Category : " + Sm.GetGrdStr(Grd, r, 1) + Environment.NewLine +
                                "Job's Name : " + Sm.GetGrdStr(Grd, r, 3) + Environment.NewLine + Environment.NewLine +
                                "Currency is not valid.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        #endregion

        #region Grid Method

        #region Grid RequestEdit

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd1));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd1, LueUOMCode1, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd1, LueCurCode1, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd2));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd2, LueUOMCode2, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd2, LueCurCode2, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd2, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd3));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd3, LueUOMCode3, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd3, LueCurCode3, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd3, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd4));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd4, LueUOMCode4, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd4, LueCurCode4, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd4, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd5));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd5, LueUOMCode5, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd5, LueCurCode5, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd5, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd6));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd6, LueUOMCode6, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd6, LueCurCode6, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd6, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd7));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd7, LueUOMCode7, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd7, LueCurCode7, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd7, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd8));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd8, LueUOMCode8, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd8, LueCurCode8, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd8, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd8, Grd8.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd9));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd9, LueUOMCode9, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd9, LueCurCode9, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd9, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd9, Grd9.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd10_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd10));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd10, LueUOMCode10, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd10, LueCurCode10, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd10, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd10, Grd10.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd11));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd11, LueUOMCode11, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd11, LueCurCode11, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd11, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd11, Grd11.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd12));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd12, LueUOMCode12, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd12, LueCurCode12, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd12, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd12, Grd12.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd13));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd13, LueUOMCode13, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd13, LueCurCode13, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd13, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd13, Grd13.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd14));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd14, LueUOMCode14, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd14, LueCurCode14, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd14, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd14, Grd14.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd15));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd15, LueUOMCode15, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd15, LueCurCode15, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd15, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd15, Grd15.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd16));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd16, LueUOMCode16, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd16, LueCurCode16, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd16, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd16, Grd16.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd17_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd17));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd17, LueUOMCode17, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd17, LueCurCode17, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd17, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd17, Grd17.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd18_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd18));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd18, LueUOMCode18, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd18, LueCurCode18, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd18, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd18, Grd18.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd19_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd19));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd19, LueUOMCode19, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd19, LueCurCode19, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd19, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd19, Grd19.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd20_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd20));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd20, LueUOMCode20, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd20, LueCurCode20, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd20, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd20, Grd20.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd21_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd21));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd21, LueUOMCode21, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd21, LueCurCode21, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd21, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd21, Grd21.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd22_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd22));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd22, LueUOMCode22, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd22, LueCurCode22, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd22, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd22, Grd22.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd23_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd23));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd23, LueUOMCode23, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd23, LueCurCode23, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd23, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd23, Grd23.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd24_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd24));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd24, LueUOMCode24, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd24, LueCurCode24, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd24, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd24, Grd24.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd25_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd25));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd25, LueUOMCode25, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd25, LueCurCode25, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd25, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd25, Grd25.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        private void Grd26_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd26));
            }

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                if (e.ColIndex == 5) LueRequestEdit(Grd26, LueUOMCode26, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) LueRequestEdit(Grd26, LueCurCode26, ref fCell, ref fAccept, e);
            }
            Sm.GrdRequestEdit(Grd26, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd26, Grd26.Rows.Count - 1, new int[] { 4, 7, 8 });
        }

        #endregion

        #region Grid KeyDown

        private void Grd1KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeEstPrice(Grd1, TxtSubTotal1);
            Sm.GrdEnter(Grd1, e);
        }

        private void Grd2KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeEstPrice(Grd2, TxtSubTotal2);
            Sm.GrdEnter(Grd2, e);
        }

        private void Grd3KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeEstPrice(Grd3, TxtSubTotal3);
            Sm.GrdEnter(Grd3, e);
        }

        private void Grd4KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            ComputeEstPrice(Grd4, TxtSubTotal4);
            Sm.GrdEnter(Grd4, e);
        }

        private void Grd5KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            ComputeEstPrice(Grd5, TxtSubTotal5);
            Sm.GrdEnter(Grd5, e);
        }

        private void Grd6KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            ComputeEstPrice(Grd6, TxtSubTotal6);
            Sm.GrdEnter(Grd6, e);
        }

        private void Grd7KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
            ComputeEstPrice(Grd7, TxtSubTotal7);
            Sm.GrdEnter(Grd7, e);
        }

        private void Grd8KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
            ComputeEstPrice(Grd8, TxtSubTotal8);
            Sm.GrdEnter(Grd8, e);
        }

        private void Grd9KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd9, e, BtnSave);
            ComputeEstPrice(Grd9, TxtSubTotal9);
            Sm.GrdEnter(Grd9, e);
        }

        private void Grd10KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd10, e, BtnSave);
            ComputeEstPrice(Grd10, TxtSubTotal10);
            Sm.GrdEnter(Grd10, e);
        }

        private void Grd11KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
            ComputeEstPrice(Grd11, TxtSubTotal11);
            Sm.GrdEnter(Grd11, e);
        }

        private void Grd12KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
            ComputeEstPrice(Grd12, TxtSubTotal12);
            Sm.GrdEnter(Grd12, e);
        }

        private void Grd13KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd13, e, BtnSave);
            ComputeEstPrice(Grd13, TxtSubTotal13);
            Sm.GrdEnter(Grd13, e);
        }

        private void Grd14KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
            ComputeEstPrice(Grd14, TxtSubTotal14);
            Sm.GrdEnter(Grd14, e);
        }

        private void Grd15KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd15, e, BtnSave);
            ComputeEstPrice(Grd15, TxtSubTotal15);
            Sm.GrdEnter(Grd15, e);
        }

        private void Grd16KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
            ComputeEstPrice(Grd16, TxtSubTotal16);
            Sm.GrdEnter(Grd16, e);
        }

        private void Grd17KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd17, e, BtnSave);
            ComputeEstPrice(Grd17, TxtSubTotal17);
            Sm.GrdEnter(Grd17, e);
        }

        private void Grd18KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd18, e, BtnSave);
            ComputeEstPrice(Grd18, TxtSubTotal18);
            Sm.GrdEnter(Grd18, e);
        }

        private void Grd19KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd19, e, BtnSave);
            ComputeEstPrice(Grd19, TxtSubTotal19);
            Sm.GrdEnter(Grd19, e);
        }

        private void Grd20KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd20, e, BtnSave);
            ComputeEstPrice(Grd20, TxtSubTotal20);
            Sm.GrdEnter(Grd20, e);
        }

        private void Grd21KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd21, e, BtnSave);
            ComputeEstPrice(Grd21, TxtSubTotal21);
            Sm.GrdEnter(Grd21, e);
        }

        private void Grd22KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd22, e, BtnSave);
            ComputeEstPrice(Grd22, TxtSubTotal22);
            Sm.GrdEnter(Grd22, e);
        }

        private void Grd23KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd23, e, BtnSave);
            ComputeEstPrice(Grd23, TxtSubTotal23);
            Sm.GrdEnter(Grd23, e);
        }

        private void Grd24KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd24, e, BtnSave);
            ComputeEstPrice(Grd24, TxtSubTotal24);
            Sm.GrdEnter(Grd24, e);
        }

        private void Grd25KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd25, e, BtnSave);
            ComputeEstPrice(Grd25, TxtSubTotal25);
            Sm.GrdEnter(Grd25, e);
        }

        private void Grd26KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd26, e, BtnSave);
            ComputeEstPrice(Grd26, TxtSubTotal26);
            Sm.GrdEnter(Grd26, e);
        }

        #endregion

        #region Grid AfterCommitEdit

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd1, TxtSubTotal1);
            //Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 4 }, e);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd2, TxtSubTotal2);
            //Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 4 }, e);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd3, TxtSubTotal3);
            //Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 4 }, e);
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd4, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd4, TxtSubTotal4);
            //Sm.GrdAfterCommitEditTrimString(Grd4, new int[] { 4 }, e);
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd5, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd5, TxtSubTotal5);
            Sm.GrdAfterCommitEditTrimString(Grd5, new int[] { 4 }, e);
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd6, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd6, TxtSubTotal6);
           // Sm.GrdAfterCommitEditTrimString(Grd6, new int[] { 4 }, e);
        }

        private void Grd7_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd7, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd7, TxtSubTotal7);
           //Sm.GrdAfterCommitEditTrimString(Grd7, new int[] { 4 }, e);
        }

        private void Grd8_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd8, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd8, TxtSubTotal8);
            //Sm.GrdAfterCommitEditTrimString(Grd8, new int[] { 4 }, e);
        }

        private void Grd9_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd9, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd9, TxtSubTotal9);
            //Sm.GrdAfterCommitEditTrimString(Grd9, new int[] { 4 }, e);
        }

        private void Grd10_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd10, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd10, TxtSubTotal10);
            //Sm.GrdAfterCommitEditTrimString(Grd10, new int[] { 4 }, e);
        }

        private void Grd11_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd11, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd11, TxtSubTotal11);
            //Sm.GrdAfterCommitEditTrimString(Grd11, new int[] { 4 }, e);
        }

        private void Grd12_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd12, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd12, TxtSubTotal12);
            //Sm.GrdAfterCommitEditTrimString(Grd12, new int[] { 4 }, e);
        }

        private void Grd13_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd13, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd13, TxtSubTotal13);
            //Sm.GrdAfterCommitEditTrimString(Grd13, new int[] { 4 }, e);
        }

        private void Grd14_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd14, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd14, TxtSubTotal14);
            //Sm.GrdAfterCommitEditTrimString(Grd14, new int[] { 4 }, e);
        }

        private void Grd15_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd15, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd15, TxtSubTotal15);
            //Sm.GrdAfterCommitEditTrimString(Grd15, new int[] { 4 }, e);
        }

        private void Grd16_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd16, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd16, TxtSubTotal16);
            //Sm.GrdAfterCommitEditTrimString(Grd16, new int[] { 4 }, e);
        }

        private void Grd17_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd17, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd17, TxtSubTotal17);
            //Sm.GrdAfterCommitEditTrimString(Grd17, new int[] { 4 }, e);
        }

        private void Grd18_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd18, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd18, TxtSubTotal18);
            //Sm.GrdAfterCommitEditTrimString(Grd18, new int[] { 4 }, e);
        }

        private void Grd19_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd19, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd19, TxtSubTotal19);
            //Sm.GrdAfterCommitEditTrimString(Grd19, new int[] { 4 }, e);
        }

        private void Grd20_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd20, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd20, TxtSubTotal20);
           // Sm.GrdAfterCommitEditTrimString(Grd20, new int[] { 4 }, e);
        }

        private void Grd21_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd21, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd21, TxtSubTotal21);
            //Sm.GrdAfterCommitEditTrimString(Grd21, new int[] { 4 }, e);
        }

        private void Grd22_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd22, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd22, TxtSubTotal22);
            //Sm.GrdAfterCommitEditTrimString(Grd22, new int[] { 4 }, e);
        }

        private void Grd23_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd23, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd23, TxtSubTotal23);
            //Sm.GrdAfterCommitEditTrimString(Grd23, new int[] { 4 }, e);
        }

        private void Grd24_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd24, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd24, TxtSubTotal24);
            //Sm.GrdAfterCommitEditTrimString(Grd24, new int[] { 4 }, e);
        }

        private void Grd25_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd25, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd25, TxtSubTotal25);
            //Sm.GrdAfterCommitEditTrimString(Grd25, new int[] { 4 }, e);
        }

        private void Grd26_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd26, new int[] { 7 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 4) ComputeEstPrice(Grd26, TxtSubTotal26);
            //Sm.GrdAfterCommitEditTrimString(Grd26, new int[] { 4 }, e);
        }

        #endregion

        #region Grid EllipsisButtonClick

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd1));
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd2));
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd3));
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd4));
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd5));
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd6));
        }

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd7));
        }

        private void Grd8_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd8));
        }

        private void Grd9_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd9));
        }

        private void Grd10_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd10));
        }

        private void Grd11_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd11));
        }

        private void Grd12_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd12));
        }

        private void Grd13_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd13));
        }

        private void Grd14_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd14));
        }

        private void Grd15_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd15));
        }

        private void Grd16_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd16));
        }

        private void Grd17_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd17));
        }

        private void Grd18_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd18));
        }

        private void Grd19_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd19));
        }

        private void Grd20_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd20));
        }

        private void Grd21_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd21));
        }

        private void Grd22_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd22));
        }

        private void Grd23_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd23));
        }

        private void Grd24_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd24));
        }

        private void Grd25_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd25));
        }

        private void Grd26_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequest5Dlg8(this, Grd26));
        }


        #endregion

        #endregion

        #region Additional Method

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select CurCode As Col1 From tblCurrency ", "Currency");
        }

        private void SaveGrd(iGrid Grd, String GrpType, String GrpName)
        {
            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 2).Length > 0)
                {
                    mFrmParent.mlJob2.Add(new FrmMaterialRequest5.Job2()
                    {
                        ItCode = TxtItCode.Text,
                        DNo = Sm.Right("00" + (r + 1).ToString(), 3),
                        GrpType = GrpType,
                        GrpName = GrpName,
                        LocName = TxtLocation.Text,
                        JobCtCode = Sm.GetGrdStr(Grd, r, 1),
                        JobCtName = Sm.GetGrdStr(Grd, r, 2),
                        JobName = Sm.GetGrdStr(Grd, r, 3),
                        Qty = Sm.GetGrdDec(Grd, r, 4),
                        UomCode = Sm.GetGrdStr(Grd, r, 5),
                        CurCode = Sm.GetGrdStr(Grd, r, 6),
                        Price = Sm.GetGrdDec(Grd, r, 7),
                        Remark = Sm.GetGrdStr(Grd, r, 9)
                    });
                }
            }

            //foreach (var l in mFrmParent.mlJob2)
            //    Console.WriteLine(l.GrpType + " " + l.GrpName);
        }

        private void ShowGrd(iGrid Grd, String GrpType, DevExpress.XtraEditors.TextEdit TxtGrpName)
        {
            if (mFrmParent.mlJob2.Count > 0)
            {
                int r = 0;
                Grd.BeginUpdate();
                foreach (var x in mFrmParent.mlJob2.Where(w =>
                    Sm.CompareStr(w.ItCode, TxtItCode.Text) && Sm.CompareStr(w.GrpType, GrpType)))
                {
                    TxtGrpName.EditValue = x.GrpName;
                    Grd.Rows.Add();
                    Grd.Cells[r, 1].Value = x.JobCtCode;
                    Grd.Cells[r, 2].Value = x.JobCtName;
                    Grd.Cells[r, 3].Value = x.JobName;
                    Grd.Cells[r, 4].Value = x.Qty;
                    Grd.Cells[r, 5].Value = x.UomCode;
                    Grd.Cells[r, 6].Value = x.CurCode;
                    Grd.Cells[r, 7].Value = Sm.FormatNum(x.Price, 0);
                    Grd.Cells[r, 9].Value = x.Remark;
                    r++;
                }
            }
            Grd.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 4, 7, 8 });
            Grd.EndUpdate();
        }



        #endregion

        #endregion

        #region Event

        #region Event Lue

        #region LueRequestEdit

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #region LueEditvalue

        #region LueUomCode

        private void LueUOMCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode1, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode2, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode3, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode4, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode5, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode6, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode7, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode8, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode9, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode10, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode11, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode12, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode13, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode14, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode15, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode16_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode16, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode17_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode17, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode18_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode18, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode19_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode19, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode20_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode20, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode21_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode21, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode22_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode22, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode23_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode23, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode24_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode24, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode25_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode25, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUOMCode26_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUOMCode26, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        #endregion

        #region LueCurCode

        private void LueCurCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode1, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode2, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode3, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode4, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode5, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode6, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode7, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode8, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode9, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode10, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode11, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode12, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode13, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode14, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode15, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode16_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode16, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode17_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode17, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode18_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode18, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode19_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode19, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode20_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode20, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode21_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode21, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode22_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode22, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode23_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode23, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode24_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode24, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode25_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode25, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode26_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode26, new Sm.RefreshLue1(SetLueCurCode));
        }

        #endregion

        #endregion

        #region LueKey

        #region LueUomCode

        private void LueUOMCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueUOMCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueUOMCode3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueUOMCode4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueUOMCode5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueUOMCode6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueUOMCode7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void LueUOMCode8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void LueUOMCode9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void LueUOMCode10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void LueUOMCode11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void LueUOMCode12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void LueUOMCode13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void LueUOMCode14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void LueUOMCode15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void LueUOMCode16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueUOMCode17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void LueUOMCode18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void LueUOMCode19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void LueUOMCode20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void LueUOMCode21_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd21, ref fAccept, e);
        }

        private void LueUOMCode22_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd22, ref fAccept, e);
        }

        private void LueUOMCode23_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd23, ref fAccept, e);
        }

        private void LueUOMCode24_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd24, ref fAccept, e);
        }

        private void LueUOMCode25_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd25, ref fAccept, e);
        }

        private void LueUOMCode26_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd26, ref fAccept, e);
        }

        #endregion

        #region LueCurCode

        private void LueCurCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCurCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueCurCode3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueCurCode4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueCurCode5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueCurCode6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueCurCode7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void LueCurCode8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void LueCurCode9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void LueCurCode10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void LueCurCode11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void LueCurCode12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void LueCurCode13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void LueCurCode14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void LueCurCode15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void LueCurCode16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueCurCode17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void LueCurCode18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void LueCurCode19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void LueCurCode20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void LueCurCode21_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd21, ref fAccept, e);
        }

        private void LueCurCode22_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd22, ref fAccept, e);
        }

        private void LueCurCode23_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd23, ref fAccept, e);
        }

        private void LueCurCode24_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd24, ref fAccept, e);
        }

        private void LueCurCode25_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd25, ref fAccept, e);
        }

        private void LueCurCode26_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd26, ref fAccept, e);
        }

        #endregion

        #endregion

        #region LueValidated

        #region LueUomCode

        private void LueUOMCode1_Validated(object sender, EventArgs e)
        {
            LueUOMCode1.Visible = false;
        }

        private void LueUOMCode2_Validated(object sender, EventArgs e)
        {
            LueUOMCode2.Visible = false;
        }

        private void LueUOMCode3_Validated(object sender, EventArgs e)
        {
            LueUOMCode3.Visible = false;
        }

        private void LueUOMCode4_Validated(object sender, EventArgs e)
        {
            LueUOMCode4.Visible = false;
        }

        private void LueUOMCode5_Validated(object sender, EventArgs e)
        {
            LueUOMCode5.Visible = false;
        }

        private void LueUOMCode6_Validated(object sender, EventArgs e)
        {
            LueUOMCode6.Visible = false;
        }

        private void LueUOMCode7_Validated(object sender, EventArgs e)
        {
            LueUOMCode7.Visible = false;
        }

        private void LueUOMCode8_Validated(object sender, EventArgs e)
        {
            LueUOMCode8.Visible = false;
        }

        private void LueUOMCode9_Validated(object sender, EventArgs e)
        {
            LueUOMCode9.Visible = false;
        }

        private void LueUOMCode10_Validated(object sender, EventArgs e)
        {
            LueUOMCode10.Visible = false;
        }

        private void LueUOMCode11_Validated(object sender, EventArgs e)
        {
            LueUOMCode11.Visible = false;
        }

        private void LueUOMCode12_Validated(object sender, EventArgs e)
        {
            LueUOMCode12.Visible = false;
        }

        private void LueUOMCode13_Validated(object sender, EventArgs e)
        {
            LueUOMCode13.Visible = false;
        }

        private void LueUOMCode14_Validated(object sender, EventArgs e)
        {
            LueUOMCode14.Visible = false;
        }

        private void LueUOMCode15_Validated(object sender, EventArgs e)
        {
            LueUOMCode15.Visible = false;
        }

        private void LueUOMCode16_Validated(object sender, EventArgs e)
        {
            LueUOMCode16.Visible = false;
        }

        private void LueUOMCode17_Validated(object sender, EventArgs e)
        {
            LueUOMCode17.Visible = false;
        }

        private void LueUOMCode18_Validated(object sender, EventArgs e)
        {
            LueUOMCode18.Visible = false;
        }

        private void LueUOMCode19_Validated(object sender, EventArgs e)
        {
            LueUOMCode19.Visible = false;
        }

        private void LueUOMCode20_Validated(object sender, EventArgs e)
        {
            LueUOMCode20.Visible = false;
        }

        private void LueUOMCode21_Validated(object sender, EventArgs e)
        {
            LueUOMCode21.Visible = false;
        }

        private void LueUOMCode22_Validated(object sender, EventArgs e)
        {
            LueUOMCode22.Visible = false;
        }

        private void LueUOMCode23_Validated(object sender, EventArgs e)
        {
            LueUOMCode23.Visible = false;
        }

        private void LueUOMCode24_Validated(object sender, EventArgs e)
        {
            LueUOMCode24.Visible = false;
        }

        private void LueUOMCode25_Validated(object sender, EventArgs e)
        {
            LueUOMCode25.Visible = false;
        }

        private void LueUOMCode26_Validated(object sender, EventArgs e)
        {
            LueUOMCode6.Visible = false;
        }

        #endregion

        #region LueCurCode

        private void LueCurCode1_Validated(object sender, EventArgs e)
        {
            LueCurCode1.Visible = false;
        }

        private void LueCurCode2_Validated(object sender, EventArgs e)
        {
            LueCurCode2.Visible = false;
        }

        private void LueCurCode3_Validated(object sender, EventArgs e)
        {
            LueCurCode3.Visible = false;
        }

        private void LueCurCode4_Validated(object sender, EventArgs e)
        {
            LueCurCode4.Visible = false;
        }

        private void LueCurCode5_Validated(object sender, EventArgs e)
        {
            LueCurCode5.Visible = false;
        }

        private void LueCurCode6_Validated(object sender, EventArgs e)
        {
            LueCurCode6.Visible = false;
        }

        private void LueCurCode7_Validated(object sender, EventArgs e)
        {
            LueCurCode7.Visible = false;
        }

        private void LueCurCode8_Validated(object sender, EventArgs e)
        {
            LueCurCode8.Visible = false;
        }

        private void LueCurCode9_Validated(object sender, EventArgs e)
        {
            LueCurCode9.Visible = false;
        }

        private void LueCurCode10_Validated(object sender, EventArgs e)
        {
            LueCurCode10.Visible = false;
        }

        private void LueCurCode11_Validated(object sender, EventArgs e)
        {
            LueCurCode11.Visible = false;
        }

        private void LueCurCode12_Validated(object sender, EventArgs e)
        {
            LueCurCode12.Visible = false;
        }

        private void LueCurCode13_Validated(object sender, EventArgs e)
        {
            LueCurCode13.Visible = false;
        }

        private void LueCurCode14_Validated(object sender, EventArgs e)
        {
            LueCurCode14.Visible = false;
        }

        private void LueCurCode15_Validated(object sender, EventArgs e)
        {
            LueCurCode15.Visible = false;
        }

        private void LueCurCode16_Validated(object sender, EventArgs e)
        {
            LueCurCode16.Visible = false;
        }

        private void LueCurCode17_Validated(object sender, EventArgs e)
        {
            LueCurCode17.Visible = false;
        }

        private void LueCurCode18_Validated(object sender, EventArgs e)
        {
            LueCurCode18.Visible = false;
        }

        private void LueCurCode19_Validated(object sender, EventArgs e)
        {
            LueCurCode19.Visible = false;
        }

        private void LueCurCode20_Validated(object sender, EventArgs e)
        {
            LueCurCode20.Visible = false;
        }

        private void LueCurCode21_Validated(object sender, EventArgs e)
        {
            LueCurCode21.Visible = false;
        }

        private void LueCurCode22_Validated(object sender, EventArgs e)
        {
            LueCurCode22.Visible = false;
        }

        private void LueCurCode23_Validated(object sender, EventArgs e)
        {
            LueCurCode23.Visible = false;
        }

        private void LueCurCode24_Validated(object sender, EventArgs e)
        {
            LueCurCode24.Visible = false;
        }

        private void LueCurCode25_Validated(object sender, EventArgs e)
        {
            LueCurCode25.Visible = false;
        }

        private void LueCurCode26_Validated(object sender, EventArgs e)
        {
            LueCurCode6.Visible = false;
        }

        #endregion

        #endregion

        #region LueLeave

        #region LueUomCode
        private void LueUOMCode1_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode1.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode1.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode1).Trim();
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueUOMCode1.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode2_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode2.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode2.Text.Trim().Length == 0)
                    Grd2.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode2).Trim();
                    Grd2.Cells[fCell.RowIndex, 5].Value = LueUOMCode2.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode3_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode3.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode3.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode3).Trim();
                    Grd3.Cells[fCell.RowIndex, 5].Value = LueUOMCode3.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode4_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode4.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode4.Text.Trim().Length == 0)
                    Grd4.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode4).Trim();
                    Grd4.Cells[fCell.RowIndex, 5].Value = LueUOMCode4.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode5_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode5.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode5.Text.Trim().Length == 0)
                    Grd5.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode5).Trim();
                    Grd5.Cells[fCell.RowIndex, 5].Value = LueUOMCode5.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode6_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode6.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode6.Text.Trim().Length == 0)
                    Grd6.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode6).Trim();
                    Grd6.Cells[fCell.RowIndex, 5].Value = LueUOMCode6.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode7_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode7.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode7.Text.Trim().Length == 0)
                    Grd7.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode7).Trim();
                    Grd7.Cells[fCell.RowIndex, 5].Value = LueUOMCode7.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode8_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode8.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode8.Text.Trim().Length == 0)
                    Grd8.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode8).Trim();
                    Grd8.Cells[fCell.RowIndex, 5].Value = LueUOMCode8.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode9_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode9.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode9.Text.Trim().Length == 0)
                    Grd9.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode9).Trim();
                    Grd9.Cells[fCell.RowIndex, 5].Value = LueUOMCode9.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode10_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode10.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode10.Text.Trim().Length == 0)
                    Grd10.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode10).Trim();
                    Grd10.Cells[fCell.RowIndex, 5].Value = LueUOMCode10.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode11_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode11.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode11.Text.Trim().Length == 0)
                    Grd11.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode11).Trim();
                    Grd11.Cells[fCell.RowIndex, 5].Value = LueUOMCode11.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode12_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode12.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode12.Text.Trim().Length == 0)
                    Grd12.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode12).Trim();
                    Grd12.Cells[fCell.RowIndex, 5].Value = LueUOMCode12.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode13_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode13.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode13.Text.Trim().Length == 0)
                    Grd13.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode13).Trim();
                    Grd13.Cells[fCell.RowIndex, 5].Value = LueUOMCode13.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode14_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode14.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode14.Text.Trim().Length == 0)
                    Grd14.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode14).Trim();
                    Grd14.Cells[fCell.RowIndex, 5].Value = LueUOMCode14.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode15_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode15.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode15.Text.Trim().Length == 0)
                    Grd15.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode15).Trim();
                    Grd15.Cells[fCell.RowIndex, 5].Value = LueUOMCode15.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode16_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode16.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode16.Text.Trim().Length == 0)
                    Grd16.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode16).Trim();
                    Grd16.Cells[fCell.RowIndex, 5].Value = LueUOMCode16.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode17_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode17.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode17.Text.Trim().Length == 0)
                    Grd17.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode17).Trim();
                    Grd17.Cells[fCell.RowIndex, 5].Value = LueUOMCode17.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode18_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode18.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode18.Text.Trim().Length == 0)
                    Grd18.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode18).Trim();
                    Grd18.Cells[fCell.RowIndex, 5].Value = LueUOMCode18.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode19_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode19.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode19.Text.Trim().Length == 0)
                    Grd19.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode19).Trim();
                    Grd19.Cells[fCell.RowIndex, 5].Value = LueUOMCode19.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode20_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode20.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode20.Text.Trim().Length == 0)
                    Grd20.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode20).Trim();
                    Grd20.Cells[fCell.RowIndex, 5].Value = LueUOMCode20.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode21_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode21.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode21.Text.Trim().Length == 0)
                    Grd21.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd21.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode21).Trim();
                    Grd21.Cells[fCell.RowIndex, 5].Value = LueUOMCode21.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode22_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode22.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode22.Text.Trim().Length == 0)
                    Grd22.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd22.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode22).Trim();
                    Grd22.Cells[fCell.RowIndex, 5].Value = LueUOMCode22.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode23_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode23.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode23.Text.Trim().Length == 0)
                    Grd23.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd23.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode23).Trim();
                    Grd23.Cells[fCell.RowIndex, 5].Value = LueUOMCode23.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode24_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode24.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode24.Text.Trim().Length == 0)
                    Grd24.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd24.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode24).Trim();
                    Grd24.Cells[fCell.RowIndex, 5].Value = LueUOMCode24.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode25_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode25.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode25.Text.Trim().Length == 0)
                    Grd25.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd25.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode25).Trim();
                    Grd25.Cells[fCell.RowIndex, 5].Value = LueUOMCode25.GetColumnValue("Col2");
                }
            }
        }

        private void LueUOMCode26_Leave(object sender, EventArgs e)
        {
            if (LueUOMCode26.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (LueUOMCode26.Text.Trim().Length == 0)
                    Grd26.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd26.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueUOMCode26).Trim();
                    Grd26.Cells[fCell.RowIndex, 5].Value = LueUOMCode26.GetColumnValue("Col2");
                }
            }
        }


        #endregion

        #region LueCurCode
        private void LueCurCode1_Leave(object sender, EventArgs e)
        {
            if (LueCurCode1.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode1.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode1).Trim();
                    Grd1.Cells[fCell.RowIndex, 6].Value = LueCurCode1.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode2_Leave(object sender, EventArgs e)
        {
            if (LueCurCode2.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode2.Text.Trim().Length == 0)
                    Grd2.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode2).Trim();
                    Grd2.Cells[fCell.RowIndex, 6].Value = LueCurCode2.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode3_Leave(object sender, EventArgs e)
        {
            if (LueCurCode3.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode3.Text.Trim().Length == 0)
                    Grd3.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode3).Trim();
                    Grd3.Cells[fCell.RowIndex, 6].Value = LueCurCode3.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode4_Leave(object sender, EventArgs e)
        {
            if (LueCurCode4.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode4.Text.Trim().Length == 0)
                    Grd4.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode4).Trim();
                    Grd4.Cells[fCell.RowIndex, 6].Value = LueCurCode4.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode5_Leave(object sender, EventArgs e)
        {
            if (LueCurCode5.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode5.Text.Trim().Length == 0)
                    Grd5.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode5).Trim();
                    Grd5.Cells[fCell.RowIndex, 6].Value = LueCurCode5.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode6_Leave(object sender, EventArgs e)
        {
            if (LueCurCode6.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode6.Text.Trim().Length == 0)
                    Grd6.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode6).Trim();
                    Grd6.Cells[fCell.RowIndex, 6].Value = LueCurCode6.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode7_Leave(object sender, EventArgs e)
        {
            if (LueCurCode7.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode7.Text.Trim().Length == 0)
                    Grd7.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode7).Trim();
                    Grd7.Cells[fCell.RowIndex, 6].Value = LueCurCode7.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode8_Leave(object sender, EventArgs e)
        {
            if (LueCurCode8.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode8.Text.Trim().Length == 0)
                    Grd8.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode8).Trim();
                    Grd8.Cells[fCell.RowIndex, 6].Value = LueCurCode8.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode9_Leave(object sender, EventArgs e)
        {
            if (LueCurCode9.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode9.Text.Trim().Length == 0)
                    Grd9.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode9).Trim();
                    Grd9.Cells[fCell.RowIndex, 6].Value = LueCurCode9.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode10_Leave(object sender, EventArgs e)
        {
            if (LueCurCode10.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode10.Text.Trim().Length == 0)
                    Grd10.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode10).Trim();
                    Grd10.Cells[fCell.RowIndex, 6].Value = LueCurCode10.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode11_Leave(object sender, EventArgs e)
        {
            if (LueCurCode11.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode11.Text.Trim().Length == 0)
                    Grd11.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode11).Trim();
                    Grd11.Cells[fCell.RowIndex, 6].Value = LueCurCode11.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode12_Leave(object sender, EventArgs e)
        {
            if (LueCurCode12.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode12.Text.Trim().Length == 0)
                    Grd12.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode12).Trim();
                    Grd12.Cells[fCell.RowIndex, 6].Value = LueCurCode12.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode13_Leave(object sender, EventArgs e)
        {
            if (LueCurCode13.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode13.Text.Trim().Length == 0)
                    Grd13.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode13).Trim();
                    Grd13.Cells[fCell.RowIndex, 6].Value = LueCurCode13.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode14_Leave(object sender, EventArgs e)
        {
            if (LueCurCode14.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode14.Text.Trim().Length == 0)
                    Grd14.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode14).Trim();
                    Grd14.Cells[fCell.RowIndex, 6].Value = LueCurCode14.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode15_Leave(object sender, EventArgs e)
        {
            if (LueCurCode15.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode15.Text.Trim().Length == 0)
                    Grd15.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode15).Trim();
                    Grd15.Cells[fCell.RowIndex, 6].Value = LueCurCode15.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode16_Leave(object sender, EventArgs e)
        {
            if (LueCurCode16.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode16.Text.Trim().Length == 0)
                    Grd16.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode16).Trim();
                    Grd16.Cells[fCell.RowIndex, 6].Value = LueCurCode16.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode17_Leave(object sender, EventArgs e)
        {
            if (LueCurCode17.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode17.Text.Trim().Length == 0)
                    Grd17.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode17).Trim();
                    Grd17.Cells[fCell.RowIndex, 6].Value = LueCurCode17.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode18_Leave(object sender, EventArgs e)
        {
            if (LueCurCode18.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode18.Text.Trim().Length == 0)
                    Grd18.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode18).Trim();
                    Grd18.Cells[fCell.RowIndex, 6].Value = LueCurCode18.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode19_Leave(object sender, EventArgs e)
        {
            if (LueCurCode19.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode19.Text.Trim().Length == 0)
                    Grd19.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode19).Trim();
                    Grd19.Cells[fCell.RowIndex, 6].Value = LueCurCode19.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode20_Leave(object sender, EventArgs e)
        {
            if (LueCurCode20.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode20.Text.Trim().Length == 0)
                    Grd20.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode20).Trim();
                    Grd20.Cells[fCell.RowIndex, 6].Value = LueCurCode20.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode21_Leave(object sender, EventArgs e)
        {
            if (LueCurCode21.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode21.Text.Trim().Length == 0)
                    Grd21.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd21.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode21).Trim();
                    Grd21.Cells[fCell.RowIndex, 6].Value = LueCurCode21.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode22_Leave(object sender, EventArgs e)
        {
            if (LueCurCode22.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode22.Text.Trim().Length == 0)
                    Grd22.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd22.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode22).Trim();
                    Grd22.Cells[fCell.RowIndex, 6].Value = LueCurCode22.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode23_Leave(object sender, EventArgs e)
        {
            if (LueCurCode23.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode23.Text.Trim().Length == 0)
                    Grd23.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd23.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode23).Trim();
                    Grd23.Cells[fCell.RowIndex, 6].Value = LueCurCode23.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode24_Leave(object sender, EventArgs e)
        {
            if (LueCurCode24.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode24.Text.Trim().Length == 0)
                    Grd24.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd24.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode24).Trim();
                    Grd24.Cells[fCell.RowIndex, 6].Value = LueCurCode24.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode25_Leave(object sender, EventArgs e)
        {
            if (LueCurCode25.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode25.Text.Trim().Length == 0)
                    Grd25.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd25.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode25).Trim();
                    Grd25.Cells[fCell.RowIndex, 6].Value = LueCurCode25.GetColumnValue("Col1");
                }
            }
        }

        private void LueCurCode26_Leave(object sender, EventArgs e)
        {
            if (LueCurCode26.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueCurCode26.Text.Trim().Length == 0)
                    Grd26.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd26.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode26).Trim();
                    Grd26.Cells[fCell.RowIndex, 6].Value = LueCurCode26.GetColumnValue("Col1");
                }
            }
        }


        #endregion

        #endregion

        #endregion

        #endregion
    }
}
