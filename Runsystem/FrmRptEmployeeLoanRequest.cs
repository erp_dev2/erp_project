﻿#region Update
/*
    16/02/2022 [RDA/PHT] New Apps
    14/03/2022 [RDA/PHT] Penyesuaian requirement reporting employee loan requests
    22/03/2022 [RDA/PHT] Perubahan rumus untuk kolom OverDue dan DuePerYr
    08/04/2022 [RDA/PHT] Penyesuaian rumus reporting employee loan requests
    20/04/2022 [RDA/PHT] Penyesuaian rumus ketika if else didalam perhitungan kolom overdue
    03/08/2022 [MAU/PHT] Reporting hanya menampilkan data sesuai dengan satker sesuai dengan validasi 
    13/03/2023 [SET/PHT] Add CoA Account, Voucher, Voucher Date

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmployeeLoanRequest : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        private bool
            mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmRptEmployeeLoanRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueCreditCode(ref LueCreditCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine(" 'IsFilterBySite' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;

             
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Reporting Employee's Loan Request */ ");

            SQL.AppendLine("SELECT   ");
            SQL.AppendLine("X1.DocNo, X1.DocDt, X1.SiteCode, X1.DeptCode, X1.CreditCode, X1.SiteName, X1.EmpCode, X1.EmpCodeOld, X1.EmpName, X1.CreditName, X1.Top,  ");
            SQL.AppendLine("X1.StartMth, X1.EndMth, X1.TotalAmt, X1.InterestRateAmt, X1.InstallmentPerMth, X1.Paid, X1.Outstanding, ");
            SQL.AppendLine("if(X1.Outstanding = 0, 0,  ");
            SQL.AppendLine("	if(X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid > X1.TotalAmt, X1.TotalAmt - X1.Paid,  ");
            SQL.AppendLine("		-- X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("		 if(X1.EndMth < @DocDt, X1.TotalAmt - X1.Paid, ");
            SQL.AppendLine("		 X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("		 ) ");
            SQL.AppendLine("	) ");
            SQL.AppendLine(") AS OverDue, ");
            SQL.AppendLine("if(X1.EndDateDiff > 12, X1.InstallmentPerMth * 12, ");
            SQL.AppendLine("X1.TotalAmt- X1.Paid - if(X1.Outstanding = 0, 0,  ");
            SQL.AppendLine("								if(X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid > X1.TotalAmt, X1.TotalAmt - X1.Paid,  ");
            SQL.AppendLine("									-- X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("									 if(X1.EndMth < @DocDt, X1.TotalAmt - X1.Paid, ");
            SQL.AppendLine("									 X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("									 ) ");
            SQL.AppendLine("							 	) ");
            SQL.AppendLine("							  ) ");
            SQL.AppendLine(") AS DuePerYr, ");
            SQL.AppendLine("(if(X1.Outstanding = 0, 0,  ");
            SQL.AppendLine("	if(X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid > X1.TotalAmt, X1.TotalAmt - X1.Paid,  ");
            SQL.AppendLine("		-- X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("		 if(X1.EndMth < @DocDt, X1.TotalAmt - X1.Paid, ");
            SQL.AppendLine("		 X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("		 ) ");
            SQL.AppendLine("	) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("+ ");
            SQL.AppendLine("if(X1.EndDateDiff > 12, X1.InstallmentPerMth * 12, ");
            SQL.AppendLine("X1.TotalAmt - X1.Paid - if(X1.Outstanding = 0, 0,  ");
            SQL.AppendLine("									if(X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid > X1.TotalAmt, X1.TotalAmt - X1.Paid,  ");
            SQL.AppendLine("										-- X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("										 if(X1.EndMth < @DocDt, X1.TotalAmt - X1.Paid, ");
            SQL.AppendLine("										 X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("										 ) ");
            SQL.AppendLine("								 	) ");
            SQL.AppendLine("								 ) ");
            SQL.AppendLine(")) AS ShortTermRecv, ");
            SQL.AppendLine(" ");
            SQL.AppendLine("(X1.Outstanding - (if(X1.Outstanding = 0, 0,  ");
            SQL.AppendLine("							if(X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid > X1.TotalAmt, X1.TotalAmt - X1.Paid,  ");
            SQL.AppendLine("								-- X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("								 if(X1.EndMth < @DocDt, X1.TotalAmt - X1.Paid, ");
            SQL.AppendLine("								 X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("								 ) ");
            SQL.AppendLine("							) ");
            SQL.AppendLine("						) ");
            SQL.AppendLine("						+ ");
            SQL.AppendLine("						if(X1.EndDateDiff > 12, X1.InstallmentPerMth * 12, ");
            SQL.AppendLine("						X1.TotalAmt - X1.Paid - if(X1.Outstanding = 0, 0,  ");
            SQL.AppendLine("															if(X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid > X1.TotalAmt, X1.TotalAmt - X1.Paid,  ");
            SQL.AppendLine("																-- X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("																 if(X1.EndMth < @DocDt, X1.TotalAmt - X1.Paid, ");
            SQL.AppendLine("																 X1.StartDateDiff * X1.InstallmentPerMth - X1.Paid ");
            SQL.AppendLine("																 ) ");
            SQL.AppendLine("														 	) ");
            SQL.AppendLine("														 ) ");
            SQL.AppendLine("						)) ");
            SQL.AppendLine(") AS LongTermRecv, CONCAT(X3.AcNo, ' - ', X3.AcDesc) COA, X5.DocNo VcDocNo, X5.DocDt VcDocDt ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("	SELECT Tbl.DocNo, Tbl.DocDt, Tbl.SiteCode, Tbl.DeptCode, Tbl.CreditCode, Tbl.SiteName, Tbl.EmpCode, Tbl.EmpCodeOld, Tbl.EmpName, Tbl.CreditName, Tbl.Top,  ");
            SQL.AppendLine("	CONCAT(Tbl.Yr,Tbl.StartMth,'01') AS StartMth,   ");
            SQL.AppendLine("	DATE_FORMAT(DATE_ADD(CONCAT(Tbl.Yr,Tbl.StartMth,'01'), INTERVAL Tbl.Top MONTH), '%Y%m%d') AS EndMth,  ");
            SQL.AppendLine("	ifnull(Tbl.Amt, 0) + ifnull(Tbl.InterestRateAmt, 0) AS TotalAmt,   ");
            SQL.AppendLine("	ifnull(Tbl.InterestRateAmt, 0) AS InterestRateAmt,   ");
            SQL.AppendLine("	IFNULL(Tbl.InstallmentPerMth, 0) AS InstallmentPerMth,   ");
            SQL.AppendLine("	(ifnull(Tbl.Amt,0) + ifnull(Tbl.InterestRateAmt,0)) - ifnull(Tbl.RemainingLoan,0) AS Paid,  ");
            SQL.AppendLine("	(ifnull(Tbl.Amt, 0) + ifnull(Tbl.InterestRateAmt, 0)) - ((ifnull(Tbl.Amt,0) + ifnull(Tbl.InterestRateAmt,0)) - ifnull(Tbl.RemainingLoan,0)) AS Outstanding, ");
            SQL.AppendLine("	IFNULL(ROUND((DATEDIFF(@DocDt,CONCAT(Tbl.Yr,Tbl.StartMth,'01'))/30),0),0) AS StartDateDiff, ");
            SQL.AppendLine("	IFNULL(ROUND((DATEDIFF(DATE_FORMAT(DATE_ADD(CONCAT(Tbl.Yr,Tbl.StartMth,'01'), INTERVAL Tbl.Top MONTH), '%Y%m%d'),@DocDt)/30),0),0) AS EndDateDiff, ");
            SQL.AppendLine("	Tbl.VRDocNo ");
            SQL.AppendLine("	FROM (   ");
            SQL.AppendLine("		Select A.DocNo, A.DocDt, A.CancelInd, B.SiteCode, B.DeptCode, A.VoucherRequestDocNo VRDocNo,  ");
            SQL.AppendLine("		A.EmpCode, B.EmpName, B.EmpCodeOld, C.SiteName, A.CreditCode, D.CreditName,  ");
            SQL.AppendLine("		A.Amt, A.InterestRate, A.InterestRateAmt, A.Top, A.StartMth, A.Yr,  ");
            SQL.AppendLine("		A.Amt+A.InterestRateAmt-IfNull(E.Amt , 0.00) As RemainingLoan, (SELECT Amt FROM tbladvancepaymentdtl WHERE DocNo = A.DocNo LIMIT 1) AS InstallmentPerMth ");
            SQL.AppendLine("		From TblAdvancePaymentHdr A   ");
            SQL.AppendLine("		Inner Join TblEmployee B On A.EmpCode=B.EmpCode    ");
            SQL.AppendLine("		LEFT JOIN TblSite C On B.SiteCode=C.SiteCode   ");
            SQL.AppendLine("		LEFT Join TblCredit D On A.CreditCode=D.CreditCode   ");
            SQL.AppendLine("		Left Join(   ");
            SQL.AppendLine("			Select T.DocNo, Sum(T.Amt) As Amt   ");
            SQL.AppendLine("			From TblAdvancePaymentProcess T   ");
            SQL.AppendLine("			Where Exists(   ");
            SQL.AppendLine("				Select 1   ");
            SQL.AppendLine("				From TblVoucherRequestPayrollHdr T1   ");
            SQL.AppendLine("				Inner Join TblVoucherRequestPayrollDtl2 T2 On T1.DocNo=T2.DocNo   ");
            SQL.AppendLine("				Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N'   ");
            SQL.AppendLine("				Where T1.CancelInd='N' And T1.Status='A' And T2.PayrunCode=T.PayrunCode   ");
            SQL.AppendLine("			)   ");
            SQL.AppendLine("			Group By DocNo  ");
            SQL.AppendLine("		) E On A.DocNo = E.DocNo  ");
            SQL.AppendLine("		WHERE A.CancelInd = 'N'  ");


            if (mIsFilterBySite) 
            {
                SQL.AppendLine("		And Exists( ");
                SQL.AppendLine(" Select 1 From tblgroupsite U ");
                SQL.AppendLine(" WHERE C.SiteCode = U.SiteCode ");
                SQL.AppendLine(" And GrpCode In(SELECT V.GrpCode From tbluser V WHERE V.UserCode = @UserCode) ");
                SQL.AppendLine(" ) ");

            }



            SQL.AppendLine("	) Tbl   ");
            SQL.AppendLine(")X1 ");
            SQL.AppendLine("Left Join TblCredit X2 ON X1.CreditCode=X2.CreditCode ");
            SQL.AppendLine("Left Join TblCOA X3 ON X2.AcNo=X3.AcNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr X4 ON X1.VRDocNo=X4.DocNo ");
            SQL.AppendLine("Left Join TblVoucherHdr X5 ON X4.VoucherDocNo=X5.DocNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Site",
                        "Employee Code",
                        "Old Code",
                        
                        //6-10
                        "Employee Name",
                        "Credit",
                        "Term of"+Environment.NewLine+"Payment",
                        "Start Month",
                        "End Month",
                        
                        //11-15
                        "Total Amount",
                        "Interest Amount",
                        "Installment/Month",
                        "Paid/Settled",
                        "Outstanding Balance",
                        
                        //16-20
                        "Over Due",
                        "Due in 1 Year",
                        "Short-term Receivable",
                        "Long-term Receivable",
                        "COA Account",

                        //21-22
                        "Voucher",
                        "Voucher Date"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 150, 200, 100, 130, 
                        
                        //6-10
                        200, 200, 80, 100, 100,

                        //11-15
                        200, 200, 200, 200, 200,

                        //16-20
                        200, 200, 200, 200, 250,

                        //21-22
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 10, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, false);
            Sm.SetGrdProperty(Grd1, false);

            Grd1.Cols[20].Move(8);
            Grd1.Cols[21].Move(10);
            Grd1.Cols[22].Move(11);
            for(int Col = 0; Col < Grd1.Cols.Count; Col++)
                Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "WHERE X2.InternalInd = 'Y' AND X1.StartMth <= @DocDt ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);

                string test = Sm.GetDte(DteDocDt);

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "X1.EmpCode", "X1.EmpCodeOld", "X1.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X1.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "X1.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCreditCode), "X1.CreditCode", true);
                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By X1.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "SiteName", "EmpCode", "EmpCodeOld", "EmpName",

                            //6-10
                            "CreditName", "Top", "StartMth", "EndMth", "TotalAmt",

                            //11-15
                            "InterestRateAmt", "InstallmentPerMth", "Paid", "Outstanding", "OverDue",

                            //16-20
                            "DuePerYr", "ShortTermRecv", "LongTermRecv", "COA", "VcDocNo",

                            //21
                            "VcDocDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                        }, true, false, false, false
                    );


                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 15, 16, 17, 18, 19 });

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            //Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private static void SetLueSiteCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SiteCode As Col1, SiteName As Col2 From TblSite Where ActInd='Y' Order By SiteName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCreditCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCreditCode, new Sm.RefreshLue1(Sl.SetLueCreditCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkCreditCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Credit");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Date");
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        #endregion
    }
}
