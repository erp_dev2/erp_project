﻿#region Update
/*
    29/09/2019 [TKG/GSS] New Reporting
    30/09/2019 [TKG/GSS] bulan yg lebih besar dari bulan sekarang tidak dihitung.
    02/10/2019 [TKG/GSS] perubahan perhitungan
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptFinancialNotUseStartFromFilter, menonaktifkan start from.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss2 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mAccountingRptStartFrom = string.Empty,
            mDocTitle = string.Empty;

        private bool
            mIsEntityMandatory = false,
            mIsRptProfitLossUseFilterPeriod = false,
            mIsJournalCostCenterEnabled = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptFinancialNotUseStartFromFilter = false;

        #endregion

        #region Constructor

        public FrmRptProfitLoss2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                if (mIsRptFinancialNotUseStartFromFilter)
                    Sm.SetControlReadOnly(LueStartFrom, true);
                else
                {
                    if (mAccountingRptStartFrom.Length > 0)
                    {
                        Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                        Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                    }
                    else
                    {
                        Sl.SetLueYr(LueStartFrom, string.Empty);
                        LueStartFrom.EditValue = LueYr.EditValue;
                    }
                }
                SetLueEntCode(ref LueEntCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mIsRptProfitLossUseFilterPeriod = Sm.GetParameterBoo("IsRptProfitLossUseFilterPeriod");
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsJournalCostCenterEnabled = Sm.GetParameterBoo("IsJournalCostCenterEnabled");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 300;
            Grd1.Header.Cells[0, 1].Value = "Account Name";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[0, 2].Value = "Balance";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            for (int i = 1; i <= 12; i++) SetGrd(i);

            for (int c = 0; c<Grd1.Cols.Count; c++)
                Grd1.Cols[c].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            for (int c = 2; c<Grd1.Cols.Count; c++)
                Grd1.Cols[c].Width = 140;
            Sm.GrdFormatDec(Grd1, new int[] { 
                2, 3, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26
                }, 2);
        }

        private void SetGrd(int m)
        {
            int c = (m * 2) + 1;
            Grd1.Header.Cells[1, c].Value = Sm.GetMonthName(m);
            Grd1.Header.Cells[1, c].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, c].SpanCols = 2;
            Grd1.Header.Cells[0, c].Value = "Debit";
            Grd1.Header.Cells[0, c+1].Value = "Credit";
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") ||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity"))
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string
                Yr = Sm.GetLue(LueYr),
                StartFrom = Sm.GetLue(LueStartFrom),
                EntCode = Sm.GetLue(LueEntCode);
            int LatestMth = 0;

            var lCOA = new List<COA>();
            var lCOAEntity = new List<COAEntity>();
            var lJournal2 = new List<Journal2>();
            bool IsFilterByEnt = false;

            string CurrentDateTime = Sm.ServerCurrentDateTime();
            string YrTemp = Sm.Left(CurrentDateTime, 4);
            string MthTemp = CurrentDateTime.Substring(4, 2);

            if (Yr.CompareTo(YrTemp)<0)
                LatestMth = 12;
            else
            {
                if (Yr.CompareTo(YrTemp)==0) LatestMth = int.Parse(MthTemp);
            }
            
            try
            {
                if (mIsEntityMandatory && !Sm.CompareStr(EntCode, "Consolidate"))
                {
                    IsFilterByEnt = true;
                    SetCOAEntity(ref lCOAEntity, EntCode);
                }

                Process1(ref lCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom, IsFilterByEnt, EntCode);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lJournal2, Yr, StartFrom, IsFilterByEnt, EntCode);
                        else
                            Process3(ref lJournal2, Yr, StartFrom, IsFilterByEnt, EntCode);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr, IsFilterByEnt, EntCode);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lJournal2, Yr, string.Empty, IsFilterByEnt, EntCode);
                        else
                            Process3(ref lJournal2, Yr, string.Empty, IsFilterByEnt, EntCode);
                    }

                    if (lJournal2.Count > 0)
                    {
                        for (int m = 1; m <= 12; m++)
                        {
                            switch(m)
                            {
                                case 1: 
                                    if (m<=LatestMth) Process4_01(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 2:
                                    if (m <= LatestMth) Process4_02(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 3:
                                    if (m <= LatestMth) Process4_03(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 4:
                                    if (m <= LatestMth) Process4_04(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 5:
                                    if (m <= LatestMth) Process4_05(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 6:
                                    if (m <= LatestMth) Process4_06(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 7:
                                    if (m <= LatestMth) Process4_07(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 8:
                                    if (m <= LatestMth) Process4_08(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 9:
                                    if (m <= LatestMth) Process4_09(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 10:
                                    if (m <= LatestMth) Process4_10(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 11:
                                    if (m <= LatestMth) Process4_11(ref lCOA, ref lJournal2, Yr); 
                                    break;
                                case 12:
                                    if (m <= LatestMth) Process4_12(ref lCOA, ref lJournal2, Yr); 
                                    break;
                            }
                        }
                    }
                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    
                    #region Filter by Entity

                    if (IsFilterByEnt && lCOAEntity.Count > 0 && lCOA.Count > 0)
                    {
                        Grd1.BeginUpdate();
                        Grd1.Rows.Count = 0;
                        iGRow r;

                        r = Grd1.Rows.Add();
                        r.Level = 0;
                        r.TreeButton = iGTreeButtonState.Visible;
                        r.Cells[0].Value = "COA";

                        for (var j = 0; j < lCOAEntity.Count; j++)
                        {
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Sm.CompareStr(lCOA[i].AcNo, lCOAEntity[j].AcNo))
                                {
                                    r = Grd1.Rows.Add();
                                    r.Level = lCOA[i].Level;
                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                    r.Cells[0].Value = lCOA[i].AcNo;
                                    r.Cells[1].Value = lCOA[i].AcDesc;
                                    for (var c = 2; c < Grd1.Cols.Count; c++)
                                        r.Cells[c].Value = 0m;
                                    r.Cells[2].Value = lCOA[i].Balance;
                                    r.Cells[3].Value = lCOA[i].D01;
                                    r.Cells[4].Value = lCOA[i].C01;
                                    r.Cells[5].Value = lCOA[i].D02;
                                    r.Cells[6].Value = lCOA[i].C02;
                                    r.Cells[7].Value = lCOA[i].D03;
                                    r.Cells[8].Value = lCOA[i].C03;
                                    r.Cells[9].Value = lCOA[i].D04;
                                    r.Cells[10].Value = lCOA[i].C04;
                                    r.Cells[11].Value = lCOA[i].D05;
                                    r.Cells[12].Value = lCOA[i].C05;
                                    r.Cells[13].Value = lCOA[i].D06;
                                    r.Cells[14].Value = lCOA[i].C06;
                                    r.Cells[15].Value = lCOA[i].D07;
                                    r.Cells[16].Value = lCOA[i].C07;
                                    r.Cells[17].Value = lCOA[i].D08;
                                    r.Cells[18].Value = lCOA[i].C08;
                                    r.Cells[19].Value = lCOA[i].D09;
                                    r.Cells[20].Value = lCOA[i].C09;
                                    r.Cells[21].Value = lCOA[i].D10;
                                    r.Cells[22].Value = lCOA[i].C10;
                                    r.Cells[23].Value = lCOA[i].D11;
                                    r.Cells[24].Value = lCOA[i].C11;
                                    r.Cells[25].Value = lCOA[i].D12;
                                    r.Cells[26].Value = lCOA[i].C12;
                                }
                            }
                        }

                        Grd1.TreeLines.Visible = true;
                        Grd1.Rows.CollapseAll();
                        Grd1.EndUpdate();
                    }
                    #endregion

                    #region Not Filter By Entity
                    if (!IsFilterByEnt && lCOA.Count > 0)
                    {
                        Grd1.BeginUpdate();
                        Grd1.Rows.Count = 0;
                        iGRow r;

                        r = Grd1.Rows.Add();
                        r.Level = 0;
                        r.TreeButton = iGTreeButtonState.Visible;
                        r.Cells[0].Value = "COA";

                        for (var i = 0; i < lCOA.Count; i++)
                        {
                            r = Grd1.Rows.Add();
                            r.Level = lCOA[i].Level;
                            r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                            r.Cells[0].Value = lCOA[i].AcNo;
                            r.Cells[1].Value = lCOA[i].AcDesc;
                            for (var c = 2; c < Grd1.Cols.Count; c++)
                                r.Cells[c].Value = 0m;
                            r.Cells[2].Value = lCOA[i].Balance;
                            r.Cells[3].Value = lCOA[i].D01;
                            r.Cells[4].Value = lCOA[i].C01;
                            r.Cells[5].Value = lCOA[i].D02;
                            r.Cells[6].Value = lCOA[i].C02;
                            r.Cells[7].Value = lCOA[i].D03;
                            r.Cells[8].Value = lCOA[i].C03;
                            r.Cells[9].Value = lCOA[i].D04;
                            r.Cells[10].Value = lCOA[i].C04;
                            r.Cells[11].Value = lCOA[i].D05;
                            r.Cells[12].Value = lCOA[i].C05;
                            r.Cells[13].Value = lCOA[i].D06;
                            r.Cells[14].Value = lCOA[i].C06;
                            r.Cells[15].Value = lCOA[i].D07;
                            r.Cells[16].Value = lCOA[i].C07;
                            r.Cells[17].Value = lCOA[i].D08;
                            r.Cells[18].Value = lCOA[i].C08;
                            r.Cells[19].Value = lCOA[i].D09;
                            r.Cells[20].Value = lCOA[i].C09;
                            r.Cells[21].Value = lCOA[i].D10;
                            r.Cells[22].Value = lCOA[i].C10;
                            r.Cells[23].Value = lCOA[i].D11;
                            r.Cells[24].Value = lCOA[i].C11;
                            r.Cells[25].Value = lCOA[i].D12;
                            r.Cells[26].Value = lCOA[i].C12;
                        }

                        Grd1.TreeLines.Visible = true;
                        Grd1.Rows.CollapseAll();
                        Grd1.EndUpdate();
                    }

                    #endregion
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lCOA.Clear();
                lCOAEntity.Clear();
                lJournal2.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, AcDesc, Parent, AcType ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where Left(AcNo, 1) In ('4', '5') ");
            SQL.AppendLine("And ActInd='Y' ");
            SQL.AppendLine("Order By AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr, bool IsFilterByEnt, string EntCode)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And Left(B.AcNo, 1) In ('4', '5') ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr ");
            if (IsFilterByEnt) SQL.AppendLine("And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            if (IsFilterByEnt)  Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<Journal2> l, string Yr, string StartFrom, bool IsFilterByEnt, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Left(A.DocDt, 6) As YrMth, B.AcNo, Sum(B.DAmt) AS DAmt, Sum(B.CAmt) AS CAmt ");
            SQL.AppendLine("From TblJournalHdr A  ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            if (IsFilterByEnt) SQL.AppendLine(" And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            SQL.AppendLine("Group By Left(A.DocDt, 6), B.AcNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, "12"));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "YrMth", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            YrMth = dr.GetString(0),
                            AcNo = dr.GetString(1),
                            DAmt = dr.GetDecimal(2),
                            CAmt = dr.GetDecimal(3)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3_JournalMemorial(ref List<Journal2> l, string Yr, string StartFrom, bool IsFilterByEnt, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select YrMth, AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("From ( ");

            #region Standard

            SQL.AppendLine("Select Left(A.DocDt, 6) As YrMth, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A  ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            if (IsFilterByEnt) SQL.AppendLine(" And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select Left(A.DocDt, 6) As YrMth, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A  ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            if (IsFilterByEnt) SQL.AppendLine(" And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            SQL.AppendLine("And A.CancelInd='N' And A.Status='O' ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }

            #endregion

            SQL.AppendLine(") Tbl Group By YrMth, AcNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, "12"));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "YrMth", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            YrMth = dr.GetString(0),
                            AcNo = dr.GetString(1),
                            DAmt = dr.GetDecimal(2),
                            CAmt = dr.GetDecimal(3)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4_01(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "01");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D01 += i.DAmt;
                        l1[j].C01 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_02(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "02");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D02 += i.DAmt;
                        l1[j].C02 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_03(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "03");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D03 += i.DAmt;
                        l1[j].C03 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_04(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "04");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D04 += i.DAmt;
                        l1[j].C04 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_05(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "05");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D05 += i.DAmt;
                        l1[j].C05 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_06(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "06");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D06 += i.DAmt;
                        l1[j].C06 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_07(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "07");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D07 += i.DAmt;
                        l1[j].C07 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_08(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "08");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D08 += i.DAmt;
                        l1[j].C08 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_09(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "09");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D09 += i.DAmt;
                        l1[j].C09 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_10(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "10");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D10 += i.DAmt;
                        l1[j].C10 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_11(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "11");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D11 += i.DAmt;
                        l1[j].C11 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process4_12(ref List<COA> l1, ref List<Journal2> l2, string Yr)
        {
            var YrMth = string.Concat(Yr, "12");
            bool IsFirst = false;
            int Temp = 0;

            foreach (var i in (
                from x in l2.Where(w => w.YrMth.CompareTo(YrMth) == 0)
                group x by x.AcNo into g
                select new { AcNo = g.Key, DAmt = g.Sum(s => s.DAmt), CAmt = g.Sum(s => s.CAmt) }
                ).OrderBy(o => o.AcNo))
            {
                IsFirst = false;
                for (var j = Temp; j < l1.Count; j++)
                {
                    if (
                        i.AcNo.Count(x => x == '.') == l1[j].AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, l1[j].AcNo) ||
                        i.AcNo.Count(x => x == '.') != l1[j].AcNo.Count(x => x == '.') && i.AcNo.StartsWith(string.Concat(l1[j].AcNo, '.'))
                        )
                    {
                        if (!IsFirst)
                        {
                            IsFirst = true;
                            Temp = j;
                        }
                        l1[j].D12 += i.DAmt;
                        l1[j].C12 += i.CAmt;
                        if (string.Compare(l1[j].AcNo, i.AcNo) == 0)
                            break;
                    }
                }
            }
        }

        private void Process5(ref List<COA> l)
        {
            
                for (var i = 0; i < l.Count; i++)
                {
                    l[i].YearToDateDAmt = 
                        l[i].OpeningBalanceDAmt + 
                        l[i].D01 +
                        l[i].D02 +
                        l[i].D03 +
                        l[i].D04 +
                        l[i].D05 +
                        l[i].D06 +
                        l[i].D07 +
                        l[i].D08 +
                        l[i].D09 +
                        l[i].D10 +
                        l[i].D11 +
                        l[i].D12;
                    l[i].YearToDateCAmt = 
                        l[i].OpeningBalanceCAmt + 
                        l[i].C01 +
                        l[i].C02 +
                        l[i].C03 +
                        l[i].C04 +
                        l[i].C05 +
                        l[i].C06 +
                        l[i].C07 +
                        l[i].C08 +
                        l[i].C09 +
                        l[i].C10 +
                        l[i].C11 +
                        l[i].C12;

                    if (Sm.Left(l[i].AcNo, 1) == "4")
                        l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

                    if (Sm.Left(l[i].AcNo, 1) == "5")
                        l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
                }
            
            //if (LatestMth == 0)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 1)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D01;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C01;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 2)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D02;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C02;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 3)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D03;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C03;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 4)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D04;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C04;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 5)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D05;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C05;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 6)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D06;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C06;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 7)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D07;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C07;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 8)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D08;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C08;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 9)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D09;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C09;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 10)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D10;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C10;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 11)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D11;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C11;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}

            //if (LatestMth == 12)
            //{
            //    for (var i = 0; i < l.Count; i++)
            //    {
            //        l[i].YearToDateDAmt = l[i].OpeningBalanceDAmt + l[i].D12;
            //        l[i].YearToDateCAmt = l[i].OpeningBalanceCAmt + l[i].C12;

            //        if (Sm.Left(l[i].AcNo, 1) == "4")
            //            l[i].Balance = l[i].YearToDateCAmt - l[i].YearToDateDAmt;

            //        if (Sm.Left(l[i].AcNo, 1) == "5")
            //            l[i].Balance = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            //    }
            //}
        }

        private void Process6(ref List<COA> l)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].Level == -1)
                {
                    if (string.Compare(l[i].Parent, Parent) == 0)
                    {
                        l[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < l.Count; j++)
                        {
                            if (string.Compare(l[i].Parent, l[j].AcNo) == 0)
                            {
                                Parent = l[i].Parent;
                                ParentLevel = l[j].Level;
                                l[i].Level = l[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < l.Count; j++)
                {
                    if (l[i].AcNo.Length >= l[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(l[i].AcNo, l[j].Parent) == 0)
                        {
                            l[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
            ComputeProfitLoss(ref l);
        }

        private void SetCOAEntity(ref List<COAEntity> l, string EntCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, B.EntCode ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo And B.EntCode=@EntCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And Left(A.AcNo, 1) In ('4','5') ");
            SQL.AppendLine("Order By A.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAEntity()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeProfitLoss(ref List<COA> l)
        {
            decimal balance4 = 0m, balance5 = 0m;
            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].AcNo == "4") balance4 = l[i].YearToDateCAmt - l[i].YearToDateDAmt;
                if (l[i].AcNo == "5") balance5 = l[i].YearToDateDAmt - l[i].YearToDateCAmt;
            }
            TxtProfitLoss.EditValue = Sm.FormatNum(balance4 - balance5, 0);
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y';");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public string AcType { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal D01 { get; set; }
            public decimal C01 { get; set; }
            public decimal D02 { get; set; }
            public decimal C02 { get; set; }
            public decimal D03 { get; set; }
            public decimal C03 { get; set; }
            public decimal D04 { get; set; }
            public decimal C04 { get; set; }
            public decimal D05 { get; set; }
            public decimal C05 { get; set; }
            public decimal D06 { get; set; }
            public decimal C06 { get; set; }
            public decimal D07 { get; set; }
            public decimal C07 { get; set; }
            public decimal D08 { get; set; }
            public decimal C08 { get; set; }
            public decimal D09 { get; set; }
            public decimal C09 { get; set; }
            public decimal D10 { get; set; }
            public decimal C10 { get; set; }
            public decimal D11 { get; set; }
            public decimal C11 { get; set; }
            public decimal D12 { get; set; }
            public decimal C12 { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal2
        {
            public string AcNo { get; set; }
            public string YrMth { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAEntity
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
        }

        #endregion
    }
}
