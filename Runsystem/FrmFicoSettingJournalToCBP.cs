﻿#region Update
/*
    11/02/2020 [WED/YK] new apps
    12/05/2020 [VIN/IMS] bisa remove COA saat edit
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFicoSettingJournalToCBP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool
            mMenuCodeForCustomCashFlow = false,
            mMenuCodeForCustomBalanceSheet = false,
            mMenuCodeForCustomProfitLoss = false,
            mMenuCodeForCustomEquity = false;
        private bool
            IsInsert = false;
        internal decimal mCOALevelFicoSettingJournalToCBP = 0m;
        internal FrmFicoSettingJournalToCBPFind FrmFind;

        #endregion

        #region Constructor

        public FrmFicoSettingJournalToCBP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "FICO Reporting's Setting - Journal To RKAP (CBP)";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                if (mMenuCodeForCustomCashFlow)
                {
                    LblDescription4.Visible = MeeDescription4.Visible = false;
                }

                if (mMenuCodeForCustomEquity)
                {
                    LblDescription4.Visible = MeeDescription4.Visible =
                    LblDescription3.Visible = MeeDescription3.Visible =
                    LblDescription2.Visible = MeeDescription2.Visible = false;
                }

                if (mMenuCodeForCustomProfitLoss)
                {
                    LblDescription4.Visible = MeeDescription4.Visible =
                    LblDescription3.Visible = MeeDescription3.Visible = false;
                }

                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueOption(ref LueDocType, "DocTypeFicoSettingJournalToCBP");

                if (!mMenuCodeForCustomProfitLoss)
                {
                    LblSiteCode.Visible = LueSiteCode.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    "",
                    "Account#",
                    "Account Description",
                    "Account Type"
                },
                 new int[] 
                {
                    20, 130, 200, 120
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 1, 2, 3 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueDocType, TxtCode, TxtSequence, MeeDescription1, MeeDescription2,
                        MeeDescription3, MeeDescription4, LueSiteCode
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkActInd.Properties.ReadOnly = true;
                    LueDocType.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCode, TxtSequence, MeeDescription1, MeeDescription2,
                        MeeDescription3, MeeDescription4, LueSiteCode
                    }, false);
                    Grd1.ReadOnly = false;
                    ChkActInd.Checked = true;
                    TxtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSequence, MeeDescription1, MeeDescription2,
                        MeeDescription3, MeeDescription4, LueSiteCode
                    }, false);
                    Grd1.ReadOnly = false;
                    ChkActInd.Properties.ReadOnly = false;
                    TxtSequence.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueDocType, TxtCode, MeeDescription1, MeeDescription2,
                MeeDescription3, MeeDescription4, LueSiteCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtSequence }, 11);
            ChkActInd.Checked = false;
            
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFicoSettingJournalToCBPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                IsInsert = true;
                SetFormControl(mState.Insert);
                if (mMenuCodeForCustomBalanceSheet) Sm.SetLue(LueDocType, "BalanceSheet");
                if (mMenuCodeForCustomEquity) Sm.SetLue(LueDocType, "Equity");
                if (mMenuCodeForCustomProfitLoss) Sm.SetLue(LueDocType, "ProfitLoss");
                if (mMenuCodeForCustomCashFlow) Sm.SetLue(LueDocType, "CashFlow");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueDocType, string.Empty)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid(IsInsert)) return;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveFicoSettingJournalToCBPHdr(IsInsert));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveFicoSettingJournalToCBPDtl(Row));

                Sm.ExecCommands(cml);

                ShowData(Sm.GetLue(LueDocType), TxtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Methods

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmFicoSettingJournalToCBPDlg(this));
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtCode.Text.Length >= 0)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);

                }
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmFicoSettingJournalToCBPDlg(this));

                }
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocType, string Code)
        {
            try
            {
                ClearData();
                ShowFicoSettingJournalToCBPHdr(DocType, Code);
                ShowFicoSettingJournalToCBPDtl(DocType, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowFicoSettingJournalToCBPHdr(string DocType, string Code)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.CmParam<String>(ref cm, "@Code", Code);

            Sm.ShowDataInCtrl(
                ref cm,
                "Select DocType, Code, Sequence, SiteCode, Description1, Description2, Description3, Description4, ActInd From TblFicoSettingJournalToCBPHdr Where DocType=@DocType And Code = @Code;",
                new string[] 
                {
                    //0
                    "DocType", 
                    //1-5
                    "Code", "Sequence", "SiteCode", "Description1", "Description2", 
                    //6-8
                    "Description3", "Description4", "ActInd"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    Sm.SetLue(LueDocType, Sm.DrStr(dr, c[0]));
                    TxtCode.EditValue = Sm.DrStr(dr, c[1]);
                    TxtSequence.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 11);
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[3]));
                    MeeDescription1.EditValue = Sm.DrStr(dr, c[4]);
                    MeeDescription2.EditValue = Sm.DrStr(dr, c[5]);
                    MeeDescription3.EditValue = Sm.DrStr(dr, c[6]);
                    MeeDescription4.EditValue = Sm.DrStr(dr, c[7]);
                    ChkActInd.Checked = Sm.DrStr(dr, c[8]) == "Y";
                }, true
            );
        }

        private void ShowFicoSettingJournalToCBPDtl(string DocType, string Code)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocType", DocType);
            Sm.CmParam<String>(ref cm, "@Code", Code);

            SQL.AppendLine("Select A.AcNo, B.AcDesc, ");
            SQL.AppendLine("Case B.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType ");
            SQL.AppendLine("From TblFicoSettingJournalToCBPDtl A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("    And A.DocType = @DocType ");
            SQL.AppendLine("    And A.Code = @Code; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "AcType" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid(bool IsInsert)
        {
            return
                Sm.IsLueEmpty(LueDocType, "Type") ||
                Sm.IsTxtEmpty(TxtCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtSequence, "Sequence", true) ||
                (mMenuCodeForCustomProfitLoss && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (IsInsert && IsDataExists()) ||
                (!IsInsert && IsDataAlreadyInactive()) ||
                IsGrdValueNotValid();
        }

        private bool IsDataAlreadyInactive()
        {
            if (Sm.IsDataExist("Select 1 From TblFicoSettingJournalToCBPHdr Where DocType = @Param1 And Code = @Param2 And ActInd = 'N'; ", Sm.GetLue(LueDocType), TxtCode.Text, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already inactive.");
                return true;
            }

            return false;
        }

        private bool IsDataExists()
        {
            if (Sm.IsDataExist("Select 1 From TblFicoSettingJournalToCBPHdr Where DocType = @Param1 And Code = @Param2 Limit 1; ", Sm.GetLue(LueDocType), TxtCode.Text, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "This data (Type and Code) already exists.");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Account# is empty.")) return true;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                for (int j = (i + 1); j < Grd1.Rows.Count - 1; ++j)
                {
                    if(i != j)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 1) == Sm.GetGrdStr(Grd1, j, 1))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate account# is found.");
                            Sm.FocusGrd(Grd1, j, 1);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveFicoSettingJournalToCBPHdr(bool IsInsert)
        {
            var SQL = new StringBuilder();

            if (IsInsert)
            {
                SQL.AppendLine("Insert Into TblFicoSettingJournalToCBPHdr(DocType, Code, Sequence, SiteCode, Description1, Description2, Description3, Description4, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocType, @Code, @Sequence, @SiteCode, @Description1, @Description2, @Description3, @Description4, @UserCode, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("Update TblFicoSettingJournalToCBPHdr Set ");
                SQL.AppendLine("    ActInd = @ActInd, ");
                SQL.AppendLine("    Sequence = @Sequence, ");
                SQL.AppendLine("    SiteCode = @SiteCode, ");
                SQL.AppendLine("    Description1 = @Description1, ");
                SQL.AppendLine("    Description2 = @Description2, ");
                SQL.AppendLine("    Description3 = @Description3, ");
                SQL.AppendLine("    Description4 = @Description4, ");
                SQL.AppendLine("    LastUpBy = @UserCode, ");
                SQL.AppendLine("    LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocType = @DocType ");
                SQL.AppendLine("And Code = @Code; ");

                SQL.AppendLine("Delete From TblFicoSettingJournalToCBPDtl ");
                SQL.AppendLine("Where DocType = @DocType ");
                SQL.AppendLine("And Code = @Code; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@Code", TxtCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Sequence", Decimal.Parse(TxtSequence.Text));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Description1", MeeDescription1.Text);
            Sm.CmParam<String>(ref cm, "@Description2", MeeDescription2.Text);
            Sm.CmParam<String>(ref cm, "@Description3", MeeDescription3.Text);
            Sm.CmParam<String>(ref cm, "@Description4", MeeDescription4.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveFicoSettingJournalToCBPDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblFicoSettingJournalToCBPDtl(DocType, Code, AcNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocType, @Code, @AcNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@Code", TxtCode.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        internal string GetSelectedAcNo()
        {
            string SQL = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (SQL.Length > 0) SQL += ",";
                SQL += Sm.GetGrdStr(Grd1, i, 1);
            }

            return SQL.Length == 0 ? "XXX" : SQL;
        }

        private void GetParameter()
        {
            mMenuCodeForCustomBalanceSheet = Sm.GetParameter("MenuCodeForCustomBalanceSheet") == mMenuCode;
            mMenuCodeForCustomCashFlow = Sm.GetParameter("MenuCodeForCustomCashFlow") == mMenuCode;
            mMenuCodeForCustomEquity = Sm.GetParameter("MenuCodeForCustomEquity") == mMenuCode;
            mMenuCodeForCustomProfitLoss = Sm.GetParameter("MenuCodeForCustomProfitLoss") == mMenuCode;
            mCOALevelFicoSettingJournalToCBP = Sm.GetParameterDec("COALevelFicoSettingJournalToCBP");

            if (mCOALevelFicoSettingJournalToCBP == 0m) mCOALevelFicoSettingJournalToCBP = 2m;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtSequence_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtSequence, 11);
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
            }
        }

        #endregion

        
        #endregion

       


    }
}
