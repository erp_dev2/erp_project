﻿#region Update
/*
    10/09/2019 [TKG] master baru keperluan IMM
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmIMMTrackingNo : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmIMMTrackingNoFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMTrackingNo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtTrackingNo }, true);
                    TxtTrackingNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtTrackingNo }, false);
                    TxtTrackingNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtTrackingNo });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMTrackingNoFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, "You can't edit this data");
            
            //if (Sm.IsTxtEmpty(TxtTrackingNo, "", false)) return;
            //SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblIMMTrackingNo(TrackingNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@TrackingNo, @UserCode, CurrentDateTime()); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@TrackingNo", TxtTrackingNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtTrackingNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TrackingNo)
        {
            TxtTrackingNo.EditValue = TrackingNo;
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTrackingNo, "Tracking number", false) ||
                IsTrackingNoExisted();
        }

        private bool IsTrackingNoExisted()
        {
            if (!TxtTrackingNo.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblIMMTrackingNo Where TrackingNo=@Param Limit 1;", TxtTrackingNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Tracking number already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtSellerCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTrackingNo);
        }

        private void TxtSellerName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTrackingNo);
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            var l = new List<ImportData>();
            string CurrentDt = Sm.ServerCurrentDate();
            
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                Insert1(ref l);
                if (l.Count > 0)
                {
                    Insert2(ref l, CurrentDt);
                    Sm.StdMsg(mMsgType.Info, "Importing data process is completed.");
                    BtnCancelClick(sender, e);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Insert1(ref List<ImportData> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            bool IsFirst = true;
            string TrackingNoTemp = string.Empty;
            
            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            TrackingNoTemp = arr[0].Trim();
                            l.Add(new ImportData(){ TrackingNo = TrackingNoTemp });
                        }
                    }
                }
            }
        }

        private void Insert2(ref List<ImportData> l, string CurrentDt)
        {
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            int n = 0;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", CurrentDt);
            
            foreach (var i in l)
            {
                n++;

                SQL.AppendLine("Insert Into TblIMMTrackingNo(TrackingNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select TrackingNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select @TrackingNo" + n.ToString() + " As TrackingNo ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Where Not Exists(Select 1 From TblIMMTrackingNo Where TrackingNo=@TrackingNo" + n.ToString() + "); ");

                Sm.CmParam<String>(ref cm, string.Concat("@TrackingNo", n.ToString()), i.TrackingNo);
            }

            cm.CommandText = SQL.ToString();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #endregion

        #region Class

        private class ImportData
        {
            public string TrackingNo { get; set; }
        }

        #endregion
    }
}
