﻿#region Update
/*
    23/08/2017 [TKG] tambah PO#
    31/08/2017 [TKG] mempercepat proses refresh
    25/10/2017 [HAR] Purchase invoice yang belum di Outgoingpayment kan  minta dimunculin
    13/12/2017 [HAR] Bug Fixing MR approvrnya dicancel tpi muncul voucher nya
    07/03/2018 [TKG] bug quantity po request/po/etc lbh kecil dari quantity MR
    02/10/2018 [DITA] Tambah Doc No Receiving
    25/10/2019 [WED/TWC] query left di subquery diubah ke inner agar query process lebih cepat
    03/03/2020 [DITA/ALL] Tambah filter PI#, OP#, VCR#, VC#, Site
    18/11/2021 [ICA/PHT] dari PO ke Recv di left join agar PO bisa di filter
    27/01/2022 [RIS/PHT] Menambahkan kolom PO QUantity Revision dengan param IsRptMRToVoucherUsePORevision
    14/04/2022 [RIS/PHT] Merubah parameter IsRptMRToVoucherUsePORevision menjadi IsRptProcurementUsePORevision
    18/08/2022 [TYO/PRODUCT] filter department berdasarkan parameter IsFilterByDept
    17/03/2023 [MAU/MNET] memindahkan kolom remark
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMRToVoucher : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "";
        private bool 
            mIsFilterByItCt = false,
            mIsRptProcurementUsePORevision = false,
            mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmRptMRToVoucher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.DocNo, A.DocDt, ");
            SQL.AppendLine("B.DNo, A.DeptCode, C.DeptName, B.ItCode, D.ItName, ");
            SQL.AppendLine("B.Qty-IfNull(J.QtyCancel, 0) As Qty, D.PurchaseUomCode, ");
            SQL.AppendLine("Case B.Status When 'C' Then 'Cancel' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End As StatusMR, ");
            SQL.AppendLine("E.UserMR, IfNull(G.PORequestQty, 0.00) As PORequestQty, E.StatusPOR, E.UserPOR, ");
            SQL.AppendLine("IfNull(H.POQty, 0) As POQty, ");
            SQL.AppendLine("IfNull(I.RecvVdQty, 0) As RecvVdQty, ");
            SQL.AppendLine("IfNull(F.QtyReturn, 0) As QtyReturn, ");
            if (mIsRptProcurementUsePORevision)
                SQL.AppendLine("IfNull(K.PORevisionQty, 0) As PORevisionQty, ");
            else
                SQL.AppendLine("Null As PORevisionQty, ");
            SQL.AppendLine("E.PIDocNo, E.OPDocNo, E.VCRDocNo, E.VCDocNo, E.UserOP, E.VdName, ");
            SQL.AppendLine("A.Remark As HRemark, B.Remark As DRemark, E.PODocNo, E.RecvVdDocNo, A.SiteCode ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Status In ('A', 'O') ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");

            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T7.DocNo Order By T7.DocNo Separator ', ') As PIDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T9.DocNo Order By T9.DocNo Separator ', ') As OPDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T9.VoucherRequestDocNo Order By T9.VoucherRequestDocNo Separator ', ') As VCRDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T10.DocNo Order By T10.DocNo Separator ', ') As VCDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T11.UserCode, ' on ', Concat(Substring(T11.LastUpDt, 7, 2), '/', Substring(T11.LastUpDt, 5, 2), '/', Left(T11.LastUpDt, 4))) Order By T11.ApprovalDNo Separator ', ') As UserOP, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T12.UserCode, ' on ', Concat(Substring(T12.LastUpDt, 7, 2), '/', Substring(T12.LastUpDt, 5, 2), '/', Left(T12.LastUpDt, 4))) Order By T12.ApprovalDNo Separator ', ') As UserMR, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T13.UserCode, ' on ', Concat(Substring(T13.LastUpDt, 7, 2), '/', Substring(T13.LastUpDt, 5, 2), '/', Left(T13.LastUpDt, 4))) Order By T13.ApprovalDNo Separator ', ') As UserPOR, ");
            SQL.AppendLine("    Group_Concat(Distinct Case T13.Status When 'C' Then 'Cancel' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End Order By T13.Status Separator ', ') As StatusPOR, ");
            SQL.AppendLine("    Group_Concat(Distinct T16.VdName Order By T16.VdName Separator ', ') As VdName, ");
            SQL.AppendLine("    Group_Concat(Distinct T4.DocNo Separator ', ') As PODocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T5.DocNo) As RecvVdDocNo ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblRecvVdDtl T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo And T5.CancelInd='N' And T5.Status In ('O', 'A') ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceDtl T6 On T5.DocNo=T6.RecvVdDocNo And T5.DNo=T6.RecvVdDNo ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceHdr T7 On T6.DocNo=T7.DocNo And T7.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblOutgoingPaymentDtl T8 On T7.DocNo=T8.InvoiceDocNo ");
            SQL.AppendLine("    Left Join TblOutgoingPaymentHdr T9 On T8.DocNo=T9.DocNo And T9.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblVoucherHdr T10 On T9.VoucherRequestDocNo=T10.VoucherRequestDocNo And T10.CancelInd='N' ");
            SQL.AppendLine("    Left Join TblDocApproval T11 On T9.DocNo=T11.DocNo And T11.DocType='OutgoingPayment' And T11.UserCode Is Not Null ");
            SQL.AppendLine("    Left Join TblDocApproval T12 On T2.DocNo=T12.DocNo And T2.DNo=T12.DNo And T12.DocType='MaterialRequest' And T12.UserCode Is Not Null ");
            SQL.AppendLine("    Left Join TblDocApproval T13 On T3.DocNo=T13.DocNo And T3.DNo=T13.DNo And T13.DocType='PORequest' And T13.UserCode Is Not Null ");
            SQL.AppendLine("    Inner Join TblQtDtl T14 On T3.QtDocNo=T14.DocNo And T3.QtDNo=T14.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr T15 On T14.DocNo=T15.DocNo ");
            SQL.AppendLine("    Inner Join TblVendor T16 On T15.VdCode=T16.VdCode ");
            SQL.AppendLine("    Left Join TblMRQtyCancel T17 On T2.DocNo=T17.MRDocNo And T2.DNo=T17.MRDNo And T17.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") E On B.DocNo=E.DocNo And B.DNo=E.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select E.MaterialRequestDocNo, E.MaterialRequestDNo, SUM(B.Qty) As QtyReturn ");
            SQL.AppendLine("    From TblDoVDHdr A ");
            SQL.AppendLine("    Inner Join TblDOVdDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl C On B.RecvVdDocNo = C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblPODtl D On C.PODocNo=D.DocNo And C.PODNo=D.DNo  And D.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo  And E.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr F ");
            SQL.AppendLine("        On E.MaterialRequestDocNo=F.DocNo ");
            SQL.AppendLine("        And (F.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Where B.CancelInd = 'N' ");
            SQL.AppendLine("    Group By E.MaterialRequestDocNo, E.MaterialRequestDNo ");
            SQL.AppendLine(") F On B.DocNo=F.MaterialRequestDocNo And B.DNo=F.MaterialRequestDNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Sum(T3.Qty) As PORequestQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") G On B.DocNo=G.DocNo And B.DNo=G.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Sum(T4.Qty) As POQty ");            
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") H On B.DocNo=H.DocNo And B.DNo=H.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Sum(T5.QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo And T5.CancelInd='N' And T5.Status In ('O', 'A') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") I On B.DocNo=I.DocNo And B.DNo=I.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Sum(T3.Qty) As QtyCancel ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblMRQtyCancel T3 On T2.DocNo=T3.MRDocNo And T2.DNo=T3.MRDNo And T3.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") J On B.DocNo=J.DocNo And B.DNo=J.DNo ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
                SQL.AppendLine("    T5.Qty As PORevisionQty ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And (T1.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
                SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblPORevision T5 On T4.DocNo = T5.PODocNo And T4.DNo = T5.PODNo And T5.Status = 'A' ");
                SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 AND T5.DocNo = (Select MAX(docno) From TblPORevision Where PODocNo = T5.PODocNo And Status = 'A') ");
                SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
                SQL.AppendLine(") K On B.DocNo=K.DocNo And B.DNo=K.DNo ");
            }
            

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Material Request#",
                        "Date",
                        "DNo",
                        "Department",
                        "Department",
                        
                        //6-10
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Material Request"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Status",

                        //11-15
                        "By",
                        "",
                        "PO Request"+Environment.NewLine+"Quantity",
                        "Status",
                        "By",

                        //16-20
                        "",
                        "PO"+Environment.NewLine+"Quantity",
                        "Received"+Environment.NewLine+"Quantity",
                        "Return"+Environment.NewLine+"Quantity",
                        "Invoice#",

                        //21-25
                        "Outgoing Payment#",
                        "By",
                        "",
                        "Voucher Request#",
                        "Voucher#",

                        //26-30
                        "Local#",
                        "Vendor",
                        "Remark",
                        "PO#",
                        "",

                        //31-33
                        "Received #",
                        "",
                        "PO"+Environment.NewLine+"Quantity Revision"
                    },
                    new int[] 
                    {   //0
                        50,

                        //1-5
                        150, 100, 0, 0, 150,
                        
                        //6-10
                        100, 180, 100, 70, 80, 
                        
                        //11-15
                        280, 20, 100, 100, 280, 
                        
                        //16-20
                        20, 100, 100, 100, 200,
                        
                        //21-25
                        200, 200, 20, 200, 200, 
                       
                        //26-30
                        150, 150, 400, 200,20,

                        //31-32
                        200, 20, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 12, 16, 23, 32 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 13, 17, 18, 19, 33 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 31, 33 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 9 }, false);
            Grd1.Cols[27].Move(6);
            Grd1.Cols[29].Move(18);
            Grd1.Cols[31].Move(20);
            Grd1.Cols[32].Move(21);
            Grd1.Cols[33].Move(20);
            Grd1.Cols[28].Move(18);
            if (!mIsRptProcurementUsePORevision)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 33 }, false);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "E.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPIDocNo.Text, "E.PIDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtOPDocNo.Text, "E.OPDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVCRDocNo.Text, "E.VCRDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVCDocNo.Text, "E.VCDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By DocDt;",
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DNo", "DeptCode", "DeptName", "ItCode",
                            
                            //6-10
                            "ItName", "Qty", "PurchaseUomCode", "StatusMR", "UserMR", 
                            
                            //11-15
                            "PORequestQty", "StatusPOR", "UserPOR","POQty", "RecvVdQty",

                            //16-20
                            "QtyReturn", "PIDocNo", "OPDocNo", "UserOP", "VCRDocNo",  

                            //21-25
                            "VCDocNo", "LocalDocNo", "Vdname", "HRemark", "DRemark",
 
                            //26-28
                            "PODocNo", "RecvVdDocNo", "PORevisionQty"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            if (Sm.DrStr(dr, 9) == "Cancel")
                            {
                                Grd.Rows[Row].BackColor = Color.Red;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                            
                            Grd.Cells[Row, 28].Value =
                                Sm.DrStr(dr, c[24]).Length == 0 ?
                                Sm.DrStr(dr, c[25]) :
                                string.Concat(Sm.DrStr(dr, c[24]), " ", Sm.DrStr(dr, c[25])).Trim();
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 28);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 13, 17, 18, 19, 33 });
                Grd1.GroupObject.Add(26);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptMRToVoucherDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptMRToVoucherDlg2(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptMRToVoucherDlg4(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 32 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string[] split = Sm.GetGrdStr(Grd1, e.RowIndex, 31).Split(',');

                foreach (string s in split)
                {
                    var f = new FrmRptMRToVoucherDlg5(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1), s);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.TxtDocNo.EditValue = s;
                    f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f.TxtVdName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 27);
                    f.ShowDialog();
                }
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptMRToVoucherDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptMRToVoucherDlg2(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptMRToVoucherDlg4(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 8);
                f.TxtDept.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 32 && Sm.GetGrdStr(Grd1, e.RowIndex, 31).Length != 0)
            {
                string[] split = Sm.GetGrdStr(Grd1, e.RowIndex, 31).Split(',');

                foreach(string s in split)
                {
                    var f = new FrmRptMRToVoucherDlg5(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3), Sm.GetGrdStr(Grd1, e.RowIndex, 1), s);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.TxtDocNo.EditValue = s;
                    f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f.TxtVdName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 27);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Material request#");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);

        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtPIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtOPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtVCRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtVCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PI#");
        }

        private void ChkOPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Outgoing Payment#");
        }

        private void ChkVCRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher request#");
        }

        private void ChkVCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }


        #endregion 

     
       
        
        
    }
}
