﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesPersonPerformance3Dlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptSalesPersonPerformance3 mFrmParent;
        private string mSQL = string.Empty, mSPCode = string.Empty, mSPName = string.Empty, mYr = string.Empty, mMth = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesPersonPerformance3Dlg(FrmRptSalesPersonPerformance3 FrmParent, String SPCode, String SPName, String Yr, String Mth)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSPCode = SPCode;
            mSPName = SPName;
            mYr = Yr;
            mMth = Mth;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                ShowData();
                TxtMth.Properties.ReadOnly = TxtSalesPerson.Properties.ReadOnly = TxtYear.Properties.ReadOnly = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select X1.SPCode,  X1.SPname, 'CBD' As Ind, X1.DOcNO, X1.DocDt, X1.Yr, X1.Mth, X1.AMT, X1.Amt2    "); 
            SQL.AppendLine("    from (   ");
            SQL.AppendLine("        Select A.DocNo, A.DocDt,  ifnull(D.SpCode, B.SpCode) As SpCode, ifNull(A.Salesname, C.SpName) As SpName ,   ");
            SQL.AppendLine("        Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, A.Amt, A.Amt-(ifnull(E.DAmt,0)+ ifnull(E.CAmt, 0)) As Amt2   "); 
            SQL.AppendLine("        From TblsalesInvoicehdr A  ");
            SQL.AppendLine("        Inner Join TblSOhdr B On A.SODocNo = B.DocNo  ");
            SQL.AppendLine("        Left Join TblsalesPerson C On B.SpCode = C.SpCode  ");
            SQL.AppendLine("        Left Join TblsalesPerson D On A.salesname = D.SpName  ");
            SQL.AppendLine("        Left Join TblSalesInvoiceDtl2 E On A.DocNo = E.DocNo And E.OptAcDesc  = '1'  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.SoDocno is not null And left(A.DocDt, 6) = @MonthFilter  ");
            SQL.AppendLine("    )X1  ");
            SQL.AppendLine("    Union All  ");
            SQL.AppendLine("    Select X1.SPCode,  X1.SPname, 'TOP' As Ind, X1.DOcNO, X1.DocDt,  X1.Yr, X1.Mth, X1.AMT, X1.Amt2   ");
            SQL.AppendLine("    from (   ");
            SQL.AppendLine("        Select Distinct  ");
            SQL.AppendLine("        A.DocNo, A.DocDt,  ifnull(I.SpCode, G.SpCode) As SpCode, ifNull(A.Salesname, H.SpName) As SpName ,  ");
            SQL.AppendLine("        Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, A.Amt, A.Amt-(ifnull(J.DAmt,0)+ ifnull(J.CAmt, 0)) As Amt2  ");  
            SQL.AppendLine("        From TblsalesInvoicehdr A  ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("        Inner Join TblDOCt2hdr C On B.DOCtDocNo = C.DocnO  ");
            SQL.AppendLine("        Inner Join TblDoCt2Dtl2 D On B.DOCtDocNo = D.Docno And B.DOCtDno = D.Dno  ");
            SQL.AppendLine("        Inner Join TblDrHdr E On C.DRDocno = E.DocNo And C.DRDocNo Is not null  ");
            SQL.AppendLine("        Inner Join TblDRDtl F On C.DRDocno = F.DocNo And D.DrDno = F.Dno  ");
            SQL.AppendLine("        Inner Join TblSOHdr G On F.SODocNo=G.DocNo  And G.CancelInd = 'N'  ");
            SQL.AppendLine("        Left Join TblsalesPerson H On G.SpCode = H.SpCode  ");
            SQL.AppendLine("        Left Join TblsalesPerson I On A.salesname = I.SpName  ");
            SQL.AppendLine("        Left Join TblSalesInvoiceDtl2 J On A.DocNo = J.DocNo And J.OptAcDesc  = '1'  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.SoDocno is null And left(A.DocDt, 6) =  @MonthFilter  ");
            SQL.AppendLine("    )X1  ");
            SQL.AppendLine("    Where X1.Amt2>0  ");
            SQL.AppendLine("    UNION All  ");
            SQL.AppendLine("    Select X1.SPCode,  X1.SPname, 'EXP' As Ind, X1.DOcNO, X1.DocDt,  X1.Yr, X1.Mth, X1.AMT, X1.Amt2  "); 
            SQL.AppendLine("    from (   ");
            SQL.AppendLine("        Select Distinct  ");
            SQL.AppendLine("        A.DocNo, A.DocDt,  ifnull(I.SpCode, G.SpCode) As SpCode, ifNull(A.Salesname, H.SpName) As SpName ,  ");
            SQL.AppendLine("        Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, A.Amt, A.Amt-(ifnull(J.DAmt,0)+ ifnull(J.CAmt, 0)) As Amt2    ");
            SQL.AppendLine("        From TblsalesInvoicehdr A  ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo   ");
            SQL.AppendLine("        Inner Join TblDOCt2hdr C On B.DOCtDocNo = C.DocnO  And C.PLDocNo Is not null  ");
            SQL.AppendLine("        Inner Join TblDoCt2Dtl3 D On B.DOCtDocNo = D.Docno And B.DOCtDno = D.Dno  ");
            SQL.AppendLine("        Inner Join   ");
            SQL.AppendLine("        (  ");
            SQL.AppendLine("            Select DocNo, SODOcNO From TblPLDtl  ");
            SQL.AppendLine("            Group By DocNO, SoDocNo  ");
            SQL.AppendLine("        )E On C.PlDocNO = E.DocNO  ");
            SQL.AppendLine("        Inner Join TblSOHdr G On E.SODocNo=G.DocNo  And G.CancelInd = 'N'  ");
            SQL.AppendLine("        Left Join TblsalesPerson H On G.SpCode = H.SpCode  ");
            SQL.AppendLine("        Left Join TblsalesPerson I On A.salesname = I.SpName  ");
            SQL.AppendLine("        Left Join TblSalesInvoiceDtl2 J On A.DocNo = J.DocNo And J.OptAcDesc  = '1'  ");
            SQL.AppendLine("        Where A.CancelInd = 'N' And A.SoDocno is null And left(A.DocDt, 6) =  @MonthFilter ");
            SQL.AppendLine("    )X1  ");
            SQL.AppendLine("    Where X1.Amt2>0 ");
            SQL.AppendLine(")Z ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Type",
                        "Document",
                        "Date",
                        "Invoice Amount",
                        "Invoice Amount "+Environment.NewLine+" Without Freight",
                    },
                    new int[] 
                    {
                        //0
                        30,
                        //1-5
                        150, 200, 100, 150, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { });
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, mSPCode, "Z.SPCode", true);
                Sm.FilterStr(ref Filter, ref cm, mYr, "Z.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, mMth, "Z.Mth", true);
                Sm.CmParam<String>(ref cm, "@MonthFilter", string.Concat(mYr, mMth));

           
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order by Z.DocDt ",
                        new string[]
                        {
                            //0
                            "Ind",  
                            //1-5
                            "DocNo", "DocDt", "Amt", "Amt2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {

                var f1 = new FrmDOCt2("");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {

                var f1 = new FrmSO2("");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                string CodeSI = Sm.GetValue("Select SODocNo from tblSalesInvoiceHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 7) + "' limit 1 ");

                if (CodeSI.Length > 1)
                {
                    var f1 = new FrmSalesInvoice2("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f1.ShowDialog();
                }
                else
                {
                    var f1 = new FrmSalesInvoice("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f1.ShowDialog();
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f1 = new FrmDOCt2("");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {

                var f1 = new FrmSO2("");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                string CodeSI = Sm.GetValue("Select SODocNo from tblSalesInvoiceHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 7) + "' limit 1 ");

                if (CodeSI.Length > 1)
                {
                    var f1 = new FrmSalesInvoice2("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f1.ShowDialog();
                }
                else
                {
                    var f1 = new FrmSalesInvoice("");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f1.ShowDialog();
                }
            }
        }

        #endregion

        #endregion

    }
}
