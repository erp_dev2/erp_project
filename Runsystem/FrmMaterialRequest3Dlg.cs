﻿#region Update
/*
    01/10/2019 [WED/IMS] melihat data BOQ jika parameter IsNTPSOCInMRMandatory bernilai Y
    01/11/2019 [DITA/IMS] tambah kolom Specifications
    20/11/2019 [WED/IMS] ambil data dari BOM Revision
    21/11/2019 [WED/IMS] tambah filter BOM# dan Project
    27/11/2019 [WED/IMS] tambah kolom project code, project name, customer PO#, spmk#
    11/02/2020 [TKG/IMS] perhitungan stock tanpa batch# yg berisi project code
    15/02/2020 [TKG/IMS] menggunakan proses BOM yg baru
    28/02/2020 [TKG/IMS] tambah finished goods dan component
    05/03/2020 [TKG/IMS] perubahan validasi item yg dipilih berdasarkan so contract (item's code, bom, finished good, component)
    21/07/2020 [TKG/IMS] Tambah filter item's local code
    25/09/2020 [WED/IMS] tambah input ke Cancelled Qty
    05/11/2020 [WED/IMS] kalau MenuCodeForPRService true, hanya ambil item service saja, else hanya ambil item non service saja
    19/12/2020 [WED/IMS] bisa pilih item sama jika dia PR For Service
    16/02/2021 [WED/IMS] bisa pilih item sama jika dia PR For Service, baik Project nya isi ataupun SO Contract nya yg isi
    23/02/2021 [WED/IMS] untuk PR For Service, item yang terpilih hanya yg stock bebas, dengan batchno sesuai parameter BatchNoForFreeStock
    09/03/2021 [WED/IMS] untuk PR For Service, item yang muncul adalah dari master item dengan validasi yg sudah ada. 
                         Bedanya adalah nanti untuk nilai Stock nya itu ambil dari Stock Summary yang Batch nya sama dengan parameter BatchNoForFreeStock atau yang Batch nya sama dengan ProjectCode
    16/03/2021 [WED/IMS] untuk PR For Production, item yang muncul adalah yg batch nya 0
    17/03/2021 [WED/IMS] untuk PR For Production, item yang muncul hanya yg dari parameter BatchNoForFreeStock, bukan dari Project Code
    17/03/2021 [WED/IMS] untuk PR For Production, ketika SO Contract nya diisi pun melihat logic nya punya PR For Service, yaitu hanya item2 yang ada di BOM nya aja yg muncul
    13/04/2021 [WED/IMS] untuk PR For Service, ketika SO Contract nya diisi, item nya hanya melihat item dari List of Item di SOContract nya
    14/04/2021 [WED/IMS] untuk PR For Service, ketika SO Contract nya diisi, balikin lagi ke keadaan semula (tanpa lihat list of item di SO Contract)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest3 mFrmParent;
        private string mSQL = string.Empty, nSQL = string.Empty, oSQL = string.Empty, mSQL2 = string.Empty, mDeptCode = string.Empty, mReqType = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest3Dlg(FrmMaterialRequest3 FrmParent, string ReqType, string DeptCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mReqType = ReqType;
            mDeptCode = DeptCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item's Code", 
                    "Local Code",
                    "", 
                    "Group",
                    
                    //6-10
                    "Item's Name",
                    "Specification",
                    "Item's Category",
                    "Item's Sub Category Code",
                    "Sub Category",
                    
                    //11-15
                    "UoM",
                    "Quotation#",
                    "D No",
                    "Quotation"+Environment.NewLine+"Date",
                    "Estimated"+Environment.NewLine+"Price",
                    
                    //16-20
                    "Minimum"+Environment.NewLine+"Stock",
                    "Reorder"+Environment.NewLine+"Point",
                    "",
                    "Stock",
                    "Quantity",

                    //21-23
                    "Bom#",
                    "Finished Good",
                    "Component"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 120, 20, 170,
                    
                    //6-10
                    200, 200, 150, 80, 150,
                    
                    //11-15
                    80, 150, 100, 100, 150,

                    //16-20
                    150, 150, 20, 150, 150,

                    //21-23
                    150, 200, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4, 18 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 19, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 8, 9, 10, 13, 14, 15, 16, 17, 18 }, false);
            Grd1.Cols[19].Move(11);
            Grd1.Cols[20].Move(12);
            Sm.SetGrdProperty(Grd1 , false);
        }

        private void SetSQL(string SelectedItem, string Filter)
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(mReqType, "2") && !mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, ");
                SQL.AppendLine("A.ItName, A.Specification, ");
                SQL.AppendLine("0.00 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName, F.Bom2DocNo, ");
                SQL.AppendLine("G.FinishedGood, H.Component ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mFrmParent.mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                if (mFrmParent.mMenuCodeForPRService)
                    SQL.AppendLine("    And A.ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("    And A.ServiceItemInd = 'N' ");

                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct DocNo As Bom2DocNo, ItCode ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select DocNo, ItCode ");
                SQL.AppendLine("        From TblBom2Hdr ");
                SQL.AppendLine("        Where SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And CancelInd='N' ");
                SQL.AppendLine("        And Status='A' ");
                if (mFrmParent.mMenuCodeForPRService)
                    SQL.AppendLine("        And ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("        And ServiceItemInd = 'N' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode2 ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine(") F On A.ItCode=F.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select DocNo As Bom2DocNo, ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct ItName Order By ItName Separator ', ') As FinishedGood ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select T1.DocNo, T1.ItCode, T2.ItName ");
                SQL.AppendLine("        From TblBom2Hdr T1, TblItem T2 ");
                SQL.AppendLine("        Where T1.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And T1.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.Status='A' ");
                SQL.AppendLine("        And T1.ItCode=T2.ItCode ");
                if (mFrmParent.mMenuCodeForPRService)
                    SQL.AppendLine("        And T1.ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("        And T1.ServiceItemInd = 'N' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And A.ItCode=C.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode2, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And A.ItCode=C.ItCode ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("    Where ItName Is Not Null ");
                SQL.AppendLine("    Group By DocNo, ItCode ");
                SQL.AppendLine(") G On F.Bom2DocNo=G.Bom2DocNo And F.ItCode=G.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select DocNo As Bom2DocNo, ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct ItName Order By ItName Separator ', ' ) As Component ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And B.ItCode=C.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And B.ItCode=C.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode2, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And B.ItCode=C.ItCode ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("    Where ItName Is Not Null ");
                SQL.AppendLine("    Group By DocNo, ItCode ");
                SQL.AppendLine(") H On F.Bom2DocNo=H.Bom2DocNo And F.ItCode=H.ItCode ");

                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine(Filter);
            }
            else
            {
                Filter = ((Filter.Replace("F.", "I.")).Replace("G.", "J.")).Replace("H.", "K.");

                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("A.ItName, A.Specification , ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1.00 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0.00) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName, I.Bom2DocNo, J.FinishedGood, K.Component ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mFrmParent.mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                if (mFrmParent.mMenuCodeForPRService)
                    SQL.AppendLine("    And A.ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("    And A.ServiceItemInd = 'N' ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct DocNo As Bom2DocNo, ItCode From ( ");
                SQL.AppendLine("    Select DocNo, ItCode ");
                SQL.AppendLine("    From TblBom2Hdr ");
                SQL.AppendLine("    Where SOContractDocNo Is Not Null ");
                SQL.AppendLine("    And SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("    And CancelInd='N' ");
                SQL.AppendLine("    And Status='A' ");
                if (mFrmParent.mMenuCodeForPRService)
                    SQL.AppendLine("    And ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("    ANd ServiceItemInd = 'N' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.DocNo, B.ItCode ");
                SQL.AppendLine("    From TblBom2Hdr A, TblBom2Dtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("    And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.DocNo, B.ItCode2 ");
                SQL.AppendLine("    From TblBom2Hdr A, TblBom2Dtl2 B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("    And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A' ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine(") I On A.ItCode=I.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select DocNo As Bom2DocNo, ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct ItName Order By ItName Separator ', ') As FinishedGood ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select T1.DocNo, T1.ItCode, T2.ItName ");
                SQL.AppendLine("        From TblBom2Hdr T1, TblItem T2 ");
                SQL.AppendLine("        Where T1.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And T1.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T1.Status='A' ");
                SQL.AppendLine("        And T1.ItCode=T2.ItCode ");
                if (mFrmParent.mMenuCodeForPRService)
                    SQL.AppendLine("        And T1.ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("        And T1.ServiceItemInd = 'N' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And A.ItCode=C.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode2, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And A.ItCode=C.ItCode ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("    Where ItName Is Not Null ");
                SQL.AppendLine("    Group By DocNo, ItCode ");
                SQL.AppendLine(") J On I.Bom2DocNo=J.Bom2DocNo And I.ItCode=J.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select DocNo As Bom2DocNo, ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct ItName Order By ItName Separator ', ') As Component ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And B.ItCode=C.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And B.ItCode=C.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select A.DocNo, B.ItCode2, C.ItName ");
                SQL.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B, TblItem C ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status='A' ");
                SQL.AppendLine("        And B.ItCode=C.ItCode ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("    Where ItName Is Not Null ");
                SQL.AppendLine("    Group By DocNo, ItCode ");
                SQL.AppendLine(") K On I.Bom2DocNo=K.Bom2DocNo And I.ItCode=K.ItCode ");

                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine(Filter);
            }

            if (Sm.IsDataExist("Select 1 From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            if (SelectedItem.Length > 0 && !mFrmParent.mMenuCodeForPRService) SQL.AppendLine("And Position(Concat('##', A.ItCode, '##') In @SelectedItem)<1 ");

            //if (mFrmParent.mMenuCodeForPRService)
            //{
                if (mFrmParent.TxtSOCDocNo.Text.Length > 0)
                {
                    //if (mFrmParent.mMenuCodeForPRService)
                    //{
                    //    SQL.AppendLine("And A.ItCode In ( ");
                    //    SQL.AppendLine("    Select ItCode ");
                    //    SQL.AppendLine("    From TblSOContractDtl ");
                    //    SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
                    //    SQL.AppendLine("    And DocNo In ");
                    //    SQL.AppendLine("    ( ");
                    //    SQL.AppendLine("        Select DocNo ");
                    //    SQL.AppendLine("        From TblSOContractHdr ");
                    //    SQL.AppendLine("        Where DocNo = @SOContractDocNo ");
                    //    SQL.AppendLine("        And CancelInd = 'N' ");
                    //    SQL.AppendLine("        And Status = 'A' ");
                    //    SQL.AppendLine("    ) ");
                    //    SQL.AppendLine(") ");
                    //}
                    if (!mFrmParent.mMenuCodeForPRService)
                    {
                        SQL.AppendLine("And A.ItCode In ( ");
                        SQL.AppendLine("    Select ItCode ");
                        SQL.AppendLine("    From TblBom2Hdr ");
                        SQL.AppendLine("    Where SOContractDocNo Is Not Null ");
                        SQL.AppendLine("    And SOContractDocNo=@SOContractDocNo ");
                        SQL.AppendLine("    And CancelInd='N' ");
                        SQL.AppendLine("    And Status='A' ");
                        if (mFrmParent.mMenuCodeForPRService)
                            SQL.AppendLine("    And ServiceItemInd = 'Y' ");
                        else
                            SQL.AppendLine("    And ServiceItemInd = 'N' ");
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select B.ItCode ");
                        SQL.AppendLine("    From TblBom2Hdr A, TblBom2Dtl B ");
                        SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                        SQL.AppendLine("    And A.SOContractDocNo Is Not Null ");
                        SQL.AppendLine("    And A.SOContractDocNo=@SOContractDocNo ");
                        SQL.AppendLine("    And A.CancelInd='N' ");
                        SQL.AppendLine("    And A.Status='A' ");
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select B.ItCode2 ");
                        SQL.AppendLine("    From TblBom2Hdr A, TblBom2Dtl2 B ");
                        SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                        SQL.AppendLine("    And A.SOContractDocNo Is Not Null ");
                        SQL.AppendLine("    And A.SOContractDocNo=@SOContractDocNo ");
                        SQL.AppendLine("    And A.CancelInd='N' ");
                        SQL.AppendLine("    And A.Status='A' ");
                        SQL.AppendLine(")  ");
                    }
                }
            //}
            //else
            //{
            //    SQL.AppendLine("And A.ItCode In ( ");
            //    SQL.AppendLine("    Select Distinct ItCode ");
            //    SQL.AppendLine("    From TblStockSummary ");
            //    SQL.AppendLine("    Where Find_In_set(BatchNo, @BatchNoForFreeStock) ");
            //    SQL.AppendLine("    Union All ");
            //    SQL.AppendLine("    Select Distinct ItCode ");
            //    SQL.AppendLine("    From TblStockSummary ");
            //    SQL.AppendLine("    Where Find_In_Set(BatchNo, @ProjectCode) ");
            //    SQL.AppendLine(") ");
            //}

            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select T1.*, ");
            SQL2.AppendLine("IfNull(T2.Stock, 0.00) As Stock, ");
            SQL2.AppendLine("Case When IfNull(T3.Qty, 0.00)-IfNull(T4.MaterialRequestQty, 0.00)<0.00 Then ");
            SQL2.AppendLine("   0.00 ");
            SQL2.AppendLine("Else ");
            SQL2.AppendLine("   IfNull(T3.Qty, 0.00)-IfNull(T4.MaterialRequestQty, 0.00) ");
            SQL2.AppendLine("End As Qty ");
            SQL2.AppendLine("From ( ");
            SQL2.AppendLine(SQL.ToString());
            SQL2.AppendLine(") T1 ");
            SQL2.AppendLine("Left Join ( ");
            SQL2.AppendLine("    Select ItCode, Sum(Qty) Stock ");
            SQL2.AppendLine("    From TblStockSummary ");
            SQL2.AppendLine("    Where Qty <> 0 ");
            SQL2.AppendLine("    And BatchNo Is Not Null ");
            if (mFrmParent.mMenuCodeForPRService)
                SQL2.AppendLine("    And BatchNo Not In (Select ProjectCode from TblProjectGroup Where ProjectCode Is Not Null) ");
            else
            {
                SQL2.AppendLine("    And ");
                //SQL2.AppendLine("    ( Find_In_Set(BatchNo, @BatchNoForFreeStock) Or BatchNo = @ProjectCode ) ");
                SQL2.AppendLine("    ( Find_In_Set(BatchNo, @BatchNoForFreeStock) ) ");
            }
            if (mFrmParent.mMenuCodeForPRService)
            {
                SQL2.AppendLine("    And ItCode In ( ");
                SQL2.AppendLine("        Select ItCode ");
                SQL2.AppendLine("        From TblBom2Hdr ");
                SQL2.AppendLine("        Where SOContractDocNo Is Not Null ");
                SQL2.AppendLine("        And SOContractDocNo=@SOContractDocNo ");
                SQL2.AppendLine("        And CancelInd='N' ");
                SQL2.AppendLine("        And Status='A' ");
                if (mFrmParent.mMenuCodeForPRService)
                    SQL2.AppendLine("        And ServiceItemInd = 'Y' ");
                else
                    SQL.AppendLine("         And ServiceItemInd = 'N' ");
                SQL2.AppendLine("    Union All ");
                SQL2.AppendLine("        Select B.ItCode ");
                SQL2.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B ");
                SQL2.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL2.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL2.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL2.AppendLine("        And A.CancelInd='N' ");
                SQL2.AppendLine("        And A.Status='A' ");
                SQL2.AppendLine("    Union All ");
                SQL2.AppendLine("        Select B.ItCode2 ");
                SQL2.AppendLine("        From TblBom2Hdr A, TblBom2Dtl2 B ");
                SQL2.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL2.AppendLine("        And A.SOContractDocNo Is Not Null ");
                SQL2.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
                SQL2.AppendLine("    )  ");
            }
            SQL2.AppendLine("    Group By ItCode ");
            SQL2.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
            SQL2.AppendLine("Left Join ( ");
            SQL2.AppendLine("    Select DocNo As Bom2DocNo, ItCode, Sum(Qty) As Qty From (");
            SQL2.AppendLine("        Select DocNo, ItCode, Qty ");
            SQL2.AppendLine("        From TblBom2Hdr ");
            SQL2.AppendLine("        Where SOContractDocNo Is Not Null ");
            SQL2.AppendLine("        And SOContractDocNo=@SOContractDocNo ");
            SQL2.AppendLine("        And CancelInd='N' ");
            SQL2.AppendLine("        And Status='A' ");
            if (mFrmParent.mMenuCodeForPRService)
                SQL2.AppendLine("         And ServiceItemInd = 'Y' ");
            else
                SQL2.AppendLine("         And ServiceItemInd = 'N' ");
            SQL2.AppendLine("    Union All ");
            SQL2.AppendLine("        Select A.DocNo, B.ItCode, A.Qty*B.Qty As Qty ");
            SQL2.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B ");
            SQL2.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL2.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL2.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL2.AppendLine("        And A.CancelInd='N' ");
            SQL2.AppendLine("        And A.Status='A' ");
            SQL2.AppendLine("    Union All ");
            SQL2.AppendLine("        Select A.DocNo, C.ItCode2 As ItCode, A.Qty*B.Qty*C.Qty As Qty ");
            SQL2.AppendLine("        From TblBom2Hdr A, TblBom2Dtl B, TblBom2Dtl2 C ");
            SQL2.AppendLine("        Where A.DocNo=B.DocNo ");
            SQL2.AppendLine("        And B.DocNo=C.DocNo ");
            SQL2.AppendLine("        And B.ItCode=C.ItCode ");
            SQL2.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL2.AppendLine("        And A.SOContractDocNo=@SOContractDocNo ");
            SQL2.AppendLine("        And A.CancelInd='N' ");
            SQL2.AppendLine("        And A.Status='A' ");
            SQL2.AppendLine("    ) T ");
            SQL2.AppendLine("    Group By DocNo, ItCode ");
            SQL2.AppendLine(") T3 On T1.Bom2DocNo=T3.Bom2DocNo And T1.ItCode=T3.ItCode ");
            SQL2.AppendLine("Left Join ( ");
            SQL2.AppendLine("    Select B.Bom2DocNo, B.ItCode, Sum(B.Qty) As MaterialRequestQty ");
            SQL2.AppendLine("    From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
            SQL2.AppendLine("    Where A.DocNo=B.DocNo ");
            SQL2.AppendLine("    And A.SOCDocNo Is Not Null ");
            SQL2.AppendLine("    And A.SOCDocNo=@SOContractDocNo ");
            SQL2.AppendLine("    And A.CancelInd='N' ");
            SQL2.AppendLine("    And A.Status In ('O', 'A') ");
            SQL2.AppendLine("    And B.CancelInd='N' ");
            SQL2.AppendLine("    And B.Status In ('O', 'A') ");
            SQL2.AppendLine("    Group By B.Bom2DocNo, B.ItCode ");
            SQL2.AppendLine(") T4 On T1.Bom2DocNo=T4.Bom2DocNo And T1.ItCode=T4.ItCode ");
            SQL2.AppendLine("Order By T1.ItName; ");
                
            mSQL = SQL2.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 8, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                var SelectedItem = mFrmParent.GetSelectedItem();
                
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", mFrmParent.TxtSOCDocNo.Text);
                Sm.CmParam<String>(ref cm, "@BatchNoForFreeStock", mFrmParent.mBatchNoForFreeStock);
                Sm.CmParam<String>(ref cm, "@ProjectCode", mFrmParent.TxtProjectCode.Text);
                
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", SelectedItem);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName", "A.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtBom2DocNo.Text, "F.Bom2DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtFinishedGood.Text, "G.FinishedGood", false);
                Sm.FilterStr(ref Filter, ref cm, TxtComponent.Text, "H.Component", false);

                SetSQL(SelectedItem, Filter);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItGrpName", "ItName", "Specification",  "ItCtName",   
                            
                            //6-7
                            "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                            //11-15
                            "DocDt", "UPrice", "MinStock", "ReorderStock", "Stock",

                            //16-19
                            "Qty", "Bom2DocNo", "FinishedGood", "Component"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                {
                    if (IsChoose == false) IsChoose = true;

                    Row1 = mFrmParent.Grd1.Rows.Count - 1;
                    Row2 = Row;

                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 6);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 7);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 9);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 10);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 11);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 12);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 13);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 14);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 15);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 19);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 20);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 21);
                    if (mFrmParent.mIsBudgetActive) mFrmParent.Grd1.Cells[Row1, 11].ForeColor = Sm.GetGrdStr(mFrmParent.Grd1, Row1, 20).Length > 0 ? Color.Black : Color.Red;
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 22);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 23);
                    mFrmParent.Grd1.Cells[Row1, 37].Value = 0m;

                    mFrmParent.ComputeTotal(Row1);

                    mFrmParent.Grd1.Rows.Add();
                    Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 17, 23, 24, 29, 37 });
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            mFrmParent.SetSeqNo();
            mFrmParent.SetDroppingReqProjImplItQty();
            mFrmParent.SetDroppingReqBCItQty();
        }

        private bool IsItCodeAlreadyChosen(int r)
        {
            var key = string.Empty;

            if (mFrmParent.TxtSOCDocNo.Text.Length > 0 || mFrmParent.TxtProjectCode.Text.Length > 0)
            {
                if (!mFrmParent.mMenuCodeForPRService)
                {
                    key = string.Concat(
                         Sm.GetGrdStr(Grd1, r, 2),
                         Sm.GetGrdStr(Grd1, r, 21),
                         Sm.GetGrdStr(Grd1, r, 22),
                         Sm.GetGrdStr(Grd1, r, 23)
                         );
                    for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                        if (Sm.CompareStr(
                            string.Concat(
                                Sm.GetGrdStr(mFrmParent.Grd1, Index, 8),
                                Sm.GetGrdStr(mFrmParent.Grd1, Index, 34),
                                Sm.GetGrdStr(mFrmParent.Grd1, Index, 35),
                                Sm.GetGrdStr(mFrmParent.Grd1, Index, 36)
                                ), key))
                            return true;
                }
            }
            else
            {
                key = Sm.GetGrdStr(Grd1, r, 2);
                for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), key)) return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 18)
            {
                e.DoDefault = false;
                var f = new FrmQt(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 18)
            {
                var f = new FrmQt(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtBom2DocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBom2DocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bom#");
        }

        private void TxtFinishedGood_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkFinishedGood_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Finished good");
        }

        private void TxtComponent_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkComponent_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Component");
        }

        #endregion

        #endregion
     }
}