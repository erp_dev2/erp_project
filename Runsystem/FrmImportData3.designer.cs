﻿namespace RunSystem
{
    partial class FrmImportData3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImportData3));
            this.LblImportCode = new System.Windows.Forms.Label();
            this.LueImportCode = new DevExpress.XtraEditors.LookUpEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.BtnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFileName = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnMutatedFrom = new System.Windows.Forms.Button();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueImportCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFileName.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1006, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.panel1.Size = new System.Drawing.Size(105, 595);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.BtnBrowse);
            this.panel2.Controls.Add(this.TxtFileName);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LblImportCode);
            this.panel2.Controls.Add(this.LueImportCode);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.panel2.Size = new System.Drawing.Size(1006, 595);
            // 
            // LblImportCode
            // 
            this.LblImportCode.AutoSize = true;
            this.LblImportCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblImportCode.ForeColor = System.Drawing.Color.Red;
            this.LblImportCode.Location = new System.Drawing.Point(36, 15);
            this.LblImportCode.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.LblImportCode.Name = "LblImportCode";
            this.LblImportCode.Size = new System.Drawing.Size(64, 22);
            this.LblImportCode.TabIndex = 3;
            this.LblImportCode.Text = "Import";
            this.LblImportCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueImportCode
            // 
            this.LueImportCode.EnterMoveNextControl = true;
            this.LueImportCode.Location = new System.Drawing.Point(111, 11);
            this.LueImportCode.Margin = new System.Windows.Forms.Padding(8);
            this.LueImportCode.Name = "LueImportCode";
            this.LueImportCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.Appearance.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueImportCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueImportCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueImportCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueImportCode.Properties.DropDownRows = 30;
            this.LueImportCode.Properties.MaxLength = 1;
            this.LueImportCode.Properties.NullText = "[Empty]";
            this.LueImportCode.Properties.PopupWidth = 350;
            this.LueImportCode.Size = new System.Drawing.Size(460, 28);
            this.LueImportCode.TabIndex = 4;
            this.LueImportCode.ToolTip = "F4 : Show/hide list";
            this.LueImportCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueImportCode.EditValueChanged += new System.EventHandler(this.LueImportCode_EditValueChanged);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // BtnBrowse
            // 
            this.BtnBrowse.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBrowse.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBrowse.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBrowse.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBrowse.Appearance.Options.UseBackColor = true;
            this.BtnBrowse.Appearance.Options.UseFont = true;
            this.BtnBrowse.Appearance.Options.UseForeColor = true;
            this.BtnBrowse.Appearance.Options.UseTextOptions = true;
            this.BtnBrowse.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBrowse.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("BtnBrowse.Image")));
            this.BtnBrowse.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBrowse.Location = new System.Drawing.Point(576, 40);
            this.BtnBrowse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnBrowse.Name = "BtnBrowse";
            this.BtnBrowse.Size = new System.Drawing.Size(36, 32);
            this.BtnBrowse.TabIndex = 7;
            this.BtnBrowse.ToolTip = "Browse File";
            this.BtnBrowse.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBrowse.ToolTipTitle = "Run System";
            this.BtnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
            // 
            // TxtFileName
            // 
            this.TxtFileName.EnterMoveNextControl = true;
            this.TxtFileName.Location = new System.Drawing.Point(111, 43);
            this.TxtFileName.Margin = new System.Windows.Forms.Padding(8);
            this.TxtFileName.Name = "TxtFileName";
            this.TxtFileName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFileName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFileName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFileName.Properties.Appearance.Options.UseFont = true;
            this.TxtFileName.Properties.MaxLength = 25;
            this.TxtFileName.Size = new System.Drawing.Size(460, 28);
            this.TxtFileName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(12, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 22);
            this.label4.TabIndex = 5;
            this.label4.Text = "File (CSV)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 90);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1006, 505);
            this.panel3.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            this.splitContainer1.Panel1.Controls.Add(this.BtnMutatedFrom);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Grd2);
            this.splitContainer1.Panel2.Controls.Add(this.button1);
            this.splitContainer1.Size = new System.Drawing.Size(1006, 505);
            this.splitContainer1.SplitterDistance = 511;
            this.splitContainer1.TabIndex = 0;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 25;
            this.Grd1.Location = new System.Drawing.Point(0, 39);
            this.Grd1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(511, 466);
            this.Grd1.TabIndex = 31;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnMutatedFrom
            // 
            this.BtnMutatedFrom.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnMutatedFrom.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnMutatedFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMutatedFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMutatedFrom.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnMutatedFrom.Location = new System.Drawing.Point(0, 0);
            this.BtnMutatedFrom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnMutatedFrom.Name = "BtnMutatedFrom";
            this.BtnMutatedFrom.Size = new System.Drawing.Size(511, 39);
            this.BtnMutatedFrom.TabIndex = 24;
            this.BtnMutatedFrom.Text = "Form Input";
            this.BtnMutatedFrom.UseVisualStyleBackColor = false;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 25;
            this.Grd2.Location = new System.Drawing.Point(0, 39);
            this.Grd2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(491, 466);
            this.Grd2.TabIndex = 31;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(491, 39);
            this.button1.TabIndex = 24;
            this.button1.Text = "CSV Column";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1006, 90);
            this.panel4.TabIndex = 9;
            // 
            // FrmImportData2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 595);
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "FrmImportData2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueImportCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFileName.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblImportCode;
        private DevExpress.XtraEditors.LookUpEdit LueImportCode;
        private System.Windows.Forms.OpenFileDialog OD;
        public DevExpress.XtraEditors.SimpleButton BtnBrowse;
        internal DevExpress.XtraEditors.TextEdit TxtFileName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button BtnMutatedFrom;
        private System.Windows.Forms.Button button1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel4;
    }
}