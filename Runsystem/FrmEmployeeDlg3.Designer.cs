﻿namespace RunSystem
{
    partial class FrmEmployeeDlg3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkSection = new DevExpress.XtraEditors.CheckEdit();
            this.LueSection = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkSiteCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueSSInd = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.ChkSSInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkStatus = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkLeaveStartDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteLeaveStartDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteLeaveStartDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkDocDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ChkDeptCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkEmpCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSSInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLeaveStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmpCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(767, 0);
            this.panel1.Size = new System.Drawing.Size(70, 550);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 528);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ChkLeaveStartDt);
            this.panel2.Controls.Add(this.DteLeaveStartDt2);
            this.panel2.Controls.Add(this.DteLeaveStartDt1);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkDocDt);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.ChkDeptCode);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtEmpCode);
            this.panel2.Controls.Add(this.ChkEmpCode);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(767, 95);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(767, 455);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // BtnClose
            // 
            this.BtnClose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnClose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnClose.Appearance.Options.UseBackColor = true;
            this.BtnClose.Appearance.Options.UseFont = true;
            this.BtnClose.Appearance.Options.UseForeColor = true;
            this.BtnClose.Appearance.Options.UseTextOptions = true;
            this.BtnClose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkSection);
            this.panel4.Controls.Add(this.LueSection);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.LueSiteCode);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.ChkSiteCode);
            this.panel4.Controls.Add(this.LueSSInd);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.ChkSSInd);
            this.panel4.Controls.Add(this.LueStatus);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.ChkStatus);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(456, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(307, 91);
            this.panel4.TabIndex = 26;
            // 
            // ChkSection
            // 
            this.ChkSection.Location = new System.Drawing.Point(287, 68);
            this.ChkSection.Name = "ChkSection";
            this.ChkSection.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSection.Properties.Appearance.Options.UseFont = true;
            this.ChkSection.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSection.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSection.Properties.Caption = " ";
            this.ChkSection.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSection.Size = new System.Drawing.Size(19, 22);
            this.ChkSection.TabIndex = 37;
            this.ChkSection.ToolTip = "Remove filter";
            this.ChkSection.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSection.ToolTipTitle = "Run System";
            this.ChkSection.CheckedChanged += new System.EventHandler(this.ChkSection_CheckedChanged);
            // 
            // LueSection
            // 
            this.LueSection.EnterMoveNextControl = true;
            this.LueSection.Location = new System.Drawing.Point(93, 68);
            this.LueSection.Margin = new System.Windows.Forms.Padding(5);
            this.LueSection.Name = "LueSection";
            this.LueSection.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.Appearance.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSection.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSection.Properties.DropDownRows = 30;
            this.LueSection.Properties.MaxLength = 16;
            this.LueSection.Properties.NullText = "[Empty]";
            this.LueSection.Properties.PopupWidth = 200;
            this.LueSection.Size = new System.Drawing.Size(191, 20);
            this.LueSection.TabIndex = 36;
            this.LueSection.ToolTip = "F4 : Show/hide list";
            this.LueSection.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSection.EditValueChanged += new System.EventHandler(this.LueSection_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(42, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 14);
            this.label10.TabIndex = 35;
            this.label10.Text = "Section";
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(93, 47);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 200;
            this.LueSiteCode.Size = new System.Drawing.Size(191, 20);
            this.LueSiteCode.TabIndex = 33;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(61, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "Site";
            // 
            // ChkSiteCode
            // 
            this.ChkSiteCode.Location = new System.Drawing.Point(287, 45);
            this.ChkSiteCode.Name = "ChkSiteCode";
            this.ChkSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCode.Properties.Caption = " ";
            this.ChkSiteCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCode.Size = new System.Drawing.Size(19, 22);
            this.ChkSiteCode.TabIndex = 34;
            this.ChkSiteCode.ToolTip = "Remove filter";
            this.ChkSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCode.ToolTipTitle = "Run System";
            this.ChkSiteCode.CheckedChanged += new System.EventHandler(this.ChkSiteCode_CheckedChanged);
            // 
            // LueSSInd
            // 
            this.LueSSInd.EnterMoveNextControl = true;
            this.LueSSInd.Location = new System.Drawing.Point(93, 25);
            this.LueSSInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueSSInd.Name = "LueSSInd";
            this.LueSSInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSInd.Properties.Appearance.Options.UseFont = true;
            this.LueSSInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSSInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSSInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSSInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSSInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSSInd.Properties.DropDownRows = 5;
            this.LueSSInd.Properties.MaxLength = 16;
            this.LueSSInd.Properties.NullText = "[Empty]";
            this.LueSSInd.Properties.PopupWidth = 200;
            this.LueSSInd.Size = new System.Drawing.Size(191, 20);
            this.LueSSInd.TabIndex = 30;
            this.LueSSInd.ToolTip = "F4 : Show/hide list";
            this.LueSSInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSSInd.EditValueChanged += new System.EventHandler(this.LueSSInd_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 14);
            this.label2.TabIndex = 29;
            this.label2.Text = "Social Security";
            // 
            // ChkSSInd
            // 
            this.ChkSSInd.Location = new System.Drawing.Point(287, 24);
            this.ChkSSInd.Name = "ChkSSInd";
            this.ChkSSInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSSInd.Properties.Appearance.Options.UseFont = true;
            this.ChkSSInd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSSInd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSSInd.Properties.Caption = " ";
            this.ChkSSInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSSInd.Size = new System.Drawing.Size(19, 22);
            this.ChkSSInd.TabIndex = 31;
            this.ChkSSInd.ToolTip = "Remove filter";
            this.ChkSSInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSSInd.ToolTipTitle = "Run System";
            this.ChkSSInd.CheckedChanged += new System.EventHandler(this.ChkSSInd_CheckedChanged);
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(93, 3);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 4;
            this.LueStatus.Properties.MaxLength = 16;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 200;
            this.LueStatus.Size = new System.Drawing.Size(191, 20);
            this.LueStatus.TabIndex = 27;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 26;
            this.label1.Text = "Status";
            // 
            // ChkStatus
            // 
            this.ChkStatus.Location = new System.Drawing.Point(287, 2);
            this.ChkStatus.Name = "ChkStatus";
            this.ChkStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkStatus.Properties.Appearance.Options.UseFont = true;
            this.ChkStatus.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkStatus.Properties.Caption = " ";
            this.ChkStatus.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkStatus.Size = new System.Drawing.Size(19, 22);
            this.ChkStatus.TabIndex = 28;
            this.ChkStatus.ToolTip = "Remove filter";
            this.ChkStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkStatus.ToolTipTitle = "Run System";
            this.ChkStatus.CheckedChanged += new System.EventHandler(this.ChkStatus_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(209, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "-";
            // 
            // ChkLeaveStartDt
            // 
            this.ChkLeaveStartDt.Location = new System.Drawing.Point(329, 68);
            this.ChkLeaveStartDt.Name = "ChkLeaveStartDt";
            this.ChkLeaveStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLeaveStartDt.Properties.Appearance.Options.UseFont = true;
            this.ChkLeaveStartDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLeaveStartDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLeaveStartDt.Properties.Caption = " ";
            this.ChkLeaveStartDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLeaveStartDt.Size = new System.Drawing.Size(20, 22);
            this.ChkLeaveStartDt.TabIndex = 42;
            this.ChkLeaveStartDt.ToolTip = "Remove filter";
            this.ChkLeaveStartDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLeaveStartDt.ToolTipTitle = "Run System";
            this.ChkLeaveStartDt.CheckedChanged += new System.EventHandler(this.ChkLeaveStartDt_CheckedChanged);
            // 
            // DteLeaveStartDt2
            // 
            this.DteLeaveStartDt2.EditValue = null;
            this.DteLeaveStartDt2.EnterMoveNextControl = true;
            this.DteLeaveStartDt2.Location = new System.Drawing.Point(224, 69);
            this.DteLeaveStartDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLeaveStartDt2.Name = "DteLeaveStartDt2";
            this.DteLeaveStartDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt2.Properties.Appearance.Options.UseFont = true;
            this.DteLeaveStartDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLeaveStartDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLeaveStartDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLeaveStartDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLeaveStartDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLeaveStartDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLeaveStartDt2.Size = new System.Drawing.Size(100, 20);
            this.DteLeaveStartDt2.TabIndex = 41;
            this.DteLeaveStartDt2.EditValueChanged += new System.EventHandler(this.DteLeaveStartDt2_EditValueChanged);
            // 
            // DteLeaveStartDt1
            // 
            this.DteLeaveStartDt1.EditValue = null;
            this.DteLeaveStartDt1.EnterMoveNextControl = true;
            this.DteLeaveStartDt1.Location = new System.Drawing.Point(105, 69);
            this.DteLeaveStartDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLeaveStartDt1.Name = "DteLeaveStartDt1";
            this.DteLeaveStartDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt1.Properties.Appearance.Options.UseFont = true;
            this.DteLeaveStartDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLeaveStartDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLeaveStartDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLeaveStartDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLeaveStartDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLeaveStartDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLeaveStartDt1.Size = new System.Drawing.Size(100, 20);
            this.DteLeaveStartDt1.TabIndex = 39;
            this.DteLeaveStartDt1.EditValueChanged += new System.EventHandler(this.DteLeaveStartDt1_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 14);
            this.label9.TabIndex = 38;
            this.label9.Text = "Permanent Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(209, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 14);
            this.label4.TabIndex = 35;
            this.label4.Text = "-";
            // 
            // ChkDocDt
            // 
            this.ChkDocDt.Location = new System.Drawing.Point(329, 46);
            this.ChkDocDt.Name = "ChkDocDt";
            this.ChkDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocDt.Properties.Appearance.Options.UseFont = true;
            this.ChkDocDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocDt.Properties.Caption = " ";
            this.ChkDocDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocDt.Size = new System.Drawing.Size(20, 22);
            this.ChkDocDt.TabIndex = 37;
            this.ChkDocDt.ToolTip = "Remove filter";
            this.ChkDocDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocDt.ToolTipTitle = "Run System";
            this.ChkDocDt.CheckedChanged += new System.EventHandler(this.ChkDocDt_CheckedChanged);
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(224, 47);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(100, 20);
            this.DteDocDt2.TabIndex = 36;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(105, 47);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(100, 20);
            this.DteDocDt1.TabIndex = 34;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 14);
            this.label3.TabIndex = 33;
            this.label3.Text = "Birth Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(29, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "Department";
            // 
            // ChkDeptCode
            // 
            this.ChkDeptCode.Location = new System.Drawing.Point(329, 25);
            this.ChkDeptCode.Name = "ChkDeptCode";
            this.ChkDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptCode.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptCode.Properties.Caption = " ";
            this.ChkDeptCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptCode.Size = new System.Drawing.Size(19, 22);
            this.ChkDeptCode.TabIndex = 32;
            this.ChkDeptCode.ToolTip = "Remove filter";
            this.ChkDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptCode.ToolTipTitle = "Run System";
            this.ChkDeptCode.CheckedChanged += new System.EventHandler(this.ChkDeptCode_CheckedChanged);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(105, 25);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(220, 20);
            this.LueDeptCode.TabIndex = 31;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(42, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "Employee";
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(105, 3);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 40;
            this.TxtEmpCode.Size = new System.Drawing.Size(220, 20);
            this.TxtEmpCode.TabIndex = 28;
            this.TxtEmpCode.Validated += new System.EventHandler(this.TxtEmpCode_Validated);
            // 
            // ChkEmpCode
            // 
            this.ChkEmpCode.Location = new System.Drawing.Point(329, 3);
            this.ChkEmpCode.Name = "ChkEmpCode";
            this.ChkEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEmpCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEmpCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEmpCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEmpCode.Properties.Caption = " ";
            this.ChkEmpCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEmpCode.Size = new System.Drawing.Size(19, 22);
            this.ChkEmpCode.TabIndex = 29;
            this.ChkEmpCode.ToolTip = "Remove filter";
            this.ChkEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEmpCode.ToolTipTitle = "Run System";
            this.ChkEmpCode.CheckedChanged += new System.EventHandler(this.ChkEmpCode_CheckedChanged);
            // 
            // FrmEmployeeDlg3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 550);
            this.Name = "FrmEmployeeDlg3";
            this.Text = "List of Employee";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSSInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLeaveStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmpCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkSection;
        private DevExpress.XtraEditors.LookUpEdit LueSection;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSSInd;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkSSInd;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkStatus;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.CheckEdit ChkLeaveStartDt;
        internal DevExpress.XtraEditors.DateEdit DteLeaveStartDt2;
        internal DevExpress.XtraEditors.DateEdit DteLeaveStartDt1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkDocDt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private DevExpress.XtraEditors.CheckEdit ChkEmpCode;
    }
}