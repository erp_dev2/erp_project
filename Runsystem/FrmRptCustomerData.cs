﻿#region Update
/*
    27/04/2018 [WED] customer code saat di excel tetep muncul angka 0 didepan
    17/05/2018 [TKG] Ubah tampilan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCustomerData : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptCustomerData(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode, A.CtName, A.CtShortCode, B.CtCtName,  ");
            SQL.AppendLine("A.Address, C.CityName, D.Cntname, A.PostalCd, ");
            SQL.AppendLine("A.Phone, A.Fax, A.Email, A.Mobile, A.NPWP, ");
            SQL.AppendLine("E.ContactPerson, F.ShippingAddress, G.Asset, A.Remark "); 
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Left Join TblCustomerCategory B On A.CtCtCode=B.CtCtCode  ");
            SQL.AppendLine("Left Join tblCity C On A.CityCode=C.CityCode  ");
            SQL.AppendLine("Left Join TblCountry D On A.CntCode=D.CntCode  ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, ");
            SQL.AppendLine("    Group_Concat(Concat(ContactPersonName, Case When Position Is Null Then '' Else Concat(' (', Position, ')') End) Order By ContactPersonName Separator '###') As ContactPerson ");
            SQL.AppendLine("    From TblCustomerContactPerson ");
            SQL.AppendLine("    Where ContactPersonName Is Not Null ");
            SQL.AppendLine("    Group By CtCode ");
            SQL.AppendLine(") E On A.CtCode=E.CtCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.CtCode, ");
            SQL.AppendLine("    Group_Concat(Concat(T1.Name, ");
            SQL.AppendLine("    Case When T1.Address Is Null Then ");
            SQL.AppendLine("        Case When T2.CityName Is Null Then '' Else Concat(' (', T2.CityName, ')') End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("    Concat(' (', T1.Address, ");
            SQL.AppendLine("        Case When T2.CityName Is Null Then '' Else Concat(', ', T2.CityName) End, ");
            SQL.AppendLine("    ')') ");
            SQL.AppendLine("    End) Order By T1.Name Separator '###') As ShippingAddress ");
            SQL.AppendLine("    From TblCustomerShipAddress T1 ");
            SQL.AppendLine("    Left Join TblCity T2 On T1.CityCode=T2.CityCode ");
            SQL.AppendLine("    Where T1.Name Is Not Null ");
            SQL.AppendLine("    Group By T1.CtCode ");
            SQL.AppendLine(") F On A.CtCode=F.CtCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.CtCode, ");
            SQL.AppendLine("    Group_Concat(Concat( ");
            SQL.AppendLine("    Concat('Asset : ', T2.AssetName, '(', T2.AssetCode, ')'), ");
            SQL.AppendLine("    Case When T2.Wide<=0.00 Then '' Else Concat('###Wide : ', Convert(Format(T2.Wide, 2) Using utf8)) End, ");
            SQL.AppendLine("    Case When T1.AssetLocation Is Null Then '' Else Concat('###Location : ', T1.AssetLocation) End,");
            SQL.AppendLine("    Case When T1.ContractNo Is Null Then '' Else Concat('###Contract# : ', T1.ContractNo) End ");
            SQL.AppendLine("    ) Order By T2.AssetName Separator '######') As Asset ");
            SQL.AppendLine("    From TblCustomerAsset T1 ");
            SQL.AppendLine("    Left Join TblAsset T2 On T1.AssetCode=T2.AssetCode ");
            SQL.AppendLine("    Group By T1.CtCode ");
            SQL.AppendLine(") G On A.CtCode=G.CtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Customer"+Environment.NewLine+"Code", 
                        "Customer Name",
                        "Short Code",
                        "Category",
                        "Address",

                        //6-10
                        "City",
                        "Country", 
                        "Postal"+Environment.NewLine+"Code",
                        "Phone",
                        "Fax",

                        //11-15
                        "Email",
                        "Mobile",
                        "NPWP",
                        "Contact Person",
                        "Shipping Address", 

                        //16-17
                        "Asset",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 180, 300,                         

                        //6-10
                        180, 180, 80, 100, 100, 

                        //11-15
                        130, 100, 140, 400, 400, 

                        //16-17
                        400, 400
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCtName.Text, new string[] { "A.CtCode", "A.Ctname" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CtName;",
                        new string[]
                        {
                            //0
                            "CtCode",

                            //1-5
                            "Ctname", "CtShortCode", "CtCtName", "Address", "CityName", 

                            //6-10
                            "CntName", "PostalCd", "Phone", "Fax", "Email", 

                            //11-15
                            "Mobile", "NPWP", "ContactPerson", "ShippingAddress", "Asset",  
                            
                            //16
                            "Remark"  
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Grd1.Cells[Row, 14].Value = Sm.DrStr(dr, 13).Replace("###", Environment.NewLine);
                            Grd1.Cells[Row, 15].Value = Sm.DrStr(dr, 14).Replace("###", Environment.NewLine);
                            Grd1.Cells[Row, 16].Value = Sm.DrStr(dr, 15).Replace("###", Environment.NewLine);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                Grd1.Rows.AutoHeight();
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        private void TxtCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
