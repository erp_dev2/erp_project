﻿namespace RunSystem
{
    partial class FrmEmployeeRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkTerminate = new DevExpress.XtraEditors.CheckEdit();
            this.ChkMutated = new DevExpress.XtraEditors.CheckEdit();
            this.ChkResigned = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPromoted = new DevExpress.XtraEditors.CheckEdit();
            this.LblResigned = new System.Windows.Forms.Label();
            this.LblTerminate = new System.Windows.Forms.Label();
            this.LblMutated = new System.Windows.Forms.Label();
            this.LblPromoted = new System.Windows.Forms.Label();
            this.LblNewPosition = new System.Windows.Forms.Label();
            this.LblRetired = new System.Windows.Forms.Label();
            this.ChkRetired = new DevExpress.XtraEditors.CheckEdit();
            this.ChkNewPosition = new DevExpress.XtraEditors.CheckEdit();
            this.LblOther2 = new System.Windows.Forms.Label();
            this.ChkOther2 = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueCompetenceLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtAge = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.DteUseDt = new DevExpress.XtraEditors.DateEdit();
            this.ChkGroupMutation = new DevExpress.XtraEditors.CheckEdit();
            this.ChkNewEmp = new DevExpress.XtraEditors.CheckEdit();
            this.ChkDeptMutation = new DevExpress.XtraEditors.CheckEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.MeeJobDesk = new DevExpress.XtraEditors.MemoExEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.MeeOther = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtAmount = new DevExpress.XtraEditors.TextEdit();
            this.MeeWorkExperience = new DevExpress.XtraEditors.MemoExEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeOther2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtProcess = new DevExpress.XtraEditors.TextEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TcEmployeeRequest = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LuePICName = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPICName = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtSubOrdinat = new DevExpress.XtraEditors.TextEdit();
            this.TxtCandidate = new DevExpress.XtraEditors.TextEdit();
            this.TxtHead = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtExpectedSalaryAmt = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTerminate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMutated.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkResigned.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPromoted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRetired.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOther2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUseDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUseDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGroupMutation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewEmp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptMutation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeJobDesk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOther.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeWorkExperience.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOther2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcess.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TcEmployeeRequest)).BeginInit();
            this.TcEmployeeRequest.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICName.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubOrdinat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCandidate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpectedSalaryAmt.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 518);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(772, 518);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(84, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(111, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(5, 8);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(73, 14);
            this.label72.TabIndex = 9;
            this.label72.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(84, 5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(200, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(20, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 22;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(99, 28);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(276, 20);
            this.LueDeptCode.TabIndex = 23;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(99, 91);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(276, 20);
            this.MeeRemark.TabIndex = 29;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(46, 94);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 28;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 121);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(265, 14);
            this.label1.TabIndex = 30;
            this.label1.Text = "Requisition reason, because our employee";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTerminate
            // 
            this.ChkTerminate.Location = new System.Drawing.Point(30, 163);
            this.ChkTerminate.Name = "ChkTerminate";
            this.ChkTerminate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTerminate.Properties.Appearance.Options.UseFont = true;
            this.ChkTerminate.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTerminate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTerminate.Properties.Caption = " ";
            this.ChkTerminate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTerminate.Size = new System.Drawing.Size(20, 22);
            this.ChkTerminate.TabIndex = 33;
            this.ChkTerminate.ToolTip = "Remove filter";
            this.ChkTerminate.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTerminate.ToolTipTitle = "Run System";
            // 
            // ChkMutated
            // 
            this.ChkMutated.Location = new System.Drawing.Point(30, 184);
            this.ChkMutated.Name = "ChkMutated";
            this.ChkMutated.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkMutated.Properties.Appearance.Options.UseFont = true;
            this.ChkMutated.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkMutated.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkMutated.Properties.Caption = " ";
            this.ChkMutated.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkMutated.Size = new System.Drawing.Size(20, 22);
            this.ChkMutated.TabIndex = 35;
            this.ChkMutated.ToolTip = "Remove filter";
            this.ChkMutated.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkMutated.ToolTipTitle = "Run System";
            // 
            // ChkResigned
            // 
            this.ChkResigned.Location = new System.Drawing.Point(30, 142);
            this.ChkResigned.Name = "ChkResigned";
            this.ChkResigned.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkResigned.Properties.Appearance.Options.UseFont = true;
            this.ChkResigned.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkResigned.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkResigned.Properties.Caption = " ";
            this.ChkResigned.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkResigned.Size = new System.Drawing.Size(19, 22);
            this.ChkResigned.TabIndex = 31;
            this.ChkResigned.ToolTip = "Remove filter";
            this.ChkResigned.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkResigned.ToolTipTitle = "Run System";
            // 
            // ChkPromoted
            // 
            this.ChkPromoted.Location = new System.Drawing.Point(30, 205);
            this.ChkPromoted.Name = "ChkPromoted";
            this.ChkPromoted.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPromoted.Properties.Appearance.Options.UseFont = true;
            this.ChkPromoted.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPromoted.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPromoted.Properties.Caption = " ";
            this.ChkPromoted.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPromoted.Size = new System.Drawing.Size(20, 22);
            this.ChkPromoted.TabIndex = 37;
            this.ChkPromoted.ToolTip = "Remove filter";
            this.ChkPromoted.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPromoted.ToolTipTitle = "Run System";
            // 
            // LblResigned
            // 
            this.LblResigned.AutoSize = true;
            this.LblResigned.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblResigned.Location = new System.Drawing.Point(51, 145);
            this.LblResigned.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblResigned.Name = "LblResigned";
            this.LblResigned.Size = new System.Drawing.Size(56, 14);
            this.LblResigned.TabIndex = 32;
            this.LblResigned.Text = "Resigned";
            this.LblResigned.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblTerminate
            // 
            this.LblTerminate.AutoSize = true;
            this.LblTerminate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTerminate.Location = new System.Drawing.Point(51, 166);
            this.LblTerminate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTerminate.Name = "LblTerminate";
            this.LblTerminate.Size = new System.Drawing.Size(63, 14);
            this.LblTerminate.TabIndex = 34;
            this.LblTerminate.Text = "Terminate";
            this.LblTerminate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblMutated
            // 
            this.LblMutated.AutoSize = true;
            this.LblMutated.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMutated.Location = new System.Drawing.Point(51, 187);
            this.LblMutated.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblMutated.Name = "LblMutated";
            this.LblMutated.Size = new System.Drawing.Size(53, 14);
            this.LblMutated.TabIndex = 36;
            this.LblMutated.Text = "Mutated";
            this.LblMutated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPromoted
            // 
            this.LblPromoted.AutoSize = true;
            this.LblPromoted.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPromoted.Location = new System.Drawing.Point(51, 208);
            this.LblPromoted.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPromoted.Name = "LblPromoted";
            this.LblPromoted.Size = new System.Drawing.Size(61, 14);
            this.LblPromoted.TabIndex = 38;
            this.LblPromoted.Text = "Promoted";
            this.LblPromoted.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblNewPosition
            // 
            this.LblNewPosition.AutoSize = true;
            this.LblNewPosition.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNewPosition.Location = new System.Drawing.Point(51, 251);
            this.LblNewPosition.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblNewPosition.Name = "LblNewPosition";
            this.LblNewPosition.Size = new System.Drawing.Size(78, 14);
            this.LblNewPosition.TabIndex = 42;
            this.LblNewPosition.Text = "New Position";
            this.LblNewPosition.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblRetired
            // 
            this.LblRetired.AutoSize = true;
            this.LblRetired.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRetired.Location = new System.Drawing.Point(51, 230);
            this.LblRetired.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRetired.Name = "LblRetired";
            this.LblRetired.Size = new System.Drawing.Size(46, 14);
            this.LblRetired.TabIndex = 40;
            this.LblRetired.Text = "Retired";
            this.LblRetired.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkRetired
            // 
            this.ChkRetired.Location = new System.Drawing.Point(30, 227);
            this.ChkRetired.Name = "ChkRetired";
            this.ChkRetired.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRetired.Properties.Appearance.Options.UseFont = true;
            this.ChkRetired.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkRetired.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkRetired.Properties.Caption = " ";
            this.ChkRetired.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRetired.Size = new System.Drawing.Size(20, 22);
            this.ChkRetired.TabIndex = 39;
            this.ChkRetired.ToolTip = "Remove filter";
            this.ChkRetired.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkRetired.ToolTipTitle = "Run System";
            // 
            // ChkNewPosition
            // 
            this.ChkNewPosition.Location = new System.Drawing.Point(30, 248);
            this.ChkNewPosition.Name = "ChkNewPosition";
            this.ChkNewPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNewPosition.Properties.Appearance.Options.UseFont = true;
            this.ChkNewPosition.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkNewPosition.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkNewPosition.Properties.Caption = " ";
            this.ChkNewPosition.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNewPosition.Size = new System.Drawing.Size(20, 22);
            this.ChkNewPosition.TabIndex = 41;
            this.ChkNewPosition.ToolTip = "Remove filter";
            this.ChkNewPosition.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkNewPosition.ToolTipTitle = "Run System";
            // 
            // LblOther2
            // 
            this.LblOther2.AutoSize = true;
            this.LblOther2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOther2.Location = new System.Drawing.Point(51, 272);
            this.LblOther2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblOther2.Name = "LblOther2";
            this.LblOther2.Size = new System.Drawing.Size(63, 14);
            this.LblOther2.TabIndex = 44;
            this.LblOther2.Text = "Other . . .";
            this.LblOther2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkOther2
            // 
            this.ChkOther2.Location = new System.Drawing.Point(30, 269);
            this.ChkOther2.Name = "ChkOther2";
            this.ChkOther2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkOther2.Properties.Appearance.Options.UseFont = true;
            this.ChkOther2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkOther2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkOther2.Properties.Caption = " ";
            this.ChkOther2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkOther2.Size = new System.Drawing.Size(20, 22);
            this.ChkOther2.TabIndex = 43;
            this.ChkOther2.ToolTip = "Remove filter";
            this.ChkOther2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkOther2.ToolTipTitle = "Run System";
            this.ChkOther2.CheckedChanged += new System.EventHandler(this.ChkOther2_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(116, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevelCode
            // 
            this.LueLevelCode.EnterMoveNextControl = true;
            this.LueLevelCode.Location = new System.Drawing.Point(154, 57);
            this.LueLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelCode.Name = "LueLevelCode";
            this.LueLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelCode.Properties.DropDownRows = 30;
            this.LueLevelCode.Properties.MaxLength = 16;
            this.LueLevelCode.Properties.NullText = "[Empty]";
            this.LueLevelCode.Properties.PopupWidth = 300;
            this.LueLevelCode.Size = new System.Drawing.Size(257, 20);
            this.LueLevelCode.TabIndex = 24;
            this.LueLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelCode.EditValueChanged += new System.EventHandler(this.LueLevelCode_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(52, 241);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 14);
            this.label2.TabIndex = 53;
            this.label2.Text = "Experience Level";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCompetenceLevel
            // 
            this.LueCompetenceLevel.EnterMoveNextControl = true;
            this.LueCompetenceLevel.Location = new System.Drawing.Point(154, 238);
            this.LueCompetenceLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompetenceLevel.Name = "LueCompetenceLevel";
            this.LueCompetenceLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevel.Properties.Appearance.Options.UseFont = true;
            this.LueCompetenceLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompetenceLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompetenceLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompetenceLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompetenceLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompetenceLevel.Properties.DropDownRows = 30;
            this.LueCompetenceLevel.Properties.MaxLength = 1;
            this.LueCompetenceLevel.Properties.NullText = "[Empty]";
            this.LueCompetenceLevel.Properties.PopupWidth = 300;
            this.LueCompetenceLevel.Size = new System.Drawing.Size(257, 20);
            this.LueCompetenceLevel.TabIndex = 54;
            this.LueCompetenceLevel.ToolTip = "F4 : Show/hide list";
            this.LueCompetenceLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompetenceLevel.EditValueChanged += new System.EventHandler(this.LueCompetenceLevel_EditValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(219, 178);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(59, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "Years Old";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAge
            // 
            this.TxtAge.EnterMoveNextControl = true;
            this.TxtAge.Location = new System.Drawing.Point(154, 175);
            this.TxtAge.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAge.Name = "TxtAge";
            this.TxtAge.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAge.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAge.Properties.Appearance.Options.UseFont = true;
            this.TxtAge.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAge.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAge.Size = new System.Drawing.Size(62, 20);
            this.TxtAge.TabIndex = 47;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(81, 102);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 14);
            this.label26.TabIndex = 27;
            this.label26.Text = "Usage Date";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUseDt
            // 
            this.DteUseDt.EditValue = null;
            this.DteUseDt.EnterMoveNextControl = true;
            this.DteUseDt.Location = new System.Drawing.Point(154, 99);
            this.DteUseDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUseDt.Name = "DteUseDt";
            this.DteUseDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUseDt.Properties.Appearance.Options.UseFont = true;
            this.DteUseDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUseDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUseDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUseDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUseDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUseDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUseDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUseDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUseDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUseDt.Size = new System.Drawing.Size(111, 20);
            this.DteUseDt.TabIndex = 28;
            // 
            // ChkGroupMutation
            // 
            this.ChkGroupMutation.Location = new System.Drawing.Point(28, 380);
            this.ChkGroupMutation.Name = "ChkGroupMutation";
            this.ChkGroupMutation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkGroupMutation.Properties.Appearance.Options.UseFont = true;
            this.ChkGroupMutation.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkGroupMutation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkGroupMutation.Properties.Caption = " Group Mutation";
            this.ChkGroupMutation.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkGroupMutation.Size = new System.Drawing.Size(112, 22);
            this.ChkGroupMutation.TabIndex = 49;
            this.ChkGroupMutation.ToolTip = "Remove filter";
            this.ChkGroupMutation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkGroupMutation.ToolTipTitle = "Run System";
            // 
            // ChkNewEmp
            // 
            this.ChkNewEmp.Location = new System.Drawing.Point(28, 361);
            this.ChkNewEmp.Name = "ChkNewEmp";
            this.ChkNewEmp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNewEmp.Properties.Appearance.Options.UseFont = true;
            this.ChkNewEmp.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkNewEmp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkNewEmp.Properties.Caption = " New Employee";
            this.ChkNewEmp.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNewEmp.Size = new System.Drawing.Size(109, 22);
            this.ChkNewEmp.TabIndex = 48;
            this.ChkNewEmp.ToolTip = "Remove filter";
            this.ChkNewEmp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkNewEmp.ToolTipTitle = "Run System";
            // 
            // ChkDeptMutation
            // 
            this.ChkDeptMutation.Location = new System.Drawing.Point(28, 341);
            this.ChkDeptMutation.Name = "ChkDeptMutation";
            this.ChkDeptMutation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptMutation.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptMutation.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptMutation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptMutation.Properties.Caption = " Department Mutation";
            this.ChkDeptMutation.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptMutation.Size = new System.Drawing.Size(145, 22);
            this.ChkDeptMutation.TabIndex = 47;
            this.ChkDeptMutation.ToolTip = "Remove filter";
            this.ChkDeptMutation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptMutation.ToolTipTitle = "Run System";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(25, 322);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(139, 14);
            this.label21.TabIndex = 46;
            this.label21.Text = "Source of Requisition";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeJobDesk
            // 
            this.MeeJobDesk.EnterMoveNextControl = true;
            this.MeeJobDesk.Location = new System.Drawing.Point(24, 315);
            this.MeeJobDesk.Margin = new System.Windows.Forms.Padding(5);
            this.MeeJobDesk.Name = "MeeJobDesk";
            this.MeeJobDesk.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeJobDesk.Properties.Appearance.Options.UseFont = true;
            this.MeeJobDesk.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeJobDesk.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeJobDesk.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeJobDesk.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeJobDesk.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeJobDesk.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeJobDesk.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeJobDesk.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeJobDesk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeJobDesk.Properties.MaxLength = 1000;
            this.MeeJobDesk.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeJobDesk.Properties.ShowIcon = false;
            this.MeeJobDesk.Size = new System.Drawing.Size(387, 20);
            this.MeeJobDesk.TabIndex = 58;
            this.MeeJobDesk.ToolTip = "F4 : Show/hide text";
            this.MeeJobDesk.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeJobDesk.ToolTipTitle = "Run System";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(19, 290);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 14);
            this.label20.TabIndex = 57;
            this.label20.Text = "Job Description";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(19, 12);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(162, 14);
            this.label19.TabIndex = 20;
            this.label19.Text = "Requested Candidate For";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(112, 262);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 14);
            this.label18.TabIndex = 55;
            this.label18.Text = "Other";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(28, 81);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(123, 14);
            this.label16.TabIndex = 25;
            this.label16.Text = "Requisition Candidate";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeOther
            // 
            this.MeeOther.EnterMoveNextControl = true;
            this.MeeOther.Location = new System.Drawing.Point(154, 259);
            this.MeeOther.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOther.Name = "MeeOther";
            this.MeeOther.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther.Properties.Appearance.Options.UseFont = true;
            this.MeeOther.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOther.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOther.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOther.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOther.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOther.Properties.MaxLength = 400;
            this.MeeOther.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeOther.Properties.ShowIcon = false;
            this.MeeOther.Size = new System.Drawing.Size(257, 20);
            this.MeeOther.TabIndex = 56;
            this.MeeOther.ToolTip = "F4 : Show/hide text";
            this.MeeOther.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOther.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(51, 220);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 14);
            this.label11.TabIndex = 51;
            this.label11.Text = "Work Experience";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmount
            // 
            this.TxtAmount.EnterMoveNextControl = true;
            this.TxtAmount.Location = new System.Drawing.Point(154, 78);
            this.TxtAmount.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmount.Name = "TxtAmount";
            this.TxtAmount.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmount.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmount.Properties.Appearance.Options.UseFont = true;
            this.TxtAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmount.Size = new System.Drawing.Size(62, 20);
            this.TxtAmount.TabIndex = 26;
            // 
            // MeeWorkExperience
            // 
            this.MeeWorkExperience.EnterMoveNextControl = true;
            this.MeeWorkExperience.Location = new System.Drawing.Point(154, 217);
            this.MeeWorkExperience.Margin = new System.Windows.Forms.Padding(5);
            this.MeeWorkExperience.Name = "MeeWorkExperience";
            this.MeeWorkExperience.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWorkExperience.Properties.Appearance.Options.UseFont = true;
            this.MeeWorkExperience.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWorkExperience.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeWorkExperience.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWorkExperience.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeWorkExperience.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWorkExperience.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeWorkExperience.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWorkExperience.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeWorkExperience.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeWorkExperience.Properties.MaxLength = 1000;
            this.MeeWorkExperience.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeWorkExperience.Properties.ShowIcon = false;
            this.MeeWorkExperience.Size = new System.Drawing.Size(257, 20);
            this.MeeWorkExperience.TabIndex = 52;
            this.MeeWorkExperience.ToolTip = "F4 : Show/hide text";
            this.MeeWorkExperience.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeWorkExperience.ToolTipTitle = "Run System";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(19, 129);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(223, 14);
            this.label17.TabIndex = 29;
            this.label17.Text = "General Requirement for Candidate";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(102, 38);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 14);
            this.label15.TabIndex = 21;
            this.label15.Text = "Position";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(90, 199);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 14);
            this.label14.TabIndex = 49;
            this.label14.Text = "Education";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(154, 196);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 30;
            this.LueLevel.Properties.MaxLength = 1;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 300;
            this.LueLevel.Size = new System.Drawing.Size(257, 20);
            this.LueLevel.TabIndex = 50;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel.EditValueChanged += new System.EventHandler(this.LueEducation_EditValueChanged);
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(154, 36);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 30;
            this.LuePosCode.Properties.MaxLength = 16;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 300;
            this.LuePosCode.Size = new System.Drawing.Size(257, 20);
            this.LuePosCode.TabIndex = 22;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(122, 178);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 14);
            this.label13.TabIndex = 46;
            this.label13.Text = "Age";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(104, 157);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 30;
            this.label12.Text = "Gender";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(154, 154);
            this.LueGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 30;
            this.LueGender.Properties.MaxLength = 1;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 300;
            this.LueGender.Size = new System.Drawing.Size(257, 20);
            this.LueGender.TabIndex = 45;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGender.EditValueChanged += new System.EventHandler(this.LueGender_EditValueChanged);
            // 
            // MeeOther2
            // 
            this.MeeOther2.EnterMoveNextControl = true;
            this.MeeOther2.Location = new System.Drawing.Point(53, 292);
            this.MeeOther2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOther2.Name = "MeeOther2";
            this.MeeOther2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther2.Properties.Appearance.Options.UseFont = true;
            this.MeeOther2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOther2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOther2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOther2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOther2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOther2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOther2.Properties.MaxLength = 400;
            this.MeeOther2.Properties.PopupFormSize = new System.Drawing.Size(330, 20);
            this.MeeOther2.Properties.ShowIcon = false;
            this.MeeOther2.Size = new System.Drawing.Size(322, 20);
            this.MeeOther2.TabIndex = 45;
            this.MeeOther2.ToolTip = "F4 : Show/hide text";
            this.MeeOther2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOther2.ToolTipTitle = "Run System";
            this.MeeOther2.Validated += new System.EventHandler(this.MeeOther2_Validated);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(65, 10);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 14);
            this.label61.TabIndex = 20;
            this.label61.Text = "Site";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(99, 7);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(276, 20);
            this.LueSiteCode.TabIndex = 21;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(45, 29);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 14);
            this.label25.TabIndex = 11;
            this.label25.Text = "Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(403, 4);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(72, 22);
            this.ChkCancelInd.TabIndex = 16;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(45, 73);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 14);
            this.label28.TabIndex = 26;
            this.label28.Text = "Process";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProcess
            // 
            this.TxtProcess.EnterMoveNextControl = true;
            this.TxtProcess.Location = new System.Drawing.Point(99, 70);
            this.TxtProcess.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProcess.Name = "TxtProcess";
            this.TxtProcess.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProcess.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProcess.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProcess.Properties.Appearance.Options.UseFont = true;
            this.TxtProcess.Properties.MaxLength = 13;
            this.TxtProcess.Size = new System.Drawing.Size(153, 20);
            this.TxtProcess.TabIndex = 27;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TcEmployeeRequest);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 52);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 466);
            this.panel3.TabIndex = 54;
            // 
            // TcEmployeeRequest
            // 
            this.TcEmployeeRequest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcEmployeeRequest.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcEmployeeRequest.Location = new System.Drawing.Point(0, 0);
            this.TcEmployeeRequest.Name = "TcEmployeeRequest";
            this.TcEmployeeRequest.SelectedTabPage = this.Tp1;
            this.TcEmployeeRequest.Size = new System.Drawing.Size(772, 466);
            this.TcEmployeeRequest.TabIndex = 19;
            this.TcEmployeeRequest.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 438);
            this.Tp1.Text = "Requisition";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.LuePICName);
            this.panel5.Controls.Add(this.LblPICName);
            this.panel5.Controls.Add(this.ChkGroupMutation);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.ChkPromoted);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.ChkNewEmp);
            this.panel5.Controls.Add(this.TxtProcess);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.ChkDeptMutation);
            this.panel5.Controls.Add(this.ChkResigned);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.MeeOther2);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.ChkMutated);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Controls.Add(this.LblOther2);
            this.panel5.Controls.Add(this.LblMutated);
            this.panel5.Controls.Add(this.LblRetired);
            this.panel5.Controls.Add(this.ChkTerminate);
            this.panel5.Controls.Add(this.ChkRetired);
            this.panel5.Controls.Add(this.LblTerminate);
            this.panel5.Controls.Add(this.ChkOther2);
            this.panel5.Controls.Add(this.LblPromoted);
            this.panel5.Controls.Add(this.LblNewPosition);
            this.panel5.Controls.Add(this.LblResigned);
            this.panel5.Controls.Add(this.ChkNewPosition);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 438);
            this.panel5.TabIndex = 22;
            // 
            // LuePICName
            // 
            this.LuePICName.EnterMoveNextControl = true;
            this.LuePICName.Location = new System.Drawing.Point(99, 49);
            this.LuePICName.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICName.Name = "LuePICName";
            this.LuePICName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICName.Properties.Appearance.Options.UseFont = true;
            this.LuePICName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICName.Properties.DropDownRows = 30;
            this.LuePICName.Properties.MaxLength = 16;
            this.LuePICName.Properties.NullText = "[Empty]";
            this.LuePICName.Properties.PopupWidth = 300;
            this.LuePICName.Size = new System.Drawing.Size(276, 20);
            this.LuePICName.TabIndex = 25;
            this.LuePICName.ToolTip = "F4 : Show/hide list";
            this.LuePICName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePICName.EditValueChanged += new System.EventHandler(this.LuePICName_EditValueChanged);
            // 
            // LblPICName
            // 
            this.LblPICName.AutoSize = true;
            this.LblPICName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPICName.ForeColor = System.Drawing.Color.Red;
            this.LblPICName.Location = new System.Drawing.Point(68, 52);
            this.LblPICName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPICName.Name = "LblPICName";
            this.LblPICName.Size = new System.Drawing.Size(25, 14);
            this.LblPICName.TabIndex = 24;
            this.LblPICName.Text = "PIC";
            this.LblPICName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 438);
            this.Tp2.Text = "Candidate";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.TxtSubOrdinat);
            this.panel6.Controls.Add(this.TxtCandidate);
            this.panel6.Controls.Add(this.TxtHead);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.LueEmploymentStatus);
            this.panel6.Controls.Add(this.TxtExpectedSalaryAmt);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.LueLevelCode);
            this.panel6.Controls.Add(this.LueGender);
            this.panel6.Controls.Add(this.MeeJobDesk);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.LueCompetenceLevel);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.LuePosCode);
            this.panel6.Controls.Add(this.TxtAge);
            this.panel6.Controls.Add(this.LueLevel);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.DteUseDt);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.MeeWorkExperience);
            this.panel6.Controls.Add(this.TxtAmount);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.MeeOther);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 438);
            this.panel6.TabIndex = 22;
            // 
            // TxtSubOrdinat
            // 
            this.TxtSubOrdinat.EnterMoveNextControl = true;
            this.TxtSubOrdinat.Location = new System.Drawing.Point(154, 413);
            this.TxtSubOrdinat.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubOrdinat.Name = "TxtSubOrdinat";
            this.TxtSubOrdinat.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSubOrdinat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubOrdinat.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubOrdinat.Properties.Appearance.Options.UseFont = true;
            this.TxtSubOrdinat.Properties.MaxLength = 250;
            this.TxtSubOrdinat.Size = new System.Drawing.Size(257, 20);
            this.TxtSubOrdinat.TabIndex = 65;
            // 
            // TxtCandidate
            // 
            this.TxtCandidate.EnterMoveNextControl = true;
            this.TxtCandidate.Location = new System.Drawing.Point(154, 391);
            this.TxtCandidate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCandidate.Name = "TxtCandidate";
            this.TxtCandidate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCandidate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCandidate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCandidate.Properties.Appearance.Options.UseFont = true;
            this.TxtCandidate.Properties.MaxLength = 250;
            this.TxtCandidate.Size = new System.Drawing.Size(257, 20);
            this.TxtCandidate.TabIndex = 63;
            // 
            // TxtHead
            // 
            this.TxtHead.EnterMoveNextControl = true;
            this.TxtHead.Location = new System.Drawing.Point(154, 369);
            this.TxtHead.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHead.Name = "TxtHead";
            this.TxtHead.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHead.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHead.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHead.Properties.Appearance.Options.UseFont = true;
            this.TxtHead.Properties.MaxLength = 250;
            this.TxtHead.Size = new System.Drawing.Size(257, 20);
            this.TxtHead.TabIndex = 61;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(445, 59);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(114, 14);
            this.label24.TabIndex = 68;
            this.label24.Text = "Employment Status";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(465, 38);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 14);
            this.label23.TabIndex = 66;
            this.label23.Text = "Expected Salary";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(79, 416);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 14);
            this.label22.TabIndex = 64;
            this.label22.Text = "Sub Ordinat";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(90, 394);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 14);
            this.label10.TabIndex = 62;
            this.label10.Text = "Candidate";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(116, 372);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 60;
            this.label9.Text = "Head";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(561, 56);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 30;
            this.LueEmploymentStatus.Properties.MaxLength = 16;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 300;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(200, 20);
            this.LueEmploymentStatus.TabIndex = 69;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmploymentStatus.EditValueChanged += new System.EventHandler(this.LueEmploymentStatus_EditValueChanged);
            // 
            // TxtExpectedSalaryAmt
            // 
            this.TxtExpectedSalaryAmt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtExpectedSalaryAmt.EnterMoveNextControl = true;
            this.TxtExpectedSalaryAmt.Location = new System.Drawing.Point(561, 35);
            this.TxtExpectedSalaryAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExpectedSalaryAmt.Name = "TxtExpectedSalaryAmt";
            this.TxtExpectedSalaryAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExpectedSalaryAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExpectedSalaryAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExpectedSalaryAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtExpectedSalaryAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtExpectedSalaryAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtExpectedSalaryAmt.Size = new System.Drawing.Size(200, 20);
            this.TxtExpectedSalaryAmt.TabIndex = 67;
            this.TxtExpectedSalaryAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            this.TxtExpectedSalaryAmt.Validated += new System.EventHandler(this.TxtExpectedSalaryAmt_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 345);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(202, 14);
            this.label7.TabIndex = 59;
            this.label7.Text = "Position For The New Candidate";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd1);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 153);
            this.Tp3.Text = "Approval Information";
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 153);
            this.Grd1.TabIndex = 26;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.TxtDocNo);
            this.panel4.Controls.Add(this.label72);
            this.panel4.Controls.Add(this.DteDocDt);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 52);
            this.panel4.TabIndex = 8;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.TxtStatus);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.MeeCancelReason);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.ChkCancelInd);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(291, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(481, 52);
            this.panel7.TabIndex = 13;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(146, 27);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(106, 20);
            this.TxtStatus.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(100, 30);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 17;
            this.label6.Text = "Status";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(146, 5);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(250, 20);
            this.MeeCancelReason.TabIndex = 15;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "Reason For Cancellation";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmEmployeeRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 518);
            this.Name = "FrmEmployeeRequest";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTerminate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMutated.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkResigned.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPromoted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRetired.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOther2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUseDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUseDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkGroupMutation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewEmp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptMutation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeJobDesk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOther.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeWorkExperience.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOther2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcess.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TcEmployeeRequest)).EndInit();
            this.TcEmployeeRequest.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICName.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubOrdinat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCandidate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpectedSalaryAmt.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkTerminate;
        private DevExpress.XtraEditors.CheckEdit ChkMutated;
        private DevExpress.XtraEditors.CheckEdit ChkResigned;
        private DevExpress.XtraEditors.CheckEdit ChkPromoted;
        private System.Windows.Forms.Label LblPromoted;
        private System.Windows.Forms.Label LblMutated;
        private System.Windows.Forms.Label LblTerminate;
        private System.Windows.Forms.Label LblResigned;
        private System.Windows.Forms.Label LblNewPosition;
        private System.Windows.Forms.Label LblRetired;
        private DevExpress.XtraEditors.CheckEdit ChkRetired;
        private DevExpress.XtraEditors.CheckEdit ChkNewPosition;
        private System.Windows.Forms.Label LblOther2;
        private DevExpress.XtraEditors.CheckEdit ChkOther2;
        private DevExpress.XtraEditors.MemoExEdit MeeOther2;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtAmount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.MemoExEdit MeeOther;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeWorkExperience;
        private DevExpress.XtraEditors.MemoExEdit MeeJobDesk;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.CheckEdit ChkGroupMutation;
        private DevExpress.XtraEditors.CheckEdit ChkNewEmp;
        private DevExpress.XtraEditors.CheckEdit ChkDeptMutation;
        private System.Windows.Forms.Label label61;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.DateEdit DteUseDt;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtAge;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.TextEdit TxtProcess;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueCompetenceLevel;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueLevelCode;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl TcEmployeeRequest;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label6;
        protected TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.LookUpEdit LuePICName;
        private System.Windows.Forms.Label LblPICName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        internal DevExpress.XtraEditors.TextEdit TxtExpectedSalaryAmt;
        internal DevExpress.XtraEditors.TextEdit TxtSubOrdinat;
        internal DevExpress.XtraEditors.TextEdit TxtCandidate;
        internal DevExpress.XtraEditors.TextEdit TxtHead;
    }
}