﻿#region Update
/*
    22/09/2020 [TKG/HIN] tambah 4 angka di belakang koma utk persentase
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSSProgram : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSSPCode = string.Empty;
        internal FrmSSProgramFind FrmFind;

        #endregion

        #region Constructor

        public FrmSSProgram(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Social Security Program";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sl.SetLueOption(ref LueCCGrpCode, "CostCenterGroup");
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mSSPCode.Length != 0)
                {
                    ShowData(mSSPCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Social Security's" + Environment.NewLine + "Code",

                        //1-5
                        "Social Security's Name",
                        "Active",
                        "Employer %",
                        "Employee %",
                        "Total %"
                    },
                     new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        250, 80, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5 }, 4);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtSSPCode, TxtSSPName, ChkFamilyInd, LueCCGrpCode, MeeRemark }, true);
                    BtnAcNo1.Enabled = false;
                    TxtSSPCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtSSPCode, TxtSSPName, ChkFamilyInd, LueCCGrpCode, MeeRemark }, false);
                    BtnAcNo1.Enabled = true;
                    TxtSSPCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtSSPName, ChkFamilyInd, LueCCGrpCode, MeeRemark }, false);
                    BtnAcNo1.Enabled = true;
                    TxtSSPName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSSPCode, TxtSSPName, TxtAcNo1, TxtAcDesc1, LueCCGrpCode, MeeRemark
            });
            ChkFamilyInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSSProgramFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSSPCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (!TxtSSPCode.Properties.ReadOnly)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", "") == DialogResult.No || 
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSSProgram());
            Sm.ExecCommands(cml);
            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSSPCode, "Social security program's code", false) ||
                Sm.IsTxtEmpty(TxtSSPName, "Social security program's name", false) ||
                IsSSPCodeExisted() ||
                IsSSPNameExisted();
        }

        private bool IsSSPCodeExisted()
        {
            if (!TxtSSPCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select SSPCode From TblSSProgram Where SSPCode=@SSPCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@SSPCode", TxtSSPCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Social security program code ( " + TxtSSPCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsSSPNameExisted()
        {
            if (!TxtSSPName.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select SSPName From TblSSProgram Where SSPName=@SSPName " + 
                    (TxtSSPCode.Properties.ReadOnly?" And SSPCode<>@SSPCode ":string.Empty) + 
                    " Limit 1;"
                };
                if (TxtSSPCode.Properties.ReadOnly) Sm.CmParam<String>(ref cm, "@SSPCode", TxtSSPCode.Text);
                Sm.CmParam<String>(ref cm, "@SSPName", TxtSSPName.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Social security program name ( " + TxtSSPName.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveSSProgram()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSSProgram(SSPCode, SSPName, FamilyInd, CCGrpCode, AcNo1, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SSPCode, @SSPName, @FamilyInd, @CCGrpCode, @AcNo1, @Remark, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update SSPName=@SSPName, FamilyInd=@FamilyInd, CCGrpCode=@CCGrpCode, AcNo1=@AcNo1, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SSPCode", TxtSSPCode.Text);
            Sm.CmParam<String>(ref cm, "@SSPName", TxtSSPName.Text);
            Sm.CmParam<String>(ref cm, "@FamilyInd", ChkFamilyInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@AcNo1", TxtAcNo1.Text);
            Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCCGrpCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSSProgram());
            Sm.ExecCommands(cml);
            ShowData(TxtSSPCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtSSPCode, "Social security program code", false)||
                Sm.IsTxtEmpty(TxtSSPName, "Social security program name", false) ||
                IsSSPNameExisted();
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string SSPCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowSSProgram(SSPCode);
                ShowSS(SSPCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSSProgram(string SSPCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SSPCode", SSPCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.SSPCode, A.SSPName, A.FamilyInd, A.CCGrpCode, A.AcNo1, B.AcDesc As AcDesc1, A.Remark " +
                    "From TblSSProgram A " +
                    "Left Join TblCOA B On A.AcNo1=B.AcNo " +
                    "Where A.SSPCode=@SSPCode;",
                    new string[] 
                    { 
                        //0
                        "SSPCode", 
                        //1-5
                        "SSPName", "FamilyInd", "CCGrpCode", "AcNo1", "AcDesc1", 
                        //6
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSSPCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtSSPName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkFamilyInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                        Sm.SetLue(LueCCGrpCode, Sm.DrStr(dr, c[3]));
                        TxtAcNo1.EditValue = Sm.DrStr(dr, c[4]);
                        TxtAcDesc1.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowSS(string SSPCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SSPCode", SSPCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select SSCode, SSName, ActInd, EmployerPerc, EmployeePerc, TotalPerc ");
            SQL.AppendLine("From TblSS ");
            SQL.AppendLine("Where SSPCode=@SSPCode Order By SSName;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "SSCode", 
                    "SSName", "ActInd", "EmployerPerc", "EmployeePerc", "TotalPerc" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSSPCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSSPCode);
        }

        private void TxtSSPName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSSPName);
        }

        private void LueCCGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCCGrpCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
        }

        #endregion

        #region Button Event

        private void BtnAcNo1_Click(object sender, EventArgs e)
        {
            var f = new FrmCOAStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mIsChoose)
            {
                TxtAcNo1.EditValue = f.mAcNo;
                TxtAcDesc1.EditValue = f.mAcDesc;
            }
            f.Close();
            this.Focus();
        }

        #endregion

        #endregion
    }
}
