﻿namespace RunSystem
{
    partial class FrmSOContract3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSOContract3));
            this.panel4 = new System.Windows.Forms.Panel();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.BtnPRJIDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPRJIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.LueCustomerContactperson = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueJOType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.MeeProjectDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnBOQDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnContact = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtPICSales = new DevExpress.XtraEditors.TextEdit();
            this.TxtContractAmtBefTax = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtProfitMarginPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtProfitMarginAmt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtTotalResource = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.LueShpMCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.TxtContractAmtAfterTax = new DevExpress.XtraEditors.TextEdit();
            this.TxtSAName = new DevExpress.XtraEditors.TextEdit();
            this.BtnCustomerShipAddress = new DevExpress.XtraEditors.SimpleButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtAddress = new DevExpress.XtraEditors.TextEdit();
            this.TxtCity = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnCtShippingAddress = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.TxtCountry = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtCOAAmt = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpAdditional = new DevExpress.XtraTab.XtraTabPage();
            this.TcSOContract = new DevExpress.XtraTab.XtraTabControl();
            this.TpBOQ = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpItem = new DevExpress.XtraTab.XtraTabPage();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpResource = new DevExpress.XtraTab.XtraTabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtDirectPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtIndirectPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtRemunerationPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtIndirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtCostPerc = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TxtDirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtRemunerationCost = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.TpJO = new DevExpress.XtraTab.XtraTabPage();
            this.DteJODocDt = new DevExpress.XtraEditors.DateEdit();
            this.LueJOCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpBankGuarantee = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtBankGuaranteeNo = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.DteBankGuaranteeDt = new DevExpress.XtraEditors.DateEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpApproval = new DevExpress.XtraTab.XtraTabPage();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpUploadFile = new DevExpress.XtraTab.XtraTabPage();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPRJIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomerContactperson.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICSales.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtBefTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitMarginPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitMarginAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtAfterTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcSOContract)).BeginInit();
            this.TcSOContract.SuspendLayout();
            this.TpBOQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpResource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectCost.Properties)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).BeginInit();
            this.TpJO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpBankGuarantee.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankGuaranteeNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            this.TpApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpUploadFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(877, 0);
            this.panel1.Size = new System.Drawing.Size(70, 597);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcSOContract);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(877, 548);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 548);
            this.panel3.Size = new System.Drawing.Size(877, 49);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 575);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(877, 49);
            this.Grd1.TabIndex = 78;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.DteStartDt);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.DteEndDt);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.BtnPRJIDocNo);
            this.panel4.Controls.Add(this.TxtPRJIDocNo);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.LueCustomerContactperson);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.LueJOType);
            this.panel4.Controls.Add(this.TxtProjectName);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.MeeProjectDesc);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.TxtLocalDocNo);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.LueStatus);
            this.panel4.Controls.Add(this.BtnBOQDocNo);
            this.panel4.Controls.Add(this.BtnContact);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.TxtBOQDocNo);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.LueCtCode);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.DteDocDt);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtDocNo);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(877, 326);
            this.panel4.TabIndex = 11;
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(148, 301);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.MaxLength = 8;
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(117, 20);
            this.DteEndDt.TabIndex = 45;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(89, 304);
            this.label33.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(58, 14);
            this.label33.TabIndex = 44;
            this.label33.Text = "End Date";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPRJIDocNo
            // 
            this.BtnPRJIDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPRJIDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPRJIDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPRJIDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPRJIDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPRJIDocNo.Appearance.Options.UseFont = true;
            this.BtnPRJIDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPRJIDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPRJIDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPRJIDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPRJIDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPRJIDocNo.Image")));
            this.BtnPRJIDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPRJIDocNo.Location = new System.Drawing.Point(416, 152);
            this.BtnPRJIDocNo.Name = "BtnPRJIDocNo";
            this.BtnPRJIDocNo.Size = new System.Drawing.Size(13, 21);
            this.BtnPRJIDocNo.TabIndex = 30;
            this.BtnPRJIDocNo.ToolTip = "Show BOQ";
            this.BtnPRJIDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPRJIDocNo.ToolTipTitle = "Run System";
            // 
            // TxtPRJIDocNo
            // 
            this.TxtPRJIDocNo.EnterMoveNextControl = true;
            this.TxtPRJIDocNo.Location = new System.Drawing.Point(148, 153);
            this.TxtPRJIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPRJIDocNo.Name = "TxtPRJIDocNo";
            this.TxtPRJIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPRJIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPRJIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPRJIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPRJIDocNo.Properties.MaxLength = 16;
            this.TxtPRJIDocNo.Properties.ReadOnly = true;
            this.TxtPRJIDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtPRJIDocNo.TabIndex = 29;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(11, 156);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(136, 14);
            this.label31.TabIndex = 28;
            this.label31.Text = "Project Implementation";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCustomerContactperson
            // 
            this.LueCustomerContactperson.EnterMoveNextControl = true;
            this.LueCustomerContactperson.Location = new System.Drawing.Point(148, 174);
            this.LueCustomerContactperson.Margin = new System.Windows.Forms.Padding(5);
            this.LueCustomerContactperson.Name = "LueCustomerContactperson";
            this.LueCustomerContactperson.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomerContactperson.Properties.Appearance.Options.UseFont = true;
            this.LueCustomerContactperson.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomerContactperson.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCustomerContactperson.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomerContactperson.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCustomerContactperson.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomerContactperson.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCustomerContactperson.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomerContactperson.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCustomerContactperson.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCustomerContactperson.Properties.DropDownRows = 30;
            this.LueCustomerContactperson.Properties.NullText = "[Empty]";
            this.LueCustomerContactperson.Properties.PopupWidth = 300;
            this.LueCustomerContactperson.Size = new System.Drawing.Size(264, 20);
            this.LueCustomerContactperson.TabIndex = 32;
            this.LueCustomerContactperson.ToolTip = "F4 : Show/hide list";
            this.LueCustomerContactperson.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCustomerContactperson.EditValueChanged += new System.EventHandler(this.LueCustomerContactperson_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(56, 241);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 14);
            this.label24.TabIndex = 38;
            this.label24.Text = "Joint Operation";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueJOType
            // 
            this.LueJOType.EnterMoveNextControl = true;
            this.LueJOType.Location = new System.Drawing.Point(148, 238);
            this.LueJOType.Margin = new System.Windows.Forms.Padding(5);
            this.LueJOType.Name = "LueJOType";
            this.LueJOType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.Appearance.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJOType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJOType.Properties.DropDownRows = 30;
            this.LueJOType.Properties.NullText = "[Empty]";
            this.LueJOType.Properties.PopupWidth = 300;
            this.LueJOType.Size = new System.Drawing.Size(264, 20);
            this.LueJOType.TabIndex = 39;
            this.LueJOType.ToolTip = "F4 : Show/hide list";
            this.LueJOType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(148, 196);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 20;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(264, 20);
            this.TxtProjectName.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(66, 199);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 14);
            this.label16.TabIndex = 34;
            this.label16.Text = "Project Name";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeProjectDesc
            // 
            this.MeeProjectDesc.EnterMoveNextControl = true;
            this.MeeProjectDesc.Location = new System.Drawing.Point(148, 217);
            this.MeeProjectDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeProjectDesc.Name = "MeeProjectDesc";
            this.MeeProjectDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeProjectDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeProjectDesc.Properties.MaxLength = 5000;
            this.MeeProjectDesc.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeProjectDesc.Properties.ShowIcon = false;
            this.MeeProjectDesc.Size = new System.Drawing.Size(264, 20);
            this.MeeProjectDesc.TabIndex = 37;
            this.MeeProjectDesc.ToolTip = "F4 : Show/hide text";
            this.MeeProjectDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeProjectDesc.ToolTipTitle = "Run System";
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(352, 68);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 20;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(37, 221);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 14);
            this.label23.TabIndex = 36;
            this.label23.Text = "Project Description";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(62, 71);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 14);
            this.label27.TabIndex = 18;
            this.label27.Text = "Cancel Reason";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(148, 69);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 250;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(202, 20);
            this.MeeCancelReason.TabIndex = 19;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(148, 90);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 80;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtLocalDocNo.TabIndex = 22;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(52, 93);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 14);
            this.label20.TabIndex = 21;
            this.label20.Text = "Local Document";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(148, 48);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 20;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 500;
            this.LueStatus.Size = new System.Drawing.Size(202, 20);
            this.LueStatus.TabIndex = 17;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged_1);
            // 
            // BtnBOQDocNo
            // 
            this.BtnBOQDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBOQDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBOQDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOQDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBOQDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseFont = true;
            this.BtnBOQDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBOQDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBOQDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBOQDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBOQDocNo.Image")));
            this.BtnBOQDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBOQDocNo.Location = new System.Drawing.Point(416, 131);
            this.BtnBOQDocNo.Name = "BtnBOQDocNo";
            this.BtnBOQDocNo.Size = new System.Drawing.Size(13, 21);
            this.BtnBOQDocNo.TabIndex = 27;
            this.BtnBOQDocNo.ToolTip = "Show BOQ";
            this.BtnBOQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBOQDocNo.ToolTipTitle = "Run System";
            this.BtnBOQDocNo.Click += new System.EventHandler(this.BtnBOQDocNo_Click);
            // 
            // BtnContact
            // 
            this.BtnContact.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnContact.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnContact.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnContact.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnContact.Appearance.Options.UseBackColor = true;
            this.BtnContact.Appearance.Options.UseFont = true;
            this.BtnContact.Appearance.Options.UseForeColor = true;
            this.BtnContact.Appearance.Options.UseTextOptions = true;
            this.BtnContact.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnContact.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnContact.Image = ((System.Drawing.Image)(resources.GetObject("BtnContact.Image")));
            this.BtnContact.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnContact.Location = new System.Drawing.Point(415, 174);
            this.BtnContact.Name = "BtnContact";
            this.BtnContact.Size = new System.Drawing.Size(14, 21);
            this.BtnContact.TabIndex = 33;
            this.BtnContact.ToolTip = "Add Contact Person";
            this.BtnContact.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnContact.ToolTipTitle = "Run System";
            this.BtnContact.Click += new System.EventHandler(this.BtnContact_Click);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(148, 259);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(264, 20);
            this.MeeRemark.TabIndex = 41;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(0, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 14);
            this.label4.TabIndex = 31;
            this.label4.Text = "Customer Contact Person";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(100, 261);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(148, 132);
            this.TxtBOQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 16;
            this.TxtBOQDocNo.Properties.ReadOnly = true;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtBOQDocNo.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(61, 134);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Bill of Quantity";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(88, 114);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(148, 111);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(264, 20);
            this.LueCtCode.TabIndex = 24;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(54, 50);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 14);
            this.label12.TabIndex = 16;
            this.label12.Text = "Approval Status";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(148, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(125, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(114, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(148, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(264, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(74, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.TxtPICSales);
            this.panel6.Controls.Add(this.TxtContractAmtBefTax);
            this.panel6.Controls.Add(this.label34);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Controls.Add(this.TxtProfitMarginPerc);
            this.panel6.Controls.Add(this.TxtProfitMarginAmt);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtTotalResource);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.LueShpMCode);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.TxtMobile);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.TxtEmail);
            this.panel6.Controls.Add(this.TxtContractAmtAfterTax);
            this.panel6.Controls.Add(this.TxtSAName);
            this.panel6.Controls.Add(this.BtnCustomerShipAddress);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtAddress);
            this.panel6.Controls.Add(this.TxtCity);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.TxtPostalCd);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.BtnCtShippingAddress);
            this.panel6.Controls.Add(this.TxtPhone);
            this.panel6.Controls.Add(this.TxtCountry);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(432, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(445, 326);
            this.panel6.TabIndex = 44;
            // 
            // TxtPICSales
            // 
            this.TxtPICSales.EnterMoveNextControl = true;
            this.TxtPICSales.Location = new System.Drawing.Point(173, 153);
            this.TxtPICSales.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICSales.Name = "TxtPICSales";
            this.TxtPICSales.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPICSales.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICSales.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICSales.Properties.Appearance.Options.UseFont = true;
            this.TxtPICSales.Properties.MaxLength = 40;
            this.TxtPICSales.Properties.ReadOnly = true;
            this.TxtPICSales.Size = new System.Drawing.Size(263, 20);
            this.TxtPICSales.TabIndex = 64;
            // 
            // TxtContractAmtBefTax
            // 
            this.TxtContractAmtBefTax.EnterMoveNextControl = true;
            this.TxtContractAmtBefTax.Location = new System.Drawing.Point(173, 195);
            this.TxtContractAmtBefTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmtBefTax.Name = "TxtContractAmtBefTax";
            this.TxtContractAmtBefTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmtBefTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmtBefTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmtBefTax.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmtBefTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmtBefTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmtBefTax.Properties.ReadOnly = true;
            this.TxtContractAmtBefTax.Size = new System.Drawing.Size(263, 20);
            this.TxtContractAmtBefTax.TabIndex = 68;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(0, 198);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(172, 14);
            this.label34.TabIndex = 67;
            this.label34.Text = "Contract Amount(Before Tax)";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(415, 261);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(19, 14);
            this.label32.TabIndex = 76;
            this.label32.Text = "%";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProfitMarginPerc
            // 
            this.TxtProfitMarginPerc.EnterMoveNextControl = true;
            this.TxtProfitMarginPerc.Location = new System.Drawing.Point(350, 258);
            this.TxtProfitMarginPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProfitMarginPerc.Name = "TxtProfitMarginPerc";
            this.TxtProfitMarginPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProfitMarginPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProfitMarginPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProfitMarginPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtProfitMarginPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtProfitMarginPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtProfitMarginPerc.Properties.ReadOnly = true;
            this.TxtProfitMarginPerc.Size = new System.Drawing.Size(61, 20);
            this.TxtProfitMarginPerc.TabIndex = 75;
            // 
            // TxtProfitMarginAmt
            // 
            this.TxtProfitMarginAmt.EnterMoveNextControl = true;
            this.TxtProfitMarginAmt.Location = new System.Drawing.Point(173, 258);
            this.TxtProfitMarginAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProfitMarginAmt.Name = "TxtProfitMarginAmt";
            this.TxtProfitMarginAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProfitMarginAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProfitMarginAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProfitMarginAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtProfitMarginAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtProfitMarginAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtProfitMarginAmt.Properties.ReadOnly = true;
            this.TxtProfitMarginAmt.Size = new System.Drawing.Size(174, 20);
            this.TxtProfitMarginAmt.TabIndex = 74;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(97, 261);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 14);
            this.label6.TabIndex = 73;
            this.label6.Text = "Profit Margin";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalResource
            // 
            this.TxtTotalResource.EnterMoveNextControl = true;
            this.TxtTotalResource.Location = new System.Drawing.Point(173, 237);
            this.TxtTotalResource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalResource.Name = "TxtTotalResource";
            this.TxtTotalResource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalResource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalResource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalResource.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalResource.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalResource.Properties.ReadOnly = true;
            this.TxtTotalResource.Size = new System.Drawing.Size(263, 20);
            this.TxtTotalResource.TabIndex = 72;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(83, 241);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(89, 14);
            this.label30.TabIndex = 71;
            this.label30.Text = "Total Resource";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueShpMCode
            // 
            this.LueShpMCode.EnterMoveNextControl = true;
            this.LueShpMCode.Location = new System.Drawing.Point(173, 174);
            this.LueShpMCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueShpMCode.Name = "LueShpMCode";
            this.LueShpMCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.Appearance.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShpMCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShpMCode.Properties.DropDownRows = 30;
            this.LueShpMCode.Properties.NullText = "[Empty]";
            this.LueShpMCode.Properties.PopupWidth = 300;
            this.LueShpMCode.Size = new System.Drawing.Size(263, 20);
            this.LueShpMCode.TabIndex = 66;
            this.LueShpMCode.ToolTip = "F4 : Show/hide list";
            this.LueShpMCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShpMCode.EditValueChanged += new System.EventHandler(this.LueShpMCode_EditValueChanged_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(73, 178);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 14);
            this.label22.TabIndex = 65;
            this.label22.Text = "Shipping Method";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(116, 156);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 14);
            this.label21.TabIndex = 63;
            this.label21.Text = "PIC Sales";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(173, 132);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Properties.ReadOnly = true;
            this.TxtMobile.Size = new System.Drawing.Size(263, 20);
            this.TxtMobile.TabIndex = 62;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(131, 134);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 14);
            this.label17.TabIndex = 61;
            this.label17.Text = "Mobile";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(138, 113);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 14);
            this.label5.TabIndex = 59;
            this.label5.Text = "Email";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(173, 111);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Properties.ReadOnly = true;
            this.TxtEmail.Size = new System.Drawing.Size(263, 20);
            this.TxtEmail.TabIndex = 60;
            // 
            // TxtContractAmtAfterTax
            // 
            this.TxtContractAmtAfterTax.EnterMoveNextControl = true;
            this.TxtContractAmtAfterTax.Location = new System.Drawing.Point(173, 216);
            this.TxtContractAmtAfterTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmtAfterTax.Name = "TxtContractAmtAfterTax";
            this.TxtContractAmtAfterTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmtAfterTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmtAfterTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmtAfterTax.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmtAfterTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmtAfterTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmtAfterTax.Properties.ReadOnly = true;
            this.TxtContractAmtAfterTax.Size = new System.Drawing.Size(263, 20);
            this.TxtContractAmtAfterTax.TabIndex = 70;
            // 
            // TxtSAName
            // 
            this.TxtSAName.EnterMoveNextControl = true;
            this.TxtSAName.Location = new System.Drawing.Point(173, 6);
            this.TxtSAName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSAName.Name = "TxtSAName";
            this.TxtSAName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSAName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSAName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSAName.Properties.Appearance.Options.UseFont = true;
            this.TxtSAName.Properties.MaxLength = 20;
            this.TxtSAName.Properties.ReadOnly = true;
            this.TxtSAName.Size = new System.Drawing.Size(212, 20);
            this.TxtSAName.TabIndex = 47;
            // 
            // BtnCustomerShipAddress
            // 
            this.BtnCustomerShipAddress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCustomerShipAddress.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCustomerShipAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCustomerShipAddress.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCustomerShipAddress.Appearance.Options.UseBackColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseFont = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseForeColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseTextOptions = true;
            this.BtnCustomerShipAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCustomerShipAddress.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCustomerShipAddress.Image = ((System.Drawing.Image)(resources.GetObject("BtnCustomerShipAddress.Image")));
            this.BtnCustomerShipAddress.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCustomerShipAddress.Location = new System.Drawing.Point(384, 6);
            this.BtnCustomerShipAddress.Name = "BtnCustomerShipAddress";
            this.BtnCustomerShipAddress.Size = new System.Drawing.Size(24, 21);
            this.BtnCustomerShipAddress.TabIndex = 48;
            this.BtnCustomerShipAddress.ToolTip = "Find Customer\'s Shipping Information";
            this.BtnCustomerShipAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCustomerShipAddress.ToolTipTitle = "Run System";
            this.BtnCustomerShipAddress.Click += new System.EventHandler(this.BtnCustomerShipAddress_Click_1);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(130, 93);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 57;
            this.label14.Text = "Phone";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(101, 71);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 14);
            this.label13.TabIndex = 55;
            this.label13.Text = "Postal Code";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAddress
            // 
            this.TxtAddress.EnterMoveNextControl = true;
            this.TxtAddress.Location = new System.Drawing.Point(173, 27);
            this.TxtAddress.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAddress.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAddress.Properties.Appearance.Options.UseFont = true;
            this.TxtAddress.Properties.MaxLength = 20;
            this.TxtAddress.Properties.ReadOnly = true;
            this.TxtAddress.Size = new System.Drawing.Size(263, 20);
            this.TxtAddress.TabIndex = 51;
            // 
            // TxtCity
            // 
            this.TxtCity.EnterMoveNextControl = true;
            this.TxtCity.Location = new System.Drawing.Point(301, 48);
            this.TxtCity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCity.Properties.Appearance.Options.UseFont = true;
            this.TxtCity.Properties.MaxLength = 20;
            this.TxtCity.Properties.ReadOnly = true;
            this.TxtCity.Size = new System.Drawing.Size(135, 20);
            this.TxtCity.TabIndex = 54;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(8, 219);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(164, 14);
            this.label9.TabIndex = 69;
            this.label9.Text = "Contract Amount(After Tax)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(173, 69);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Properties.ReadOnly = true;
            this.TxtPostalCd.Size = new System.Drawing.Size(263, 20);
            this.TxtPostalCd.TabIndex = 56;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(122, 30);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 50;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(94, 51);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 14);
            this.label10.TabIndex = 52;
            this.label10.Text = "Country, City";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCtShippingAddress
            // 
            this.BtnCtShippingAddress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtShippingAddress.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtShippingAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtShippingAddress.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtShippingAddress.Appearance.Options.UseBackColor = true;
            this.BtnCtShippingAddress.Appearance.Options.UseFont = true;
            this.BtnCtShippingAddress.Appearance.Options.UseForeColor = true;
            this.BtnCtShippingAddress.Appearance.Options.UseTextOptions = true;
            this.BtnCtShippingAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtShippingAddress.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtShippingAddress.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtShippingAddress.Image")));
            this.BtnCtShippingAddress.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtShippingAddress.Location = new System.Drawing.Point(411, 6);
            this.BtnCtShippingAddress.Name = "BtnCtShippingAddress";
            this.BtnCtShippingAddress.Size = new System.Drawing.Size(24, 21);
            this.BtnCtShippingAddress.TabIndex = 49;
            this.BtnCtShippingAddress.ToolTip = "Add Customer Shipping Address";
            this.BtnCtShippingAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtShippingAddress.ToolTipTitle = "Run System";
            this.BtnCtShippingAddress.Click += new System.EventHandler(this.BtnCtShippingAddress_Click_1);
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(173, 90);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Properties.ReadOnly = true;
            this.TxtPhone.Size = new System.Drawing.Size(263, 20);
            this.TxtPhone.TabIndex = 58;
            // 
            // TxtCountry
            // 
            this.TxtCountry.EnterMoveNextControl = true;
            this.TxtCountry.Location = new System.Drawing.Point(173, 48);
            this.TxtCountry.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCountry.Name = "TxtCountry";
            this.TxtCountry.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCountry.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCountry.Properties.Appearance.Options.UseFont = true;
            this.TxtCountry.Properties.MaxLength = 20;
            this.TxtCountry.Properties.ReadOnly = true;
            this.TxtCountry.Size = new System.Drawing.Size(125, 20);
            this.TxtCountry.TabIndex = 53;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(84, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 14);
            this.label11.TabIndex = 46;
            this.label11.Text = "Shipping Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(766, 289);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 238);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(766, 51);
            this.panel7.TabIndex = 25;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(536, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(230, 51);
            this.panel8.TabIndex = 29;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(6, 8);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 14);
            this.label28.TabIndex = 30;
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCOAAmt
            // 
            this.TxtCOAAmt.EnterMoveNextControl = true;
            this.TxtCOAAmt.Location = new System.Drawing.Point(63, 5);
            this.TxtCOAAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCOAAmt.Name = "TxtCOAAmt";
            this.TxtCOAAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCOAAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCOAAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCOAAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCOAAmt.Properties.ReadOnly = true;
            this.TxtCOAAmt.Size = new System.Drawing.Size(160, 20);
            this.TxtCOAAmt.TabIndex = 31;
            this.TxtCOAAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(8, 8);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 14);
            this.label18.TabIndex = 26;
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo2
            // 
            this.TxtVoucherDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherDocNo2.Location = new System.Drawing.Point(124, 26);
            this.TxtVoucherDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo2.Name = "TxtVoucherDocNo2";
            this.TxtVoucherDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherDocNo2.TabIndex = 49;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(57, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 28;
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherRequestDocNo2
            // 
            this.TxtVoucherRequestDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo2.Location = new System.Drawing.Point(124, 5);
            this.TxtVoucherRequestDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo2.Name = "TxtVoucherRequestDocNo2";
            this.TxtVoucherRequestDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherRequestDocNo2.TabIndex = 27;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 19;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 238);
            this.Grd4.TabIndex = 24;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpAdditional
            // 
            this.TpAdditional.Appearance.Header.Options.UseTextOptions = true;
            this.TpAdditional.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAdditional.Name = "TpAdditional";
            this.TpAdditional.Size = new System.Drawing.Size(766, 289);
            // 
            // TcSOContract
            // 
            this.TcSOContract.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcSOContract.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcSOContract.Location = new System.Drawing.Point(0, 326);
            this.TcSOContract.Name = "TcSOContract";
            this.TcSOContract.SelectedTabPage = this.TpBOQ;
            this.TcSOContract.Size = new System.Drawing.Size(877, 222);
            this.TcSOContract.TabIndex = 76;
            this.TcSOContract.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpBOQ,
            this.TpItem,
            this.TpResource,
            this.TpJO,
            this.TpBankGuarantee,
            this.TpApproval,
            this.TpUploadFile});
            // 
            // TpBOQ
            // 
            this.TpBOQ.Appearance.Header.Options.UseTextOptions = true;
            this.TpBOQ.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBOQ.Controls.Add(this.Grd2);
            this.TpBOQ.Name = "TpBOQ";
            this.TpBOQ.Size = new System.Drawing.Size(871, 194);
            this.TpBOQ.Text = "List of Item Bill of Quantity";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(871, 216);
            this.Grd2.TabIndex = 77;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            // 
            // TpItem
            // 
            this.TpItem.Appearance.Header.Options.UseTextOptions = true;
            this.TpItem.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpItem.Controls.Add(this.LueTaxCode2);
            this.TpItem.Controls.Add(this.LueTaxCode1);
            this.TpItem.Controls.Add(this.Grd3);
            this.TpItem.Name = "TpItem";
            this.TpItem.Size = new System.Drawing.Size(766, 216);
            this.TpItem.Text = "Revenue Item";
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(359, 23);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 30;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 200;
            this.LueTaxCode2.Size = new System.Drawing.Size(125, 20);
            this.LueTaxCode2.TabIndex = 79;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            this.LueTaxCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTaxCode2_KeyDown);
            this.LueTaxCode2.Leave += new System.EventHandler(this.LueTaxCode2_Leave);
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(224, 23);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 30;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 200;
            this.LueTaxCode1.Size = new System.Drawing.Size(125, 20);
            this.LueTaxCode1.TabIndex = 78;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode1.EditValueChanged += new System.EventHandler(this.LueTaxCode1_EditValueChanged);
            this.LueTaxCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTaxCode1_KeyDown);
            this.LueTaxCode1.Leave += new System.EventHandler(this.LueTaxCode1_Leave);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 216);
            this.Grd3.TabIndex = 77;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpResource
            // 
            this.TpResource.Appearance.Header.Options.UseTextOptions = true;
            this.TpResource.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpResource.Controls.Add(this.Grd6);
            this.TpResource.Controls.Add(this.panel9);
            this.TpResource.Name = "TpResource";
            this.TpResource.Size = new System.Drawing.Size(766, 216);
            this.TpResource.Text = "Resource";
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 93);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 123);
            this.Grd6.TabIndex = 98;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.label47);
            this.panel9.Controls.Add(this.label38);
            this.panel9.Controls.Add(this.label37);
            this.panel9.Controls.Add(this.label36);
            this.panel9.Controls.Add(this.TxtDirectPerc);
            this.panel9.Controls.Add(this.TxtIndirectPerc);
            this.panel9.Controls.Add(this.TxtTotalPerc);
            this.panel9.Controls.Add(this.TxtRemunerationPerc);
            this.panel9.Controls.Add(this.TxtIndirectCost);
            this.panel9.Controls.Add(this.label35);
            this.panel9.Controls.Add(this.panel11);
            this.panel9.Controls.Add(this.TxtTotal);
            this.panel9.Controls.Add(this.label44);
            this.panel9.Controls.Add(this.TxtDirectCost);
            this.panel9.Controls.Add(this.label45);
            this.panel9.Controls.Add(this.TxtRemunerationCost);
            this.panel9.Controls.Add(this.label46);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(766, 93);
            this.panel9.TabIndex = 77;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(329, 71);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(19, 14);
            this.label47.TabIndex = 93;
            this.label47.Text = "%";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(329, 48);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 14);
            this.label38.TabIndex = 89;
            this.label38.Text = "%";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(329, 29);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 14);
            this.label37.TabIndex = 85;
            this.label37.Text = "%";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(329, 8);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(19, 14);
            this.label36.TabIndex = 81;
            this.label36.Text = "%";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectPerc
            // 
            this.TxtDirectPerc.EnterMoveNextControl = true;
            this.TxtDirectPerc.Location = new System.Drawing.Point(274, 27);
            this.TxtDirectPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectPerc.Name = "TxtDirectPerc";
            this.TxtDirectPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDirectPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectPerc.Properties.MaxLength = 30;
            this.TxtDirectPerc.Properties.ReadOnly = true;
            this.TxtDirectPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtDirectPerc.TabIndex = 84;
            // 
            // TxtIndirectPerc
            // 
            this.TxtIndirectPerc.EnterMoveNextControl = true;
            this.TxtIndirectPerc.Location = new System.Drawing.Point(274, 48);
            this.TxtIndirectPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndirectPerc.Name = "TxtIndirectPerc";
            this.TxtIndirectPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIndirectPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndirectPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndirectPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtIndirectPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtIndirectPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtIndirectPerc.Properties.MaxLength = 30;
            this.TxtIndirectPerc.Properties.ReadOnly = true;
            this.TxtIndirectPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtIndirectPerc.TabIndex = 88;
            // 
            // TxtTotalPerc
            // 
            this.TxtTotalPerc.EnterMoveNextControl = true;
            this.TxtTotalPerc.Location = new System.Drawing.Point(274, 69);
            this.TxtTotalPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPerc.Name = "TxtTotalPerc";
            this.TxtTotalPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPerc.Properties.MaxLength = 30;
            this.TxtTotalPerc.Properties.ReadOnly = true;
            this.TxtTotalPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtTotalPerc.TabIndex = 92;
            // 
            // TxtRemunerationPerc
            // 
            this.TxtRemunerationPerc.EnterMoveNextControl = true;
            this.TxtRemunerationPerc.Location = new System.Drawing.Point(274, 6);
            this.TxtRemunerationPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemunerationPerc.Name = "TxtRemunerationPerc";
            this.TxtRemunerationPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemunerationPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemunerationPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemunerationPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtRemunerationPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemunerationPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemunerationPerc.Properties.MaxLength = 30;
            this.TxtRemunerationPerc.Properties.ReadOnly = true;
            this.TxtRemunerationPerc.Size = new System.Drawing.Size(52, 20);
            this.TxtRemunerationPerc.TabIndex = 80;
            // 
            // TxtIndirectCost
            // 
            this.TxtIndirectCost.EnterMoveNextControl = true;
            this.TxtIndirectCost.Location = new System.Drawing.Point(116, 48);
            this.TxtIndirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndirectCost.Name = "TxtIndirectCost";
            this.TxtIndirectCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIndirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtIndirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtIndirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtIndirectCost.Properties.MaxLength = 30;
            this.TxtIndirectCost.Properties.ReadOnly = true;
            this.TxtIndirectCost.Size = new System.Drawing.Size(156, 20);
            this.TxtIndirectCost.TabIndex = 87;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(38, 51);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 14);
            this.label35.TabIndex = 86;
            this.label35.Text = "Indirect Cost";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.label40);
            this.panel11.Controls.Add(this.label41);
            this.panel11.Controls.Add(this.TxtCostPerc);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(337, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(429, 93);
            this.panel11.TabIndex = 94;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(403, 10);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(19, 14);
            this.label40.TabIndex = 97;
            this.label40.Text = "%";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(146, 9);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(102, 14);
            this.label41.TabIndex = 95;
            this.label41.Text = "Cost Percentage ";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TxtCostPerc
            // 
            this.TxtCostPerc.EnterMoveNextControl = true;
            this.TxtCostPerc.Location = new System.Drawing.Point(251, 7);
            this.TxtCostPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostPerc.Name = "TxtCostPerc";
            this.TxtCostPerc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCostPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtCostPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCostPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCostPerc.Properties.MaxLength = 30;
            this.TxtCostPerc.Properties.ReadOnly = true;
            this.TxtCostPerc.Size = new System.Drawing.Size(149, 20);
            this.TxtCostPerc.TabIndex = 96;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(116, 69);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.MaxLength = 30;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(156, 20);
            this.TxtTotal.TabIndex = 91;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(80, 73);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 14);
            this.label44.TabIndex = 90;
            this.label44.Text = "Total";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDirectCost
            // 
            this.TxtDirectCost.EnterMoveNextControl = true;
            this.TxtDirectCost.Location = new System.Drawing.Point(116, 27);
            this.TxtDirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectCost.Name = "TxtDirectCost";
            this.TxtDirectCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectCost.Properties.MaxLength = 30;
            this.TxtDirectCost.Properties.ReadOnly = true;
            this.TxtDirectCost.Size = new System.Drawing.Size(156, 20);
            this.TxtDirectCost.TabIndex = 83;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(48, 30);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 14);
            this.label45.TabIndex = 82;
            this.label45.Text = "Direct Cost";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemunerationCost
            // 
            this.TxtRemunerationCost.EnterMoveNextControl = true;
            this.TxtRemunerationCost.Location = new System.Drawing.Point(116, 6);
            this.TxtRemunerationCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemunerationCost.Name = "TxtRemunerationCost";
            this.TxtRemunerationCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemunerationCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemunerationCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseFont = true;
            this.TxtRemunerationCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemunerationCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemunerationCost.Properties.MaxLength = 30;
            this.TxtRemunerationCost.Properties.ReadOnly = true;
            this.TxtRemunerationCost.Size = new System.Drawing.Size(156, 20);
            this.TxtRemunerationCost.TabIndex = 79;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(32, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(83, 14);
            this.label46.TabIndex = 78;
            this.label46.Text = "Remuneration";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpJO
            // 
            this.TpJO.Appearance.Header.Options.UseTextOptions = true;
            this.TpJO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpJO.Controls.Add(this.DteJODocDt);
            this.TpJO.Controls.Add(this.LueJOCode);
            this.TpJO.Controls.Add(this.Grd5);
            this.TpJO.Name = "TpJO";
            this.TpJO.Size = new System.Drawing.Size(766, 216);
            this.TpJO.Text = "Joint Operation";
            // 
            // DteJODocDt
            // 
            this.DteJODocDt.EditValue = null;
            this.DteJODocDt.EnterMoveNextControl = true;
            this.DteJODocDt.Location = new System.Drawing.Point(352, 27);
            this.DteJODocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJODocDt.Name = "DteJODocDt";
            this.DteJODocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJODocDt.Properties.Appearance.Options.UseFont = true;
            this.DteJODocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJODocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJODocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJODocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJODocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJODocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJODocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJODocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJODocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJODocDt.Size = new System.Drawing.Size(125, 20);
            this.DteJODocDt.TabIndex = 78;
            this.DteJODocDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteJODocDt_KeyDown);
            this.DteJODocDt.Leave += new System.EventHandler(this.DteJODocDt_Leave);
            // 
            // LueJOCode
            // 
            this.LueJOCode.EnterMoveNextControl = true;
            this.LueJOCode.Location = new System.Drawing.Point(79, 26);
            this.LueJOCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueJOCode.Name = "LueJOCode";
            this.LueJOCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.Appearance.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJOCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJOCode.Properties.DropDownRows = 30;
            this.LueJOCode.Properties.NullText = "[Empty]";
            this.LueJOCode.Properties.PopupWidth = 300;
            this.LueJOCode.Size = new System.Drawing.Size(264, 20);
            this.LueJOCode.TabIndex = 77;
            this.LueJOCode.ToolTip = "F4 : Show/hide list";
            this.LueJOCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueJOCode.EditValueChanged += new System.EventHandler(this.LueJOCode_EditValueChanged);
            this.LueJOCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueJOCode_KeyDown);
            this.LueJOCode.Leave += new System.EventHandler(this.LueJOCode_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 216);
            this.Grd5.TabIndex = 79;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpBankGuarantee
            // 
            this.TpBankGuarantee.Appearance.Header.Options.UseTextOptions = true;
            this.TpBankGuarantee.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBankGuarantee.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpBankGuarantee.Appearance.PageClient.Options.UseBackColor = true;
            this.TpBankGuarantee.Controls.Add(this.panel5);
            this.TpBankGuarantee.Name = "TpBankGuarantee";
            this.TpBankGuarantee.Size = new System.Drawing.Size(766, 216);
            this.TpBankGuarantee.Text = "Bank Guarantee";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtBankGuaranteeNo);
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.DteBankGuaranteeDt);
            this.panel5.Controls.Add(this.label26);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.LueBankCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 216);
            this.panel5.TabIndex = 77;
            // 
            // TxtBankGuaranteeNo
            // 
            this.TxtBankGuaranteeNo.EnterMoveNextControl = true;
            this.TxtBankGuaranteeNo.Location = new System.Drawing.Point(118, 5);
            this.TxtBankGuaranteeNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankGuaranteeNo.Name = "TxtBankGuaranteeNo";
            this.TxtBankGuaranteeNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankGuaranteeNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankGuaranteeNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankGuaranteeNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankGuaranteeNo.Properties.MaxLength = 80;
            this.TxtBankGuaranteeNo.Size = new System.Drawing.Size(264, 20);
            this.TxtBankGuaranteeNo.TabIndex = 79;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(7, 8);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(103, 14);
            this.label29.TabIndex = 78;
            this.label29.Text = "Bank Guarantee#";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBankGuaranteeDt
            // 
            this.DteBankGuaranteeDt.EditValue = null;
            this.DteBankGuaranteeDt.EnterMoveNextControl = true;
            this.DteBankGuaranteeDt.Location = new System.Drawing.Point(118, 47);
            this.DteBankGuaranteeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBankGuaranteeDt.Name = "DteBankGuaranteeDt";
            this.DteBankGuaranteeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBankGuaranteeDt.Properties.Appearance.Options.UseFont = true;
            this.DteBankGuaranteeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBankGuaranteeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBankGuaranteeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBankGuaranteeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBankGuaranteeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBankGuaranteeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBankGuaranteeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBankGuaranteeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBankGuaranteeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBankGuaranteeDt.Size = new System.Drawing.Size(125, 20);
            this.DteBankGuaranteeDt.TabIndex = 83;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(77, 50);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 14);
            this.label26.TabIndex = 82;
            this.label26.Text = "Date";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(77, 28);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 14);
            this.label25.TabIndex = 90;
            this.label25.Text = "Bank";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(118, 26);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(202, 20);
            this.LueBankCode.TabIndex = 81;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // TpApproval
            // 
            this.TpApproval.Appearance.Header.Options.UseTextOptions = true;
            this.TpApproval.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpApproval.Controls.Add(this.Grd7);
            this.TpApproval.Name = "TpApproval";
            this.TpApproval.Size = new System.Drawing.Size(766, 216);
            this.TpApproval.Text = "Approval Information";
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(766, 216);
            this.Grd7.TabIndex = 77;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpUploadFile
            // 
            this.TpUploadFile.Appearance.Header.Options.UseTextOptions = true;
            this.TpUploadFile.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpUploadFile.Controls.Add(this.Grd8);
            this.TpUploadFile.Name = "TpUploadFile";
            this.TpUploadFile.Size = new System.Drawing.Size(766, 216);
            this.TpUploadFile.Text = "Upload File";
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(766, 216);
            this.Grd8.TabIndex = 77;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd8_EllipsisButtonClick);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(148, 280);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.MaxLength = 8;
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(117, 20);
            this.DteStartDt.TabIndex = 43;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(83, 282);
            this.label39.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(64, 14);
            this.label39.TabIndex = 42;
            this.label39.Text = "Start Date";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSOContract3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 597);
            this.Name = "FrmSOContract3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPRJIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomerContactperson.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICSales.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtBefTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitMarginPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitMarginAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalResource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmtAfterTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcSOContract)).EndInit();
            this.TcSOContract.ResumeLayout(false);
            this.TpBOQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpResource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectCost.Properties)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemunerationCost.Properties)).EndInit();
            this.TpJO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpBankGuarantee.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankGuaranteeNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            this.TpApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpUploadFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtCOAAmt;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo2;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraTab.XtraTabPage TpAdditional;
        private DevExpress.XtraTab.XtraTabControl TcSOContract;
        private DevExpress.XtraTab.XtraTabPage TpBOQ;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage TpItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        internal DevExpress.XtraEditors.TextEdit TxtSAName;
        public DevExpress.XtraEditors.SimpleButton BtnCustomerShipAddress;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.LookUpEdit LueStatus;
        internal DevExpress.XtraEditors.TextEdit TxtAddress;
        internal DevExpress.XtraEditors.TextEdit TxtCity;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtCountry;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.SimpleButton BtnCtShippingAddress;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        public DevExpress.XtraEditors.SimpleButton BtnContact;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueShpMCode;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        public DevExpress.XtraEditors.SimpleButton BtnBOQDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmtAfterTax;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.MemoExEdit MeeProjectDesc;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraTab.XtraTabPage TpJO;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Label label24;
        public DevExpress.XtraEditors.LookUpEdit LueJOType;
        private DevExpress.XtraTab.XtraTabPage TpBankGuarantee;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.TextEdit TxtBankGuaranteeNo;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.DateEdit DteBankGuaranteeDt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        public DevExpress.XtraEditors.LookUpEdit LueBankCode;
        internal DevExpress.XtraEditors.DateEdit DteJODocDt;
        public DevExpress.XtraEditors.LookUpEdit LueJOCode;
        internal DevExpress.XtraEditors.TextEdit TxtTotalResource;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.LookUpEdit LueCustomerContactperson;
        public DevExpress.XtraEditors.SimpleButton BtnPRJIDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtPRJIDocNo;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtProfitMarginPerc;
        internal DevExpress.XtraEditors.TextEdit TxtProfitMarginAmt;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraTab.XtraTabPage TpResource;
        private DevExpress.XtraTab.XtraTabPage TpApproval;
        private DevExpress.XtraTab.XtraTabPage TpUploadFile;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmtBefTax;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel9;
        protected System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtCostPerc;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtDirectCost;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtRemunerationCost;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtDirectPerc;
        internal DevExpress.XtraEditors.TextEdit TxtIndirectPerc;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPerc;
        internal DevExpress.XtraEditors.TextEdit TxtRemunerationPerc;
        internal DevExpress.XtraEditors.TextEdit TxtIndirectCost;
        private System.Windows.Forms.Label label35;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        internal DevExpress.XtraEditors.TextEdit TxtPICSales;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label label39;
    }
}