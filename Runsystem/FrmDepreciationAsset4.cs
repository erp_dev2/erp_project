﻿#region Update
/*
    21/06/2022 [IBL/PRODUCT] New apps (Multiple DBA dari FrmDepreciationAsset3)
    30/06/2022 [RDA/PRODUCT] tambahan di process 1 (penyesuaian di asset date dan asset value yg diproses ketika AssetSource = 2)
    15/07/2022 [IBL/PRODUCT] nilai NBV index terahir nilainya dijadikan 0 atau 1 berdasarkan parameter DepreciationSalvageValue
                             Multiple DBA atas master asset : asset value on acquisition date -> asset value - total recondition asset; recondition value -> total recondition
                             Multiple DBA atas initial master asset : asset value on acquisition date -> (asset value - acc depre opening balance) - total recondition; recondition value -> total recondition
    27/07/2022 [SET/PRODUCT] Penyesuain perhitungan DBA sebelum & sesudah rekondisi asset - Process1(), Process3(), Process4()
    04/08/2022 [IBL/PRODUCT] nilai NBV index terahir nilainya ambil dari ResidualValue
    09/08/2022 [SET/PRODUCT] merubah source Asset Value menjadi dari nilai Asset Value di Initial Master Asset
    07/09/2022 [RDA/PRODUCT] Penyesuaian depreciation value dr master asset after recondition (garis lurus). Depre Val = AssetNBV On Recondition Dt / EcoLifeMth
    13/09/2022 [IBL/PRODUCT] Penyesuaian depreciation value dr master asset after recondition (garis lurus). Depre Val = AssetNBV On Recondition Dt - ResidualValue / EcoLifeMth
    03/04/2023 [SET/PHT] Penyesuaian source Cost Value & Annual Depreciation Value -> Process4()
    10/04/2023 [SET/PHT] Penyesuaian nilai NBV
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDepreciationAsset4 : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private bool mIsDocNoDepreciationAsset2BasedOnPurchaseDate = false;
        private string
            mDepreciationTypeGarisLurus = string.Empty,
            mDepreciationTypeSaldoMenurun = string.Empty,
            mDepreciationSalvageValue = string.Empty,
            LastYr = string.Empty;


        #endregion

        #region Constructor

        public FrmDepreciationAsset4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Asset's Code",
                        "Asset's Name",
                        "",
                        "Date of Purchase",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 120, 300, 20, 120,
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@AssetDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@AssetDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "AssetCode", "AssetName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        string.Concat(
                        "Select AssetCode, AssetName, If(AssetSource = '1', AssetDt, OpeningBalanceDt) As AssetDt ",
                        "From TblAsset ",
                        "Where ActiveInd='Y' ",
                        "And AssetDt Is Not Null ",
                        "And AssetDt Between @AssetDt1 And @AssetDt2 ",
                        "And AssetCode Not In (Select AssetCode From TblDepreciationAssetHdr Where CancelInd='N')",
                        Filter, " Order By AssetDt, AssetName;"),
                        new string[]
                        { "AssetCode", "AssetName", "AssetDt" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Save Data

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsProcessDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                int n = 0;
                if (Grd1.Rows.Count > 0)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                            SaveData(r, ref n);
                    }
                }
                if (n <= 1)
                    Sm.StdMsg(mMsgType.Info, n.ToString() + " record already processed.");
                else
                    Sm.StdMsg(mMsgType.Info, n.ToString() + " records already processed.");

                Sm.ClearGrd(Grd1, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsProcessDataNotValid()
        {
            return IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            var Dt = string.Empty;
            bool IsProcess = false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1))
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        IsProcess = true;
                        Dt = Sm.GetGrdDate(Grd1, r, 5);
                        if (Dt.Length > 0)
                        {
                            if (Sm.IsClosingJournalInvalid(Sm.Left(Dt, 8))) return true;
                        }
                    }
                }
            }
            if (!IsProcess)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process minimum 1 asset.");
                return true;
            }
            return false;
        }

        private void SaveData(int r, ref int n)
        {
            var o = new DepreciationAssetHdr();
            var l = new List<DepreciationAssetDtl>();

            string DocNo = Sm.GenerateDocNo(Sm.GetGrdDate(Grd1, r, 5).Substring(0, 8), "DepreciationAsset", "TblDepreciationAssetHdr");
            string AssetCode = Sm.GetGrdStr(Grd1, r, 2);

            Process1(DocNo, AssetCode, ref o);
            Process2(DocNo, AssetCode, ref o, ref l);
            Process3(ref o, ref l);
            Process4(ref o, ref l);
            Process5(ref o, ref l);
            n += 1;
        }

        private void Process1(string DocNo, string AssetCode, ref DepreciationAssetHdr o)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            //SQL.AppendLine("Select A.AssetDt, A.AssetValue, A.EcoLife, A.EcoLifeYr, A.DepreciationCode, A.CCCode, A.PercentageAnnualDepreciation, ");
            //SQL.AppendLine("B.ReconditionDocNo, B.ReconditionStartMth, B.ReconditionYr, B.ReconditionEndMth, A.ResidualValue ");
            SQL.AppendLine("Select  ");
            SQL.AppendLine("If(A.AssetSource = '1', A.AssetDt, A.OpeningBalanceDt) As AssetDt, ");
            SQL.AppendLine("A.AssetValue, ");
            //SQL.AppendLine("If(A.AssetSource = '1', A.AssetValue, A.AssetValue - A.AccDepr) As AssetValue, ");
            SQL.AppendLine("If(A.AssetSource = '1', A.EcoLife, A.RemEcoLifeMth) As EcoLife, ");
            SQL.AppendLine("If(A.AssetSource = '1', A.EcoLifeYr, A.RemEcoLifeYr) As EcoLifeYr, ");
            SQL.AppendLine("A.DepreciationCode, A.CCCode, A.PercentageAnnualDepreciation, A.AssetSource, ");
            SQL.AppendLine("B.ReconditionDocNo, B.ReconditionStartMth, B.ReconditionYr, B.ReconditionEndMth, A.ResidualValue  ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select AssetCode, DocNo As ReconditionDocNo, Substring(ReconditionDt, 5, 2) ReconditionStartMth, ");
            SQL.AppendLine("	Substring(ReconditionDt, 1, 4) ReconditionYr, EcoLife3 ReconditionEndMth  ");
            SQL.AppendLine("	From TblReconditionAssetHdr ");
            SQL.AppendLine("	Where CancelInd = 'N' ");
            SQL.AppendLine("	And AssetCode = @AssetCode ");
            SQL.AppendLine("	Order By CreateDt Desc Limit 1 ");
            SQL.AppendLine(")B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("Where A.AssetCode=@AssetCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {
                    //0
                    "AssetDt", 

                    //1-5
                    "AssetValue", "EcoLife", "EcoLifeYr", "DepreciationCode", "CCCode",

                    //6-10
                    "PercentageAnnualDepreciation", "ReconditionDocNo", "ReconditionStartMth", "ReconditionYr", "ReconditionEndMth",

                    //11-12
                    "ResidualValue", "AssetSource"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        o.DocNo = DocNo;
                        o.AssetCode = AssetCode;
                        o.DepreciationDt = Sm.DrStr(dr, c[0]);
                        o.AssetValue = Sm.DrDec(dr, c[1]);
                        o.EcoLife = Sm.DrDec(dr, c[2]);
                        o.EcoLifeYr = Sm.DrDec(dr, c[3]);
                        o.DepreciationCode = Sm.DrStr(dr, c[4]);
                        o.CCCode = Sm.DrStr(dr, c[5]);
                        o.PercentageAnnualDepreciation = Sm.DrDec(dr, c[6]);
                        o.ReconditionDocNo = Sm.DrStr(dr, c[7]);
                        o.ReconditionStartMth = Sm.DrStr(dr, c[8]);
                        o.ReconditionYr = Sm.DrStr(dr, c[9]);
                        o.ReconditionEndMth = Sm.DrDec(dr, c[10]);
                        o.ResidualValue = Sm.DrDec(dr, c[11]);
                        o.AssetSource = Sm.DrStr(dr, c[12]);
                        o.Status = "Multiple Depreciation of Business Asset.";
                        o.Remark = "Processed By System.";
                    }
                }
                dr.Close();
            }
        }

        //SetMthGrd
        private void Process2(string DocNoTemp, string AssetCode, ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            try
            {
                decimal
                    StartMth = decimal.Parse(o.DepreciationDt.Substring(4, 2)) - 1,
                    Year = decimal.Parse(o.DepreciationDt.Substring(0, 4)),
                    EndMth = 0m;

                if (o.ReconditionDocNo.Length > 0)
                {
                    if (o.ReconditionStartMth.Length > 0)
                    {
                        StartMth = decimal.Parse(o.ReconditionStartMth) - 1;
                        Year = decimal.Parse(o.ReconditionYr);
                    }
                }
                else
                {
                    if (o.DepreciationDt.Length > 0)
                    {
                        StartMth = decimal.Parse(o.DepreciationDt.Substring(4, 2)) - 1;
                        Year = decimal.Parse(o.DepreciationDt.Substring(0, 4));
                    }
                }

                if (mDepreciationTypeGarisLurus == "1" && o.DepreciationCode == "1")
                    EndMth = o.EcoLife;
                else if (mDepreciationTypeSaldoMenurun == "1" && o.DepreciationCode == "2")
                    EndMth = o.EcoLife;
                else
                {
                    EndMth = 12 - StartMth;
                    EndMth += ((o.EcoLifeYr - 1) * 12);
                }

                for (int x = 0; x < EndMth; x++)
                {
                    if (StartMth == 12)
                    {
                        StartMth = 1;
                        Year = Year + 1;
                    }
                    else
                        StartMth += 1;

                    l.Add(new DepreciationAssetDtl()
                    {
                        DocNo = DocNoTemp,
                        MthPurchase = (x + 1).ToString(),
                        Mth = Sm.Right(string.Concat("0", StartMth), 2),
                        Yr = Year.ToString(),
                        DepreciationValue = 0m,
                        CCCode = o.CCCode,
                        CostValue = 0m,
                        AnnualCostValue = 0m,
                        AccDeprValue = 0m,
                        NBV = 0m,
                        ReconditionDocNo = string.Empty
                    });
                }
            }
            catch(Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void Process3(ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            string
                SiteName = string.Empty,
                ProfitCenterName = string.Empty,
                ReconditionDocNo = string.Empty,
                ReconditionStartMth = string.Empty,
                ReconditionEndMth = string.Empty,
                YrMth = string.Empty;
            decimal
                ResidualValue = 0m,
                AssetValue = 0m,
                ReconditionValue = 0m,
                TotalAssetValue = 0m,
                AssetDepreciableValue = 0m,
                NBVValue2 = 0m,
                AccDeprOB = 0m
                ;

            ResidualValue = o.ResidualValue;

            var SQL = new StringBuilder();

            SQL.AppendLine("/*DepreciationAsset3 - SetDepAssetDtl*/ ");
            SQL.AppendLine("Select B.SiteName, D.ProfitCenterName, ifnull(A.ResidualValue, 0.00) ResidualValue, ");
            //SQL.AppendLine("Case A.AssetSource ");
            //SQL.AppendLine("	When '1' Then IfNull(A.AssetValue, 0.00) - IfNull(F.ReconditionValue, 0.00) ");
            //SQL.AppendLine("	When '2' Then IfNull(A.AssetValue, 0.00) - IfNull(F.ReconditionValue, 0.00) - IfNull(A.AccDepr, 0.00)");
            //SQL.AppendLine("End As AssetValue, ");
            SQL.AppendLine("IFNULL(A.AssetValue, 0.00) - IFNULL(F.ReconditionValue, 0.00) AS AssetValue, ");
            SQL.AppendLine("E.DocNo ReconditionDocNo, E.ReconditionStartMth, Ifnull(F.ReconditionValue, 0.00) ReconditionValue, IfNull(E.NBVValue2, 0.00) NBVValue2, E.ReconditionEndMth, ");
            SQL.AppendLine("IF(A.AssetSource = '2', IFNULL(A.AccDepr, 0.00), 0.00) AS AccDepr ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("left Join TblSite B On A.SiteCode = B.SiteCode ");
            SQL.AppendLine("Left Join TblCostCenter C On A.CCCode = C.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo, AssetCode, Left(ReconditionDt, 6) ReconditionStartMth, ReconditionValue, NBVValue2, ");
            SQL.AppendLine("    Left(Date_Format(Date_Add(ReconditionDt, Interval (EcoLife3-1) Month), '%Y%m%d'), 6) ReconditionEndMth ");
            SQL.AppendLine("    From TblReconditionAssetHdr ");
            SQL.AppendLine("    Where AssetCode = @AssetCode AND CancelInd = 'N' AND STATUS = 'A' ");
            SQL.AppendLine("    Order By CreateDt Desc Limit 1 ");
            SQL.AppendLine(")E On A.AssetCode = E.AssetCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select AssetCode, Sum(ReconditionValue) As ReconditionValue ");
            SQL.AppendLine("	From TblReconditionAssetHdr ");
            SQL.AppendLine("	Where AssetCode = @AssetCode And CancelInd = 'N' And Status = 'A' ");
            SQL.AppendLine("	Group By AssetCode ");
            SQL.AppendLine(")F On A.AssetCode = F.AssetCode ");
            SQL.AppendLine("Where A.AssetCode = @AssetCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", o.AssetCode);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    "SiteName",
                    "ProfitCenterName", "ResidualValue", "AssetValue", "ReconditionStartMth", "ReconditionValue",
                    "ReconditionEndMth", "ReconditionDocNo", "NBVValue2", "AccDepr"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    SiteName = Sm.DrStr(dr, c[0]);
                    ProfitCenterName = Sm.DrStr(dr, c[1]);
                    AssetValue = Sm.DrDec(dr, c[3]);
                    ReconditionStartMth = Sm.DrStr(dr, c[4]);
                    ReconditionValue = Sm.DrDec(dr, c[5]);
                    ReconditionEndMth = Sm.DrStr(dr, c[6]);
                    ReconditionDocNo = Sm.DrStr(dr, c[7]);
                    NBVValue2 = Sm.DrDec(dr, c[8]);
                    AccDeprOB = Sm.DrDec(dr, c[9]);
                }, true
            );

            for (int i = 0; i < l.Count; i++)
            {
                YrMth = l[i].Yr + l[i].Mth;
                l[i].ReconditionValue = 0m;

                if (ReconditionDocNo.Length > 0)
                {
                    if (Decimal.Parse(YrMth) >= Decimal.Parse(ReconditionStartMth) && Decimal.Parse(YrMth) <= Decimal.Parse(ReconditionEndMth))
                    {
                        l[i].ReconditionValue = ReconditionValue;
                        l[i].ReconditionDocNo = ReconditionDocNo;
                        l[i].NBVValue2 = NBVValue2;
                    }
                }

                TotalAssetValue = AssetValue + l[i].ReconditionValue;
                //AssetDepreciableValue = TotalAssetValue - ResidualValue;
                AssetDepreciableValue = TotalAssetValue - AccDeprOB - ResidualValue;

                l[i].TotalAssetValue = TotalAssetValue;
                l[i].AssetDepreciableValue = AssetDepreciableValue;
            }
        }

        private void Process4(ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            decimal
                StartMth = 0m,
                CostValue = 0m,
                Prosen = 0m,
                EcoLife = 0m,
                ResidualMonth = 0m,
                DepreciationValue = 0m,
                NBV = 0m,
                Balance = 0m,
                AnnualDepreciationValue = 0m
                ;
            string Year = string.Empty,
                AssetSource = string.Empty,
                RecoditionDocNo = string.Empty;

            StartMth = decimal.Parse(o.DepreciationDt.Substring(4, 2)) - 1;
            Year = o.DepreciationDt.Substring(0, 4);
            CostValue = o.AssetValue;
            Prosen = o.PercentageAnnualDepreciation;
            EcoLife = 12 - StartMth;
            ResidualMonth = 12 - StartMth;
            EcoLife += ((o.EcoLifeYr - 1) * 12);
            AssetSource = o.AssetSource;
            RecoditionDocNo = o.ReconditionDocNo;

            if (mDepreciationTypeGarisLurus == "1") EcoLife = o.EcoLife;

            #region DeprMethod = 2 saldo menurun
            if (o.DepreciationCode == "2")
            {
                #region DeprType = 0  saldo menurun RunSystem
                if (mDepreciationTypeSaldoMenurun == "0")
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (i == 0)
                        {
                            l[i].Balance = Math.Round(CostValue, 2);
                            l[i].AnnualCostValue = Math.Round((Prosen * CostValue / 100m), 2);
                            l[i].DepreciationValue = Math.Round(l[i].AnnualCostValue / ResidualMonth, 2);
                        }
                        else
                        {
                            #region Old

                            if (i == (EcoLife - 1))
                            {
                                if (mDepreciationSalvageValue == "0")
                                {
                                    l[i].Balance = 0m;
                                    l[i].AnnualCostValue = 0m;
                                    l[i].DepreciationValue = Math.Round((l[i - 1].Balance) - o.AssetValue, 2);
                                }
                                else
                                {
                                    l[i].Balance = 1m;
                                    l[i].AnnualCostValue = 1m;
                                    l[i].DepreciationValue = Math.Round((l[i - 1].Balance) - o.AssetValue, 2);
                                }
                            }
                            else
                            {
                                if (l[i].Yr == Year)
                                {
                                    if (i == 0)
                                        l[i].Balance = Math.Round(CostValue, 2);
                                    else
                                    {
                                        if (i == 0)
                                            l[i].Balance = 0m;
                                        else
                                            l[i].Balance = Math.Round(l[i - 1].Balance - l[i - 1].DepreciationValue, 2);
                                    }
                                    l[i].AnnualDepreciationValue = Math.Round((Prosen * CostValue / 100m), 2);
                                    l[i].DepreciationValue = Math.Round(l[i].AnnualDepreciationValue / GetYrCount(l[i].Yr, ref l), 2);
                                }
                                else
                                {
                                    Year = l[i].Yr;
                                    if (i == 0)
                                        CostValue = 0m;
                                    else
                                        CostValue = l[i - 1].Balance - l[i - 1].DepreciationValue;

                                    if (i == 0)
                                        l[i].Balance = 0m;
                                    else
                                        l[i].Balance = Math.Round(l[i - 1].Balance - l[i].DepreciationValue, 2);

                                    l[i].AnnualDepreciationValue = Math.Round((Prosen * CostValue / 100m), 2);
                                    l[i].DepreciationValue = Math.Round(l[i].AnnualDepreciationValue / GetYrCount(l[i].Yr, ref l), 2);
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                #region DeprType = 1 saldo menurun Non RunSystem
                else
                {
                    int CountData = 0;
                    int lastRow = l.Count - 1;
                    if (l.Count > 1)
                    {
                        LastYr = l[lastRow].Yr;
                        if (l[0].ReconditionDocNo.Length > 0) Year = l[0].Yr;
                    }

                    string CurrYr = Year;
                    bool IsFirst = true;

                    for (int i = 0; i < l.Count; i++)
                    {
                        if (l[i].Yr == CurrYr && CurrYr == Year) // tahun awal
                        {
                            if (IsFirst)
                            {
                                if (l[i].ReconditionDocNo.Length > 0) AnnualDepreciationValue = l[i].NBVValue2 * (Prosen / 100);
                                else AnnualDepreciationValue = l[i].AssetDepreciableValue * (Prosen / 100);

                                IsFirst = false;
                            }
                            DepreciationValue = AnnualDepreciationValue / 12;
                        }
                        else // tahun setelahnya
                        {
                            if (l[i].Yr != CurrYr)
                            {
                                CountData = GetYrCount(l[i].Yr, ref l);
                                if (l[i].Yr != LastYr)
                                    AnnualDepreciationValue = (NBV - o.ResidualValue) * (Prosen / 100);
                                else
                                    AnnualDepreciationValue = (NBV - o.ResidualValue);
                            }

                            DepreciationValue = AnnualDepreciationValue / CountData;
                        }

                        l[i].DepreciationValue = Math.Round(DepreciationValue, 2);
                        l[i].AnnualDepreciationValue = Math.Round(AnnualDepreciationValue, 2);

                        SetAccDeprValueAndNBVRow(i, ref o, ref l);
                        NBV = l[i].NBV;
                        CurrYr = l[i].Yr;
                        Balance = l[i].TotalAssetValue - l[i].AccDeprValue;
                        l[i].Balance = Math.Round(Balance, 2);
                    }
                }
                #endregion
            }
            #endregion

            #region DeprMethod = 1 garis lurus
            else
            {
                #region DeprType = 1 Garis lurus non Runsystem
                if (mDepreciationTypeGarisLurus == "1")
                {
                    int CountData = 0;
                    int lastRow = l.Count - 1;
                    for (int i = 0; i < l.Count; i++)
                    {
                        //AssetDepreciableValue = Sm.GetGrdDec(Grd1, Row, 12);
                        //l[i].AssetDepreciableValue
                        Year = l[i].Yr;
                        CountData = GetYrCount(Year, ref l);

                        if (i == 0)
                        {
                            l[i].Balance = Math.Round((CostValue - (CostValue / EcoLife)), 2);
                        }
                        else if (i == lastRow)
                        {
                            if (mDepreciationSalvageValue == "0")
                                l[i].Balance = 0m;
                            else
                                l[i].Balance = 1m;
                        }
                        else
                        {
                            l[i].Balance = Math.Round((l[i-1].Balance - l[i-1].DepreciationValue), 2);
                        }
                        if (i == lastRow)
                        {
                            if (mDepreciationSalvageValue == "0")
                            {
                                //l[i].DepreciationValue = Math.Round((l[i].AssetDepreciableValue / EcoLife), 2);
                                //l[i].AnnualDepreciationValue = Math.Round(((l[i].AssetDepreciableValue / EcoLife) * CountData), 2);
                                l[i].AnnualDepreciationValue = Math.Round(((l[i].AssetDepreciableValue / EcoLife) * CountData), 2);
                            }
                            else
                            {
                                //l[i].DepreciationValue = Math.Round((l[i].AssetDepreciableValue / EcoLife) - 1, 2);
                                //l[i].AnnualDepreciationValue = Math.Round(((l[i].AssetDepreciableValue / EcoLife) * CountData) - 1, 2);
                                l[i].AnnualDepreciationValue = Math.Round(((l[i].AssetDepreciableValue / EcoLife) * CountData) - 1, 2);
                            }
                        }
                        else
                        {
                            if (RecoditionDocNo.Length > 0) 
                            {
                                l[i].DepreciationValue = Math.Round(((l[i].NBVValue2 - o.ResidualValue) / EcoLife), 2);
                                l[i].AnnualDepreciationValue = Math.Round(((l[i].NBVValue2 - o.ResidualValue) / EcoLife) * CountData, 2);
                            }
                            else
                            {
                                l[i].DepreciationValue = Math.Round((l[i].AssetDepreciableValue / EcoLife), 2);
                                l[i].AnnualDepreciationValue = Math.Round(((l[i].AssetDepreciableValue / EcoLife) * CountData), 2);
                            }
                        }
                    }
                }
                #endregion

                #region DeprType = 0 Garis lurus Runsystem
                else
                {
                    int CountData = 0;
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (l[i].Yr == Year)
                        {
                            if (i == 0)
                            {
                                CountData = GetYrCount(l[i].Yr, ref l);
                                l[i].Balance = Math.Round(CostValue, 2);
                            }
                            else
                            {
                                l[i].Balance = Math.Round((l[i-1].Balance) - (l[i-1].DepreciationValue), 2);
                            }

                            l[i].AnnualDepreciationValue = Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2);
                            l[i].DepreciationValue = Math.Round((CostValue / EcoLife), 2);
                        }
                        else
                        {
                            CountData = CountData + GetYrCount(l[i].Yr, ref l);
                            if (i == 0)
                            {
                                l[i].Balance = 0m;
                            }
                            else
                            {
                                l[i].Balance = Math.Round((l[i-1].Balance) - (l[i-1].DepreciationValue), 2);
                            }
                            l[i].AnnualDepreciationValue = Math.Round((CostValue - ((CostValue / EcoLife) * CountData)), 2);
                            l[i].DepreciationValue = Math.Round((CostValue / EcoLife), 2);
                        }
                    }
                }
                #endregion
            }
            #endregion

            if (!(o.DepreciationCode == "2" && mDepreciationTypeSaldoMenurun == "1")) SetAccDeprValueAndNBV(ref o, ref l);
        }

        private void Process5(ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {

            bool IsFirst = true;
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDepreciationAssetHdr(DocNo, DocDt, CancelInd, AssetCode, AssetValue, ResidualValue, DepreciationDt, EcoLifeYr, EcoLife, DepreciationCode, PercentageAnnualDepreciation, Status, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @AssetCode, @AssetValue, @ResidualValue, @DepreciationDt, @EcoLifeYr, @EcoLife, @DepreciationCode, @PercentageAnnualDepreciation, @Status, @Remark, @UserCode, CurrentDateTime()); ");

            for (int i = 0; i < l.Count; i++)
            {
                var mh = l[i].AnnualDepreciationValue;
                var s = l[i].CostValue;
                var p = l[i].AnnualCostValue;
                var sp = l[i].Balance;
                if (IsFirst)
                {
                    SQL.AppendLine("Insert Into TblDepreciationAssetDtl(DocNo, DNo, ActiveInd, MthPurchase, Mth, Yr, DepreciationValue, CCCode, CostValue, AnnualCostValue, ReconditionDocNo, AccDeprValue, NBV, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values");
                    IsFirst = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", 'Y', @MthPurchase_" + i.ToString() + " ");
                SQL.AppendLine(", @Mth_" + i.ToString() + ", @Yr_" + i.ToString() + ", @DepreciationValue_" + i.ToString() + " ");
                SQL.AppendLine(", @CCCode_" + i.ToString() + ", @CostValue_" + i.ToString() + ", @AnnualCostValue_" + i.ToString() + " ");
                SQL.AppendLine(", @ReconditionDocNo_" + i.ToString() + ", @AccDeprValue_" + i.ToString() + ", @NBV_" + i.ToString() + " ");
                SQL.AppendLine(", @UserCode, CurrentDateTime())");

                Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("00" + (i + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@MthPurchase_" + i.ToString(), l[i].MthPurchase);
                Sm.CmParam<String>(ref cm, "@Mth_" + i.ToString(), l[i].Mth);
                Sm.CmParam<String>(ref cm, "@Yr_" + i.ToString(), l[i].Yr);
                Sm.CmParam<Decimal>(ref cm, "@DepreciationValue_" + i.ToString(), l[i].DepreciationValue);
                Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), l[i].CCCode);
                Sm.CmParam<Decimal>(ref cm, "@CostValue_" + i.ToString(), l[i].Balance);
                Sm.CmParam<Decimal>(ref cm, "@AnnualCostValue_" + i.ToString(), l[i].AnnualDepreciationValue);
                Sm.CmParam<String>(ref cm, "@ReconditionDocNo_" + i.ToString(), l[i].ReconditionDocNo);
                Sm.CmParam<Decimal>(ref cm, "@AccDeprValue_" + i.ToString(), l[i].AccDeprValue);
                Sm.CmParam<Decimal>(ref cm, "@NBV_" + i.ToString(), l[i].NBV);
            }
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", o.DocNo);
            if (!mIsDocNoDepreciationAsset2BasedOnPurchaseDate)
                Sm.CmParamDt(ref cm, "@DocDt", Sm.ServerCurrentDate());
            else
                Sm.CmParamDt(ref cm, "@DocDt", o.DepreciationDt);

            Sm.CmParam<String>(ref cm, "@AssetCode", o.AssetCode);
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", o.AssetValue);
            Sm.CmParam<Decimal>(ref cm, "@ResidualValue", o.ResidualValue);
            Sm.CmParamDt(ref cm, "@DepreciationDt", o.DepreciationDt);
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", o.EcoLifeYr);
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", o.EcoLife);
            Sm.CmParam<String>(ref cm, "@DepreciationCode", o.DepreciationCode);
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", o.PercentageAnnualDepreciation);
            Sm.CmParam<String>(ref cm, "@Status", o.Status);
            Sm.CmParam<String>(ref cm, "@Remark", o.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private int GetYrCount(string Year, ref List<DepreciationAssetDtl> l)
        {
            int c = 0;
            for (int i = 0; i < l.Count; i++)
                if (l[i].Yr == Year) c += 1;
            return c;
        }

        private void SetAccDeprValueAndNBVRow(int i, ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            decimal NBV = 0m, AccDeprValue = 0m, PrevAccDeprValue = 0m, AccDeprValueRoA = 0m, DepreciationValue = 0m;
            int lastRow = l.Count() - 1;

            if (i != 0) PrevAccDeprValue = l[i - 1].AccDeprValue;

            if (i == 0)
            {
                if (l[i].ReconditionDocNo.Length > 0)
                {
                    AccDeprValueRoA = Sm.GetValueDec("Select AssetValue2 From TblReconditionAssetHdr Where DocNo = @Param", l[i].ReconditionDocNo);
                }
                else
                    AccDeprValueRoA = Sm.GetValueDec("Select If(AssetSource = '2', AccDepr, 0.00) From TblAsset Where AssetCode = @Param", o.AssetCode);
                AccDeprValue = AccDeprValueRoA + l[i].DepreciationValue;
            }
            else
            {
                if (i == lastRow)
                {
                    DepreciationValue = l[i - 1].NBV - o.ResidualValue;
                    l[i].DepreciationValue = DepreciationValue;
                }
                AccDeprValue = PrevAccDeprValue + l[i].DepreciationValue;
            }

            NBV = l[i].TotalAssetValue - AccDeprValue;

            l[i].AccDeprValue = AccDeprValue;

            if (i == lastRow)
                l[i].NBV = o.ResidualValue;
            else
                l[i].NBV = NBV;
        }

        private void SetAccDeprValueAndNBV(ref DepreciationAssetHdr o, ref List<DepreciationAssetDtl> l)
        {
            decimal AccDeprValueRoA = 0m;
            int lastRow = l.Count() - 1;

            for (int i = 0; i < l.Count; ++i)
            {
                if (i == 0)
                {
                    if (l[i].ReconditionDocNo.Length > 0)
                        AccDeprValueRoA = Sm.GetValueDec("Select AssetValue2 From TblReconditionAssetHdr Where DocNo = @Param", l[i].ReconditionDocNo);
                    else
                        AccDeprValueRoA = Sm.GetValueDec("Select If(AssetSource = '2', AccDepr, 0.00) From TblAsset Where AssetCode = @Param", o.AssetCode);
                    l[i].AccDeprValue = AccDeprValueRoA + l[i].DepreciationValue;
                }
                else
                {
                    if (i == lastRow)
                        l[i].DepreciationValue = l[i - 1].NBV - o.ResidualValue;
                    l[i].AccDeprValue = l[i - 1].AccDeprValue + l[i].DepreciationValue;
                }

                if (i == lastRow)
                    l[i].NBV = o.ResidualValue;
                else
                    l[i].NBV = l[i].TotalAssetValue - l[i].AccDeprValue;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsDocNoDepreciationAsset2BasedOnPurchaseDate', 'DepreciationTypeGarisLurus', 'DepreciationTypeSaldoMenurun', 'DepreciationSalvageValue' ");
            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDocNoDepreciationAsset2BasedOnPurchaseDate": mIsDocNoDepreciationAsset2BasedOnPurchaseDate = ParValue == "Y"; break;
                            
                            //String
                            case "DepreciationTypeGarisLurus": mDepreciationTypeGarisLurus = ParValue; break;
                            case "DepreciationTypeSaldoMenurun": mDepreciationTypeSaldoMenurun = ParValue; break;
                            case "DepreciationSalvageValue": mDepreciationSalvageValue = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAsset2(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion

        #region Class

        private class DepreciationAssetHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string AssetCode { get; set; }
            public decimal AssetValue { get; set; }
            public string DepreciationDt { get; set; }
            public decimal EcoLifeYr { get; set; }
            public decimal EcoLife { get; set; }
            public string DepreciationCode { get; set; }
            public string CCCode { get; set; }
            public decimal PercentageAnnualDepreciation { get; set; }
            public string ReconditionDocNo { get; set; }
            public string ReconditionStartMth { get; set; }
            public string ReconditionYr { get; set; }
            public decimal ReconditionEndMth { get; set; }
            public decimal ResidualValue { get; set; }
            public string AssetSource { get; set; }
            public string Status { get; set; }
            public string Remark { get; set; }
        }

        private class DepreciationAssetDtl
        {
            public string DocNo { get; set; }
            public string MthPurchase { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public decimal DepreciationValue { get; set; }
            public string CCCode { get; set; }
            public decimal CostValue { get; set; }
            public decimal AnnualCostValue { get; set; }
            public decimal AnnualDepreciationValue { get; set; }
            public decimal AccDeprValue { get; set; }
            public decimal NBV { get; set; }
            public decimal NBVValue2 { get; set; }
            public decimal AssetValue { get; set; }
            public string ReconditionDocNo { get; set; }
            public decimal ReconditionValue { get; set; }
            public decimal TotalAssetValue { get; set; }
            public decimal AssetDepreciableValue { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }
}
