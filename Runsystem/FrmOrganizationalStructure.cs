﻿#region Update
/*
    05/06/2017 [WED] tambah tab Competence, dari master Competence.
    09/06/2017 [WED] tambah field Number of Job Holder
    15/06/2017 [HAR] tambah Level di header, tambah printout
    22/11/2017 [ARI] tambah tab experience dan education
    12/03/2018 [HAR] tab education tambah inputan education dari option dan major dari master major
    20/03/2018 [WED] tambah HardCompetence dan SoftCompetence. Fungsinya untuk hitung bobot prosentase di Matrix Employee Performance Competence
    26/03/2018 [WED] label Position --> Master Position
    26/03/2018 [WED] tambah Position (input manual, mandatory), site (mandatory), department (non mandatory)
    26/03/2018 [WED] grid experience tambah combo Level Working Exp, ambil dari master level
    26/03/2018 [WED] tambah tab Potence, ambil dari master kompetensi yang group nya potensi
    28/03/2018 [WED] tambah field min dan max age
    10/03/2020 [IBL/SIER] Menyembunyikan tab Potency
    10/03/2020 [IBL/SIER] Menambah tab baru di job description (Work Indicator & Specification)
    29/03/2022 [TRI/SIER] menampilkan competency semua type yang ada di Option CompetenceLevelType
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
#endregion

namespace RunSystem
{
    public partial class FrmOrganizationalStructure : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mPosCode = string.Empty, mPosCode2 = string.Empty;
        private string mSavedPosCode2 = string.Empty;
        internal FrmOrganizationalStructureFind FrmFind;
        private int mStateIndicator = 0;
        private bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsOrganizationalStructureShowPotency = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmOrganizationalStructure(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            GetParameter();
            SetGrd();
            SetFormControl(mState.View);

            tabControl1.SelectTab("TpgCompetence");
            SetLueCompetenceCode(ref LueCompetenceCode);
            LueCompetenceCode.Visible = false;

            tabControl1.SelectTab("TpgEducation");
            SetLueEducationCode(ref LueEducation);
            LueEducation.Visible = false;
            SetLueMajorCode(ref LueMajor);
            LueMajor.Visible = false;

            tabControl1.SelectTab("TpgExperience");
            Sl.SetLueLevelCode(ref LueLevel);
            LueLevel.Visible = false;

            tabControl1.SelectTab("TpgFunction");

            Sl.SetLuePosCode(ref LuePosCode);
            Sl.SetLuePosCode(ref LueParent);
            Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");

            if (!mIsOrganizationalStructureShowPotency) tabControl1.TabPages.Remove(TpgPotence);

            //if this application is called from other application
            if (mPosCode.Length != 0)
            {
                this.Text = Sm.GetMenuDesc("FrmOrganizationalStructure");
                ShowData(mPosCode, mPosCode2);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();

        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LuePosCode, LueParent, LueCompetenceCode, TxtJobHolderQty, TxtLevel, 
                        TxtHardCompetence, TxtSoftCompetence, TxtPosName2, LueSiteCode, LueDeptCode, TxtMinAge, TxtMaxAge
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    Grd5.ReadOnly = true;
                    Grd6.ReadOnly = true;
                    Grd7.ReadOnly = true;
                    Grd8.ReadOnly = true;
                    Grd9.ReadOnly = true;
                    LuePosCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LuePosCode, LueParent, LueCompetenceCode, TxtJobHolderQty, TxtLevel, 
                        TxtHardCompetence, TxtSoftCompetence, TxtPosName2, LueSiteCode, LueDeptCode, TxtMinAge, TxtMaxAge
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    Grd6.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Grd8.ReadOnly = false;
                    Grd9.ReadOnly = false;
                    LuePosCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueParent, LueCompetenceCode, TxtJobHolderQty, TxtLevel, 
                        TxtHardCompetence, TxtSoftCompetence, TxtPosName2, LueSiteCode, LueDeptCode, TxtMinAge, TxtMaxAge
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    Grd6.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Grd8.ReadOnly = false;
                    Grd9.ReadOnly = false;
                    TxtPosName2.Focus();
                    break;
                default:
                    break;
            }
        }

        private void SetGrd()
        {
            #region Grid1 - Function
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-2
                        "Name",
                        "Description",
                    },
                     new int[] 
                    {
                        20,
                        150, 400
                    }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            #endregion

            #region Grid2 - Job Desc
            Grd2.Cols.Count = 3;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                   Grd2,
                   new string[] 
                    {
                        //0
                        "No",

                        //1-2
                        "Name",
                        "Description",
                    },
                    new int[] 
                    {
                        20,
                        150, 400
                    }
           );
            Sm.GrdColReadOnly(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            #endregion

            #region Grid3 - Authorized
            Grd3.Cols.Count = 3;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd3,
                  new string[] 
                    {
                        //0
                        "No",

                        //1-2
                        "Name",
                        "Description",
                    },
                   new int[] 
                    {
                        20,
                        150, 400
                    }
            );
            Sm.GrdColReadOnly(Grd3, new int[] { 0 });
            Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
            #endregion

            #region Grid4 - Competence
            Grd4.Cols.Count = 5;
            Grd4.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd4,
                  new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "Name",
                        "Description",
                        "Minimum Score",
                        "CompetenceCode"
                    },
                   new int[] 
                    {
                        20,
                        150, 400, 100, 80
                    }
            );
            Sm.GrdColReadOnly(Grd4, new int[] { 0 });
            Sm.GrdFormatDec(Grd4, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 4 }, false);
            #endregion

            #region Grid5 - Experience
            Grd5.Cols.Count = 5;
            Grd5.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[] 
                {
                    //0
                    "No",

                    //1-4
                    "Name",
                    "Description",
                    "LevelCode",
                    "Level Working"+Environment.NewLine+"Experience",
                },
                 new int[] 
                {
                    20,
                    150, 400, 0, 180
                }
            );
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 3 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 3 }, false);
            #endregion

            #region Grid6 - Education
            Grd6.Cols.Count = 7;
            Grd6.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                        //0
                        "No",
                        //1-5
                        "OptCode",
                        "Education",
                        "Major Code",
                        "Major",
                        "Name",
                        //6
                        "Description",
                    },
                    new int[] 
                    {
                        20,
                        20, 120, 20, 120, 250, 
                        400
                    }
            );
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 1, 3 });
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 3 }, false);
            #endregion

            #region Grid7 - Potency
            Grd7.Cols.Count = 4;
            Grd7.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "",
                        "PotenceCode",
                        "Potency"
                    },
                     new int[] 
                    {
                        20,
                        20, 0, 180
                    }
            );
            Sm.GrdColReadOnly(Grd7, new int[] { 0, 2, 3 });
            Sm.GrdColButton(Grd7, new int[] { 1 });
            Sm.GrdColInvisible(Grd7, new int[] { 0, 2 }, false);
            #endregion

            #region Grid8 - Work Indicator
            Grd8.Cols.Count = 3;
            Grd8.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd8,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-2
                        "Name",
                        "Description",
                    },
                     new int[] 
                    {
                        20,
                        150, 400
                    }
            );
            Sm.GrdColReadOnly(Grd8, new int[] { 0 });
            Sm.GrdColInvisible(Grd8, new int[] { 0 }, false);
            #endregion

            #region Grid9 - Specification
            Grd9.Cols.Count = 4;
            Grd9.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd9,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "Skill",
                        "Knowledge",
                        "Training",
                    },
                     new int[] 
                    {
                        20,
                        150, 200, 300
                    }
            );
            Sm.GrdColReadOnly(Grd9, new int[] { 0 });
            Sm.GrdColInvisible(Grd9, new int[] { 0 }, false);
            #endregion
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LuePosCode, LueParent, LueCompetenceCode, LueLevel, TxtPosName2, LueSiteCode, LueDeptCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtJobHolderQty }, 0);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtLevel, TxtHardCompetence, TxtSoftCompetence, TxtMinAge, TxtMaxAge }, 2);
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.ClearGrd(Grd9, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 3 });
            mStateIndicator = 0;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOrganizationalStructureFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LuePosCode, "Position")) return;
            mStateIndicator = 2;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LuePosCode, "Position") || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblOrganizationalStructure Where PosCode=@PosCode" };
                Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                string mPosCode2 = (mStateIndicator == 2) ? mSavedPosCode2 : GetPosCode2();

                cml.Add(SaveOrganizationalStructure(mPosCode2));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveOrganizationalStructureDtl(mPosCode2, Row));
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveOrganizationalStructureDtl2(mPosCode2, Row));
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveOrganizationalStructureDtl3(mPosCode2, Row));
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 4).Length > 0) cml.Add(SaveOrganizationalStructureDtl4(mPosCode2, Row));
                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) cml.Add(SaveOrganizationalStructureDtl5(mPosCode2, Row));
                for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0) cml.Add(SaveOrganizationalStructureDtl6(mPosCode2, Row));
                for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0) cml.Add(SaveOrganizationalStructureDtl7(mPosCode2, Row));
                for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0) cml.Add(SaveOrganizationalStructureDtl8(mPosCode2, Row));
                for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd9, Row, 1).Length > 0) cml.Add(SaveOrganizationalStructureDtl9(mPosCode2, Row));

                Sm.ExecCommands(cml);

                ShowData(Sm.GetLue(LuePosCode), mPosCode2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LuePosCode, "Position") || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            PrintData(Sm.GetLue(LuePosCode));
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PosCode, string PosCode2)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowOrganizationStructure(PosCode, PosCode2);
                ShowOrganizationStructureDtl(PosCode, PosCode2);
                ShowOrganizationStructureDtl2(PosCode, PosCode2);
                ShowOrganizationStructureDtl3(PosCode, PosCode2);
                ShowOrganizationStructureDtl4(PosCode, PosCode2);
                ShowOrganizationStructureDtl5(PosCode, PosCode2);
                ShowOrganizationStructureDtl6(PosCode, PosCode2);
                ShowOrganizationStructureDtl7(PosCode, PosCode2);
                ShowOrganizationStructureDtl8(PosCode, PosCode2);
                ShowOrganizationStructureDtl9(PosCode, PosCode2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOrganizationStructure(string PosCode, string PosCode2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From TblOrganizationalStructure  ");
            SQL.AppendLine("Where PosCode = @PosCode And PosCode2 = @PosCode2;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "PosCode",

                        //1-5
                        "Parent", "JobHolderQty", "Level", "HardCompetence", "SoftCompetence",

                        //6-10
                        "PosName2", "SiteCode", "DeptCode", "PosCode2", "MinAge",

                        //11
                        "MaxAge"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[0]));
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[1]));
                        TxtJobHolderQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                        TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 2);
                        TxtHardCompetence.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 2);
                        TxtSoftCompetence.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 2);
                        TxtPosName2.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[8]));
                        mSavedPosCode2 = Sm.DrStr(dr, c[9]);
                        TxtMinAge.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 2);
                        TxtMaxAge.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 2);
                    }, true
                );
        }

        private void ShowOrganizationStructureDtl(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select Dno, Name, Description From TblOrganizationalStructureDtl Where PosCode=@PosCode And PosCode2=@PosCode2 Order By DNo;",
                    new string[] { "Dno", "Name", "Description" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowOrganizationStructureDtl2(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select Dno, Name, Description From TblOrganizationalStructureDtl2 Where PosCode=@PosCode And PosCode2=@PosCode2 Order By DNo;",
                    new string[] { "Dno", "Name", "Description" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowOrganizationStructureDtl3(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                    "Select Dno, Name, Description From TblOrganizationalStructureDtl3 Where PosCode=@PosCode And PosCode2=@PosCode2 Order By DNo;",
                    new string[] { "Dno", "Name", "Description" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowOrganizationStructureDtl4(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                    "Select DNo, CompetenceCode, CompetenceName, Description, MinScore From TblOrganizationalStructureDtl4 Where PosCode=@PosCode And PosCode2=@PosCode2 Order By DNo;",
                    new string[] { "Dno", "CompetenceName", "Description", "MinScore", "CompetenceCode" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd4, 0, 0);
        }

        private void ShowOrganizationStructureDtl5(string PosCode, string PosCode2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Name, A.Description, A.LevelCode, B.LevelName ");
            SQL.AppendLine("From TblOrganizationalStructureDtl5 A ");
            SQL.AppendLine("Left Join TblLevelHdr B On A.LevelCode = B.LevelCode ");
            SQL.AppendLine("Where A.PosCode = @PosCode And A.PosCode2=@PosCode2 ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] { "DNo", "Name", "Description", "LevelCode", "LevelName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowOrganizationStructureDtl6(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd6, ref cm,
                    "Select A.Dno, A.EducationCode, B.OptDesc As Education, A.MajorCode, C.MajorName, A.Name, A.Description From TblOrganizationalStructureDtl6 A " +
                    "Left Join Tbloption B On A.EducationCode = B.OptCode And B.Optcat = 'EmployeeEducationLevel' " +
                    "Left Join Tblmajor C on A.MajorCode = C.MajorCode " +
                    "Where A.PosCode=@PosCode And A.PosCode2=@PosCode2 Order By A.DNo;",
                    new string[] { "Dno", "EducationCode", "Education", "MajorCode", "majorName", "Name", "Description" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowOrganizationStructureDtl7(string PosCode, string PosCode2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.PotenceCode, B.CompetenceName As PotenceName ");
            SQL.AppendLine("From TblOrganizationalStructureDtl7 A ");
            SQL.AppendLine("Left Join TblCompetence B On A.PotenceCode = B.CompetenceCode And B.CompetenceLevelType = '1' ");
            SQL.AppendLine("Where A.PosCode = @PosCode And PosCode2=@PosCode2 ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd7, ref cm, SQL.ToString(),
                    new string[] { "DNo", "PotenceCode", "PotenceName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 0);
        }

        private void ShowOrganizationStructureDtl8(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd8, ref cm,
                    "Select Dno, Name, Description From TblOrganizationalStructureDtl8 Where PosCode=@PosCode And PosCode2=@PosCode2 Order By DNo;",
                    new string[] { "Dno", "Name", "Description" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }

        private void ShowOrganizationStructureDtl9(string PosCode, string PosCode2)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2.Length <= 0 ? "" : PosCode2);

            Sm.ShowDataInGrid(
                    ref Grd9, ref cm,
                    "Select Dno, Skill, Knowledge, Training From TblOrganizationalStructureDtl9 Where PosCode=@PosCode And PosCode2=@PosCode2 Order By DNo;",
                    new string[] { "Dno", "Skill", "Knowledge", "Training" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd9, 0, 0);
        }

        #endregion

        #region Save Data

        #region Insert Query

        private MySqlCommand SaveOrganizationalStructure(string PosCode2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOrganizationalStructure(PosCode, PosCode2, PosName2, SiteCode, DeptCode, Parent, JobHolderQty, Level, HardCompetence, SoftCompetence, MinAge, MaxAge, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PosCode, @PosCode2, @PosName2, @SiteCode, @DeptCode, @Parent, @JobHolderQty, @Level, @HardCompetence, @SoftCompetence, @MinAge, @MaxAge, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update PosName2=@PosName2, SiteCode=@SiteCode, DeptCode=@DeptCode, Parent=@Parent, JobHolderQty = @JobHolderQty, Level=@Level, HardCompetence=@HardCompetence, SoftCompetence=@SoftCompetence, MinAge=@MinAge, MaxAge=@MaxAge, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblOrganizationalStructureDtl Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl2 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl3 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl4 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl5 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl6 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl7 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl8 Where PosCode=@PosCode And PosCode2=@PosCode2; ");
            SQL.AppendLine("Delete From TblOrganizationalStructureDtl9 Where PosCode=@PosCode And PosCode2=@PosCode2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@PosName2", TxtPosName2.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<Decimal>(ref cm, "@JobHolderQty", decimal.Parse(TxtJobHolderQty.Text));
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            Sm.CmParam<Decimal>(ref cm, "@HardCompetence", decimal.Parse(TxtHardCompetence.Text));
            Sm.CmParam<Decimal>(ref cm, "@SoftCompetence", decimal.Parse(TxtSoftCompetence.Text));
            Sm.CmParam<Decimal>(ref cm, "@MinAge", decimal.Parse(TxtMinAge.Text));
            Sm.CmParam<Decimal>(ref cm, "@MaxAge", decimal.Parse(TxtMaxAge.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl(PosCode, PosCode2, Dno, Name, Description, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @Name, @Description, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl2(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl2(PosCode, PosCode2, Dno, Name, Description, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @Name, @Description, @CreateBy, CurrentDateTime()) ; "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl3(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl3(PosCode, PosCode2, Dno, Name, Description, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @Name, @Description, @CreateBy, CurrentDateTime()) ; "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl4(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl4(PosCode, PosCode2, DNo, CompetenceCode, CompetenceName, Description, MinScore, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @CompetenceCode, @CompetenceName, @Description, @MinScore, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@CompetenceName", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@MinScore", Sm.GetGrdDec(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl5(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl5(PosCode, PosCode2, Dno, Name, Description, LevelCode, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @Name, @Description, @LevelCode, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl6(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl6(PosCode, PosCode2, Dno, EducationCode, MajorCode, Name, Description, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @EducationCode, @MajorCode, @Name, @Description, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EducationCode", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@MajorCode", Sm.GetGrdStr(Grd6, Row, 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd6, Row, 5));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd6, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl7(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl7(PosCode, PosCode2, DNo, PotenceCode, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @DNo, @PotenceCode, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PotenceCode", Sm.GetGrdStr(Grd7, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl8(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl8(PosCode, PosCode2, Dno, Name, Description, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @Name, @Description, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd8, Row, 1));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd8, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOrganizationalStructureDtl9(string PosCode2, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblOrganizationalStructureDtl9(PosCode, PosCode2, Dno, Skill, Knowledge, Training, CreateBy, CreateDt) " +
                "Values(@PosCode, @PosCode2, @Dno, @Skill, @Knowledge, @Training, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@PosCode2", PosCode2);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Skill", Sm.GetGrdStr(Grd9, Row, 1));
            Sm.CmParam<String>(ref cm, "@Knowledge", Sm.GetGrdStr(Grd9, Row, 2));
            Sm.CmParam<String>(ref cm, "@Training", Sm.GetGrdStr(Grd9, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LuePosCode, "Master Position") ||
                Sm.IsTxtEmpty(TxtPosName2, "Position", false) ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                //IsPosCodeExisted() ||
                Sm.IsTxtEmpty(TxtJobHolderQty, "Number of Job Holder", true) ||
                IsAgeValid() ||
                IsCompetenceHeaderValueInvalid() ||
                IsSumCompetenceHeaderValueInvalid() ||
                IsCompetenceCodeExisted() ||
                IsGrdValueNotValid();
        }

        private bool IsAgeValid()
        {
            if (Decimal.Parse(TxtMinAge.Text) < 0 || Decimal.Parse(TxtMaxAge.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Min. Age and Max. Age should be positive number.");
                TxtMinAge.Focus();
                return true;
            }

            if (Decimal.Parse(TxtMinAge.Text) != 0)
            {
                if (Decimal.Parse(TxtMaxAge.Text) <= Decimal.Parse(TxtMinAge.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "Max. Age should be bigger than Min. Age.");
                    TxtMaxAge.Focus();
                    return true;
                }
            }

            return false;
        }

        private bool IsSumCompetenceHeaderValueInvalid()
        {
            if (Decimal.Parse(TxtHardCompetence.Text) + Decimal.Parse(TxtSoftCompetence.Text) > 100)
            {
                var f = new StringBuilder();

                f.AppendLine("Hard Competence = " + TxtHardCompetence.Text);
                f.AppendLine("Soft Competence = " + TxtSoftCompetence.Text);
                f.AppendLine("Total = " + (Decimal.Parse(TxtHardCompetence.Text) + Decimal.Parse(TxtSoftCompetence.Text)));
                f.AppendLine(Environment.NewLine);
                f.AppendLine("Maximum summary of both competences are 100. ");

                Sm.StdMsg(mMsgType.Warning, f.ToString());
                return true;
            }
            
            return false;
        }

        private bool IsCompetenceHeaderValueInvalid()
        {
            if (Decimal.Parse(TxtHardCompetence.Text) < 0 || Decimal.Parse(TxtSoftCompetence.Text) < 0 ||
                Decimal.Parse(TxtHardCompetence.Text) > 100 || Decimal.Parse(TxtSoftCompetence.Text) > 100)
            {
                Sm.StdMsg(mMsgType.Warning, "Hard Competence and Soft Competence should be positive number, and between 0 - 100.");
                TxtHardCompetence.Focus();
                return true;
            }

            return false;
        }

        private bool IsCompetenceCodeExisted()
        {
            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    for (int Row2 = 1; Row2 < Grd4.Rows.Count - 1; Row2++)
                    {
                        if ((Sm.GetGrdStr(Grd4, Row, 4) == Sm.GetGrdStr(Grd4, Row2, 4)) && Row != Row2)
                        {
                            Sm.StdMsg(mMsgType.Warning, "This competence ( " + Sm.GetGrdStr(Grd4, Row, 1) + " ) is already exist.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Function Name is empty."))
                    {
                        tabControl1.SelectTab("TpgFunction");
                        return true;
                    }
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Job Desc Name is empty."))
                    {
                        tabControl1.SelectTab("TpgJob");
                        return true;
                    }
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "Authorized Name is empty."))
                    {
                        tabControl1.SelectTab("TpgAuthorized");
                        return true;
                    }
                }
            }
            
            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd4, Row, 3, true, "Minimum Score is zero."))
                    {
                        tabControl1.SelectTab("TpgCompetence");
                        return true;
                    }

                    if (Sm.IsGrdValueEmpty(Grd4, Row, 4, false, "Competence is empty."))
                    {
                        tabControl1.SelectTab("TpgCompetence");
                        Sm.FocusGrd(Grd4, Row, 1);
                        return true;
                    }
                }
            }

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 1, false, "Experience Name is empty."))
                    {
                        tabControl1.SelectTab("TpgExperience");
                        return true;
                    }
                }
            }

            if (Grd6.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd6, Row, 1, false, "Education is empty."))
                    {
                        tabControl1.SelectTab("TpgEducation");
                        Sm.FocusGrd(Grd6, Row, 1);
                        return true;
                    }
                }
            }

            if(Grd7.Rows.Count > 1)
            {
                for (int Row1 = 0; Row1 < Grd7.Rows.Count - 1; Row1++)
                {
                    for (int Row2 = Row1 + 1; Row2 < Grd7.Rows.Count; Row2++)
                    {
                        if (Sm.GetGrdStr(Grd7, Row1, 2) == Sm.GetGrdStr(Grd7, Row2, 2))
                        {
                            tabControl1.SelectTab("TpgPotence");
                            Sm.StdMsg(mMsgType.Warning, "Duplicate Potency : " + Sm.GetGrdStr(Grd7, Row2, 3));
                            Sm.FocusGrd(Grd7, Row2, 3);
                            return true;
                        }
                    }
                }
            }

            if (Grd8.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd8, Row, 1, false, "Work Indicator Name is empty."))
                    {
                        tabControl1.SelectTab("TpgWorkIndicator");
                        Sm.FocusGrd(Grd8, Row, 1);
                        return true;
                    }
                }
            }

            if (Grd9.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd9.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd9, Row, 1, false, "Specification Skill is empty."))
                    {
                        tabControl1.SelectTab("TpgSpecification");
                        Sm.FocusGrd(Grd9, Row, 1);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsPosCodeExisted()
        {
            if (!LuePosCode.Properties.ReadOnly && Sm.IsDataExist("Select PosCode From TblOrganizationalStructure Where PosCode ='" + Sm.GetLue(LuePosCode) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Position already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Methods

        private string GetPosCode2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    (Select Right(Concat('0000', Convert(PosCode2+1, Char)), 4) ");
	        SQL.AppendLine("    From ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Convert(Right(PosCode2, 4), Decimal) As PosCode2 ");
		    SQL.AppendLine("        From TblOrganizationalStructure ");
		    SQL.AppendLine("        Order By PosCode2 Desc Limit 1 ");
	        SQL.AppendLine("    ) As T) ");
            SQL.AppendLine(", '0001' ");
            SQL.AppendLine(") As PosCode2 ");

            return Sm.GetValue(SQL.ToString());
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsOrganizationalStructureShowPotency = Sm.GetParameterBoo("IsOrganizationalStructureShowPotency");
        }

        private void SetLuePotence(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            string mCompetenceCode = string.Empty;

            if(Grd7.Rows.Count > 0)
            {
                for (int i = 0; i < Grd7.Rows.Count - 1;  i++)
                {
                    if (Sm.GetGrdStr(Grd7, i, 1).Length > 0)
                    {
                        if (mCompetenceCode.Length > 0) mCompetenceCode += ",";
                        mCompetenceCode += Sm.GetGrdStr(Grd7, i, 1);
                    }
                }
            }

            SQL.AppendLine("Select CompetenceCode As Col1, CompetenceName As Col2 ");
            SQL.AppendLine("From TblCompetence ");
            SQL.AppendLine("Where CompetenceLevelType = '1' ");
            SQL.AppendLine("And ActiveInd = 'Y' ");
            
            //if(mCompetenceCode.Length > 0)
            //    SQL.AppendLine("And Not Find_In_Set(CompetenceCode, '"+mCompetenceCode+"') ");
            
            SQL.AppendLine("Order By CompetenceName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                 0, 35, false, true, "Code", "Potence", "Col2", "Col1");
        }

        internal string GetSelectedCompetenceCode()
        {
            var mCompetenceCode = string.Empty;

            if(Grd7.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 2).Length != 0)
                        mCompetenceCode += "##" + Sm.GetGrdStr(Grd7, Row, 2) + "##";
                }
            }

            return (mCompetenceCode.Length == 0 ? "##XXX##" : mCompetenceCode);
        }

        public static void SetLueEducationCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.OptCode As Col1, T.OptDesc As Col2 " +
                "From TblOption T Where Optcat = 'EmployeeEducationLevel' ; ",
                 0, 35, false, true, "Code", "Education", "Col2", "Col1");
        }

        public static void SetLueMajorCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.MajorCode As Col1, T.MajorName As Col2 " +
                "From TblMajor T Order By T.MajorName; ",
                 0, 35, false, true, "Code", "Major", "Col2", "Col1");
        }

        public static void SetLueCompetenceCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.CompetenceCode As Col1, T.CompetenceName As Col2 " +
                "From TblCompetence T Where " + 
                (Sm.GetParameter("DocTitle") == "SIER" ? "CompetenceLevelType IN ('1','2','3') " : "CompetenceLevelType = '2'") 
                + " Order By T.CompetenceName; ",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        private void PrintData(string PosCode)
        {
            var l = new List<OrgStructure>();
            var l1 = new List<OrgStructure1>();
            var l2 = new List<OrgStructure2>();
            var l3 = new List<OrgStructure3>();
            var l4 = new List<OrgStructure4>();


            string[] TableName = { "OrgStructure", "OrgStructure1", "OrgStructure2", "OrgStructure3", "OrgStructure4" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As CompanyName, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As CompanyPhone, ");
            SQL.AppendLine("A.PosCode, A.Level, B.Posname, C.EmpName, E.Info  ");
            SQL.AppendLine("From TblOrganizationalStructure A ");
            SQL.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Inner Join TblEmployee C On B.PosCode = C.PosCode ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X.EmpCOde, If(X.ScoreEmp>Y.MinScore, 'S', 'W') As Info ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.PosCode, B.CompetenceCode, Sum(B.Score) As ScoreEmp  ");
            SQL.AppendLine("        From TblEmployee A ");
            SQL.AppendLine("        Inner Join TblEmployeeCompetence B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("        Group BY EmpCode, PosCode, CompetenceCode ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Left Join TblOrganizationalStrucTureDtl4 Y On X.PosCode = Y.PosCode And X.CompetenceCode = Y.CompetenceCode ");
            SQL.AppendLine(")E On C.EmpCode = E.EmpCode ");

            SQL.AppendLine("Where A.Level=0 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",
                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "PosCode", 
                         "PosName",
                         "Level",
                         //6-7
                         "CompanyLogo",
                         "EmpName",
                         "Info"
                       });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OrgStructure()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            Level = Sm.DrInt(dr, c[5]),
                            CompanyLogo = Sm.DrStr(dr, c[6]),
                            EmpName = Sm.DrStr(dr, c[7]),
                            Info = Sm.DrStr(dr, c[8]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Header 1
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.PosCode, A.Level, B.Posname, C.EmpName, E.Info  ");
            SQL1.AppendLine("From TblOrganizationalStructure A ");
            SQL1.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            SQL1.AppendLine("Inner Join TblEmployee C On B.PosCode = C.PosCode ");

            SQL1.AppendLine("Left Join  ");
            SQL1.AppendLine("( ");
            SQL1.AppendLine("    Select X.EmpCOde, If(X.ScoreEmp>Y.MinScore, 'S', 'W') As Info ");
            SQL1.AppendLine("    From ( ");
            SQL1.AppendLine("        Select A.EmpCode, A.PosCode, B.CompetenceCode, Sum(B.Score) As ScoreEmp  ");
            SQL1.AppendLine("        From TblEmployee A ");
            SQL1.AppendLine("        Inner Join TblEmployeeCompetence B On A.EmpCode = B.EmpCode ");
            SQL1.AppendLine("        Group BY EmpCode, PosCode, CompetenceCode ");
            SQL1.AppendLine("    )X ");
            SQL1.AppendLine("    Left Join TblOrganizationalStrucTureDtl4 Y On X.PosCode = Y.PosCode And X.CompetenceCode = Y.CompetenceCode ");
            SQL1.AppendLine(")E On C.EmpCode = E.EmpCode ");

            SQL1.AppendLine("Where A.Level=1 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL1.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "PosCode", 
                        //1-3
                         "PosName",
                         "Level",
                         "EmpName",
                         "Info"
                       });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new OrgStructure1()
                        {

                            PosCode = Sm.DrStr(dr, c[0]),
                            PosName = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrInt(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                            Info = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Header 2
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.PosCode, A.Level, B.Posname, C.EmpName, E.Info  ");
            SQL2.AppendLine("From TblOrganizationalStructure A ");
            SQL2.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            SQL2.AppendLine("Inner Join TblEmployee C On B.PosCode = C.PosCode ");

            SQL2.AppendLine("Left Join  ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("    Select X.EmpCOde, If(X.ScoreEmp>Y.MinScore, 'S', 'W') As Info ");
            SQL2.AppendLine("    From ( ");
            SQL2.AppendLine("        Select A.EmpCode, A.PosCode, B.CompetenceCode, Sum(B.Score) As ScoreEmp  ");
            SQL2.AppendLine("        From TblEmployee A ");
            SQL2.AppendLine("        Inner Join TblEmployeeCompetence B On A.EmpCode = B.EmpCode ");
            SQL2.AppendLine("        Group BY EmpCode, PosCode, CompetenceCode ");
            SQL2.AppendLine("    )X ");
            SQL2.AppendLine("    Left Join TblOrganizationalStrucTureDtl4 Y On X.PosCode = Y.PosCode And X.CompetenceCode = Y.CompetenceCode ");
            SQL2.AppendLine(")E On C.EmpCode = E.EmpCode ");

            SQL2.AppendLine("Where A.Level=2 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL2.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "PosCode", 
                        //1-3
                         "PosName",
                         "Level",
                         "EmpName",
                         "Info"
                       });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new OrgStructure2()
                        {

                            PosCode = Sm.DrStr(dr, c[0]),
                            PosName = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrInt(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                            Info = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Header 3
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select A.PosCode, A.Level, B.Posname, C.EmpName, E.Info  ");
            SQL3.AppendLine("From TblOrganizationalStructure A ");
            SQL3.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            SQL3.AppendLine("Inner Join TblEMployee C On A.PosCode = C.PosCode");

            SQL3.AppendLine("Left Join  ");
            SQL3.AppendLine("( ");
            SQL3.AppendLine("    Select X.EmpCOde, If(X.ScoreEmp>Y.MinScore, 'S', 'W') As Info ");
            SQL3.AppendLine("    From ( ");
            SQL3.AppendLine("        Select A.EmpCode, A.PosCode, B.CompetenceCode, Sum(B.Score) As ScoreEmp  ");
            SQL3.AppendLine("        From TblEmployee A ");
            SQL3.AppendLine("        Inner Join TblEmployeeCompetence B On A.EmpCode = B.EmpCode ");
            SQL3.AppendLine("        Group BY EmpCode, PosCode, CompetenceCode ");
            SQL3.AppendLine("    )X ");
            SQL3.AppendLine("    Left Join TblOrganizationalStrucTureDtl4 Y On X.PosCode = Y.PosCode And X.CompetenceCode = Y.CompetenceCode ");
            SQL3.AppendLine(")E On C.EmpCode = E.EmpCode ");

            SQL3.AppendLine("Where A.Level=3 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL3.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "PosCode", 
                        //1-3
                         "PosName",
                         "Level",
                         "EmpName",
                         "Info"
                       });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new OrgStructure3()
                        {

                            PosCode = Sm.DrStr(dr, c[0]),
                            PosName = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrInt(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                            Info = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l3);
            #endregion

            #region Header 4
            var SQL4 = new StringBuilder();

            SQL4.AppendLine("Select A.PosCode, A.Level, B.Posname, C.EmpName, E.Info  ");
            SQL4.AppendLine("From TblOrganizationalStructure A ");
            SQL4.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            SQL4.AppendLine("Inner Join TblEmployee C On B.PosCode = C.PosCode");

            SQL4.AppendLine("Left Join  ");
            SQL4.AppendLine("( ");
            SQL4.AppendLine("    Select X.EmpCOde, If(X.ScoreEmp>Y.MinScore, 'S', 'W') As Info ");
            SQL4.AppendLine("    From ( ");
            SQL4.AppendLine("        Select A.EmpCode, A.PosCode, B.CompetenceCode, Sum(B.Score) As ScoreEmp  ");
            SQL4.AppendLine("        From TblEmployee A ");
            SQL4.AppendLine("        Inner Join TblEmployeeCompetence B On A.EmpCode = B.EmpCode ");
            SQL4.AppendLine("        Group BY EmpCode, PosCode, CompetenceCode ");
            SQL4.AppendLine("    )X ");
            SQL4.AppendLine("    Left Join TblOrganizationalStrucTureDtl4 Y On X.PosCode = Y.PosCode And X.CompetenceCode = Y.CompetenceCode ");
            SQL4.AppendLine(")E On C.EmpCode = E.EmpCode ");

            SQL4.AppendLine("Where A.Level=4 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL4.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "PosCode", 
                        //1-3
                         "PosName",
                         "Level",
                         "EmpName"
                       });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new OrgStructure4()
                        {

                            PosCode = Sm.DrStr(dr, c[0]),
                            PosName = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrInt(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                            Info = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l4);
            #endregion

            Sm.PrintReport("OrgStructure", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Grid Events

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                if (Sm.GetGrdStr(Grd2, 0, 1).Length != 0)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        Grd2.Cells[Row, 1].Value = Sm.GetGrdStr(Grd2, 0, 1);
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
            Sm.GrdKeyDown(Grd8, e, BtnFind, BtnSave);
        }

        private void Grd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd9, e, BtnSave);
            Sm.GrdKeyDown(Grd9, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
        }

        private void Grd3_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
            {
                //if ((Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex)) && mStateIndicator != 2 )
                if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
                {
                    LueRequestEdit(Grd4, LueCompetenceCode, ref fCell, ref fAccept, e);
                    SetLueCompetenceCode(ref LueCompetenceCode);
                }
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 3 });
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd5, e.RowIndex);

            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex) && BtnSave.Enabled)
            {
                LueRequestEdit(Grd5, LueLevel, ref fCell, ref fAccept, e);
                Sl.SetLueLevelCode(ref LueLevel);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd6, LueEducation, ref fCell, ref fAccept, e);
                SetLueEducationCode(ref LueEducation);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                LueRequestEdit(Grd6, LueMajor, ref fCell, ref fAccept, e);
                SetLueMajorCode(ref LueMajor);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 1, 5, 6 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                e.DoDefault = false;
                if(e.ColIndex == 1)
                    Sm.FormShowDialog(new FrmOrganizationalStructureDlg(this));
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }
        }

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmOrganizationalStructureDlg(this));
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
            Sm.GrdKeyDown(Grd7, e, BtnFind, BtnSave);
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
        }

        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd9, e.RowIndex);
        }

        #endregion

        #region Misc Control Event

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueCompetenceCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceCode, new Sm.RefreshLue1(SetLueCompetenceCode));
        }

        private void LueCompetenceCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueCompetenceCode_Leave(object sender, EventArgs e)
        {
            if (LueCompetenceCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueCompetenceCode).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 4].Value =
                    Grd4.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueCompetenceCode);
                    Grd4.Cells[fCell.RowIndex, 1].Value = LueCompetenceCode.GetColumnValue("Col2");
                }
                LueCompetenceCode.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd4, (fCell.RowIndex + 1), new int[] { 3 });
            }
        }

        private void TxtJobHolderQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtJobHolderQty, 0);
        }

        

        private void LueEducation_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEducation, new Sm.RefreshLue1(SetLueEducationCode));
        }

        private void LueMajor_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMajor, new Sm.RefreshLue1(SetLueMajorCode));
        }

        private void LueEducation_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueMajor_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueEducation_Leave(object sender, EventArgs e)
        {
            if (LueEducation.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueEducation).Length == 0)
                    Grd6.Cells[fCell.RowIndex, 1].Value =
                    Grd6.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueEducation);
                    Grd6.Cells[fCell.RowIndex, 2].Value = LueEducation.GetColumnValue("Col2");
                }
                LueEducation.Visible = false;
            }
        }

        private void LueMajor_Leave(object sender, EventArgs e)
        {
            if (LueMajor.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueMajor).Length == 0)
                    Grd6.Cells[fCell.RowIndex, 3].Value =
                    Grd6.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueMajor);
                    Grd6.Cells[fCell.RowIndex, 4].Value = LueMajor.GetColumnValue("Col2");
                }
                LueMajor.Visible = false;
            }
        }

        private void TxtHardCompetence_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                var mHC = Decimal.Parse(TxtHardCompetence.Text);
                if (mHC != 0)
                {
                    if (mHC < 0) mHC *= -1;

                    TxtHardCompetence.EditValue = Sm.FormatNum(mHC, 2);
                    TxtSoftCompetence.EditValue = Sm.FormatNum((100m - mHC), 2);
                }
            }
        }

        private void TxtSoftCompetence_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                var mSC = Decimal.Parse(TxtSoftCompetence.Text);
                if (mSC != 0)
                {
                    if (mSC < 0) mSC *= -1;

                    TxtSoftCompetence.EditValue = Sm.FormatNum(mSC, 2);
                    TxtHardCompetence.EditValue = Sm.FormatNum((100m - mSC), 2);
                }
            }
        }

        private void TxtMinAge_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                var mMin = Decimal.Parse(TxtMinAge.Text);
                if (mMin != 0)
                {
                    if (mMin < 0) mMin *= -1;

                    TxtMinAge.EditValue = Sm.FormatNum(mMin, 2);
                }
            }
        }

        private void TxtMaxAge_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                var mMax = Decimal.Parse(TxtMaxAge.Text);
                if (mMax != 0)
                {
                    if (mMax < 0) mMax *= -1;

                    TxtMaxAge.EditValue = Sm.FormatNum(mMax, 2);
                }
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void LueLevel_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueLevel_Leave(object sender, EventArgs e)
        {
            if (LueLevel.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueLevel).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 3].Value =
                    Grd5.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueLevel);
                    Grd5.Cells[fCell.RowIndex, 4].Value = LueLevel.GetColumnValue("Col2");
                }
                LueLevel.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class OrgStructure
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string PosCode { set; get; }
            public string PosName { set; get; }
            public string EmpName { set; get; }
            public Int32 Level { get; set; }
            public string Info { set; get; }
        }


        private class OrgStructure1
        {
            public string PosCode { set; get; }
            public string PosName { set; get; }
            public Int32 Level { get; set; }
            public string EmpName { set; get; }
            public string Info { set; get; }

        }

        private class OrgStructure2
        {
            public string PosCode { set; get; }
            public string PosName { set; get; }
            public Int32 Level { get; set; }
            public string EmpName { set; get; }
            public string Info { set; get; }

        }

        private class OrgStructure3
        {
            public string PosCode { set; get; }
            public string PosName { set; get; }
            public Int32 Level { get; set; }
            public string EmpName { set; get; }
            public string Info { set; get; }

        }

        private class OrgStructure4
        {
            public string PosCode { set; get; }
            public string PosName { set; get; }
            public Int32 Level { get; set; }
            public string EmpName { set; get; }
            public string Info { set; get; }

        }


        #endregion

    }
}
