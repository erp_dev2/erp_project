﻿#region Update
/*
    20/06/2017 [TKG] Update remark di journal
    13/07/2017 [WED] Tambah entity di JournalDtl
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    18/11/2019 [DITA/MAI] tambah tab kawasan berikat
    31/08/2020 [WED/IOK] validasi gabisa cancel kalau udah dibikin PRI nya
    08/09/2020 [TKG/IOK] ubah query cancel data
    10/08/2021 [TRI/ALL] tambah Validasi tidak bisa SAVE untuk transaksi yang terbentuk jurnal otomatis ketika terdapat setting COA otomatis yang masih kosong
    15/03/2022 [TKG/GSS] merubah GetParameter dan proses save
    13/10/2022 [HPH/PRODUCT] Membuat amount dari Return berubah menjadi AP deposit customer terpilih ketika transaksi yang berdasarkan DO sudah terbayar semua
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOVd : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mPEBGrpCode = string.Empty,
            mPEBDocType = string.Empty,
            mPEBFilePath = string.Empty,
            mPEBPassword = string.Empty,
        mDepositVendorReturnSource = string.Empty;
        // mCustomsDocCodeDOVd = string.Empty; 
        private string mEntCode = string.Empty, mDocType="02";
        internal int mNumberOfInventoryUomCode = 1;
        internal FrmDOVdFind FrmFind;
        internal bool mIsItGrpCodeShow = false,
            mIsCheckCOAJournalNotExists = false,
            mIsKawasanBerikatEnabled = false;
        internal bool mIsAutoGeneratePurchaseLocalDocNo = false;
        private bool mIsAutoJournalActived = false;
        private List<LocalDocument> mlLocalDocument = null;

        #endregion

        #region Constructor

        public FrmDOVd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "DO To Vendor (Returning Item)";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled)
                {
                    Tp2.PageVisible = true;
                    if (!IsAuthorizedToAccessPEB())
                    {
                        BtnPEB.Visible = false;
                    }
                }
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueVdCode(ref LueVdCode);
                mlLocalDocument = new List<LocalDocument>();
                if (mIsKawasanBerikatEnabled)
                {
                    TcRecvVd.SelectedTabPage = Tp2;
                    Sl.SetLueOption(ref LueCustomsDocCode, "CustomsDocCodeDOVd");
                }
                TcRecvVd.SelectedTabPage = Tp1;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsItGrpCodeShow', 'IsAutoGeneratePurchaseLocalDocNo', 'IsAutoJournalActived', 'IsCheckCOAJournalNotExists', 'KB_Server', ");
            SQL.AppendLine("'PEBGrpCode', 'PEBDocType', 'PEBFilePath', 'PEBPassword', 'NumberOfInventoryUomCode', 'DepositVendorReturnSource' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": mIsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;

                            //string
                            case "KB_Server": mIsKawasanBerikatEnabled = ParValue.Length>0; break;
                            case "PEBGrpCode": mPEBGrpCode = ParValue; break;
                            case "PEBDocType": mPEBDocType = ParValue; break;
                            case "PEBFilePath": mPEBFilePath = ParValue; break;
                            case "PEBPassword": mPEBPassword = ParValue; break;
                            case "DepositVendorReturnSource": mDepositVendorReturnSource = ParValue; break;

                            //integer
                            case "NumberOfInventoryUomCode": 
                                if (ParValue.Length==0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
            if (mPEBDocType.Length <= 0) mPEBDocType = "30";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 5;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                       //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Received#",
                        "Received DNo",
                        
                        //6-10
                        "Date",
                        "Item Code",
                        "",
                        "Item Name",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",
                        
                        //21-25
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Currency",
                        "Price",

                        //26-27
                        "Group",
                        "Local#"
                    },
                     new int[] 
                    {
                        20, 
                        50, 0, 20, 150, 0, 
                        100, 80, 20, 200, 150, 
                        180, 80, 80, 80, 80, 
                        80, 80, 80, 80, 80, 
                        80, 80, 250, 80, 100,
                        100, 150
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 24, 25, 26, 27 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21, 25 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 11, 12, 13, 17, 18, 19, 20, 21, 22, 24, 25, 26 }, false);
            ShowInventoryUomCode();
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[26].Visible = true;
                Grd1.Cols[26].Move(9);
            }
            Grd1.Cols[27].Move(5);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtLocalDocNo, ChkReplacedItemInd, DteDocDt, LueWhsCode, LueVdCode, MeeRemark,
                        LueCustomsDocCode, TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkReplacedItemInd, DteDocDt, LueWhsCode, LueVdCode, MeeRemark, LueCustomsDocCode
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    Grd1.ReadOnly = false;
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, MeeRemark,
                TxtKBContractNo, DteKBContractDt,
                TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, DteKBRegistrationDt, TxtKBSubmissionNo,
                TxtKBPackaging, LueCustomsDocCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKBPackagingQty }, 0);
            mEntCode = string.Empty;
            //mCustomsDocCodeDOVd = string.Empty;
            ChkReplacedItemInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 25 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOVdFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkReplacedItemInd.Checked)
                Sm.StdMsg(mMsgType.Warning, "You can't cancel returning transaction for replaced items.");
            else
                SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue(
                            "Select C.EntCode From TblWarehouse A, TblCostCenter B, TblProfitCenter C " +
                            "Where A.CCCode = B.CCCode And B.ProfitCenterCode = C.ProfitCenterCode And A.WhsCode=@Param limit 1;", 
                            Sm.GetLue(LueWhsCode));
                
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetParameter("NumberOfInventoryUomCode");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "DOVdHdr", "DOVdDtl" };

            var l = new List<DOVdHdr>();
            var ldtl = new List<DOVdDtl>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y') As DocDt,B.WhsName,C.VdName,A.Remark ");
            SQL.AppendLine("From TblDoVdHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",
                         //1-5
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",
                         "WhsName",
                         //6-8
                         "VdName",
                         "Remark",
                         "CompanyLogo",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOVdHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            WhsName = Sm.DrStr(dr, c[5]),
                            VdName = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            CompanyLogo = Sm.DrStr(dr, c[8]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.RecvVdDocNo,Date_Format(C.DocDt,'%d %M %Y') As DocDt, ");
                SQLDtl.AppendLine("A.ItCode,B.ItName,A.BatchNo,A.Lot,A.Bin,");
                SQLDtl.AppendLine("A.Qty,A.Qty2,A.Qty3,B.InventoryUOMCode,B.InventoryUOMCode2,B.InventoryUOMCode3, ");
                SQLDtl.AppendLine("A.Remark, B.ItGrpCode ");
                SQLDtl.AppendLine("From TblDoVdDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblRecvVdHdr C On A.RecvVdDocNo=C.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "RecvVdDocNo",
                         //1-5
                         "DocDt",
                         "ItCode",
                         "ItName",
                         "BatchNo",
                         "Lot",
                         //6-10
                         "Bin",
                         "Qty",
                         "Qty2",
                         "Qty3",
                         "InventoryUOMCode",
                         //11-14
                         "InventoryUOMCode2",
                         "InventoryUOMCode3",
                         "Remark",
                         "ItGrpCode"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new DOVdDtl()
                        {
                            RecvVdDocNo = Sm.DrStr(drDtl, cDtl[0]),
                            DocDt = Sm.DrStr(drDtl, cDtl[1]),
                            ItCode = Sm.DrStr(drDtl, cDtl[2]),
                            ItName = Sm.DrStr(drDtl, cDtl[3]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[4]),
                            Lot = Sm.DrStr(drDtl, cDtl[5]),
                            Bin = Sm.DrStr(drDtl, cDtl[6]),
                            Qty = Sm.DrDec(drDtl, cDtl[7]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[8]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[9]),
                            InventoryUOMCode = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUOMCode2 = Sm.DrStr(drDtl, cDtl[11]),
                            InventoryUOMCode3 = Sm.DrStr(drDtl, cDtl[12]),
                            Remark = Sm.DrStr(drDtl, cDtl[13]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[14])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            int a = int.Parse(ParValue);
            if (a == 1)
            {
                Sm.PrintReport("DOVd1", myLists, TableName, false);
            }
            else if (a == 2)
            {
                Sm.PrintReport("DOVd2", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("DOVd3", myLists, TableName, false);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && TxtDocNo.Text.Length == 0 && 
                !Sm.IsLueEmpty(LueVdCode, "Vendor") && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOVdDlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LueWhsCode)));
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                    ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && !IsRecvVdDocNoEmpty(e) && Sm.IsGrdColSelected(new int[] { 15, 18, 21, 23 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 25 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && TxtDocNo.Text.Length == 0 &&
                !Sm.IsLueEmpty(LueVdCode, "Vendor") && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmDOVdDlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15, 18, 21 }, e.ColIndex) &&
                Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0;

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 15, 18, 21, 16, 19, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 7, 15, 21, 18, 16, 22, 19);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 18, 15, 21, 19, 16, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 7, 18, 21, 15, 19, 22, 16);
            }

            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 7, 21, 15, 18, 22, 16, 19);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 7, 21, 18, 15, 22, 19, 16);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            ComputeStock();

            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsInsertedDataNotValid()) return;

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;


            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
                if (IsLocalDocumentNotValid(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    )) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length>0)
                {
                    SetLocalDocNo(
                        "DOVd",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOVd", "TblDOVdHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOVd(
                DocNo,
                LocalDocNo,
                SeqNo,
                DeptCode,
                ItSCCode,
                Mth,
                Yr,
                Revision
                ));

            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveDOVdDtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));
            
            if (ChkReplacedItemInd.Checked || mDepositVendorReturnSource == "2")
            {
                cml.Add(SaveVendorDeposit(DocNo));
                cml.Add(UpdatePOProcessInd(DocNo));
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblDOVdHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 4));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";


            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblRecvVdHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCurCodeNotValid() ||
                IsJournalSettingInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 received document#.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Received document# is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 15, true, "Quantity is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 18, true, "Quantity 2 is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 21, true, "Quantity 3 is empty.")) return true;

                if (Sm.GetGrdStr(Grd1, Row, 16).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 16), Sm.GetGrdStr(Grd1, Row, 19)) &&
                    Sm.GetGrdDec(Grd1, Row, 15) != Sm.GetGrdDec(Grd1, Row, 18))
                {
                    Sm.StdMsg(mMsgType.Warning,
                         "Received# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                         "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                         "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                         "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                         "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                         "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                         "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + 
                         "UoM 1 : " + Sm.GetGrdStr(Grd1, Row, 16) + Environment.NewLine + 
                         "UoM 2 : " + Sm.GetGrdStr(Grd1, Row, 19) + Environment.NewLine + Environment.NewLine +
                         "Quantity 1 (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 15), 0) + ") is different from quantity 2 (" +
                            Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0) + ")."
                         );
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, Row, 16).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 16), Sm.GetGrdStr(Grd1, Row, 22)) &&
                    Sm.GetGrdDec(Grd1, Row, 15) != Sm.GetGrdDec(Grd1, Row, 21))
                {
                    Sm.StdMsg(mMsgType.Warning,
                         "Received# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                         "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                         "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                         "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                         "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                         "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                         "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                         "UoM 1 : " + Sm.GetGrdStr(Grd1, Row, 16) + Environment.NewLine +
                         "UoM 3 : " + Sm.GetGrdStr(Grd1, Row, 22) + Environment.NewLine + Environment.NewLine +
                         "Quantity 1 (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 15), 0) + ") is different from quantity 3 (" +
                            Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 21), 0) + ")."
                         );
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, Row, 19).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), Sm.GetGrdStr(Grd1, Row, 22)) &&
                    Sm.GetGrdDec(Grd1, Row, 18) != Sm.GetGrdDec(Grd1, Row, 21))
                {
                    Sm.StdMsg(mMsgType.Warning,
                         "Received# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                         "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                         "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                         "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                         "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                         "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                         "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                         "UoM 2 : " + Sm.GetGrdStr(Grd1, Row, 19) + Environment.NewLine +
                         "UoM 3 : " + Sm.GetGrdStr(Grd1, Row, 22) + Environment.NewLine + Environment.NewLine +
                         "Quantity 2 (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0) + ") is different from quantity 3 (" +
                            Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 21), 0) + ")."
                         );
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                {
                    Sm.StdMsg(mMsgType.Warning,
                         "Received# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                         "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                         "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                         "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                         "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                         "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                         "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                         "Quantity 1 (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 15), 0) + ") is bigger than stock (" +
                         Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 14), 0) + ")."
                         );
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                {
                    Sm.StdMsg(mMsgType.Warning,
                         "Received# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                         "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                         "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                         "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                         "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                         "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                         "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                         "Quantity 2 (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0) + ") is bigger than stock (" +
                         Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 17), 0) + ")."
                         );
                    return true;
                }


                if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                {
                    Sm.StdMsg(mMsgType.Warning,
                         "Received# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                         "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                         "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                         "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                         "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                         "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                         "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                         "Quantity 3 (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 21), 0) + ") is bigger than stock (" +
                         Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 20), 0) + ")."
                         );
                    return true;
                }
            }
            return false;
        }

        private bool IsCurCodeNotValid()
        {
            if (!ChkReplacedItemInd.Checked) return false;

            string CurCode = Sm.GetGrdStr(Grd1, 0, 24), CurCode2 = string.Empty;
            
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                CurCode2 = Sm.GetGrdStr(Grd1, Row, 24);
                if (CurCode2.Length>0 && !Sm.CompareStr(CurCode, CurCode2))
                { 
                    Sm.StdMsg(mMsgType.Warning, "You need to split this received# because currency issue.");
                    return true;
                }
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            string mVendorAcNoUnInvoiceAP = Sm.GetValue("Select ParValue From TblParameter Where Parcode='VendorAcNoUnInvoiceAP'");
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter
            if (mVendorAcNoUnInvoiceAP.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter VendorAcNoUnInvoiceAP is empty.");
                return true;
            }

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.Acno Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 7);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDOVd(
            string DocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Return To Vendor */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblDOVdHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("WhsCode, VdCode, ReplacedItemInd, Remark, CreateBy, CreateDt, ");
            SQL.AppendLine("CustomsDocCode, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, KBPackaging, KBPackagingQty) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("@WhsCode, @VdCode, @ReplacedItemInd, @Remark, @UserCode, CurrentDateTime(), ");
            SQL.AppendLine("@CustomsDocCode, @KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @KBSubmissionNo, @KBPackaging, @KBPackagingQty); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDOVdDtl(DocNo, DNo, CancelInd, RecvVdDocNo, RecvVdDNo, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @RecvVdDocNo_" + r.ToString() +
                        ", @RecvVdDNo_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @BatchNo_" + r.ToString() +
                        ", @Source_" + r.ToString() +
                        ", @Lot_" + r.ToString() +
                        ", @Bin_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@RecvVdDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@RecvVdDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                    Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 21));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 23));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@ReplacedItemInd", ChkReplacedItemInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", Sm.GetLue(LueCustomsDocCode));
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@KBPackaging", TxtKBPackaging.Text);
            Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", decimal.Parse(TxtKBPackagingQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveDOVdDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblDOVdDtl(DocNo, DNo, CancelInd, RecvVdDocNo, RecvVdDNo, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt)");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'N', @RecvVdDocNo, @RecvVdDNo, @ItCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@RecvVdDocNo", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@RecvVdDNo", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark,  ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOVdHdr A ");
            SQL.AppendLine("Inner Join TblDOVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join TblStockMovement T2 On T1.WhsCode=T2.WhsCode And T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source And T1.Lot=T2.Lot And T1.Bin=T2.Bin And T2.DocType=@DocType And T2.DocNo=@DocNo ");
            SQL.AppendLine("Set T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "02");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorDeposit(string DocNo)
        {
            var SQL = new StringBuilder();
            
            string CurCode = Sm.GetGrdStr(Grd1, 0, 24);
            
            Decimal Amt = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0) 
                    Amt += (Sm.GetGrdDec(Grd1, Row, 15)*Sm.GetGrdDec(Grd1, Row, 25));

            SQL.AppendLine("Insert Into TblVendorDepositMovement ");
            SQL.AppendLine("(DocNo, DocType, DocDt, VdCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, '07', @DocDt, @VdCode, @CurCode, @Amt, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VdCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdatePOProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
            SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.PODocNo As DocNo, T.PODNo As DNo, Sum(T.QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl T ");
            SQL.AppendLine("        Where Exists( ");
            SQL.AppendLine("            Select X3.PODocNo ");
            SQL.AppendLine("            From TblDOVdHdr X1, TblDOVdDtl X2, TblRecvVdDtl X3 ");
            SQL.AppendLine("            Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            And X2.CancelInd='N' ");
            SQL.AppendLine("            And X2.RecvVdDocNo=X3.DocNo ");
            SQL.AppendLine("            And X2.RecvVdDNo=X3.DNo ");
            SQL.AppendLine("            And X3.CancelInd='N' ");
            SQL.AppendLine("            And X3.PODocNo=T.PODocNo ");
            SQL.AppendLine("            And X3.PODNo=T.PODNo ");
            SQL.AppendLine("            And X1.ReplacedItemInd='Y' ");
            SQL.AppendLine("            And X1.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And T.CancelInd='N' ");
            SQL.AppendLine("        Group By T.PODocNo, T.PODNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.PODocNo As DocNo, T.PODNo As DNo, Sum(T.Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel T ");
            SQL.AppendLine("        Where Exists( ");
            SQL.AppendLine("            Select X3.PODocNo ");
            SQL.AppendLine("            From TblDOVdHdr X1, TblDOVdDtl X2, TblRecvVdDtl X3 ");
            SQL.AppendLine("            Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            And X2.CancelInd='N' ");
            SQL.AppendLine("            And X2.RecvVdDocNo=X3.DocNo ");
            SQL.AppendLine("            And X2.RecvVdDNo=X3.DNo ");
            SQL.AppendLine("            And X3.CancelInd='N' ");
            SQL.AppendLine("            And X3.PODocNo=T.PODocNo ");
            SQL.AppendLine("            And X3.PODNo=T.PODNo ");
            SQL.AppendLine("            And X1.ReplacedItemInd='Y' ");
            SQL.AppendLine("            And X1.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And T.CancelInd='N' ");
            SQL.AppendLine("        Group By T.PODocNo, T.PODNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
            SQL.AppendLine("        From TblDOVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("            On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("            And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("            And T3.CancelInd='N' ");
            SQL.AppendLine("            And Exists( ");
            SQL.AppendLine("                Select X3.PODocNo ");
            SQL.AppendLine("                From TblDOVdHdr X1, TblDOVdDtl X2, TblRecvVdDtl X3 ");
            SQL.AppendLine("                Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.CancelInd='N' ");
            SQL.AppendLine("                And X2.RecvVdDocNo=X3.DocNo ");
            SQL.AppendLine("                And X2.RecvVdDNo=X3.DNo ");
            SQL.AppendLine("                And X3.CancelInd='N' ");
            SQL.AppendLine("                And X3.PODocNo=T3.PODocNo ");
            SQL.AppendLine("                And X3.PODNo=T3.PODNo ");
            SQL.AppendLine("                And X1.ReplacedItemInd='Y' ");
            SQL.AppendLine("                And X1.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("        Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Where Exists( ");
            SQL.AppendLine("                Select X3.PODocNo ");
            SQL.AppendLine("                From TblDOVdHdr X1, TblDOVdDtl X2, TblRecvVdDtl X3 ");
            SQL.AppendLine("                Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.CancelInd='N' ");
            SQL.AppendLine("                And X2.RecvVdDocNo=X3.DocNo ");
            SQL.AppendLine("                And X2.RecvVdDNo=X3.DNo ");
            SQL.AppendLine("                And X3.CancelInd='N' ");
            SQL.AppendLine("                And X3.PODocNo=A.DocNo ");
            SQL.AppendLine("                And X3.PODNo=A.DNo ");
            SQL.AppendLine("                And X1.ReplacedItemInd='Y' ");
            SQL.AppendLine("                And X1.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblDOVdHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Returning Item To Vendor : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDOVdHdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("        Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblDOVdHdr A ");
            SQL.AppendLine("        Inner Join TblDOVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select D.AcNo, 0 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblDOVdDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            UpdateCancelledData();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int r= 0; r< Grd1.Rows.Count;r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                    cml.Add(EditData(r));
            }
            //cml.Add(CancelDOVdDtl(TxtDocNo.Text, DNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblDOVdDtl  ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsDataAlreadyProcessedToPurchaseReturnInvoice(DNo);
        }

        private bool IsDataAlreadyProcessedToPurchaseReturnInvoice(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'")) return false;

            var SQL = new StringBuilder();
            string PRIDocNo = string.Empty;

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DOVdDocNo = @Param And A.DOVdDNo In (" + DNo + ") ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            PRIDocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            if (PRIDocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already processed to PRI#" + PRIDocNo);
                return true;
            }

            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelDOVdDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOVdDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join TblStockMovement T2 On T1.WhsCode=T2.WhsCode And T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source And T1.Lot=T2.Lot And T1.Bin=T2.Bin And T2.DocType=@DocType And T2.DocNo=@DocNo ");
            SQL.AppendLine("Set T1.Qty=T1.Qty-T2.Qty, T1.Qty2=T1.Qty2-T2.Qty2, T1.Qty3=T1.Qty3-T2.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblStockMovement Set Qty=0, Qty2=0, Qty3=0, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocType=@DocType And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "02");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditData(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOVdDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '02', A.DocNo, B.DNo, 'Y', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, B.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOVdHdr A ");
            SQL.AppendLine("Inner Join TblDOVdDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            int No = 1;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2))
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DNo=@DNo" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 0));
                    No += 1;
                }
            }

            Filter = " And (" + Filter + ")";

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblDOVdDtl A Set A.JournalDocNo=@JournalDocNo Where A.DocNo=@DocNo " + Filter + ";");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Returning Item To Vendor : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblDOVdHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("        Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblDOVdDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter);
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("        From TblDOVdHdr A ");
            SQL.AppendLine("        Inner Join TblDOVdDtl B On A.DocNo=B.DocNo " + Filter.Replace("A.", "B."));
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowDOVdHdr(DocNo);
                ShowDOVdDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOVdHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, VdCode, ReplacedItemInd, "+
                    "KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBPackaging, KBPackagingQty, KBNonDocInd, KBSubmissionNo, CustomsDocCode, Remark, LocalDocNo " +
                    " From TblDOVdHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "VdCode", "ReplacedItemInd", "Remark" ,
                        "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt","KBRegistrationNo", 
                        "KBRegistrationDt", "KBPackaging", "KBPackagingQty", "KBSubmissionNo","CustomsDocCode",
                        "LocalDocNo"
                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[3]));
                        ChkReplacedItemInd.Checked = Sm.DrStr(dr, c[4])=="Y";
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[7]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[9]));
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[11]));
                        TxtKBPackaging.EditValue = Sm.DrStr(dr, c[12]);
                        TxtKBPackagingQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetLue(LueCustomsDocCode, Sm.DrStr(dr, c[15]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[16]);
                    }, true
                );
        }

        private void ShowDOVdDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.CancelInd, A.RecvVdDocNo, A.RecvVdDNo, B.DocDt, ");
                SQL.AppendLine("A.ItCode, C.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
                SQL.AppendLine("A.Qty, C.InventoryUomCode, A.Qty2, C.InventoryUomCode2, A.Qty3, C.InventoryUomCode3, ");
                SQL.AppendLine("A.Remark, D.CurCode, D.UPrice, C.ItGrpCode ");
                SQL.AppendLine("From TblDOVdDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdHdr B On A.RecvVdDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Inner Join TblStockPrice D On A.Source=D.Source ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.ItCode;");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "RecvVdDocNo", "RecvVdDNo", "DocDt", "ItCode", 
                        
                        //6-10
                        "ItName", "BatchNo", "Source", "Lot", "Bin", 
                        
                        //11-15
                        "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 
                        
                        //16-20
                        "InventoryUomCode3", "Remark", "CurCode", "UPrice", "ItGrpCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Grd.Cells[Row, 14].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                        Grd.Cells[Row, 17].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                        Grd.Cells[Row, 20].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    }, false, false, true, false
                );
                Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 25 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private bool IsRecvVdDocNoEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal string GetSelectedRecvVd()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ComputeStock()
        {
            var SQL = new StringBuilder();
            string Key = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    Key += (
                        ", '" + 
                        Sm.GetLue(LueWhsCode) + 
                        Sm.GetGrdStr(Grd1, Row, 12) +
                        Sm.GetGrdStr(Grd1, Row, 13) +
                        Sm.GetGrdStr(Grd1, Row, 7) +
                        Sm.GetGrdStr(Grd1, Row, 10) +
                        Sm.GetGrdStr(Grd1, Row, 11) +
                        "'");
                }
            }
            SQL.AppendLine("Select Concat(WhsCode, Lot, Bin, ItCode, BatchNo, Source) As Key1, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Concat(WhsCode, Lot, Bin, ItCode, BatchNo, Source) In (" + Key + ") ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Key1", "Qty", "Qty2", "Qty3" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.GetGrdStr(Grd1, Row, 7).Length > 0 &&
                                Sm.CompareStr(
                                    Sm.DrStr(dr, 0),
                                        Sm.GetLue(LueWhsCode) +
                                        Sm.GetGrdStr(Grd1, Row, 12) +
                                        Sm.GetGrdStr(Grd1, Row, 13) +
                                        Sm.GetGrdStr(Grd1, Row, 7) +
                                        Sm.GetGrdStr(Grd1, Row, 10) +
                                        Sm.GetGrdStr(Grd1, Row, 11)
                                ))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 3);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsAuthorizedToAccessPEB()
        {
            bool mFlag = false;
            string mCurrentGrpCode = Sm.GetValue("Select GrpCode From TblUser Where UserCode = @Param; ", Gv.CurrentUserCode);

            string[] mPEBGrpCodeList = mPEBGrpCode.Split(',');
            foreach (string m in mPEBGrpCodeList)
            {
                if (m == mCurrentGrpCode)
                {
                    mFlag = true;
                    break;
                }
            }

            return mFlag;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                    DteKBRegistrationDt, TxtKBSubmissionNo, LueCustomsDocCode
                });
            ClearGrd();
            }
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueCustomsDocCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCustomsDocCode, new Sm.RefreshLue2(Sl.SetLueOption), "CustomsDocCodeDOVd");

                BtnKBContractNo.Enabled = true;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty
                }, true);


                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                    DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKBPackagingQty }, 0);
                ClearGrd();
            }
        }

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueVdCode, "Vendor") && !Sm.IsLueEmpty(LueCustomsDocCode, "Customs document code"))
                Sm.FormShowDialog(new FrmDOVdDlg2(this, Sm.GetLue(LueCustomsDocCode)));
        }

        private void TxtKBContractNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBContractNo);
        }

        private void TxtKBPLNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBPLNo);
        }

        private void TxtKBRegistrationNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBRegistrationNo);
        }

        private void TxtKBSubmissionNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBSubmissionNo);
        }

        private void TxtKBPackaging_Validated(object sender, EventArgs e)
        {

        }

        private void TxtKBPackagingQty_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtKBPackagingQty, 0);
        }

        private void BtnPEB_Click(object sender, EventArgs e)
        {
            if (mIsKawasanBerikatEnabled && IsAuthorizedToAccessPEB())
            {
                if (BtnSave.Enabled)
                {
                    Sm.FormShowDialog(new FrmDOVdDlg3(this));
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class DOVdHdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string VdName { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
        }

        private class DOVdDtl
        {
            public string RecvVdDocNo { get; set; }
            public string DocDt { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUOMCode { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public string InventoryUOMCode3 { get; set; }
            public string Remark { get; set; }
            public string ItGrpCode { get; set; }
        }

        #endregion

    }
}
