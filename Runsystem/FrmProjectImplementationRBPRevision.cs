﻿#region Update
/*
    12/08/2019 [TKG] New Application
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationRBPRevision : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mRevisionNo = string.Empty
            ;
        internal bool mIsFilterBySite = false;
        internal FrmProjectImplementationRBPRevisionFind FrmFind;

        #endregion

        #region Constructor

        public FrmProjectImplementationRBPRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectImplementationRBPRevision");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mRevisionNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                { 
                    "Month", 
                    "Year", "Remuneration", "Direct Cost", "Remark", "Dropping Request",
                    "DNo", "VendorCode", "Vendor", "Tax", "SS Health", 
                    "SS Employment", "Others", "Total"
                },
                new int[] 
                { 
                    50, 
                    80, 100, 100, 200, 130,
                    0, 0, 200, 120, 120, 
                    120, 120, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 9, 10, 11, 12, 13 }, 0);
            Grd1.Cols[8].Move(4);
            Grd1.Cols[4].Move(13);
            Grd1.Cols[5].Move(13);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtPRJIDocNo, 
                TxtResourceItCode, TxtResourceItName, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotalWithTax, TxtTotalDirectCost, TxtTotalRemuneration }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3, 9, 10, 11, 12, 13 });
        }

        #endregion

        #endregion

        #region Additional Method

        private void ComputeTotalRemuneration()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 0).Length != 0 || Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    if (Sm.GetGrdDec(Grd1, row, 2) != 0m)
                    {
                        Total += Sm.GetGrdDec(Grd1, row, 13);
                    }
                }
            }
            TxtTotalRemuneration.EditValue = Sm.FormatNum(Total, 0);
        }

        private void ComputeTotalDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 0).Length != 0 || Sm.GetGrdStr(Grd1, row, 1).Length != 0)
                {
                    if (Sm.GetGrdDec(Grd1, row, 3) != 0m)
                    {
                        Total += Sm.GetGrdDec(Grd1, row, 13);
                    }
                }
            }
            TxtTotalDirectCost.EditValue = Sm.FormatNum(Total, 0);
        }

        private void ComputeTotalDetail(int Row)
        {
            decimal Total = 0m;

            if (Sm.GetGrdDec(Grd1, Row, 2) != 0m)
            {
                Total = Sm.GetGrdDec(Grd1, Row, 2) + Sm.GetGrdDec(Grd1, Row, 9) +
                    Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 11) + Sm.GetGrdDec(Grd1, Row, 12);
            }

            if (Sm.GetGrdDec(Grd1, Row, 3) != 0m)
            {
                Total = Sm.GetGrdDec(Grd1, Row, 3) + Sm.GetGrdDec(Grd1, Row, 9) +
                    Sm.GetGrdDec(Grd1, Row, 12);
            }

            Grd1.Cells[Row, 13].Value = Total;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectImplementationRBPRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, "You can't insert new document.");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            Sm.StdMsg(mMsgType.Info, "You can't edit document.");
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo, string RevisionNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProjectImplementationRBPRevisionHdr(DocNo, RevisionNo);
                ShowProjectImplementationRBPRevisionDtl(DocNo, RevisionNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectImplementationRBPRevisionHdr(string DocNo, string RevisionNo)
        {
            TxtRevisionNo.EditValue = RevisionNo;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PRJIDocNo, A.ResourceItCode, C.ItName, B.Amt, ");
            SQL.AppendLine("A.TotalRemuneration, A.TotalDirectCost, A.Remark ");
            SQL.AppendLine("From TblProjectImplementationRBPRevisionHdr A ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 B ");
            SQL.AppendLine("    On A.PRJIDocNo=B.DocNo ");
            SQL.AppendLine("    And A.ResourceItCode=B.ResourceItCode ");
            SQL.AppendLine("Left Join TblItem C On A.ResourceItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.RevisionNo=@RevisionNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionNo", RevisionNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "PRJIDocNo", "ResourceItCode",  
                    
                    //6-10
                    "ItName", "Amt", "TotalRemuneration", "TotalDirectCost", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtResourceItCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtResourceItName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtTotalWithTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtTotalRemuneration.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtTotalDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        private void ShowProjectImplementationRBPRevisionDtl(string DocNo, string RevisionNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.Mth, A.Yr, A.RemunerationAmt, A.DirectCostAmt, A.Remark, C.DocNo, A.DNo, A.VdCode, D.VdName, ");
            SQLDtl.AppendLine("A.TaxAmt, A.SSHealthAmt, A.SSEmploymentAmt, A.OthersAmt, A.TotalAmt ");
            SQLDtl.AppendLine("From TblProjectImplementationRBPRevisionDtl A ");
            SQLDtl.AppendLine("Left Join TblProjectImplementationRBPRevisionHdr B On A.DocNo=B.DocNo And A.RevisionNo=B.RevisionNo ");
            SQLDtl.AppendLine("Left Join TblDroppingRequestHdr C On B.PRJIDocNo=C.PRJIDocNo And A.Yr=C.Yr And A.Mth=C.Mth ");
            SQLDtl.AppendLine("Left Join TblVendor D On A.VdCode=D.VdCode ");
            SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
            SQLDtl.AppendLine("And A.DocNo=@DocNo And A.RevisionNo=@RevisionNo ");
            SQLDtl.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionNo", RevisionNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    "Mth", 
                    "Yr", "RemunerationAmt", "DirectCostAmt", "Remark", "DocNo",
                    "DNo", "VdCode", "VdName", "TaxAmt", "SSHealthAmt",
                    "SSEmploymentAmt", "OthersAmt", "TotalAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 9, 10, 11, 12, 13 });
        }

        private void SetYrMthInfo()
        {
            var SQL = new StringBuilder();
            string 
                Mth = string.Empty, 
                Yr = string.Empty, 
                Remark = string.Empty, 
                DocNo = string.Empty;
            decimal RemunerationAmt = 0m, DirectCostAmt = 0m;

            SQL.AppendLine("Select A.Mth, A.Yr, A.RemunerationAmt, A.DirectCostAmt, A.Remark, C.DocNo ");
            SQL.AppendLine("From TblProjectImplementationRBPDtl A ");
            SQL.AppendLine("Left Join TblProjectImplementationRBPHdr B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr C On B.PRJIDocNo=C.PRJIDocNo And A.Yr=C.Yr And A.Mth=C.Mth ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "Mth", 
                    "Yr", "RemunerationAmt", "DirectCostAmt", "Remark", "DocNo"
                });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Mth = Sm.DrStr(dr, 0);
                        Yr = Sm.DrStr(dr, 1);
                        RemunerationAmt = Sm.DrDec(dr, 2);
                        DirectCostAmt = Sm.DrDec(dr, 3);
                        Remark = Sm.DrStr(dr, 4);
                        DocNo = Sm.DrStr(dr, 5);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 0), Mth) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), Yr) &&
                                Sm.GetGrdStr(Grd1, r, 5).Length==0 &&
                                DocNo.Length>0
                                )
                            {
                                Sm.StdMsg(mMsgType.Warning, 
                                    "Year : " + Yr + Environment.NewLine +
                                    "Month : " + Mth + Environment.NewLine +
                                    "Dropping Request# : " + DocNo + Environment.NewLine +
                                    "This data already processed to dropping request." + Environment.NewLine +
                                    "All the existing information will be reset to the previous value."
                                    );
                                Grd1.Cells[r, 2].Value = RemunerationAmt;
                                Grd1.Cells[r, 3].Value = DirectCostAmt;
                                Grd1.Cells[r, 4].Value = Remark;
                                Grd1.Cells[r, 5].Value = DocNo;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnPRJIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPRJIDocNo, "Project#", false))
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = "***";
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPRJIDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}