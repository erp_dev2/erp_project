﻿namespace RunSystem
{
    partial class FrmGradeLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtGrdLvlCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtGrdLvlName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueGrdLvlGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtBasicSalary = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtBasicSalary2 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtHealthSSSalary = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEmploymentSSSalary = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtTaxSalary = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtSeqNo = new DevExpress.XtraEditors.TextEdit();
            this.LblSeqNo = new System.Windows.Forms.Label();
            this.TxtScale = new DevExpress.XtraEditors.TextEdit();
            this.LblScale = new System.Windows.Forms.Label();
            this.LblOddEven = new System.Windows.Forms.Label();
            this.LueOddEven = new DevExpress.XtraEditors.LookUpEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtOtherSSSalary = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLvlName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBasicSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBasicSalary2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHealthSSSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmploymentSSSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeqNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOddEven.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOtherSSSalary.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(772, 240);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 240);
            this.panel3.Size = new System.Drawing.Size(772, 233);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 233);
            this.Grd1.TabIndex = 35;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtGrdLvlCode
            // 
            this.TxtGrdLvlCode.EnterMoveNextControl = true;
            this.TxtGrdLvlCode.Location = new System.Drawing.Point(231, 9);
            this.TxtGrdLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrdLvlCode.Name = "TxtGrdLvlCode";
            this.TxtGrdLvlCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrdLvlCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.TxtGrdLvlCode.Properties.MaxLength = 16;
            this.TxtGrdLvlCode.Size = new System.Drawing.Size(166, 20);
            this.TxtGrdLvlCode.TabIndex = 11;
            this.TxtGrdLvlCode.Validated += new System.EventHandler(this.TxtGrdLvlCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(156, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Grade Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrdLvlName
            // 
            this.TxtGrdLvlName.EnterMoveNextControl = true;
            this.TxtGrdLvlName.Location = new System.Drawing.Point(231, 31);
            this.TxtGrdLvlName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrdLvlName.Name = "TxtGrdLvlName";
            this.TxtGrdLvlName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGrdLvlName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrdLvlName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrdLvlName.Properties.Appearance.Options.UseFont = true;
            this.TxtGrdLvlName.Properties.MaxLength = 80;
            this.TxtGrdLvlName.Size = new System.Drawing.Size(495, 20);
            this.TxtGrdLvlName.TabIndex = 14;
            this.TxtGrdLvlName.Validated += new System.EventHandler(this.TxtGrdLvlName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(153, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Grade Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(151, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Grade Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGrdLvlGrpCode
            // 
            this.LueGrdLvlGrpCode.EnterMoveNextControl = true;
            this.LueGrdLvlGrpCode.Location = new System.Drawing.Point(231, 53);
            this.LueGrdLvlGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlGrpCode.Name = "LueGrdLvlGrpCode";
            this.LueGrdLvlGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlGrpCode.Properties.DropDownRows = 30;
            this.LueGrdLvlGrpCode.Properties.NullText = "[Empty]";
            this.LueGrdLvlGrpCode.Properties.PopupWidth = 500;
            this.LueGrdLvlGrpCode.Size = new System.Drawing.Size(495, 20);
            this.LueGrdLvlGrpCode.TabIndex = 16;
            this.LueGrdLvlGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlGrpCode.EditValueChanged += new System.EventHandler(this.LueGrdLvlGrpCode_EditValueChanged);
            // 
            // TxtBasicSalary
            // 
            this.TxtBasicSalary.EnterMoveNextControl = true;
            this.TxtBasicSalary.Location = new System.Drawing.Point(231, 97);
            this.TxtBasicSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBasicSalary.Name = "TxtBasicSalary";
            this.TxtBasicSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBasicSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBasicSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBasicSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtBasicSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBasicSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBasicSalary.Properties.MaxLength = 12;
            this.TxtBasicSalary.Size = new System.Drawing.Size(166, 20);
            this.TxtBasicSalary.TabIndex = 20;
            this.TxtBasicSalary.Validated += new System.EventHandler(this.TxtBasicSalary_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(112, 99);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 14);
            this.label15.TabIndex = 19;
            this.label15.Text = "Monthly Basic Salary";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBasicSalary2
            // 
            this.TxtBasicSalary2.EnterMoveNextControl = true;
            this.TxtBasicSalary2.Location = new System.Drawing.Point(231, 119);
            this.TxtBasicSalary2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBasicSalary2.Name = "TxtBasicSalary2";
            this.TxtBasicSalary2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBasicSalary2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBasicSalary2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBasicSalary2.Properties.Appearance.Options.UseFont = true;
            this.TxtBasicSalary2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBasicSalary2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBasicSalary2.Properties.MaxLength = 12;
            this.TxtBasicSalary2.Size = new System.Drawing.Size(166, 20);
            this.TxtBasicSalary2.TabIndex = 22;
            this.TxtBasicSalary2.Validated += new System.EventHandler(this.TxtBasicSalary2_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(131, 121);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Daily Basic Salary";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHealthSSSalary
            // 
            this.TxtHealthSSSalary.EnterMoveNextControl = true;
            this.TxtHealthSSSalary.Location = new System.Drawing.Point(231, 141);
            this.TxtHealthSSSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHealthSSSalary.Name = "TxtHealthSSSalary";
            this.TxtHealthSSSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHealthSSSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHealthSSSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHealthSSSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtHealthSSSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHealthSSSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHealthSSSalary.Properties.MaxLength = 12;
            this.TxtHealthSSSalary.Size = new System.Drawing.Size(166, 20);
            this.TxtHealthSSSalary.TabIndex = 24;
            this.TxtHealthSSSalary.Validated += new System.EventHandler(this.TxtHealthSSSalary_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(42, 144);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(185, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Salaries For Health SS Calculation";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmploymentSSSalary
            // 
            this.TxtEmploymentSSSalary.EnterMoveNextControl = true;
            this.TxtEmploymentSSSalary.Location = new System.Drawing.Point(231, 163);
            this.TxtEmploymentSSSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmploymentSSSalary.Name = "TxtEmploymentSSSalary";
            this.TxtEmploymentSSSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmploymentSSSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmploymentSSSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmploymentSSSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtEmploymentSSSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmploymentSSSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmploymentSSSalary.Properties.MaxLength = 12;
            this.TxtEmploymentSSSalary.Size = new System.Drawing.Size(166, 20);
            this.TxtEmploymentSSSalary.TabIndex = 26;
            this.TxtEmploymentSSSalary.Validated += new System.EventHandler(this.TxtEmploymentSSSalary_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(9, 165);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(218, 14);
            this.label6.TabIndex = 25;
            this.label6.Text = "Salaries For Employment SS Calculation";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxSalary
            // 
            this.TxtTaxSalary.EnterMoveNextControl = true;
            this.TxtTaxSalary.Location = new System.Drawing.Point(231, 185);
            this.TxtTaxSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxSalary.Name = "TxtTaxSalary";
            this.TxtTaxSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxSalary.Properties.MaxLength = 12;
            this.TxtTaxSalary.Size = new System.Drawing.Size(166, 20);
            this.TxtTaxSalary.TabIndex = 28;
            this.TxtTaxSalary.Validated += new System.EventHandler(this.TxtTaxSalary_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(75, 187);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 14);
            this.label7.TabIndex = 27;
            this.label7.Text = "Salaries For Tax Calculation";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(192, 77);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "Level";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevelCode
            // 
            this.LueLevelCode.EnterMoveNextControl = true;
            this.LueLevelCode.Location = new System.Drawing.Point(231, 75);
            this.LueLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelCode.Name = "LueLevelCode";
            this.LueLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelCode.Properties.DropDownRows = 30;
            this.LueLevelCode.Properties.NullText = "[Empty]";
            this.LueLevelCode.Properties.PopupWidth = 500;
            this.LueLevelCode.Size = new System.Drawing.Size(495, 20);
            this.LueLevelCode.TabIndex = 18;
            this.LueLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelCode.EditValueChanged += new System.EventHandler(this.LueLevelCode_EditValueChanged);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(406, 8);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // TxtSeqNo
            // 
            this.TxtSeqNo.EnterMoveNextControl = true;
            this.TxtSeqNo.Location = new System.Drawing.Point(236, 43);
            this.TxtSeqNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeqNo.Name = "TxtSeqNo";
            this.TxtSeqNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeqNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeqNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeqNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSeqNo.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSeqNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSeqNo.Properties.MaxLength = 12;
            this.TxtSeqNo.Size = new System.Drawing.Size(166, 20);
            this.TxtSeqNo.TabIndex = 14;
            this.TxtSeqNo.Validated += new System.EventHandler(this.TxtSeqNo_Validated);
            // 
            // LblSeqNo
            // 
            this.LblSeqNo.AutoSize = true;
            this.LblSeqNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSeqNo.ForeColor = System.Drawing.Color.Red;
            this.LblSeqNo.Location = new System.Drawing.Point(170, 46);
            this.LblSeqNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSeqNo.Name = "LblSeqNo";
            this.LblSeqNo.Size = new System.Drawing.Size(62, 14);
            this.LblSeqNo.TabIndex = 13;
            this.LblSeqNo.Text = "Sequence";
            this.LblSeqNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtScale
            // 
            this.TxtScale.EnterMoveNextControl = true;
            this.TxtScale.Location = new System.Drawing.Point(236, 65);
            this.TxtScale.Margin = new System.Windows.Forms.Padding(5);
            this.TxtScale.Name = "TxtScale";
            this.TxtScale.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtScale.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtScale.Properties.Appearance.Options.UseBackColor = true;
            this.TxtScale.Properties.Appearance.Options.UseFont = true;
            this.TxtScale.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtScale.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtScale.Properties.MaxLength = 12;
            this.TxtScale.Size = new System.Drawing.Size(166, 20);
            this.TxtScale.TabIndex = 16;
            this.TxtScale.Validated += new System.EventHandler(this.TxtScale_Validated);
            // 
            // LblScale
            // 
            this.LblScale.AutoSize = true;
            this.LblScale.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblScale.ForeColor = System.Drawing.Color.Red;
            this.LblScale.Location = new System.Drawing.Point(197, 68);
            this.LblScale.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblScale.Name = "LblScale";
            this.LblScale.Size = new System.Drawing.Size(35, 14);
            this.LblScale.TabIndex = 15;
            this.LblScale.Text = "Scale";
            this.LblScale.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblOddEven
            // 
            this.LblOddEven.AutoSize = true;
            this.LblOddEven.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOddEven.ForeColor = System.Drawing.Color.Black;
            this.LblOddEven.Location = new System.Drawing.Point(162, 90);
            this.LblOddEven.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblOddEven.Name = "LblOddEven";
            this.LblOddEven.Size = new System.Drawing.Size(70, 14);
            this.LblOddEven.TabIndex = 17;
            this.LblOddEven.Text = "Odd / Even";
            this.LblOddEven.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblOddEven.Click += new System.EventHandler(this.label9_Click);
            // 
            // LueOddEven
            // 
            this.LueOddEven.EnterMoveNextControl = true;
            this.LueOddEven.Location = new System.Drawing.Point(236, 87);
            this.LueOddEven.Margin = new System.Windows.Forms.Padding(5);
            this.LueOddEven.Name = "LueOddEven";
            this.LueOddEven.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOddEven.Properties.Appearance.Options.UseFont = true;
            this.LueOddEven.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOddEven.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOddEven.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOddEven.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOddEven.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOddEven.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOddEven.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOddEven.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOddEven.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOddEven.Properties.DropDownRows = 30;
            this.LueOddEven.Properties.NullText = "[Empty]";
            this.LueOddEven.Properties.PopupWidth = 350;
            this.LueOddEven.Size = new System.Drawing.Size(166, 20);
            this.LueOddEven.TabIndex = 18;
            this.LueOddEven.ToolTip = "F4 : Show/hide list";
            this.LueOddEven.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(772, 240);
            this.Tc1.TabIndex = 10;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.ChkActInd);
            this.Tp1.Controls.Add(this.label1);
            this.Tp1.Controls.Add(this.TxtGrdLvlCode);
            this.Tp1.Controls.Add(this.label2);
            this.Tp1.Controls.Add(this.TxtGrdLvlName);
            this.Tp1.Controls.Add(this.LueGrdLvlGrpCode);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.label15);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.TxtBasicSalary);
            this.Tp1.Controls.Add(this.LueLevelCode);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.TxtTaxSalary);
            this.Tp1.Controls.Add(this.TxtBasicSalary2);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.TxtEmploymentSSSalary);
            this.Tp1.Controls.Add(this.TxtHealthSSSalary);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 212);
            this.Tp1.Text = "General";
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.label9);
            this.Tp2.Controls.Add(this.TxtOtherSSSalary);
            this.Tp2.Controls.Add(this.TxtSeqNo);
            this.Tp2.Controls.Add(this.LblOddEven);
            this.Tp2.Controls.Add(this.LblSeqNo);
            this.Tp2.Controls.Add(this.LueOddEven);
            this.Tp2.Controls.Add(this.LblScale);
            this.Tp2.Controls.Add(this.TxtScale);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 212);
            this.Tp2.Text = "Additional";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(30, 24);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(202, 14);
            this.label9.TabIndex = 11;
            this.label9.Text = "Salaries For Non BPJS SS Calculation";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOtherSSSalary
            // 
            this.TxtOtherSSSalary.EnterMoveNextControl = true;
            this.TxtOtherSSSalary.Location = new System.Drawing.Point(236, 21);
            this.TxtOtherSSSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOtherSSSalary.Name = "TxtOtherSSSalary";
            this.TxtOtherSSSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOtherSSSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOtherSSSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOtherSSSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtOtherSSSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOtherSSSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOtherSSSalary.Properties.MaxLength = 12;
            this.TxtOtherSSSalary.Size = new System.Drawing.Size(166, 20);
            this.TxtOtherSSSalary.TabIndex = 12;
            this.TxtOtherSSSalary.Validated += new System.EventHandler(this.TxtOtherSSSalary_Validated);
            // 
            // FrmGradeLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmGradeLevel";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrdLvlName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBasicSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBasicSalary2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHealthSSSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmploymentSSSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeqNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtScale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOddEven.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOtherSSSalary.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtGrdLvlName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtGrdLvlCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueGrdLvlGrpCode;
        internal DevExpress.XtraEditors.TextEdit TxtBasicSalary;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtBasicSalary2;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxSalary;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtEmploymentSSSalary;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtHealthSSSalary;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueLevelCode;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        internal DevExpress.XtraEditors.TextEdit TxtSeqNo;
        private System.Windows.Forms.Label LblSeqNo;
        internal DevExpress.XtraEditors.TextEdit TxtScale;
        private System.Windows.Forms.Label LblScale;
        private System.Windows.Forms.Label LblOddEven;
        internal DevExpress.XtraEditors.LookUpEdit LueOddEven;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtOtherSSSalary;
    }
}