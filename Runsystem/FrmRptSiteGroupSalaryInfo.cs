﻿#region Update
/*
    09/03/2022 [TKG/PHT] new reporting
    18/03/2022 [TKG/PHT] ubah label dari division menjadi group, group menjadi site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSiteGroupSalaryInfo : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSiteGroupSalaryInfo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Site Group Salary Information */ ");

            SQL.AppendLine("Select IfNull(C.OptDesc, '-') As SiteDivisionDesc, B.OptDesc As SiteGroupDesc, "); 
            SQL.AppendLine("IfNull(D.BruttoCount, 0.00) As BruttoCount, ");
            SQL.AppendLine("IfNull(D.BruttoAmt, 0.00) As BruttoAmt, ");
            SQL.AppendLine("IfNull(E.TaxAllowanceCount, 0.00) As TaxAllowanceCount, ");
            SQL.AppendLine("IfNull(E.TaxAllowanceAmt, 0.00) As TaxAllowanceAmt, ");
            SQL.AppendLine("IfNull(F.SSEmployerBNILifeCount, 0.00) As SSEmployerBNILifeCount, ");
            SQL.AppendLine("IfNull(F.SSEmployerBNILifeAmt, 0.00) As SSEmployerBNILifeAmt, ");
            SQL.AppendLine("IfNull(G.SSEmployeeTSPCount, 0.00) As SSEmployeeTSPCount, ");
            SQL.AppendLine("IfNull(G.SSEmployeeTSPAmt, 0.00) As SSEmployeeTSPAmt, ");
            SQL.AppendLine("IfNull(H.KESRMHCount, 0.00) As KESRMHCount, ");
            SQL.AppendLine("IfNull(H.KESRMHAmt, 0.00) As KESRMHAmt, ");
            SQL.AppendLine("A.EmpCount, A.NettoAmt, ");
            SQL.AppendLine("IfNull(I.TSMKDinamisAmt, 0.00)+IfNull(J.TSMKStatisAmt, 0.00) As TSMK ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Count(1) As EmpCount, Sum(T1.Netto) As NettoAmt ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblOption B On A.SiteGroup = B.OptCode And B.OptCat = 'SiteGroup' ");
            SQL.AppendLine("Left Join TblOption C On B.Property1 = C.OptCode And C.OptCat = 'SiteDivision' ");
            SQL.AppendLine("Left Join( ");
            SQL.AppendLine("    Select T3.SiteGroup, Count(1) As BruttoCount, Sum(T1.Brutto) As BruttoAmt ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And T1.Brutto > 0.00 ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") D On A.SiteGroup = D.SiteGroup ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Count(1) As TaxAllowanceCount, Sum(T1.TaxAllowance) As TaxAllowanceAmt ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And T1.TaxAllowance > 0.00 ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") E On A.SiteGroup = E.SiteGroup ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Count(1) As SSEmployerBNILifeCount, Sum(T1.SSEmployerBNILife) As SSEmployerBNILifeAmt ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And T1.SSEmployerBNILife > 0.00 ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") F On A.SiteGroup = F.SiteGroup ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Count(1) As SSEmployeeTSPCount, Sum(T1.SSEmployeeTSP) As SSEmployeeTSPAmt ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And T1.SSEmployeeTSP > 0.00 ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") G On A.SiteGroup = G.SiteGroup ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Count(1) As KESRMHCount, Sum((T1.SSEmployerPKES - T1.SSEmployeePKES) + (T1.SSEmployerPRMH - T1.SSEmployeePRMH)) As KESRMHAmt ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And (T1.SSEmployerPKES - T1.SSEmployeePKES) + (T1.SSEmployerPRMH - T1.SSEmployeePRMH) > 0.00 ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") H On A.SiteGroup = H.SiteGroup ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Sum(T1.Amt) As TSMKDinamisAmt ");
            SQL.AppendLine("    From TblPayrollProcessAD T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And T1.ADCode = 'AL004' ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") I On A.SiteGroup = I.SiteGroup ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteGroup, Sum(T1.Amt) As TSMKStatisAmt ");
            SQL.AppendLine("    From TblPayrollProcessAD T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode = T2.PayrunCode And T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblSite T3 On T2.SiteCode = T3.SiteCode And T3.SiteGroup Is Not Null ");
            SQL.AppendLine("    Where Left(T1.PayrunCode, 6) = @YrMth ");
            SQL.AppendLine("    And T1.ADCode = 'AL025' ");
            SQL.AppendLine("    Group By T3.SiteGroup ");
            SQL.AppendLine(") J On A.SiteGroup = J.SiteGroup;  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 3;
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;

            SetGrdHdr(0, "No", 3, 50);
            SetGrdHdr(1, "Group", 3, 200);
            SetGrdHdr(2, "Site", 3, 200);
            SetGrdHdr3();
            SetGrdHdr(3, "Person-Brutto", 1, 90);
            SetGrdHdr(4, "Brutto", 1, 180);
            SetGrdHdr(5, "Person-Tax Allowance", 1, 140);
            SetGrdHdr(6, "Tax Allowance", 1, 180);
            SetGrdHdr(7, "Person-Dapen", 1, 90);
            SetGrdHdr(8, "Dapen", 1, 180);
            SetGrdHdr(9, "Person-Taspen", 1, 100);
            SetGrdHdr(10, "Taspen", 1, 180);
            SetGrdHdr(11, "Person-YKP3JS", 1, 110);
            SetGrdHdr(12, "YKP3JS", 1, 180);
            SetGrdHdr(13, "Total Payment", 1, 180);
            SetGrdHdr(14, "Person", 1, 180);
            SetGrdHdr(15, "Brutto without" + Environment.NewLine + "Dapen+Taspen+YKP3JS", 1, 180);
            SetGrdHdr(16, "Netto", 1, 180);
            SetGrdHdr(17, "Performance-SMK", 1, 180);
            SetGrdHdr2(3, "Total Salary", 4);
            SetGrdHdr2(7, "Payment", 7);
            SetGrdHdr2(14, "Total", 4);

            Sm.GrdFormatDec(Grd1,
                new int[] { 
                    3, 4, 5, 6, 7, 8, 9, 10,
                    11, 12, 13, 14, 15, 16, 17
                }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetGrdHdr(int col, string Title, int SpanRows, int ColWidth)
        {
            Grd1.Header.Cells[0, col].Value = Title;
            Grd1.Header.Cells[0, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, col].SpanRows = SpanRows;
            Grd1.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(int col, string Title, int SpanCols)
        {
            Grd1.Header.Cells[1, col].Value = Title;
            Grd1.Header.Cells[1, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[1, col].SpanCols = SpanCols;
            Grd1.Cols[col].Width = 160;
        }

        private void SetGrdHdr3()
        {
            Grd1.Header.Cells[2, 3].Value = "Employee's Company";
            Grd1.Header.Cells[2, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[2, 3].SpanCols = 15;
            Grd1.Cols[3].Width = 2800;
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, mSQL,
                new string[]
                    {
                        //0
                        "SiteDivisionDesc",
                        
                        //1-5
                        "SiteGroupDesc",
                        "BruttoCount",
                        "BruttoAmt",
                        "TaxAllowanceCount",
                        "TaxAllowanceAmt",

                        //6-10
                        "SSEmployerBNILifeCount",
                        "SSEmployerBNILifeAmt",
                        "SSEmployeeTSPCount",
                        "SSEmployeeTSPAmt",
                        "KESRMHCount",

                        //11-14
                        "KESRMHAmt",
                        "EmpCount", 
                        "NettoAmt",
                        "TSMK"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row+1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Grd1.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 8) + Sm.GetGrdDec(Grd, Row, 10) + Sm.GetGrdDec(Grd, Row, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Grd1.Cells[Row, 15].Value = Sm.GetGrdDec(Grd, Row, 4) - (Sm.GetGrdDec(Grd, Row, 8) + Sm.GetGrdDec(Grd, Row, 10) + Sm.GetGrdDec(Grd, Row, 12));
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                }, true, true, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
        
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion
    }
}
