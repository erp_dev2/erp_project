﻿#region Update
/*
    11/01/2018 [WED] Item bisa dipilih berkali kali
    01/12/2021 [TYO] set kolom inspected qty readonly ketika item belum dipilih
    06/12/2021 [YOG/IOK] Pada menu QC Planning (01110103), bisa planning lebih dari 1 baris untuk 1 item/batch dalam satu dokumen. Karena parameter yang digunakan berbeda-beda untuk setiap 1 item dengan batch# yang sama tersebut. Menggunakan parameter IsQCParameterInDetail
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

#region Old Code
/*
 override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("Case T1.DocType When '1' Then 'Material' When '2' Then 'Result' End As DocType, ");
            SQL.AppendLine("T1.DocNo, T1.DocName, T1.ItCode, T2.ItCodeInternal, T2.ItName, T1.Qty, T2.PlanningUomCode As Uom, T2.PlanningUomCode2 As Uom2 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select '1' As Doctype, A.DocNo, A.DocName, B.ItCode, B.Qty ");
            SQL.AppendLine("    From TblBOMHdr A ");
            SQL.AppendLine("    Inner Join TblBOMDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.ActiveInd = 'Y' ");
            SQL.AppendLine("    And A.DocNo In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct X2.BOMDocNo ");
            SQL.AppendLine("        From TblMaterialPlanningHdr X1 ");
            SQL.AppendLine("        Inner Join TblMaterialPlanningDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        Where X1.CancelInd = 'N' ");
            SQL.AppendLine("        And X2.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As Doctype, A.DocNo, A.DocName, B.DocCode As ItCode, B.Qty ");
            SQL.AppendLine("    From TblBOMHdr A ");
            SQL.AppendLine("    Inner Join TblBOMDtl B On A.DocNo = B.DocNo And B.DocType = '1' ");
            SQL.AppendLine("    Where A.ActiveInd = 'Y' ");
            SQL.AppendLine("    And A.DocNo In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct X2.BOMDocNo ");
            SQL.AppendLine("        From TblMaterialPlanningHdr X1 ");
            SQL.AppendLine("        Inner Join TblMaterialPlanningDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        Where X1.CancelInd = 'N' ");
            SQL.AppendLine("        And X2.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode And T2.ActInd = 'Y' ");
            SQL.AppendLine("Where Locate(Concat('##', T1.ItCode, '-', '-', '-', '##'), @SelectedItem)<1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Type",
                    "BOM#",
                    "BOM Name",
                    "Item's Code", 
                    
                    //6-10
                    "",
                    "Item's" +Environment.NewLine + "Local Code",
                    "Item's Name", 
                    "Quantity",
                    "Uom",

                    //11-14
                    "Quantity 2",
                    "Uom 2",
                    "Quantity 3",
                    "Uom 3",
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 120, 180, 250, 100, 
                    
                    //6-10
                    20, 100, 250, 80, 80,

                    //11-14
                    80, 80, 80, 80
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7 }, false);

            if (mFrmParent.mNumberOfInventoryUomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13, 14 }, false);
            }

            if (mFrmParent.mNumberOfInventoryUomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, false);
            }

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", mWorkCenterDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T1.ItCode", "T2.ItCodeInternal", "T2.ItName" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T2.ItName;",
                    new string[] 
                    { 
                        //0
                        "DocType",

                        //1-5
                        "DocNo", "DocName", "ItCode", "ItCodeInternal", "ItName",  
                        
                        //6-8
                        "Qty", "Uom", "Uom2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 8);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 8);
                        mFrmParent.Grd1.Cells[Row1, 13].Value = "-";
                        mFrmParent.Grd1.Cells[Row1, 14].Value = "-";
                        mFrmParent.Grd1.Cells[Row1, 15].Value = "-";
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 14);
                        mFrmParent.Grd1.Cells[Row1, 0].Value = mFrmParent.mQCDefaultStatus;
                        mFrmParent.Grd1.Cells[Row1, 1].Value = mFrmParent.mQCStatusDesc;

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3, 4 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 16, 18, 20 });
                    }
                }

                mFrmParent.ComputeSubTotal();

                //iGSubtotalManager.ShowSubtotalsInCells = false;
                //mFrmParent.AddSubtotal();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 9), Sm.GetGrdStr(Grd1, Row, 5))
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
 */
        #endregion

namespace RunSystem
{
    public partial class FrmQCPlanningDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmQCPlanning mFrmParent;
        private string mSQL = string.Empty, mWorkCenterDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmQCPlanningDlg2(FrmQCPlanning FrmParent, string WorkCenterDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWorkCenterDocNo = WorkCenterDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.ItCode, T2.ItCodeInternal, T2.ItName, Sum(T1.Qty) As Qty, T2.PlanningUomCode As Uom, T2.PlanningUomCode2 As Uom2 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.ItCode, B.Qty ");
            SQL.AppendLine("    From TblBOMHdr A ");
            SQL.AppendLine("    Inner Join TblBOMDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.ActiveInd = 'Y' ");
            SQL.AppendLine("    And A.DocNo In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct X2.BOMDocNo ");
            SQL.AppendLine("        From TblMaterialPlanningHdr X1 ");
            SQL.AppendLine("        Inner Join TblMaterialPlanningDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        Where X1.CancelInd = 'N' ");
            SQL.AppendLine("        And X2.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.DocCode As ItCode, B.Qty ");
            SQL.AppendLine("    From TblBOMHdr A ");
            SQL.AppendLine("    Inner Join TblBOMDtl B On A.DocNo = B.DocNo And B.DocType = '1' ");
            SQL.AppendLine("    Where A.ActiveInd = 'Y' ");
            SQL.AppendLine("    And A.DocNo In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct X2.BOMDocNo ");
            SQL.AppendLine("        From TblMaterialPlanningHdr X1 ");
            SQL.AppendLine("        Inner Join TblMaterialPlanningDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        Where X1.CancelInd = 'N' ");
            SQL.AppendLine("        And X2.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.WorkCenterDocNo = @WorkCenterDocNo ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode And T2.ActInd = 'Y' ");
            //SQL.AppendLine("Where Locate(Concat('##', T1.ItCode, '-', '-', '-', '##'), @SelectedItem)<1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Item's Code",
                    "",
                    "Item's" +Environment.NewLine + "Local Code",
                    "Item's Name", 
                    
                    //6-10
                    "Quantity",
                    "Uom",
                    "Quantity 2",
                    "Uom 2",
                    "Quantity 3",
                    
                    //11
                    "Uom 3",
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 20, 100, 250, 
                    
                    //6-10
                    80, 80, 80, 80, 80,

                    //11
                    80
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 8, 10 }, false);

            if (mFrmParent.mNumberOfInventoryUomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11 }, false);
            }

            if (mFrmParent.mNumberOfInventoryUomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11 }, false);
            }

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", mWorkCenterDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T1.ItCode", "T2.ItCodeInternal", "T2.ItName" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Group By T1.ItCode, T2.ItCodeInternal, T2.ItName, T2.PlanningUomCode, T2.PlanningUomCode2 Order By T2.ItName;",
                    new string[] 
                    { 
                        //0
                        "ItCode", 

                        //1-5
                        "ItCodeInternal", "ItName", "Qty", "Uom", "Uom2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 5);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 /*&& !IsItCodeAlreadyChosen(Row)*/)
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 5);
                        mFrmParent.Grd1.Cells[Row1, 13].Value = "-";
                        mFrmParent.Grd1.Cells[Row1, 17].Value = "-";
                        mFrmParent.Grd1.Cells[Row1, 18].Value = "-";
                        mFrmParent.Grd1.Cells[Row1, 19].Value = 0m;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 7);
                        mFrmParent.Grd1.Cells[Row1, 21].Value = 0m;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 9);
                        mFrmParent.Grd1.Cells[Row1, 23].Value = 0m;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 11);
                        mFrmParent.Grd1.Cells[Row1, 0].Value = mFrmParent.mQCDefaultStatus;
                        mFrmParent.Grd1.Cells[Row1, 1].Value = mFrmParent.mQCStatusDesc;

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3, 4 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 16, 19, 21, 23, 25 });

                        if(Sm.GetLue(mFrmParent.LueWhsCode).Length <= 0 && Sm.GetLue(mFrmParent.LueWorkCenter).Length > 0)
                        {
                            Sm.GrdColReadOnly(false, true, mFrmParent.Grd1, new int[] { 19, 21, 23, 25 });
                        }

                    }
                }

                mFrmParent.ComputeSubTotal();

                //iGSubtotalManager.ShowSubtotalsInCells = false;
                //mFrmParent.AddSubtotal();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        //private bool IsItCodeAlreadyChosen(int Row)
        //{
        //    for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
        //    {
        //        if (
        //            Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 9), Sm.GetGrdStr(Grd1, Row, 2))
        //            )
        //            return true;
        //    }
        //    return false;
        //}

        #endregion

        #region Grid Method

        private void Grd1_AfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
