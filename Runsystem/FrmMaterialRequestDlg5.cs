﻿#region Update
/*
    08/11/2019 [TKG/SIER] layar dialog untuk Job 
    12/11/2019 [TKG/SIER] Kalau job currency kosong maka akan diisi dengan estimated currency dari item tsb.
    28/04/2020 [IBL/SIER] Tambah kolom quantity
 *  01/12/2020 [ICA/SIER] UnHide kolom Category
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequestDlg5 : RunSystem.FrmBase15
    {
        #region Field

        private string mItCode = string.Empty, mMainCurCode = string.Empty;
        private int mRow = -1;
        private FrmMaterialRequest mFrmParent;
        private bool mIsInsert;

        #endregion

        #region Constructor

        public FrmMaterialRequestDlg5(FrmMaterialRequest FrmParent, int r, bool IsInsert)
        {
            InitializeComponent();
            mRow = r;
            mFrmParent = FrmParent;
            mIsInsert = IsInsert;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                ShowData();
                if (!mIsInsert)
                {
                    Grd1.ReadOnly = true;
                    BtnSave.Enabled = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Job's Code",
                        "Job's Name",
                        "Category",
                        "UoM",
                        "Previous"+Environment.NewLine+"Currency",
                        
                        //6-10
                        "Previous"+Environment.NewLine+"Price",
                        "Currency",
                        "Price",
                        "Qty",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 200, 200, 80, 80,
                        
                        //6-10
                        120, 100, 120, 70, 300
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 9}, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowData()
        {
            var CurCode = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 28);
            if (CurCode.Length == 0) CurCode = mMainCurCode;
            TxtItCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 8);
            TxtItName.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 11);
            TxtCurCode.EditValue = CurCode;
            TxtEstPrice.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 29), 0);
            if (mFrmParent.mlJob.Count > 0)
            {
                int r = 0;
                Grd1.BeginUpdate();
                foreach (var x in mFrmParent.mlJob.Where(w =>
                    Sm.CompareStr(w.ItCode, TxtItCode.Text)))
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[r, 1].Value = x.JobCode;
                    Grd1.Cells[r, 2].Value = x.JobName;
                    Grd1.Cells[r, 3].Value = x.JobCtName;
                    Grd1.Cells[r, 4].Value = x.UomCode;
                    Grd1.Cells[r, 5].Value = x.PrevCurCode;
                    Grd1.Cells[r, 6].Value = Sm.FormatNum(x.PrevUPrice, 0);
                    Grd1.Cells[r, 7].Value = x.CurCode;
                    Grd1.Cells[r, 8].Value = Sm.FormatNum(x.UPrice, 0);
                    Grd1.Cells[r, 9].Value = Sm.FormatNum(x.Qty, 0);
                    Grd1.Cells[r, 10].Value = x.Remark;
                    r++;
                }
            }
            Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
            Grd1.EndUpdate();
            ComputeEstPrice();
        }

        #endregion

        #region Button Methods

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (IsGrdValueNotValid() || IsCurCodeInvalid() ) return;
            try
            {
                if (mFrmParent.mlJob.Count > 0)
                {
                    mFrmParent.mlJob.RemoveAll(x =>Sm.CompareStr(x.ItCode, TxtItCode.Text));
                }
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length>0)
                    {
                        mFrmParent.mlJob.Add(new FrmMaterialRequest.Job()
                        {
                            ItCode = TxtItCode.Text,
                            JobCode = Sm.GetGrdStr(Grd1, r, 1),
                            JobName = Sm.GetGrdStr(Grd1, r, 2),
                            JobCtName = Sm.GetGrdStr(Grd1, r, 3),
                            UomCode = Sm.GetGrdStr(Grd1, r, 4),
                            PrevCurCode = Sm.GetGrdStr(Grd1, r, 5),
                            PrevUPrice = Sm.GetGrdDec(Grd1, r, 6),
                            CurCode = Sm.GetGrdStr(Grd1, r, 7).Length==0?TxtCurCode.Text:Sm.GetGrdStr(Grd1, r, 7),
                            UPrice = Sm.GetGrdDec(Grd1, r, 8),
                            Qty = Sm.GetGrdDec(Grd1, r, 9),
                            Remark = Sm.GetGrdStr(Grd1, r, 10)
                        });
                    }
                }
                mFrmParent.Grd1.Cells[mRow, 29].Value = decimal.Parse(TxtEstPrice.Text);
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeEstPrice()
        {
            var EstPrice = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                    EstPrice += Sm.GetGrdDec(Grd1, r, 8) * Sm.GetGrdDec(Grd1, r, 9);
            }

            if (Sm.GetGrdStr(Grd1, 0, 7).Length>0) TxtCurCode.EditValue = Sm.GetGrdStr(Grd1, 0, 7);
            TxtEstPrice.EditValue = Sm.FormatNum(EstPrice, 0);
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Job is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 8, true, "Price should be bigger than 0.00.")) return true;
                }
            }
            return false;
        }

        private bool IsCurCodeInvalid()
        {
            var CurCode = Sm.GetGrdStr(Grd1, 0, 7);
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                    {
                        if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, r, 7)))
                        { 
                            Sm.StdMsg(mMsgType.Warning,
                                "Job's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                                "Job's Name : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine + Environment.NewLine +
                                "Currency is not valid.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmMaterialRequestDlg6(this));
            }
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeEstPrice();
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 8 }, e);
            if (e.ColIndex==8) ComputeEstPrice();
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 9 }, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaterialRequestDlg6(this));
        }


        #endregion

        #endregion

        #region Event

        #region Grid Event

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            ComputeEstPrice();
        }

        #endregion

        #endregion
    }
}
