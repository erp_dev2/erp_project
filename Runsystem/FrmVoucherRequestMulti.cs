﻿#region Update
/*
    10/11/2020 [TKG/PHT] New application
    26/01/2021 [HAR/PHT] generate docno
    29/04/2021 [MYA/PHT] menambahkan cash type dan parameter IsVoucherUseCashType
    07/05/2021 [BRI/PHT] tambah remark dan format remark di csv
    11/06/2021 [DITA/PHT] Mee remark -> remark header disimpan menjadi remark header di VR agar tertarik di GL remark header
    12/07/2021 [IQA/PHT] Memindah cash type di multi VC, yang awalnya di multi VR
    21/04/2022 [TKG/PHT] merubah GetParameter() dan mempercepat proses save
    17/05/2022 [TKG/PHT] tambah document approval berdasarkan department
    09/06/2022 [BRI/PHT] rubah source remark
    21/02/2023 [WED/PHT] tambah validasi closing journal berdasarkan parameter IsClosingJournalBasedOnMultiProfitCenter
    22/02/2023 [WED/PHT] validasi Profit Center melihat dari Bank Account nya
    15/03/2023 [DITA/PHT] validasi ketika amount yg diinputkan - (minus)
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestMulti : RunSystem.FrmBase15
    {
        #region Field

        private string mMenuCode = string.Empty;
        private bool
            mIsDocApprovalBasedOnDeptEnabled = false,
            mIsFilterByDept = false,
            //mIsVoucherUseCashType = false,
            mIsRemarkForApprovalMandatory = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsAutoJournalActived = false
            ;
            
        #endregion

        #region Constructor

        public FrmVoucherRequestMulti(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                Sl.SetLueUserCode(ref LuePIC);
                if (mIsRemarkForApprovalMandatory) LblRemark.ForeColor = Color.Red;
                ClearGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsDocApprovalBasedOnDeptEnabled = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='VoucherRequestSwitchingBankAcc' And DeptCode Is Not Null Limit 1;");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterByDept', 'IsVoucherRequestRemarkForApprovalMandatory', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsAutoJournalActived' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsVoucherRequestRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Bank Account (C)", 
                    "Bank Account (C)", 
                    "Bank Account (D)", 
                    "Bank Account (D)",
                    "Amount",

                    //6-10
                    "Remark",
                    "Voucher Request#",
                    "Bank Code",
                    "Entity Code",
                    "Site Code",

                    //11-13
                    "Currency Code",
                    "ProfitCenterCode (C)",
                    "ProfitCenterCode (D)"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 300, 150, 300, 130,
                        
                    //6-10
                    200, 0, 0, 0, 0,

                    //11-13
                    0, 0, 0
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 12, 13 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsFirst = true;
                var SQL1 = new StringBuilder();
                var SQL2 = new StringBuilder();
                var SQL3 = new StringBuilder();
                var SQL4 = new StringBuilder();
                var cm = new MySqlCommand();
                var cml = new List<MySqlCommand>();
                var SQLForNewVoucherRequestDocNo = Sm.GetNewVoucherRequestDocNo(Sm.Left(Sm.GetDte(DteDocDt), 8), "VoucherRequest", 1);
               
                SQL1.AppendLine("Set @Dt:=CurrentDateTime(); ");
                SQL1.AppendLine("Set @MainCurCode:=(Select ParValue From TblParameter Where ParCode='MainCurCode' And ParValue Is Not Null); ");

                SQL2.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL2.AppendLine("Values");

                if (mIsDocApprovalBasedOnDeptEnabled)
                    SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        SQL1.Append("Set @DocNo_" + r.ToString() + ":=" + SQLForNewVoucherRequestDocNo + ";");

                        if (IsFirst)
                            IsFirst = false;
                        else
                        {
                            SQL2.AppendLine(", ");
                            if (mIsDocApprovalBasedOnDeptEnabled) SQL3.AppendLine(" Union All ");
                        }
                           
                        SQL1.AppendLine("Insert Into TblVoucherRequestHdr ");
                        SQL1.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, ");
                        SQL1.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, CashTypeCode, PaymentType, ");
                        SQL1.AppendLine("BankCode, PIC, CurCode, Amt, CurCode2, ExcRate, ");
                        SQL1.AppendLine("DocEnclosure, EntCode, Remark, SiteCode, MultiInd, CreateBy, CreateDt) ");
                        SQL1.AppendLine("Values");
                        SQL1.AppendLine("(@DocNo_"+r.ToString() + ", @DocDt, 'N', 'O', 'N', @DeptCode, '16', ");
                        SQL1.AppendLine("'C', @BankAcCode_" + r.ToString() + ", 'D', @BankAcCode2_" + r.ToString() + ", NULL, @PaymentType, ");
                        SQL1.AppendLine("@BankCode_" + r.ToString() + ", @PIC, @CurCode_" + r.ToString() + ", @Amt_" + r.ToString() + ", @CurCode_" + r.ToString() + ", 1.00, ");
                        SQL1.AppendLine("0.00, @EntCode_" + r.ToString() + ", @Remark_" + r.ToString() + ", @SiteCode_" + r.ToString() + ", 'Y', @UserCode, @Dt); ");

                        SQL2.AppendLine("(@DocNo_" + r.ToString() + ", '001', 'Multi Voucher Request', @Amt_" + r.ToString() + ", @Remark_" + r.ToString() + ", @UserCode, @Dt) ");

                        if (mIsDocApprovalBasedOnDeptEnabled)
                        {
                            SQL3.AppendLine("Select T.DocType, @DocNo_" + r.ToString() + ", '001', T.DNo, @UserCode, @Dt ");
                            SQL3.AppendLine("From TblDocApprovalSetting T ");
                            SQL3.AppendLine("Where T.DocType='VoucherRequestSwitchingBankAcc' ");
                            SQL3.AppendLine("And T.DeptCode=@DeptCode ");
                            SQL3.AppendLine("And (T.StartAmt=0 ");
                            SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                            SQL3.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                            SQL3.AppendLine("    From TblVoucherRequestHdr A ");
                            SQL3.AppendLine("    Left Join ( ");
                            SQL3.AppendLine("        Select B1.CurCode1, B1.Amt ");
                            SQL3.AppendLine("        From TblCurrencyRate B1 ");
                            SQL3.AppendLine("        Inner Join ( ");
                            SQL3.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                            SQL3.AppendLine("            From TblCurrencyRate ");
                            SQL3.AppendLine("            Where CurCode2=@MainCurCode ");
                            SQL3.AppendLine("            Group By CurCode1 ");
                            SQL3.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                            SQL3.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                            SQL3.AppendLine("    Where A.DocNo=@DocNo_" + r.ToString());
                            SQL3.AppendLine("), 0)) ");

                            SQL4.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
                            SQL4.AppendLine("Where DocNo=@DocNo_" + r.ToString());
                            SQL4.AppendLine(" And DocNo Not In (Select DocNo From TblDocApproval Where DocType='VoucherRequestSwitchingBankAcc' And DocNo=@DocNo_" + r.ToString() + "); ");
                        }
                            
                        //Sm.CmParam<String>(ref cm, "@DocNo_"+r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                        Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                        Sm.CmParam<String>(ref cm, "@BankAcCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                        Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                        Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                        Sm.CmParam<String>(ref cm, "@BankCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                        Sm.CmParam<String>(ref cm, "@EntCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                        Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                        Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                    }
                }
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                //Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetLue(LueCashTypeCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
               
                cm.CommandText = 
                    SQL1.ToString() + 
                    SQL2.ToString() + ";" +
                    (mIsDocApprovalBasedOnDeptEnabled? SQL3.ToString() + ";" : string.Empty) +
                    SQL4.ToString();
                cml.Add(cm);
                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, "Process is completed.");
                ClearGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void Import1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string BankAcCodeTemp = string.Empty, BankAcCode2Temp = string.Empty;
            var AmtTemp = 0m;
            string RemarkTemp = string.Empty;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            BankAcCodeTemp = arr[0].Trim();
                            BankAcCode2Temp = arr[1].Trim();
                            if (arr[2].Trim().Length > 0)
                                AmtTemp = decimal.Parse(arr[2].Trim());
                            else
                                AmtTemp = 0m;
                            if (AmtTemp < 0m)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Amount should not be less than 0.00 ");
                                return;
                            }
                            RemarkTemp = arr[3].Trim();
                            l.Add(new Result()
                            {
                                BankAcCode = BankAcCodeTemp,
                                BankAcCode2 = BankAcCode2Temp,
                                BankAcNm = string.Empty,
                                BankAcNm2 = string.Empty,
                                Amt = AmtTemp,
                                Remark = RemarkTemp,
                                BankCode = string.Empty,
                                EntCode = string.Empty,
                                SiteCode = string.Empty,
                                CurCode = string.Empty
                            });
                        }
                    }
                }
            }
        }

        private void Import2(ref List<BankAccount> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BankAcCode, BankAcName, BankCode, EntCode, SiteCode, CurCode From ( ");
            SQL.AppendLine("    Select BankAcCode, ");
            SQL.AppendLine("    Trim(Concat( ");
            SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
            SQL.AppendLine("    Case When A.BankAcNo Is Not Null Then Concat(A.BankAcNo) Else IfNull(A.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
            SQL.AppendLine(")) As BankAcName, A.BankCode, A.EntCode, A.SiteCode, A.CurCode ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine(") T Order By BankAcName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ 
                    "BankAcCode", 
                    "BankAcName", 
                    "BankCode", 
                    "EntCode", 
                    "SiteCode", 
                    "CurCode" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BankAccount()
                        {
                            BankAcCode = Sm.DrStr(dr, c[0]),
                            BankAcName = Sm.DrStr(dr, c[1]),
                            BankCode = Sm.DrStr(dr, c[2]),
                            EntCode = Sm.DrStr(dr, c[3]),
                            SiteCode = Sm.DrStr(dr, c[4]),
                            CurCode = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import3(ref List<Result> l, ref List<BankAccount> l2)
        {
            byte v = 0;
            foreach (var i in l)
            {
                v = 0;
                foreach (var j in l2)
                {
                    if (Sm.CompareStr(i.BankAcCode, j.BankAcCode))
                    {
                        i.BankAcNm = j.BankAcName;
                        i.BankCode = j.BankCode;
                        i.EntCode = j.EntCode;
                        i.SiteCode = j.SiteCode;
                        i.CurCode = j.CurCode;
                        v++;
                    }

                    if (Sm.CompareStr(i.BankAcCode2, j.BankAcCode))
                    {
                        i.BankAcNm2 = j.BankAcName;
                        v++;
                    }
                    if (v == 2) break;
                }
            }
        }

        private void Import4(ref List<Result> l)
        {
            int No = 1;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = No;
                r.Cells[1].Value = l[i].BankAcCode;
                r.Cells[2].Value = l[i].BankAcNm;
                r.Cells[3].Value = l[i].BankAcCode2;
                r.Cells[4].Value = l[i].BankAcNm2;
                r.Cells[5].Value = l[i].Amt;
                r.Cells[6].Value = l[i].Remark;
                r.Cells[7].Value = null;
                r.Cells[8].Value = l[i].BankCode;
                r.Cells[9].Value = l[i].EntCode;
                r.Cells[10].Value = l[i].SiteCode;
                r.Cells[11].Value = l[i].CurCode;
                No++;
            }
            r = Grd1.Rows.Add();
            r.Cells[0].Value = No;
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Grd1.EndUpdate();
        }

        private bool IsInsertedDataNotValid()
        {
            string menuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmVoucherRequestMulti2' Limit 1; ");
            var VRM2 = new FrmVoucherRequestMulti2(menuCode);
            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

            if (mIsClosingJournalBasedOnMultiProfitCenter && mIsAutoJournalActived)
            {
                VRM2.ProcessProfitCenter(ref Grd1, 1, 3, 12, 13);
            }

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsRemarkForApprovalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                //(mIsVoucherUseCashType && Sm.IsLueEmpty(LueCashTypeCode, "Cash Type")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                VRM2.IsClosingJournalForMultiVRInvalid(ref Grd1, DocDt, mIsClosingJournalBasedOnMultiProfitCenter, mIsAutoJournalActived, 1, 3, 12, 13)
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 bank account.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Bank Account (C)")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Bank Account (D)")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Amount is 0.00.")) return true;
            }
            return false;
        }

        //internal void SetLueCashTypeCode(ref LookUpEdit Lue, string CashTypeCode)
        //{
        //    Sm.SetLue2(
        //        ref Lue,
        //        "Select CashTypeCode As Col1, CashTypeName As Col2 From TblCashType Order By CashTypeName",
        //        0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //    if (CashTypeCode.Length != 0) Sm.SetLue(Lue, CashTypeCode);
        //}

        //private bool Process(int r, ref List<Result1> l1, ref List<Result2> l2, ref List<Result3> l3)
        //{
        //    var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");

        //    l1.Clear();
        //    l2.Clear();
        //    l3.Clear();

        //    Process1(ref r, ref l1);
        //    if (l1.Count > 0)
        //    {
        //        Process2(ref l1, ref l2);
        //        Process3(ref l1, ref l2, ref l3);
        //        if (IsInvalid(r, ref l1))
        //            return false;
        //        else
        //        {
        //            Process4(DocNo, ref l3, r);
        //            return true;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //private void Process1(ref int r, ref List<Result1> l)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Select B.DNo, A.WhsCode, B.ItCode, C.ItName, ");
        //    SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, B.UPrice ");
        //    SQL.AppendLine("From TblDOCtHdr A, TblDOCtDtl B, TblItem C ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo ");
        //    SQL.AppendLine("And A.DocNo=B.DocNo ");
        //    SQL.AppendLine("And B.CancelInd='N' ");
        //    SQL.AppendLine("And B.Qty>0.00 ");
        //    SQL.AppendLine("And B.ItCode=C.ItCode ");
        //    SQL.AppendLine("Order By B.DNo; ");

        //    Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 3));

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        { 
        //            "DNo", 
        //            "WhsCode", "ItCode", "ItName", "Qty", "Qty2", 
        //            "Qty3", "UPrice" 
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new Result1()
        //                {
        //                    DNo = Sm.DrStr(dr, c[0]),
        //                    WhsCode = Sm.DrStr(dr, c[1]),
        //                    ItCode = Sm.DrStr(dr, c[2]),
        //                    ItName = Sm.DrStr(dr, c[3]),
        //                    Qty = Sm.DrDec(dr, c[4]),
        //                    Qty2 = Sm.DrDec(dr, c[5]),
        //                    Qty3 = Sm.DrDec(dr, c[6]),
        //                    UPrice = Sm.DrDec(dr, c[7]),
        //                    Stock = 0m,
        //                    Stock2 = 0m,
        //                    Stock3 = 0m

        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void Process2(ref List<Result1> l1, ref List<Result2> l2)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    int i = 0;
        //    var Filter = string.Empty;

        //    SQL.AppendLine("Select Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3 ");
        //    SQL.AppendLine("From TblStockSummary ");
        //    SQL.AppendLine("Where Qty>0 ");
        //    SQL.AppendLine("And WhsCode=@WhsCode ");
        //    foreach (var x in l1)
        //    {
        //        if (i == 0) Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
        //        if (Filter.Length > 0) Filter += " Or ";
        //        Filter += " ItCode=@ItCode0" + i.ToString();
        //        Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), x.ItCode);
        //        ++i;
        //    }
        //    SQL.AppendLine(" And (" + Filter + "); ");


        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        { 
        //            "Lot", 
        //            "Bin", "ItCode", "PropCode", "BatchNo", "Source",
        //            "Qty", "Qty2", "Qty3" 
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l2.Add(new Result2()
        //                {
        //                    Lot = Sm.DrStr(dr, c[0]),
        //                    Bin = Sm.DrStr(dr, c[1]),
        //                    ItCode = Sm.DrStr(dr, c[2]),
        //                    PropCode = Sm.DrStr(dr, c[3]),
        //                    BatchNo = Sm.DrStr(dr, c[4]),
        //                    Source = Sm.DrStr(dr, c[5]),
        //                    Qty = Sm.DrDec(dr, c[6]),
        //                    Qty2 = Sm.DrDec(dr, c[7]),
        //                    Qty3 = Sm.DrDec(dr, c[8])
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void Process3(ref List<Result1> l1, ref List<Result2> l2, ref List<Result3> l3)
        //{
        //    decimal QtyTemp = 0m, QtyTemp2 = 0m, QtyTemp3 = 0m;
        //    foreach (var i in l1)
        //    {
        //        QtyTemp = i.Qty;
        //        QtyTemp2 = i.Qty2;
        //        QtyTemp3 = i.Qty3;
        //        foreach (var j in l2.Where(w => Sm.CompareStr(w.ItCode, i.ItCode) && w.Qty > 0m))
        //        {
        //            if (j.Qty >= QtyTemp)
        //            {
        //                l3.Add(new Result3()
        //                {
        //                    Lot = j.Lot,
        //                    Bin = j.Bin,
        //                    ItCode = j.ItCode,
        //                    PropCode = j.PropCode,
        //                    BatchNo = j.BatchNo,
        //                    Source = j.Source,
        //                    Qty = QtyTemp,
        //                    Qty2 = QtyTemp2,
        //                    Qty3 = QtyTemp3,
        //                    UPrice = i.UPrice
        //                });
        //                j.Qty -= QtyTemp;
        //                i.Stock += QtyTemp;
        //                QtyTemp = 0m;

        //                j.Qty2 -= QtyTemp2;
        //                i.Stock2 += QtyTemp2;
        //                QtyTemp2 = 0m;

        //                j.Qty3 -= QtyTemp3;
        //                i.Stock3 += QtyTemp3;
        //                QtyTemp3 = 0m;
        //            }
        //            else
        //            {
        //                l3.Add(new Result3()
        //                {
        //                    Lot = j.Lot,
        //                    Bin = j.Bin,
        //                    ItCode = j.ItCode,
        //                    PropCode = j.PropCode,
        //                    BatchNo = j.BatchNo,
        //                    Source = j.Source,
        //                    Qty = j.Qty,
        //                    Qty2 = j.Qty2,
        //                    Qty3 = j.Qty3,
        //                    UPrice = i.UPrice
        //                });
        //                i.Stock += j.Qty;
        //                QtyTemp -= j.Qty;
        //                j.Qty = 0m;

        //                i.Stock2 += j.Qty2;
        //                QtyTemp2 -= j.Qty2;
        //                j.Qty2 = 0m;

        //                i.Stock3 += j.Qty3;
        //                QtyTemp3 -= j.Qty3;
        //                j.Qty3 = 0m;
        //            }
        //            if (QtyTemp <= 0m) break;
        //        }
        //    }
        //}

        //private void Process4(string DocNo, ref List<Result3> l3, int r)
        //{
        //    var SQL = new StringBuilder();
        //    var cml = new List<MySqlCommand>();
        //    var cm = new MySqlCommand();
        //    int i = 0;

        //    SQL.AppendLine("Set @CurrentDt:=CurrentDateTime(); ");

        //    SQL.AppendLine("Insert Into TblDOCtHdr ");
        //    SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, DOCtDocNoSource, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
        //    SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocNo, @DocDt, @Status, @ProcessInd, @DOCtDocNo, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
        //    SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, Remark, @UserCode, @CurrentDt ");
        //    SQL.AppendLine("From TblDOCtHdr Where DocNo=@DOCtDocNo; ");

        //    if (mIsDOCtApprovalActived)
        //    {
        //        SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @CurrentDt ");
        //        SQL.AppendLine("From TblDocApprovalSetting T ");
        //        SQL.AppendLine("Where T.DocType='DOCt'; ");

        //        SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
        //        SQL.AppendLine("Where DocNo=@DocNo ");
        //        SQL.AppendLine("And Not Exists( ");
        //        SQL.AppendLine("    Select 1 From TblDocApproval ");
        //        SQL.AppendLine("    Where DocType='DOCt' ");
        //        SQL.AppendLine("    And DocNo=@DocNo ");
        //        SQL.AppendLine("    ); ");
        //    }

        //    foreach (var x in l3)
        //    {
        //        ++i;

        //        SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values(@DocNo, @DNo" + i.ToString() + ", 'N', @ItCode" + i.ToString() + ", @PropCode" + i.ToString() + ", @BatchNo" + i.ToString() + ", 'O', @Source" + i.ToString() + ", @Lot" + i.ToString() + ", @Bin" + i.ToString() + ", 0.00, Null, @Qty" + i.ToString() + ", @Qty2" + i.ToString() + ", @Qty3" + i.ToString() + ", @UPrice" + i.ToString() + ", Null, @UserCode, @CurrentDt); ");

        //        Sm.CmParam<String>(ref cm, "@DNo" + i.ToString(), Sm.Right("00" + (i + 1).ToString(), 3));
        //        Sm.CmParam<String>(ref cm, "@ItCode" + i.ToString(), x.ItCode);
        //        Sm.CmParam<String>(ref cm, "@PropCode" + i.ToString(), x.PropCode);
        //        Sm.CmParam<String>(ref cm, "@BatchNo" + i.ToString(), x.BatchNo);
        //        Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), x.Source);
        //        Sm.CmParam<String>(ref cm, "@Lot" + i.ToString(), x.Lot);
        //        Sm.CmParam<String>(ref cm, "@Bin" + i.ToString(), x.Bin);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty" + i.ToString(), x.Qty);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty2" + i.ToString(), x.Qty2);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty3" + i.ToString(), x.Qty3);
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice" + i.ToString(), x.UPrice);
        //    }

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@Status", mIsDOCtApprovalActived ? "O" : "A");
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseCtQtPrice ? "D" : "F");
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, r, 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", "07");

        //    if (!mIsDOCtUseCtQtPrice)
        //    {
        //        SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
        //        SQL.AppendLine("CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
        //        SQL.AppendLine("Case When A.Remark Is Null Then ");
        //        SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //        SQL.AppendLine("Else ");
        //        SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //        SQL.AppendLine("End As Remark, ");
        //        SQL.AppendLine("@UserCode, @CurrentDt ");
        //        SQL.AppendLine("From TblDOCtHdr A ");
        //        SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
        //        SQL.AppendLine("Where A.DocNo=@DocNo ");
        //        SQL.AppendLine("And A.Status='A'; ");

        //        SQL.AppendLine("Update TblStockSummary As A ");
        //        SQL.AppendLine("Inner Join ( ");
        //        SQL.AppendLine("    Select T1.WhsCode, T2.Lot, T2.Bin, T2.Source, ");
        //        SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
        //        SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
        //        SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
        //        SQL.AppendLine("    From TblDOCtHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
        //        SQL.AppendLine("    Where T1.DocNo=@DocNo ");
        //        SQL.AppendLine("    And T1.Status='A' ");
        //        SQL.AppendLine("    Group By T1.WhsCode, T2.Lot, T2.Bin, T2.Source ");
        //        SQL.AppendLine(") B ");
        //        SQL.AppendLine("    On A.WhsCode=B.WhsCode ");
        //        SQL.AppendLine("    And A.Lot=B.Lot ");
        //        SQL.AppendLine("    And A.Bin=B.Bin ");
        //        SQL.AppendLine("    And A.Source=B.Source ");
        //        SQL.AppendLine("Set ");
        //        SQL.AppendLine("    A.Qty=A.Qty-IfNull(B.Qty, 0.00), ");
        //        SQL.AppendLine("    A.Qty2=A.Qty2-IfNull(B.Qty2, 0.00), ");
        //        SQL.AppendLine("    A.Qty3=A.Qty3-IfNull(B.Qty3, 0.00), ");
        //        SQL.AppendLine("    A.LastUpBy=@UserCode, ");
        //        SQL.AppendLine("    A.LastUpDt=@CurrentDt; ");

        //        if (mIsAutoJournalActived)
        //        {
        //            SQL.AppendLine("Set @CurCode:=(Select CurCode From TblDOCtHdr Where DocNo=@DocNo); ");
        //            SQL.AppendLine("Set @EntCode:=(");
        //            SQL.AppendLine("    Select C.EntCode ");
        //            SQL.AppendLine("    From TblWarehouse A, TblCostCenter B, TblProfitCenter C ");
        //            SQL.AppendLine("    Where A.CCCode=B.CCCode And B.ProfitCenterCode=C.ProfitCenterCode ");
        //            SQL.AppendLine("    And A.WhsCode In (Select WhsCode From TblDOCtHdr Where DocNo=@DocNo) ");
        //            SQL.AppendLine("); ");

        //            SQL.AppendLine("Update TblDOCtHdr Set ");
        //            SQL.AppendLine("    JournalDocNo=");
        //            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
        //            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

        //            SQL.AppendLine("Insert Into TblJournalHdr ");
        //            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select A.JournalDocNo, ");
        //            SQL.AppendLine("A.DocDt, ");
        //            SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
        //            SQL.AppendLine("@MenuCode As MenuCode, ");
        //            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
        //            SQL.AppendLine("From TblDOCtHdr A ");
        //            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
        //            SQL.AppendLine("Where A.DocNo=@DocNo ");
        //            SQL.AppendLine("And A.Status='A'; ");

        //            SQL.AppendLine("Set @Row:=0; ");

        //            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
        //            SQL.AppendLine("From TblJournalHdr A ");
        //            SQL.AppendLine("Inner Join ( ");
        //            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

        //            if (mIsAcNoForSaleUseItemCategory)
        //            {
        //                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
        //                else
        //                    SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
        //                SQL.AppendLine("        0.00 As CAmt ");
        //                SQL.AppendLine("        From TblDOCtHdr A ");
        //                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
        //                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
        //                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
        //                SQL.AppendLine("        Where A.DocNo=@DocNo ");
        //                SQL.AppendLine("        Group By E.AcNo5 ");
        //            }
        //            else
        //            {
        //                SQL.AppendLine("        Select D.ParValue As AcNo, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
        //                else
        //                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
        //                SQL.AppendLine("        0.00 As CAmt ");
        //                SQL.AppendLine("        From TblDOCtHdr A ");
        //                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
        //                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
        //                SQL.AppendLine("        Where A.DocNo=@DocNo ");
        //            }

        //            SQL.AppendLine("        Union All ");
        //            SQL.AppendLine("        Select E.AcNo, ");
        //            SQL.AppendLine("        0.00 As DAmt, ");
        //            if (mIsDOCtAmtRounded)
        //                SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
        //            else
        //                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
        //            SQL.AppendLine("        From TblDOCtHdr A ");
        //            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
        //            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
        //            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
        //            SQL.AppendLine("        Where A.DocNo=@DocNo ");

        //            SQL.AppendLine("        Union All ");
        //            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
        //            if (mIsDOCtAmtRounded)
        //                SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
        //            else
        //                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
        //            SQL.AppendLine("        0.00 As CAmt ");
        //            SQL.AppendLine("        From ( ");
        //            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
        //            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
        //            SQL.AppendLine("            From TblDOCtHdr A ");
        //            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //            SQL.AppendLine("            Left Join ( ");
        //            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
        //            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
        //            SQL.AppendLine("            ) C On 0=0 ");
        //            SQL.AppendLine("            Where A.DocNo=@DocNo ");
        //            SQL.AppendLine("        ) X1 ");
        //            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
        //            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
        //            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
        //            SQL.AppendLine("        Union All ");

        //            if (mIsAcNoForSaleUseItemCategory)
        //            {
        //                SQL.AppendLine("        Select X.AcNo, ");
        //                SQL.AppendLine("        0.00 As DAmt, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
        //                else
        //                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
        //                SQL.AppendLine("        From ( ");
        //                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
        //                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
        //                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
        //                SQL.AppendLine("            As Amt ");
        //                SQL.AppendLine("            From TblDOCtHdr A ");
        //                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("            Left Join ( ");
        //                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
        //                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
        //                SQL.AppendLine("            ) C On 0=0 ");
        //                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
        //                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
        //                SQL.AppendLine("            Where A.DocNo=@DocNo ");
        //                SQL.AppendLine("        ) X ");
        //                SQL.AppendLine("        Group By X.AcNo ");
        //            }
        //            else
        //            {
        //                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
        //                SQL.AppendLine("        0.00 As DAmt, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
        //                else
        //                    SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
        //                SQL.AppendLine("        From ( ");
        //                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
        //                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
        //                SQL.AppendLine("            From TblDOCtHdr A ");
        //                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("            Left Join ( ");
        //                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
        //                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
        //                SQL.AppendLine("            ) C On 1=1 ");
        //                SQL.AppendLine("            Where A.DocNo=@DocNo ");
        //                SQL.AppendLine("        ) X1 ");
        //                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
        //                SQL.AppendLine("        Group By X2.ParValue ");
        //            }

        //            SQL.AppendLine("    ) Tbl Group By AcNo ");
        //            SQL.AppendLine(") B On 1=1 ");
        //            SQL.AppendLine("Where A.DocNo In ( ");
        //            SQL.AppendLine("    Select JournalDocNo ");
        //            SQL.AppendLine("    From TblDOCtHdr ");
        //            SQL.AppendLine("    Where DocNo=@DocNo ");
        //            SQL.AppendLine("    And JournalDocNo Is Not Null ");
        //            SQL.AppendLine("    ); ");
        //        }


        //        Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
        //        Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
        //    }

        //    cm.CommandText = SQL.ToString();
        //    cml.Add(cm);
        //    Sm.ExecCommands(cml);
        //}

        //private bool IsInvalid(int r, ref List<Result1> l)
        //{
        //    var Msg = string.Empty;
        //    var SQL = new StringBuilder();

        //    Grd1.Cells[r, 2].Value = string.Empty;

        //    SQL.AppendLine("Select 1 ");
        //    SQL.AppendLine("From TblDOCtHdr ");
        //    SQL.AppendLine("Where DocNo In ( ");
        //    SQL.AppendLine("    Select Distinct T1.DOCtDocNoSource ");
        //    SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
        //    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //    SQL.AppendLine("    And T1.Status='A' ");
        //    SQL.AppendLine("    And T1.DOCtDocNoSource Is Not Null ");
        //    SQL.AppendLine("    And T2.CancelInd='N' ");
        //    SQL.AppendLine(") And DocNo=@Param;");

        //    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, r, 3)))
        //    {
        //        Msg =
        //            "Document# : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
        //            "Warehouse : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
        //            "Customer : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine + Environment.NewLine +
        //            "This DO already copied.";
        //        Sm.StdMsg(mMsgType.Warning, Msg);
        //        Grd1.Cells[r, 2].Value = Msg;
        //        Grd1.Rows.AutoHeight();
        //        Sm.FocusGrd(Grd1, r, 2);
        //        return true;
        //    }
        //    foreach (var i in l)
        //    {
        //        if (i.Stock < i.Qty)
        //        {
        //            Msg =
        //                "Document# : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
        //                "Warehouse : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
        //                "Customer : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
        //                "Item's Code : " + i.ItCode + Environment.NewLine +
        //                "Item's Name : " + i.ItName + Environment.NewLine +
        //                "DO's Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
        //                "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
        //                "Not enough stock.";
        //            Sm.StdMsg(mMsgType.Warning, Msg);
        //            Msg =
        //                "Item's Code : " + i.ItCode + Environment.NewLine +
        //                "Item's Name : " + i.ItName + Environment.NewLine +
        //                "DO's Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
        //                "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
        //                "Not enough stock.";
        //            Grd1.Cells[r, 2].Value = Msg;
        //            Grd1.Rows.AutoHeight();
        //            Sm.FocusGrd(Grd1, r, 2);
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept?"Y":"N");    
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Result>();
            var l2 = new List<BankAccount>();

            ClearGrd();
            try
            {
                Import1(ref l);
                if (l.Count > 0)
                {
                    Import2(ref l2);
                    Import3(ref l, ref l2);
                    Import4(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                l2.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string BankAcCode { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcCode2 { get; set; }
            public string BankAcNm2 { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
            public string BankCode { get; set; }
            public string EntCode { get; set; }
            public string SiteCode { get; set; }
            public string CurCode { get; set; }
        }

        private class BankAccount
        {
            public string BankAcCode { get; set; }
            public string BankAcName { get; set; }
            public string BankCode { get; set; }
            public string EntCode { get; set; }
            public string SiteCode { get; set; }
            public string CurCode { get; set; }
        }

        #endregion
    }
}
