﻿#region Update
/*
    13/09/2022 [RDA/TWC] New apps based on RptTax 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptTax2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mSalaryInd = "1",
            mRptTaxType = "1";
        private bool 
            mIsNotFilterByAuthorization = false,
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptTax2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                //Sl.SetLueMth(LueMth);
                //Sl.SetLueYr(LueYr, string.Empty);

                var Dt = Sm.ServerCurrentDateTime();
                //Sm.SetLue(LueYr, Sm.Left(Dt, 4));
                //Sm.SetLue(LueMth, Dt.Substring(4, 2));

                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mRptTaxType = Sm.GetParameter("RptTaxType");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.PayrunCode, E.PayrunName, A.EmpCode, B.EmpName, B.DeptCode, B.SiteCode, C.DeptName, D.SiteName, ");
            SQL.AppendLine("Concat(Left(E.EndDt, 4), '-', Substring(E.EndDt, 5, 2)) As Period,  ");
            SQL.AppendLine("A.NPWP, A.PTKP, E.PayrunPeriod, G.OptDesc As PayrunPeriodDesc, A.Salary, A.FixAllowance, A.OT1Amt+A.OT2Amt+A.OTHolidayAmt As OT, A.IncProduction, ");
            if(mSalaryInd == "2")
            {
                SQL.AppendLine(" (IfNull(A.Salary,0)+IfNull(A.IncEmployee,0)+IfNull(A.TaxableFixAllowance,0)+IfNull(A.OT1Amt,0)+IfNull(A.OT2Amt,0)+IfNull(A.OTHolidayAmt,0)+ ");
                SQL.AppendLine(" IfNull(A.Transport,0)+IfNull(A.Meal,0)+IfNull(A.FieldAssignment,0)+IfNull(A.SSemployeremployment,0)-IfNull(A.SSemployerpension,0)+IfNull(A.SalaryAdjustment,0)) As Bruto, ");
            }
            if(mSalaryInd=="1")
            {
                SQL.AppendLine("(IfNull(A.Salary,0)+ IfNull(A.ProcessPLAmt,0)-IfNull(A.ProcessUPLAmt,0)+ IfNull(A.OT1Amt,0)+ IfNull(A.OT2Amt,0)+ ");
                SQL.AppendLine("IfNull(A.OTHolidayAmt,0)+ IfNull(A.TaxableFixAllowance,0)+ IfNull(A.EmploymentPeriodAllowance,0)+ IfNull(A.IncEmployee,0)+ ");
                SQL.AppendLine("IfNull(A.IncMinWages,0)+ IfNull(A.IncProduction,0)+ IfNull(A.IncPerformance,0)+ IfNull(A.PresenceReward,0)+ IfNull(A.HolidayEarning,0)+ ");
                SQL.AppendLine("IfNull(A.ExtraFooding,0)+ IfNull(A.SSEmployerHealth,0)+ IfNull(A.SSEmployerEmployment,0)+ IfNull(A.SalaryAdjustment,0)- ");
                SQL.AppendLine("IfNull(A.TaxableFixDeduction,0) - IfNull(A.DedEmployee,0)- IfNull(A.DedProduction,0)- IfNull(A.DedProdLeave,0)- IfNull(A.SSEmployeeEmployment,0)) As Bruto, ");
            }
            if (mSalaryInd == "3")
            {
                if (Sm.GetParameter("DocTitle") == "HIN")
                {
                    //SQL.AppendLine("Case When D.HOInd='Y' Then ");
                    //SQL.AppendLine("    A.Salary ");
                    //SQL.AppendLine("    +A.TaxableFixAllowance ");
                    //SQL.AppendLine("    -A.TaxableFixDeduction ");
                    //SQL.AppendLine("    +A.Transport ");
                    //SQL.AppendLine("    +A.Meal ");
                    //SQL.AppendLine("    +A.ADLeave ");
                    //SQL.AppendLine("    +A.SalaryAdjustment ");
                    //SQL.AppendLine("    +A.ServiceChargeIncentive ");
                    //SQL.AppendLine("    - (A.SSEmployeeHealth ");
                    //SQL.AppendLine("    + A.SSEmployeeEmployment ");
                    //SQL.AppendLine("    + A.SSEePension ");
                    //SQL.AppendLine("    + A.SSEmployeePension ");
                    //SQL.AppendLine("    + A.SSEmployeePension2 ");
                    //SQL.AppendLine("    ) ");
                    //SQL.AppendLine("Else ");
                    //SQL.AppendLine("    A.Salary ");
                    //SQL.AppendLine("    +A.TaxableFixAllowance ");
                    //SQL.AppendLine("    -A.TaxableFixDeduction ");
                    //SQL.AppendLine("    +A.Transport ");
                    //SQL.AppendLine("    +A.Meal ");
                    //SQL.AppendLine("    +A.ADLeave ");
                    //SQL.AppendLine("    +A.SalaryAdjustment ");
                    //SQL.AppendLine("    +A.ServiceChargeIncentive ");
                    //SQL.AppendLine("    - (A.SSEmployeeHealth ");
                    //SQL.AppendLine("    + A.SSEmployeeEmployment ");
                    //SQL.AppendLine("    + A.SSEePension ");
                    //SQL.AppendLine("    + A.SSEmployeePension ");
                    //SQL.AppendLine("    + A.SSEmployeePension2 ");
                    //SQL.AppendLine("    ) ");
                    //SQL.AppendLine("End As Bruto, ");
                    SQL.AppendLine("A.Salary + ");
                    SQL.AppendLine("A.TaxableFixAllowance + ");
                    SQL.AppendLine("A.Transport + ");
                    SQL.AppendLine("A.Meal + ");
                    SQL.AppendLine("A.ADLeave + ");
                    SQL.AppendLine("A.SalaryAdjustment + ");
                    SQL.AppendLine("A.ServiceChargeIncentive + ");
                    SQL.AppendLine("A.SSEmployerHealth + ");
                    SQL.AppendLine("A.SSErLifeInsurance + ");
                    SQL.AppendLine("A.SSErWorkingAccident ");
                    SQL.AppendLine("As Bruto, ");
                }
                else
                {
                    SQL.AppendLine("IfNull(A.Salary,0)+ IfNull(A.ProcessPLAmt,0)+ IfNull(A.OT1Amt,0)+ IfNull(A.OT2Amt,0)+ ");
                    SQL.AppendLine("IfNull(A.OTHolidayAmt,0)+IfNull(A.TaxableFixAllowance,0)-IfNull(A.TaxableFixDeduction,0)+");
                    SQL.AppendLine("IfNull(A.Transport,0)+ IfNull(A.Meal,0)+ IfNull(A.SalaryAdjustment,0)-IfNull(A.EmpAdvancePayment,0)+IfNull(A.ServiceChargeIncentive,0) ");
                    SQL.AppendLine("As Bruto, ");
                }
            }
            SQL.AppendLine("ifnull(H.Value, 0.00) RHA, ifnull(I.Value, 0.00) Bonus, A.EOYTax+A.Tax+ifnull(H.Value, 0.00)+ifnull(I.Value, 0.00) As Tax,  ");
            SQL.AppendLine("H.DocNoRHA, I.DocNoBonus ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun E ");
            SQL.AppendLine("    On A.PayrunCode=E.PayrunCode ");
            SQL.AppendLine("    And E.CancelInd='N' ");
            //SQL.AppendLine("    And Substring(E.EndDt,5,2)=@Mth ");
            //SQL.AppendLine("    And Left(E.EndDt, 4)=@Yr ");
            SQL.AppendLine("    AND E.EndDt BETWEEN @StartDt AND @EndDt ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And B.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On B.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.EmpCode, Sum(B.Amt) As Amt ");
            SQL.AppendLine("    From TblAllowancededuction A ");
            SQL.AppendLine("    Inner Join TblEmployeeAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("    Where A.TaxInd='Y' ");
            SQL.AppendLine("    Group by B.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            SQL.AppendLine("Left Join TblOption G On E.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT A.DocNo DocNoRHA, B.EmpCode, B.Value, LEFT(A.DocDt, 6) RHADt ");
            SQL.AppendLine("    FROM tblrhahdr A ");
            SQL.AppendLine("    INNER JOIN tblrhadtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    LEFT JOIN tblvoucherrequesthdr C ON A.VoucherRequestDocNo = C.DocNo  ");
            SQL.AppendLine("    LEFT JOIN tblvoucherhdr D ON C.VoucherDocNo = D.DocNo AND D.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("    WHERE A.CancelInd = 'N' AND A.Status = 'A' AND D.CancelInd = 'N' ");
            SQL.AppendLine("    AND LEFT(A.DocDt, 6) BETWEEN LEFT(@StartDt, 6) AND LEFT(@EndDt, 6) ");
            SQL.AppendLine(")H ON A.EmpCode = H.EmpCode AND LEFT(E.PayrunCode, 6) = H.RHADt ");
            SQL.AppendLine("LEFT JOIN( ");
            SQL.AppendLine("    SELECT A.DocNo DocNoBonus, B.EmpCode, B.Value, LEFT(A.DocDt, 6) BonusDt ");
            SQL.AppendLine("    FROM tblbonushdr A ");
            SQL.AppendLine("    INNER JOIN tblbonusdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    LEFT JOIN tblvoucherrequesthdr C ON A.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("    LEFT JOIN tblvoucherhdr D ON C.VoucherDocNo = D.DocNo AND D.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("    WHERE A.CancelInd = 'N' AND A.Status = 'A' AND D.CancelInd = 'N' ");
            SQL.AppendLine("    AND LEFT(A.DocDt, 6) BETWEEN LEFT(@StartDt, 6) AND LEFT(@EndDt, 6) ");
            SQL.AppendLine(")I ON A.EmpCode = I.EmpCode AND LEFT(E.PayrunCode, 6) = I.BonusDt ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(" )X ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Year",
                        "Month",
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun Name",
                        "Employee's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Employee's Name",
                        "Department",
                        "Site",
                        "Period",
                        "NPWP",
                       
                        //11-15
                        "PTKP",
                        "Payrun Period",
                        "Salary",
                        "Allowance",
                        "Overtime",

                        //16-20
                        "Production"+Environment.NewLine+"Incentive",
                        "Bruto",
                        "RHA",
                        "Bonus",
                        "Total Tax",
                        "",
                        
                        "#Document RHA",
                        "#Document Bonus"
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        60, 60, 100, 170, 100, 
                        
                        //6-10
                        200, 150, 100, 0, 140, 
                        
                        //11-15
                        100, 150, 100, 100, 100, 
                        
                        //16-20
                        100, 120, 100, 100, 100,

                        //21-23
                        20, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16, 17, 18, 19, 20 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 21 });
            if (mRptTaxType=="1")
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 9, 22, 23 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16, 22, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23 });
        }

        override protected void HideInfoInGrd()
        {
            if (mRptTaxType == "1")
                Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 9, 22, 23 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16, 22, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            //if (Sm.IsLueEmpty(LueMth, "Month")) return;
            //if (Sm.IsLueEmpty(LueYr, "Year")) return;

            if (Sm.IsDteEmpty(DteStartDt, "Start Date")) return;
            if (Sm.IsDteEmpty(DteEndDt, "End Date")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " where 1=1 "; 
                    
                //string Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                //Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParam<String>(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "X.EmpCode", "X.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPayCode.Text, new string[] { "X.PayrunCode", "X.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "X.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "X.PayrunPeriod", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "PayrunCode",

                            //1-5
                            "PayrunName", "EmpCode", "EmpName","DeptName", "SiteName",
                            
                            //6-10
                            "Period", "NPWP", "PTKP", "PayrunPeriodDesc", "Salary", 
                            
                            //11-15
                            "FixAllowance",  "OT", "IncProduction", "Bruto", "RHA",
                            
                            //16-19
                            "Bonus", "Tax", "DocNoRHA", "DocNoBonus"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            //Grd.Cells[Row, 1].Value = Yr;
                            //Grd.Cells[Row, 2].Value = Mth;
                            Grd.Cells[Row, 1].Value = string.Empty;
                            Grd.Cells[Row, 2].Value = string.Empty;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                        }, true, false, true, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 13, 14, 15, 16, 17, 18, 19, 20 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, r, 3).Length != 0 && Sm.GetGrdStr(Grd1, r, 5).Length != 0)
            {
                 ShowTaxInformation(Sm.GetGrdStr(Grd1, r, 3), Sm.GetGrdStr(Grd1, r, 5), Sm.GetGrdStr(Grd1, r, 22), Sm.GetGrdStr(Grd1, r, 23));
            }
        }

        private void ShowTaxInformation(string PayrunCode, string EmpCode, string DocNoRHA, string DocNoBonus)
        {
            StringBuilder
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("SELECT A.PayrunCode, B.EndDt, A.EmpCode, ifnull(A.Tax, 0.00) TaxPayroll, ifnull(C.Tax, 0.00) TaxRHA, ifnull(D.Tax, 0.00) TaxBonus ");
            SQL.AppendLine("FROM tblpayrollprocess1 A ");
            SQL.AppendLine("INNER JOIN tblpayrun B ON A.PayrunCode = B.PayrunCode ");
            SQL.AppendLine("LEFT JOIN( ");
            SQL.AppendLine("	SELECT A.PayrunCode, B.EndDt, A.EmpCode, C.Tax ");
            SQL.AppendLine("	FROM tblpayrollprocess1 A ");
            SQL.AppendLine("	INNER JOIN tblpayrun B ON A.PayrunCode = B.PayrunCode ");
            SQL.AppendLine("	INNER JOIN tblrhadtl C ON A.EmpCode = C.EmpCode ");
            SQL.AppendLine("	INNER JOIN tblrhahdr D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("	WHERE A.PayrunCode = @PayrunCode AND A.EmpCode = @EmpCode  ");
            SQL.AppendLine("	AND D.DocNo=@DocNoRHA ");
            SQL.AppendLine(")C ON A.EmpCode = C.EmpCode ");
            SQL.AppendLine("LEFT JOIN( ");
            SQL.AppendLine("	SELECT A.PayrunCode, B.EndDt, A.EmpCode, C.Tax ");
            SQL.AppendLine("	FROM tblpayrollprocess1 A ");
            SQL.AppendLine("	INNER JOIN tblpayrun B ON A.PayrunCode = B.PayrunCode ");
            SQL.AppendLine("	INNER JOIN tblbonusdtl C ON A.EmpCode = C.EmpCode ");
            SQL.AppendLine("	INNER JOIN tblbonushdr D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("	WHERE A.PayrunCode = @PayrunCode AND A.EmpCode = @EmpCode  ");
            SQL.AppendLine("	AND D.DocNo=@DocNoBonus ");
            SQL.AppendLine(")D ON A.EmpCode = D.EmpCode ");
            SQL.AppendLine("WHERE A.PayrunCode = @PayrunCode AND A.EmpCode = @EmpCode ");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParam<String>(ref cm, "@DocNoRHA", DocNoRHA);
                    Sm.CmParam<String>(ref cm, "@DocNoBonus", DocNoBonus);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "PayrunCode", "EmpCode", "TaxPayroll", "TaxRHA", "TaxBonus" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append("Payrun Code : ");
                            Msg.AppendLine(Sm.DrStr(dr, c[0]));
                            Msg.Append("Employee Code : ");
                            Msg.AppendLine(Sm.DrStr(dr, c[1]));
                            Msg.Append("\nTax Payroll : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[2]), 0));
                            Msg.Append("Tax RHA : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[3]), 0));
                            Msg.Append("Tax Bonus : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[4]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
 
        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (DteEndDt.Text != string.Empty)
            {
                if (Sm.CompareDtTm(Sm.GetDte(DteEndDt), Sm.GetDte(DteStartDt)) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Start date is later than end date.");
                    Sm.SetDte(DteStartDt, Sm.GetDte(DteEndDt));
                }
            }
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (DteStartDt.Text != string.Empty)
            {
                if (Sm.CompareDtTm(Sm.GetDte(DteStartDt), Sm.GetDte(DteEndDt)) > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                    Sm.SetDte(DteEndDt, Sm.GetDte(DteStartDt));
                }
            }
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtPayCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }
        
        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        #endregion

        #endregion
    }
}
