﻿namespace RunSystem
{
    partial class FrmUpdateVoucherCashType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdateVoucherCashType));
            this.BtnVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOCDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LblCashType = new System.Windows.Forms.Label();
            this.LueCashTypeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtOldCashTypeCode = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOldCashTypeCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(689, 0);
            this.panel1.Size = new System.Drawing.Size(70, 155);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(70, 33);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtOldCashTypeCode);
            this.panel2.Controls.Add(this.LueCashTypeCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LblCashType);
            this.panel2.Controls.Add(this.BtnVoucher);
            this.panel2.Controls.Add(this.BtnSOCDocNo);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(689, 155);
            // 
            // BtnVoucher
            // 
            this.BtnVoucher.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucher.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucher.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucher.Appearance.Options.UseBackColor = true;
            this.BtnVoucher.Appearance.Options.UseFont = true;
            this.BtnVoucher.Appearance.Options.UseForeColor = true;
            this.BtnVoucher.Appearance.Options.UseTextOptions = true;
            this.BtnVoucher.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucher.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucher.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucher.Image")));
            this.BtnVoucher.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucher.Location = new System.Drawing.Point(363, 11);
            this.BtnVoucher.Name = "BtnVoucher";
            this.BtnVoucher.Size = new System.Drawing.Size(24, 19);
            this.BtnVoucher.TabIndex = 5;
            this.BtnVoucher.ToolTip = "Browse File";
            this.BtnVoucher.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucher.ToolTipTitle = "Run System";
            this.BtnVoucher.Click += new System.EventHandler(this.BtnVoucher_Click);
            // 
            // BtnSOCDocNo
            // 
            this.BtnSOCDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOCDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOCDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOCDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOCDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseFont = true;
            this.BtnSOCDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOCDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOCDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOCDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOCDocNo.Location = new System.Drawing.Point(361, 11);
            this.BtnSOCDocNo.Name = "BtnSOCDocNo";
            this.BtnSOCDocNo.Size = new System.Drawing.Size(24, 19);
            this.BtnSOCDocNo.TabIndex = 25;
            this.BtnSOCDocNo.ToolTip = "Browse File";
            this.BtnSOCDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOCDocNo.ToolTipTitle = "Run System";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(98, 13);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 25;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtDocNo.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(23, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "Document#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblCashType
            // 
            this.LblCashType.AutoSize = true;
            this.LblCashType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashType.ForeColor = System.Drawing.Color.Black;
            this.LblCashType.Location = new System.Drawing.Point(10, 39);
            this.LblCashType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCashType.Name = "LblCashType";
            this.LblCashType.Size = new System.Drawing.Size(86, 14);
            this.LblCashType.TabIndex = 6;
            this.LblCashType.Text = "Old Cash Type";
            this.LblCashType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashTypeCode
            // 
            this.LueCashTypeCode.EnterMoveNextControl = true;
            this.LueCashTypeCode.Location = new System.Drawing.Point(98, 59);
            this.LueCashTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashTypeCode.Name = "LueCashTypeCode";
            this.LueCashTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.Appearance.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashTypeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashTypeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashTypeCode.Properties.DropDownRows = 25;
            this.LueCashTypeCode.Properties.NullText = "[Empty]";
            this.LueCashTypeCode.Properties.PopupWidth = 250;
            this.LueCashTypeCode.Size = new System.Drawing.Size(531, 20);
            this.LueCashTypeCode.TabIndex = 9;
            this.LueCashTypeCode.ToolTip = "F4 : Show/hide list";
            this.LueCashTypeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 62);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "New Cash Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOldCashTypeCode
            // 
            this.TxtOldCashTypeCode.EnterMoveNextControl = true;
            this.TxtOldCashTypeCode.Location = new System.Drawing.Point(98, 35);
            this.TxtOldCashTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOldCashTypeCode.Name = "TxtOldCashTypeCode";
            this.TxtOldCashTypeCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOldCashTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOldCashTypeCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOldCashTypeCode.Properties.Appearance.Options.UseFont = true;
            this.TxtOldCashTypeCode.Properties.MaxLength = 25;
            this.TxtOldCashTypeCode.Properties.ReadOnly = true;
            this.TxtOldCashTypeCode.Size = new System.Drawing.Size(531, 20);
            this.TxtOldCashTypeCode.TabIndex = 7;
            // 
            // FrmUpdateVoucherCashType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 155);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmUpdateVoucherCashType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOldCashTypeCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnVoucher;
        public DevExpress.XtraEditors.SimpleButton BtnSOCDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCashTypeCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblCashType;
        internal DevExpress.XtraEditors.TextEdit TxtOldCashTypeCode;
    }
}