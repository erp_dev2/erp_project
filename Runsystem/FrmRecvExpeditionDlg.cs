﻿#region Update
/*
    27/01/2020 [TKG/IMS] New Application
    24/04/2020 [TKG/IMS] item yg sudah direceived partial sisanya bisa diproses oleh expedisi lagi
    02/10/2020 [TKG/IMS] receiving expedition ketika diproses di recv vd sisanya akan hangus kalau tidak diproses, dan harus diproses kembali di recv expedition
    04/11/2020 [TKG/IMS] hanya item non jasa yg bisa diproses
    18/12/2020 [WED/IMS] hitung dari Recv Expedition yang status nya O dan A
    21/01/2021 [ICA/IMS] menghapus E.ServiceItemInd='N' pada saat show data
    31/03/2021 [WED/IMS] data recv expedition yang di reject, muncul lagi di sini
    08/04/2021 [WED/IMS] perbaikan query lagi
    04/08/2021 [vin/IMS] PO service dapat ditarik 
    13/08/2021 [vin/IMS] Bug : quantuty reject 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvExpeditionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvExpedition mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvExpeditionDlg(FrmRecvExpedition FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "PO#",
                    "DNo", 
                    "Date",
                    "Local#",

                    //6-10
                    "Item's Code", 
                    "Item's Name", 
                    "Local Code",
                    "Specification",
                    "Outstanding",

                    //11-15
                    "UoM"+Environment.NewLine+"(Purchase)",
                    "UoM"+Environment.NewLine+"(Inventory 1)",
                    "UoM"+Environment.NewLine+"(Inventory 2)",
                    "UoM"+Environment.NewLine+"(Inventory 3)",
                    "PO's Remark",

                    //16-19
                    "Project's Code", 
                    "Project's Name",
                    "Customer's PO#", 
                    "PO DocNo"
                },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 0, 80, 100,
                        
                        //6-10
                        100, 200, 100, 200, 100, 
                        
                        //11-15
                        60, 0, 0, 0, 200, 
                        
                        //16-19
                        120, 200, 130, 100
                    }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 12, 13, 14, 15, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select ifnull(F2.DocNo, A.DocNo) DocNo, A.DocNo PODocNo, B.DNo, A.DocDt, A.LocalDocNo, ");
            SQL.AppendLine("    D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, ");
            SQL.AppendLine("    E.PurchaseUomCode, E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("    Trim(Concat(IfNull(A.Remark, ''), ' ', IfNull(B.Remark, ''))) As PORemark, ");
            SQL.AppendLine("    B.Qty-IfNull(H.Qty2, 0.00)-IfNull(I.Qty3, 0.00) As OutstandingQty, ");
            SQL.AppendLine("    J.ProjectCode, J.ProjectName, G.PONo ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd<>'F' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr F On C.MaterialRequestDocNo=F.DocNo ");
            SQL.AppendLine("    Left Join TblMaterialRequestDtl F2 On C.MaterialRequestDocNo=F2.DocNo And C.MaterialRequestDNo=F2.DNo And F2.MaterialRequestServiceDocNo is not null ");
            SQL.AppendLine("    Left Join TblSOContractHdr G ON F.SOCDocNo=G.DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, ");
            //SQL.AppendLine("        Sum(T1.QtyPurchase-(T1.QtyPurchase-T1.RecvVdQtyPurchase)) As Qty2 ");
            SQL.AppendLine("        Sum(Case When T1.RecvVdQtyPurchase<=0.00 Then T1.QtyPurchase Else T1.RecvVdQtyPurchase End) As Qty2 ");
		    SQL.AppendLine("        From TblRecvExpeditionDtl T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPOHdr T3 ");
            SQL.AppendLine("            On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("            And T3.VdCode=@VdCode ");
            SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And T3.Status='A' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            // rejected data
            SQL.AppendLine("        And Concat(T1.DocNo, T1.DNo) In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select COncat(RecvExpeditionDocNo, RecvExpeditionDNo) ");
            SQL.AppendLine("            From TblRecvVdDtl ");
            SQL.AppendLine("            Where CancelInd = 'N' ");
            SQL.AppendLine("            And Status In ('O', 'A') ");
            SQL.AppendLine("            And RejectedInd = 'N' ");
            SQL.AppendLine("        ) ");
            // end rejected data
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
	        SQL.AppendLine("    ) H On A.DocNo=H.DocNo And B.DNo=H.DNo ");
	        SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPOHdr T3 ");
            SQL.AppendLine("            On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("            And T3.VdCode=@VdCode ");
            SQL.AppendLine("            And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            And T3.Status='A' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) I On A.DocNo=I.DocNo And B.DNo=I.DNo  ");
            SQL.AppendLine("    Left Join TblProjectGroup J On F.PGCode=J.PGCode ");
            SQL.AppendLine("    Where A.VdCode=@VdCode ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.Status='A' ");
            SQL.AppendLine(") X ");
            SQL.AppendLine("Where X.OutstandingQty>0.00 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName", "X.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By X.DocDt, X.DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
                        
                        //1-5
                        "DNo", "DocDt", "LocalDocNo", "ItCode", "ItName", 

                        //6-10
                        "ItCodeInternal", "Specification", "OutstandingQty", "PurchaseUomCode", "InventoryUomCode", 

                        //11-15
                        "InventoryUomCode2", "InventoryUomCode3", "PORemark", "ProjectCode", "ProjectName", 

                        //16-17
                        "PONo", "PODocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 2);//po biasa 
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 19);//po service 
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 16); //-> project code dicopy ke batch#

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 18, 20, 22 });
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, Row1, new int[] { 1, 2 });
                        Sm.SetGrdStringValueEmpty(mFrmParent.Grd1, Row1, new int[] { 12, 14, 15, 24 });

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 11), Sm.GetGrdStr(Grd1, Row2, 12)))
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 10);
                        else
                            mFrmParent.Grd1.Cells[Row1, 18].Value = 0m;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 11), Sm.GetGrdStr(Grd1, Row2, 13)))
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 10);
                        else
                            mFrmParent.Grd1.Cells[Row1, 20].Value = 0m;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 11), Sm.GetGrdStr(Grd1, Row2, 14)))
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 10);
                        else
                            mFrmParent.Grd1.Cells[Row1, 22].Value = 0m;

                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 11), Sm.GetGrdStr(Grd1, Row2, 12)))
                        {
                            Sm.ComputeQtyBasedOnConvertionFormula("12", mFrmParent.Grd1, Row1, 8, 18, 20, 22, 19, 21, 23);
                            Sm.ComputeQtyBasedOnConvertionFormula("13", mFrmParent.Grd1, Row1, 8, 18, 22, 20, 19, 23, 21);
                        }
                        mFrmParent.ShowPOInfo(Row1);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 16, 18, 20, 22 });
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");

            mFrmParent.SetSeqNo();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
