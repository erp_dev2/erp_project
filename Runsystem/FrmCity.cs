﻿#region Update
/*
    22/07/2021 [MYA/PHT] Menambahkan field location pada master city
    15/03/2023 [RDA/PHT] Menambahkan field region pada master city

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCity : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmCityFind FrmFind;

        #endregion

        #region Constructor

        public FrmCity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
            Sl.SetLueProvCode(ref LueProvCode);
            Sl.SetLueRingCode(ref LueRingCode);
            Sl.SetLueOption(ref LueLocationCode, "LevelLocation");
            Sl.SetLueRegionCode(ref LueRegionCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCityCode, TxtCityName, LueProvCode, LueRingCode, LueLocationCode, LueRegionCode
                    }, true);
                    TxtCityCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCityCode, TxtCityName, LueProvCode, LueRingCode, LueLocationCode, LueRegionCode
                    }, false);
                    TxtCityCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtCityCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCityName, LueProvCode, LueRingCode, LueLocationCode, LueRegionCode
                    }, false);
                    TxtCityName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCityCode, TxtCityName, LueProvCode, LueRingCode, LueLocationCode, LueRegionCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCityFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCityCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCityCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCity Where CityCode=@CityCode" };
                Sm.CmParam<String>(ref cm, "@CityCode", TxtCityCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCity(CityCode, CityName, ProvCode, RingCode, LocationCode, RegionCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CityCode, @CityName, @ProvCode, @RingCode, @LocationCode, @RegionCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CityName=@CityName, ProvCode=@ProvCode, RingCode=@RingCode, LocationCode=@LocationCode, RegionCode=@RegionCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CityCode", TxtCityCode.Text);
                Sm.CmParam<String>(ref cm, "@CityName", TxtCityName.Text);
                Sm.CmParam<String>(ref cm, "@ProvCode", Sm.GetLue(LueProvCode));
                Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
                Sm.CmParam<String>(ref cm, "@LocationCode", Sm.GetLue(LueLocationCode));
                Sm.CmParam<String>(ref cm, "@RegionCode", Sm.GetLue(LueRegionCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtCityCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProvCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CityCode", ProvCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select CityCode, CityName, ProvCode, RingCode, LocationCode, RegionCode From TblCity Where CityCode=@CityCode",
                        new string[] 
                        {
                            "CityCode", "CityName", "ProvCode", "RingCode", "LocationCode", "RegionCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCityCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCityName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueProvCode, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueRingCode, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueLocationCode, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueRegionCode, Sm.DrStr(dr, c[5]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCityCode, "City code", false) ||
                Sm.IsTxtEmpty(TxtCityName, "City name", false) ||
                Sm.IsLueEmpty(LueProvCode, "Province") ||
                IsCityCodeExisted();
        }

        private bool IsCityCodeExisted()
        {
            if (!TxtCityCode.Properties.ReadOnly && Sm.IsDataExist("Select CityCode From TblCity Where CityCode='" + TxtCityCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "City code ( " + TxtCityCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCityCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCityCode);
        }

        private void TxtCityName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCityName);
        }

        private void LueProvCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
        }

        private void LueRingCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueRingCode, new Sm.RefreshLue1(Sl.SetLueRingCode));
        }

        private void LueLocationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocationCode, new Sm.RefreshLue1(Sl.SetLueLocCode));
        }

        #endregion

        #endregion
    }
}
