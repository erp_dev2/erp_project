﻿#region Update
/*
 * 29/12/2021 [TYO/PRODUCT] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAsset2Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAsset2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAsset2Dlg3(FrmAsset2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Receiving Item From Vendor";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Receiving#",
                    "",
                    "Local# PO",
                    "DNo",

                    //6-10
                    "Date",
                    "Item Code",
                    "Item Name",
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",

                    //11-15
                    "Currency",
                    "Amount",
                    "POInd",
                    "Currency Rate",
                    "Amount IDR"
                },
                new int[] 
                {
                    //0
                    50,
                    
                    //1-5
                    20, 180, 20, 180, 0, 

                    //6-10
                    80, 100, 200, 100, 100, 

                   //11-15
                    80, 120, 0, 0, 0
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7, 13, 14, 15 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 12, 14, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            if (!mFrmParent.mIsRecvForAssetShowPOLocalDocNo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, IFNULL(U.ExcRate, 1) AS ExcRate, (T.Amt*IFNULL(U.ExcRate, 1)) AS AmtIDR From ( ");

            SQL.AppendLine("Select A.DocNo, B.DNo, B.Source, A.DocDt, B.ItCode, C.ItName, C.ItCodeInternal, C.ForeignName, ");
            SQL.AppendLine("(B.Qty ");
            if (mFrmParent.mIsAssetRecvQtyEditable) SQL.AppendLine(" - IfNull(F.AssetQty, 0.00) ");
            SQL.AppendLine(") as Qty, ");
            SQL.AppendLine("C.InventoryUomCode As Uom, D.CurCode, ");
            if (mFrmParent.mIsAssetRecvQtyEditable)
            {
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When B.UPrice > 0 Then B.UPrice ");
                SQL.AppendLine("    When E.UPrice > 0 Then E.Uprice ");
                SQL.AppendLine("    Else 0.00 ");
                SQL.AppendLine("End As Amt, ");
            }
            else
                SQL.AppendLine("((((100-E.Discount)/100)*(E.POQty * E.UPrice)) - E.DiscountAmt + E.RoundingValue) As Amt, ");
            SQL.AppendLine("A.POInd, D.LocalDocNo As LocalDocNo ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblPOHdr D On B.PODocNo = D.DocNo ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T1.DocNo, T1.DNo, T3.UPrice, T1.Qty As POQty, T1.Discount, T1.DiscountAmt, T1.RoundingValue ");
	        SQL.AppendLine("    From TblPODtl T1 ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo = T2.DocNo And T1.PORequestDNo = T2.DNo ");
	        SQL.AppendLine("    Inner Join TblQtDtl T3 On T2.QtDocNo = T3.DocNo And T2.QtDNo = T3.DNo ");
            SQL.AppendLine(") E On D.DocNo = E.DocNo And B.PODNo = E.DNo ");
            if (mFrmParent.mIsAssetRecvQtyEditable)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T2.RecvVdDocNo, T2.RecvVdDNo, Sum(T2.Qty) AssetQty ");
                SQL.AppendLine("    From TblAsset T1 ");
                SQL.AppendLine("    Inner Join TblAssetDtl T2 On T1.AssetCode = T2.AssetCode ");
                SQL.AppendLine("        And T1.ActiveInd = 'Y' ");
                SQL.AppendLine("    Group BY T2.RecvVdDocNo, T2.RecvVdDNo ");
                SQL.AppendLine(") F On B.DocNo = F.RecvVdDocNo And B.DNo = F.RecvVdDNo ");
            }
           
            SQL.AppendLine("Where B.CancelInd = 'N' ");
            SQL.AppendLine("And B.Status = 'A' ");
            SQL.AppendLine("And C.FixedItemInd = 'Y' ");
            SQL.AppendLine("And A.POInd = 'Y' ");
            if(!mFrmParent.mIsAssetRecvQtyEditable)
                SQL.AppendLine("And B.AssetCode Is Null ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And Locate(Concat('##', A.DocNo, '*', B.DNo, '##'), @SelectedRecvVd)<1 ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DocNo, B.DNo, B.Source, A.DocDt, B.ItCode, C.ItName, C.ItCodeInternal, C.ForeignName, ");
            SQL.AppendLine("(B.Qty ");
            if (mFrmParent.mIsAssetRecvQtyEditable) SQL.AppendLine(" - IfNull(D.AssetQty, 0.00) ");
            SQL.AppendLine(") As Qty, ");
            SQL.AppendLine("C.InventoryUomCode As Uom, A.CurCode, ");
            if (mFrmParent.mIsAssetRecvQtyEditable) SQL.AppendLine("B.UPrice As Amt, ");
            else SQL.AppendLine("((B.UPrice*B.QtyPurchase)-(B.Discount*0.01*(B.UPrice*B.QtyPurchase))+B.RoundingValue) As Amt, ");
            SQL.AppendLine("A.POInd, null As LocalDocNo ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            if (mFrmParent.mIsAssetRecvQtyEditable)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T2.RecvVdDocNo, T2.RecvVdDNo, Sum(T2.Qty) AssetQty ");
                SQL.AppendLine("    From TblAsset T1 ");
                SQL.AppendLine("    Inner Join TblAssetDtl T2 On T1.AssetCode = T2.AssetCode ");
                SQL.AppendLine("        And T1.ActiveInd = 'Y' ");
                SQL.AppendLine("    Group BY T2.RecvVdDocNo, T2.RecvVdDNo ");
                SQL.AppendLine(") D On B.DocNo = D.RecvVdDocNo And B.DNo = D.RecvVdDNo ");
            }
            SQL.AppendLine("Where B.CancelInd = 'N' ");
            SQL.AppendLine("And B.Status = 'A' ");
            SQL.AppendLine("And C.FixedItemInd = 'Y' ");
            SQL.AppendLine("And A.POInd = 'N' ");
            if (!mFrmParent.mIsAssetRecvQtyEditable)
                SQL.AppendLine("And B.AssetCode Is Null ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And Locate(Concat('##', A.DocNo, '*', B.DNo, '##'), @SelectedRecvVd)<1 ");

            SQL.AppendLine(")T ");
            SQL.AppendLine("INNER JOIN tblstockprice U ON T.Source = U.Source AND T.ItCode = U.ItCode AND T.CurCode = U.CurCode ");

            //if (mFrmParent.mIsAssetRecvQtyEditable)
            //{
            //    SQL.AppendLine("LEFT JOIN ( ");
            //    SQL.AppendLine("     Select B.RecvVdDocno, B.RecvVdDno, SUM(B.Qty) Qty from tblAsset A ");
            //    SQL.AppendLine("     INNER JOIN tblAssetDtl B ON A.AssetCode = B.AssetCode ");
            //    SQL.AppendLine("     AND A.ActiveInd = 'Y' ");
            //    SQL.AppendLine("     GROUP BY B.RecvVdDocno, B.RecvVdDno ");
            //    SQL.AppendLine(") U ON T.DocNo = U.RecvVdDocno AND T.Dno = U.RecvVdDno ");
            //}


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (
                   Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                   Sm.IsDteEmpty(DteDocDt2, "End date") ||
                   Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                   ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";
                if(mFrmParent.mIsAssetRecvQtyEditable)
                    Filter += " AND T.QTY > 0.00 "; 
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SelectedRecvVd", mFrmParent.GetSelectedRecvVd());

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName", "T.ItCodeInternal", "T.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DNo", "DocDt", "ItCode", "ItName", 
                        
                        //6-10
                        "Qty", "Uom", "CurCode", "Amt", "POInd",

                        //11-12
                        "ExcRate", "AmtIDR"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !IsRecvVdAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 15);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 11 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 receiving item.");
             
                mFrmParent.ComputeRate();
        }

        private bool IsRecvVdAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 0), Sm.GetGrdStr(Grd1, Row, 5))
                    )
                    return true;
            }
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if(Sm.GetGrdStr(Grd1, e.RowIndex, 13) == "Y")
                {
                    var f = new FrmRecvVd(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmRecvVd' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 13) == "N")
                {
                    var f = new FrmRecvVd2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmRecvVd2' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 13) == "Y")
                {
                    e.DoDefault = false;
                    var f = new FrmRecvVd(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmRecvVd' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 13) == "N")
                {
                    e.DoDefault = false;
                    var f = new FrmRecvVd2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmRecvVd2' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Receiving#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
