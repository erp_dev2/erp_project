﻿namespace RunSystem
{
    partial class FrmPWG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUomCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueWagesFormulationCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Tab1 = new DevExpress.XtraTab.XtraTabControl();
            this.TP1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.TPWCSFC = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TPItem = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.TxtOutstanding = new DevExpress.XtraEditors.TextEdit();
            this.TxtBalance = new DevExpress.XtraEditors.TextEdit();
            this.TxtProcessed = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtOutstanding2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtBalance2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtProcessed2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtIndex = new DevExpress.XtraEditors.TextEdit();
            this.TxtValue = new DevExpress.XtraEditors.TextEdit();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.LueWorkCenterDocNo = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueProductionShiftCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtHolidayWagesIndex = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtHolName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TP3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.ChkCalcPerPerson = new DevExpress.XtraEditors.CheckEdit();
            this.TP2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.TxtEmpCount = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWagesFormulationCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tab1)).BeginInit();
            this.Tab1.SuspendLayout();
            this.TP1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.TPWCSFC.SuspendLayout();
            this.panel5.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TPItem.SuspendLayout();
            this.panel6.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel10.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstanding.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcessed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstanding2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcessed2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkCenterDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionShiftCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHolidayWagesIndex.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHolName.Properties)).BeginInit();
            this.TP3.SuspendLayout();
            this.panel11.SuspendLayout();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCalcPerPerson.Properties)).BeginInit();
            this.TP2.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tab1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 31);
            this.panel3.TabIndex = 10;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(252, 4);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 13;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(83, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 71);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 33;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(65, 68);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(700, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(307, 20);
            this.MeeRemark.TabIndex = 34;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(86, 5);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 17;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            this.DteDocDt.Validated += new System.EventHandler(this.DteDocDt_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(49, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUomCode
            // 
            this.TxtUomCode.EnterMoveNextControl = true;
            this.TxtUomCode.Location = new System.Drawing.Point(65, 26);
            this.TxtUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUomCode.Name = "TxtUomCode";
            this.TxtUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUomCode.Properties.MaxLength = 40;
            this.TxtUomCode.Properties.ReadOnly = true;
            this.TxtUomCode.Size = new System.Drawing.Size(307, 20);
            this.TxtUomCode.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(29, 29);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "UoM";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWagesFormulationCode
            // 
            this.LueWagesFormulationCode.EnterMoveNextControl = true;
            this.LueWagesFormulationCode.Location = new System.Drawing.Point(65, 5);
            this.LueWagesFormulationCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWagesFormulationCode.Name = "LueWagesFormulationCode";
            this.LueWagesFormulationCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.Appearance.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWagesFormulationCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWagesFormulationCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWagesFormulationCode.Properties.DropDownRows = 25;
            this.LueWagesFormulationCode.Properties.NullText = "[Empty]";
            this.LueWagesFormulationCode.Properties.PopupWidth = 222;
            this.LueWagesFormulationCode.Size = new System.Drawing.Size(307, 20);
            this.LueWagesFormulationCode.TabIndex = 28;
            this.LueWagesFormulationCode.ToolTip = "F4 : Show/hide list";
            this.LueWagesFormulationCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWagesFormulationCode.EditValueChanged += new System.EventHandler(this.LueWagesFormulationCode_EditValueChanged);
            this.LueWagesFormulationCode.Validated += new System.EventHandler(this.LueWagesFormulationCode_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(12, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 14);
            this.label4.TabIndex = 27;
            this.label4.Text = "Formula";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(6, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 31;
            this.label5.Text = "Currency";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(65, 47);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 20;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 222;
            this.LueCurCode.Size = new System.Drawing.Size(199, 20);
            this.LueCurCode.TabIndex = 32;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // Tab1
            // 
            this.Tab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tab1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tab1.Location = new System.Drawing.Point(0, 31);
            this.Tab1.Name = "Tab1";
            this.Tab1.SelectedTabPage = this.TP1;
            this.Tab1.Size = new System.Drawing.Size(772, 442);
            this.Tab1.TabIndex = 14;
            this.Tab1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TP1,
            this.TP3,
            this.TP2});
            // 
            // TP1
            // 
            this.TP1.Appearance.Header.Options.UseTextOptions = true;
            this.TP1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TP1.Controls.Add(this.xtraTabControl2);
            this.TP1.Controls.Add(this.panel10);
            this.TP1.Controls.Add(this.panel9);
            this.TP1.Name = "TP1";
            this.TP1.Size = new System.Drawing.Size(766, 414);
            this.TP1.Text = "Work Center and SFC";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 94);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.TPWCSFC;
            this.xtraTabControl2.Size = new System.Drawing.Size(766, 243);
            this.xtraTabControl2.TabIndex = 35;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TPWCSFC,
            this.TPItem});
            // 
            // TPWCSFC
            // 
            this.TPWCSFC.Appearance.Header.Options.UseTextOptions = true;
            this.TPWCSFC.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TPWCSFC.Controls.Add(this.panel5);
            this.TPWCSFC.Name = "TPWCSFC";
            this.TPWCSFC.Size = new System.Drawing.Size(760, 215);
            this.TPWCSFC.Text = "Work Center\'s SFC";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.splitContainer1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(760, 215);
            this.panel5.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Grd2);
            this.splitContainer1.Size = new System.Drawing.Size(760, 215);
            this.splitContainer1.SplitterDistance = 68;
            this.splitContainer1.TabIndex = 0;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(760, 68);
            this.Grd1.TabIndex = 36;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(760, 143);
            this.Grd2.TabIndex = 37;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TPItem
            // 
            this.TPItem.Appearance.Header.Options.UseTextOptions = true;
            this.TPItem.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TPItem.Controls.Add(this.panel6);
            this.TPItem.Name = "TPItem";
            this.TPItem.Size = new System.Drawing.Size(760, 0);
            this.TPItem.Text = "Item";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.splitContainer2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(760, 0);
            this.panel6.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Grd3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.Grd4);
            this.splitContainer2.Size = new System.Drawing.Size(760, 0);
            this.splitContainer2.SplitterDistance = 99;
            this.splitContainer2.TabIndex = 0;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(760, 99);
            this.Grd3.TabIndex = 37;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(760, 112);
            this.Grd4.TabIndex = 38;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.AliceBlue;
            this.panel10.Controls.Add(this.splitContainer3);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 337);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(766, 77);
            this.panel10.TabIndex = 34;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContainer3.Location = new System.Drawing.Point(0, 3);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.TxtIndex);
            this.splitContainer3.Panel2.Controls.Add(this.TxtValue);
            this.splitContainer3.Panel2.Controls.Add(this.TxtAmt);
            this.splitContainer3.Panel2.Controls.Add(this.label23);
            this.splitContainer3.Panel2.Controls.Add(this.label21);
            this.splitContainer3.Panel2.Controls.Add(this.label20);
            this.splitContainer3.Size = new System.Drawing.Size(766, 74);
            this.splitContainer3.SplitterDistance = 495;
            this.splitContainer3.TabIndex = 35;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.TxtOutstanding);
            this.splitContainer4.Panel1.Controls.Add(this.TxtBalance);
            this.splitContainer4.Panel1.Controls.Add(this.TxtProcessed);
            this.splitContainer4.Panel1.Controls.Add(this.label22);
            this.splitContainer4.Panel1.Controls.Add(this.label8);
            this.splitContainer4.Panel1.Controls.Add(this.label11);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.TxtOutstanding2);
            this.splitContainer4.Panel2.Controls.Add(this.TxtBalance2);
            this.splitContainer4.Panel2.Controls.Add(this.TxtProcessed2);
            this.splitContainer4.Panel2.Controls.Add(this.label13);
            this.splitContainer4.Panel2.Controls.Add(this.label14);
            this.splitContainer4.Panel2.Controls.Add(this.label15);
            this.splitContainer4.Size = new System.Drawing.Size(495, 74);
            this.splitContainer4.SplitterDistance = 236;
            this.splitContainer4.TabIndex = 0;
            // 
            // TxtOutstanding
            // 
            this.TxtOutstanding.EnterMoveNextControl = true;
            this.TxtOutstanding.Location = new System.Drawing.Point(83, 6);
            this.TxtOutstanding.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstanding.Name = "TxtOutstanding";
            this.TxtOutstanding.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOutstanding.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstanding.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstanding.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstanding.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstanding.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstanding.Properties.ReadOnly = true;
            this.TxtOutstanding.Size = new System.Drawing.Size(146, 20);
            this.TxtOutstanding.TabIndex = 39;
            // 
            // TxtBalance
            // 
            this.TxtBalance.EnterMoveNextControl = true;
            this.TxtBalance.Location = new System.Drawing.Point(83, 50);
            this.TxtBalance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalance.Name = "TxtBalance";
            this.TxtBalance.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalance.Properties.Appearance.Options.UseFont = true;
            this.TxtBalance.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalance.Properties.ReadOnly = true;
            this.TxtBalance.Size = new System.Drawing.Size(146, 20);
            this.TxtBalance.TabIndex = 43;
            // 
            // TxtProcessed
            // 
            this.TxtProcessed.EnterMoveNextControl = true;
            this.TxtProcessed.Location = new System.Drawing.Point(83, 28);
            this.TxtProcessed.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProcessed.Name = "TxtProcessed";
            this.TxtProcessed.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProcessed.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProcessed.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProcessed.Properties.Appearance.Options.UseFont = true;
            this.TxtProcessed.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtProcessed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtProcessed.Properties.ReadOnly = true;
            this.TxtProcessed.Size = new System.Drawing.Size(146, 20);
            this.TxtProcessed.TabIndex = 41;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(5, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 14);
            this.label22.TabIndex = 38;
            this.label22.Text = "Outstanding";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "Processed";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(31, 53);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 14);
            this.label11.TabIndex = 42;
            this.label11.Text = "Balance";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOutstanding2
            // 
            this.TxtOutstanding2.EnterMoveNextControl = true;
            this.TxtOutstanding2.Location = new System.Drawing.Point(97, 6);
            this.TxtOutstanding2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstanding2.Name = "TxtOutstanding2";
            this.TxtOutstanding2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOutstanding2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstanding2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstanding2.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstanding2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstanding2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstanding2.Properties.ReadOnly = true;
            this.TxtOutstanding2.Size = new System.Drawing.Size(146, 20);
            this.TxtOutstanding2.TabIndex = 45;
            // 
            // TxtBalance2
            // 
            this.TxtBalance2.EnterMoveNextControl = true;
            this.TxtBalance2.Location = new System.Drawing.Point(97, 50);
            this.TxtBalance2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalance2.Name = "TxtBalance2";
            this.TxtBalance2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalance2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalance2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalance2.Properties.Appearance.Options.UseFont = true;
            this.TxtBalance2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalance2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalance2.Properties.ReadOnly = true;
            this.TxtBalance2.Size = new System.Drawing.Size(146, 20);
            this.TxtBalance2.TabIndex = 49;
            // 
            // TxtProcessed2
            // 
            this.TxtProcessed2.EnterMoveNextControl = true;
            this.TxtProcessed2.Location = new System.Drawing.Point(97, 28);
            this.TxtProcessed2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProcessed2.Name = "TxtProcessed2";
            this.TxtProcessed2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProcessed2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProcessed2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProcessed2.Properties.Appearance.Options.UseFont = true;
            this.TxtProcessed2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtProcessed2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtProcessed2.Properties.ReadOnly = true;
            this.TxtProcessed2.Size = new System.Drawing.Size(146, 20);
            this.TxtProcessed2.TabIndex = 47;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(8, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 14);
            this.label13.TabIndex = 44;
            this.label13.Text = "Outstanding 2";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(20, 31);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 14);
            this.label14.TabIndex = 46;
            this.label14.Text = "Processed 2";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(34, 53);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 14);
            this.label15.TabIndex = 48;
            this.label15.Text = "Balance 2";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIndex
            // 
            this.TxtIndex.EnterMoveNextControl = true;
            this.TxtIndex.Location = new System.Drawing.Point(67, 6);
            this.TxtIndex.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndex.Name = "TxtIndex";
            this.TxtIndex.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIndex.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndex.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndex.Properties.Appearance.Options.UseFont = true;
            this.TxtIndex.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtIndex.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtIndex.Properties.ReadOnly = true;
            this.TxtIndex.Size = new System.Drawing.Size(192, 20);
            this.TxtIndex.TabIndex = 51;
            // 
            // TxtValue
            // 
            this.TxtValue.EnterMoveNextControl = true;
            this.TxtValue.Location = new System.Drawing.Point(67, 28);
            this.TxtValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtValue.Name = "TxtValue";
            this.TxtValue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtValue.Properties.Appearance.Options.UseFont = true;
            this.TxtValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtValue.Properties.ReadOnly = true;
            this.TxtValue.Size = new System.Drawing.Size(192, 20);
            this.TxtValue.TabIndex = 54;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(67, 50);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(192, 20);
            this.TxtAmt.TabIndex = 56;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(8, 53);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(51, 14);
            this.label23.TabIndex = 55;
            this.label23.Text = "Amount";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(22, 30);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(37, 14);
            this.label21.TabIndex = 53;
            this.label21.Text = "Value";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(21, 7);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 14);
            this.label20.TabIndex = 50;
            this.label20.Text = "Index";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.AliceBlue;
            this.panel9.Controls.Add(this.label6);
            this.panel9.Controls.Add(this.LueWorkCenterDocNo);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.LueProductionShiftCode);
            this.panel9.Controls.Add(this.TxtHolidayWagesIndex);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.panel4);
            this.panel9.Controls.Add(this.DteDocDt);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.TxtHolName);
            this.panel9.Controls.Add(this.label3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(766, 94);
            this.panel9.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(5, 29);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "Work Center";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWorkCenterDocNo
            // 
            this.LueWorkCenterDocNo.EnterMoveNextControl = true;
            this.LueWorkCenterDocNo.Location = new System.Drawing.Point(86, 26);
            this.LueWorkCenterDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.LueWorkCenterDocNo.Name = "LueWorkCenterDocNo";
            this.LueWorkCenterDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenterDocNo.Properties.Appearance.Options.UseFont = true;
            this.LueWorkCenterDocNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenterDocNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWorkCenterDocNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenterDocNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWorkCenterDocNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenterDocNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWorkCenterDocNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkCenterDocNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWorkCenterDocNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWorkCenterDocNo.Properties.DropDownRows = 25;
            this.LueWorkCenterDocNo.Properties.NullText = "[Empty]";
            this.LueWorkCenterDocNo.Properties.PopupWidth = 222;
            this.LueWorkCenterDocNo.Size = new System.Drawing.Size(297, 20);
            this.LueWorkCenterDocNo.TabIndex = 19;
            this.LueWorkCenterDocNo.ToolTip = "F4 : Show/hide list";
            this.LueWorkCenterDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWorkCenterDocNo.EditValueChanged += new System.EventHandler(this.LueWorkCenterDocNo_EditValueChanged);
            this.LueWorkCenterDocNo.Validated += new System.EventHandler(this.LueWorkCenterDocNo_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(50, 50);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 14);
            this.label12.TabIndex = 20;
            this.label12.Text = "Shift";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProductionShiftCode
            // 
            this.LueProductionShiftCode.EnterMoveNextControl = true;
            this.LueProductionShiftCode.Location = new System.Drawing.Point(86, 47);
            this.LueProductionShiftCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionShiftCode.Name = "LueProductionShiftCode";
            this.LueProductionShiftCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.Appearance.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionShiftCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionShiftCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionShiftCode.Properties.DropDownRows = 12;
            this.LueProductionShiftCode.Properties.NullText = "[Empty]";
            this.LueProductionShiftCode.Properties.PopupWidth = 500;
            this.LueProductionShiftCode.Size = new System.Drawing.Size(297, 20);
            this.LueProductionShiftCode.TabIndex = 21;
            this.LueProductionShiftCode.ToolTip = "F4 : Show/hide list";
            this.LueProductionShiftCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionShiftCode.EditValueChanged += new System.EventHandler(this.LueProductionShiftCode_EditValueChanged);
            this.LueProductionShiftCode.Validated += new System.EventHandler(this.LueProductionShiftCode_Validated);
            // 
            // TxtHolidayWagesIndex
            // 
            this.TxtHolidayWagesIndex.EnterMoveNextControl = true;
            this.TxtHolidayWagesIndex.Location = new System.Drawing.Point(344, 68);
            this.TxtHolidayWagesIndex.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHolidayWagesIndex.Name = "TxtHolidayWagesIndex";
            this.TxtHolidayWagesIndex.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtHolidayWagesIndex.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHolidayWagesIndex.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHolidayWagesIndex.Properties.Appearance.Options.UseFont = true;
            this.TxtHolidayWagesIndex.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHolidayWagesIndex.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHolidayWagesIndex.Properties.ReadOnly = true;
            this.TxtHolidayWagesIndex.Size = new System.Drawing.Size(39, 20);
            this.TxtHolidayWagesIndex.TabIndex = 25;
            this.TxtHolidayWagesIndex.Tag = "Holiday\'s Index";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(302, 71);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 14);
            this.label16.TabIndex = 24;
            this.label16.Tag = "Holiday\'s Index";
            this.label16.Text = "Index";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.LueWagesFormulationCode);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.LueCurCode);
            this.panel4.Controls.Add(this.TxtUomCode);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(388, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(378, 94);
            this.panel4.TabIndex = 26;
            // 
            // TxtHolName
            // 
            this.TxtHolName.EnterMoveNextControl = true;
            this.TxtHolName.Location = new System.Drawing.Point(86, 68);
            this.TxtHolName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHolName.Name = "TxtHolName";
            this.TxtHolName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtHolName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHolName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHolName.Properties.Appearance.Options.UseFont = true;
            this.TxtHolName.Properties.MaxLength = 40;
            this.TxtHolName.Properties.ReadOnly = true;
            this.TxtHolName.Size = new System.Drawing.Size(213, 20);
            this.TxtHolName.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(37, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Holiday";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TP3
            // 
            this.TP3.Appearance.Header.Options.UseTextOptions = true;
            this.TP3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TP3.Controls.Add(this.panel11);
            this.TP3.Name = "TP3";
            this.TP3.Size = new System.Drawing.Size(766, 174);
            this.TP3.Text = "Wages Calculated Per Person";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.splitContainer6);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(766, 174);
            this.panel11.TabIndex = 0;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 33);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.Grd7);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.Grd8);
            this.splitContainer6.Size = new System.Drawing.Size(766, 141);
            this.splitContainer6.SplitterDistance = 104;
            this.splitContainer6.TabIndex = 21;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(766, 104);
            this.Grd7.TabIndex = 16;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd7_AfterCommitEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(766, 33);
            this.Grd8.TabIndex = 21;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.AliceBlue;
            this.panel13.Controls.Add(this.ChkCalcPerPerson);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(766, 33);
            this.panel13.TabIndex = 14;
            // 
            // ChkCalcPerPerson
            // 
            this.ChkCalcPerPerson.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCalcPerPerson.Location = new System.Drawing.Point(6, 6);
            this.ChkCalcPerPerson.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCalcPerPerson.Name = "ChkCalcPerPerson";
            this.ChkCalcPerPerson.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCalcPerPerson.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCalcPerPerson.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCalcPerPerson.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCalcPerPerson.Properties.Appearance.Options.UseFont = true;
            this.ChkCalcPerPerson.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCalcPerPerson.Properties.Caption = "Calculated Per Person";
            this.ChkCalcPerPerson.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCalcPerPerson.Size = new System.Drawing.Size(151, 22);
            this.ChkCalcPerPerson.TabIndex = 15;
            this.ChkCalcPerPerson.CheckedChanged += new System.EventHandler(this.ChkCalcPerPerson_CheckedChanged);
            // 
            // TP2
            // 
            this.TP2.Appearance.Header.Options.UseTextOptions = true;
            this.TP2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TP2.Controls.Add(this.splitContainer5);
            this.TP2.Name = "TP2";
            this.TP2.Size = new System.Drawing.Size(766, 174);
            this.TP2.Text = "Direct labor";
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.Grd5);
            this.splitContainer5.Panel1.Controls.Add(this.textEdit1);
            this.splitContainer5.Panel1.Controls.Add(this.panel14);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.Grd6);
            this.splitContainer5.Panel2.Controls.Add(this.textEdit2);
            this.splitContainer5.Size = new System.Drawing.Size(766, 174);
            this.splitContainer5.SplitterDistance = 117;
            this.splitContainer5.TabIndex = 18;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 30);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 57);
            this.Grd5.TabIndex = 15;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            // 
            // textEdit1
            // 
            this.textEdit1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textEdit1.EditValue = "Non-Coordinator";
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(0, 0);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.PowderBlue;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.textEdit1.Properties.MaxLength = 30;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(766, 30);
            this.textEdit1.TabIndex = 14;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.AliceBlue;
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(0, 87);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(766, 30);
            this.panel14.TabIndex = 16;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.TxtEmpCount);
            this.panel15.Controls.Add(this.label26);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel15.Location = new System.Drawing.Point(558, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(208, 30);
            this.panel15.TabIndex = 17;
            // 
            // TxtEmpCount
            // 
            this.TxtEmpCount.EnterMoveNextControl = true;
            this.TxtEmpCount.Location = new System.Drawing.Point(144, 5);
            this.TxtEmpCount.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCount.Name = "TxtEmpCount";
            this.TxtEmpCount.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCount.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCount.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCount.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmpCount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmpCount.Properties.ReadOnly = true;
            this.TxtEmpCount.Size = new System.Drawing.Size(57, 20);
            this.TxtEmpCount.TabIndex = 19;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(9, 8);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(127, 14);
            this.label26.TabIndex = 18;
            this.label26.Text = "Number of Employees";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 30);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 23);
            this.Grd6.TabIndex = 21;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            // 
            // textEdit2
            // 
            this.textEdit2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textEdit2.EditValue = "Coordinator";
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(0, 0);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.PowderBlue;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.textEdit2.Properties.MaxLength = 30;
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(766, 30);
            this.textEdit2.TabIndex = 20;
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmPWG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmPWG";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWagesFormulationCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tab1)).EndInit();
            this.Tab1.ResumeLayout(false);
            this.TP1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.TPWCSFC.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TPItem.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel10.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstanding.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcessed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstanding2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcessed2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkCenterDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionShiftCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHolidayWagesIndex.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHolName.Properties)).EndInit();
            this.TP3.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkCalcPerPerson.Properties)).EndInit();
            this.TP2.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl Tab1;
        private DevExpress.XtraTab.XtraTabPage TP2;
        internal DevExpress.XtraEditors.TextEdit TxtUomCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        public DevExpress.XtraEditors.LookUpEdit LueWagesFormulationCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraTab.XtraTabPage TP1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtHolName;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtHolidayWagesIndex;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraTab.XtraTabPage TP3;
        private System.Windows.Forms.Panel panel11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage TPWCSFC;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage TPItem;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.SplitContainer splitContainer2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        internal DevExpress.XtraEditors.TextEdit TxtOutstanding;
        internal DevExpress.XtraEditors.TextEdit TxtBalance;
        internal DevExpress.XtraEditors.TextEdit TxtProcessed;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtOutstanding2;
        internal DevExpress.XtraEditors.TextEdit TxtBalance2;
        internal DevExpress.XtraEditors.TextEdit TxtProcessed2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtIndex;
        internal DevExpress.XtraEditors.TextEdit TxtValue;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel13;
        private DevExpress.XtraEditors.CheckEdit ChkCalcPerPerson;
        private System.Windows.Forms.Panel panel14;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCount;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.SplitContainer splitContainer5;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.SplitContainer splitContainer6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueProductionShiftCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueWorkCenterDocNo;
    }
}