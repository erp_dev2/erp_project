﻿#region Update
/*
    12/03/2020 [IBL/SRN] Tambah combo box untuk memilih cost center group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAllowanceDeduction : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mADCode = string.Empty;
        internal FrmAllowanceDeductionFind FrmFind;

        #endregion

        #region Constructor

        public FrmAllowanceDeduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Allowance/Deduction";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);

                Sl.SetLueADAmtType(ref LueAmtType);
                Sl.SetLueADType(ref LueADType);
                Sl.SetLueOption(ref LueCostCenterGroup, "CostCenterGroup");

                //if this application is called from other application
                if (mADCode.Length != 0)
                {
                    ShowData(mADCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtADCode, TxtADName, LueADType, LueCostCenterGroup, LueAmtType, MeeRemark,
                        ChkTaxInd
                    }, true);
                    TxtADCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtADCode, TxtADName, LueADType, LueCostCenterGroup, LueAmtType, MeeRemark,
                        ChkTaxInd
                    }, false);
                    TxtADCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtADName, LueADType, LueAmtType, LueCostCenterGroup, MeeRemark, ChkTaxInd
                    }, false);
                    TxtADName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtADCode, TxtADName, LueADType, LueCostCenterGroup, LueAmtType, MeeRemark
            });
            ChkTaxInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAllowanceDeductionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtADCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtADCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAllowanceDeduction Where ADCode=@ADCode;" };
                Sm.CmParam<String>(ref cm, "@ADCode", TxtADCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblAllowanceDeduction(ADCode, ADName, ADType, CCGrpCode, AmtType, TaxInd, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ADCode, @ADName, @ADType, @CCGrpCode, @AmtType, @TaxInd, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ADName=@ADName, ADType=@ADType, CCGrpCode=@CCGrpCode, AmtType=@AmtType, TaxInd=@TaxInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ADCode", TxtADCode.Text);
                Sm.CmParam<String>(ref cm, "@ADName", TxtADName.Text);
                Sm.CmParam<String>(ref cm, "@ADType", Sm.GetLue(LueADType));
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCostCenterGroup));
                Sm.CmParam<String>(ref cm, "@AmtType", Sm.GetLue(LueAmtType));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@TaxInd", ChkTaxInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtADCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ADCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ADCode", ADCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select ADCode, ADName, ADType, CCGrpCode, AmtType, Remark, TaxInd " +
                        "From TblAllowanceDeduction Where ADCode=@ADCode;",
                        new string[] 
                        {
                            //0
                            "ADCode", 
                            
                            //1-5
                            "ADName", "ADType", "CCGrpCode", "AmtType", "Remark", "TaxInd" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtADCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtADName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueADType, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueCostCenterGroup, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueAmtType, Sm.DrStr(dr, c[4]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                            ChkTaxInd.Checked = Sm.DrStr(dr, c[6]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtADName, "Allowance/Deduction code", false) ||
                Sm.IsTxtEmpty(TxtADName, "Allowance/Deduction name", false) ||
                Sm.IsLueEmpty(LueADType, "Allowance/Deduction type") ||
                Sm.IsLueEmpty(LueAmtType, "Amount type") ||
                IsADCodeExisted();
        }

        private bool IsADCodeExisted()
        {
            if (!TxtADCode.Properties.ReadOnly && Sm.IsDataExist("Select ADCode From TblAllowanceDeduction Where ADCode='" + TxtADCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Allowance/Deduction code ( " + TxtADCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtADCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtADCode);
        }

        private void TxtADName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtADName);
        }

        private void LueADType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueADType, new Sm.RefreshLue1(Sl.SetLueADType));
        }

        private void LueAmtType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAmtType, new Sm.RefreshLue1(Sl.SetLueADAmtType));
        }

        private void LueCostCenterGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostCenterGroup, new Sm.RefreshLue2(Sl.SetLueOption),"CostCenterGroup");
        }

        #endregion

        #endregion

    }
}
