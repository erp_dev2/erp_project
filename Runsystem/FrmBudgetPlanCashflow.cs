﻿#region update
/*
    23/08/2021 [SET/AMKA] Membuat Menu baru Budget Plan Cashflow
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetPlanCashflow : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool IsInsert = false;
        internal FrmBudgetPlanCashflowFind FrmFind;

        #endregion

        #region Constructor

        public FrmBudgetPlanCashflow(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);
            Sl.SetLueYr(LueYr, "");
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueProfitCenterCode, LueYr, MeeCancelReason, ChkCancelInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueProfitCenterCode, LueYr
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
                    TxtDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       MeeCancelReason, ChkCancelInd
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] {  });
                    break;
                default:
                    break;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            //Grd1.FrozenArea.ColCount = 0;
            Grd1.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-2
                        "Cash Type", "RKAP"
                    },
                    new int[] 
                    {
                       //0
                       50, 
                       //1-2
                       200, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueProfitCenterCode, LueYr, MeeCancelReason
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetPlanCashflowFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            GetDataCashType();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            IsInsert = true;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            //IsInsert = false;
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region

        public void ShowData(string DocNo)
        {
            try
            {
                IsInsert = false;
                ClearData();
                ShowBudgetPlanCashflowHdr(DocNo);
                ShowBudgetPlanCashflowDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetPlanCashflowHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select DocNo, CancelInd, CancelReason, DocDt, ProfitCenterCode, Yr " +
                  "From TblBudgetPlanCashflowHdr Where DocNo=@DocNo",
                 new string[] 
                   {
                      //0
                      "DocNo",

                      //1-5
                      "CancelReason", "CancelInd", "DocDt", "ProfitCenterCode", "Yr",

                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[1]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[3]));
                     Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[4]));
                     Sm.SetLue(LueYr, Sm.DrStr(dr, c[5]));
                 }, true
             );
        }

        private void ShowBudgetPlanCashflowDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.CashTypeName , A.Amt ");
            SQL.AppendLine("From TblBudgetPlanCashflowDtl A ");
            SQL.AppendLine("Inner Join TblCashType B On A.CashTypeCode = B.CashTypeCode ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "CashTypeName", 

                           //1-2
                           "Amt",

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        //Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        

        #endregion

        #region Save data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetPlanCashflow", "TblBudgetPlanCashflowHdr");

            cml.Add(SaveBudgetPlanCashflowHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveBudgetPlanCashflowDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsBudgetPlanCashflowInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 detail.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "RKAP is empty.");
                    return true;
                }
                if (Sm.GetGrdDec(Grd1, Row, 2) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be less than 0.");
                    return true;
                }
            }
            return false;
        }

        private bool IsBudgetPlanCashflowInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblBudgetPlanCashflowHdr Where CancelInd = 'N' ");
            SQL.AppendLine("And ProfitCenterCode = '"+Sm.GetLue(LueProfitCenterCode)+"' ");
            SQL.AppendLine("And Yr = '" + Sm.GetLue(LueYr) + "' Limit 1 ");

            if (Sm.IsDataExist(SQL.ToString()))
            {
                Sm.StdMsg(mMsgType.Warning, "Profit Center and Year Already Exists.");
                return true;
            }

            return false;
        }

        private MySqlCommand SaveBudgetPlanCashflowHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetPlanCashflowHdr ");
            SQL.AppendLine("(DocNo, DocDt, ProfitCenterCode, Yr, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ProfitCenterCode, @Yr, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetPlanCashflowDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetPlanCashflowDtl ");
            SQL.AppendLine("(DocNo, DNo, CashTypeCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @CashTypeCode, @Amt, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData(string DocNo)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(EditBudgetPlanCashflowHdr(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblBudgetPlanCashflowHdr Where CancelInd = 'Y' And DocNo = @DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

        private MySqlCommand EditBudgetPlanCashflowHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetPlanCashflowHdr ");
            SQL.AppendLine("Set CancelInd=@CancelInd, CancelReason=@CancelReason, ");
            SQL.AppendLine("LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetDataCashType()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CashTypeCode, CashTypeName ");
            SQL.AppendLine("From TblCashType;");

            var cm = new MySqlCommand();

            //Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "CashTypeCode",

                           //1
                           "CashTypeName"

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion
        
    }
}
