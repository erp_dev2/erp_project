﻿namespace RunSystem
{
    partial class FrmWT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtWTName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtWTCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDailyWorkingHr = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtWeeklyWorkingHr = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtMonthlyWorkingHr = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtWeeklyWorkdays = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAnnualWorkingHr = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDailyWorkingHr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeeklyWorkingHr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMonthlyWorkingHr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeeklyWorkdays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualWorkingHr.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtAnnualWorkingHr);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtMonthlyWorkingHr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtWeeklyWorkdays);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtWeeklyWorkingHr);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtDailyWorkingHr);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtWTName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtWTCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(368, 23);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // TxtWTName
            // 
            this.TxtWTName.EnterMoveNextControl = true;
            this.TxtWTName.Location = new System.Drawing.Point(198, 47);
            this.TxtWTName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWTName.Name = "TxtWTName";
            this.TxtWTName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWTName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWTName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWTName.Properties.Appearance.Options.UseFont = true;
            this.TxtWTName.Properties.MaxLength = 40;
            this.TxtWTName.Size = new System.Drawing.Size(451, 20);
            this.TxtWTName.TabIndex = 13;
            this.TxtWTName.Validated += new System.EventHandler(this.TxtWTName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(75, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Working Time Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWTCode
            // 
            this.TxtWTCode.EnterMoveNextControl = true;
            this.TxtWTCode.Location = new System.Drawing.Point(198, 25);
            this.TxtWTCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWTCode.Name = "TxtWTCode";
            this.TxtWTCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWTCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWTCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWTCode.Properties.MaxLength = 16;
            this.TxtWTCode.Size = new System.Drawing.Size(167, 20);
            this.TxtWTCode.TabIndex = 10;
            this.TxtWTCode.Validated += new System.EventHandler(this.TxtWTCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(78, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Working Time Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDailyWorkingHr
            // 
            this.TxtDailyWorkingHr.EnterMoveNextControl = true;
            this.TxtDailyWorkingHr.Location = new System.Drawing.Point(198, 69);
            this.TxtDailyWorkingHr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDailyWorkingHr.Name = "TxtDailyWorkingHr";
            this.TxtDailyWorkingHr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDailyWorkingHr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDailyWorkingHr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDailyWorkingHr.Properties.Appearance.Options.UseFont = true;
            this.TxtDailyWorkingHr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDailyWorkingHr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDailyWorkingHr.Size = new System.Drawing.Size(99, 20);
            this.TxtDailyWorkingHr.TabIndex = 15;
            this.TxtDailyWorkingHr.Validated += new System.EventHandler(this.TxtDailyWorkingHr_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(83, 73);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 14);
            this.label15.TabIndex = 14;
            this.label15.Text = "Daily Working Hour";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWeeklyWorkingHr
            // 
            this.TxtWeeklyWorkingHr.EnterMoveNextControl = true;
            this.TxtWeeklyWorkingHr.Location = new System.Drawing.Point(198, 91);
            this.TxtWeeklyWorkingHr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWeeklyWorkingHr.Name = "TxtWeeklyWorkingHr";
            this.TxtWeeklyWorkingHr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWeeklyWorkingHr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeeklyWorkingHr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWeeklyWorkingHr.Properties.Appearance.Options.UseFont = true;
            this.TxtWeeklyWorkingHr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWeeklyWorkingHr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWeeklyWorkingHr.Size = new System.Drawing.Size(99, 20);
            this.TxtWeeklyWorkingHr.TabIndex = 17;
            this.TxtWeeklyWorkingHr.Validated += new System.EventHandler(this.TxtWeeklyWorkingHr_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(67, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Weekly Working Hour";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMonthlyWorkingHr
            // 
            this.TxtMonthlyWorkingHr.EnterMoveNextControl = true;
            this.TxtMonthlyWorkingHr.Location = new System.Drawing.Point(198, 135);
            this.TxtMonthlyWorkingHr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMonthlyWorkingHr.Name = "TxtMonthlyWorkingHr";
            this.TxtMonthlyWorkingHr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMonthlyWorkingHr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMonthlyWorkingHr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMonthlyWorkingHr.Properties.Appearance.Options.UseFont = true;
            this.TxtMonthlyWorkingHr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMonthlyWorkingHr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMonthlyWorkingHr.Size = new System.Drawing.Size(99, 20);
            this.TxtMonthlyWorkingHr.TabIndex = 21;
            this.TxtMonthlyWorkingHr.Validated += new System.EventHandler(this.TxtMonthlyWorkingHr_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(64, 139);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 14);
            this.label4.TabIndex = 20;
            this.label4.Text = "Monthly Working Hour";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWeeklyWorkdays
            // 
            this.TxtWeeklyWorkdays.EnterMoveNextControl = true;
            this.TxtWeeklyWorkdays.Location = new System.Drawing.Point(198, 113);
            this.TxtWeeklyWorkdays.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWeeklyWorkdays.Name = "TxtWeeklyWorkdays";
            this.TxtWeeklyWorkdays.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWeeklyWorkdays.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWeeklyWorkdays.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWeeklyWorkdays.Properties.Appearance.Options.UseFont = true;
            this.TxtWeeklyWorkdays.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWeeklyWorkdays.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWeeklyWorkdays.Size = new System.Drawing.Size(99, 20);
            this.TxtWeeklyWorkdays.TabIndex = 19;
            this.TxtWeeklyWorkdays.Validated += new System.EventHandler(this.TxtWeeklyWorkdays_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(89, 117);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 14);
            this.label5.TabIndex = 18;
            this.label5.Text = "Weekly Workdays";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAnnualWorkingHr
            // 
            this.TxtAnnualWorkingHr.EnterMoveNextControl = true;
            this.TxtAnnualWorkingHr.Location = new System.Drawing.Point(198, 157);
            this.TxtAnnualWorkingHr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAnnualWorkingHr.Name = "TxtAnnualWorkingHr";
            this.TxtAnnualWorkingHr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAnnualWorkingHr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAnnualWorkingHr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAnnualWorkingHr.Properties.Appearance.Options.UseFont = true;
            this.TxtAnnualWorkingHr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAnnualWorkingHr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAnnualWorkingHr.Size = new System.Drawing.Size(99, 20);
            this.TxtAnnualWorkingHr.TabIndex = 23;
            this.TxtAnnualWorkingHr.Validated += new System.EventHandler(this.TxtAnnualWorkingHr_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(70, 161);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 14);
            this.label7.TabIndex = 22;
            this.label7.Text = "Annual Working Hour";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmWT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmWT";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDailyWorkingHr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeeklyWorkingHr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMonthlyWorkingHr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWeeklyWorkdays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualWorkingHr.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.TextEdit TxtWTName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtWTCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtAnnualWorkingHr;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtMonthlyWorkingHr;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtWeeklyWorkdays;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtWeeklyWorkingHr;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtDailyWorkingHr;
        private System.Windows.Forms.Label label15;
    }
}