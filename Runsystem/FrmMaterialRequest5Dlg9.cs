﻿#region Update
/*
  20/07/2022 [DITA/SIER] New Apps
  22/07/2022 [DITA/SIER] tambah param : BudgetCategoryCodeForCrossDepartment  untuk menampung budget category yg bisa dipilih
  05/08/2022 [DITA/SIER]  jika department for budget yg dipilih merupakan department itu sendiri maka budgetcategory si department itu akan otomatis muncul tanpa di set diparameter
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest5Dlg9 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest5 mFrmParent;
        private string mYr = string.Empty, mMth=string.Empty, mDeptCode = string.Empty, mSQL = string.Empty, mDeptCode2 = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest5Dlg9(FrmMaterialRequest5 FrmParent, string Yr, string Mth, string DeptCode, string DeptCode2)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDeptCode = DeptCode;
            mDeptCode2 = DeptCode2;
            mYr = Yr;
            mMth = Mth;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Department",
                        "Budget Category",
                        "Local Code#",
                        "Total Budget",
                        "Used",

                        //6-7
                        "After Used",
                        "BCCode"
                        
                    },
                     new int[]
                    {
                        //0
                        50,
 
                        //1-5
                        150, 250, 120, 130, 130,
                        
                        //6-7
                        130,0
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter, string Filter2, string mBCCode)
        {
            var SQL = new StringBuilder();

            if (mFrmParent.mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("Select B.DeptName, C.BCCode, C.BCName, C.LocalCode, ");
                SQL.AppendLine("A.Amt2 Budget,  ");
                SQL.AppendLine(Sm.SelectUsedBudget());
                SQL.AppendLine("As Used, ifnull(A.Amt2, 0.00)-"+ Sm.SelectUsedBudget() + " AfterUsed ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2 ");
                SQL.AppendLine("    From TblBudgetSummary A ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode And C.ActInd = 'Y' ");
                SQL.AppendLine("And Find_In_Set(C.BCCode,@DeptBCCode) ");
                SQL.AppendLine(Filter2);
                SQL.AppendLine(Sm.ComputeUsedBudget(2, mDeptCode2, mYr, mMth, mBCCode));
                //SQL.AppendLine("Group By DeptName, BCCode, BCName, LocalCode ");
                SQL.AppendLine("Order By A.Yr, B.DeptName, C.BCName; ");
            }
            else
            {
                SQL.AppendLine("Select B.DeptName, C.BCCode, C.BCName, C.LocalCode, ");
                SQL.AppendLine("A.Amt2 Budget, ");
                SQL.AppendLine(Sm.SelectUsedBudget());
                SQL.AppendLine("As Used, ifnull(A.Amt2, 0.00)-ifnull("+ Sm.SelectUsedBudget() + ", 0.00) AfterUsed ");
                SQL.AppendLine("From TblBudgetSummary A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode And C.ActInd = 'Y' ");
                SQL.AppendLine("And Find_In_Set(C.BCCode,@DeptBCCode) ");
                SQL.AppendLine(Filter2);
                SQL.AppendLine(Sm.ComputeUsedBudget(2, mDeptCode2, mYr, mMth, mBCCode));
                SQL.AppendLine(Filter);
                //SQL.AppendLine("Group By DeptName, BCCode, BCName, LocalCode ");
                SQL.AppendLine("Order By A.Yr, A.Mth, B.DeptName, C.BCName; ");
            }



            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string
                    Filter = string.Empty, Filter2 = "And 0=0 ";
                var cm = new MySqlCommand();
                string mThisDeptBCCode = mDeptCode==mDeptCode2 ? Sm.GetValue("Select Group_Concat(BCCode) From TblBudgetCategory Where DeptCode=@Param1 And ActInd='Y' And BCCode Not In(Select Parvalue From TblParameter Where ParCode=@Param2)", mDeptCode, mFrmParent.mBudgetCategoryCodeForCrossDepartment, string.Empty) : "";
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.CmParam<String>(ref cm, "@BudgetCategoryCodeForCrossDepartment", mFrmParent.mBudgetCategoryCodeForCrossDepartment);
                Sm.CmParam<String>(ref cm, "@DeptBCCode", mThisDeptBCCode.Length > 0 ? string.Concat(mThisDeptBCCode, ',', mFrmParent.mBudgetCategoryCodeForCrossDepartment) : mFrmParent.mBudgetCategoryCodeForCrossDepartment);
                //Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                //Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);


                Sm.FilterStr(ref Filter, ref cm, mDeptCode2, "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, mYr, "A.Yr", true);
                if (!mFrmParent.mIsBudget2YearlyFormat) Sm.FilterStr(ref Filter, ref cm, mMth, "A.Mth", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtLocalDoc.Text, new string[] { "C.LocalCode" });
                Sm.FilterStr(ref Filter2, ref cm, TxtBCCode.Text, new string[] { "C.BCCode", "C.BCName" });
                string BCCode = string.Empty;
                if (TxtLocalDoc.Text.Length > 0) BCCode = Sm.GetValue("Select Group_Concat(Distinct Ifnull(BCCode, '')) From TblBudgetCategory Where LocalCode Like @Param ", string.Concat("%", TxtLocalDoc.Text, "%"));


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter, Filter2, BCCode) ,
                        new string[]
                        { 
                             //0
                             "DeptName",
                             
                             //1-5
                             "BCName",
                             "LocalCode",
                             "Budget",
                             "Used",
                             "AfterUsed",

                             //6
                             "BCCode"
                             
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                var BCCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                Sl.SetLueBCCode(ref mFrmParent.LueBCCode, BCCode, string.Empty);
                mFrmParent.ComputeRemainingBudget();
                this.Close();
            }
        }

        #endregion

        #region Grid Method
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLocalCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void TxtBCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Code#");
        }
        private void ChkBCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget Category");
        }

      

        #endregion

        #endregion

    }
}
