﻿#region Update
/*
    03/05/2021 [WED/IOK] new apps
    17/06/2021 [WED/IOK] tambah informasi nilai deposit summary
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceRawMaterial3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseInvoiceRawMaterial3 mFrmParent;
        private string mSQL = string.Empty;
        private decimal PIRawMaterialWithNPWP = 0m, PIRawMaterialWithoutNPWP = 0m;

        #endregion

        #region Constructor

        public FrmPurchaseInvoiceRawMaterial3Dlg(FrmPurchaseInvoiceRawMaterial3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                Sl.SetLueVdCode(ref LueVdCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private decimal GetPIRawMaterial(string Param)
        {
            var Value = Sm.GetParameter(Param);
            if (Value.Length==0)
                return 0m;
            else
                return decimal.Parse(Value);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.VdCode, D.VdName, B.QueueNo, ");
            SQL.AppendLine("C.CreateDt, D.TIN, D.TaxInd, E.TTName, ");
            SQL.AppendLine("Case When Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblRecvRawMaterialHdr T1, TblRecvRawMaterialDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T2.EmpType='3' ");
            SQL.AppendLine("    And T1.LegalDocVerifyDocNo=A.LegalDocVerifyDocNo Limit 1 ");
            SQL.AppendLine("    ) Then E.UnloadingCost Else 0 End As UnloadingCost, ");
            SQL.AppendLine("Cast((Select ParValue From TblParameter Where ParCode='PIRawMaterialWithNPWP') As Decimal(18, 4)) As PIRawMaterialWithNPWP, ");
            SQL.AppendLine("Cast((Select ParValue From TblParameter Where ParCode='PIRawMaterialWithoutNPWP') As Decimal(18, 4)) As PIRawMaterialWithoutNPWP, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) Downpayment ");
            SQL.AppendLine("From TblRawMaterialVerify A ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblLoadingQueue C On B.QueueNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblVendor D On B.VdCode = D.VdCode ");
            SQL.AppendLine("Left Join TblTransportType E On C.TTCode = E.TTCode ");
            SQL.AppendLine("Left Join TblVendorDepositSummary F On B.VdCode = F.VdCode And F.CurCode = @MainCurCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And IfNull(A.ProcessInd, 'O')<>'F' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "",
                        "Date",
                        "Vendor", 
                        "Queue#",

                        //6-10
                        "Queue Date",
                        "Delivery Type",
                        "Unloading Cost",
                        "Taxpayer Identification#",
                        "Tax Indicator",

                        //11-14
                        "Tax % (With NPWP)",
                        "Tax % (Without NPWP)",
                        "Downpayment",
                        "VdCode"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 10 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8, 9, 10, 11, 12, 14 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MainCurCode", mFrmParent.mMainCurCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtQueueNo.Text, "B.QueueNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "VdName", "QueueNo", "CreateDt", "TTName", 

                        //6-10
                        "UnloadingCost", "TIN", "TaxInd", "PIRawMaterialWithNPWP", "PIRawMaterialWithoutNPWP",

                        //11-12
                        "Downpayment", "VdCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.TxtRawMaterialVerifyDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.mVdCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14);
                    mFrmParent.TxtVdCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtDownpayment.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 13), 0);
                    mFrmParent.TxtQueueNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                    Sm.SetDte(mFrmParent.DteQueueDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 6));
                    mFrmParent.TxtTTCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                    mFrmParent.TxtTC.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8), 0);
                    mFrmParent.TxtTin.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                    mFrmParent.ChkTaxInd.Checked = Sm.GetGrdBool(Grd1, Grd1.CurRow.Index, 10);
                    mFrmParent.PIRawMaterialWithNPWP = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 11);
                    mFrmParent.PIRawMaterialWithoutNPWP = Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12);

                    mFrmParent.TxtTaxPercentage.EditValue = Sm.FormatNum(0m, 0);
                    
                    if (Sm.GetLue(mFrmParent.LueDocType).Length!=0)
                    {
                        if (Sm.GetLue(mFrmParent.LueDocType) == "1" && mFrmParent.ChkTaxInd.Checked)
                        {
                            if (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9).Length != 0)
                                mFrmParent.TxtTaxPercentage.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 11), 0);
                            else
                                mFrmParent.TxtTaxPercentage.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                        }

                        if (Sm.GetLue(mFrmParent.LueDocType) == "2")
                            mFrmParent.TxtTaxPercentage.EditValue = Sm.FormatNum(0m, 0);
                    }
                    mFrmParent.ShowInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRawMaterialVerify(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRawMaterialVerify(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Queue number");
        }

        #endregion

        #endregion

    }
}
