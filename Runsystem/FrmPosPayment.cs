﻿#region Update
/*
    11/09/2017 [TKG] Daftar item di Find product ditambah validasi status sudah ter-approved.
    13/10/2017 [TKG] nomor nota berdasarkan pos#
    15/05/2018 [TKG] payment type difilter berdasarkan otorisasi group
*/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;


namespace RunSystem
{
    public partial class FrmPosPayment : Form
    {
        private decimal mTotalPayment = 0;
        private string mFormat = "";
        private int CurCol = 0, CurRow = 0;
        private bool mIsPOSPaymentTypeFilterByBankAccount = false;

        internal struct PaymentType
        {
            public string PayCode { set; get; }
            public string PayName { set; get; }
            public decimal PayCharge { set; get; }
            public bool IsCash { set; get; }
        }

        public FrmPosPayment(decimal TotalPayment, string Format)
        {
            InitializeComponent();
            mFormat = Format;
            mTotalPayment = TotalPayment;
        }

        private void FrmPosPayment_Load(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                TxtTotalSales.Text = String.Format(mFormat, mTotalPayment);
                RecalculateOutstanding();
                GrdPaymentBy.Visible = false;
                Grd1.Rows.Count = 1;
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });

                Grd1.Cols[1].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[1].CellStyle.FormatString = mFormat;
                Grd1.Cols[1].CellStyle.ValueType = typeof(decimal);

                Grd1.Cols[2].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[2].CellStyle.FormatString = mFormat;
                Grd1.Cols[2].CellStyle.ValueType = typeof(decimal);
                
                Grd1.Cols[3].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[3].CellStyle.FormatString = mFormat;
                Grd1.Cols[3].CellStyle.ValueType = typeof(decimal);

                //Grd1.Cols[6].CellStyle.TextAlign = iGContentAlignment.TopRight;
                //Grd1.Cols[6].CellStyle.FormatString = "{0:#,##0.00}";
                //Grd1.Cols[6].CellStyle.ValueType = typeof(decimal);

                Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);


                GrdPaymentBy.Rows.Count = 0;


                SetPaymentList();
                CurCol = 0;
                SetFocus(Grd1, 0, CurCol, false);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsPOSPaymentTypeFilterByBankAccount = Sm.GetParameterBoo("IsPOSPaymentTypeFilterByBankAccount");
        }

        private void SetFocus(TenTec.Windows.iGridLib.iGrid TheGrid, int Row, int Col, bool IsEditable)
        {
            TheGrid.Focus();
            TheGrid.Cells[Row, Col].Selected = true;
            if (IsEditable == true)
                TheGrid.RequestEditCurCell(true);
        }

        private void FrmPosPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 13) BtnOK_Click(sender, e);
            if (e.KeyChar == 27) BtnCancel_Click(sender, e);
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if (IsDataNotValid()) return;

            decimal mChangeVal = -1 * RecalculateOutstanding();

            if (mChangeVal > 0)
            {
                decimal TotalCash = 0;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                    if (Sm.GetGrdStr(Grd1, i, 7) == "Y")
                        TotalCash += Sm.GetGrdDec(Grd1, i, 1);

                if (TotalCash < mChangeVal)
                {
                    Sm.StdMsg(mMsgType.Warning, "Unable to proceed. Non Cash Payment must not be more than total transaction");
                    return;
                }
            }

            Sm.StdMsg(mMsgType.Info, "Change : " + String.Format(mFormat, mChangeVal));

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private decimal RecalculateOutstanding()
        {
          decimal TotPaid = 0;
          decimal TotCharge = 0;

          for (int Index = 0; Index < Grd1.Rows.Count; Index++)
          {
              TotCharge = TotCharge + Sm.GetGrdDec(Grd1, Index, 2);
              TotPaid = TotPaid + Sm.GetGrdDec(Grd1, Index, 3);
          }
          TxtCharge.Text = String.Format(mFormat, TotCharge);
          TxtGrandTotal.Text = String.Format(mFormat, decimal.Parse(TxtTotalSales.Text) + TotCharge);
          TxtPayment.Text = String.Format(mFormat, TotPaid);
          TxtOutstanding.Text = String.Format(mFormat, decimal.Parse(TxtTotalSales.Text) + TotCharge - TotPaid);
          return decimal.Parse(TxtTotalSales.Text) + TotCharge - TotPaid;
        }

        private void SetPaymentList()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.PayTpNo, T.PayTpNm, T.Charge, T.IsCash ");
            SQL.AppendLine("From TblPOSPayByType T ");
            if (mIsPOSPaymentTypeFilterByBankAccount)
            {
                SQL.AppendLine("Where (T.BankAcCode Is Null Or ( ");
                SQL.AppendLine("    T.BankAcCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode=IfNull(T.BankAcCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            SQL.AppendLine("Order By T.PayTpNm;");

            try
            {
                GrdPaymentBy.Rows.Count = 0;
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("PayTpNo"),

                        //1-3
                        dr.GetOrdinal("PayTpNm"),
                        dr.GetOrdinal("Charge"),
                        dr.GetOrdinal("IsCash")
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            GrdPaymentBy.Rows.Count++;
                            GrdPaymentBy.Cells[GrdPaymentBy.Rows.Count - 1, 0].Value = Sm.DrStr(dr, c[0]);
                            GrdPaymentBy.Cells[GrdPaymentBy.Rows.Count - 1, 1].Value = Sm.DrStr(dr, c[1]);
                            GrdPaymentBy.Cells[GrdPaymentBy.Rows.Count - 1, 2].Value = Sm.DrDec(dr, c[2]);
                            GrdPaymentBy.Cells[GrdPaymentBy.Rows.Count - 1, 3].Value = Sm.DrStr(dr, c[3]);
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowPayment()
        {
            if (CurCol == 0)
                ShowPaymentList();
            else
                GrdPaymentBy.Visible = false;
        }

        private void Grd1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ShowPayment();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            CurCol = e.ColIndex;
            CurRow = e.RowIndex;
        }

        private void ShowPaymentList()
        {
            GrdPaymentBy.Visible = true;
            GrdPaymentBy.Left = 12;
            GrdPaymentBy.Top = 135 + (20 * CurRow);
            if (CurRow < GrdPaymentBy.Rows.Count)
                GrdPaymentBy.Rows[CurRow].Selected = true;
            GrdPaymentBy.Focus();
        }

        private void ChoosePaymentBy()
        {
            Grd1.Cells[CurRow, 0].Value = Sm.GetGrdStr(GrdPaymentBy, GrdPaymentBy.CurRow.Index, 1);
            Grd1.Cells[CurRow, 4].Value = "";
            Grd1.Cells[CurRow, 5].Value = Sm.GetGrdStr(GrdPaymentBy, GrdPaymentBy.CurRow.Index, 0);
            Grd1.Cells[CurRow, 6].Value = Sm.GetGrdStr(GrdPaymentBy, GrdPaymentBy.CurRow.Index, 2);
            Grd1.Cells[CurRow, 7].Value = Sm.GetGrdStr(GrdPaymentBy, GrdPaymentBy.CurRow.Index, 3);
            CurCol = 1;
            GrdPaymentBy.Visible = false;
            SetFocus(Grd1, CurRow, CurCol, true);

        }

        private void GrdPaymentBy_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) ChoosePaymentBy();
        }

        private void ClearRow(int Row)
        {
            Grd1.Cells[Row, 0].Value = "";
            Grd1.Cells[Row, 1].Value = 0;
            Grd1.Cells[Row, 2].Value = 0;
            Grd1.Cells[Row, 3].Value = 0;
            Grd1.Cells[Row, 4].Value = "";
            Grd1.Cells[Row, 5].Value = "";
            Grd1.Cells[Row, 6].Value = 0;
            Grd1.Cells[Row, 7].Value = "";
            Grd1.Cells[Row, 8].Value = 0;
            CurCol = 0;
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {

                if (Sm.GetGrdStr(Grd1, CurRow, 0) == "")
                {
                    CurCol = 0;
                    SetFocus(Grd1, CurRow, CurCol, true);
                }
                else if (Sm.GetGrdStr(Grd1, CurRow, 1) == "")
                {
                    CurCol = 1;
                    SetFocus(Grd1, CurRow, CurCol, true);
                }
                else
                {
                    Grd1.Cells[e.RowIndex, 2].Value = 0;
                    Grd1.Cells[e.RowIndex, 3].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 1);

                    // return back every thing
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                        Grd1.Cells[i, 8].Value = Sm.GetGrdDec(Grd1, i, 1);

                    decimal CheckChange = -1 * RecalculateOutstanding();

                    if (CheckChange > 0)
                    {
                        for (int i = 0; i < Grd1.Rows.Count; i++)
                        {
                            if (Sm.GetGrdStr(Grd1, i, 7) == "Y")
                                if (CheckChange < Sm.GetGrdDec(Grd1, i, 8))
                                {
                                    Grd1.Cells[i, 8].Value = Sm.GetGrdDec(Grd1, i, 8) - CheckChange;
                                    CheckChange = 0;
                                }
                                else
                                {
                                    CheckChange = CheckChange - Sm.GetGrdDec(Grd1, i, 8);
                                    Grd1.Cells[i, 8].Value = 0;
                                }
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "N")
                    {
                        CurCol = 4;

                        if (CheckChange > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Unable to proceed. Non Cash Payment must not be more than total transaction");                          
                            return;
                        }
                        else
                            if (Sm.GetGrdDec(Grd1, e.RowIndex, 6) > 0)
                            {
                                decimal ExtraCharge = (Sm.GetGrdDec(Grd1, e.RowIndex, 1) * Sm.GetGrdDec(Grd1, e.RowIndex, 6) / 100);
                                if (Sm.StdMsgYN("Question", "Extra Charge : " + String.Format(mFormat, ExtraCharge) +
                                    Environment.NewLine + "Please Swipe By " + Sm.GetGrdStr(Grd1, e.RowIndex, 0) + " : " + String.Format(mFormat, ExtraCharge + Sm.GetGrdDec(Grd1, e.RowIndex, 1))) == DialogResult.Yes)
                                {
                                    Grd1.Cells[e.RowIndex, 2].Value = ExtraCharge;
                                    Grd1.Cells[e.RowIndex, 3].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 1) + ExtraCharge;
                                }
                                else
                                    ClearRow(e.RowIndex);
                            }

                        SetFocus(Grd1, CurRow, CurCol, false);
                    }
                    else
                    {
                        if (decimal.Parse(TxtOutstanding.Text) <= 0)
                        {                           
                            // Save transaction
                            BtnOK_Click(sender, e);
                        }
                        else
                        {
                            // if last row is empty
                            if (Sm.GetGrdStr(Grd1, Grd1.Rows.Count - 1, 0) != "")
                                Grd1.Rows.Count++;

                            CurCol = 0;

                            CurRow = Grd1.Rows.Count - 1;

                            SetFocus(Grd1, CurRow, CurCol, false);
                        }
                    }
                }
            }
            if (e.ColIndex == 4)
            {
                if (decimal.Parse(TxtOutstanding.Text) <= 0)
                {
                    BtnOK_Click(sender, e);
                }
                else
                {
                    // if last row is empty
                    if (Sm.GetGrdStr(Grd1, Grd1.Rows.Count - 1, 0) != "")
                        Grd1.Rows.Count++;

                    CurCol = 0;

                    CurRow = Grd1.Rows.Count - 1;

                    SetFocus(Grd1, CurRow, CurCol, false);
                }
            }
        }

        private void Grd1_CellClick(object sender, iGCellClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                ShowPayment();
            }
        }

        private void GrdPaymentBy_DoubleClick(object sender, EventArgs e)
        {
            ChoosePaymentBy();
        }

        private bool IsDataNotValid()
        {
            decimal Outstanding = 0m;
            if (TxtOutstanding.Text.Length > 0) Outstanding = Decimal.Parse(TxtOutstanding.Text);
            if (Outstanding>0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid outstanding amount.");
                return true;
            }
            if (Sm.GetGrdStr(Grd1, 0, 0) == "")
            {
                Sm.StdMsg(mMsgType.Warning, "Please select Payment Type");
                SetFocus(Grd1, 0, 0, true);

                return true;
            }
            return false;
        }

    }
}
