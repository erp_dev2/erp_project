﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDailySFCDlg2 : RunSystem.FrmBase2
    {
        #region Field

        private FrmDailySFC mFrmParent;
        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mWorkcenterDocNo = string.Empty, mStartDt = string.Empty, mEndDt = string.Empty;


        #endregion

        #region Constructor

        public FrmDailySFCDlg2(FrmDailySFC FrmParent, string WorkcenterDocNo, string StartDt, string EndDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWorkcenterDocNo = WorkcenterDocNo;
            mStartDt = StartDt;
            mEndDt = EndDt;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                BtnExcel.Visible = BtnPrint.Visible = BtnRefresh.Visible = false;
                TxtWCDocName.Properties.ReadOnly = TxtWCDocNo.Properties.ReadOnly = true;
                SetGrd();
                SetSQL();
                TxtWCDocNo.EditValue = mWorkcenterDocNo;
                TxtWCDocName.EditValue = Sm.GetValue("Select Docname From tblworkcenterhdr Where DocNo = '"+mWorkcenterDocNo+"'");
                base.FrmLoad(sender, e);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WorkCenterDocNo, D.DocName, C.WhsName, A.DocNo As SFCDocNo, A.DocDt,  B.Qty  ");
	         SQL.AppendLine("from TblShopfloorControlHdr A   ");
	         SQL.AppendLine("Inner Join TblShopfloorControlDtl B On A.DocNo = B.DocNo   ");
	         SQL.AppendLine("Inner Join Tblwarehouse C On A.WhsCode = C.WhsCode   ");
	         SQL.AppendLine("Inner Join TblWorkcenterHdr D On  A.WorkcenterDocNo = D.DocNo   ");
	         SQL.AppendLine("Where A.WorkCenterDocNo = '"+mWorkcenterDocNo+"' And A.CancelInd = 'N'   ");
	         SQL.AppendLine("And A.DocDt between '"+mStartDt+"' And '"+mEndDt+"'   ");
             
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Workcenter",
                        "Warehouse",
                        "SFC",
                        "SFC Date",

                        //6-7
                        "",
                        "Result",
                    },
                    new int[] 
                    {
                        50,
                        150, 200, 150, 150, 100, 
                        20, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7});
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0";

                var cm = new MySqlCommand();

                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.WorkcenterDocNo;",
                        new string[]
                        { 
                            //0
                            "WorkCenterDocNo",  

                            //1-5
                            "DocName", "WhsName", "SFCDocNo", "DocDt", "Qty",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
        }


        #endregion

        #region Grid method

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        
        #endregion  

    }
}
