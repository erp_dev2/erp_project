﻿#region Update
/*
    08/09/2020 [VIN/SRN] new apps
    28/09/2020 [VIN/SRN] Bug: Employment Status selain PKWTT, PKWT, dan Honorer muncul 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpEmploymentStatus : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpEmploymentStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueDivisionCode(ref LueDivisionCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("SELECT A.SiteCode, B.DivisionCode, A.DeptCode, C.SiteName, E.DivisionName, D.DeptName, ");
            SQL.AppendLine("COUNT(A1.employmentstatus) PKWTT,  ");
            SQL.AppendLine("COUNT(A2.employmentstatus) PKWT, ");
            SQL.AppendLine("COUNT(A3.employmentstatus) Honorer,  ");
            SQL.AppendLine("COUNT(A1.employmentstatus)+COUNT(A2.employmentstatus)+COUNT(A3.employmentstatus) Total  ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT EmpCode, MAX(StartDt) StartDt ");
            SQL.AppendLine("    FROM TblEmployeePPS ");
            SQL.AppendLine("    WHERE LEFT(StartDt, 6) = CONCAT(@Yr, @Mth) ");
            SQL.AppendLine("    GROUP BY EmpCode ");
            SQL.AppendLine(") T ");

            SQL.AppendLine("INNER JOIN tblemployeepps A ON T.EmpCode = A.EmpCode AND T.StartDt = A.StartDt AND A.EmploymentStatus IN ('1','2','3') ");
            SQL.AppendLine("INNER JOIN tblemployee B ON A.EmpCode=B.EmpCode  ");
            SQL.AppendLine("LEFT JOIN  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("	SELECT empcode, StartDt, employmentstatus from tblemployeepps WHERE  EmploymentStatus='1' ");
            SQL.AppendLine("	AND SUBSTRING(StartDt, 1, 6)=CONCAT(@Yr, @Mth)  ");
            SQL.AppendLine(") A1 ON A.EmpCode=A1.empcode AND A.StartDt = A1.StartDt ");

            SQL.AppendLine("LEFT JOIN  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("	select empcode, StartDt, employmentstatus from tblemployeepps WHERE  EmploymentStatus='2'  ");
            SQL.AppendLine("	AND SUBSTRING(StartDt, 1, 6)=CONCAT(@Yr, @Mth) ");
            SQL.AppendLine(") A2 ON A.EmpCode=A2.empcode AND A.StartDt = A2.StartDt ");

            SQL.AppendLine("LEFT JOIN   ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	SELECT empcode, StartDt, employmentstatus from tblemployeepps WHERE  EmploymentStatus='3'  ");
            SQL.AppendLine("	AND SUBSTRING(StartDt, 1, 6)=CONCAT(@Yr, @Mth)  ");
            SQL.AppendLine(") A3 ON A.EmpCode=A3.empcode AND A.StartDt = A3.StartDt ");

            SQL.AppendLine("LEFT JOIN tblsite C ON B.SiteCode=C.SiteCode  ");
            SQL.AppendLine("LEFT JOIN tbldepartment D ON A.DeptCode=D.DeptCode ");
            SQL.AppendLine("LEFT JOIN tbldivision E ON B.DivisionCode=E.DivisionCode  ");
            SQL.AppendLine("LEFT JOIN tbloption T7 ON A.EmploymentStatus=T7.OptCode AND T7.OptCat='EmploymentStatus' ");
            SQL.AppendLine("WHERE ");
            SQL.AppendLine("(B.ResignDt IS NULL OR (B.ResignDt Is Not Null And substring(B.ResignDt, 1, 6)>CONCAT(@Yr, @Mth) ) ) ");
            SQL.AppendLine("GROUP BY A.SiteCode, B.DivisionCode, A.DeptCode, C.SiteName, E.DivisionName, D.DeptName  ");
            SQL.AppendLine(")A  ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Site",
                    "Divisi",
                    "Department", 
                    "PKWTT",
                    "PKWT", 
                    
                    //6-10
                    "Honorer",
                    "Total",
                    "Site Code",
                    "Division Code",
                    "Department Code"

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 200, 200, 100, 100,  
                    
                    //6-10
                    100, 100, 50, 50, 50

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 }, false);

        }
        

        override protected void ShowData()
        {
            
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " where 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDivisionCode), "A.DivisionCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + "  order BY A.SiteName, A.DeptName, A.DivisionName; ",
                    new string[]
                    {
                        //0
                        "SiteName",  

                        //1-5
                        "DivisionName", "DeptName", "PKWTT", "PKWT", "Honorer", 

                        //6-9
                        "Total", "SiteCode", "DivisionCode", "DeptCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        
                    }, true, false, false, false
                );
                
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion 

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");

        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDivisionCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Division");

        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");

        }
        #endregion 
    }
}
