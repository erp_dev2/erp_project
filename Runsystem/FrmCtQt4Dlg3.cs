﻿#region Update
/*
    07/04/2020 [WED/SRN] new apps, CtQt pakai discount rate 5 biji
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCtQt4Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmCtQt4 mFrmParent;
        private string mCtCode = string.Empty;
        #endregion

        #region Constructor

        public FrmCtQt4Dlg3(FrmCtQt4 FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Agent";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "",
                        "Agent Code", 
                        "Agent Name"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Locate(Concat('##', AgtCode, '##'), @SelectedAgent)<1 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@SelectedAgent", mFrmParent.GetSelectedAgent());
                Sm.FilterStr(ref Filter, ref cm, TxtAgtName.Text, new string[] { "AgtCode", "AgtName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select AgtCode, AgtName From TblAgent " +
                        "Where CtCode=@CtCode " +
                        Filter + " Order By AgtName;",
                        new string[] 
                        { 
                            "AgtCode", "AgtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsAgtCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 2, Grd1, Row2, 3);

                        mFrmParent.Grd3.Rows.Add();

                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 agent.");
        }


        private bool IsAgtCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareGrdStr(mFrmParent.Grd3, Index, 1, Grd1, Row, 2)) return true;
            return false;
        }

        #endregion

        #region grid method
        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                if (Sm.GetGrdStr(Grd1, 0, 2).Length != 0)
                {
                    if (Sm.GetGrdBool(Grd1, 0, 1) == false)
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            Grd1.Cells[Row, 1].Value = true;
                        }
                    }
                    else
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            Grd1.Cells[Row, 1].Value = false;
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #region Event

        private void TxtAgtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAgtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Agent");
        }

        #endregion
    }
}
