﻿namespace RunSystem
{
    partial class FrmTrnReprocessJournal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.Type = new System.Windows.Forms.Label();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.ChkUnbalancedJournal = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkUnbalancedJournal.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(422, 0);
            this.panel1.Size = new System.Drawing.Size(70, 85);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(70, 33);
            this.BtnProcess.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.ChkUnbalancedJournal);
            this.panel2.Controls.Add(this.LueType);
            this.panel2.Controls.Add(this.Type);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteEndDt);
            this.panel2.Controls.Add(this.DteStartDt);
            this.panel2.Size = new System.Drawing.Size(422, 85);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(150, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date";
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(164, 5);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(101, 20);
            this.DteEndDt.TabIndex = 3;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(47, 5);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(101, 20);
            this.DteStartDt.TabIndex = 2;
            // 
            // Type
            // 
            this.Type.AutoSize = true;
            this.Type.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type.ForeColor = System.Drawing.Color.Red;
            this.Type.Location = new System.Drawing.Point(10, 31);
            this.Type.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(35, 14);
            this.Type.TabIndex = 4;
            this.Type.Text = "Type";
            this.Type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(47, 28);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 30;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 400;
            this.LueType.Size = new System.Drawing.Size(367, 20);
            this.LueType.TabIndex = 19;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(67, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 14);
            this.label9.TabIndex = 21;
            this.label9.Text = "Unbalanced Journal";
            // 
            // ChkUnbalancedJournal
            // 
            this.ChkUnbalancedJournal.Location = new System.Drawing.Point(47, 52);
            this.ChkUnbalancedJournal.Name = "ChkUnbalancedJournal";
            this.ChkUnbalancedJournal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkUnbalancedJournal.Properties.Appearance.Options.UseFont = true;
            this.ChkUnbalancedJournal.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkUnbalancedJournal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkUnbalancedJournal.Properties.Caption = " ";
            this.ChkUnbalancedJournal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkUnbalancedJournal.Size = new System.Drawing.Size(19, 22);
            this.ChkUnbalancedJournal.TabIndex = 20;
            // 
            // FrmTrnReprocessJournal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 85);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmTrnReprocessJournal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkUnbalancedJournal.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label Type;
        private DevExpress.XtraEditors.LookUpEdit LueType;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.CheckEdit ChkUnbalancedJournal;
    }
}