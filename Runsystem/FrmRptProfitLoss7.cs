﻿#region Update
/*
    13/11/2020 [WED/VIR] new apps, Laba Rugi Format 2
    28/12/2020 [DITA/VIR] BUG: nilai journal belum muncul. permasalahan saat order by COA
    04/01/2020 [WED/VIR] COA yg journalnya connect dengan SOContract dimunculkan
    22/09/2021 [TKG/VIR] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
    28/12/2022 [VIN/VIR] COA Penjualan ganti menjadi 4.0.0.0
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss7 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mProfitLoss7SelectedCOA = string.Empty;

        private bool
            mIsAccountingRptUseJournalPeriod = false,
            mIsRptAccountingShowJournalMemorial = false;

        #endregion

        #region Constructor

        public FrmRptProfitLoss7(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetLueSiteCode(ref LueSiteCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Site", 
                    "Project#",
                    "Project Name",
                    "Penjualan",
                    "Biaya Langsung",

                    //6-10
                    "Laba Kotor Usaha",
                    "Pendapatan Lain-Lain",
                    "Beban Lain-Lain",
                    "Laba Bersih Sebelum Bunga",
                    "Bunga Bank",

                    //11-15
                    "Laba Bersih Sebelum Pajak",
                    "PPh Final",
                    "Laba Setelah Pajak",
                    "PRJI#",
                    "SOC#"
                },
                new int[] 
                {
                    //0
                    25,

                    //1-5
                    200, 100, 360, 150, 150, 

                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 0, 0
                }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15 });
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DtePeriodTo, "Period To") ||
                Sm.IsLueEmpty(LueSiteCode, "Site")
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string SOCDocNo = string.Empty;
                string SelectedCOA = string.Empty;
                string mPeriodTo = Sm.Left(Sm.GetDte(DtePeriodTo), 8);

                var l = new List<RowData>();
                var l2 = new List<COASOContract>();

                PrepData(ref l);
                if (l.Count > 0)
                {
                    SOCDocNo = GetSOCDocNo(ref l);
                    PrepCOASOContract(ref l2, SOCDocNo);
                    if (l2.Count > 0)
                    {
                        SelectedCOA = GetSelectedCOA(ref l2);
                        Process1(ref l2, SelectedCOA, mPeriodTo);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process2_JournalMemorial(ref l2, SelectedCOA, mPeriodTo);
                        else
                            Process2(ref l2, SelectedCOA, mPeriodTo);
                        Process3(ref l, ref l2);
                    }
                    Process4(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                //Grd1.Rows.CollapseAll();

                l.Clear(); l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mProfitLoss7SelectedCOA = Sm.GetParameter("ProfitLoss7SelectedCOA");
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

            if (mProfitLoss7SelectedCOA.Length == 0) mProfitLoss7SelectedCOA = "4.0.0.0,5,7.0.0,8,8.1,8.2";

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SiteCode As Col1, SiteName As Col2 ");
            SQL.AppendLine("From TblSite ");
            SQL.AppendLine("Where HOInd = 'N' ");
            SQL.AppendLine("Order By SiteName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void PrepData(ref List<RowData> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select F.SiteName, IfNull(C.ProjectCode2, C.ProjectCode) ProjectCode, E.ProjectName, A.DocNo, B.SOCDocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("    And E.SiteCode = @SiteCode ");
            SQL.AppendLine("Inner Join TblSite F On E.SiteCode = F.SiteCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteName", "ProjectCode", "ProjectName", "DocNo", "SOCDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RowData()
                        {
                            SiteName = Sm.DrStr(dr, c[0]),
                            ProjectCode = Sm.DrStr(dr, c[1]),
                            ProjectName = Sm.DrStr(dr, c[2]),
                            PRJIDocNo = Sm.DrStr(dr, c[3]),
                            SOCDocNo = Sm.DrStr(dr, c[4]),
                            Penjualan = 0m,
                            BiayaLangsung = 0m,
                            LabaKotorUsaha = 0m,
                            PendapatanLainLain = 0m,
                            BebanLainLain = 0m,
                            LabaBersihSebelumBunga = 0m,
                            BungaBank = 0m,
                            LabaBersihSebelumPajak = 0m,
                            PPhFinal = 0m,
                            LabaSetelahPajak = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSOCDocNo(ref List<RowData> l)
        {
            string DocNo = string.Empty;

            foreach(var x in l)
            {
                if (DocNo.Length > 0) DocNo += ",";
                DocNo += x.SOCDocNo;
            }

            return DocNo;
        }

        private void PrepCOASOContract(ref List<COASOContract> l2, string SOCDocNo)
        {
            string[] AcNo = mProfitLoss7SelectedCOA.Split(',');
            var lastObj = AcNo.Last();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.SOCDocNo, T.AcNo, T.AcType From ( ");
            SQL.AppendLine("    Select A.SOCDocNo, A.AcNo, B.OptDesc AcType ");
            SQL.AppendLine("    From TblCOA A ");
            SQL.AppendLine("    Left Join TblOption B On Left(A.AcNo, 1) = B.OptCode And B.OptCat = 'ProfitLoss6AcType' ");
            SQL.AppendLine("    Where A.SOCDocNo Is Not Null ");
            SQL.AppendLine("    And A.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(A.SOCDocNo, @SOCDocNo) ");
            SQL.AppendLine("    And ");
            SQL.AppendLine("    ( ");

            foreach(string x in AcNo)
            {
                SQL.AppendLine("        A.AcNo Like '" + x + "%' ");
                if (x != lastObj)
                    SQL.AppendLine("        Or ");
            }

            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.SOContractDocNo As SOCDocNo, A.AcNo, C.OptDesc As AcType ");
            SQL.AppendLine("    From TblJournalDtl A ");
            SQL.AppendLine("    Inner Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("        And A.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        And Find_In_Set(A.SOContractDocNo, @SOCDocNo) ");
            SQL.AppendLine("        And B.ActInd = 'Y' ");
            SQL.AppendLine("    Left Join TblOption C On Left(A.AcNo, 1) = C.OptCode And C.OptCat = 'ProfitLoss6AcType' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Order By T.AcNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SOCDocNo", SOCDocNo);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SOCDocNo", "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COASOContract()
                        {
                            SOCDocNo = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            DAmt = 0m,
                            CAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<COASOContract> l2)
        {
            string AcNo = string.Empty;

            foreach(var x in l2)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        // Opening Balance
        private void Process1(ref List<COASOContract> l2, string SelectedCOA, string PeriodTo)
        {
            string Yr = Sm.Left(PeriodTo, 4);
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var lJournal = new List<Journal>();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Order By B.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < l2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == l2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != l2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            l2[j].DAmt += lJournal[i].DAmt;
                            l2[j].CAmt += lJournal[i].CAmt;
                            if (string.Compare(l2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        // Journal sampai Period To
        private void Process2(ref List<COASOContract> l2, string SelectedCOA, string PeriodTo)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
            }
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@YrMth", Sm.Left(PeriodTo, 6));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < l2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == l2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != l2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            l2[j].DAmt += lJournal[i].DAmt;
                            l2[j].CAmt += lJournal[i].CAmt;
                            if (string.Compare(l2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process2_JournalMemorial(ref List<COASOContract> l2, string SelectedCOA, string PeriodTo)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
            }
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("    And Find_In_Set(C.AcNo, @SelectedCOA) ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) <= @YrMth ");
            }
            else
                SQL.AppendLine("    And Left(A.DocDt, 6) <= @YrMth ");

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                Sm.CmParam<String>(ref cm, "@YrMth", Sm.Left(PeriodTo, 6));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < l2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == l2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, l2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != l2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(l2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            l2[j].DAmt += lJournal[i].DAmt;
                            l2[j].CAmt += lJournal[i].CAmt;
                            if (string.Compare(l2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        // ceplokin data ke RowData
        private void Process3(ref List<RowData> l, ref List<COASOContract> l2)
        {
            foreach(var x in l)
            {
                foreach(var y in l2.Where(w => w.SOCDocNo == x.SOCDocNo))
                {
                    if (y.AcNo.Contains("4.0.0.0"))
                    {
                        x.Penjualan += (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }

                    if (y.AcNo.Contains("5.") && Sm.Left(y.AcNo, 2) == "5.")
                    {
                        x.BiayaLangsung += (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }

                    if (y.AcNo.Contains("7.") && Sm.Left(y.AcNo, 2) == "7.")
                    {
                        x.PendapatanLainLain += (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }

                    if (y.AcNo.Contains("8.") && Sm.Left(y.AcNo, 2) == "8.")
                    {
                        x.BebanLainLain += (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }

                    if (y.AcNo.Contains("8.1") && Sm.Left(y.AcNo, 3) == "8.1")
                    {
                        x.BungaBank += (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }

                    if (y.AcNo.Contains("8.2") && Sm.Left(y.AcNo, 3) == "8.2")
                    {
                        x.PPhFinal += (y.AcType == "C") ? (y.CAmt - y.DAmt) : (y.DAmt - y.CAmt);
                    }
                }

                x.LabaKotorUsaha = x.Penjualan - x.BiayaLangsung;
                x.LabaBersihSebelumBunga = x.LabaKotorUsaha + x.PendapatanLainLain - x.BebanLainLain;
                x.LabaBersihSebelumPajak = x.LabaBersihSebelumBunga - x.BungaBank;
                x.LabaSetelahPajak = x.LabaBersihSebelumPajak - x.PPhFinal;
            }
        }

        // ceplokin data ke grid
        private void Process4(ref List<RowData> l)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach(var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.SiteName;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.ProjectCode;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = x.ProjectName;
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.Penjualan, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.BiayaLangsung, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.FormatNum(x.LabaKotorUsaha, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = Sm.FormatNum(x.PendapatanLainLain, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.FormatNum(x.BebanLainLain, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = Sm.FormatNum(x.LabaBersihSebelumBunga, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 10].Value = Sm.FormatNum(x.BungaBank, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 11].Value = Sm.FormatNum(x.LabaBersihSebelumPajak, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 12].Value = Sm.FormatNum(x.PPhFinal, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 13].Value = Sm.FormatNum(x.LabaSetelahPajak, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 14].Value = x.PRJIDocNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 15].Value = x.SOCDocNo;

                Row += 1;
            }

            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Class

        private class RowData
        {
            public string SiteName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string PRJIDocNo { get; set; }
            public string SOCDocNo { get; set; }

            public decimal Penjualan { get; set; }
            public decimal BiayaLangsung { get; set; }
            public decimal LabaKotorUsaha { get; set; }
            public decimal PendapatanLainLain { get; set; }
            public decimal BebanLainLain { get; set; }
            public decimal LabaBersihSebelumBunga { get; set; }
            public decimal BungaBank { get; set; }
            public decimal LabaBersihSebelumPajak { get; set; }
            public decimal PPhFinal { get; set; }
            public decimal LabaSetelahPajak { get; set; }
        }

        private class COASOContract
        {
            public string SOCDocNo { get; set; }
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string ParentDesc { get; set; }
        }

        #endregion
    }
}
