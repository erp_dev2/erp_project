﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionPenaltyDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProductionPenalty mFrmParent;
        private string mSQL = "", mWagesFormulationCode = "";
        private decimal mTotalEmployee = 0m, mTotal = 0m;

        #endregion

        #region Constructor

        public FrmProductionPenaltyDlg3(FrmProductionPenalty FrmParent, decimal TotalEmployee, decimal Total, string WagesFormulationCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTotalEmployee = TotalEmployee;
            mTotal = Total;
            mWagesFormulationCode = WagesFormulationCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, C.DeptName, D.GrdLvlName, A.GrdLvlCode, E.GrdLvlIndex, ");
            SQL.AppendLine("Case When @TotalEmployee=0 Then 0 Else (1/@TotalEmployee)*@Total*IfNull(E.GrdLvlIndex, 1) End As Penalty ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 E On A.GrdLvlCode=E.GrdLvlCode And E.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Where A.ResignDt Is Null ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Employee Code", 
                        "Employee Name", 
                        "Position", 
                        "Department",

                        //6-9
                        "Grade Level Code",
                        "Grade Level",
                        "Index",
                        "Penalty"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And Position(Concat('##', A.EmpCode, '##') In @SelectedEmployee)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WagesFormulationCode", mWagesFormulationCode);
                Sm.CmParam<String>(ref cm, "@SelectedEmployee", mFrmParent.GetSelectedEmployee());
                Sm.CmParam<decimal>(ref cm, "@TotalEmployee", mTotalEmployee);
                Sm.CmParam<decimal>(ref cm, "@Total", mTotal);

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.DeptName, A.EmpName;",
                        new string[] 
                        { 
                            //0
                            "EmpCode",
 
                            //1-5
                            "EmpName", "PosName", "DeptName", "GrdLvlCode", "GrdLvlName",  
                            
                            //6-7
                            "GrdLvlIndex", "Penalty"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            try
            {
                if (Grd1.Rows.Count != 0)
                {

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsEmpCodeAlreadyChosen(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd4.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 3, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 4, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 5, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 6, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 7, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 8, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 9, Grd1, Row2, 9);

                            mFrmParent.Grd4.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd4, mFrmParent.Grd4.Rows.Count - 1, new int[] { 8, 9 });
                        }
                    }
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsEmpCodeAlreadyChosen(int Row)
        {
            string Key1 = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd4.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 2), Key1)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
