﻿#region Update
/*
    12/09/2017 [TKG] tambah employment status, tambah validasi berdasarkan otorisasi
    13/03/2018 [TKG] tambah contract extension, validasi employment status
    16/08/2019 [TKG] tambah filter site
    09/11/2019 [DITA] saat contract date mandatory start date akan otomatis terisi dengan contract date
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpContractDocDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpContractDoc mFrmParent;
        private string mDocDt;
        internal bool mIsEmpContractDtMandatory = false;
        string mSQL = string.Empty;
       
        #endregion

        #region Constructor

        public FrmEmpContractDocDlg(FrmEmpContractDoc FrmParent, string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR?"Y":"N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, A.DeptCode, C.DeptName, D.PosName, H.SiteName, A.JoinDt, A.ContractDt, ");
            SQL.AppendLine("E.OptDesc As Gender, F.GrdLvlName, ");
            SQL.AppendLine("A.EmploymentStatus, G.OptDesc As EmploymentStatusDesc, F.BasicSalary  ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode=B.UserCode  ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode "); 
            SQL.AppendLine("Left Join TblPosition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblOption E On A.Gender=E.OptCode and E.OptCat='Gender' "); 
            SQL.AppendLine("Left Join TblGradeLevelHdr F On A.GrdlvlCode = F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption G On A.EmploymentStatus=G.OptCode and G.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblSite H On A.SiteCode=H.SiteCode ");
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=@DocDt ) Or A.ResignDt Is Null) ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilteredByEmploymentStatus)
            {
                SQL.AppendLine("And A.EmploymentStatus Is Not Null ");
                SQL.AppendLine("And Find_In_Set(A.EmploymentStatus, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='EmpContractDocEmploymentStatus' And ParValue Is Not Null)) ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Old"+Environment.NewLine+"Code", 
                        "Employee's"+Environment.NewLine+"Name",
                        "Department",
                        "Position",

                        //6-10
                        "Site",
                        "Join",
                        "Gender",
                        "Grade",
                        "Status",
                        
                        //11-13
                        "Status",
                        "Salary",
                        "Contract"
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 13 });
            if (mIsEmpContractDtMandatory) Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 10, 12 }, false);
            else Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 10, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, true);
            Grd1.Cols[13].Move(8);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                String Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.CmParam<String>(ref cm, "@DocDt", mDocDt);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.EmpName ",
                        new string[] 
                        { 
                            //0
                            "EmpCode", 
                            
                            //1-5
                            "EmpCodeOld", "EmpName", "DeptName", "PosName", "SiteName",

                            //6-10
                             "JoinDt", "Gender", "GrdLvlName", "EmploymentStatus", "EmploymentStatusDesc", 
                             
                             //11-12
                             "BasicSalary", "ContractDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int r = Grd1.CurRow.Index;
                
                mFrmParent.TxtEmpCode.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, r, 3);
                mFrmParent.TxtDeptName.EditValue = Sm.GetGrdStr(Grd1, r, 4);
                mFrmParent.TxtPosition.EditValue = Sm.GetGrdStr(Grd1, r, 5);
                mFrmParent.TxtSite.EditValue = Sm.GetGrdStr(Grd1, r, 6);
                if(mIsEmpContractDtMandatory) Sm.SetDte(mFrmParent.DteStartDt, Sm.GetGrdDate(Grd1, r, 13));
                else Sm.SetDte(mFrmParent.DteStartDt, Sm.GetGrdDate(Grd1, r, 7)); 
                mFrmParent.TxtGrade.EditValue = Sm.GetGrdStr(Grd1, r, 9);
                Sm.SetLue(mFrmParent.LueEmploymentStatus, Sm.GetGrdStr(Grd1, r, 10)); 
                mFrmParent.TxtSalary.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 12), 0);
                mFrmParent.ShowContractExtHistory();

                this.Close();
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region
        private void GetParameter()
        {
            mIsEmpContractDtMandatory = Sm.GetParameterBoo("IsEmpContractDtMandatory");
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #region Grid Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
