﻿#region Update
/*
    26/04/2018 [TKG] Outgoing payment difilter berdasarkan otorisasi group thd departemen yg diambil dari PI (Y=Difilter. N=Tidak difilter)
    10/01/2020 [HAR/IOK] BUG saat menghitung outstanding amount di purchase return invoice (query)
    02/03/2020 [TKG/IMS] Berdasarkan parameter IsPITotalWithoutTaxInclDownpaymentEnabled, Total without tax include downpayment
    16/12/2020 [WED/IMS] ambil Purchase Invoice yang status nya approved
 *  04/02/2021 [ICA/IMS] Menampilkan AcAmt dan COATaxInd untuk menampilkan Outstanding Amount yang diambil dari Purchase Invoice dengan COA. 
 *  30/04/2021 [VIN/IMS] menampilkan amount PI dengan COA maupun tanpa COA
 *  27/09/2021 [ICA/AMKA] menambah ProcessInvoiceItem saat choose data
 *  11/01/2022 [ICA/AMKA] Bug ketika menampilkan amt PI dengan COA maupun tanpa COA berdasarkan COATaxInd dan parameter PurchaseInvoiceCOAAmtCalculationMethod
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOutgoingPayment2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmOutgoingPayment2 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmOutgoingPayment2Dlg(FrmOutgoingPayment2 FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "Date",
                        "", 
                        "Type",

                        //6-10
                        "Type",
                        "Vendor",
                        "Vendor",
                        "Currency",    
                        "Invoice Amount",

                        //11-15
                        "Due Date",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        "Dept Code",
                        "Department",
                        "Local#", 

                        //16-18
                        "AcAmt",
                        "COATaxInd",
                        "Invoice Amount"+Environment.NewLine+"Without COA",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 80, 20, 0, 
                        
                        //6-10
                        130, 0, 200, 60, 180,  
                        
                        //11-14
                        80, 130, 0, 150, 180,

                        //16-18
                        0, 0, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 7, 16, 13, 17, 18 }, false);
            if (!mFrmParent.mIsOutgoingPaymentApprovalNeedDept)
                Sm.GrdColInvisible(Grd1, new int[] { 14 }, false);
            Grd1.Cols[15].Move(3);
            if (mFrmParent.mIsOutgoingPaymentUsePIWithCOA)
            {
                Grd1.Cols[18].Move(12);
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, true);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.*, Tbl2.VdName, Tbl3.DeptName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("Select ");
	        SQL.AppendLine("A.DocNo, A.DocDt, '1' As InvoiceType, 'Purchase Invoice' As InvoiceTypeDesc, ");
	        SQL.AppendLine("A.VdCode, A.CurCode, ");
            if (mFrmParent.mIsPITotalWithoutTaxInclDownpaymentEnabled)
                SQL.AppendLine("(A.Amt+A.TaxAmt-IfNull(B.Amt, 0)) As Amt, ");
            else
	            SQL.AppendLine("(A.Amt+A.TaxAmt-A.DownPayment-IfNull(B.Amt, 0)) As Amt, ");
	        SQL.AppendLine("A.DueDt, A.VdInvNo, A.DeptCode, A.LocalDocNo, ");
            if (mFrmParent.mIsOutgoingPaymentUsePIWithCOA)
                SQL.AppendLine("IfNull(C.AcAmt, 0) As AcAmt, IfNull(A.COATaxInd, 'N') As COATaxInd ");
            else
                SQL.AppendLine("0.00 As AcAmt, 'N' As COATaxInd ");
	        SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
	        SQL.AppendLine("Left Join ( ");
		    SQL.AppendLine("    Select T.PurchaseInvoiceDocNo, Sum(T.Amt) Amt ");
		    SQL.AppendLine("    From ( ");
			SQL.AppendLine("        Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
			SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' "); 
			SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And IfNull(T3.ProcessInd, 'O')<>'F' ");
            if (mFrmParent.mIsOutgoingPaymentFilterByPIDept)
            {
                SQL.AppendLine("    And (T3.DeptCode Is Null ");
                SQL.AppendLine("    Or (T3.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(T3.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ))) ");
            }
		    SQL.AppendLine("        Where T1.CancelInd='N' "); 
		    SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select T1.PurchaseInvoiceDocNo, T1.Amt ");
			SQL.AppendLine("        From TblApsHdr T1 ");
			SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T2 On T1.PurchaseInvoiceDocNo=T2.DocNo And IfNull(T2.ProcessInd, 'O')<>'F' ");
		    SQL.AppendLine("        Where T1.CancelInd='N' ");
		    SQL.AppendLine("    ) T Group By T.PurchaseInvoiceDocNo ");
	        SQL.AppendLine(") B On A.DocNo=B.PurchaseInvoiceDocNo ");
            if (mFrmParent.mIsOutgoingPaymentUsePIWithCOA)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select X.DocNo, X.AcNo, ");
                SQL.AppendLine("    Case ");
                SQL.AppendLine("        When X.AcType = 'D' && X.DAmt>0 Then X.DAmt ");
                SQL.AppendLine("        When X.AcType = 'D' && X.CAmt>0 Then X.CAmt *-1 ");
                SQL.AppendLine("        When X.AcType = 'C' && X.CAmt>0 Then X.Camt ");
                SQL.AppendLine("        When X.AcType = 'C' && X.DAmt>0 Then X.DAmt *-1 ");
                SQL.AppendLine("    End As AcAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo, B.AcNo, C.Actype, B.DAmt, B.Camt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblCOA C On B.AcNo=C.Acno ");
                if(mFrmParent.mPurchaseInvoiceCOAAmtCalculationMethod == "1")
                {
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoAP' ");
                    //SQL.AppendLine("        Where A.DocNo='@DocNo' ");
                    SQL.AppendLine("        And A.COATaxInd = 'Y' ");
                    SQL.AppendLine("        And C.AcNo=Concat(D.ParValue, A.VdCode)");
                }
                else
                {
                    SQL.AppendLine("        And B.AcNo In ( ");
                    SQL.AppendLine("            SELECT T5.AcNo9 ");
                    SQL.AppendLine("            FROM TblPurchaseInvoiceHdr T ");
                    SQL.AppendLine("            INNER JOIN TblPurchaseInvoiceDtl T2 ON T.DocNo = T2.DocNo ");
                    SQL.AppendLine("            INNER JOIN TblRecvVdDtl T3 ON T2.RecvVdDocNo = T3.DocNo ");
                    SQL.AppendLine("                AND T2.RecvVdDNo = T3.DNo ");
                    SQL.AppendLine("                AND T3.CancelInd = 'N' ");
                    SQL.AppendLine("            INNER JOIN TblItem T4 ON T3.ItCode = T4.ItCode ");
                    SQL.AppendLine("            INNER JOIN TblItemCategory T5 ON T4.ItCtCode = T5.ItCtCode ");
                    SQL.AppendLine("            WHERE T.DOcNo = A.DocNo AND T5.AcNo9 = B.AcNo ");
                    SQL.AppendLine("        ) ");
                }
                SQL.AppendLine("    ) X ");
                SQL.AppendLine(") C On A.DocNo = C.DocNo ");
            }
	        SQL.AppendLine("Where A.CancelInd='N' ");
            if (mFrmParent.mIsPurchaseInvoiceUseApproval) SQL.AppendLine("And A.Status = 'A' ");
	        SQL.AppendLine("And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or (A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
	        SQL.AppendLine("Union All ");
	        SQL.AppendLine("Select ");
	        SQL.AppendLine("A.DocNo, A.DocDt, '2' As InvoiceType, 'Purchase Return Invoice' As InvoiceTypeDesc, ");
	        SQL.AppendLine("A.VdCode, A.CurCode, ");
	        SQL.AppendLine("(A.Amt-IfNull(B.Amt, 0)) As Amt, ");
            SQL.AppendLine("Null As DueDt, Null As VdInvNo, A.DeptCode As DeptCode, Null As LocalDocNo, 0.00 As AcAmt, 'N' As COATaxInd ");
	        SQL.AppendLine("From TblPurchaseReturnInvoiceHdr A ");
	        SQL.AppendLine("Left Join ( ");
			SQL.AppendLine("    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt ");
			SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
			SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And IfNull(T3.ProcessInd, 'O')<>'F' ");
		    SQL.AppendLine("    Where T1.CancelInd='N' "); 
		    SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
		    SQL.AppendLine("    Group By T2.InvoiceDocNo ");
	        SQL.AppendLine(") B On A.DocNo=B.DocNo ");
	        SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And IfNull(A.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And A.DocNo In (");
                SQL.AppendLine("    Select T.DocNo From TblPurchaseInvoiceHdr T Where 1=1 ");
                SQL.AppendLine("And (T.SiteCode Is Null Or (T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
                SQL.AppendLine("    ) ");
            }
            //SQL.AppendLine(") ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Inner Join TblVendor Tbl2 On Tbl1.VdCode=Tbl2.VdCode");
            SQL.AppendLine("Left Join TblDepartment Tbl3 On Tbl1.DeptCode=Tbl3.DeptCode");
            SQL.AppendLine("Where Locate(Concat('##', Tbl1.DocNo, Tbl1.InvoiceType, '##'), @SelectedInvoice)<1 ");
            SQL.AppendLine("And Tbl1.VdCode=@VdCode ");
            if(mFrmParent.mIsOutgoingPaymentUsePIWithCOA)
                SQL.AppendLine("AND Tbl1.Amt+Tbl1.AcAmt>0 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SelectedInvoice", mFrmParent.GetSelectedInvoice());
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By InvoiceType, DocDt Desc, DocNo Desc;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "InvoiceType", "InvoiceTypeDesc", "VdCode", "VdName", 
                            
                            //6-10
                            "CurCode", "Amt", "DueDt", "VdInvNo", "DeptCode",
 
                            //11-15
                            "DeptName", "LocalDocNo", "AcAmt", "COATaxInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            if (mFrmParent.mIsOutgoingPaymentUsePIWithCOA)
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            else
                                Grd.Cells[Row, 10].Value = dr.GetDecimal(c[7]) - dr.GetDecimal(c[13]);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 17, 14);
                            Grd.Cells[Row, 18].Value = dr.GetDecimal(c[7]) - dr.GetDecimal(c[13]);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        //if (mFrmParent.mIsOutgoingPaymentUsePIWithCOA && Sm.GetGrdBool(Grd1, Row, 17))
                        //    Grd1.Cells[Row2, 10].Value = Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        mFrmParent.Grd1.Cells[Row1, 11].Value = 0m;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 18);
                        mFrmParent.Grd1.Cells[Row1, 14].Value = null;
                        mFrmParent.ComputeGiroAmt();
                        mFrmParent.ComputeAmt();
                        mFrmParent.ProcessInvoiceItem(Sm.GetGrdStr(Grd1, Row2, 2));
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 11 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 purchase invoice or purchase return invoice document.");
            else this.Close();
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 5);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key,
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 5)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmPurchaseReturnInvoice(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmPurchaseReturnInvoice(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
