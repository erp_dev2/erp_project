﻿#region Update
/*
 *      10/08/2021 [ICA/ALL] New Apps
        18/08/2021 [DITA/ALL] pada saat narik coa di buat checklist dan bisa narik parent dan juga multiple acno
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCapitalStatementSettingDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCapitalStatementSetting mFrmParent;
        private string mSQL = string.Empty;
        private int
            mCurRow = 0;

        #endregion

        #region Constructor

        public FrmCapitalStatementSettingDlg(FrmCapitalStatementSetting FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();

                Sl.SetLueAcCtCode(ref LueAcCtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5 
                        "",
                        "Account#", 
                        "Description", 
                        "Type",
                        "Category",

                        //6
                        "COA Alias"
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcTypeDesc, C.AcCtName, A.Alias ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
            //SQL.AppendLine("Where A.AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");
            SQL.AppendLine("Where A.ActInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo;",
                        new string[] 
                        { 
                            "AcNo", "AcDesc", "AcTypeDesc", "AcCtName", "Alias"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );

                if (Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 4).Length > 0)
                {
                    int mListIndex = 0;
                    for (int t = 0; t < mFrmParent.lCR.Count; ++t)
                    {
                        if (mFrmParent.lCR[t].Row == mCurRow)
                        {
                            mListIndex = t;
                            break;
                        }
                    }

                    string[] mAcNos = mFrmParent.lCR[mListIndex].AcNo.Split(',');
                    int mIndex = 0;
                    for (int x = 0; x < mAcNos.Count(); ++x)
                    {
                        for (int i = mIndex; i < Grd1.Rows.Count; ++i)
                        {
                            if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                            {
                                if (Sm.GetGrdStr(Grd1, i, 2) == mAcNos[x])
                                {
                                    Grd1.Cells[i, 1].Value = true;
                                    mIndex = i;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            var AcNo = string.Empty;
            if (Grd1.Rows.Count != 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1))
                    {
                        if (AcNo.Length > 0) AcNo += ",";
                        AcNo += string.Concat(Sm.GetGrdStr(Grd1, r, 2));
                    }
                }
            }

            mFrmParent.GetCOAList(mCurRow, AcNo);

            //if (Sm.IsFindGridValid(Grd1, 1))
            //{
            //    int Row = Grd1.CurRow.Index;

            //    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 4, Grd1, Row, 2);
            //    for (int row = 0; row < mFrmParent.Grd1.Rows.Count - 1; row++)
            //    {
            //        mFrmParent.ComputeBalance(row);
            //    }
            //    //mFrmParent.ComputeBalance(mCurRow);
            //    this.Close();
            //}
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account number");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account description");
        }

        private void LueAcCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCtCode, new Sm.RefreshLue1(Sl.SetLueAcCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAcCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account category");
        }

        #endregion

        #endregion

    }
}
