﻿#region Update
/*
    15/02/2023 [SET/HEX] Menu Dlg baru
    22/02/2023 [SET/HEX] penyesuaian source Uom untuk FrmParent
    03/03/2023 [SET/HEX] Penyesuaian untuk currency dtl FrmParent
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmApprovalSheetDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmApprovalSheet mFrmParent;
        private string 
            mSQL = string.Empty, 
            mCCCode = string.Empty,
            mLocCode = string.Empty,
            mItCtCode = string.Empty;
        private int mCurRow;

        #endregion

        #region Constructor

        public FrmApprovalSheetDlg(FrmApprovalSheet FrmParent, int CurRow, string ItCtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
            mItCtCode = ItCtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                //Sl.SetLueCCCode(ref LueCCCode);
                //Sl.SetLueSiteCode(ref LueSiteCode);
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.ItCode, A.ItName, A.ForeignName, B.ItCtName, A.PurchaseUoMCode UoMCode, C.UomName, A.Specification ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner JOIN tblitemcategory B ON A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("Inner JOIN tblUom C ON A.PurchaseUoMCode = C.UoMCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' And A.ItCtCode = @ItCtCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Item's Code",
                        "Item's Name", 
                        "", 
                        "Foreign Name",

                        //6-8
                        "Item's Category",
                        "UomCode",
                        "UoM",
                        "Specification",
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 150, 20, 250,

                        //6-10
                        150, 100, 100, 100, 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd1, new int[] {  }, 0);
            Sm.GrdFormatDate(Grd1, new int[] {  });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And A.ItCode Not In ("+mFrmParent.GetSelectedItem()+")";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ItCtCode", mItCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName", "ForeignName", "ItCtName", "UoMCode", "UomName", 
                            
                            //6
                            "Specification",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row, 8);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 5, 9, 10 });
                        Close();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Property.");
        }


        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 3), Sm.GetGrdStr(Grd1, Row, 3)) 
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmPropertyInventory(mFrmParent.mMenuCode);
            //    f.Tag = mFrmParent.mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mPropertyCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
            //    f.ShowDialog();
            //}
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void LueCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void LueSite_Validated(object sender, EventArgs e)
        {

            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

    }
}
