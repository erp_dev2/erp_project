﻿#region Update
/*
    06/11/2017 [WED] insert ke tabel summary2
    11/06/2018 [WED] bug saat insert tabel summary2 dari IDR to non IDR
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerDepositCurSwitch : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmCustomerDepositCurSwitchFind FrmFind;
        internal string mDocNo = string.Empty;
        private bool mIsAutoJournalActived = false;
        private string
            mMainCurCode = string.Empty,
            mAcNoForForeignExchange = string.Empty,
            mCustomerAcNoDownPayment = string.Empty;

        #endregion

        #region Constructor

        public FrmCustomerDepositCurSwitch(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetLueCtCode(ref LueCtCode);
                Sl.SetLueCurCode(ref LueCurCode1);
                Sl.SetLueCurCode(ref LueCurCode2);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCurCode1, LueCurCode2, LueCtCode, TxtExistingAmt1,
                        TxtAmt1, TxtAmt2, TxtRateAmt, MeeRemark
                    }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtExistingAmt1, TxtAmt2
                    }, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode1, LueCurCode2, LueCtCode, TxtRateAmt, 
                        TxtAmt1, MeeRemark
                    }, false);
                    TxtRateAmt.EditValue = Sm.FormatNum(1, 0);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCtCode, LueCurCode1, LueCurCode2, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>  
            {
                TxtAmt1, TxtAmt2,  TxtExistingAmt1, TxtRateAmt
            }, 0);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueCurCode1, LueCurCode2, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt1, TxtAmt2,  TxtExistingAmt1, TxtRateAmt
            }, 0);
        }



        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCustomerDepositCurSwitchFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CustomerDepositCurSwitch", "TblCustomerDepositCurSwitch");

            if (IsExtAmountNotValid()) return;

            var cml = new List<MySqlCommand>();

            var lR = new List<Rate>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            decimal DP = 0m;

            SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary2 ");
            SQL.AppendLine("Where CtCode = @CtCode ");
            SQL.AppendLine("And CurCode = @CurCode1 ");
            SQL.AppendLine("And Amt > 0 Order By CreateDt Asc; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@CurCode1", Sm.GetLue(LueCurCode1));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lR.Add(new Rate()
                        {
                            CtCode = Sm.DrStr(dr, c[0]),

                            CurCode = Sm.DrStr(dr, c[1]),
                            ExcRate = Sm.DrDec(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }

            if (lR.Count > 0)
            {
                DP = decimal.Parse(TxtAmt1.Text);
                for (int i = 0; i < lR.Count; i++)
                {
                    if (DP > 0)
                    {
                        cml.Add(ReduceAmtSummary2(ref lR, i, DP));
                        DP = DP - lR[i].Amt;
                    }
                }

                // save ke tabel summary2 untuk nilai switch nya

                if (Sm.GetLue(LueCurCode1) != mMainCurCode && Sm.GetLue(LueCurCode2) != mMainCurCode) // switch from non-MainCurCode to non-MainCurCode
                {
                    decimal rate = 0m;
                    for (int x = 0; x < lR.Count; x++)
                    {
                        rate += lR[x].ExcRate / decimal.Parse(TxtRateAmt.Text);
                    }

                    rate = rate / lR.Count;

                    cml.Add(SaveExcRateOtherMainCurCode(rate));
                }
                else // swicth from MainCurCode or to MainCurCode
                {
                    cml.Add(SaveExcRateMainCurCode());
                }

            }
            else
            {
                // 07/06/2018 [TKG] untuk IDR to Non IDR
                if (!Sm.CompareStr(Sm.GetLue(LueCurCode2), mMainCurCode))
                    cml.Add(SaveCustomerDepositSummary2());
            }
            
            cml.Add(SaveCustomerDepositCurSwitch(DocNo));

            if (mIsAutoJournalActived)
            {
                decimal Amt1 = 0m, Amt2 = 0m;
                PrepareJournal(ref Amt1, ref Amt2);
                if (Amt1 != Amt2)
                {
                    if (Amt1 < Amt2)
                        cml.Add(SaveJournal(DocNo, 1, Amt2 - Amt1));
                    else
                        cml.Add(SaveJournal(DocNo, 2, Amt1 - Amt2));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private MySqlCommand SaveCustomerDepositCurSwitch(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositCurSwitch (DocNo, DocDt, CtCode, CurCode1, Amt1, RateAmt1, CurCode2, Amt2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @CtCode, @CurCode1, @Amt1, @RateAmt1, @CurCode2, @Amt2, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("values (@DocNo, @DocType1, @DocDt, @CtCode, @CurCode1, @AmtMin, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("values (@DocNo, @DocType2, @DocDt, @CtCode, @CurCode2, @AmtPlus, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@CtCode, @CurCode1, @Amt1, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt-@Amt1, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@CtCode, @CurCode2, @Amt2, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt+@Amt2, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode1", Sm.GetLue(LueCurCode1));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtMin", Decimal.Parse(TxtAmt1.Text) * -1);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt1", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtPlus", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DocType1", "05");
            Sm.CmParam<String>(ref cm, "@DocType2", "06");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCustomerDepositSummary2()
        {
            var SQL = new StringBuilder();
            decimal RateAmt = Decimal.Parse(TxtRateAmt.Text), ExcRate = 0m;

            if (RateAmt != 0) ExcRate = 1m / RateAmt;

            SQL.AppendLine("Insert Into TblCustomerDepositSummary2 ");
            SQL.AppendLine("(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @CurCode, @ExcRate, @Amt, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        Amt=Amt+@Amt, ");
            SQL.AppendLine("        LastUpBy=@UserCode, ");
            SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, byte Type, decimal Amt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblCustomerDepositCurSwitch Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Customer Deposit (Currency Switch) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblCustomerDepositCurSwitch Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");

            if (Type == 1)
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("Concat(@CustomerAcNoDownPayment, @CtCode) As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("Concat(@CustomerAcNoDownPayment, @CtCode) As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AcNoForForeignExchange", mAcNoForForeignExchange);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoDownPayment", mCustomerAcNoDownPayment);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCurCode1, "Currency (Switch From)") ||
                Sm.IsTxtEmpty(TxtAmt1, "Switched Amount", true) ||
                Sm.IsLueEmpty(LueCurCode2, "Currency (Switch To)") ||
                IsCurCodeNotDifferent() ||
                Sm.IsTxtEmpty(TxtRateAmt, "Rate Amount", true) ||
                Sm.IsTxtEmpty(TxtAmt2, "Amount", true) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsAmountNotValid();
        }

        private bool IsCurCodeNotDifferent()
        {
            if (Sm.GetLue(LueCurCode1) == Sm.GetLue(LueCurCode2))
            {
                Sm.StdMsg(mMsgType.Info, "Switched Currency should be different.");
                LueCurCode2.Focus();
                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            decimal Exist = decimal.Parse(TxtExistingAmt1.Text);
            decimal Amt1 = decimal.Parse(TxtAmt1.Text);
            if (Amt1 > Exist)
            {
                Sm.StdMsg(mMsgType.Warning, "Switched Amount is bigger than Existing Amount.");
                return true;
            }
            return false;
        }

        private bool IsExtAmountNotValid()
        {
            decimal Amt = decimal.Parse(TxtAmt1.Text);
            decimal ExAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblCustomerDepositSummary Where CtCode = '" + Sm.GetLue(LueCtCode) + "' And CurCode = '" + Sm.GetLue(LueCurCode1) + "' "));

            if (Amt > ExAmt)
            {
                Sm.StdMsg(mMsgType.Warning, "Switched Amount is bigger than Existing Amount.");
                TxtExistingAmt1.EditValue = Sm.FormatNum(ExAmt, 0);
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select DocNo, DocDt, CtCode, CurCode1, Amt1, RateAmt1, CurCode2, Amt2, Remark " +
                        "From TblCustomerDepositCurSwitch Where DocNo=@DocNo",
                        new string[] 
                        {
                            "DocNo", 
                            
                            "DocDt", "CtCode", "CurCode1", "Amt1", "RateAmt1",  
                            
                            "CurCode2", "Amt2", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueCurCode1, Sm.DrStr(dr, c[3]));
                            TxtAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                            TxtRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                            Sm.SetLue(LueCurCode2, Sm.DrStr(dr, c[6]));
                            TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        }, true
                    );
                ExistingAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional method

        private MySqlCommand ReduceAmtSummary2(ref List<Rate> lR, int i, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=" + x + "; ");

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand SaveExcRateMainCurCode()
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @CtCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm1, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", (Sm.GetLue(LueCurCode2) == mMainCurCode) ? 1 : (1 / decimal.Parse(TxtRateAmt.Text)));
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand SaveExcRateOtherMainCurCode(decimal rate)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @CtCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm1, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", rate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mAcNoForForeignExchange = Sm.GetParameter("AcNoForForeignExchange");
            mCustomerAcNoDownPayment = Sm.GetParameter("CustomerAcNoDownPayment");
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
        }

        private void PrepareJournal(ref decimal Amt1, ref decimal Amt2)
        {
            string
                CurCode1 = Sm.GetLue(LueCurCode1),
                CurCode2 = Sm.GetLue(LueCurCode2);

            if (TxtAmt1.Text.Length > 0)
                Amt1 = decimal.Parse(TxtAmt1.Text);

            if (!Sm.CompareStr(CurCode1, mMainCurCode))
                Amt1 *= GetCurRate(CurCode1, mMainCurCode);

            if (TxtAmt2.Text.Length > 0)
                Amt2 = decimal.Parse(TxtAmt2.Text);

            if (!Sm.CompareStr(CurCode2, mMainCurCode))
                Amt2 *= GetCurRate(CurCode2, mMainCurCode);
        }

        private decimal GetCurRate(string CurCode1, string CurCode2)
        {
            var SQL = new StringBuilder();
            var Value = string.Empty;

            SQL.AppendLine("Select Amt From TblCurrencyRate ");
            SQL.AppendLine("Where CurCode1=@CurCode1 ");
            SQL.AppendLine("And CurCode2=@CurCode2 ");
            SQL.AppendLine("And RateDt<=@DocDt ");
            SQL.AppendLine("Order By RateDt Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode1", CurCode1);
            Sm.CmParam<String>(ref cm, "@CurCode2", CurCode2);
            Value = Sm.GetValue(cm);

            if (Value.Length == 0)
                return 0m;
            else
                return decimal.Parse(Value);
        }


        #region SetLue

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct A.CtCode As Col1, B.CtName As Col2 " +
                "From TblCustomerDepositSummary A " +
                "Inner Join TblCustomer B On A.CtCode=B.CtCode " +
                "Order By B.CtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void ExistingAmt()
        {
            try
            {
                if (Sm.GetLue(LueCurCode1).Length != 0)
                {
                    string ExAmt = Sm.GetValue("Select Amt From TblCustomerDepositSummary Where CtCode = '" + Sm.GetLue(LueCtCode) + "' And CurCode = '" + Sm.GetLue(LueCurCode1) + "'; ");
                    TxtExistingAmt1.EditValue = Sm.FormatNum(ExAmt, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeAmt()
        {
            try
            {
                if (Sm.GetLue(LueCurCode2).Length != 0 && TxtRateAmt.Text.Length != 0 && TxtAmt1.Text.Length != 0 && TxtDocNo.Text.Length == 0)
                {
                    decimal Amt = Decimal.Parse(TxtAmt1.Text) * Decimal.Parse(TxtRateAmt.Text);
                    TxtAmt2.EditValue = Sm.FormatNum(Amt, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData2();
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            Sl.SetLueCurCode(ref LueCurCode1);
        }

        private void LueCurCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode1, new Sm.RefreshLue1(Sl.SetLueCurCode));
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ LueCurCode2 });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                {
                    TxtAmt1, TxtAmt2,  TxtExistingAmt1, TxtRateAmt
                }, 0);
            }
            ExistingAmt();
        }

        private void LueCurCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt2, TxtRateAmt }, 0);
            Sm.RefreshLookUpEdit(LueCurCode2, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtAmt1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt1, 0);
        }

        private void TxtRateAmt_Validated(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                ComputeAmt();
                Sm.FormatNumTxt(TxtRateAmt, 0);
            }
        }

        #endregion

        #endregion

        #region Class

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }

        #endregion

        #endregion
    }
}
