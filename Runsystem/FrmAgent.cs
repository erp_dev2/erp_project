﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAgent : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal string AgentParam = Sm.GetParameter("IsAgtCodeAutoIncrement");

        iGCell fCell;
        bool fAccept;
        internal FrmAgentFind FrmFind;

        #endregion

        #region Constructor

        public FrmAgent(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueCityCode(ref LueCityCode);
                Sl.SetLueCntCode(ref LueCountryCode);
                LueBankCode.Visible = false;
                LueCity2.Visible = false;
                LueCntCode.Visible = false;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        "Contact Person Name",
                        "Position",
                        "Contact Number"
                    },
                     new int[] 
                    {
                        300, 200, 200
                    }
                );

            Grd2.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Code", "Bank Name", "Branch Name", "Account Name", "Account Number", "Swift Code" },
                    new int[] { 50, 250, 150, 200, 180, 80 }
                );
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);

            Grd3.Cols.Count = 12;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] { 
                        "Name",
                        "Address", "CountryCode", "Country", "CityCode", "City",  
                        "PostalCode","Phone", "Fax", "Email Address", "Mobile", 
                        "Remark" },
                    new int[] { 150,200, 80, 100, 80, 200, 80, 100, 100, 150, 100, 200 }
                );
            Sm.GrdColInvisible(Grd3, new int[] { 2, 4 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAgtCode, TxtAgtName, ChkActInd, LueCtCode, TxtNPWP, 
                        MeeAddress, LueCityCode, TxtPostalCd, TxtPhone, TxtFax, 
                        TxtEmail, TxtMobile, MeeRemark, LueBankCode, LueCity2, LueCountryCode, LueCntCode
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    TxtAgtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtAgtCode, (AgentParam == "Y"));    
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAgtName, LueCtCode, MeeAddress, LueCityCode, LueCountryCode, LueCntCode,
                        TxtNPWP, TxtPostalCd, TxtPhone, TxtFax, TxtEmail, LueBankCode, LueCity2,
                        TxtMobile, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    TxtAgtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtAgtCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAgtName, ChkActInd, LueCtCode, MeeAddress, TxtNPWP, LueCityCode, TxtPostalCd, 
                        TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeRemark, LueBankCode, LueCity2, LueCountryCode, LueCntCode
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    TxtAgtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAgtCode, TxtAgtName, LueCtCode, TxtNPWP, MeeAddress, 
                LueCityCode, TxtPostalCd, TxtPhone, TxtFax, TxtEmail, 
                TxtMobile, MeeRemark, LueBankCode, LueCity2, LueCountryCode, LueCntCode
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAgentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAgtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAgtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAgent Where AgtCode=@AgtCode" };
                Sm.CmParam<String>(ref cm, "@AgtCode", TxtAgtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
              
                string AgtCode = TxtAgtCode.Text;
                
                if (AgentParam == "Y" && TxtAgtCode.Text.Length == 0)
                    AgtCode = GenerateAgtCode();

                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid(AgtCode)) return;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveAgent(AgtCode));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveAgentContaAgtPerson(AgtCode, Row));
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveAgentBankAccount(AgtCode, Row));
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0) cml.Add(SaveAgentShipAddress(AgtCode, Row));

                Sm.ExecCommands(cml);

                ShowData(AgtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 2, 3, 4 }, e);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 4, 5 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd2, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd2, LueBankCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sl.SetLueBankCode(ref LueBankCode);
            }
        }

        private void Grd3_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 6, 7, 8, 9, 10, 11 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd3, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueCity2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLueCityCode(ref LueCity2);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueCntCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLueCntCode(ref LueCntCode);
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 2, 5, 6, 7, 8, 9 }, e);
        }

        #endregion

        #region Show Data

        public void ShowData(string AgtCode)
        {
            try
            {
                ClearData();
                ShowAgent(AgtCode);
                ShowAgentContaAgtPerson(AgtCode);
                ShowAgentBankAccount(AgtCode);
                ShowAgentShipAddress(AgtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAgent(string AgtCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select * From TblAgent Where AgtCode=@AgtCode",
                    new string[] 
                    {
                        "AgtCode", 
                        "AgtName", "ActInd", "CntCode", "CtCode", "NPWP",  
                        "Address", "CityCode", "PostalCd", "Phone", "Fax",  
                        "Email","Mobile", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAgtCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAgtName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueCountryCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[4]));
                        TxtNPWP.EditValue = Sm.DrStr(dr, c[5]);
                        MeeAddress.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[7]));
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[9]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[10]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[11]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[12]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                    }, true
                );
        }

        private void ShowAgentContaAgtPerson(string AgtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select ContactPersonName, Position, ContactNumber From TblAgentContactPerson Where AgtCode=@AgtCode Order By ContactPersonName",
                    new string[] 
                    { 
                        "ContactPersonName", "Position", "ContactNumber"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowAgentBankAccount(string AgtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.BankCode, B.BankName, A.BankBranch, A.BankAcName, A.BankAcNo, A.SwiftCode " +
                    "From TblAgentBankAccount A, TblBank B " +
                    "Where A.BankCode=B.BankCode And A.AgtCode=@AgtCode " +
                    "Order By A.DNo",
                    new string[] 
                    { 
                        "BankCode", 
                        "BankName", "BankBranch", "BankAcName", "BankAcNo", "SwiftCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowAgentShipAddress(string AgtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                      "Select A.Name, A.Address, A.CntCode, D.CntName, A.CityCode, Concat(B.CityName, ' ( ', IfNull(C.ProvName, ''), ' ', IfNull(D.CntName, ''), ' )') " +
                      "As CityName, A.PostalCd,  " +
                      "A.Phone, A.Fax, A.Email, A.Mobile,  A.Remark " +
                      "From TblAgentShipAddress A " +
                      "Inner Join TblCity B On A.CityCode = B.CityCode " +
                      "Left Join TblProvince C On B.ProvCode=C.ProvCode " +
                      "Left Join TblCountry D On A.CntCode=D.CntCode Where A.AgtCode=@AgtCode",
                    new string[] 
                    {
                        "Name",  
                        "Address", "CntCode", "CntName", "CityCode", "CityName",  
                        "PostalCd", "Phone", "Fax", "Email", "Mobile",  
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    }, false, false, true, false
                );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid(string AgtCode)
        {
            return
                (!TxtAgtCode.Properties.ReadOnly && Sm.IsTxtEmpty(TxtAgtCode, "Agent Code", false)) ||
                (!TxtAgtCode.Properties.ReadOnly && IsAgtCodeExisted(AgtCode)) ||
                Sm.IsTxtEmpty(TxtAgtName, "Agent Name", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                IsCtCodeNotValid() ||
                Sm.IsLueEmpty(LueCountryCode, "Country");
        }

        private bool IsAgtCodeExisted(string AgtCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select AgtCode From TblAgent Where AgtCode=@AgtCode Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Agent code ( " + AgtCode + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Name is empty.")) return true;
            return false;
        }

        private string GenerateAgtCode()
        {
            return Sm.GetValue(
                "Select Right(Concat('00000', AgtCodeMax) , 5) As AgtCodeTemp " +
                "From (Select ifnull(Max(Cast(Trim(AgtCode) As Decimal)), 00000)+1 AgtCodeMax From TblAgent) T;"
                );
        }

        private bool IsCtCodeNotValid()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select CtCode From TblCustomer Where AgentInd='N' And CtCode=@CtCode Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, LueCtCode.GetColumnValue("Col2") + " can't have any agent.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveAgent(string AgtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAgent(AgtCode, AgtName, ActInd, CntCode, CtCode, NPWP, Address, CityCode, PostalCd, Phone, Fax, Email, Mobile, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AgtCode, @AgtName, @ActInd, @CntCode, @CtCode, @NPWP, @Address, @CityCode, @PostalCd, @Phone, @fax, @Email, @Mobile, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update AgtName=@AgtName, ActInd=@ActInd, CntCode=@CntCode, CtCode=@CtCode, NPWP=@NPWP, Address=@Address, CityCode=@CityCode, PostalCd=@PostalCd, Phone=@Phone, fax=@fax, Email=@Email, Mobile=@Mobile, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblAgentContactPerson Where AgtCode=@AgtCode; ");
            SQL.AppendLine("Delete From TblAgentBankAccount Where AgtCode=@AgtCode; ");
            SQL.AppendLine("Delete From TblAgentShipAddress Where AgtCode=@AgtCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.CmParam<String>(ref cm, "@AgtName", TxtAgtName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CntCode", Sm.GetLue(LueCountryCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            Sm.CmParam<String>(ref cm, "@PostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Fax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAgentContaAgtPerson(string AgtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAgentContactPerson(AgtCode, DNo, ContactPersonName, Position, ContactNumber, CreateBy, CreateDt) " +
                    "Values(@AgtCode, @DNo, @ContactPersonName, @Position, @ContactNumber, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ContactPersonName", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ContactNumber", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAgentBankAccount(string AgtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAgentBankAccount(AgtCode, DNo, BankCode, BankBranch, BankAcName, BankAcNo, SwiftCode, CreateBy, CreateDt) " +
                    "Values(@AgtCode, @DNo, @BankCode, @BankBranch, @BankAcName, @BankAcNo, @SwiftCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@BankBranch", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@BankAcName", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@BankAcNo", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@SwiftCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAgentShipAddress(string AgtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAgentShipAddress(AgtCode, DNo, Name, Address, CntCode, CityCode, PostalCd, Phone, Fax, Email, Mobile, Remark, CreateBy, CreateDt) " +
                    "Values(@AgtCode, @DNo, @Name, @Address, @CntCode, @CityCode, @PostalCd, @Phone, @Fax, @Email, @Mobile, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@AgtCode", AgtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@Address", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CntCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PostalCd", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@Phone", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Fax", Sm.GetGrdStr(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@Email", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@Mobile", Sm.GetGrdStr(Grd3, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Additional Method

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }
        #endregion

        #endregion

        #region Event

        private void TxtAgtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAgtCode);
        }

        private void TxtAgtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAgtName);
        }

        private void TxtNPWP_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtNPWP);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));           
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPhone);
        }

        private void TxtFax_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFax);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmail);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMobile);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueBankCode_Leave(object sender, EventArgs e)
        {
            if (LueBankCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueBankCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueBankCode);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueBankCode.GetColumnValue("Col2");
                }
                LueBankCode.Visible = false;
            }
        }

        private void LueBankCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueCity2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueCity2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCity2, new Sm.RefreshLue1(Sl.SetLueCityCode));

        }

        private void LueCity2_Leave(object sender, EventArgs e)
        {
            if (LueCity2.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueCity2).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 4].Value =
                    Grd3.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueCity2);
                    Grd3.Cells[fCell.RowIndex, 5].Value = LueCity2.GetColumnValue("Col2");
                }
                LueCity2.Visible = false;
            }
        }

        private void LueCountryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCountryCode, new Sm.RefreshLue1(Sl.SetLueCntCode));
        }

        private void LueCntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCntCode, new Sm.RefreshLue1(Sl.SetLueCntCode));
        }

        private void LueCntCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueCntCode_Leave(object sender, EventArgs e)
        {
            if (LueCntCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueCntCode).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 2].Value =
                    Grd3.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueCntCode);
                    Grd3.Cells[fCell.RowIndex, 3].Value = LueCntCode.GetColumnValue("Col2");
                }
                LueCntCode.Visible = false;
            }
        }

        #endregion

    }
}
