﻿#region Update
/*
    04/08/2017 [TKG] Set PosNo
    20/09/2017 [HAR] ubah form login
    25/06/2018 [TKG] Berdasarkan parameter IsAppNeedToUpdateNewVersion, apakah user perlu update aplikasi ke versi terbaru.
*/ 
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Collections.Generic;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;
using System.Drawing;
using System.Text;
using System.IO;
using System.Reflection;
using System.Configuration;

using Dxe = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Wa = RunSystem.WinAPI;

#endregion

namespace RunSystem
{
    public partial class FrmLoginNew : Form
    {
        #region Field

        private Form mFrmParent;

        #endregion

        #region Constructor

        public FrmLoginNew(Form FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            
        }

        #endregion

        #region Method

        private void GetAppConfigInfo()
        {
            try
            {
                string UserCode = Sm.ReadSetting("UserCode");
                if (UserCode.Length != 0)
                {
                    TxtUserCode.Text = UserCode;
                    ChkKeepUserCode.Checked = true;
                }

                TxtServer.Text = Gv.Server = Sm.ReadSetting("Server");
                TxtDatabase.Text = Gv.Database = Sm.ReadSetting("Database");
                var Port = Sm.ReadSetting("Port")??string.Empty;
                Gv.Port = Port;
                Gv.PosNo = Sm.ReadSetting("PosNo") ?? "1";
                LblCompanyName.Text = Gv.CompanyName;
                LblVersionNo.Text = "Version " + Gv.VersionNo;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetAppConfig()
        {
            try
            {
                Sm.WriteSetting("Server", TxtServer.Text);
                Sm.WriteSetting("Database", TxtDatabase.Text);
                Sm.WriteSetting("UserCode", ChkKeepUserCode.Checked ? TxtUserCode.Text : "");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsUserNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("    Case When ");
	        SQL.AppendLine("        Not Exists( ");
		    SQL.AppendLine("            Select UserCode From TblUser  ");
		    SQL.AppendLine("            Where Upper(UserCode)=@UserCode ");
		    SQL.AppendLine("            And (ExpDt Is Null Or (ExpDt Is Not Null And ExpDt>Replace(CurDate(), '-', ''))) ");
	        SQL.AppendLine("            ) Then 1  ");
            SQL.AppendLine("        Else ");
	        SQL.AppendLine("            Case When  ");
		    SQL.AppendLine("                Not Exists( ");
			SQL.AppendLine("                    Select UserCode From TblUser Where Upper(UserCode)=@UserCode And Pwd=@Pwd ");
            SQL.AppendLine("                    ) Then 2  ");
		    SQL.AppendLine("            Else ");
            SQL.AppendLine("                0 End ");
            SQL.AppendLine("    End As ErrType;");

            var cm = new MySqlCommand
            {
                CommandText =SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text.ToUpper());
            Sm.CmParam<String>(ref cm, "@Pwd", TxtPwd.Text);
            var ErrType = Sm.GetValue(cm);
            
            switch(ErrType)
            {
                case "1":
                    Sm.StdMsg(mMsgType.Warning, "You don't have any access to log-in.");
                    return true;
                case "2":
                    Sm.StdMsg(mMsgType.Warning, "Invalid password.");
                    return true;
            }
            return false;
        }

        private void InsertLog()
        {
            Gv.LogIn = Sm.GetValue("Select Concat(Replace(CurDate(), '-', ''), Replace(CurTime(), ':', ''));");

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLog(UserCode, LogIn, IP, Machine, SysVer, CreateBy, CreateDt) " +
                    "Values(@UserCode, @LogIn, @IP, @Machine, @SysVer, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Login", Gv.LogIn);
            Sm.CmParam<String>(ref cm, "@IP", GetIP());
            Sm.CmParam<String>(ref cm, "@Machine", System.Environment.MachineName);
            Sm.CmParam<String>(ref cm, "@SysVer", Gv.VersionNo);
            Sm.ExecCommand(cm);   
        }

        private string GetIP()
        {
            foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                if (Sm.CompareStr(ip.AddressFamily.ToString(), "InterNetwork")) return ip.ToString();
            return "";
        }

        private void SetParameter()
        {
            Gv.FormatNum0 = "{0:#,##0.00##}";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select ParCode, ParValue From TblParameter " +
                        "Where ParCode In ('CompanyLogoWidth', 'CompanyLogoFile', 'CompanyLogoFile2', 'ImgFileItem', 'FormatNum0', 'AutoLogOffInterval', 'IsSaveAppUsageHistoryActivated') " +
                        "Order By ParCode"
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                while (dr.Read())
                {
                    switch (Sm.DrStr(dr, c[0]))
                    {
                        case "CompanyLogoWidth":
                            Gv.CompanyLogoWidth = Sm.DrStr(dr, c[1]).Length == 0 ? 50 : Int32.Parse(Sm.DrStr(dr, c[1]));
                            break;
                        case "CompanyLogoFile":
                            Gv.CompanyLogoFile = Sm.DrStr(dr, c[1]).Length == 0 ? "XXX" : Sm.DrStr(dr, c[1]);
                            break;
                        case "CompanyLogoFile2":
                            Gv.CompanyLogoFile2 = Sm.DrStr(dr, c[1]).Length == 0 ? "XXX" : Sm.DrStr(dr, c[1]);
                            break;
                        case "ImgFileItem":
                            Gv.ImgFileItem = Sm.DrStr(dr, c[1]).Length == 0 ? "XXX" : Sm.DrStr(dr, c[1]);
                            break;
                        case "FormatNum0":
                            Gv.FormatNum0 = Sm.DrStr(dr, c[1]).Length == 0 ? "{0:#,##0.00##}" : Sm.DrStr(dr, c[1]);
                            break;
                        case "AutoLogOffInterval":
                            Gv.AutoLogOff = Sm.DrStr(dr, c[1]).Length == 0 ? 3600000 : 60000 * decimal.Parse(Sm.DrStr(dr, c[1]));
                            break;
                        case "IsSaveAppUsageHistoryActivated":
                            Gv.IsSaveAppUsageHistoryActivated = Sm.DrStr(dr, c[1]) == "Y";
                            break;
                    }
                }
                dr.Close();
            }
        }

        private void SetPosNo()
        { 
            if (Sm.GetParameterBoo("IsPOSPosNoUseExtFileSetting"))
            {
                var l = new List<string>();
                GetListOfPosNo(ref l);
                foreach (var i in l)
                { 
                    if (Sm.IsFileExist(
                        Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\",
                        string.Concat("RunSystemPosNo", i), 
                        "Txt"))
                    {
                        Gv.PosNo = i;
                        break;
                    }
                }
            }
        }

        private void GetListOfPosNo(ref List<string> l)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var dr = new MySqlCommand()
                {    
                    Connection = cn,
                    CommandTimeout = 600,
                    CommandText = "Select PosNo From TblPosNo Order By PosNo;"
                }.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "PosNo" });
                if (dr.HasRows)
                {
                    while (dr.Read()) l.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private void ShowDBProcessListNotification()
        {
            if (!Sm.GetParameterBoo("IsShowDBProcessListNotification")) return;

            var MinRecordForDBProcessListNotification = Sm.GetParameterDec("MinRecordForDBProcessListNotification");
            var cm = new MySqlCommand() { CommandText = "Select Count(1) From Information_Schema.PROCESSLIST Where db Is Not Null;" };
            var Total = Sm.GetValue(cm);
            if (Total.Length > 0)
            {
                if (decimal.Parse(Total) > MinRecordForDBProcessListNotification)
                    Sm.StdMsg(mMsgType.Info, "DB server process : " + Total + " records.");
            }
        }

        #endregion

        #region Event

        #region Form Event

        private void FrmLogin_Load(object sender, EventArgs e)
        {
          panel1.Left = (this.Width - panel1.Size.Width) / 2 + (this.Left / 2);
           panel1.Top = (this.Height - panel1.Size.Height) / 2 + (this.Top / 2);

           panel3.Left = 320;
           panel3.Top = (this.Height - panel3.Size.Height) / 2 + (this.Top / 2);
            GetAppConfigInfo();
            //TxtUserCode.Text = "TKG";
            //TxtPwd.Text = "7";
            //BtnLogin_Click(sender, e);
                        
            TxtUserCode.Focus();
        }

        #endregion

        #region Misc Control Event

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            TxtUserCode.Text = TxtUserCode.Text.Trim();
        }

        private void TxtPwd_Validated(object sender, EventArgs e)
        {
            TxtPwd.Text = TxtPwd.Text.Trim();
        }

        private void TxtDatabase_Validated(object sender, EventArgs e)
        {
            TxtDatabase.Text = TxtDatabase.Text.Trim();
        }

        private void TxtServer_Validated(object sender, EventArgs e)
        {
            TxtServer.Text = TxtServer.Text.Trim();
        }

        private void TxtUserCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TxtPwd.Focus();
            }
        }

        private void TxtPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BtnLogin.Focus();
            }
        }

        private void TxtDatabase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TxtServer.Focus();
            }
        }

        private void TxtServer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TxtUserCode.Focus();
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (TxtDatabase.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Database is empty.");
                TxtDatabase.Focus();
                return;
            }
            else if (TxtServer.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Server is empty.");
                TxtServer.Focus();
                return;
            }
            else if (TxtUserCode.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "User ID is empty.");
                TxtUserCode.Focus();
                return;
            }
            else if (TxtPwd.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Password is empty.");
                TxtPwd.Focus();
                return;
            }

            try
            {
                Gv.Server = TxtServer.Text;
                Gv.Database = TxtDatabase.Text;
                Gv.AnimateForm = 1;
                ConnectDB();

                if (IsLoginInvalid() || IsAppVersionInvalid()) return;

                if (!IsUserNotValid())
                {
                    Gv.ApplicationPath = Application.StartupPath;
                    Gv.CurrentUserCode = SetUserCode(TxtUserCode.Text.ToUpper());
                    SetParameter();
                    SetPosNo();
                    ShowWarningLogin();
                    InsertLog();
                    SetAppConfig();
                    ShowDBProcessListNotification();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ConnectDB()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            { cn.Open(); }
        }

        private string SetUserCode(string UserCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select UserCode From TblUser Where Upper(UserCode)=@UserCode;"
            };
            Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
            return Sm.GetValue(cm);   
        }

        private void ShowWarningLogin()
        {
            var WarningLoginHr = Sm.GetParameter("WarningLoginHr");

            if ((WarningLoginHr.Length == 0 ? "0" : WarningLoginHr)!="0")
            {
                var WarningLogin = GetWarningLogin(WarningLoginHr);
                if (WarningLogin.Length != 0)
                {
                    Sm.StdMsg(mMsgType.Info,
                        "You have not closed the application properly on " + WarningLogin);
                }
            }
        }

        private string GetWarningLogin(string WarningLoginHr)
        {
            string WarningLogin = string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Concat(Machine, ' (', IP, ')') As Place "); 
            SQL.AppendLine("From TblLog  ");
            SQL.AppendLine("Where UserCode=@UserCode  ");
            SQL.AppendLine("And Logout is Null ");
            SQL.AppendLine("And Login Between  ");
            SQL.AppendLine("Date_Format(DATE_ADD(STR_TO_DATE(CurrentDateTime(), '%Y%m%d%H%i'),INTERVAL -" + WarningLoginHr + " HOUR), '%Y%m%d%H%i') ");
            SQL.AppendLine("And CurrentDateTime() ");
            SQL.AppendLine("Order By Login Desc ");
            SQL.AppendLine("Limit 10; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "Place" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        WarningLogin += Environment.NewLine + "- " + Sm.DrStr(dr, 0);
                }
                dr.Close();
            }

            return WarningLogin;
        }

        private bool IsLoginInvalid()
        {
            if (Sm.GetParameterBoo("IsLoginInvalid"))
            {
                Sm.StdMsg(mMsgType.Info, "Under maintenance.");
                return true;
            }
            return false;
        }

        private bool IsAppVersionInvalid()
        {
            if (Sm.GetParameterBoo("IsAppNeedToUpdateNewVersion"))
            {
                var NewVersion = Sm.GetParameter("RunSystemDownloadVersion");
                var CurrentVersion = Gv.VersionNo;

                if (NewVersion.Length > 0 && CurrentVersion.Length > 0 &&
                    decimal.Parse(CurrentVersion.Replace(".", "")) < decimal.Parse(NewVersion.Replace(".", "")))
                {
                    Sm.StdMsg(mMsgType.Info,
                        "Your application is out of date." + Environment.NewLine + 
                        "You need to update your application." + Environment.NewLine + 
                        "Please contact system administrator if you need any help.");
                    return true;
                }
            }
            return false;
        }

        private void BtnSetting_Click(object sender, EventArgs e)
        {
            bool x = PnlSetting.Visible;
            if (x)
            {
                PnlSetting.Visible = false;
            }
            else
            {
                PnlSetting.Visible = true;
            }
        }

       
        #endregion


        #endregion
    }
}
