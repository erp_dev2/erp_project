﻿#region Update
/*
    22/09/2017 [HAR] tambah info site dan total
    04/10/2017 [TKG] bug fixing perhitungan total 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyAPOld : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMonthlyAPOld(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueVdCode(ref LueVdCode);

                string CurrentDate = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueYr, Sm.Left(CurrentDate, 4));
                Sm.SetLue(LueMth, CurrentDate.Substring(4, 2));
                
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Process1(ref List<Vendor> l, string VdCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.VdCode, A.VdName, B.CurCode From TblVendor A, TblCurrency B ");
            if (VdCode.Length > 0)
                SQL.AppendLine("Where A.VdCode=@VdCode ");
            SQL.AppendLine("Order By A.VdName, B.CurCode;");

            if (VdCode.Length > 0)
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "VdName", "CurCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Vendor()
                        {
                            VdCode = Sm.DrStr(dr, c[0]),
                            VdName = Sm.DrStr(dr, c[1]),
                            CurCode = Sm.DrStr(dr, c[2]),
                            Outstanding = 0m,
                            Paid = 0m,
                            Balance = 0m, 
                            SiteName = string.Empty,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Vendor> l, string VdCode, string Dt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Vendor= string.Empty, Currency = string.Empty, siteName = string.Empty;

            SQL.AppendLine("Select A.VdCode, F.CurCode, ");
            SQL.AppendLine("(((B.Qty*E.UPrice) * ((100.00-C.Discount)*0.01)) - C.DiscountAmt + C.RoundingValue) As Amt, H.SiteName ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B "); 
	        SQL.AppendLine("    On A.DocNo=B.DocNo ");
	        SQL.AppendLine("    And B.CancelInd='N' "); 
	        SQL.AppendLine("    And IfNull(B.Status, 'O') In ('O', 'A') "); 
	        SQL.AppendLine("    And Not Exists( ");
		    SQL.AppendLine("        Select 1 ");
		    SQL.AppendLine("        From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl T2 ");
		    SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
		    SQL.AppendLine("        And T1.VdCode=A.VdCode ");
		    SQL.AppendLine("        And T2.RecvVdDocNo=B.DocNo ");
		    SQL.AppendLine("        And T2.RecvVdDNo=B.DNo ");
            SQL.AppendLine("        And T1.DocDt<@Dt ");
		    SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblPODtl C On B.PODocNo=C.DocNo And B.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtDtl E On D.QtDocNo=E.DocNo And D.QtDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr F On D.QtDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblPOHdr G On B.PoDocNo=G.DocNo  ");
            SQL.AppendLine("Left Join TblSite H On G.SiteCode=H.SiteCode ");
            SQL.AppendLine("Where A.DocDt<@Dt ");
            if (VdCode.Length > 0)
                SQL.AppendLine("And A.VdCode=@VdCode ");
            SQL.AppendLine("Order By A.VdCode, F.CurCode;");

            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "CurCode", "Amt", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Vendor = Sm.DrStr(dr, c[0]);
                        Currency = Sm.DrStr(dr, c[1]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].VdCode, Vendor) == 0 &&
                                string.Compare(l[i].CurCode, Currency) == 0)
                            {
                                l[i].Outstanding += Sm.DrDec(dr, c[2]);
                                l[i].SiteName = Sm.DrStr(dr, c[3]);
                            }
                        }
                        
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Vendor> l, string VdCode, string Dt, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Vendor = string.Empty, Currency = string.Empty;

            SQL.AppendLine("Select T1.VdCode, T1.CurCode, ");
            SQL.AppendLine("(T1.Amt+T1.TaxAmt-T1.Downpayment)-IfNull(T2.Amt, 0)-IfNull(T3.Amt, 0) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select C.InvoiceDocNo As DocNo, Sum(C.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr D On C.InvoiceDocNo=D.DocNo And D.DocDt<@Dt2 ");
            if (VdCode.Length > 0) SQL.AppendLine("        And D.VdCode=@VdCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            SQL.AppendLine("    Group By C.InvoiceDocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblAPSHdr T3 On T1.DocNo=T3.PurchaseInvoiceDocNo And T3.DocDt<@Dt2 And T3.CancelInd='N' ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.DocDt<@Dt2 ");
            if (VdCode.Length > 0) SQL.AppendLine(" And T1.VdCode=@VdCode ");
            SQL.AppendLine("And (T1.Amt+T1.TaxAmt-T1.Downpayment-IfNull(T2.Amt, 0)-IfNull(T3.Amt, 0))>0;");

            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
            if (VdCode.Length > 0)
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Vendor = Sm.DrStr(dr, c[0]);
                        Currency = Sm.DrStr(dr, c[1]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].VdCode, Vendor) == 0 &&
                                string.Compare(l[i].CurCode, Currency) == 0)
                            {
                                l[i].Outstanding += Sm.DrDec(dr, c[2]);
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Vendor> l, string VdCode, string YrMth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Vendor = string.Empty, Currency = string.Empty;

            SQL.AppendLine("Select B.VdCode, B.CurCode, B.Amt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            if (VdCode.Length > 0) SQL.AppendLine("    And B.VdCode=@VdCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth; ");

            Sm.CmParam<String>(ref cm, "@YrMth", YrMth);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Vendor = Sm.DrStr(dr, c[0]);
                        Currency = Sm.DrStr(dr, c[1]);
                        
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].VdCode, Vendor) == 0 &&
                                string.Compare(l[i].CurCode, Currency) == 0)
                                l[i].Paid = l[i].Paid + Sm.DrDec(dr, c[2]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Vendor's Code",
                        "Vendor's Name",
                        "Currency",
                        "Outstanding",
                        "Paid",

                        //6
                        "Balance",
                        "Site"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 250, 80, 150, 150,
                        
                        //6
                        150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Rows.AutoHeight();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            decimal Pd = 0, Bal = 0, Outs = 0;
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lVendor = new List<Vendor>();

            var cm = new MySqlCommand();

            var VdCode = Sm.GetLue(LueVdCode);
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var Yr2 = Yr;
            var Mth2 = Mth;

            if (Mth == "12")
            {
                Yr2 = Yr + 1;
                Mth2 = "01";
            }
            else
                Mth2 = Sm.Right("0"+(decimal.Parse(Mth)+1).ToString(), 2);
            
            try
            {
                Process1(ref lVendor, VdCode);
                Process2(ref lVendor, VdCode, Yr2 + Mth2 + "01");
                Process3(ref lVendor, VdCode, Yr + Mth + "01", Yr2 + Mth2 + "01");
                Process4(ref lVendor, VdCode, Yr+Mth);

                for (int i = lVendor.Count-1; i>=0; i--)
                {
                    if (lVendor[i].Outstanding == 0 && lVendor[i].Paid == 0)
                        lVendor.Remove(lVendor[i]);
                    else
                        lVendor[i].Balance = lVendor[i].Outstanding - lVendor[i].Paid;
                }
                int Row = 0;
                Grd1.GroupObject.Clear();
                Grd1.Group();
                Grd1.BeginUpdate();
                Grd1.Rows.Count = 0;
                foreach (var i in lVendor.OrderBy(x => x.VdName).ThenBy(x => x.CurCode))
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Grd1.Cells[Row, 1].Value = i.VdCode;
                    Grd1.Cells[Row, 2].Value = i.VdName;
                    Grd1.Cells[Row, 3].Value = i.CurCode;
                    Grd1.Cells[Row, 4].Value = i.Outstanding;
                    Grd1.Cells[Row, 5].Value = i.Paid;
                    Grd1.Cells[Row, 6].Value = i.Balance;
                    Grd1.Cells[Row, 7].Value = i.SiteName;
                    Pd += i.Paid;
                    Bal += i.Balance;
                    Outs += i.Outstanding;
                    Row++;
                }

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6 });

                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lVendor.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

       

        #endregion

        #region Class

        private class Vendor
        {
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string CurCode { get; set; }
            public decimal Outstanding { get; set; }
            public decimal Paid { get; set; }
            public decimal Balance { get; set; }
            public string SiteName { get; set; }
        }

        #endregion
    }
}
