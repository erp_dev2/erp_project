﻿#region Update
/*
    12/09/2019 [TKG/IMS] tambah filter dan informasi item category
    19/09/2019 [TKG/IMS] tambah informasi dimensions
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
    07/11/2019 [WED/IMS] Column freeze ditambah
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBomDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBom mFrmParent;
        private string mSQL = string.Empty, mDocNoWC="" ;

        #endregion

        #region Constructor

        public FrmBomDlg2(FrmBom FrmParent, string DocNoWC)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNoWC = DocNoWC;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueOption(ref LueDocType, "BomDocType");
                Sl.SetLueItCtCode(ref LueItCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Type",
                    "",
                    "Document#",
                    "Name",
                    
                    //6-10
                    "Source",
                    "Capacity",
                    "UoM",
                    "Old Code",
                    "Item's Category",

                    //11-13
                    "Dimensions",
                    "Specification",
                    "Local Code"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 150, 20, 120, 200, 
                    
                    //6-10
                    150, 130, 100, 100, 180,
                
                    //11-13
                    300, 100, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new[] { 7 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 4, 5, 6, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 7, 8 });
            if (!mFrmParent.mIsBOMShowDimensions) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 12, 13 });
            Grd1.Cols[9].Move(6);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.DocNo, A.DocName, B.OptDesc, A.Capacity, A.Uom, A.OldCode, A.ItCtName, A.Dimensions, A.Specification, A.ItCodeInternal ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Cast('3' As char) As DocType, EmpCode As DocNo, EmpName As DocName, ");
            SQL.AppendLine("    Cast('1' As char) As Capacity, Null As Uom, EmpCodeOld As OldCode, ");
            SQL.AppendLine("    Null As ItCtCode, Null As ItCtName, Null As Dimensions, Null as Specification, Null As ItCodeInternal ");
            SQL.AppendLine("    From TblEmployee ");
            SQL.AppendLine("	Where ResignDt is null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Cast('2' As char), DocNo, DocName, Cast('1' As char), Null As Uom, Null As OldCode, ");
            SQL.AppendLine("    Null As ItCtCode, Null As ItCtName, Null As Dimensions, Null as Specification, Null As ItCodeInternal ");
            SQL.AppendLine("    From TblFormulaHdr  ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Cast('1' As Char), T1.ItCode, T1.ItName, Null As Capacity, T1.PlanningUomCode2 As UoM, Null As OldCode, ");
            SQL.AppendLine("    T2.ItCtCode, T2.ItCtName, ");
            if (mFrmParent.mIsBOMShowDimensions)
            {
                SQL.AppendLine("    Trim(Concat( ");
                SQL.AppendLine("    Case When IfNull(T1.Length, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Length : ', Trim(Concat(Convert(Format(IfNull(T1.Length, 0.00), 2) Using utf8), ' ', IfNull(T1.LengthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("    Case When IfNull(T1.Height, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Height : ', Trim(Concat(Convert(Format(IfNull(T1.Height, 0.00), 2) Using utf8), ' ', IfNull(T1.HeightUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("    Case When IfNull(T1.Width, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Width : ', Trim(Concat(Convert(Format(IfNull(T1.Width, 0.00), 2) Using utf8), ' ', IfNull(T1.WidthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End ");
                SQL.AppendLine("    )) As Dimensions ");
            }
            else
                SQL.AppendLine("    Null As Dimensions ");
            SQL.AppendLine(", T1.Specification, T1.ItCodeInternal ");
            SQL.AppendLine("    From TblItem T1 ");
            SQL.AppendLine("    Left Join TblItemCategory T2 On T1.ItCtCode=T2.ItCtCode ");
            SQL.AppendLine("    Where T1.PlanningItemInd = 'Y' And T1.ActInd = 'Y' ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblOption B On B.OptCat='BomDocType' And A.DocType=B.OptCode ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where Concat(A.DocType, A.DocNo) Not In (" + mFrmParent.GetSelectedDocument() + ") ";
                

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNoWC);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.DocName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "A.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocType, A.DocNo;",
                    new string[]
                    {
                        //0
                        "DocType",

                        //1-5
                        "DocNo", 
                        "DocName",
                        "OptDesc",
                        "Capacity",
                        "UoM",

                        //6-10
                        "OldCode",
                        "ItCtName",
                        "Dimensions",
                        "Specification",
                        "ItCodeInternal"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocumentAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6, 10 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsDocumentAlreadyChosen(int Row)
        {
            var key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 4); 
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 1)+Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), 
                    key
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "1"))
                {
                    var f1 = new FrmItem("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "2"))
                {
                    var f2 = new FrmFormula("***");
                    f2.Tag = "***";
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "3"))
                {
                    var f3 = new FrmEmployee("***");
                    f3.Tag = "***";
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f3.ShowDialog();
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "1"))
                {
                    var f1 = new FrmItem("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "2"))
                {
                    var f2 = new FrmFormula("***");
                    f2.Tag = "***";
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 2), "3"))
                {
                    var f3 = new FrmEmployee("***");
                    f3.Tag = "***";
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f3.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueOption), "BomDocType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document's type");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }


        #endregion
    }
}
