﻿#region Update
/*
    20/06/2019 [TKG] berdasarkan parameter IsSalaryAdjustmentInclResignee, resignee ikut diproses atau tidak.
    16/06/2020 [TKG/HIN] tambah indikator Nontaxable berdasarkan parameter IsSalaryAdjustmentNontaxableEnabled
    16/02/2022 [TKG/GSS] Merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalaryAdjustment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool 
            mIsNotFilterByAuthorization = false, 
            mIsSalaryAdjustmentInclResignee = false,
            mIsSalaryAdjustmentNontaxableEnabled = false;
        internal FrmSalaryAdjustmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmSalaryAdjustment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Salary Adjustment";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                ChkNontaxable.Visible = mIsSalaryAdjustmentNontaxableEnabled;
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "DNo", "Description", "Amount [+/-]" },
                    new int[]{ 0, 300, 150 }
                );
            Sm.GrdFormatDec(Grd1, new int[]{ 2 }, 0 );
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, ChkCancelInd, ChkNontaxable, DtePaidDt, MeeRemark }, true);
                    BtnEmpCode.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, ChkNontaxable, DtePaidDt, MeeRemark }, false);
                    BtnEmpCode.Enabled = true;
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtEmpCode, TxtEmpName, TxtEmpCodeOld,   
                TxtPosCode, TxtDeptCode, DtePaidDt, TxtPayrunCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ChkNontaxable.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Grd1.Cells[0, 2].Value = 0m;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalaryAdjustmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDeptCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = 0;
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1 }, e);
            GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2 }, e);
            if (e.ColIndex == 2) ComputeAmt();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalaryAdjustment", "TblSalaryAdjustmentHdr");

            var cml = new List<MySqlCommand>();
            
            cml.Add(SaveSalaryAdjustmentHdr(DocNo));
            cml.Add(SaveSalaryAdjustmentDtl(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
            //        cml.Add(SaveSalaryAdjustmentDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee's code", false) ||
                Sm.IsDteEmpty(DtePaidDt, "Payment date") ||
                IsGrdEmpty()||
                IsGrdValueNotValid();;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Description is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Description : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine + "Amount is 0.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveSalaryAdjustmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Salary Adjustment (Hdr) */ ");
            SQL.AppendLine("Insert Into TblSalaryAdjustmentHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Nontaxable, EmpCode, Amt, PaidDt, PayrunCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @Nontaxable, @EmpCode, @Amt, @PaidDt, Null, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Nontaxable", ChkNontaxable.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParamDt(ref cm, "@PaidDt", Sm.GetDte(DtePaidDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalaryAdjustmentDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Salary Adjustment (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalaryAdjustmentDtl(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @Description_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Description_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 2));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveSalaryAdjustmentDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSalaryAdjustmentDtl(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @Description, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            try
            {
                if (IsEditedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblSalaryAdjustmentHdr Set ");
                SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsDataNotCancelled() ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var cm = new MySqlCommand();
            cm.CommandText = "Select DocNo From TblSalaryAdjustmentHdr Where DocNo=@DocNo And CancelInd='Y';";
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalaryAdjustmentHdr(DocNo);
                ShowSalaryAdjustmentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalaryAdjustmentHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.Amt, A.PaidDt, A.PayrunCode, A.Nontaxable, A.Remark ");
            SQL.AppendLine("From TblSalaryAdjustmentHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "EmpCode", "EmpName", "EmpCodeOld", 
                        
                        //6-10
                        "PosName", "DeptName", "Amt", "PaidDt", "PayrunCode", 
                        
                        //11-12
                        "Nontaxable", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        Sm.SetDte(DtePaidDt, Sm.DrStr(dr, c[9]));
                        TxtPayrunCode.EditValue = Sm.DrStr(dr, c[10]);
                        ChkNontaxable.Checked = Sm.DrStr(dr, c[11]) == "Y";
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
        }

        private void ShowSalaryAdjustmentDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, Description, Amt ");
            SQL.AppendLine("From TblSalaryAdjustmentDtl Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { "DNo", "Description", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
                );
            Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = 0m;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            //mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsPayrollDataFilterByAuthorization', 'IsSalaryAdjustmentInclResignee', 'IsSalaryAdjustmentNontaxableEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSalaryAdjustmentInclResignee": mIsSalaryAdjustmentInclResignee = ParValue == "Y"; break;
                            case "IsSalaryAdjustmentNontaxableEnabled": mIsSalaryAdjustmentNontaxableEnabled = ParValue == "Y"; break;
                            case "IsPayrollDataFilterByAuthorization": mIsNotFilterByAuthorization = ParValue == "N"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Amt += Sm.GetGrdDec(Grd1, Row, 2);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void GrdAfterCommitEditSetZeroIfEmpty(iGrid Grd, int[] ColIndex, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(ColIndex, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd, e.RowIndex, e.ColIndex).Length == 0)
                    Grd.Cells[e.RowIndex, e.ColIndex].Value = 0m;
            }
        }

        #endregion

        #endregion

        #region Misc control Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalaryAdjustmentDlg(this));
        }

        #endregion
    }
}
