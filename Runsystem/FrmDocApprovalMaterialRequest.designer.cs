﻿namespace RunSystem
{
    partial class FrmDocApprovalMaterialRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPurchaseUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtQty = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtReqType = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtInStock = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtOutstanding = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtOrdered = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtMinStock = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtReorderPoint = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtMth06 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth03 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth01 = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtMth12 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth09 = new DevExpress.XtraEditors.TextEdit();
            this.PicItem = new System.Windows.Forms.PictureBox();
            this.TxtItCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtSpecification = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPurchaseUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstanding.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOrdered.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReorderPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth06.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth03.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth01.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth09.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 369);
            this.panel3.Size = new System.Drawing.Size(842, 143);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 143);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtSpecification);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.TxtItCodeInternal);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.PicItem);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.TxtMth12);
            this.panel2.Controls.Add(this.TxtMth09);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.TxtMth06);
            this.panel2.Controls.Add(this.TxtMth03);
            this.panel2.Controls.Add(this.TxtMth01);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtReorderPoint);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.TxtMinStock);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtOrdered);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtOutstanding);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtInStock);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtReqType);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.DteUsageDt);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtPurchaseUomCode);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtDeptCode);
            this.panel2.Controls.Add(this.TxtQty);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(842, 369);
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(113, 316);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.ReadOnly = true;
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(109, 20);
            this.DteUsageDt.TabIndex = 50;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(41, 318);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 14);
            this.label10.TabIndex = 49;
            this.label10.Text = "Usage Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPurchaseUomCode
            // 
            this.TxtPurchaseUomCode.EnterMoveNextControl = true;
            this.TxtPurchaseUomCode.Location = new System.Drawing.Point(226, 184);
            this.TxtPurchaseUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPurchaseUomCode.Name = "TxtPurchaseUomCode";
            this.TxtPurchaseUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtPurchaseUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPurchaseUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPurchaseUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPurchaseUomCode.Properties.MaxLength = 16;
            this.TxtPurchaseUomCode.Properties.ReadOnly = true;
            this.TxtPurchaseUomCode.Size = new System.Drawing.Size(99, 20);
            this.TxtPurchaseUomCode.TabIndex = 29;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(113, 118);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 16;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(379, 20);
            this.TxtItName.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(43, 121);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Item Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(113, 96);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(212, 20);
            this.TxtItCode.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(46, 99);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Item Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(113, 52);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 16;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(379, 20);
            this.TxtDeptCode.TabIndex = 16;
            // 
            // TxtQty
            // 
            this.TxtQty.EnterMoveNextControl = true;
            this.TxtQty.Location = new System.Drawing.Point(113, 184);
            this.TxtQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty.Properties.Appearance.Options.UseFont = true;
            this.TxtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty.Properties.ReadOnly = true;
            this.TxtQty.Size = new System.Drawing.Size(109, 20);
            this.TxtQty.TabIndex = 28;
            this.TxtQty.Validated += new System.EventHandler(this.TxtQty_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(57, 187);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 14);
            this.label11.TabIndex = 27;
            this.label11.Text = "Quantity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(113, 339);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ReadOnly = true;
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(644, 22);
            this.MeeRemark.TabIndex = 54;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(64, 342);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 53;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(38, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(113, 30);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.ReadOnly = true;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(78, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(113, 8);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(212, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(38, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtReqType
            // 
            this.TxtReqType.EnterMoveNextControl = true;
            this.TxtReqType.Location = new System.Drawing.Point(113, 74);
            this.TxtReqType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReqType.Name = "TxtReqType";
            this.TxtReqType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReqType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReqType.Properties.Appearance.Options.UseFont = true;
            this.TxtReqType.Properties.MaxLength = 16;
            this.TxtReqType.Properties.ReadOnly = true;
            this.TxtReqType.Size = new System.Drawing.Size(379, 20);
            this.TxtReqType.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(27, 77);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 14);
            this.label12.TabIndex = 17;
            this.label12.Text = "Request Type";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInStock
            // 
            this.TxtInStock.EnterMoveNextControl = true;
            this.TxtInStock.Location = new System.Drawing.Point(113, 206);
            this.TxtInStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInStock.Name = "TxtInStock";
            this.TxtInStock.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtInStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInStock.Properties.Appearance.Options.UseFont = true;
            this.TxtInStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInStock.Properties.ReadOnly = true;
            this.TxtInStock.Size = new System.Drawing.Size(109, 20);
            this.TxtInStock.TabIndex = 31;
            this.TxtInStock.Validated += new System.EventHandler(this.TxtInStock_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(58, 209);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "In Stock";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOutstanding
            // 
            this.TxtOutstanding.EnterMoveNextControl = true;
            this.TxtOutstanding.Location = new System.Drawing.Point(113, 228);
            this.TxtOutstanding.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstanding.Name = "TxtOutstanding";
            this.TxtOutstanding.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtOutstanding.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstanding.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstanding.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstanding.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstanding.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstanding.Properties.ReadOnly = true;
            this.TxtOutstanding.Size = new System.Drawing.Size(109, 20);
            this.TxtOutstanding.TabIndex = 34;
            this.TxtOutstanding.Validated += new System.EventHandler(this.TxtOutstanding_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(45, 231);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 14);
            this.label8.TabIndex = 33;
            this.label8.Text = "Requested";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOrdered
            // 
            this.TxtOrdered.EnterMoveNextControl = true;
            this.TxtOrdered.Location = new System.Drawing.Point(113, 250);
            this.TxtOrdered.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOrdered.Name = "TxtOrdered";
            this.TxtOrdered.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtOrdered.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOrdered.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOrdered.Properties.Appearance.Options.UseFont = true;
            this.TxtOrdered.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOrdered.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOrdered.Properties.ReadOnly = true;
            this.TxtOrdered.Size = new System.Drawing.Size(109, 20);
            this.TxtOrdered.TabIndex = 38;
            this.TxtOrdered.Validated += new System.EventHandler(this.TxtOrdered_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(29, 253);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 14);
            this.label9.TabIndex = 37;
            this.label9.Text = "Ordered (PO)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMinStock
            // 
            this.TxtMinStock.EnterMoveNextControl = true;
            this.TxtMinStock.Location = new System.Drawing.Point(113, 272);
            this.TxtMinStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinStock.Name = "TxtMinStock";
            this.TxtMinStock.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMinStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinStock.Properties.Appearance.Options.UseFont = true;
            this.TxtMinStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinStock.Properties.ReadOnly = true;
            this.TxtMinStock.Size = new System.Drawing.Size(109, 20);
            this.TxtMinStock.TabIndex = 42;
            this.TxtMinStock.Validated += new System.EventHandler(this.TxtMinStock_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(22, 275);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 14);
            this.label13.TabIndex = 41;
            this.label13.Text = "Minimum Stock";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtReorderPoint
            // 
            this.TxtReorderPoint.EnterMoveNextControl = true;
            this.TxtReorderPoint.Location = new System.Drawing.Point(113, 294);
            this.TxtReorderPoint.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReorderPoint.Name = "TxtReorderPoint";
            this.TxtReorderPoint.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtReorderPoint.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReorderPoint.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReorderPoint.Properties.Appearance.Options.UseFont = true;
            this.TxtReorderPoint.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtReorderPoint.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtReorderPoint.Properties.ReadOnly = true;
            this.TxtReorderPoint.Size = new System.Drawing.Size(109, 20);
            this.TxtReorderPoint.TabIndex = 46;
            this.TxtReorderPoint.Validated += new System.EventHandler(this.TxtReorderPoint_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(29, 297);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 14);
            this.label14.TabIndex = 45;
            this.label14.Text = "Reorder Point";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMth06
            // 
            this.TxtMth06.EnterMoveNextControl = true;
            this.TxtMth06.Location = new System.Drawing.Point(384, 272);
            this.TxtMth06.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth06.Name = "TxtMth06";
            this.TxtMth06.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth06.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth06.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth06.Properties.Appearance.Options.UseFont = true;
            this.TxtMth06.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth06.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth06.Properties.ReadOnly = true;
            this.TxtMth06.Size = new System.Drawing.Size(109, 20);
            this.TxtMth06.TabIndex = 44;
            this.TxtMth06.Validated += new System.EventHandler(this.TxtMth06_Validated);
            // 
            // TxtMth03
            // 
            this.TxtMth03.EnterMoveNextControl = true;
            this.TxtMth03.Location = new System.Drawing.Point(384, 250);
            this.TxtMth03.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth03.Name = "TxtMth03";
            this.TxtMth03.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth03.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth03.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth03.Properties.Appearance.Options.UseFont = true;
            this.TxtMth03.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth03.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth03.Properties.ReadOnly = true;
            this.TxtMth03.Size = new System.Drawing.Size(109, 20);
            this.TxtMth03.TabIndex = 40;
            this.TxtMth03.Validated += new System.EventHandler(this.TxtMth03_Validated);
            // 
            // TxtMth01
            // 
            this.TxtMth01.EnterMoveNextControl = true;
            this.TxtMth01.Location = new System.Drawing.Point(384, 228);
            this.TxtMth01.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth01.Name = "TxtMth01";
            this.TxtMth01.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth01.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth01.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth01.Properties.Appearance.Options.UseFont = true;
            this.TxtMth01.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth01.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth01.Properties.ReadOnly = true;
            this.TxtMth01.Size = new System.Drawing.Size(109, 20);
            this.TxtMth01.TabIndex = 36;
            this.TxtMth01.Validated += new System.EventHandler(this.TxtMth01_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(264, 209);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 14);
            this.label17.TabIndex = 32;
            this.label17.Text = "Item Consumption";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(295, 232);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 14);
            this.label18.TabIndex = 35;
            this.label18.Text = "1 Last Month";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(295, 254);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 14);
            this.label15.TabIndex = 39;
            this.label15.Text = "3 Last Month";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(295, 275);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 14);
            this.label16.TabIndex = 43;
            this.label16.Text = "6 Last Month";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(288, 319);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(86, 14);
            this.label19.TabIndex = 51;
            this.label19.Text = "12 Last Month";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(295, 298);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 14);
            this.label20.TabIndex = 47;
            this.label20.Text = "9 Last Month";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMth12
            // 
            this.TxtMth12.EnterMoveNextControl = true;
            this.TxtMth12.Location = new System.Drawing.Point(384, 316);
            this.TxtMth12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth12.Name = "TxtMth12";
            this.TxtMth12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth12.Properties.Appearance.Options.UseFont = true;
            this.TxtMth12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth12.Properties.ReadOnly = true;
            this.TxtMth12.Size = new System.Drawing.Size(109, 20);
            this.TxtMth12.TabIndex = 52;
            this.TxtMth12.Validated += new System.EventHandler(this.TxtMth12_Validated);
            // 
            // TxtMth09
            // 
            this.TxtMth09.EnterMoveNextControl = true;
            this.TxtMth09.Location = new System.Drawing.Point(384, 294);
            this.TxtMth09.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth09.Name = "TxtMth09";
            this.TxtMth09.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth09.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth09.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth09.Properties.Appearance.Options.UseFont = true;
            this.TxtMth09.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth09.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth09.Properties.ReadOnly = true;
            this.TxtMth09.Size = new System.Drawing.Size(109, 20);
            this.TxtMth09.TabIndex = 48;
            this.TxtMth09.Validated += new System.EventHandler(this.TxtMth09_Validated);
            // 
            // PicItem
            // 
            this.PicItem.Location = new System.Drawing.Point(545, 6);
            this.PicItem.Name = "PicItem";
            this.PicItem.Size = new System.Drawing.Size(290, 328);
            this.PicItem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicItem.TabIndex = 91;
            this.PicItem.TabStop = false;
            // 
            // TxtItCodeInternal
            // 
            this.TxtItCodeInternal.EnterMoveNextControl = true;
            this.TxtItCodeInternal.Location = new System.Drawing.Point(113, 140);
            this.TxtItCodeInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCodeInternal.Name = "TxtItCodeInternal";
            this.TxtItCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtItCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtItCodeInternal.Properties.MaxLength = 16;
            this.TxtItCodeInternal.Properties.ReadOnly = true;
            this.TxtItCodeInternal.Size = new System.Drawing.Size(379, 20);
            this.TxtItCodeInternal.TabIndex = 24;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(7, 143);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(104, 14);
            this.label21.TabIndex = 23;
            this.label21.Text = "Item\'s Local Code";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSpecification
            // 
            this.TxtSpecification.EnterMoveNextControl = true;
            this.TxtSpecification.Location = new System.Drawing.Point(113, 162);
            this.TxtSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSpecification.Name = "TxtSpecification";
            this.TxtSpecification.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpecification.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpecification.Properties.Appearance.Options.UseFont = true;
            this.TxtSpecification.Properties.MaxLength = 16;
            this.TxtSpecification.Properties.ReadOnly = true;
            this.TxtSpecification.Size = new System.Drawing.Size(379, 20);
            this.TxtSpecification.TabIndex = 26;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(36, 165);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 14);
            this.label22.TabIndex = 25;
            this.label22.Text = "Specification";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmDocApprovalMaterialRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 512);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmDocApprovalMaterialRequest";
            this.Text = "Material Request Information";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPurchaseUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstanding.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOrdered.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReorderPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth06.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth03.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth01.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth09.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtPurchaseUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        internal DevExpress.XtraEditors.TextEdit TxtQty;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtReqType;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtOrdered;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtOutstanding;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtInStock;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtReorderPoint;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtMinStock;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtMth06;
        internal DevExpress.XtraEditors.TextEdit TxtMth03;
        internal DevExpress.XtraEditors.TextEdit TxtMth01;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtMth12;
        internal DevExpress.XtraEditors.TextEdit TxtMth09;
        private System.Windows.Forms.PictureBox PicItem;
        internal DevExpress.XtraEditors.TextEdit TxtItCodeInternal;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtSpecification;
        private System.Windows.Forms.Label label22;
    }
}