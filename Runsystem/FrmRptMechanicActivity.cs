﻿#region Update
/*
    24/08/2017 [HAR] bug saat expand data paling bawah (grid nethod)
    01/08/2017 [Ari], tambah kolom TO, TO Name, Location
    17/06/2022 [BRI/IOK] tambah filter + unhide 3 kolom
    17/06/2022 [BRI/IOK] rubah source activity dan tambah maintenance type bedasarkan param IsWorkOrderDetailEditable
    21/06/2022 [BRI/PRODUCT] Menambahkan dialog work order di loop mechanic activity
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMechanicActivity : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool
            mIsFilterBySite = false,
            mIsWorkOrderDetailEditable = false;

        #endregion

        #region Constructor

        public FrmRptMechanicActivity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                SetLueLocCode(ref LueLocCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WORDocNo, I.OptDesc As Mtc, J.Optdesc As Sym, K.Optdesc As MaintenanceType," + (mIsWorkOrderDetailEditable ? " B.Remark As Activity, ":" K.Optdesc As Activity, "));
            SQL.AppendLine("C.MechanicCode, D.MechanicName, B.Dt1, B.Tm1, B.Dt2, B.Tm2, E.TOCode, G.AssetName, H.LocName ");  
            SQL.AppendLine("From TblWOHdr A ");
            SQL.AppendLine("Inner Join TblWODtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner join TblWODtl2 C On Concat(B.DocNo, B.Dno) = Concat(C.DocNo, C.DNo) ");
            SQL.AppendLine("Inner Join TblMechanic D On C.MechanicCode = D.MechanicCode ");
            SQL.AppendLine("Inner Join TblWOR E On A.WORDocNo = E.DocNo And E.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblTOHdr F On E.TOCode = F.AssetCode ");
            SQL.AppendLine("Inner Join TblAsset G On E.TOCode = G.AssetCode ");
            SQL.AppendLine("Left Join TblLocation H On F.LocCode=H.LocCode ");
            SQL.AppendLine("Left Join TblOption I On E.MtcType=I.OptCode And I.OptCat ='MaintenanceType' "); 
            SQL.AppendLine("Left Join TblOption J On E.SymProblem=J.OptCode And J.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Left Join TblOption K On B.WOActivity=K.OptCode And K.OptCat ='WOActivity' "); 
            SQL.AppendLine("Where A.cancelInd = 'N' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Document"+Environment.NewLine+"Work Order", 
                        "Document Date"+Environment.NewLine+"Work Order", 
                        "",
                        "Document"+Environment.NewLine+"Work Order Request", 
                        "Technical"+Environment.NewLine+"Object",

                        //6-10
                        "Technical Object"+Environment.NewLine+"Name",
                        "Location",
                        "Maintenance",
                        "Symptom Problem",
                        "Maintenance Type",

                        //11-15
                        "Activity",
                        "Mechanic"+Environment.NewLine+"Code",
                        "Mechanic",
                        "Date Start",
                        "Time Start",

                        //16-17
                        "Date End",
                        "Time End"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        150, 100, 20, 170, 120, 
                        //6-10
                        200, 200, 150, 150, 150,  
                        //11-15
                        150, 80, 150, 100, 80, 
                        //16-17
                        100, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 16 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (!mIsWorkOrderDetailEditable) Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "H.LocCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtMechanic.Text, new string[] { "D.MechanicCode", "D.MechanicName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By  A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo",  
                            //1-5
                            "DocDt", "WORDocNo", "TOCode", "AssetName", "LocName",
                            //6-10
                            "Mtc", "Sym", "MaintenanceType", "Activity", "MechanicCode", 
                            //11-15
                            "MechanicName", "Dt1", "Tm1", "Dt2", "Tm2"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 17, 15);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            var WODocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
            if (e.ColIndex == 3 && WODocNo.Length > 0)
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = WODocNo;
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsWorkOrderDetailEditable = Sm.GetParameterBoo("IsWorkOrderDetailEditable");
        }

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document Work Order");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtMechanic_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMechanic_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Mechanic");
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        #endregion
    }
}
