﻿#region Update
/*
    27/06/2018 [TKG] New application
    27/08/2018 [TKG] ambil dari banyak dokumen QC
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPNT3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPNT3 mFrmParent;
        private string mWorkCenterDocNo = string.Empty;
        private decimal 
            mHolidayWagesIndex = 1m, 
            mNotHolidayWagesIndex = 1m;

        #endregion

        #region Constructor

        public FrmPNT3Dlg(
            FrmPNT3 FrmParent, 
            string WorkCenterDocNo, 
            decimal HolidayWagesIndex, 
            decimal NotHolidayWagesIndex
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWorkCenterDocNo = WorkCenterDocNo;
            mHolidayWagesIndex = HolidayWagesIndex;
            mNotHolidayWagesIndex = NotHolidayWagesIndex;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "SFC#",
                        "SFC DNo",
                        "SFC Date",
                        "Shift Code",

                        //6-10
                        "Shift", 
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Index",
                        "Batch#",
                        
                        //11-15
                        "UoM",
                        "UoM",
                        "Holiday",
                        "Holiday"+Environment.NewLine+"Index",
                        "Remark",

                        //16
                        "Quantity"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 130, 80, 100, 0,  
                    
                        //6-10
                        150, 80, 200, 70, 180,  

                        //11-15
                        80, 80, 150, 80, 400,

                        //16
                        130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 14, 16 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 6, 7, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.ProductionShiftCode, C.ProductionShiftName, ");
            SQL.AppendLine("B.ItCode, D.ItName, B.BatchNo, D.PlanningUomCode, D.PlanningUomCode2, ");
            SQL.AppendLine("F.HolName, if(IfNull(F.HolName, '')='', @NotHolidayWagesIndex, @HolidayWagesIndex) As HolidayWagesIndex, IfNull(E.ItIndex, 0) As ItIndex, ");
            SQL.AppendLine("G.Remark, IfNull(G.Qty, 0.00) As Qty ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.ProcessInd2<>'F' ");
            SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @SelectedData)<1 ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select ItCode ");
            SQL.AppendLine("        From TblProcessQCInspectionDtl ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And Status='3' ");
            SQL.AppendLine("        And Qty>0 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        And ItCode=B.ItCode ");
            SQL.AppendLine("        And BatchNo=B.BatchNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblProductionShift C On A.ProductionShiftCode=C.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblItemIndex E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblHoliday F On A.DocDt=F.HolDt ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select ItCode, BatchNo, Sum(Qty) Qty, ");
            SQL.AppendLine("    Group_Concat(Concat(BatchNo, ' : ', Convert(Format(Qty, 2) Using utf8)) Order By BatchNo Separator ', ') As Remark ");
            SQL.AppendLine("    From TblProcessQCInspectionDtl ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status='3' ");
            SQL.AppendLine("    And Qty>0 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By ItCode, BatchNo ");
            SQL.AppendLine(") G On D.ItCode=G.ItCode And B.BatchNo=G.BatchNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.WorkCenterDocNo=@WorkCenterDocNo ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", Filter2 = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);

                if (mFrmParent.Grd8.Rows.Count >= 1)
                {
                    var DocNo = string.Empty;

                    for (int r = 0; r < mFrmParent.Grd8.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd8, r, 1);
                        if (DocNo.Length != 0)
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (DocNo=@DocNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                        }
                    }
                }
                if (Filter2.Length > 0)
                    Filter2 = " And (" + Filter2 + ") ";
                
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", mWorkCenterDocNo);
                Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", mHolidayWagesIndex);
                Sm.CmParam<Decimal>(ref cm, "@NotHolidayWagesIndex", mNotHolidayWagesIndex);
                Sm.CmParam<String>(ref cm, "@SelectedData", mFrmParent.GetSelectedData());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter2) + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                             
                            //1-5
                            "DNo",
                            "DocDt", 
                            "ProductionShiftCode",
                            "ProductionShiftName",
                            "ItCode",
                            
                            //6-10
                            "ItName",
                            "ItIndex",
                            "BatchNo",
                            "PlanningUomCode",
                            "PlanningUomCode2",
                            
                            //11-14
                            "HolName",
                            "HolidayWagesIndex",
                            "Remark",
                            "Qty"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 13);
                        mFrmParent.Grd1.Cells[Row1, 15].Value = Sm.GetGrdStr(Grd1, Row2, 13).Length > 0;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 16);
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, Row1, new int[] { 11, 17 });
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 15 });
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 11, 13, 16, 17 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
            else
            {
                mFrmParent.ShowDocInfo();
                for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 2).Length > 0)
                    {
                        Sm.ComputeQtyBasedOnConvertionFormula("21", mFrmParent.Grd1, Row, 7, 13, 11, 14, 12);
                        {
                            mFrmParent.ComputeValue(Row);
                            mFrmParent.ComputeItemProcessed();
                            mFrmParent.ComputePenaltyBasedOnFormula(Sm.GetGrdDate(mFrmParent.Grd1, Row, 4), Sm.GetGrdStr(mFrmParent.Grd1, Row, 5));
                            mFrmParent.ComputeNonCoordinatorSCFPenalty(Sm.GetGrdDate(mFrmParent.Grd1, Row, 4), Sm.GetGrdStr(mFrmParent.Grd1, Row, 5));
                            mFrmParent.ComputeCoordinatorSCFPenalty(Sm.GetGrdDate(mFrmParent.Grd1, Row, 4), Sm.GetGrdStr(mFrmParent.Grd1, Row, 5));
                            mFrmParent.ComputeNonCoordinatorPenalty();
                            mFrmParent.ComputeCoordinatorPenalty();
                        }
                    }
                }
            }
        }

        private bool IsDocNoAlreadyChosen(int Row)
        {
            string Key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Key, 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 3))) 
                return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        #endregion

        #endregion

    }
}
