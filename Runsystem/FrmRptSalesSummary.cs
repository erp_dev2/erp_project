﻿#region Update
/*
    24/09/2020 [VIN/IKAS] new reporting
 *  30/09/2020 [ICA/IKAS] menambah kolom (jumlah sales dan income)
 */ 
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptSalesSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetLueGroup(ref LueGroup);
                SetLueMerchant(ref LueMerchant);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.MerchantID, A.GrupID, A.GrupName, A.MerchantName, A.Date, ");
            SQL.AppendLine("A.ProductName, A.ProductID, A.Price UPrice, A.Qty Sales, A.Total Income  ");
            SQL.AppendLine("FROM tblsalessummary A ");
            SQL.AppendLine("WHERE A.Date Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Group ID",
                    "Group",
                    "Merchant ID",
                    "Merchant",
                    "Date",
                     
                    
                    //6-10
                    "Product ID",
                    "Product",
                    "Unit Price",
                    "Sales",
                    "Income"

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 180, 150, 200, 150,  
                    
                    //6-10
                    150, 160, 120, 80, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6 }, false);

        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtProduct.Text, new string[] { "A.ProductName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueGroup), "A.GrupID", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMerchant), "A.MerchantID", true);

               
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.GrupName; ",
                    new string[]
                    {
                        //0
                        "GrupID",
                        
 
                        //1-5
                        "GrupName", "MerchantID", "MerchantName", "Date", "ProductID", 

                        //6-9
                        "ProductName", "UPrice", "Sales", "Income"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetLueGroup (ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.grupid As Col1, A.grupname As Col2 ");
            SQL.AppendLine("From tblsalessummary A ");
            SQL.AppendLine("Order By A.grupname; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        private void SetLueMerchant(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.MerchantID As Col1, A.MerchantName As Col2 ");
            SQL.AppendLine("From tblsalessummary A ");
            SQL.AppendLine("Order By A.MerchantName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #endregion

        #region events

        private void TxtProduct_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);

        }

        private void ChkProduct_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");

        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGroup, new Sm.RefreshLue1(SetLueGroup));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkGroup_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group");
        }

        private void LueMerchant_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMerchant, new Sm.RefreshLue1(SetLueMerchant));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMerchant_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group");
        }

        #endregion events

    }
}
