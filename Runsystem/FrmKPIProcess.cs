﻿#region Update
/*
    23/08/2018 [WED] ngerapihin kolom Grd
    28/08/2018 [HAR] bug fixxing KPI nonaktif masih muncul, ganti refresh data dengan button
    27/11/2018 [DITA] Based On Parameter, KPI Process bisa diedit valuenya berdasarkan parameter
    24/12/2021 [TYO/ALL] KPI Process sort by sequence berdasarkan parameter IsKPIProcessUseSequence
    18/05/2022 [TYO/Product] KPI# yang sudah di tarik di Performance Review tidak bisa ditarik 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmKPIProcess : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmKPIProcessFind FrmFind;
        internal bool mIsKPIPAllowToEditValue = false;
        internal bool mIsKPIProcessUseSequence = false;

        #endregion

        #region Constructor

        public FrmKPIProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "KPI Process";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetLueKPICode(ref LueKPI);
                SetFormControl(mState.View);
               
                base.FrmLoad(sender, e);
                //if this application is called from other application
                //if (mDocNo.Length != 0)
                //{
                //    ShowData(mDocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "KPI DNo",
                        "Sequence",
                        "Result" + Environment.NewLine + "Indicator",
                        "Bobot",
                        "Unit of Measurement",
                        
                        
                        //6-9
                        "Year / Month",
                        "Type",
                        "Target of Year",
                        "Value",
                    },
                     new int[] 
                    {
                        20, 
                        20, 80, 250, 80, 150,
                        100, 80, 100, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 8, 9 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 1);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, mIsKPIProcessUseSequence);
        }


        override protected void HideInfoInGrd()
        {
            if(mIsKPIProcessUseSequence)
                Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueKPI, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    DteDocDt.Focus();
                    BtnResult.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, LueKPI, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnResult.Enabled = true;
                    break;
                case mState.Edit:
                    if (mIsKPIPAllowToEditValue)
                    {
                        if (!ChkCancelInd.Checked)
                            Grd1.ReadOnly = false;
                    }
                    else
                    {
                        Grd1.ReadOnly = true;
                       
                    }
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueKPI, MeeRemark
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmKPIProcessFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();

                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            //ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "KPIProcess", "TblKPIProcessHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveKPIProcessHdr(DocNo));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveKPIProcessDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueKPI, "KPI Name")||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 KPI.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveKPIProcessHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIProcessHdr(DocNo, DocDt, CancelInd, KPIDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, @KPIDocNo, @Remark, @UserCode, CurrentDateTime()); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "KPIDocNo", Sm.GetLue(LueKPI));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveKPIProcessDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIProcessDtl(DocNo, DNo, KPIDNo, ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("Sequence, ");
            SQL.AppendLine("Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @KPIDNo, ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("@Sequence, ");
            SQL.AppendLine("@Value, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@KPIDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Sequence", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(EditKPIProcessCancel(TxtDocNo.Text));
            cml.Add(DeleteKPIProcessDtl(TxtDocNo.Text) );
            
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveKPIProcessDtl(TxtDocNo.Text, Row));
            }
            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document Number", false) ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblKPIProcessHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditKPIProcessCancel(string DocNo)
        {
            var SQL = new StringBuilder();
            
             SQL.AppendLine("Update TblKPIProcessHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " );
             SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", @DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");

            return cm;
        }

        private MySqlCommand DeleteKPIProcessDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblKPIProcessDtl Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowKPIProcessHdr(DocNo);
                ShowKPIProcessDtl(DocNo);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowKPIProcessHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, A.KPIDocNo, A.Remark From TblKPIProcessHdr  A " +
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "KPIDocNo", "Remark"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        SetLueKPICode(ref LueKPI);
                        Sm.SetLue(LueKPI, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowKPIProcessDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, B.KPIDno, ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("B.Sequence, ");
            else
                SQL.AppendLine("Null As Sequence, ");
            SQL.AppendLine("C.Result, C.bobot, C.uom, C.option, C.target, C.Type, B.Value ");
            SQL.AppendLine("From TblKPIProcessHdr A ");
            SQL.AppendLine("Inner Join TblKPIProcessDtl B On A.DocNo = B.Docno");
            SQL.AppendLine("Inner Join TblKPIDtl C On A.KPIDocNo = C.DocNo And B.KPIDno = C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "KPIDno", "Sequence", "Result", "bobot", "uom",  
                    //6-9
                    "option", "type", "target",  "Value" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(Grd1, Grd1.Rows.Count - 1, new int[] { 3, 7, 8 });
        }


        private void ShowDataKPIDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("B.Sequence, ");
            else
                SQL.AppendLine("Null As Sequence, ");
            SQL.AppendLine("B.Result, B.bobot, B.uom, B.option, B.Type,  B.target ");
            SQL.AppendLine("From TblKPIHdr A ");
            SQL.AppendLine("Inner Join TblKPIDtl B On A.DocNo = B.Docno");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            if (mIsKPIProcessUseSequence)
                SQL.AppendLine("Order By B.Sequence ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Sequence", "Result", "bobot", "uom", "option", 
                    //6-7
                    "type", "target"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                    Grd1.Cells[Row, 9].Value = 0;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(Grd1, Grd1.Rows.Count - 1, new int[] { 4, 8, 9 });
        }


        #endregion

        #region Additional Method


        private void GetParameter()
        {
            mIsKPIPAllowToEditValue = Sm.GetParameterBoo("IsKPIPAllowToEditValue");
            mIsKPIProcessUseSequence = Sm.GetParameterBoo("IsKPIProcessUseSequence");
        }

        private void SetLueKPICode(ref LookUpEdit Lue)
        {
            if (BtnSave.Enabled)
            {
                Sm.SetLue2(
                    ref Lue,
                    "Select A.DocNo As Col1, A.KPIName As Col2 From TblKPIHdr A Where ActInd = 'Y' "+
                    "And Not EXISTS ( " +
                    "	Select 1 From TblPerformanceReviewHdr  " +
                    "	Where KPIDocNo = A.DocNo And CancelInd = 'N' ) " +
                    "Order By Col2;",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            else
            {
                Sm.SetLue2(
                    ref Lue,
                    "Select DocNo As Col1, KPIName As Col2 From TblKPIHdr Order By Col2;",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
        }

        #endregion

        private void LueKPI_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            Sm.RefreshLookUpEdit(LueKPI, new Sm.RefreshLue1(SetLueKPICode));

        }

        private void BtnResult_Click(object sender, EventArgs e)
        {

            if (Sm.GetLue(LueKPI).Length > 0 && BtnSave.Enabled)
                ShowDataKPIDtl(Sm.GetLue(LueKPI));
        }
        #endregion

        
    }
}
