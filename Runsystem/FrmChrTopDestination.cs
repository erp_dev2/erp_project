﻿#region Update
/*
    06/04/2017 [WED] Y-Axis diganti menjadi "Top Delivery"
    06/04/2017 [WED] Function Print langsung dari PrintPreviewDialog (karena via print dialog masih nggak mau nangkep perubahan di print orientation, grayscale, dll a.k.a tetep printnya hasilnya potrait colored)
    07/04/2017 [WED] menampilkan point di tiap series Chart
    10/01/2018 [WED] filter province bisa multiselect (checked combobox edit)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Syncfusion.Windows;
using Syncfusion.Windows.Forms.Chart;
using SyXL = Syncfusion.XlsIO;
using SyDoc = Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmChrTopDestination : RunSystem.FrmBase10
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            exportFileName = string.Empty,
            file = string.Empty;

        #endregion

        #region Constructor

        public FrmChrTopDestination(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnExcel.Visible = BtnPDF.Visible = BtnWord.Visible = false;
            string CurrentDate = Sm.ServerCurrentDateTime();
            DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
            DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
            SetCcbProvCode(ref CcbProvCode);

            this.Chart.Title.Text = Sm.GetValue("Select IfNull(MenuDesc, '') as MenuDesc From TblMenu Where Param = 'FrmChrTopDestination' Limit 1;");

            base.FrmLoad(sender, e);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    IsFilterByDateInvalid()
                ) return;

                ChartAppearance.ApplyChartStyles(this.Chart);
                LoadData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Info, Exc.Message);
            }
        }

        private void LoadData()
        {
            var l = new List<TopDestination>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " And 0 = 0 ";

            SQL.AppendLine("Select C.CityName, Count(C.CityName) AS Destination ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDRHdr B on A.DRDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCity C on B.SACityCode = C.CityCode ");
            SQL.AppendLine("Left Join TblProvince D On C.ProvCode = D.ProvCode ");
            SQL.AppendLine("Where A.DRDocNo Is Not Null And B.SACityCode Is Not Null ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            Sm.FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbProvCode)), "D.ProvName", false);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString() + Filter + " Group By C.CityName Order By Count(C.CityName) Desc LIMIT 10 ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "CityName",

                    //1
                    "Destination"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TopDestination()
                        {
                            CityName = Sm.DrStr(dr, c[0]),
                            Destination = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                else
                    Sm.StdMsg(mMsgType.NoData, "");

                dr.Close();
            }

            BindChart(l);
        }

        private void BindChart(object dataChart)
        {
            this.Chart.Series.Clear();
            ChartSeries series = new ChartSeries(Chart.Title.Text);
            ChartDataBindModel dataSeriesModel = new ChartDataBindModel(dataChart);

            // If ChartDataBindModel.XName is empty or null, X value is index of point.
            dataSeriesModel.XName = "CityName";
            dataSeriesModel.YNames = new string[] { "Destination" };
            series.Text = series.Name;

            //display point on top of series
            series.Style.DisplayText = true;
            series.Style.TextOrientation = ChartTextOrientation.Up;

            //series.SeriesModel = dataSeriesModel;
            series.SeriesIndexedModelImpl = dataSeriesModel;

            ChartDataBindAxisLabelModel dataLabelsModel = new ChartDataBindAxisLabelModel(dataChart);
            dataLabelsModel.LabelName = "CityName";
            Chart.Series.Add(series);
            Chart.PrimaryXAxis.LabelsImpl = dataLabelsModel;
            Chart.PrimaryXAxis.ValueType = ChartValueType.Custom;
            Chart.PrimaryXAxis.Title = "Destination's City";
            Chart.PrimaryYAxis.Title = "Total Delivery (times)";
        }

        #endregion

        #region Additional Method

        private void SetCcbProvCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ProvName As Col ");
            SQL.AppendLine("From TblProvince A ");
            SQL.AppendLine("Inner Join TblCountry B On A.CntCode=B.CntCode And B.CntCode = 'INA' ");
            SQL.AppendLine("Order By A.ProvName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                //string Vals = string.Empty;
                //string[] val1 = Value.Split(',');

                //for (int i = 0; i < val1.Length; i++)
                //{
                //    string[] val2 = val1[i].Split('|');

                //    for (int j = 0; j < (val2.Length / 2); j++)
                //    {
                //        Vals = Vals.Length > 0 ? Vals + ", " + val2[0].Trim() : val2[0].Trim();
                //    }
                //}
                //Value = Vals;
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        protected void OpenFile(string filetype, string exportFileName)
        {
            try
            {
                //if (filetype == "Grid")
                //    gridForm.ShowDialog();
                //else
                System.Diagnostics.Process.Start(exportFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #region Button Click

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog pr = new PrintDialog();
            PrintPreviewDialog ppd = new PrintPreviewDialog();

            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.Info, "No Data");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty");
                else
                {
                    pr.AllowSomePages = true;
                    pr.AllowSelection = true;
                    pr.PrinterSettings.Clone();
                    pr.Document = Chart.PrintDocument;
                    if (pr.ShowDialog() == DialogResult.OK)
                        pr.Document.Print();
                }
            }

            //if (pr.ShowDialog() == DialogResult.OK)
            //{
            //    pr.Document = Chart.PrintDocument;
            //    ppd.Document = Chart.PrintDocument;
            //    if (ppd.ShowDialog() == DialogResult.OK)
            //        //pr.Document.Print();
            //        ppd.Document.Print();
            //}
        }

        private void BtnWord_Click(object sender, EventArgs e)
        {
            try
            {
                if(Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".doc";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + "_" + Sm.ConvertDate(Sm.GetDte(DteDocDt1)) + "_TO_" + Sm.ConvertDate(Sm.GetDte(DteDocDt2)) + ".gif";
                        //if (!System.IO.File.Exists(file))
                        Chart.SaveImage(file);

                        //Create a new document
                        WordDocument document = new WordDocument();
                        
                        //Adding a new section to the document.
                        IWSection section = document.AddSection();
                        //Adding a paragraph to the section
                        IWParagraph paragraph = section.AddParagraph();
                        //Writing text.
                        paragraph.AppendText(Chart.Title.Text);
                        //Adding a new paragraph		
                        paragraph = section.AddParagraph();
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        //Inserting chart.
                        paragraph.AppendPicture(Image.FromFile(file));
                        //Save the Document to disk.
                        document.Save(exportFileName, Syncfusion.DocIO.FormatType.Doc);
                        System.Diagnostics.Process.Start(exportFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.NoData, "");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty.");
                else
                {
                    exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".xls";

                    // A new workbook with a worksheet should be created. 
                    SyXL.IWorkbook chartBook = SyXL.ExcelUtils.CreateWorkbook(1);
                    SyXL.IWorksheet sheet = chartBook.Worksheets[0];

                    //if chart is not empty
                    // Fill the worksheet with chart data. 
                    for (int i = 1; i <= Chart.Series[0].Points.Count; i++)
                    {
                        sheet.Range[i, 1].Number = Chart.Series[0].Points[i - 1].X;
                        sheet.Range[i, 2].Number = Chart.Series[0].Points[i - 1].YValues[0];
                    }

                    // Create a chart worksheet. 
                    SyXL.IChart chart = chartBook.Charts.Add(Chart.Title.Text);

                    // Specify the title of the Chart.
                    chart.ChartTitle = Chart.Title.Text;

                    // Initialize a new series instance and add it to the series collection of the chart. 
                    SyXL.IChartSerie series = chart.Series.Add(Chart.Title.Text);

                    // Specify the chart type of the series. 
                    series.SerieType = SyXL.ExcelChartType.Column_Clustered;

                    // Specify the name of the series. This will be displayed as the text of the legend. 
                    //series.Name = Chart.Name;

                    // Specify the value ranges for the series.
                    series.Values = sheet.Range["B1:B10"];

                    // Specify the Category labels for the series. 
                    series.CategoryLabels = sheet.Range["A1:A10"];

                    // Make the chart as active sheet. 
                    chart.Activate();

                    // Save the Chart book. 
                    chartBook.SaveAs(exportFileName); chartBook.Close();
                    SyXL.ExcelUtils.Close();

                    // Launches the file. 
                    System.Diagnostics.Process.Start(exportFileName);
                }
            }
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".pdf";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //if (!System.IO.File.Exists(file))
                        this.Chart.SaveImage(file);

                        //Create a new PDF Document. The pdfDoc object represents the PDF document.
                        //This document has one page by default and additional pages have to be added.
                        PdfDocument pdfDoc = new PdfDocument();

                        pdfDoc.Pages.Add();

                        pdfDoc.Pages[0].Graphics.DrawImage(PdfImage.FromFile(file), new PointF(10, 30));

                        //Save the PDF Document to disk.
                        pdfDoc.Save(exportFileName);
                        OpenFile("Pdf", exportFileName);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbProvCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProvCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Province");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }

    #region Class

    class TopDestination
    {
        public string CityName { get; set; }
        public decimal Destination { get; set; }
    }

    #endregion
}
