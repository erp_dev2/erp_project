﻿#region Update
/*
    07/10/2017 [ARI] reporting baru
    20/11/2017 [TKG] nilai berupa angka menggunakan format 2 angka di belakang koma.
    11/01/2018 [WED] tambah source dari qc workcenter
    14/01/2018 [TKG] tambah filter item
    28/01/2018 [TKG] tambah info dan filter document date dan inspection date
    27/02/2018 [TKG] tambah filter parameter
    06/03/2018 [ARI] tambah department
    10/04/2019 [DITA] tambah filter Batch#
    15/12/2021 [RIS/IOK] Memunculakn work center document MQLCI (warehouse)
    16/06/2022 [RIS/IOK] Menambahkan kolom inspector's name, start time dan end time dengan param IsMaterialQCInspectionWOShowEndTimeInspector
    21/06/2022 [RIS/IOK] Bug : Subtotal grd planned quantity & rejected quanitity
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialQCInspection3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsMaterialQCInspectionWOShowEndTimeInspector = false;

        #endregion

        #region Constructor

        public FrmRptMaterialQCInspection3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void SetSQL()
        {

            var SQL = new StringBuilder();


            SQL.AppendLine("Select T.* From ( ");

            // Material QC Inspection (Warehouse)
            SQL.AppendLine("Select '1' As DocTypes, A.DocNo, A.QCPlanningDocNo, A.DocDt, A.StartDt, F.WhsCode, G.WhsName, F.WorkCenterDocNo As WCCode, I.DocName As WCName, F.DeptCode, H.DeptName, E.QCPDesc, E.QCPIndicator, D.ItCode, D.ItName, C.BatchNo, A.CreateBy, ");
            SQL.AppendLine("Round(C.Qty2,2)As PlanningQty, Round(B.Qty,2) As QCRejected, Round((B.Qty / C.Qty2) * 100.00, 2) As RejectedPercentage ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
                SQL.AppendLine(", GROUP_CONCAT(K.EmpName ORDER BY K.EmpName SEPARATOR ', ') AS Inspection, A.StartTm, NULL AS EndTm ");
            else
                SQL.AppendLine(", null as Inspection, null as StartTm, null as EndTm ");
            SQL.AppendLine("from tblmaterialqcinspection2hdr A ");
            SQL.AppendLine("inner join tblmaterialqcinspection2dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("inner join tblqcplanningdtl C On A.QCPlanningDocNo = C.DocNo And B.QCPlanningDNo = C.DNo ");
            SQL.AppendLine("inner join tblitem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("left join tblqcparameter E On B.QCPCode = E.QCPCode ");
            SQL.AppendLine("inner join tblqcplanninghdr F On A.QCPlanningDocNo = F.DocNo ");
            SQL.AppendLine("left join tblwarehouse G On F.WhsCode = G.WhsCode ");
            SQL.AppendLine("left join tbldepartment H On F.DeptCode = H.DeptCode ");
            SQL.AppendLine("left join tblworkcenterhdr I On F.WorkCenterDocNo = I.DocNo ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
            {
                SQL.AppendLine("inner JOIN tblmaterialqcinspection2dtl2 J ON A.DocNo = J.DocNo ");
                SQL.AppendLine("inner JOIN tblemployee K ON J.EmpCode = K.EmpCode ");
            }
            SQL.AppendLine("where B.CancelInd = 'N' and B.Status = '3' ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
                SQL.AppendLine("Group By A.DocNo, B.DNo ");
            SQL.AppendLine("Union All ");

            // Process QC Inspection (Workcenter)
            SQL.AppendLine("Select '2' As DocTypes, A.DocNo, A.QCPlanningDocNo, A.DocDt, A.StartDt, null As WhsCode, null as WhsName, F.WorkCenterDocNo As WCCode, G.DocName as WCName, F.DeptCode, H.DeptName, E.QCPDesc, E.QCPIndicator, D.ItCode, D.ItName, B.BatchNo, A.CreateBy, ");
            SQL.AppendLine("Round(C.Qty2,2)As PlanningQty, Round(B.Qty,2) As QCRejected, Round((B.Qty / C.Qty2) * 100.00, 2) As RejectedPercentage ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
                SQL.AppendLine(", GROUP_CONCAT(J.EmpName ORDER BY J.EmpName SEPARATOR ', ') AS Inspection, A.StartTm, A.EndTm AS EndTm ");
            else
                SQL.AppendLine(", null as Inspection, null as StartTm, null as EndTm ");
            SQL.AppendLine("from tblprocessqcinspectionhdr A ");
            SQL.AppendLine("inner join tblprocessqcinspectiondtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("inner join tblqcplanningdtl C On A.QCPlanningDocNo = C.DocNo And B.QCPlanningDNo = C.DNo ");
            SQL.AppendLine("inner join tblitem D On B.ItCode = D.ItCode ");
            SQL.AppendLine("left join tblqcparameter E On B.QCPCode = E.QCPCode ");
            SQL.AppendLine("inner join tblqcplanninghdr F On A.QCPlanningDocNo = F.DocNo ");
            SQL.AppendLine("inner join tblworkcenterhdr G On F.WorkCenterDocNo = G.DocNo ");
            SQL.AppendLine("left join tbldepartment H On F.DeptCode = H.DeptCode ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
            {
                SQL.AppendLine("inner join TblProcessQCInspectionDtl2 I ON A.DocNo = I.DocNo ");
                SQL.AppendLine("inner join tblemployee J ON I.EmpCode = J.EmpCode ");
            }
            SQL.AppendLine("where B.CancelInd = 'N' and B.Status = '3' ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
                SQL.AppendLine("Group By A.DocNo, B.DNo ");

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                     //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Planning#",
                    "",
                    
                    //6-10
                    "Location",
                    "Work Center#",
                    "Work Center",
                    "Department",
                    "Inspection"+Environment.NewLine+"Date",
                    
                    //11-15
                    "Parameter",
                    "Inspector's Name",
                    "Inspection Time",
                    "End Time",
                    "Indicator#",
                    
                    //16-20
                    "Item's Name",
                    "Batch#",
                    "Planned"+Environment.NewLine+"Quantity",
                    "Rejected"+Environment.NewLine+"Quantity",
                    "Reject"+Environment.NewLine+"%",

                    //21
                    "Created By"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 20, 80, 140, 20, 
                    
                    //6-10
                    200, 150, 200, 200, 100, 
                    
                    //11-15
                    120, 250, 80, 80, 120, 
                    
                    //16-20
                    200, 130, 100, 100, 100, 
                    
                    //21
                    180
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 19, 20 }, 2);
            Sm.GrdFormatTime(Grd1, new int[] { 13, 14 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7 }, false);
            if (!mIsMaterialQCInspectionWOShowEndTimeInspector)
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteStartDt1), Sm.GetDte(DteStartDt2), "T.StartDt");
                Sm.FilterStr(ref Filter, ref cm, TxtPlanningDocNo.Text, "T.QCPlanningDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtQCPDesc.Text, "T.QCPDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "T.WCCode", "T.WCName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItName.Text, new string[] { "T.ItCode", "T.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "T.BatchNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocDt, T.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "QCPlanningDocNo", "WhsName", "WCCode", "WCName", 
                            
                            //6-10
                            "DeptName", "StartDt", "QCPDesc", "QCPIndicator", "ItName", 

                            //11-15
                            "BatchNo", "PlanningQty", "QCRejected", "RejectedPercentage", "CreateBy",

                            //16-18
                            "Inspection", "StartTm", "EndTm"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            if(mIsMaterialQCInspectionWOShowEndTimeInspector)
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 16);
                                Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 17);
                                Sm.SetGrdValue("T2", Grd, dr, c, Row, 14, 18);
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 18, 19 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0 && 
                    Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length <= 0)
                {
                    var f = new FrmMaterialQCInspection2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmMaterialQCInspection2' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length <= 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length > 0)
                {
                    var f = new FrmProcessQCInspection(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmProcessQCInspection' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmQCPlanning(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmQCPlanning' Limit 1;");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length <= 0)
                {
                    var f = new FrmMaterialQCInspection2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmMaterialQCInspection2' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length <= 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length > 0)
                {
                    var f = new FrmProcessQCInspection(mMenuCode);
                    f.Tag = mMenuCode;
                    f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmProcessQCInspection' Limit 1;");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmQCPlanning(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmQCPlanning' Limit 1;");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsMaterialQCInspectionWOShowEndTimeInspector = Sm.GetParameterBoo("IsMaterialQCInspectionWOShowEndTimeInspector");
        }
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteStartDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteStartDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkStartDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Inspection date");
        }

        private void TxtPlanningDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPlanningDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planning#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work center");
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtQCPDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQCPDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Parameter");
        }


        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        #endregion

        

      
        #endregion

        

    }
}
