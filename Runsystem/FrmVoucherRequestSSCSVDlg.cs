﻿#region Update
// 06/06/2018 [ARI] tambah filter payrun
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestSSCSVDlg : RunSystem.FrmBase4
    {
        #region Field

        string mSQL;
        private FrmVoucherRequestSSCSV mFrmParent;

        #endregion

        #region Constructor

        public FrmVoucherRequestSSCSVDlg(FrmVoucherRequestSSCSV FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e); this.Text = "List Of Voucher";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "", 
                        "Date",
                        "Payrun Code",
                        "Payrun Name",

                        //6-10
                        "Department",
                        "Start Date",
                        "End Date",
                        "Bank Account#",
                        "Remark"
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 7, 8 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("B.PayrunCode, C.PayrunName, D.DeptName, C.StartDt, C.EndDt, ");
            SQL.AppendLine("E.BankAcNo, A.Remark ");
            SQL.AppendLine("From TblVoucherRequestPayrollHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPayrun C On B.PayrunCode=C.PayrunCode ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode=D.DeptCode  ");
            SQL.AppendLine("Left Join TblBankAccount E On A.BankAcCode=E.BankAcCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='A' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "B.PayrunCode", "C.PayrunName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "PayrunCode", "PayrunName", "DeptName", "startDt",

                            //6-8
                            "EndDt", "BankAcNo", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            
                        }, true, false, false, false
                    );
                Grd1.Cols.AutoWidth();
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                Sm.SetDte(mFrmParent.DteDocDt, Sm.GetGrdDate(Grd1, Row, 3));
                mFrmParent.TxtPayrunCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtPayrunName.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                Sm.SetDte(mFrmParent.DteStartDt, Sm.GetGrdDate(Grd1, Row, 7));
                Sm.SetDte(mFrmParent.DteEndDt, Sm.GetGrdDate(Grd1, Row, 8));
                mFrmParent.mBankAcNo = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.ClearGrd();
                mFrmParent.ShowData();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        private void TxtPayCod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        #endregion

       
    }
}
