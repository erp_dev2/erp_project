﻿#region Update 
/* 
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    07/04/2020 [WED/SRN] hanya nampilkan data yg DocType = 1
    28/09/2020 [TKG/KSM] bug saat refresh data 
    24/02/2021 [WED/SIER] tambah informasi Customer Category berdasarkan parameter IsCustomerComboBasedOnCategory
    18/05/2021 [RDH/ALL] Tambah kolom TERM OF PAYMENT saat FIND Customer's Quotation (With Fixed Price)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCtQtFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCtQt mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmCtQtFind(FrmCtQt FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select K.PtName, A.DocNo, A.DocDt, A.ActInd, J.CtCtName, E.CtName, G.ItCode, H.ItName, H.ItCodeInternal, H.Specification, I.CtItCode, I.CtItName, C.UPrice,  ");
            SQL.AppendLine("Case When A.Status='O' Then 'Outstanding' ");
            SQL.AppendLine("When A.Status='A' Then 'Approved' ");
            SQL.AppendLine("When A.Status='C' Then 'Cancelled' ");
            SQL.AppendLine("End As Status, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblCtQtHdr A ");
            SQL.AppendLine("Inner Join TblCtQtDtl C On A.DocNo = C.DocNo And A.DocType = 1 ");
            SQL.AppendLine("Inner Join TblCustomer E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblItemPricehdr F On C.ItemPriceDocNo = F.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl G On C.ItemPriceDocNo = G.Docno And C.ItemPriceDNo = G.Dno ");
            SQL.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=H.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblCustomerItem I On G.ItCode = I.ItCode And A.CtCode=I.CtCode ");
            SQL.AppendLine("Left Join TblCustomerCategory J On E.CtCtCode = J.CtCtCode ");
            SQL.AppendLine("INNER JOIN tblpaymentterm K ON A.PtCode = K.PtCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Active",
                    "Status",
                    "Customer's"+Environment.NewLine+"Category",

                    //6-10
                    "Customer",
                    "Term Of Payment",
                    "Item's Code",
                    "",
                    "Local Code",

                    //11-15
                    "Item's Name",
                    "Specification",
                    "Customer's"+Environment.NewLine+"Item Code",
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Price",

                    //16-20
                    "Created By",
                    "Created Date",
                    "Created Time",  
                    "Last Updated By",
                    "Last Updated Date", 

                    //21
                    "Last Updated Time",
                },
                new int[]
                {
                    60,
                    180, 80, 60, 100, 150,
                    200, 150, 100, 20, 120, 
                    180, 200, 120, 200, 130, 
                    100, 100, 100, 100, 100,
                    100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3});
            Sm.GrdColButton(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 17, 20 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 12, 13, 16, 17, 18, 19, 20, 21 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 14 });
            if (!mFrmParent.mIsCustomerComboBasedOnCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 12, 13, 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if (mFrmParent.mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "H.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt Desc;",
                    new string[]
                    {
                        //0
                       "DocNo",

                       //1-5
                       "DocDt", "ActInd", "Status", "CtCtName", "CtName", 

                       //6-10
                       "PtName", "ItCode", "ItCodeInternal", "ItName", "Specification",  
                       
                       //11-15
                       "CtItCode", "CtItName", "UPrice", "CreateBy", "CreateDt",  
                       
                       //16
                       "LastUpBy", "LastUpDt"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 17);

                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion
        
        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
