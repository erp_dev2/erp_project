﻿#region Update
/* 
    13/07/2017 [HAR] tambah query ke PL detail untuk mendapatkan total kubikasi yang sesuai dengan PL (Rounding) 
    18/07/2017 [HAR] tambah Query untuk total USD
    14/08/2017 [HAR] tambah Query jumlah pallet
    04/09/2017 [TKG] Rate untuk value diambil dari Sales Invoice. 
                     Tambah Informasi sales invoice date.
    21/01/2019 [HAR] nilai rounding dibulatin sesuai denga printout shipment invoice
    25/01/2019 [HAR] rombak query utntuk menyamakan denga printout shipmenr invoice
    02/04/2019 [MEY] menambahkan kolom Stuffing Date diambil dari Shipment Instruction
    07/01/2019 [HAR/IOK]Commercial Invoice nilai volume kubik terisi nilai lembar
    10/01/2023 [VIN/IOK]Commercial Invoice nilai Pallet ambil dari PLDtl
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCommercialInvoice : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptCommercialInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select X.DocNo, X.LocalDocNo, X.LCNo, X.PEB, X.PEBDt, X.Remark, Sum(X.QtyPL)As QtyPL, Truncate(Sum(X.VolPEB), 4) As VolVlegal, X.CtName, ");
            SQL.AppendLine("    Round(Round(X.Amount, 2)*X.ExcRate, 4) As ExcRate, X.CurCode, Round(X.Amount, 2) As Amount, X.SalesInvoiceDt, X.SpStfDt  ");
            SQL.AppendLine("    From( ");
		    SQL.AppendLine("            Select Z.DocNo, Z.LocalDocNo, Z.LCNo, Z.PEB, Z.PEBDt, Z.Remark, ");
            //SQL.AppendLine("            Sum(Round(Z.QtyPackagingUnit,2)) As QtyPL, ");
            SQL.AppendLine("            Sum(Z.QtyPackagingUnit) As QtyPL, ");
            SQL.AppendLine("            Sum(Z.QtyInventory) As VolPEB, ");
            SQL.AppendLine("            Z.CtName, ");
            SQL.AppendLine("            sum(Z.Amount)As Amount, Z.ExcRate, Z.CurCode, Z.SalesInvoiceDt, Z.SpStfDt  ");
		    SQL.AppendLine("            From ( ");
			SQL.AppendLine("		                Select A.DocNo, H.ItCode, D2.SectionNo, A.LocalDocNo, D.LCNo, A.PEB, A.PEBDt, Substring_Index(Replace(E.Remark, '");
            SQL.AppendLine("		                ','*'),'*',1)As Remark,  I.CtName, H.UPrice, "); // Round(C.Qty, 4), ");
            SQL.AppendLine("		                G.CurCode, B.SpStfDt, ");
			//SQL.AppendLine("		                Round((H.UPrice * Round(D2.Qty, 4)), 2) As Amount, ");
            SQL.AppendLine("		                H.UPrice * D2.Qty As Amount, ");
            SQL.AppendLine("		                J.SalesInvoiceDt, ");
			SQL.AppendLine("		                Case When G.CurCode=@MainCurCode Then 1.00 Else ");
			SQL.AppendLine("		                IfNull(( ");
			SQL.AppendLine("			                Select Amt From TblCurrencyRate ");
			SQL.AppendLine("			                Where RateDt<=J.SalesInvoiceDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
			SQL.AppendLine("			                Order By RateDt Desc Limit 1 ");
			SQL.AppendLine("		                ), 0.0000) End As ExcRate, ");
            SQL.AppendLine("		                D2.QtyPackagingUnit, D2.Qty, D2.QtyInventory ");
			SQL.AppendLine("		                From TblSP A ");
			SQL.AppendLine("		                Inner Join TblSiHdr B On A.DocNo=B.SpDocNo And A.status <>'C' ");
			SQL.AppendLine("		                Inner join TblSIDtl C On B.DocNo = C.DocNo ");
			SQL.AppendLine("		                Inner Join TblPLHdr D On B.DocNo=D.SiDocNo ");
			SQL.AppendLine("		                Inner Join ( ");
			SQL.AppendLine("		                    Select itCode, DocNo, SectionNo, SODOcNo, SODNo, ");
            //SQL.AppendLine("		                    Round(Qty, 4) Qty, Round(Qtyinventory, 4) QtyInventory ");
            SQL.AppendLine("		                    Qty, QtyInventory, QtyPackagingUnit ");
			SQL.AppendLine("			                From TblPLDtl  ");
			//SQL.AppendLine("			                Group BY itCode, DocNo, SectionNo, SODOcNo, SODNo ");
			SQL.AppendLine("		                ) D2 On D.DocNo = D2.DocNo And  C.SODocNo=D2.SODocNo And C.SODNo=D2.SODNo ");
			SQL.AppendLine("		                Inner Join TblSinv E On D.DocNo=E.PlDocNo ");
			SQL.AppendLine("		                Inner Join TblSODtl F On C.SODocNo = F.DocNo And C.SODNo = F.DNo ");
			SQL.AppendLine("		                Inner Join TblSOHdr G On F.DocNo = G.DocNo And C.SODocNo = G.DocNo ");
			SQL.AppendLine("		                Inner Join TblCtQtDtl H On G.CtQtDocNo = H.DocNo And F.CtQtDNo = H.Dno ");
			SQL.AppendLine("		                Inner Join TblCustomer I On A.CtCode = I.CtCode ");
			SQL.AppendLine("		                Inner Join ( ");
			SQL.AppendLine("					                    Select Distinct T3.PLDocNo, T1.DocDt As SalesInvoiceDt ");
			SQL.AppendLine("					                    From TblSalesInvoiceHdr T1 ");
			SQL.AppendLine("					                    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
			SQL.AppendLine("					                    Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.PLDocNo Is Not Null ");
			SQL.AppendLine("					                    Inner Join TblPLHdr T4 On T3.PLDocNo=T4.DocNo ");
			SQL.AppendLine("					                    Inner Join TblSIHdr T5 On T4.SIDocNo=T5.DocNo ");
			SQL.AppendLine("					                    Inner Join TblSP T6 On T5.SPDocNo=T6.DocNo And T6.PEBDt Between @DocDt1 And @DocDt2 ");
			SQL.AppendLine("					                    Where T1.CancelInd='N' ");
			SQL.AppendLine("					                ) J On D.DocNo=J.PLDocNo ");
		    SQL.AppendLine("            )Z ");
		    //SQL.AppendLine("            Group by Z.DocNo, Z.LocalDocNo, Z.LCNo, Z.PEB, Z.PEBDt, Z.Remark, Z.ExcRate, Z.CurCode, Z.SalesInvoiceDt ");
            SQL.AppendLine("            Group By Z.DocNo, Z.LocalDocNo, Z.LCNo, Z.PEB, Z.PEBDt, Z.Remark, Z.CtName, Z.ExcRate, Z.CurCode, Z.SalesInvoiceDt, Z.SpStfDt ");
		    SQL.AppendLine("            )X  ");
            //SQL.AppendLine("    Group By X.DocNo, X.LocalDocNo, X.LCNo, X.PEB, X.PEBDt, X.Remark, X.CtName, X.ExcRate, X.CurCode, X.Amount, X.SalesInvoiceDt ");
            SQL.AppendLine("    Group By X.DocNo, X.LocalDocNo, X.LCNo, X.PEB, X.PEBDt, X.Remark, X.CtName, X.Amount, X.ExcRate, X.CurCode, X.Amount, X.SalesInvoiceDt, X.SpStfDt  ");
            //SQL.AppendLine("    Order by X.DocNo ");
            SQL.AppendLine(") X1  ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Local"+Environment.NewLine+"Invoice#",
                    "Invoice#",
                    "LC#",
                    "PEB#",
                    "PEB"+Environment.NewLine+"Date",
                    
                    //6-10
                    "Stuffing Date",
                    "Product"+Environment.NewLine+"Type",
                    "Pallet",
                    "Volume",
                    "Currency",
           

                    //11-14
                    "Total"+Environment.NewLine+"USD",
                    "Value",
                    "Buyer",
                    "Sales Invoice"+Environment.NewLine+"Date"
                },
                new int[] 
                {
                    //0
                    50,
                    
                    //1-5
                    100, 120, 120, 120, 100,
                    
                    //6-10
                    100, 230, 100, 100, 80, 
                    
                    //11-14
                    150, 130, 200, 100                
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[]{ 5, 6, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "PEB's start date") ||
                Sm.IsDteEmpty(DteDocDt2, "PEB's end date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;

                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtInvoice.Text, new string[] { "X1.LocalDocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "LocalDocNo",

                            //1-5
                            "DocNo", "LCNo", "Peb", "PebDt", "SpStfDt",  
                            
                            //6-10
                            "Remark","QtyPL", "VolVlegal", "CurCode",  "Amount", 
                        
                            //11-12
                             "ExcRate","CtName", "SalesInvoiceDt", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9, 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9, 11, 12 });
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtInvoice_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvoice_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local invoice#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion
    }
}
