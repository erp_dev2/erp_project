﻿#region Update
/*
    21/08/2019 [WED] new reporting
    22/12/2021 [ICA/YK] Mengubah source menjadi dari journal, description berasal dari COA berdasarkan parameter RptProfitLossProjectFormat
    30/12/2021 [ICA/YK] Bug data double
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLossProject : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mSQL = string.Empty, 
            mProjectIncomeDesc = string.Empty,
            mProjectCode = string.Empty,
            mProjectName = string.Empty,
            mAcNoForDODeptLOPCC = string.Empty, // cost center untuk project
            mRptProfitLossProjectFormat = string.Empty;
        internal string mAccessInd = string.Empty, mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProfitLossProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPLOBAmt, TxtPLMAmt, TxtPLCBAmt }, 0);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 1;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No.";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Description";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[1, 2].Value = "Opening Balance";
            Grd1.Header.Cells[1, 2].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 2].SpanCols = 2;
            Grd1.Header.Cells[0, 2].Value = "Debit";
            Grd1.Header.Cells[0, 3].Value = "Credit";
            Grd1.Cols[2].Width = 130;
            Grd1.Cols[3].Width = 130;

            Grd1.Header.Cells[1, 4].Value = "Mutation";
            Grd1.Header.Cells[1, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 4].SpanCols = 2;
            Grd1.Header.Cells[0, 4].Value = "Debit";
            Grd1.Header.Cells[0, 5].Value = "Credit";
            Grd1.Cols[4].Width = 130;
            Grd1.Cols[5].Width = 130;

            Grd1.Header.Cells[1, 6].Value = "Closing Balance";
            Grd1.Header.Cells[1, 6].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 6].SpanCols = 2;
            Grd1.Header.Cells[0, 6].Value = "Debit";
            Grd1.Header.Cells[0, 7].Value = "Credit";
            Grd1.Cols[6].Width = 130;
            Grd1.Cols[7].Width = 130;

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7 }, 2);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPLOBAmt, TxtPLMAmt, TxtPLCBAmt }, 8);
            mProjectCode = string.Empty;
            mProjectName = string.Empty;

            if (IsFilterDataInvalid()) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProjectIncome>();
                var l2 = new List<CostCategory>();
                var l3 = new List<FinalData>();
                var l4 = new List<COA>();
                var l5 = new List<Journal>();
                string mPRJIDocNo = string.Empty;

                mPRJIDocNo = GetPRJIDocNo();
                mProjectCode = GetProjectCode(mPRJIDocNo);
                
                if (MeeProjectName.Text.Length > 0) mProjectName = MeeProjectName.Text;
                else mProjectName = GetProjectName(mPRJIDocNo);

                if(mRptProfitLossProjectFormat == "2")
                {
                    GetCOA(ref l4);
                    Process5(ref l4, ref l5, mProjectCode);
                    Process6(ref l4, ref l5, ref l3);
                }
                else
                {
                    Process1(ref l, mPRJIDocNo);
                    Process2(ref l2, mPRJIDocNo);

                    if (l.Count > 0) Process3a(ref l, ref l3);
                    if (l2.Count > 0) Process3b(ref l2, ref l3);
                }

                if (l3.Count > 0) Process4(ref l3);
                else Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear(); l2.Clear(); l3.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsFilterDataInvalid()
        {
            return
                IsProjectEmpty() ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month")
                ;
        }

        private bool IsProjectEmpty()
        {
            if (TxtLOPDocNo.Text.Length == 0 && TxtProjectCode.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to fill either Project or Project Code.");
                TxtLOPDocNo.Focus();
                return true;
            }

            return false;
        }

        override protected void PrintData()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Grd1.Rows.Count <= 1) return;
            {
                var l = new List<Hdr>();
                var ld = new List<Dtl>();

                string[] TableName = { "Hdr", "Dtl" };
                List<IList> myLists = new List<IList>();

                #region Header

                l.Add(new Hdr()
                {
                    ProjectName = mProjectName,
                    ProjectCode = mProjectCode,
                    DocDt = string.Concat(Sm.GetLue(LueMth), "/", Sm.GetLue(LueYr)),
                    SubOBDebitAmt = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 2),
                    SubOBCreditAmt = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 3),
                    SubMDebitAmt = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 4),
                    SubMCreditAmt = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 5),
                    SubCBDebitAmt = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 6),
                    SubCBCreditAmt = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 7),
                    PLOBAmt = Decimal.Parse(TxtPLOBAmt.Text),
                    PLMAmt = Decimal.Parse(TxtPLMAmt.Text),
                    PLCBAmt = Decimal.Parse(TxtPLCBAmt.Text),
                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                });

                myLists.Add(l);

                #endregion

                #region Detail

                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        ld.Add(new Dtl()
                        {
                            No = Sm.GetGrdStr(Grd1, i, 0),
                            Description = Sm.GetGrdStr(Grd1, i, 1),
                            OBDebit = Sm.GetGrdDec(Grd1, i, 2),
                            OBCredit = Sm.GetGrdDec(Grd1, i, 3),
                            MDebit = Sm.GetGrdDec(Grd1, i, 4),
                            MCredit = Sm.GetGrdDec(Grd1, i, 5),
                            CBDebit = Sm.GetGrdDec(Grd1, i, 6),
                            CBCredit = Sm.GetGrdDec(Grd1, i, 7),
                        });
                    }
                }
                myLists.Add(ld);

                #endregion

                Sm.PrintReport("ProfitLossProject", myLists, TableName, false);
            }
        }

        #region Additional Methods

        private void GetParameter()
        {
            mProjectIncomeDesc = Sm.GetParameter("ProjectIncomeDesc");
            mAcNoForDODeptLOPCC = Sm.GetParameter("AcNoForDODeptLOPCC");
            mRptProfitLossProjectFormat = Sm.GetParameter("RptProfitLossProjectFormat");

            if (mProjectIncomeDesc.Length == 0) mProjectIncomeDesc = "Project Income";
            if (mRptProfitLossProjectFormat.Length == 0) mRptProfitLossProjectFormat = "1";
        }

        private string GetPRJIDocNo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty;

            SQL.AppendLine("Select E.DocNo ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            if (TxtLOPDocNo.Text.Length > 0)
                SQL.AppendLine("    And A.DocNo Like @LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.DocNo = C.BOQDocNo ");
            if (TxtProjectCode.Text.Length > 0)
                SQL.AppendLine("    And C.ProjectCode2 = @ProjectCode ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr D On C.DocNo = D.SOCDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr E On D.DocNo = E.SOContractDocNo And E.CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                Sm.CmParam<String>(ref cm, "@LOPDocNo", string.Concat("%", TxtLOPDocNo.Text, "%"));
                Sm.CmParam<String>(ref cm, "@ProjectCode", TxtProjectCode.Text);

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = dr.GetString(0);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private string GetProjectCode(string PRJIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.ProjectCode2 ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), PRJIDocNo);
        }

        private string GetProjectName(string PRJIDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select E.ProjectName ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), PRJIDocNo);
        }

        private void GetCOA(ref List<COA> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.OptCode, B.AcNo, B.AcDesc, B.AcType ");
            SQL.AppendLine("FROM TblOption A ");
            SQL.AppendLine("INNER JOIN TblCOA B ON A.Property1 = B.AcNo ");
            SQL.AppendLine("WHERE A.OptCat = 'RptProfitLossProjectAcNo' ");
            SQL.AppendLine("Order By A.OptCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "AcType", "OptCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            Code = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process1(ref List<ProjectIncome> l, string PRJIDocNo)
        {
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL2.AppendLine("SELECT T.Description, SUM(T.OBDebit) OBDebit, SUM(T.OBCredit) OBCredit, SUM(T.MDebit) MDebit, SUM(T.MCredit) MCredit ");
            SQL2.AppendLine("FROM ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("    SELECT 'Project Income' AS Description, 0.00 AS OBDebit, B.SettledAmt AS OBCredit, 0.00 AS MDebit, 0.00 AS MCredit ");
	        SQL2.AppendLine("    FROM TblProjectImplementationHdr A ");
	        SQL2.AppendLine("    INNER JOIN TblProjectImplementationDtl B ON A.DocNo = B.DocNo AND A.DocNo = @DocNo ");
	        SQL2.AppendLine("        AND B.SettledInd = 'Y' AND B.SettleDt IS NOT NULL ");
	        SQL2.AppendLine("        AND LEFT(B.SettleDt, 4) < @Yr ");
            	    
	        SQL2.AppendLine("    UNION ALL ");

            SQL2.AppendLine("    SELECT 'Project Income' AS Description, 0.00 AS OBDebit, 0.00 AS OBCredit, 0.00 AS MDebit, B.SettledAmt AS MCredit ");
	        SQL2.AppendLine("    FROM TblProjectImplementationHdr A ");
	        SQL2.AppendLine("    INNER JOIN TblProjectImplementationDtl B ON A.DocNo = B.DocNo AND A.DocNo = @DocNo ");
	        SQL2.AppendLine("        AND B.SettledInd = 'Y' AND B.SettleDt IS NOT NULL ");
            SQL2.AppendLine("        AND LEFT(B.SettleDt, 4) = @Yr ");
            SQL2.AppendLine("        AND SUBSTR(B.SettleDt, 5, 2) <= @Mth ");
            SQL2.AppendLine(")T ");
            SQL2.AppendLine("GROUP BY T.Description; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                Sm.CmParam<String>(ref cm, "@DocNo", PRJIDocNo);
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL2.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OBDebit", "OBCredit", "MDebit", "MCredit" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProjectIncome()
                        {
                            Description = mProjectIncomeDesc,
                            OBDebit = Sm.DrDec(dr, c[0]),
                            OBCredit = Sm.DrDec(dr, c[1]),
                            MDebit = Sm.DrDec(dr, c[2]),
                            MCredit = Sm.DrDec(dr, c[3]),
                            CBDebit = (Sm.DrDec(dr, c[0]) + Sm.DrDec(dr, c[2])),
                            CBCredit = (Sm.DrDec(dr, c[1]) + Sm.DrDec(dr, c[3]))
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<CostCategory> l2, string PRJIDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT T.Description, SUM(T.OBDebit) OBDebit, SUM(T.OBCredit) OBCredit, SUM(T.MDebit) MDebit, SUM(T.MCredit) MCredit ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T2.CCtName AS Description, T1.OBDebit, T1.OBCredit, T1.MDebit, T1.MCredit ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        SELECT D.CCtCode, (A.RemunerationAmt + A.DirectCostAmt) OBDebit, 0.00 AS OBCredit, 0.00 AS MDebit, 0.00 AS MCredit ");
	        SQL.AppendLine("        FROM TblDroppingRequestDtl A ");
	        SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr B ON A.DocNo = B.DocNo AND LEFT(B.DocDt, 4) < @Yr ");
	        SQL.AppendLine("            AND B.CancelInd = 'N' AND B.Status = 'A' ");
	        SQL.AppendLine("        INNER JOIN TblProjectImplementationRBPHdr C ON A.PRBPDocNo = C.DocNo AND C.PRJIDocNo = @DocNo ");
	        SQL.AppendLine("        INNER JOIN TblItemCostCategory D ON C.ResourceItCode = D.ItCode AND D.CCCode = @CCCode ");
            		
	        SQL.AppendLine("        UNION ALL ");
            		
	        SQL.AppendLine("        SELECT D.CCtCode, 0.00 OBDebit, 0.00 AS OBCredit, (A.RemunerationAmt + A.DirectCostAmt) AS MDebit, 0.00 AS MCredit ");
	        SQL.AppendLine("        FROM TblDroppingRequestDtl A ");
	        SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr B ON A.DocNo = B.DocNo AND LEFT(B.DocDt, 4) = @Yr AND SUBSTR(B.DocDt, 5, 2) <= @Mth ");
	        SQL.AppendLine("            AND B.CancelInd = 'N' AND B.Status = 'A' ");
	        SQL.AppendLine("        INNER JOIN TblProjectImplementationRBPHdr C ON A.PRBPDocNo = C.DocNo AND C.PRJIDocNo = @DocNo ");
	        SQL.AppendLine("        INNER JOIN TblItemCostCategory D ON C.ResourceItCode = D.ItCode AND D.CCCode = @CCCode ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    INNER JOIN TblCostCategory T2 ON T1.CCtCode = T2.CCtCode ");
            	
            SQL.AppendLine("    UNION ALL ");
            	
            SQL.AppendLine("    SELECT 'Miscellaneous' AS Description, X1.OBDebit, X1.OBCredit, X1.MDebit, X1.MCredit ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        SELECT (A.RemunerationAmt + A.DirectCostAmt) OBDebit, 0.00 AS OBCredit, 0.00 AS MDebit, 0.00 AS MCredit ");
	        SQL.AppendLine("        FROM TblDroppingRequestDtl A ");
	        SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr B ON A.DocNo = B.DocNo AND LEFT(B.DocDt, 4) < @Yr ");
	        SQL.AppendLine("            AND B.CancelInd = 'N' AND B.Status = 'A' ");
	        SQL.AppendLine("        INNER JOIN TblProjectImplementationRBPHdr C ON A.PRBPDocNo = C.DocNo AND C.PRJIDocNo = @DocNo ");
	        SQL.AppendLine("            AND C.ResourceItCode NOT IN (SELECT DISTINCT ItCode FROM TblItemCostCategory WHERE CCCode = @CCCode) ");
            		
	        SQL.AppendLine("        UNION ALL ");
            		
	        SQL.AppendLine("        SELECT 0.00 OBDebit, 0.00 AS OBCredit, (A.RemunerationAmt + A.DirectCostAmt) AS MDebit, 0.00 AS MCredit ");
	        SQL.AppendLine("        FROM TblDroppingRequestDtl A ");
	        SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr B ON A.DocNo = B.DocNo AND LEFT(B.DocDt, 4) = @Yr AND SUBSTR(B.DocDt, 5, 2) <= @Mth ");
	        SQL.AppendLine("            AND B.CancelInd = 'N' AND B.Status = 'A' ");
	        SQL.AppendLine("        INNER JOIN TblProjectImplementationRBPHdr C ON A.PRBPDocNo = C.DocNo AND C.PRJIDocNo = @DocNo ");
	        SQL.AppendLine("            AND C.ResourceItCode NOT IN (SELECT DISTINCT ItCode FROM TblItemCostCategory WHERE CCCode = @CCCode) ");
            SQL.AppendLine("    ) X1 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.Description; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                Sm.CmParam<String>(ref cm, "@DocNo", PRJIDocNo);
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@CCCode", mAcNoForDODeptLOPCC);

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Description", "OBDebit", "OBCredit", "MDebit", "MCredit" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new CostCategory()
                        {
                            Description = Sm.DrStr(dr, c[0]),
                            OBDebit = Sm.DrDec(dr, c[1]),
                            OBCredit = Sm.DrDec(dr, c[2]),
                            MDebit = Sm.DrDec(dr, c[3]),
                            MCredit = Sm.DrDec(dr, c[4]),
                            CBDebit = (Sm.DrDec(dr, c[1]) + Sm.DrDec(dr, c[3])),
                            CBCredit = (Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[4]))
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3a(ref List<ProjectIncome> l, ref List<FinalData> l3)
        {
            for (int i = 0; i < l.Count; i++)
            {
                l3.Add(new FinalData()
                {
                    Description = l[i].Description,
                    OBDebit = l[i].OBDebit,
                    OBCredit = l[i].OBCredit,
                    MDebit = l[i].MDebit,
                    MCredit = l[i].MCredit,
                    CBDebit = l[i].CBDebit,
                    CBCredit = l[i].CBCredit
                });
            }
        }

        private void Process3b(ref List<CostCategory> l2, ref List<FinalData> l3)
        {
            for (int i = 0; i < l2.Count; i++)
            {
                l3.Add(new FinalData()
                {
                    Description = l2[i].Description,
                    OBDebit = l2[i].OBDebit,
                    OBCredit = l2[i].OBCredit,
                    MDebit = l2[i].MDebit,
                    MCredit = l2[i].MCredit,
                    CBDebit = l2[i].CBDebit,
                    CBCredit = l2[i].CBCredit
                });
            }
        }

        private void Process4(ref List<FinalData> l3)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 1;
            for (int i = 0; i < l3.Count; i++)
            {
                Grd1.Rows.Add();
                Grd1.Cells[i, 0].Value = No;
                Grd1.Cells[i, 1].Value = l3[i].Description;
                Grd1.Cells[i, 2].Value = l3[i].OBDebit;
                Grd1.Cells[i, 3].Value = l3[i].OBCredit;
                Grd1.Cells[i, 4].Value = l3[i].MDebit;
                Grd1.Cells[i, 5].Value = l3[i].MCredit;
                Grd1.Cells[i, 6].Value = l3[i].CBDebit;
                Grd1.Cells[i, 7].Value = l3[i].CBCredit;
                No += 1;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 3, 4, 5, 6, 7 });

            ComputePL();
        }

        private void Process5(ref List<COA> l, ref List<Journal> l1, string mProjectCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.AcNo, B.AcType, SUM(OBDebit) OBDebit, SUM(OBCredit) OBCredit, SUM(MDebit) MDebit, SUM(MCredit) MCredit ");
            SQL.AppendLine("FROM ( ");
            foreach(var x in l)
            {
                if (x.Code != "01")
                    SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("    SELECT AcNo, SUM(OBDebit) OBDebit, SUM(OBCredit) OBCredit, SUM(MDebit) MDebit, SUM(MCredit) MCredit ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("	    Select ");
                SQL.AppendLine("	    case ");
                SQL.AppendLine("		    When B.AcNo LIKE '" + x.AcNo + "%' then '" + x.AcNo + "' ");
                SQL.AppendLine("	    END AS AcNo, B.DAmt OBDebit, B.CAmt OBCredit, 0.00 MDebit, 0.00 MCredit ");
                SQL.AppendLine("	    FROM TblJournalHdr A ");
                SQL.AppendLine("	    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
                SQL.AppendLine("	    WHERE LEFT(A.DocDt, 4) < @Yr ");
                SQL.AppendLine("        AND B.AcNo Like '%" + mProjectCode + "' ");
                SQL.AppendLine("	    AND B.AcNo LIKE '"+x.AcNo+"%' ");
                SQL.AppendLine("	    UNION ALL ");
                SQL.AppendLine("	    SELECT ");
                SQL.AppendLine("	    case ");
                SQL.AppendLine("		    When B.AcNo LIKE '"+x.AcNo+"%' then '"+x.AcNo+"' ");
                SQL.AppendLine("	    END AS AcNo, 0.00 OBDebit, 0.00 OBCredit, B.DAmt MDebit, B.CAmt MCredit ");
                SQL.AppendLine("	    FROM TblJournalHdr A ");
                SQL.AppendLine("	    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
                SQL.AppendLine("	    WHERE LEFT(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        AND SUBSTR(A.DocDt, 5, 2) <= @Mth ");
                SQL.AppendLine("        AND B.AcNo Like '%" + mProjectCode + "' ");
                SQL.AppendLine("        AND B.AcNo LIKE '"+x.AcNo+"%' ");
                SQL.AppendLine("    )T"+x.Code+" ");
            }
            SQL.AppendLine(")A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("GROUP BY A.AcNo, B.AcType ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                Sm.CmParam<String>(ref cm, "@ProjectCode", mProjectCode);
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType", "OBDebit", "OBCredit", "MDebit", "MCredit" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new Journal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            OBDebit = Sm.DrDec(dr, c[2]),
                            OBCredit = Sm.DrDec(dr, c[3]),
                            MDebit = Sm.DrDec(dr, c[4]),
                            MCredit = Sm.DrDec(dr, c[5]),
                            CBCredit = (Sm.DrStr(dr, c[1]) == "C" ? ((Sm.DrDec(dr, c[3]) + Sm.DrDec(dr, c[5])) - (Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[4]))) : 0),
                            CBDebit = (Sm.DrStr(dr, c[1]) == "D" ? ((Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[4])) - (Sm.DrDec(dr, c[3]) + Sm.DrDec(dr, c[5]))) : 0),
                        }) ;
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<COA> l, ref List<Journal> l1, ref List<FinalData> l3)
        {
            foreach (var i in l)
            {
                foreach (var j in l1)
                {
                    if (i.AcNo == j.AcNo)
                    {
                        l3.Add(new FinalData()
                        {
                            Description = i.AcDesc,
                            OBDebit = j.OBDebit,
                            OBCredit = j.OBCredit,
                            MDebit = j.MDebit,
                            MCredit = j.MCredit,
                            CBDebit = j.CBDebit,
                            CBCredit = j.CBCredit
                        });
                    }
                }
            }
        }

        private void ComputePL()
        {
            decimal mPLOBCreditAmt = 0m, mPLOBDebitAmt = 0m, 
                mPLMCreditAmt = 0m, mPLMDebitAmt = 0m,
                mPLCBCreditAmt = 0m, mPLCBDebitAmt = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    mPLOBDebitAmt += Sm.GetGrdDec(Grd1, i, 2);
                    mPLOBCreditAmt += Sm.GetGrdDec(Grd1, i, 3);
                    mPLMDebitAmt += Sm.GetGrdDec(Grd1, i, 4);
                    mPLMCreditAmt += Sm.GetGrdDec(Grd1, i, 5);
                    mPLCBDebitAmt += Sm.GetGrdDec(Grd1, i, 6);
                    mPLCBCreditAmt += Sm.GetGrdDec(Grd1, i, 7);
                }
            }

            TxtPLOBAmt.EditValue = Sm.FormatNum((mPLOBCreditAmt - mPLOBDebitAmt), 8);
            TxtPLMAmt.EditValue = Sm.FormatNum((mPLMCreditAmt - mPLMDebitAmt), 8);
            TxtPLCBAmt.EditValue = Sm.FormatNum((mPLCBCreditAmt - mPLCBDebitAmt), 8);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion       

        #region Events

        #region Misc Control Events

        private void TxtLOPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLOPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
            if (!ChkLOPDocNo.Checked)
            {
                MeeProjectName.EditValue = string.Empty;
            }
        }

        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRptProfitLossProjectDlg(this));
        }

        private void BtnLOPDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtLOPDocNo, "Project", false))
            {
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtLOPDocNo.Text;
                f.ShowDialog();
            }
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Code");
        }

        private void TxtPLOBAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPLOBAmt, 8);
        }

        private void TxtPLMAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPLMAmt, 8);
        }

        private void TxtPLCBAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtPLCBAmt, 8);
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string Code { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
        }

        private class ProjectIncome
        {
            public string Description { get; set; }
            public decimal OBDebit { get; set; }
            public decimal OBCredit { get; set; }
            public decimal MDebit { get; set; }
            public decimal MCredit { get; set; }
            public decimal CBDebit { get; set; }
            public decimal CBCredit { get; set; }
        }

        private class CostCategory
        {
            public string Description { get; set; }
            public decimal OBDebit { get; set; }
            public decimal OBCredit { get; set; }
            public decimal MDebit { get; set; }
            public decimal MCredit { get; set; }
            public decimal CBDebit { get; set; }
            public decimal CBCredit { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal OBDebit { get; set; }
            public decimal OBCredit { get; set; }
            public decimal MDebit { get; set; }
            public decimal MCredit { get; set; }
            public decimal CBDebit { get; set; }
            public decimal CBCredit { get; set; }
        }

        private class FinalData
        {
            public string Description { get; set; }
            public decimal OBDebit { get; set; }
            public decimal OBCredit { get; set; }
            public decimal MDebit { get; set; }
            public decimal MCredit { get; set; }
            public decimal CBDebit { get; set; }
            public decimal CBCredit { get; set; }
        }

        private class Hdr
        {
            public string ProjectName { get; set; }
            public string ProjectCode { get; set; }
            public string DocDt { get; set; }
            public decimal SubOBDebitAmt { get; set; }
            public decimal SubOBCreditAmt { get; set; }
            public decimal SubMDebitAmt { get; set; }
            public decimal SubMCreditAmt { get; set; }
            public decimal SubCBDebitAmt { get; set; }
            public decimal SubCBCreditAmt { get; set; }
            public decimal PLOBAmt { get; set; }
            public decimal PLMAmt { get; set; }
            public decimal PLCBAmt { get; set; }
            public string PrintBy { get; set; }
        }

        private class Dtl
        {
            public string No { get; set; }
            public string Description { get; set; }
            public decimal OBDebit { get; set; }
            public decimal OBCredit { get; set; }
            public decimal MDebit { get; set; }
            public decimal MCredit { get; set; }
            public decimal CBDebit { get; set; }
            public decimal CBCredit { get; set; }
        }

        #endregion

    }
}
