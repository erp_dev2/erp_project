﻿namespace RunSystem
{
    partial class FrmDOCt3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDOCt3));
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TcDOCt3 = new DevExpress.XtraTab.XtraTabControl();
            this.TpGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.label21 = new System.Windows.Forms.Label();
            this.LblInspectionSheet = new System.Windows.Forms.Label();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtInspectionSheet = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtProductDeliveryNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCtReturnNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCtDONo = new DevExpress.XtraEditors.TextEdit();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtSAName = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.BtnCustomerShipAddress = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.TxtCountry = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.MeeSAAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtCity = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnSAName = new DevExpress.XtraEditors.SimpleButton();
            this.LblSAName = new System.Windows.Forms.Label();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnRecvCtDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRecvCtDocNo1 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRecvCtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TpItem = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnRecvCt2 = new System.Windows.Forms.Button();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TcDOCt3)).BeginInit();
            this.TcDOCt3.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInspectionSheet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductDeliveryNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReturnNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtDONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSAAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvCtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.TpItem.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 610);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 610);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 31);
            this.panel3.TabIndex = 10;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(83, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(229, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(9, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TcDOCt3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 31);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 579);
            this.panel4.TabIndex = 1;
            // 
            // TcDOCt3
            // 
            this.TcDOCt3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcDOCt3.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcDOCt3.Location = new System.Drawing.Point(0, 0);
            this.TcDOCt3.Name = "TcDOCt3";
            this.TcDOCt3.SelectedTabPage = this.TpGeneral;
            this.TcDOCt3.Size = new System.Drawing.Size(772, 579);
            this.TcDOCt3.TabIndex = 13;
            this.TcDOCt3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpGeneral,
            this.TpItem});
            // 
            // TpGeneral
            // 
            this.TpGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGeneral.Controls.Add(this.BtnFile);
            this.TpGeneral.Controls.Add(this.label21);
            this.TpGeneral.Controls.Add(this.LblInspectionSheet);
            this.TpGeneral.Controls.Add(this.PbUpload3);
            this.TpGeneral.Controls.Add(this.PbUpload2);
            this.TpGeneral.Controls.Add(this.TxtInspectionSheet);
            this.TpGeneral.Controls.Add(this.ChkFile3);
            this.TpGeneral.Controls.Add(this.label19);
            this.TpGeneral.Controls.Add(this.BtnDownload3);
            this.TpGeneral.Controls.Add(this.TxtProductDeliveryNo);
            this.TpGeneral.Controls.Add(this.TxtFile3);
            this.TpGeneral.Controls.Add(this.label16);
            this.TpGeneral.Controls.Add(this.BtnFile3);
            this.TpGeneral.Controls.Add(this.TxtCtReturnNo);
            this.TpGeneral.Controls.Add(this.ChkFile2);
            this.TpGeneral.Controls.Add(this.label18);
            this.TpGeneral.Controls.Add(this.BtnDownload2);
            this.TpGeneral.Controls.Add(this.TxtCtDONo);
            this.TpGeneral.Controls.Add(this.TxtFile2);
            this.TpGeneral.Controls.Add(this.TxtSAName);
            this.TpGeneral.Controls.Add(this.label20);
            this.TpGeneral.Controls.Add(this.BtnCustomerShipAddress);
            this.TpGeneral.Controls.Add(this.BtnFile2);
            this.TpGeneral.Controls.Add(this.LueWhsCode);
            this.TpGeneral.Controls.Add(this.ChkFile);
            this.TpGeneral.Controls.Add(this.label17);
            this.TpGeneral.Controls.Add(this.BtnDownload);
            this.TpGeneral.Controls.Add(this.TxtJournalDocNo2);
            this.TpGeneral.Controls.Add(this.PbUpload);
            this.TpGeneral.Controls.Add(this.TxtJournalDocNo);
            this.TpGeneral.Controls.Add(this.TxtFile);
            this.TpGeneral.Controls.Add(this.label9);
            this.TpGeneral.Controls.Add(this.label22);
            this.TpGeneral.Controls.Add(this.label6);
            this.TpGeneral.Controls.Add(this.TxtMobile);
            this.TpGeneral.Controls.Add(this.TxtCountry);
            this.TpGeneral.Controls.Add(this.label10);
            this.TpGeneral.Controls.Add(this.TxtEmail);
            this.TpGeneral.Controls.Add(this.label7);
            this.TpGeneral.Controls.Add(this.TxtFax);
            this.TpGeneral.Controls.Add(this.label11);
            this.TpGeneral.Controls.Add(this.TxtPhone);
            this.TpGeneral.Controls.Add(this.TxtPostalCd);
            this.TpGeneral.Controls.Add(this.label14);
            this.TpGeneral.Controls.Add(this.label12);
            this.TpGeneral.Controls.Add(this.MeeRemark);
            this.TpGeneral.Controls.Add(this.label8);
            this.TpGeneral.Controls.Add(this.MeeSAAddress);
            this.TpGeneral.Controls.Add(this.TxtCity);
            this.TpGeneral.Controls.Add(this.label15);
            this.TpGeneral.Controls.Add(this.label13);
            this.TpGeneral.Controls.Add(this.BtnSAName);
            this.TpGeneral.Controls.Add(this.LblSAName);
            this.TpGeneral.Controls.Add(this.TxtCtCode);
            this.TpGeneral.Controls.Add(this.label5);
            this.TpGeneral.Controls.Add(this.label4);
            this.TpGeneral.Controls.Add(this.label27);
            this.TpGeneral.Controls.Add(this.ChkCancelInd);
            this.TpGeneral.Controls.Add(this.MeeCancelReason);
            this.TpGeneral.Controls.Add(this.BtnRecvCtDocNo2);
            this.TpGeneral.Controls.Add(this.BtnRecvCtDocNo1);
            this.TpGeneral.Controls.Add(this.TxtRecvCtDocNo);
            this.TpGeneral.Controls.Add(this.label3);
            this.TpGeneral.Controls.Add(this.DteDocDt);
            this.TpGeneral.Controls.Add(this.label2);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Size = new System.Drawing.Size(766, 551);
            this.TpGeneral.Text = "General";
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(606, 425);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 71;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(119, 512);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 14);
            this.label21.TabIndex = 80;
            this.label21.Text = "File 3";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblInspectionSheet
            // 
            this.LblInspectionSheet.AutoSize = true;
            this.LblInspectionSheet.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInspectionSheet.ForeColor = System.Drawing.Color.Black;
            this.LblInspectionSheet.Location = new System.Drawing.Point(44, 342);
            this.LblInspectionSheet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInspectionSheet.Name = "LblInspectionSheet";
            this.LblInspectionSheet.Size = new System.Drawing.Size(110, 14);
            this.LblInspectionSheet.TabIndex = 60;
            this.LblInspectionSheet.Text = "Inspection Sheet#";
            this.LblInspectionSheet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(161, 530);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(500, 17);
            this.PbUpload3.TabIndex = 85;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(161, 489);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(500, 17);
            this.PbUpload2.TabIndex = 79;
            // 
            // TxtInspectionSheet
            // 
            this.TxtInspectionSheet.EnterMoveNextControl = true;
            this.TxtInspectionSheet.Location = new System.Drawing.Point(161, 339);
            this.TxtInspectionSheet.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInspectionSheet.Name = "TxtInspectionSheet";
            this.TxtInspectionSheet.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInspectionSheet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInspectionSheet.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInspectionSheet.Properties.Appearance.Options.UseFont = true;
            this.TxtInspectionSheet.Properties.MaxLength = 30;
            this.TxtInspectionSheet.Size = new System.Drawing.Size(229, 20);
            this.TxtInspectionSheet.TabIndex = 61;
            this.TxtInspectionSheet.Validated += new System.EventHandler(this.TxtInspectionSheet_Validated);
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(578, 508);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 82;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(49, 321);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(105, 14);
            this.label19.TabIndex = 58;
            this.label19.Text = "Product Delivery#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(637, 508);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 84;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // TxtProductDeliveryNo
            // 
            this.TxtProductDeliveryNo.EnterMoveNextControl = true;
            this.TxtProductDeliveryNo.Location = new System.Drawing.Point(161, 318);
            this.TxtProductDeliveryNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductDeliveryNo.Name = "TxtProductDeliveryNo";
            this.TxtProductDeliveryNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProductDeliveryNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductDeliveryNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductDeliveryNo.Properties.Appearance.Options.UseFont = true;
            this.TxtProductDeliveryNo.Properties.MaxLength = 30;
            this.TxtProductDeliveryNo.Size = new System.Drawing.Size(229, 20);
            this.TxtProductDeliveryNo.TabIndex = 59;
            this.TxtProductDeliveryNo.Validated += new System.EventHandler(this.TxtProductDeliveryNo_Validated);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(161, 508);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(410, 20);
            this.TxtFile3.TabIndex = 81;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(37, 300);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 14);
            this.label16.TabIndex = 56;
            this.label16.Text = "Customer\'s Return#";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(606, 508);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 83;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // TxtCtReturnNo
            // 
            this.TxtCtReturnNo.EnterMoveNextControl = true;
            this.TxtCtReturnNo.Location = new System.Drawing.Point(161, 297);
            this.TxtCtReturnNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtReturnNo.Name = "TxtCtReturnNo";
            this.TxtCtReturnNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtReturnNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtReturnNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtReturnNo.Properties.Appearance.Options.UseFont = true;
            this.TxtCtReturnNo.Properties.MaxLength = 30;
            this.TxtCtReturnNo.Size = new System.Drawing.Size(229, 20);
            this.TxtCtReturnNo.TabIndex = 57;
            this.TxtCtReturnNo.Validated += new System.EventHandler(this.TxtCtReturnNo_Validated);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(578, 466);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 76;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(57, 279);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 14);
            this.label18.TabIndex = 54;
            this.label18.Text = "Customer\'s DO#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(637, 466);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 78;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // TxtCtDONo
            // 
            this.TxtCtDONo.EnterMoveNextControl = true;
            this.TxtCtDONo.Location = new System.Drawing.Point(161, 276);
            this.TxtCtDONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtDONo.Name = "TxtCtDONo";
            this.TxtCtDONo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtDONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtDONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtDONo.Properties.Appearance.Options.UseFont = true;
            this.TxtCtDONo.Properties.MaxLength = 30;
            this.TxtCtDONo.Size = new System.Drawing.Size(229, 20);
            this.TxtCtDONo.TabIndex = 55;
            this.TxtCtDONo.Validated += new System.EventHandler(this.TxtCtDONo_Validated);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(161, 466);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(410, 20);
            this.TxtFile2.TabIndex = 75;
            // 
            // TxtSAName
            // 
            this.TxtSAName.EnterMoveNextControl = true;
            this.TxtSAName.Location = new System.Drawing.Point(161, 109);
            this.TxtSAName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSAName.Name = "TxtSAName";
            this.TxtSAName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSAName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSAName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSAName.Properties.Appearance.Options.UseFont = true;
            this.TxtSAName.Properties.MaxLength = 20;
            this.TxtSAName.Properties.ReadOnly = true;
            this.TxtSAName.Size = new System.Drawing.Size(440, 20);
            this.TxtSAName.TabIndex = 52;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(119, 470);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 14);
            this.label20.TabIndex = 74;
            this.label20.Text = "File 2";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCustomerShipAddress
            // 
            this.BtnCustomerShipAddress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCustomerShipAddress.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCustomerShipAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCustomerShipAddress.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCustomerShipAddress.Appearance.Options.UseBackColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseFont = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseForeColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseTextOptions = true;
            this.BtnCustomerShipAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCustomerShipAddress.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCustomerShipAddress.Image = ((System.Drawing.Image)(resources.GetObject("BtnCustomerShipAddress.Image")));
            this.BtnCustomerShipAddress.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCustomerShipAddress.Location = new System.Drawing.Point(603, 109);
            this.BtnCustomerShipAddress.Name = "BtnCustomerShipAddress";
            this.BtnCustomerShipAddress.Size = new System.Drawing.Size(24, 21);
            this.BtnCustomerShipAddress.TabIndex = 53;
            this.BtnCustomerShipAddress.ToolTip = "Find Customer\'s Shipping Information";
            this.BtnCustomerShipAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCustomerShipAddress.ToolTipTitle = "Run System";
            this.BtnCustomerShipAddress.Click += new System.EventHandler(this.BtnCustomerShipAddress_Click);
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(606, 466);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 77;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(161, 46);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 25;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(440, 20);
            this.LueWhsCode.TabIndex = 20;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(578, 425);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 70;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(392, 405);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 14);
            this.label17.TabIndex = 67;
            this.label17.Text = "(Cancel Data)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(637, 425);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 72;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(161, 403);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(229, 20);
            this.TxtJournalDocNo2.TabIndex = 66;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(161, 447);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(500, 17);
            this.PbUpload.TabIndex = 73;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(161, 382);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(229, 20);
            this.TxtJournalDocNo.TabIndex = 65;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(161, 425);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(410, 20);
            this.TxtFile.TabIndex = 69;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(100, 385);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 64;
            this.label9.Text = "Journal#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(130, 429);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 14);
            this.label22.TabIndex = 68;
            this.label22.Text = "File";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(104, 175);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 14);
            this.label6.TabIndex = 34;
            this.label6.Text = "Country";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(161, 256);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Properties.ReadOnly = true;
            this.TxtMobile.Size = new System.Drawing.Size(440, 20);
            this.TxtMobile.TabIndex = 45;
            // 
            // TxtCountry
            // 
            this.TxtCountry.EnterMoveNextControl = true;
            this.TxtCountry.Location = new System.Drawing.Point(161, 172);
            this.TxtCountry.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCountry.Name = "TxtCountry";
            this.TxtCountry.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCountry.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCountry.Properties.Appearance.Options.UseFont = true;
            this.TxtCountry.Properties.MaxLength = 20;
            this.TxtCountry.Properties.ReadOnly = true;
            this.TxtCountry.Size = new System.Drawing.Size(265, 20);
            this.TxtCountry.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(113, 259);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 44;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(161, 235);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Properties.ReadOnly = true;
            this.TxtEmail.Size = new System.Drawing.Size(440, 20);
            this.TxtEmail.TabIndex = 43;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(120, 238);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 14);
            this.label7.TabIndex = 42;
            this.label7.Text = "Email";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(161, 214);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 40;
            this.TxtFax.Properties.ReadOnly = true;
            this.TxtFax.Size = new System.Drawing.Size(440, 20);
            this.TxtFax.TabIndex = 41;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(129, 217);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 14);
            this.label11.TabIndex = 40;
            this.label11.Text = "Fax";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(161, 193);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Properties.ReadOnly = true;
            this.TxtPhone.Size = new System.Drawing.Size(440, 20);
            this.TxtPhone.TabIndex = 39;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(505, 172);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Properties.ReadOnly = true;
            this.TxtPostalCd.Size = new System.Drawing.Size(96, 20);
            this.TxtPostalCd.TabIndex = 37;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(429, 175);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 14);
            this.label14.TabIndex = 36;
            this.label14.Text = "Postal Code";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(112, 196);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 38;
            this.label12.Text = "Phone";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(161, 361);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 450;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(440, 20);
            this.MeeRemark.TabIndex = 63;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(107, 364);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 62;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeSAAddress
            // 
            this.MeeSAAddress.EnterMoveNextControl = true;
            this.MeeSAAddress.Location = new System.Drawing.Point(161, 130);
            this.MeeSAAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSAAddress.Name = "MeeSAAddress";
            this.MeeSAAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeSAAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.Appearance.Options.UseBackColor = true;
            this.MeeSAAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSAAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSAAddress.Properties.MaxLength = 450;
            this.MeeSAAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSAAddress.Properties.ReadOnly = true;
            this.MeeSAAddress.Properties.ShowIcon = false;
            this.MeeSAAddress.Size = new System.Drawing.Size(440, 20);
            this.MeeSAAddress.TabIndex = 31;
            this.MeeSAAddress.ToolTip = "F4 : Show/hide text";
            this.MeeSAAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSAAddress.ToolTipTitle = "Run System";
            // 
            // TxtCity
            // 
            this.TxtCity.EnterMoveNextControl = true;
            this.TxtCity.Location = new System.Drawing.Point(161, 151);
            this.TxtCity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCity.Properties.Appearance.Options.UseFont = true;
            this.TxtCity.Properties.MaxLength = 20;
            this.TxtCity.Properties.ReadOnly = true;
            this.TxtCity.Size = new System.Drawing.Size(440, 20);
            this.TxtCity.TabIndex = 33;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(104, 133);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 30;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(127, 154);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 14);
            this.label13.TabIndex = 32;
            this.label13.Text = "City";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSAName
            // 
            this.BtnSAName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSAName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSAName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSAName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSAName.Appearance.Options.UseBackColor = true;
            this.BtnSAName.Appearance.Options.UseFont = true;
            this.BtnSAName.Appearance.Options.UseForeColor = true;
            this.BtnSAName.Appearance.Options.UseTextOptions = true;
            this.BtnSAName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSAName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSAName.Image = ((System.Drawing.Image)(resources.GetObject("BtnSAName.Image")));
            this.BtnSAName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSAName.Location = new System.Drawing.Point(630, 109);
            this.BtnSAName.Name = "BtnSAName";
            this.BtnSAName.Size = new System.Drawing.Size(24, 21);
            this.BtnSAName.TabIndex = 29;
            this.BtnSAName.ToolTip = "Add Customer\'s Shipping Address";
            this.BtnSAName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSAName.ToolTipTitle = "Run System";
            this.BtnSAName.Click += new System.EventHandler(this.BtnSAName_Click);
            // 
            // LblSAName
            // 
            this.LblSAName.AutoSize = true;
            this.LblSAName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSAName.ForeColor = System.Drawing.Color.Black;
            this.LblSAName.Location = new System.Drawing.Point(66, 112);
            this.LblSAName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSAName.Name = "LblSAName";
            this.LblSAName.Size = new System.Drawing.Size(88, 14);
            this.LblSAName.TabIndex = 27;
            this.LblSAName.Text = "Shipping Name";
            this.LblSAName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(161, 88);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 30;
            this.TxtCtCode.Properties.ReadOnly = true;
            this.TxtCtCode.Size = new System.Drawing.Size(440, 20);
            this.TxtCtCode.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(95, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 25;
            this.label5.Text = "Customer";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(85, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Warehouse";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(19, 28);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(135, 14);
            this.label27.TabIndex = 16;
            this.label27.Text = "Reason For Cancellation";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(603, 24);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(161, 25);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 450;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(440, 20);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // BtnRecvCtDocNo2
            // 
            this.BtnRecvCtDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvCtDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvCtDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvCtDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvCtDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnRecvCtDocNo2.Appearance.Options.UseFont = true;
            this.BtnRecvCtDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnRecvCtDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnRecvCtDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvCtDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvCtDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvCtDocNo2.Image")));
            this.BtnRecvCtDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvCtDocNo2.Location = new System.Drawing.Point(630, 67);
            this.BtnRecvCtDocNo2.Name = "BtnRecvCtDocNo2";
            this.BtnRecvCtDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvCtDocNo2.TabIndex = 24;
            this.BtnRecvCtDocNo2.ToolTip = "Show Receiving Item From Customer Document";
            this.BtnRecvCtDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvCtDocNo2.ToolTipTitle = "Run System";
            this.BtnRecvCtDocNo2.Click += new System.EventHandler(this.BtnRecvCtDocNo2_Click);
            // 
            // BtnRecvCtDocNo1
            // 
            this.BtnRecvCtDocNo1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvCtDocNo1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvCtDocNo1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvCtDocNo1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvCtDocNo1.Appearance.Options.UseBackColor = true;
            this.BtnRecvCtDocNo1.Appearance.Options.UseFont = true;
            this.BtnRecvCtDocNo1.Appearance.Options.UseForeColor = true;
            this.BtnRecvCtDocNo1.Appearance.Options.UseTextOptions = true;
            this.BtnRecvCtDocNo1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvCtDocNo1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvCtDocNo1.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvCtDocNo1.Image")));
            this.BtnRecvCtDocNo1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvCtDocNo1.Location = new System.Drawing.Point(603, 67);
            this.BtnRecvCtDocNo1.Name = "BtnRecvCtDocNo1";
            this.BtnRecvCtDocNo1.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvCtDocNo1.TabIndex = 23;
            this.BtnRecvCtDocNo1.ToolTip = "Find Receiving Item From Customer Document";
            this.BtnRecvCtDocNo1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvCtDocNo1.ToolTipTitle = "Run System";
            this.BtnRecvCtDocNo1.Click += new System.EventHandler(this.BtnRecvCtDocNo1_Click);
            // 
            // TxtRecvCtDocNo
            // 
            this.TxtRecvCtDocNo.EnterMoveNextControl = true;
            this.TxtRecvCtDocNo.Location = new System.Drawing.Point(161, 67);
            this.TxtRecvCtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecvCtDocNo.Name = "TxtRecvCtDocNo";
            this.TxtRecvCtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecvCtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecvCtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecvCtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRecvCtDocNo.Properties.MaxLength = 30;
            this.TxtRecvCtDocNo.Properties.ReadOnly = true;
            this.TxtRecvCtDocNo.Size = new System.Drawing.Size(440, 20);
            this.TxtRecvCtDocNo.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(89, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Received#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(161, 4);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(135, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(121, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpItem
            // 
            this.TpItem.Appearance.Header.Options.UseTextOptions = true;
            this.TpItem.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpItem.Controls.Add(this.splitContainer1);
            this.TpItem.Name = "TpItem";
            this.TpItem.Size = new System.Drawing.Size(766, 551);
            this.TpItem.Text = "Item List";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd2);
            this.splitContainer1.Panel1.Controls.Add(this.BtnRecvCt2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Grd1);
            this.splitContainer1.Panel2.Controls.Add(this.button1);
            this.splitContainer1.Size = new System.Drawing.Size(766, 551);
            this.splitContainer1.SplitterDistance = 202;
            this.splitContainer1.TabIndex = 0;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 23);
            this.Grd2.Name = "Grd2";
            this.Grd2.ReadOnly = true;
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 179);
            this.Grd2.TabIndex = 15;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // BtnRecvCt2
            // 
            this.BtnRecvCt2.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRecvCt2.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRecvCt2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRecvCt2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvCt2.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnRecvCt2.Location = new System.Drawing.Point(0, 0);
            this.BtnRecvCt2.Name = "BtnRecvCt2";
            this.BtnRecvCt2.Size = new System.Drawing.Size(766, 23);
            this.BtnRecvCt2.TabIndex = 14;
            this.BtnRecvCt2.Text = "Replaced Items Summary";
            this.BtnRecvCt2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnRecvCt2.UseVisualStyleBackColor = false;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 322);
            this.Grd1.TabIndex = 17;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(766, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "DO\'s Items List";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 568);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 8;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 588);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 9;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmDOCt3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 610);
            this.Name = "FrmDOCt3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TcDOCt3)).EndInit();
            this.TcDOCt3.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInspectionSheet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductDeliveryNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReturnNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtDONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSAAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvCtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.TpItem.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraTab.XtraTabControl TcDOCt3;
        private DevExpress.XtraTab.XtraTabPage TpGeneral;
        private DevExpress.XtraTab.XtraTabPage TpItem;
        internal DevExpress.XtraEditors.TextEdit TxtRecvCtDocNo;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnRecvCtDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnRecvCtDocNo1;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.SimpleButton BtnSAName;
        private System.Windows.Forms.Label LblSAName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Button BtnRecvCt2;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        internal DevExpress.XtraEditors.TextEdit TxtSAName;
        public DevExpress.XtraEditors.SimpleButton BtnCustomerShipAddress;
        internal DevExpress.XtraEditors.MemoExEdit MeeSAAddress;
        internal DevExpress.XtraEditors.TextEdit TxtCity;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        internal DevExpress.XtraEditors.TextEdit TxtCountry;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        internal DevExpress.XtraEditors.TextEdit TxtFax;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtProductDeliveryNo;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtCtReturnNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtCtDONo;
        private System.Windows.Forms.Label LblInspectionSheet;
        internal DevExpress.XtraEditors.TextEdit TxtInspectionSheet;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ProgressBar PbUpload3;
        private System.Windows.Forms.ProgressBar PbUpload2;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
    }
}