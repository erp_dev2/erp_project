﻿#region Update
/*
    19/07/2017 [HAR] Textbox Voucher request belum clear,kolom paling bawah grid tidak keliahatan,TOP format dalam decimal 
    03/10/2017 [WED] tambah credit (combobox) di header setelah join date
    15/10/2017 [TKG] perhitungan credit ratio
    30/10/2017 [HAR] tambah perhitungan bunga
    07/11/2017 [HAR] auto generate month detail
    28/11/2017 [WED] credit di filter berdasarkan group site
    28/11/2017 [WED] credit mandatory berdasarkan parameter IsCreditAdvancePaymentMandatory
    29/11/2017 [HAR] bug saat beda tahun
    22/12/2017 [TKG] tambah filter dept+site berdasarkan group
    22/01/2018 [HAR] tambah depresiasi method (saldo menurun & garis lurus)
    24/01/2018 [HAR] validasi decimal point
    09/03/2018 [TKG] untuk credit internal akan membuat VR, yg non internal tidak membuat VR.
    12/03/2018 [HAR] bug : amount tidak berubah saat merubah rate ataupun rate amount
    05/05/2018 [TKG] set voucher request document number berdasarkan standard document
    14/11/2019 [HAR/MMM] Bug saat save data
    17/01/2021 [TKG/PHT] ubah GenerateDocNo, GenerateVoucherRequestDocNo
    31/05/2021 [DITA/PHT] Voucher request dibuat tidak auto generate saat save transaksi, based on parameter : IsAdvancePaymentVRNotAutoGenerate
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
    14/02/2022 [TKG/PHT] merubah GetParameter dan proses save
    24/03/2022 [TRI/PHT] bug data credit yang tampil belum sesuai site user
    09/06/2022 [MYA/PHT] Membuat menu employee loan request tidak terpengaruh pada parameter IsFilterBySiteHR
    02/08/2022 [SET/PHT] penyesuaian Source SaveVR 
    23/08/2022 [SET/PHT] Isi default param SourceVRAdvancePayment = "1"
    21/02/2023 [WED/PHT] tambah validasi closing journal berdasarkan parameter IsClosingJournalBasedOnMultiProfitCenter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAdvancePayment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, mSiteCode = string.Empty,
            mUserCode = string.Empty, mDeptCode = string.Empty;
        iGCell fCell;
        bool fAccept;
        private string 
            mItCtRMPActual = string.Empty, 
            mSQLForActualItCode = string.Empty,
            mBankAcCodeAdvancePayment = string.Empty,
            mMainCurCode = string.Empty,
            mVoucherCodeFormatType = "1",
            mSourceVRAdvancePayment = "1",
            mDocTitle = string.Empty,
            mVRAdvancePaymentRemarkFormat = string.Empty;
        internal FrmAdvancePaymentFind FrmFind;
        internal bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false, mIsClosingJournalBasedOnMultiProfitCenter = false;
        private bool mIsCreditAdvancePaymentMandatory = false,
            mIsAutoJournalActived = false,
            mIsAdvancePaymentVRNotAutoGenerate = false,
            mIsCreditSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmAdvancePayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueCreditCode(ref LueCreditCode);
                SetLueMonth(ref LueMonth);
                SetLueMonth(ref LueMth);
                Sl.SetLueYr(LueYr, string.Empty);
                SetLueDepreciationCode(ref LueDepreciationCode);
                SetGrd();
                LueMth.Visible = false;

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7 ;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
 
                        //1-5
                        "Month", "Year", "Amount", "Interest"+Environment.NewLine+"Amount", "Total"+Environment.NewLine+"Amount",
                        //6
                        "SISA"
                    },
                    new int[] 
                    {
                        //0
                        10, 
                        //1-5
                        150, 60, 120, 120, 120,
                        //6
                        150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 4, 5 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, TxtAmt, TxtTop, LueMonth, LueDepreciationCode,
                        LueYr, MeeRemark,LueMth, LueCreditCode, TxtInterestRate, TxtInterestRateAmt, TxtTotalAmt,
                    }, true);

                    if (mIsCreditAdvancePaymentMandatory)
                        LblCreditCode.ForeColor = Color.Red;

                    BtnEmpCode.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtAmt, TxtTop, LueMonth, LueYr, 
                        MeeRemark, LueMth, LueCreditCode,LueDepreciationCode,
                        TxtAmt, TxtInterestRate, TxtInterestRateAmt
                    }, false);
                    BtnEmpCode.Enabled = true;
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mSiteCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteDocDt, TxtStatus, LueMonth, LueYr, MeeRemark, LueMth, LueCreditCode,LueDepreciationCode,
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, DteJoinDt, TxtVoucherRequestDocNo
            });
            ChkCancelInd.Checked = false;
            ChkInternalInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt, TxtTop, TxtTotal1, TxtCreditRatio, TxtRemainingLoan, TxtAmt, TxtInterestRate, TxtInterestRateAmt, TxtTotalAmt
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueMonth, LueYr
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt, TxtTop, TxtTotal1, TxtCreditRatio, TxtRemainingLoan, TxtInterestRate, TxtInterestRateAmt, TxtTotalAmt
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
        }

        private void ClearData3(int Number)
        {
            if (Number == 0)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LueMonth, LueYr
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                {
                   TxtInterestRate, TxtInterestRateAmt, TxtTotalAmt
                }, 0);
                ChkCancelInd.Checked = false;
                Sm.ClearGrd(Grd1, true);
                Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
            }
            if (Number == 1)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LueMonth, LueYr
                });
                ChkCancelInd.Checked = false;
                Sm.ClearGrd(Grd1, true);
                Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6 });
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmAdvancePaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
                if (mIsFilterBySiteHR) SetLueCreditCode(ref LueCreditCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
      
        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AdvancePayment", "TblAdvancePaymenthdr");
            string AdvancePaymentDocNoRevision = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AdvancePaymentRevision", "TblAdvancePaymentRevisionhdr");
            string VoucherRequestDocNo = string.Empty;

            if (ChkInternalInd.Checked && !mIsAdvancePaymentVRNotAutoGenerate)
            {
                if (mVoucherCodeFormatType == "2")
                    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo = GenerateVoucherRequestDocNo(); 
            }
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveAdvancePaymentHdr(DocNo, VoucherRequestDocNo));
            cml.Add(SaveAdvancePaymentDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveAdvancePaymentDtl(DocNo, Row));

            if (!IsDocNeedApproval())
                cml.Add(SaveAdvancePaymentRevision(AdvancePaymentDocNoRevision, DocNo));
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        #region Insert Data

        private bool IsInsertedDataNotValid()
        {
            var q = new StringBuilder();

            q.AppendLine("Select B.ProfitCenterCode ");
            q.AppendLine("From TblEmployee A ");
            q.AppendLine("Inner Join TblSite B On A.SiteCode = B.SiteCode ");
            q.AppendLine("Where A.EmpCode = @Param ");
            q.AppendLine("; ");

            string ProfitCenterCode = Sm.GetValue(q.ToString(), TxtEmpCode.Text);
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", true) ||
                Sm.IsLueEmpty(LueCreditCode, "Credit") ||
                Sm.IsLueEmpty(LueDepreciationCode, "Depreciation") ||
                Sm.IsTxtEmpty(TxtTop, "Term Of Payment", true) ||
                (mIsCreditAdvancePaymentMandatory && Sm.IsLueEmpty(LueCreditCode, "Credit")) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LueMonth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsAmountNotBalance() ||
                IsStartMonthNotBalance() ||
                IsStartYearNotBalance() ||
                IsStartYear() ||
                IsTopNotValid() ||
                IsAmountNotValid() ||
                IsDataCancelledAlready() ||
                (mIsClosingJournalBasedOnMultiProfitCenter && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.Left(Sm.GetDte(DteDocDt), 8), ProfitCenterCode))
                ;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Detail data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }


        private bool IsDocNeedApproval()
        {
            return 
                Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting " +
                "Where DocType='AdvancePayment' " +
                "And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@Param);",
                TxtEmpCode.Text
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to save at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Month is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Year is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "Amount is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsStartMonthNotBalance()
        {
            if ( Sm.GetLue(LueMonth) != Sm.GetGrdStr(Grd1, 0, 0)) 
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Month");
                return true;
            }
            return false;
        }

        private bool IsTopNotValid()
        {
            decimal top = decimal.Parse(TxtTop.Text);
            decimal dtl = (Grd1.Rows.Count -1);
            if (top != dtl)
            {
               Sm.StdMsg(mMsgType.Warning, "Number Of month is invalid.");
               return true;
            }
            return false;
        }

        private bool IsStartYearNotBalance()
        {
            if (Sm.GetLue(LueYr) != Sm.GetGrdStr(Grd1, 0, 2))
            {
                Sm.StdMsg(mMsgType.Warning, "Year is not balance");
                return true;
            }
            return false;
        }

        private bool IsStartYear()
        {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    int aa = int.Parse(Sm.GetLue(LueYr));
                    int bb =(Sm.GetGrdInt(Grd1, Row, 2));

                    if (aa > bb)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid Year Information");
                        return true;
                    }
                }
           
            return false;
        }

        private bool IsAmountNotBalance()
        {
            decimal amt = decimal.Parse(TxtTotalAmt.Text);
            decimal total = decimal.Parse(TxtTotal1.Text);
            if (amt != total )
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount is not balance.");
                return true;
            }
            return false;
        }

        private bool IsAmountNotValid()
        {
            decimal Amount = 0m;
            if (TxtAmt.Text.Length != 0) Amount = decimal.Parse(TxtAmt.Text);
            if (Amount < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Amount, Please check again.");
                return true;
            }
            return false;
        }

        private string GenerateDocNo(string Tbl)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblAdvancePaymentHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblAdvancePaymenthdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("   ), '0001' ");
            SQL.Append(") , '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/AP/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mBankAcCodeAdvancePayment + "' "),
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            SQL.Append("From TblVoucherRequestHdr ");
            //SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
            //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            if (Type.Length != 0) SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
            SQL.Append("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
            SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("), '0001' ");
            SQL.Append(") As Number), '/', '" + DocAbbr + "' ");
            if (Type.Length != 0) SQL.Append(", '/' , '" + Type + "' ");
            SQL.Append(") As DocNo ");

            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveAdvancePaymentHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Advance Payment - Hdr */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblAdvancePaymenthdr(DocNo, DocDt, CancelInd, EmpCode, CreditCode, DepreciationCode, Amt, InterestRate, InterestRateAmt, Top, StartMth, Yr, Remark, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @EmpCode, @CreditCode, @DepreciationCode, @Amt, @InterestRate, @InterestRateAmt, @Top, @StartMth, @Yr, @Remark, @VoucherRequestDocNo, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='AdvancePayment' ");
            SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode); ");

            SQL.AppendLine("Update TblAdvancePaymentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='AdvancePayment' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            if (VoucherRequestDocNo.Length > 0)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
                SQL.AppendLine("DocType, AcType, BankAcCode, DocEnclosure, CurCode, Amt, PIC, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @VoucherRequestDocNo, A.DocDt, 'N', 'A', ");

                if (mSourceVRAdvancePayment == "1")
                    SQL.AppendLine("B.ParValue, ");
                else if (mSourceVRAdvancePayment == "2")
                    SQL.AppendLine("@DeptCode, ");

                SQL.AppendLine("'09', 'C', @BankAcCode, 0, @CurCode, @TotalAmt, @UserCode, ");

                if (mVRAdvancePaymentRemarkFormat == "1")
                    SQL.AppendLine("Concat('Advance Payment# : ', A.DocNo), ");
                else if (mVRAdvancePaymentRemarkFormat == "2")
                    SQL.AppendLine("A.Remark, ");

                SQL.AppendLine("A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblAdvancePaymentHdr A ");
                SQL.AppendLine("Inner Join TblParameter B On B.ParCode='VoucherRequestPayrollDeptCode' ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");

                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Amt, Description, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocNo, '001', Amt, ");
                if (mDocTitle == "PHT")
                    SQL.AppendLine("Concat('Advance Payment# : ', @DocNo, ' - ', '" + mUserCode + "', ' - ', @EmpName) AS Remark, ");
                else
                    SQL.AppendLine("Remark, ");
                if (mVRAdvancePaymentRemarkFormat == "1")
                    SQL.AppendLine("Remark, ");
                else if (mVRAdvancePaymentRemarkFormat == "2")
                    SQL.AppendLine("@Remark, ");
                SQL.AppendLine("CreateBy, CreateDt ");
                SQL.AppendLine("From TblVoucherRequestHdr ");
                SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@CreditCode", Sm.GetLue(LueCreditCode));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", Sm.GetLue(LueDepreciationCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@InterestRate", Decimal.Parse(TxtInterestRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@InterestRateAmt", Decimal.Parse(TxtInterestRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Decimal.Parse(TxtTotalAmt.Text)); 
            Sm.CmParam<Decimal>(ref cm, "@Top", Decimal.Parse(TxtTop.Text));
            Sm.CmParam<String>(ref cm, "@EmpName", (TxtEmpName.Text));
            Sm.CmParam<String>(ref cm, "@StartMth", Sm.GetLue(LueMonth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mSourceVRAdvancePayment == "1")
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            else if (mSourceVRAdvancePayment == "2")
            {
                Sm.CmParam<String>(ref cm, "@UserCode", mUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            }
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", mBankAcCodeAdvancePayment);

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AP Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblAdvancePaymentDtl(DocNo, DNo, Mth, Yr, AmtMain, AmtRate, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() +
                        ", @Mth_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ", @AmtMain_" + r.ToString() +
                        ", @AmtRate_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@Mth_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<Decimal>(ref cm, "@AmtMain_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@AmtRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveAdvancePaymentDtl(string DocNo,int Row)
        //{


        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblAdvancePaymentDtl(DocNo, DNo, Mth, Yr, AmtMain, AmtRate, Amt, CreateBy, CreateDt) " +
        //            "Values (@DocNo, @DNo, @Mth, @Yr, @AmtMain, @AmtRate, @Amt, @UserCode, CurrentDateTime());"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@AmtMain", Sm.GetGrdDec(Grd1, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@AmtRate", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveAdvancePaymentRevision(string AdvancePaymentDocNoRevision, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAdvancePaymentRevisionHdr(DocNo, DocDt, AdvancePaymentDocNo, AdvancePaymentDocDt, EmpCode, Amt, Top, StartMth, Yr, Revision, Remark, CreateBy, CreateDt ) ");
            SQL.AppendLine("Select @AdvancePaymentDocNoRevision, A.DocDt, A.DocNo, A.DocDt, A.EmpCode, A.Amt, A.Top, A.StartMth, A.Yr, '0', A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblAdvancePaymentHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblAdvancePaymentRevisionDtl(DocNo, Dno, AdvancePaymentDocNo, AdvancePaymentDno, Mth, Yr, Amt, CreateBy, CreateDt ) ");
            SQL.AppendLine("Select @AdvancePaymentDocNoRevision, A.Dno, A.DocNo, A.Dno, A.Mth, A.Yr, A.Amt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblAdvancePaymentDtl A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNoRevision", AdvancePaymentDocNoRevision);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditAdvancePaymentHdr();
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            var q = new StringBuilder();

            q.AppendLine("Select B.ProfitCenterCode ");
            q.AppendLine("From TblEmployee A ");
            q.AppendLine("Inner Join TblSite B On A.SiteCode = B.SiteCode ");
            q.AppendLine("Where A.EmpCode = @Param ");
            q.AppendLine("; ");

            string ProfitCenterCode = Sm.GetValue(q.ToString(), TxtEmpCode.Text);

            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                (mIsClosingJournalBasedOnMultiProfitCenter && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.Left(Sm.GetDte(DteDocDt), 8), ProfitCenterCode))
                ;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAdvancePaymentHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private bool IsEditedDataNotValid()
        {
            return Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private void EditAdvancePaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAdvancePaymentHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N';");

            if (TxtVoucherRequestDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Update TblVoucherRequestHdr Set ");
                SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N';");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (TxtVoucherRequestDocNo.Text.Length > 0)
                Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAdvancePaymentHdr(DocNo);
                ShowAdvancePaymentDtl(DocNo);
                ComputeTotal(Grd1, TxtTotal1);
                ComputeTotalAmtHdr();
            
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAdvancePaymentHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select Tbl.DocNo, Tbl.DocDt, Tbl.CancelInd, ");
            SQL.AppendLine("Tbl.EmpCode, Tbl.EmpName, Tbl.EmpCodeOld, Tbl.PosName, Tbl.DeptName, Tbl.SiteName, Tbl.JoinDt, ");
            SQL.AppendLine("Tbl.CreditCode, Tbl.Amt, Tbl.InterestRate, Tbl.InterestRateAmt, Tbl.Top, Tbl.StartMth, Tbl.Yr, Tbl.Status, ");
            SQL.AppendLine("Tbl.VoucherRequestDocNo, Tbl.Remark, Tbl.RemainingLoan, ");
            SQL.AppendLine("Case When IfNull(Tbl.CreditRatioSalary, 0.00)=0.00 Then 0.00 ");
            SQL.AppendLine("Else IfNull(Tbl.CreditRatioBiggestLoan, 0.00)/IfNull(Tbl.CreditRatioSalary, 0.00) ");
            SQL.AppendLine("End As CreditRatio, Tbl.DepreciationCode, Tbl2.InternalInd ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, B.JoinDt, ");
            SQL.AppendLine("    A.CreditCode, A.DepreciationCode, A.Amt, A.InterestRate, A.InterestRateAmt, A.Top, A.StartMth, A.Yr, ");
            SQL.AppendLine("    Case A.Status When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' When 'A' Then 'Approved' End As Status, ");
            SQL.AppendLine("    A.VoucherRequestDocNo, A.Remark, ");
            SQL.AppendLine("    A.Amt-IfNull( ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Sum(T.Amt) As Amt ");
            SQL.AppendLine("            From TblAdvancePaymentProcess T ");
            SQL.AppendLine("            Where T.DocNo=A.DocNo ");
            SQL.AppendLine("            And Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblVoucherRequestPayrollHdr T1 ");
            SQL.AppendLine("                Inner Join TblVoucherRequestPayrollDtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("                Where T1.CancelInd='N' And T1.Status='A' And T2.PayrunCode=T.PayrunCode ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    , 0.00) As RemainingLoan, ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Max(T2.Amt) As Amt ");
            SQL.AppendLine("        From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblAdvancePaymentDtl T2 ");
	        SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("            And Not Exists( ");
		    SQL.AppendLine("                Select 1 ");
		    SQL.AppendLine("                From TblAdvancePaymentProcess T ");
		    SQL.AppendLine("                Where T.DocNo=T2.DocNo ");
		    SQL.AppendLine("                And T.Yr=T2.Yr ");
		    SQL.AppendLine("                And T.Mth=T2.Mth ");
            SQL.AppendLine("                And Exists( ");
            SQL.AppendLine("                    Select 1 ");
            SQL.AppendLine("                    From TblVoucherRequestPayrollHdr T1 ");
            SQL.AppendLine("                    Inner Join TblVoucherRequestPayrollDtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                    Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("                    Where T1.CancelInd='N' And T1.Status='A' And T2.PayrunCode=T.PayrunCode ");
            SQL.AppendLine("                ) ");
	        SQL.AppendLine("            ) ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.Status='A' ");
            SQL.AppendLine("        And T1.EmpCode=A.EmpCode ");
            SQL.AppendLine("    ) As CreditRatioBiggestLoan, ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Amt From TblEmployeeSalary ");
            SQL.AppendLine("        Where ActInd='Y' ");
            SQL.AppendLine("        And EmpCode=A.EmpCode ");
            SQL.AppendLine("        Order By DNo Desc Limit 1 ");
            SQL.AppendLine("    ) As CreditRatioSalary ");
            SQL.AppendLine("    From TblAdvancePaymentHdr A ");
            SQL.AppendLine("    Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("    Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("    Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine("Left Join TblCredit Tbl2 On Tbl.CreditCode=Tbl2.CreditCode ");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                        {
                            //0
                            "DocNo",
                            //1-5
                            "DocDt", "EmpCode", "DepreciationCode", "Top", "Amt",  
                            //6-10
                            "InterestRate", "InterestRateAmt",  "StartMth", "Yr", "Status", 
                            //11-15
                            "Remark", "EmpName", "EmpCodeOld", "PosName",  "DeptName", 
                            //16-20
                            "SiteName", "JoinDt", "VoucherRequestDocNo", "CreditCode",  "RemainingLoan", 
                            //21-23
                            "CreditRatio", "CancelInd", "InternalInd"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueDepreciationCode, Sm.DrStr(dr, c[3]));
                        TxtTop.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        TxtInterestRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        TxtInterestRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        Sm.SetLue(LueMonth, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[9]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[10]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[12]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[13]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[14]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[15]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[17]));
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetLue(LueCreditCode, Sm.DrStr(dr, c[19]));
                        TxtRemainingLoan.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                        TxtCreditRatio.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[22]), "Y");
                        ChkInternalInd.Checked = Sm.DrStr(dr, c[23])=="Y";
                    }, true
                );
        }

        private void ShowAdvancePaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DNo, MonthName(Str_To_Date(T.Mth , '%m')) As Mth, ");
            SQL.AppendLine("T.Yr, T.AmtMain, T.AmtRate, T.Amt ");
            SQL.AppendLine("From TblAdvancePaymentDtl T ");
            SQL.AppendLine("Where T.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(), 
                    new string[] 
                    { 
                        "DNo", 
                        "Mth", "Yr", "AmtMain", "AmtRate", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        internal void SetLueCreditCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CreditCode As Col1, T.CreditName As Col2 ");
            SQL.AppendLine("From TblCredit T ");
            SQL.AppendLine("Where T.ActInd = 'Y' ");

            if (mIsFilterBySiteHR && mIsCreditSiteMandatory)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode= @UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            
            SQL.AppendLine("Order By T.CreditName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
             ref Lue, ref cm,
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ComputeInterestRate()
        {
            try
            {
                ComputeTotalAmtHdr();
                decimal IntRateAmt = 0;
                decimal Amt = 0;
                if (TxtDocNo.Text.Length == 0 && Decimal.Parse(TxtAmt.Text) > 0 && TxtInterestRateAmt.Text.Length > 0
                    && Decimal.Parse(TxtInterestRateAmt.Text) > 0)
                {
                    IntRateAmt = Decimal.Parse(TxtInterestRateAmt.Text);
                    Amt = Decimal.Parse(TxtAmt.Text);
                    if (Amt > 0)
                        TxtInterestRate.EditValue = Sm.FormatNum((IntRateAmt / Amt * 100), 0);
                    else
                        TxtInterestRate.EditValue = 0;
                }
                if (TxtInterestRateAmt.Text.Length > 0 && Decimal.Parse(TxtInterestRateAmt.Text) == 0)
                {
                    TxtInterestRate.EditValue = Sm.FormatNum(0, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeInterestRateAmt()
        {
            try
            {
                decimal IntRate = 0;
                decimal TermOP = 0;
                decimal Bunga = 0;
                decimal Pokok = 0;
                decimal Pinjaman = 0;

                if (Sm.GetLue(LueDepreciationCode).Length > 0 && Sm.GetLue(LueDepreciationCode) == "1")
                {
                    if (TxtInterestRate.Text.Length != 0)
                    {
                        if (Decimal.Parse(TxtAmt.Text) > 0 && Decimal.Parse(TxtInterestRate.Text) > 0)
                        {
                            IntRate = Decimal.Parse(TxtInterestRate.Text);
                            if (IntRate != 0)
                                TxtInterestRateAmt.EditValue = Sm.FormatNum(Decimal.Parse(TxtAmt.Text) * (IntRate / 100), 0);
                            else
                                TxtInterestRateAmt.EditValue = 0;
                            ComputeTotalAmtHdr();
                        }
                    }
                    if (TxtInterestRate.Text.Length > 0 && Decimal.Parse(TxtInterestRate.Text) == 0)
                    {
                        TxtInterestRateAmt.EditValue = Sm.FormatNum(0, 0);
                    }

                }
                else if (Sm.GetLue(LueDepreciationCode).Length > 0 && Sm.GetLue(LueDepreciationCode) == "2")
                {
                    if (TxtInterestRate.Text.Length != 0)
                    {
                        if (Decimal.Parse(TxtAmt.Text) > 0 && Decimal.Parse(TxtInterestRate.Text) > 0)
                        {
                            IntRate = Decimal.Parse(TxtInterestRate.Text);
                            TermOP = Decimal.Parse(TxtTop.Text);
                            Pinjaman = Decimal.Parse(TxtAmt.Text);
                            decimal sisa = 0;
                            Pokok = Sm.Round((Pinjaman / TermOP), 2);
                            for (int i = 0; i < TermOP; i++)
                            {
                                if (i == 0)
                                {
                                    Bunga = Sm.Round((Pinjaman * IntRate / 100), 2);
                                    sisa = Pinjaman;
                                }
                                else
                                {
                                    sisa = sisa - Pokok;
                                    Bunga = Sm.Round((Bunga + (sisa * IntRate / 100)), 2);
                                }
                            }
                            TxtInterestRateAmt.EditValue = Sm.FormatNum(Sm.Round(Bunga, 0), 0);
                            ComputeTotalAmtHdr();
                        }
                    }
                    if (TxtInterestRate.Text.Length > 0 && Decimal.Parse(TxtInterestRate.Text) == 0)
                    {
                        TxtInterestRateAmt.EditValue = Sm.FormatNum(0, 0);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeInterestRest2Dtl(int Rowx)
        {
            try
            {
                decimal rate = decimal.Parse(TxtInterestRate.Text);
                if (Sm.GetGrdDec(Grd1, Rowx, 3) > 0 && TxtInterestRate.Text.Length > 0 && Sm.GetLue(LueDepreciationCode) == "1")
                {
                    rate = decimal.Parse(TxtInterestRate.Text);
                    Grd1.Cells[Rowx, 4].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Rowx, 3) * (rate / 100)), 0);
                    Grd1.Cells[Rowx, 5].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Rowx, 3) + Sm.GetGrdDec(Grd1, Rowx, 4)), 0);
                }

                else if (Sm.GetGrdDec(Grd1, Rowx, 3) > 0 && TxtInterestRate.Text.Length > 0 && Sm.GetLue(LueDepreciationCode) == "2")
                {
                    decimal IntRate = Decimal.Parse(TxtInterestRate.Text);
                    decimal Pinjaman = Decimal.Parse(TxtAmt.Text);
                    decimal Pokok = Sm.Round(Sm.GetGrdDec(Grd1, Rowx, 3), 2);
                    decimal TermOP = Decimal.Parse(TxtTop.Text);
                    if (Rowx == 0)
                    {
                        Grd1.Cells[Rowx, 4].Value = Sm.FormatNum(Sm.Round(((Pinjaman * IntRate) /100),2), 0);
                        Grd1.Cells[Rowx, 6].Value = Sm.FormatNum(Pinjaman, 0); 
                    }
                    else
                    {
                        Grd1.Cells[Rowx, 6].Value = Sm.FormatNum(Sm.GetGrdDec(Grd1, Rowx - 1, 6) - Pokok, 0); 
                        Grd1.Cells[Rowx, 4].Value = Sm.FormatNum(Sm.Round((((Pinjaman - (Rowx * Pokok)) * IntRate) / 100),2), 0);
                    }
                    Grd1.Cells[Rowx, 5].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Rowx, 3) + Sm.GetGrdDec(Grd1, Rowx, 4)), 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeTotalAmtHdr()
        {
            try
            {
                decimal amt = 0; 
                decimal rateAmt=0; 

                if (TxtAmt.Text.Length > 0)
                        amt = decimal.Parse(TxtAmt.Text);
                if (TxtInterestRateAmt.Text.Length > 0)
                    rateAmt = decimal.Parse(TxtInterestRateAmt.Text);
                    TxtTotalAmt.EditValue = Sm.FormatNum((amt + rateAmt), 0);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterBySiteHR', 'IsFilterByDeptHR', 'BankAcCodeAdvancePayment', 'MainCurCode', 'IsCreditAdvancePaymentMandatory', ");
            SQL.AppendLine("'VoucherCodeFormatType', 'IsAdvancePaymentVRNotAutoGenerate', 'IsCreditSiteMandatory', 'SourceVRAdvancePayment', 'DocTitle', ");
            SQL.AppendLine("'VRAdvancePaymentRemarkFormat', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsAutoJournalActived' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAdvancePaymentVRNotAutoGenerate": mIsAdvancePaymentVRNotAutoGenerate = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsCreditSiteMandatory" : mIsCreditSiteMandatory = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;

                            //string
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "BankAcCodeAdvancePayment": mBankAcCodeAdvancePayment = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "SourceVRAdvancePayment": mSourceVRAdvancePayment = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "VRAdvancePaymentRemarkFormat": mVRAdvancePaymentRemarkFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }
       
        public void SetLueEmpCode(ref LookUpEdit Lue)
        {
            Sm.SetLue3(
                ref Lue,
                "Select A.EmpCode As Col1, A.EmpName As Col2, B.Deptname As Col3 " +
                "From TblEmployee A " +
                "Inner Join TblDepartment B On A.DeptCode = B.DeptCode "+
                "Where (A.ResignDt Is Not Null And A.ResignDt>='" + Sm.GetDte(DteDocDt) + "' ) " +
                "Or A.ResignDt Is Null " +
                "Order By A.EmpName",
                0, 35, 40, false, true, true, "Code", "Name", "Department", "Col2", "Col1");
        }

        public static void SetLueMonth(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "select '01' As Col1, 'January' As Col2 union All " +
                "select '02','February' Union All " +
                "select '03','March' Union All " +
                "select '04','April' Union All " +
                "select '05','May' Union All " +
                "select '06','June' Union All "+
                "select '07','July' Union All "+
                "select '08','August' Union All "+
                "select '09','September' Union All "+
                "select '10','October' Union All "+
                "select '11','November' Union All "+
                "select '12','December'",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
     
        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ComputeTotal(iGrid Grd, DXE.TextEdit Txt)
        {
            try
            {
                decimal Total1 = 0m;
                for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, 5).Length != 0) 
                        Total1 += Sm.GetGrdDec(Grd, Row, 5);

                }
                Txt.Text = Sm.FormatNum(Sm.Round(Total1, 0), 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeDetail()
        {
            try
            {
                decimal TOP = 0, Yr = 0, Mth = 0, Amt = 0;
                string Mt = string.Empty;

                if (Sm.GetLue(LueDepreciationCode) == "1")
                {
                    Sm.ClearGrd(Grd1, true);
                    TOP = Decimal.Parse(TxtTop.Text);

                    if (TOP > 0 && Sm.GetLue(LueYr).Length > 0 && Sm.GetLue(LueMonth).Length > 0)
                    {
                        Yr = Decimal.Parse(Sm.GetLue(LueYr));
                        Mth = Decimal.Parse(Sm.GetLue(LueMonth));
                        for (int RowX = 0; RowX < TOP; RowX++)
                        {
                            if (Mth.ToString().Length == 1)
                            {
                                Mt = string.Concat("0", Mth.ToString());
                            }
                            else
                            {
                                Mt = Mth.ToString();
                            }
                            Grd1.Rows.Add();
                            Grd1.Cells[RowX, 0].Value = Mt;
                            Grd1.Cells[RowX, 1].Value = setMth(Mt);
                            Grd1.Cells[RowX, 2].Value = Yr;
                            if (Mth >= 12)
                            {
                                Mth = 01;
                                Yr = Yr + 1;
                            }
                            else
                            {
                                Mth = Mth + 1;
                            }
                        }
                    }
                }
                else
                {
                    Sm.ClearGrd(Grd1, true);
                    TOP = Decimal.Parse(TxtTop.Text);

                    if (TOP > 0 && Sm.GetLue(LueYr).Length > 0 && Sm.GetLue(LueMonth).Length > 0)
                    {
                        Yr = Decimal.Parse(Sm.GetLue(LueYr));
                        Mth = Decimal.Parse(Sm.GetLue(LueMonth));
                        for (int RowX = 0; RowX < TOP; RowX++)
                        {
                            if (Mth.ToString().Length == 1)
                            {
                                Mt = string.Concat("0", Mth.ToString());
                            }
                            else
                            {
                                Mt = Mth.ToString();
                            }
                            Grd1.Rows.Add();
                            Grd1.Cells[RowX, 0].Value = Mt;
                            Grd1.Cells[RowX, 1].Value = setMth(Mt);
                            Grd1.Cells[RowX, 2].Value = Yr;
                            if (Mth >= 12)
                            {
                                Mth = 01;
                                Yr = Yr + 1;
                            }
                            else
                            {
                                Mth = Mth + 1;
                            }
                        }
                    }
                }


                if (TxtAmt.Text.Length > 0 && decimal.Parse(TxtAmt.Text) > 0 && Grd1.Rows.Count > 1)
                    Amt = decimal.Parse(TxtAmt.Text) / (Grd1.Rows.Count - 1);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Grd1.Cells[Row, 3].Value = Amt;
                    ComputeInterestRest2Dtl(Row);
                    ComputeTotal(Grd1, TxtTotal1);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        private string  setMth(string Mth)
        {
            string result = null;

            switch (Mth)
            {
                case "01":
                    result = "January";
                    break;
                case "02":
                    result = "February";
                    break;
                case "03":
                    result = "March";
                    break;
                case "04":
                    result = "April";
                    break;
                case "05":
                    result = "May";
                    break;
                case "06":
                    result = "June";
                    break;
                case "07":
                    result = "July";
                    break;
                case "08":
                    result = "August";
                    break;
                case "09":
                    result = "September";
                    break;
                case "10":
                    result = "October";
                    break;
                case "11":
                    result = "November";
                    break;
                case "12":
                    result = "December";
                    break;
            }
            return result;
        }

        public static void SetLueDepreciationCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'DepreciationMethod' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3, 4, 5 }, e);
            if (e.ColIndex == 3)
            {
                Sm.FocusGrd(Grd1, e.RowIndex + 1, 3);
                ComputeInterestRest2Dtl(e.RowIndex);
                ComputeTotal(Grd1, TxtTotal1);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeTotal(Grd1, TxtTotal1);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueMth, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLueMonth(ref LueMth);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAdvancePaymentDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void LueMonth_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueMonth, new Sm.RefreshLue1(SetLueMonth));
                ComputeDetail();
                if (Sm.GetLue(LueDepreciationCode).Length > 0 && Sm.GetLue(LueDepreciationCode) == "2")
                {
                    Grd1.ReadOnly = true;
                }
                else
                {
                    Grd1.ReadOnly = false;
                }
            }
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMth, new Sm.RefreshLue1(SetLueMonth));
        }

        private void LueMth_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueMth_Leave(object sender, EventArgs e)
        {
            if (LueMth.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueMth).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueMth);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueMth.GetColumnValue("Col2");
                }
                LueMth.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd1, (fCell.RowIndex + 1), new int[] { 3 });
            }
        }

        private void TxtAmt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData3(0);
            }
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
            if(TxtAmt.Text.Length > 0)
            ComputeTotalAmtHdr();
        }

        private void TxtTop_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTop, 0);
        }

        private void LueCreditCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ChkInternalInd.Checked = false;
                if (mIsFilterBySiteHR)
                    Sm.RefreshLookUpEdit(LueCreditCode, new Sm.RefreshLue1(SetLueCreditCode));
                else
                    Sm.RefreshLookUpEdit(LueCreditCode, new Sm.RefreshLue1(Sl.SetLueCreditCode));
                var CreditCode = Sm.GetLue(LueCreditCode);
                if (CreditCode.Length > 0)
                { 
                    try
                    {
                        ChkInternalInd.Checked = Sm.IsDataExist("Select 1 From TblCredit Where InternalInd='Y' And CreditCode=@Param;", CreditCode);
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
            }
        }

        private void TxtInterestRate_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData3(1);
                ComputeInterestRateAmt();
            }
        }

        private void TxtInterestRateAmt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && Sm.GetLue(LueDepreciationCode) == "1")
            {
                ClearData3(1);
                ComputeInterestRate();
            }
        }

        private void TxtInterestRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtInterestRate, 0);
        }

        private void TxtTotalAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalAmt, 0);
        }

        private void TxtInterestRateAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtInterestRateAmt, 0);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeDetail();
                if (Sm.GetLue(LueDepreciationCode).Length > 0 && Sm.GetLue(LueDepreciationCode) == "2")
                    Grd1.ReadOnly = true;
                else
                    Grd1.ReadOnly = false;
            }
        }

        private void TxtTop_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearData3(0);
        }

        private void LueDepreciationCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData2();
            }
            Sm.RefreshLookUpEdit(LueDepreciationCode, new Sm.RefreshLue1(SetLueDepreciationCode));
            if (Sm.GetLue(LueDepreciationCode).Length > 0 && Sm.GetLue(LueDepreciationCode) == "2")
            {
                TxtInterestRateAmt.BackColor = Color.Silver;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtInterestRateAmt }, true);
            }
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtInterestRateAmt }, false);
        }

        #endregion

        #endregion       

    }
}
