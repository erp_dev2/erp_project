﻿namespace RunSystem
{
    partial class FrmBudgetCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBudgetCategory));
            this.TxtBCName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtBCCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueBudgetType = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtLocalCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCCCode = new DevExpress.XtraEditors.TextEdit();
            this.LblCCCode = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.BtnCCCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblCOA = new System.Windows.Forms.Label();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCCtCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCCtName = new DevExpress.XtraEditors.TextEdit();
            this.TxtCostCategoryCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtCostCenterCode = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBCName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBudgetType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCategoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCenterCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 238);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtCostCenterCode);
            this.panel2.Controls.Add(this.TxtCostCategoryCode);
            this.panel2.Controls.Add(this.BtnCCtCode);
            this.panel2.Controls.Add(this.TxtCCtName);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.BtnCCCode);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.LblCOA);
            this.panel2.Controls.Add(this.TxtCCCode);
            this.panel2.Controls.Add(this.LblCCCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtLocalCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueBudgetType);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.TxtBCName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtBCCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 238);
            // 
            // TxtBCName
            // 
            this.TxtBCName.EnterMoveNextControl = true;
            this.TxtBCName.Location = new System.Drawing.Point(146, 50);
            this.TxtBCName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBCName.Name = "TxtBCName";
            this.TxtBCName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBCName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBCName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBCName.Properties.Appearance.Options.UseFont = true;
            this.TxtBCName.Properties.MaxLength = 200;
            this.TxtBCName.Size = new System.Drawing.Size(463, 20);
            this.TxtBCName.TabIndex = 14;
            this.TxtBCName.Validated += new System.EventHandler(this.TxtBCName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(47, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Category Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBCCode
            // 
            this.TxtBCCode.EnterMoveNextControl = true;
            this.TxtBCCode.Location = new System.Drawing.Point(146, 8);
            this.TxtBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBCCode.Name = "TxtBCCode";
            this.TxtBCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBCCode.Properties.MaxLength = 16;
            this.TxtBCCode.Properties.ReadOnly = true;
            this.TxtBCCode.Size = new System.Drawing.Size(223, 20);
            this.TxtBCCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(50, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Category Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(65, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(146, 71);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 100;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(463, 20);
            this.LueDeptCode.TabIndex = 16;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(146, 198);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(103, 96);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBudgetType
            // 
            this.LueBudgetType.EnterMoveNextControl = true;
            this.LueBudgetType.Location = new System.Drawing.Point(146, 92);
            this.LueBudgetType.Margin = new System.Windows.Forms.Padding(5);
            this.LueBudgetType.Name = "LueBudgetType";
            this.LueBudgetType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBudgetType.Properties.Appearance.Options.UseFont = true;
            this.LueBudgetType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBudgetType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBudgetType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBudgetType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBudgetType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBudgetType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBudgetType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBudgetType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBudgetType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBudgetType.Properties.DropDownRows = 30;
            this.LueBudgetType.Properties.MaxLength = 100;
            this.LueBudgetType.Properties.NullText = "[Empty]";
            this.LueBudgetType.Properties.PopupWidth = 500;
            this.LueBudgetType.Size = new System.Drawing.Size(463, 20);
            this.LueBudgetType.TabIndex = 18;
            this.LueBudgetType.ToolTip = "F4 : Show/hide list";
            this.LueBudgetType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBudgetType.EditValueChanged += new System.EventHandler(this.LueBudgetType_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(72, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Local Code";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalCode
            // 
            this.TxtLocalCode.EnterMoveNextControl = true;
            this.TxtLocalCode.Location = new System.Drawing.Point(146, 29);
            this.TxtLocalCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalCode.Name = "TxtLocalCode";
            this.TxtLocalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalCode.Properties.MaxLength = 200;
            this.TxtLocalCode.Size = new System.Drawing.Size(463, 20);
            this.TxtLocalCode.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(54, 117);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Cost Category";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCCCode
            // 
            this.TxtCCCode.EnterMoveNextControl = true;
            this.TxtCCCode.Location = new System.Drawing.Point(146, 134);
            this.TxtCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCCCode.Name = "TxtCCCode";
            this.TxtCCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCode.Properties.MaxLength = 200;
            this.TxtCCCode.Size = new System.Drawing.Size(463, 20);
            this.TxtCCCode.TabIndex = 22;
            // 
            // LblCCCode
            // 
            this.LblCCCode.AutoSize = true;
            this.LblCCCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCCCode.ForeColor = System.Drawing.Color.Red;
            this.LblCCCode.Location = new System.Drawing.Point(66, 138);
            this.LblCCCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCCCode.Name = "LblCCCode";
            this.LblCCCode.Size = new System.Drawing.Size(72, 14);
            this.LblCCCode.TabIndex = 21;
            this.LblCCCode.Text = "Cost Center";
            this.LblCCCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(146, 176);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 200;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(615, 20);
            this.TxtAcDesc.TabIndex = 26;
            // 
            // BtnCCCode
            // 
            this.BtnCCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCCCode.Appearance.Options.UseBackColor = true;
            this.BtnCCCode.Appearance.Options.UseFont = true;
            this.BtnCCCode.Appearance.Options.UseForeColor = true;
            this.BtnCCCode.Appearance.Options.UseTextOptions = true;
            this.BtnCCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCCCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCCCode.Image")));
            this.BtnCCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCCCode.Location = new System.Drawing.Point(610, 134);
            this.BtnCCCode.Name = "BtnCCCode";
            this.BtnCCCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCCCode.TabIndex = 23;
            this.BtnCCCode.ToolTip = "Find COA\'s Account";
            this.BtnCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCCCode.ToolTipTitle = "Run System";
            this.BtnCCCode.Click += new System.EventHandler(this.BtnCCCode_Click);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(146, 155);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 200;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(463, 20);
            this.TxtAcNo.TabIndex = 25;
            // 
            // LblCOA
            // 
            this.LblCOA.AutoSize = true;
            this.LblCOA.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOA.ForeColor = System.Drawing.Color.Red;
            this.LblCOA.Location = new System.Drawing.Point(49, 161);
            this.LblCOA.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOA.Name = "LblCOA";
            this.LblCOA.Size = new System.Drawing.Size(89, 14);
            this.LblCOA.TabIndex = 24;
            this.LblCOA.Text = "COA\'s Account";
            this.LblCOA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(610, 154);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 27;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // BtnCCtCode
            // 
            this.BtnCCtCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCCtCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCCtCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCCtCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCCtCode.Appearance.Options.UseBackColor = true;
            this.BtnCCtCode.Appearance.Options.UseFont = true;
            this.BtnCCtCode.Appearance.Options.UseForeColor = true;
            this.BtnCCtCode.Appearance.Options.UseTextOptions = true;
            this.BtnCCtCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCCtCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCCtCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCCtCode.Image")));
            this.BtnCCtCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCCtCode.Location = new System.Drawing.Point(610, 112);
            this.BtnCCtCode.Name = "BtnCCtCode";
            this.BtnCCtCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCCtCode.TabIndex = 30;
            this.BtnCCtCode.ToolTip = "Find COA\'s Account";
            this.BtnCCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCCtCode.ToolTipTitle = "Run System";
            this.BtnCCtCode.Click += new System.EventHandler(this.BtnCCtCode_Click);
            // 
            // TxtCCtName
            // 
            this.TxtCCtName.EnterMoveNextControl = true;
            this.TxtCCtName.Location = new System.Drawing.Point(146, 113);
            this.TxtCCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCCtName.Name = "TxtCCtName";
            this.TxtCCtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCCtName.Properties.MaxLength = 200;
            this.TxtCCtName.Size = new System.Drawing.Size(463, 20);
            this.TxtCCtName.TabIndex = 29;
            // 
            // TxtCostCategoryCode
            // 
            this.TxtCostCategoryCode.EnterMoveNextControl = true;
            this.TxtCostCategoryCode.Location = new System.Drawing.Point(631, 114);
            this.TxtCostCategoryCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostCategoryCode.Name = "TxtCostCategoryCode";
            this.TxtCostCategoryCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCostCategoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostCategoryCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostCategoryCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCostCategoryCode.Properties.MaxLength = 200;
            this.TxtCostCategoryCode.Size = new System.Drawing.Size(75, 20);
            this.TxtCostCategoryCode.TabIndex = 31;
            // 
            // TxtCostCenterCode
            // 
            this.TxtCostCenterCode.EnterMoveNextControl = true;
            this.TxtCostCenterCode.Location = new System.Drawing.Point(631, 135);
            this.TxtCostCenterCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostCenterCode.Name = "TxtCostCenterCode";
            this.TxtCostCenterCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCostCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostCenterCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostCenterCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCostCenterCode.Properties.MaxLength = 200;
            this.TxtCostCenterCode.Size = new System.Drawing.Size(75, 20);
            this.TxtCostCenterCode.TabIndex = 32;
            // 
            // FrmBudgetCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 238);
            this.Name = "FrmBudgetCategory";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBCName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBudgetType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCategoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCenterCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtBCName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtBCCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueBudgetType;
        private DevExpress.XtraEditors.TextEdit TxtLocalCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LblCCCode;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        public DevExpress.XtraEditors.SimpleButton BtnCCCode;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label LblCOA;
        internal DevExpress.XtraEditors.TextEdit TxtCCCode;
        public DevExpress.XtraEditors.SimpleButton BtnCCtCode;
        internal DevExpress.XtraEditors.TextEdit TxtCCtName;
        internal DevExpress.XtraEditors.TextEdit TxtCostCenterCode;
        internal DevExpress.XtraEditors.TextEdit TxtCostCategoryCode;
    }
}