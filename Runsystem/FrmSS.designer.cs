﻿namespace RunSystem
{
    partial class FrmSS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSS));
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtSSName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtSSCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTotalPerc = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtEmployeePerc = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtEmployerPerc = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueSSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtAcDesc1 = new DevExpress.XtraEditors.TextEdit();
            this.LblAcDesc1 = new System.Windows.Forms.Label();
            this.TxtAcNo1 = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo1 = new System.Windows.Forms.Label();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.LblAcDesc2 = new System.Windows.Forms.Label();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo2 = new System.Windows.Forms.Label();
            this.TxtSalaryMaxLimit = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSalaryMinLimit = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCostCenterGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployeePerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployerPerc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalaryMaxLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalaryMinLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenterGroup.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 311);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnAcNo2);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtSalaryMinLimit);
            this.panel2.Controls.Add(this.LueCostCenterGroup);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtSalaryMaxLimit);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtAcDesc2);
            this.panel2.Controls.Add(this.LblAcDesc2);
            this.panel2.Controls.Add(this.TxtAcNo2);
            this.panel2.Controls.Add(this.LblAcNo2);
            this.panel2.Controls.Add(this.TxtAcDesc1);
            this.panel2.Controls.Add(this.LblAcDesc1);
            this.panel2.Controls.Add(this.TxtAcNo1);
            this.panel2.Controls.Add(this.LblAcNo1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueSSPCode);
            this.panel2.Controls.Add(this.TxtTotalPerc);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.TxtEmployeePerc);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtEmployerPerc);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtSSName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtSSCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 311);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(381, 13);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(66, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // TxtSSName
            // 
            this.TxtSSName.EnterMoveNextControl = true;
            this.TxtSSName.Location = new System.Drawing.Point(253, 35);
            this.TxtSSName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSSName.Name = "TxtSSName";
            this.TxtSSName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSSName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSSName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSSName.Properties.Appearance.Options.UseFont = true;
            this.TxtSSName.Properties.MaxLength = 80;
            this.TxtSSName.Size = new System.Drawing.Size(455, 20);
            this.TxtSSName.TabIndex = 13;
            this.TxtSSName.Validated += new System.EventHandler(this.TxtSSName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(124, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Social Security  Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSSCode
            // 
            this.TxtSSCode.EnterMoveNextControl = true;
            this.TxtSSCode.Location = new System.Drawing.Point(253, 14);
            this.TxtSSCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSSCode.Name = "TxtSSCode";
            this.TxtSSCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSSCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSSCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSSCode.Properties.MaxLength = 16;
            this.TxtSSCode.Size = new System.Drawing.Size(125, 20);
            this.TxtSSCode.TabIndex = 10;
            this.TxtSSCode.Validated += new System.EventHandler(this.TxtSSCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(131, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Social Security Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalPerc
            // 
            this.TxtTotalPerc.EnterMoveNextControl = true;
            this.TxtTotalPerc.Location = new System.Drawing.Point(253, 161);
            this.TxtTotalPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalPerc.Name = "TxtTotalPerc";
            this.TxtTotalPerc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalPerc.Size = new System.Drawing.Size(125, 20);
            this.TxtTotalPerc.TabIndex = 25;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(197, 164);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 14);
            this.label19.TabIndex = 24;
            this.label19.Text = "Total %";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployeePerc
            // 
            this.TxtEmployeePerc.EnterMoveNextControl = true;
            this.TxtEmployeePerc.Location = new System.Drawing.Point(253, 140);
            this.TxtEmployeePerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployeePerc.Name = "TxtEmployeePerc";
            this.TxtEmployeePerc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmployeePerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployeePerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployeePerc.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployeePerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmployeePerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmployeePerc.Size = new System.Drawing.Size(125, 20);
            this.TxtEmployeePerc.TabIndex = 23;
            this.TxtEmployeePerc.Validated += new System.EventHandler(this.TxtEmployeePerc_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(172, 143);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 14);
            this.label17.TabIndex = 22;
            this.label17.Text = "Employee %";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployerPerc
            // 
            this.TxtEmployerPerc.EnterMoveNextControl = true;
            this.TxtEmployerPerc.Location = new System.Drawing.Point(253, 119);
            this.TxtEmployerPerc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployerPerc.Name = "TxtEmployerPerc";
            this.TxtEmployerPerc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmployerPerc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployerPerc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployerPerc.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployerPerc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmployerPerc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmployerPerc.Size = new System.Drawing.Size(125, 20);
            this.TxtEmployerPerc.TabIndex = 21;
            this.TxtEmployerPerc.Validated += new System.EventHandler(this.TxtEmployerPerc_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(175, 122);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 14);
            this.label16.TabIndex = 20;
            this.label16.Text = "Employer %";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(114, 59);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "Social Security Program";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSSPCode
            // 
            this.LueSSPCode.EnterMoveNextControl = true;
            this.LueSSPCode.Location = new System.Drawing.Point(253, 56);
            this.LueSSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSSPCode.Name = "LueSSPCode";
            this.LueSSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSSPCode.Properties.DropDownRows = 30;
            this.LueSSPCode.Properties.NullText = "[Empty]";
            this.LueSSPCode.Properties.PopupWidth = 500;
            this.LueSSPCode.Size = new System.Drawing.Size(455, 20);
            this.LueSSPCode.TabIndex = 15;
            this.LueSSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSSPCode.EditValueChanged += new System.EventHandler(this.LueSSPCode_EditValueChanged);
            // 
            // TxtAcDesc1
            // 
            this.TxtAcDesc1.EnterMoveNextControl = true;
            this.TxtAcDesc1.Location = new System.Drawing.Point(253, 203);
            this.TxtAcDesc1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc1.Name = "TxtAcDesc1";
            this.TxtAcDesc1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc1.Properties.MaxLength = 80;
            this.TxtAcDesc1.Properties.ReadOnly = true;
            this.TxtAcDesc1.Size = new System.Drawing.Size(455, 20);
            this.TxtAcDesc1.TabIndex = 30;
            this.TxtAcDesc1.Validated += new System.EventHandler(this.TxtAcDesc1_Validated);
            // 
            // LblAcDesc1
            // 
            this.LblAcDesc1.AutoSize = true;
            this.LblAcDesc1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcDesc1.ForeColor = System.Drawing.Color.Black;
            this.LblAcDesc1.Location = new System.Drawing.Point(62, 206);
            this.LblAcDesc1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcDesc1.Name = "LblAcDesc1";
            this.LblAcDesc1.Size = new System.Drawing.Size(186, 14);
            this.LblAcDesc1.TabIndex = 29;
            this.LblAcDesc1.Text = "COA Account Description (Debt)";
            this.LblAcDesc1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo1
            // 
            this.TxtAcNo1.EnterMoveNextControl = true;
            this.TxtAcNo1.Location = new System.Drawing.Point(253, 182);
            this.TxtAcNo1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo1.Name = "TxtAcNo1";
            this.TxtAcNo1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo1.Properties.MaxLength = 20;
            this.TxtAcNo1.Properties.ReadOnly = true;
            this.TxtAcNo1.Size = new System.Drawing.Size(233, 20);
            this.TxtAcNo1.TabIndex = 27;
            this.TxtAcNo1.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // LblAcNo1
            // 
            this.LblAcNo1.AutoSize = true;
            this.LblAcNo1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo1.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo1.Location = new System.Drawing.Point(117, 185);
            this.LblAcNo1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo1.Name = "LblAcNo1";
            this.LblAcNo1.Size = new System.Drawing.Size(131, 14);
            this.LblAcNo1.TabIndex = 26;
            this.LblAcNo1.Text = "COA Account# (Debt)";
            this.LblAcNo1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(253, 245);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 80;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(455, 20);
            this.TxtAcDesc2.TabIndex = 35;
            this.TxtAcDesc2.Validated += new System.EventHandler(this.TxtAcDesc2_Validated);
            // 
            // LblAcDesc2
            // 
            this.LblAcDesc2.AutoSize = true;
            this.LblAcDesc2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcDesc2.ForeColor = System.Drawing.Color.Black;
            this.LblAcDesc2.Location = new System.Drawing.Point(9, 248);
            this.LblAcDesc2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcDesc2.Name = "LblAcDesc2";
            this.LblAcDesc2.Size = new System.Drawing.Size(239, 14);
            this.LblAcDesc2.TabIndex = 34;
            this.LblAcDesc2.Text = "COA Account Description (Downpayment)";
            this.LblAcDesc2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(253, 224);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 20;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(233, 20);
            this.TxtAcNo2.TabIndex = 32;
            this.TxtAcNo2.Validated += new System.EventHandler(this.TxtAcNo2_Validated);
            // 
            // LblAcNo2
            // 
            this.LblAcNo2.AutoSize = true;
            this.LblAcNo2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo2.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo2.Location = new System.Drawing.Point(64, 227);
            this.LblAcNo2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo2.Name = "LblAcNo2";
            this.LblAcNo2.Size = new System.Drawing.Size(184, 14);
            this.LblAcNo2.TabIndex = 31;
            this.LblAcNo2.Text = "COA Account# (Downpayment)";
            this.LblAcNo2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSalaryMaxLimit
            // 
            this.TxtSalaryMaxLimit.EnterMoveNextControl = true;
            this.TxtSalaryMaxLimit.Location = new System.Drawing.Point(253, 98);
            this.TxtSalaryMaxLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalaryMaxLimit.Name = "TxtSalaryMaxLimit";
            this.TxtSalaryMaxLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalaryMaxLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalaryMaxLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalaryMaxLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtSalaryMaxLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalaryMaxLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalaryMaxLimit.Size = new System.Drawing.Size(125, 20);
            this.TxtSalaryMaxLimit.TabIndex = 19;
            this.TxtSalaryMaxLimit.Validated += new System.EventHandler(this.TxtSalaryMaxLimit_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(117, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "Salary (Maximum Limit)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSalaryMinLimit
            // 
            this.TxtSalaryMinLimit.EnterMoveNextControl = true;
            this.TxtSalaryMinLimit.Location = new System.Drawing.Point(253, 77);
            this.TxtSalaryMinLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalaryMinLimit.Name = "TxtSalaryMinLimit";
            this.TxtSalaryMinLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalaryMinLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalaryMinLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalaryMinLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtSalaryMinLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalaryMinLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalaryMinLimit.Size = new System.Drawing.Size(125, 20);
            this.TxtSalaryMinLimit.TabIndex = 17;
            this.TxtSalaryMinLimit.Validated += new System.EventHandler(this.TxtSalaryMinLimit_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(120, 80);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 14);
            this.label5.TabIndex = 16;
            this.label5.Text = "Salary (Minimum Limit)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(140, 269);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 14);
            this.label6.TabIndex = 36;
            this.label6.Text = "Cost Center Group";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostCenterGroup
            // 
            this.LueCostCenterGroup.EnterMoveNextControl = true;
            this.LueCostCenterGroup.Location = new System.Drawing.Point(253, 266);
            this.LueCostCenterGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostCenterGroup.Name = "LueCostCenterGroup";
            this.LueCostCenterGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.Appearance.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostCenterGroup.Properties.DropDownRows = 30;
            this.LueCostCenterGroup.Properties.NullText = "[Empty]";
            this.LueCostCenterGroup.Properties.PopupWidth = 500;
            this.LueCostCenterGroup.Size = new System.Drawing.Size(455, 20);
            this.LueCostCenterGroup.TabIndex = 37;
            this.LueCostCenterGroup.ToolTip = "F4 : Show/hide list";
            this.LueCostCenterGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostCenterGroup.EditValueChanged += new System.EventHandler(this.LueCostCenterGroup_EditValueChanged);
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(495, 181);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(14, 21);
            this.BtnAcNo.TabIndex = 28;
            this.BtnAcNo.ToolTip = "Find COA";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(495, 223);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(14, 21);
            this.BtnAcNo2.TabIndex = 33;
            this.BtnAcNo2.ToolTip = "Find COA";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // FrmSS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 311);
            this.Name = "FrmSS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployeePerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployerPerc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalaryMaxLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalaryMinLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenterGroup.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.TextEdit TxtSSName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtSSCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtTotalPerc;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtEmployeePerc;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtEmployerPerc;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueSSPCode;
        private System.Windows.Forms.Label LblAcDesc1;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo1;
        private System.Windows.Forms.Label LblAcNo1;
        private System.Windows.Forms.Label LblAcDesc2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private System.Windows.Forms.Label LblAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtSalaryMaxLimit;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtSalaryMinLimit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueCostCenterGroup;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc1;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
    }
}