﻿#region Update
/*
    21/07/2019 [TKG] New application
    12/08/2019 [TKG] tambah location
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMTransferBinDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmIMMTransferBin mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty, mLocCode = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMTransferBinDlg(FrmIMMTransferBin FrmParent, string WhsCode, string LocCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mLocCode = LocCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueIMMSellerCode(ref LueSellerCode);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueBin(ref LueBin);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "Requested#",
                        "DocNo",
                        "SeqNo",
                        "Source",
                        
                        //6-10
                        "Product's Code",
                        "Product's Description",
                        "Color",
                        "Quantity",
                        "Warehouse Code",
                        
                        //11-15
                        "From" + Environment.NewLine + "(Warehouse)",
                        "Location Code",
                        "From" + Environment.NewLine + "(Location)",
                        "From" + Environment.NewLine + "(Bin)",
                        "Warehouse Code",

                        //16-20
                        "To" + Environment.NewLine + "(Warehouse)",
                        "Location Code",
                        "To" + Environment.NewLine + "(Location)",
                        "To" + Environment.NewLine + "(Bin)",
                        "Seller"
                    },
                    new int[] 
                    {
                        //0
                        80,

                        //1-5
                        20, 130, 0, 0, 150, 
                        
                        //6-10
                        120, 200, 100, 100, 0,

                        //11-15
                        200, 0, 200, 130, 0, 
                        
                        //16-20
                        200, 0, 200, 130, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 10, 12, 15, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(DocNo, DocSeqNo) As TransferBinReq, DocNo, DocSeqNo, ");
            SQL.AppendLine("Source, ProdCode, ProdDesc, Color, ");
            SQL.AppendLine("WhsCodeFrom, WhsNameFrom, LocCodeFrom, LocNameFrom, BinFrom, ");
            SQL.AppendLine("WhsCodeTo, WhsNameTo, LocCodeTo, LocNameTo, BinTo, ");
            SQL.AppendLine("Qty, SellerName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select B.DocNo, B.DocSeqNo, B.Source, F.ProdCode, F.ProdDesc, F.Color, ");
            SQL.AppendLine("    B.WhsCode As WhsCodeFrom, C.WhsName As WhsNameFrom, A.LocCode As LocCodeFrom, H.LocName As LocNameFrom, B.Bin As BinFrom, ");
            SQL.AppendLine("    A.WhsCode As WhsCodeTo, D.WhsName As WhsNameTo, Null As LocCodeTo, Null As LocNameTo, A.Bin As BinTo, ");
            SQL.AppendLine("    B.Qty, G.SellerCode, G.SellerName ");
            SQL.AppendLine("    From TblIMMTransferBinReqHdr A ");
            SQL.AppendLine("    Inner Join TblIMMTransferBinReqDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo And B.TransferBinDocNo Is Null And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblWarehouse C On B.WhsCode=C.WhsCode ");
            SQL.AppendLine("    Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("    Inner Join TblIMMStockSummary E On B.Source=E.Source And B.WhsCode=E.WhsCode And B.Bin=E.Bin And E.Qty>0.00 ");
            SQL.AppendLine("    Inner Join TblIMMProduct F On E.ProdCode=F.ProdCode ");
            SQL.AppendLine("    Inner Join TblIMMSeller G On E.SellerCode=G.SellerCode ");
            SQL.AppendLine("    Left Join TblIMMLocation H On A.LocCode=H.LocCode ");
            SQL.AppendLine("    Where A.WhsCode=@WhsCode ");
            if (mLocCode.Length > 0)
                SQL.AppendLine("And A.LocCode=@LocCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=A.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") T Where 1=1 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", Filter2 = string.Empty, Source= string.Empty;
                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r<mFrmParent.Grd1.Rows.Count;r++)
                    {
                        Source = Sm.GetGrdStr(mFrmParent.Grd1, r, 4);
                        if (Source.Length != 0)
                        {
                            if (Filter2.Length > 0) Filter2 += " And ";
                            Filter2 += "(Source<>@Source0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }

                if (Filter2.Length != 0) Filter2 = " And (" + Filter2 + ")";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@LocCode", mLocCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "TransferBinReq", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "ProdCode", "ProdDesc" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSellerCode), "SellerCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "WhsCodeTo", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBin), "BinTo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "Source", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        string.Concat(mSQL, Filter, Filter2, " Order By ProdDesc, ProdCode, Source;"),
                        new string[] 
                        { 
                            //0
                           "TransferBinReq",

                           //1-5
                           "DocNo", "DocSeqNo", "Source", "ProdCode", "ProdDesc", 

                           //6-10
                           "Color", "Qty", "WhsCodeFrom", "WhsNameFrom", "LocCodeFrom", 
                           
                           //11-15
                           "LocNameFrom", "BinFrom", "WhsCodeTo", "WhsNameTo", "LocCodeTo", 
                           
                           //16-18
                           "LocNameTo", "BinTo", "SellerName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 20);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
        }

        private bool IsDataAlreadyChosen(int r)
        {
            string Key = string.Concat(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 5));
            for (int i = 0; i <= mFrmParent.Grd1.Rows.Count - 1; i++)
                if (Sm.CompareStr(Key, string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, i, 1), Sm.GetGrdStr(mFrmParent.Grd1, i, 4)))) 
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r= 0; r< Grd1.Rows.Count;r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Requested#");
        }

        private void LueSellerCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSellerCode, new Sm.RefreshLue1(Sl.SetLueIMMSellerCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSellerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Seller");
        }

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(Sl.SetLueBin));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bin");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        #endregion

        #endregion
    }
}
