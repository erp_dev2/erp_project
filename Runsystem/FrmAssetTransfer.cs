﻿#region Update
/*
    03/08/2017 [WED] Tambah From dan To Location, berdasarkan parameter IsAssetTransferUseLocation
    30/08/2017 [WED] tambah save ke location movement dan location summary
    31/01/2018 [WED] tambah kolom Display Name
    24/05/2018 [HAR] update ke master asset (costcenter dan asset category)
    14/01/2020 [HAR/IOK] saat ambil costcenter to diubah dari berdasarkan ccname menjadi berdasarkan ccCodeto 
    trx assettrasnferrequest
    15/01/2020 [WED/SIER] tambah approval
    18/08/2022 [TYO/PRODUCT] inisialisasi parameter IsFilterByCC 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetTransfer : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mDocNo = string.Empty,
            mAccessInd = string.Empty,
            mLocCodeFrom = string.Empty,
            mLocCodeTo = string.Empty;

        internal FrmAssetTransferFind FrmFind;
        internal bool 
            mIsAssetTransferUseLocation = false, 
            mIsNeedApproval = false,
            mIsFilterByCC = false;

        #endregion

        #region Constructor

        public FrmAssetTransfer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Asset Transfer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (!mIsNeedApproval)
                {
                    LblStatus.Visible = TxtStatus.Visible = false;
                }
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Asset Code",
                        "",
                        "Asset Name",
                        "Display Name",
                        "Remark"
                    },
                     new int[] 
                    {
                        20, 
                        100, 20, 300, 200, 200
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtCCCode2, TxtCCCode, TxtLocCode, TxtLocCode2, MeeRemark, 
                        TxtAssetTransferRequestDocNo, TxtAssetCatCode, TxtAssetCatCode2
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    TxtDocNo.Focus();
                    BtnAssetTransferRequestDocNo.Visible = true;
                    BtnAssetTransferRequestDocNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
                    DteDocDt.Focus();
                    BtnAssetTransferRequestDocNo.Enabled = true;
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mLocCodeFrom = string.Empty;
            mLocCodeTo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtCCCode2, TxtCCCode, TxtLocCode, TxtLocCode2, 
                TxtAssetTransferRequestDocNo, MeeRemark, TxtAssetCatCode, TxtAssetCatCode2, TxtStatus
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssetTransferFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            if (mIsNeedApproval) TxtStatus.EditValue = "Outstanding";
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAsset(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AssetTransfer", "TblAssetTransfer");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAssetTransferHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
                    cml.Add(SaveAssetMovement(DocNo, Row));
            }

            if (mIsAssetTransferUseLocation && TxtLocCode.Text.Length > 0 && TxtLocCode2.Text.Length > 0)
            {
                var SQLCount = new StringBuilder();
                SQLCount.AppendLine("Select Count(AssetCode) As Count From TblAssetTransferRequestDtl Where DocNo = '" + TxtAssetTransferRequestDocNo.Text + "'; ");
                decimal CountAssetCode = Decimal.Parse(Sm.GetValue(SQLCount.ToString()));
                
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Group_Concat(AssetCode) As AssetCode From TblAssetTransferRequestDtl Where DocNo = '" + TxtAssetTransferRequestDocNo.Text + "'; ");
                string AssetCode = Sm.GetValue(SQL.ToString()); ;

                cml.Add(UpdateLocationTO(AssetCode, mLocCodeFrom, mLocCodeTo, DocNo));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveLocationMovement(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAssetTransferRequestDocNo, "Request#", false)||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Asset data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Asset is empty.")) return true;

                Msg =
                    "Asset Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                    "Asset Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine;
            }
            return false;
        }

        private MySqlCommand SaveAssetTransferHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssetTransfer(DocNo, DocDt, Status, AssetTransferRequestDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @AssetTransferRequestDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            if (mIsNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='AssetTransfer'; ");
            }

            SQL.AppendLine("Update TblAssetTransfer Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='AssetTransfer' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AssetTransferRequestDocNo", TxtAssetTransferRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAssetMovement(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssetMovement(DocType, DocNo, DNo, DocDt, AssetCode, CCCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values('1', @DocNo, @DNo, @DocDt, @AssetCode, @CCCode, @Remark, @CreateBy, CurrentDateTime()); ");
            SQL.AppendLine("Select '1', @DocNo, @DNo, @DocDt, @AssetCode, @CCCode, @Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblAssetTransfer ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A'; ");

            SQL.AppendLine("Insert Into TblAssetSummary(AssetCode, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @AssetCode, @CCCode, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblAssetTransfer ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("    On Duplicate Key Update CCCode = @CCCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblAsset Set CCCode = @CCCode, AssetCategoryCode=@AssetcategoryCode Where AssetCode = @AssetCode ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblAssetTransfer ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString()};

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetValue("Select CCCodeTo From TblAssettransferrequesthdr Where DocNo = '" + TxtAssetTransferRequestDocNo.Text + "' "));
            Sm.CmParam<String>(ref cm, "@AssetcategoryCode", Sm.GetValue("Select AssetCategoryCode From TblAssetCategory Where AssetCategoryname = '" + TxtAssetCatCode2.Text + "' "));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveAssetSummary(int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblAssetSummary(AssetCode, CCCode, CreateBy, CreateDt) " +
        //            "Values(@AssetCode, @CCCode, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetValue("Select CCCode From TblCostCenter Where CCName = '"+TxtCCCode2.Text+"' "));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateLocationTO(string AssetCode, string LocCodeFrom, string LocCodeTo, string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Update TblTOHdr Set ");
            SQL.AppendLine("  LocCode = @LocCodeTo, ");
            SQL.AppendLine("  LastUpBy = @CreateBy, ");
            SQL.AppendLine("  LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where Find_In_Set(AssetCode, @AssetCode) ");
            SQL.AppendLine("And LocCode = @LocCodeFrom ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblAssetTransfer ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@LocCodeFrom", LocCodeFrom);
            Sm.CmParam<String>(ref cm, "@LocCodeTo", LocCodeTo);
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLocationMovement(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLocationMovement(DocType, DocNo, DNo, DocDt, AssetCode, LocCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '1', @DocNo, @DNo, @DocDt, @AssetCode, @LocCode, @Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblAssetTransfer ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A'; ");

            SQL.AppendLine("Insert Into TblLocationSummary(AssetCode, LocCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @AssetCode, @LocCode, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblAssetTransfer ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("On Duplicate Key Update LocCode = @LocCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetValue("Select LocCode From TblLocation Where LocName = '" + TxtLocCode2.Text + "' "));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowAssetTransferHdr(DocNo);
                ShowAssetTransferDtl(TxtAssetTransferRequestDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssetTransferHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.CCName As CCCodeFrom, D.CCName As CCCodeTo, ");
            SQL.AppendLine("E.LocName As LocCodeFrom, F.LocName As LocCodeTo, ");
            SQL.AppendLine("A.AssetTransferRequestDocNo, G.AssetCategoryname AstFrom, H.AssetCategoryName AstTo, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As StatusDesc ");
            SQL.AppendLine("From TblAssetTransfer A ");
            SQL.AppendLine("Inner Join TblAssetTransferRequesthdr B On A.AssetTransferRequestDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.CCCodeFrom = C.CCCode ");
            SQL.AppendLine("Left Join TblCostCenter D On B.CCCodeTo = D.CCCode ");
            SQL.AppendLine("Left Join TblLocation E On B.LocCodeFrom = E.LocCode ");
            SQL.AppendLine("Left Join TblLocation F On B.LocCodeTo = F.LocCode ");
            SQL.AppendLine("left Join TblAssetcategory G On B.AssetCategoryCodeFrom = G.AssetCategoryCode ");
            SQL.AppendLine("left Join TblAssetcategory H On B.AssetCategoryCodeTo = H.AssetCategoryCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "DocDt", "CCCodeFrom", "CCCodeTo", "LocCodeFrom", "LocCodeTo", 
                    "AssetTransferRequestDocNo", "AstFrom", "AstTo", "Remark", "StatusDesc"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtCCCode.EditValue = Sm.DrStr(dr, c[2]);
                    TxtCCCode2.EditValue = Sm.DrStr(dr, c[3]);
                    TxtLocCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtLocCode2.EditValue = Sm.DrStr(dr, c[5]);
                    TxtAssetTransferRequestDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    TxtAssetCatCode.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAssetCatCode2.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        public void ShowCostCenter(string RequestDocNo)
        {
            var SQLCC = new StringBuilder();

            SQLCC.AppendLine("Select C.CCName As CCCodeFrom, D.CCName As CCCodeTo, E.LocName As LocCodeFrom, F.LocName As LocCodeTo ");
            SQLCC.AppendLine("From TblAssetTransferRequesthdr B ");
            SQLCC.AppendLine("Inner Join TblCostCenter C On B.CCCodeFrom = C.CCCode ");
            SQLCC.AppendLine("Left Join TblCostCenter D On B.CCCodeTo = D.CCCode ");
            SQLCC.AppendLine("Left Join TblLocation E On B.LocCodeFrom = E.LocCode ");
            SQLCC.AppendLine("Left Join TblLocation F On B.LocCodeTo = F.LocCode ");
            SQLCC.AppendLine("Where B.DocNo = @RequestDocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@RequestDocNo", RequestDocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQLCC.ToString(),
                    new string[] 
                    { 
                        "CCCodeFrom", 
                        "CCCodeTo", "LocCodeFrom", "LocCodeTo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtCCCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtCCCode2.EditValue = Sm.DrStr(dr, c[1]);
                        TxtLocCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtLocCode2.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        public void ShowAssetTransferDtl(string RequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.AssetCode, C.AssetName, C.DisplayName, B.Remark ");
            SQL.AppendLine("From TblAssetTransferRequestHdr A ");
            SQL.AppendLine("Inner Join TblAssetTransferRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblAsset C On B.AssetCode=C.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", RequestDocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "AssetCode", "AssetName", "DisplayName", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        
        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAssetTransferUseLocation = Sm.GetParameter("IsAssetTransferUseLocation") == "Y";
            mIsNeedApproval = IsNeedApproval();
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
        }

        private bool IsNeedApproval()
        {
            return Sm.GetValue("Select 1 From TblDocApprovalSetting Where DocType = 'AssetTransfer' Limit 1; ").Length > 0;
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #region Event

        #region Misc Control Events

        private void BtnAssetTransferRequestDocNo_Click(object sender, EventArgs e)
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtCCCode2, TxtCCCode, TxtAssetTransferRequestDocNo, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FormShowDialog(new FrmAssetTransferDlg(this));
        }

        #endregion

        #endregion

    }
}
