#region Update
    /*
     *  22/03/2022 [ICA/PHT] New Apps 
     *  19/04/2022 [ICA/PHT] mengubah source dari joindate ke permanentdate, menambah filter site, mengambil grade awal dari field FirstDate (bukan dari pps lagi)
     *  19/04/2022 [ICA/PHT] mengubah job transfer saat insert pps menjadi GA
     *  09/05/2022 [ICA/PHT] bug saat menarik durationmth menyebabkan grade history tidak terupdate
     *  28/06/2022 [ICA/PHT] menyimpan PPS DocNo yg terbentuk atau PPS DocNo yg menyebabkan tidak terbentuk (PPS terakhir)
     *  19/07/2022 [ICA/PHT] PPS StartDt adalah tanggal 21 dan Date di Grade History ambil dari DocDt PPS
    */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPeriodicGradeAdvancement : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNoOfYrGradeLevelChange = 0;

        #endregion

        #region Constructor

        public FrmPeriodicGradeAdvancement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                GetParameter();
                Sl.SetLueYr(LueYr, "1996", 10);
                Sl.SetLueOption(ref LueMth, "PeriodicGradeSalaryAdvancementMth");
                Sl.SetLueSiteCode(ref LueSiteCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Button Click

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (
                (Sm.StdMsgYN("Process", string.Empty, mMenuCode) == DialogResult.No) || IsProcessDataNotValid()) return;
            try
            {
                var l = new List<Employee>();
                var l2 = new List<GradeIncrease>();
                string Msg = string.Empty;

                ProcessData1(ref l);
                ProcessData2(ref l, ref l2);
                if (l2.Count > 0) ProcessSaveData(ref l2);

                Msg += l2.Count + " data is processed.";
                Msg += Environment.NewLine;
                Msg += l2.Where(w => w.IsAutoCreatePPS).Count() + " data has been formed PPS documents.";

                Sm.StdMsg(mMsgType.Info, Msg);

                l.Clear(); l2.Clear();
                LueYr.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Process Data
        private void ProcessData1(ref List<Employee> l)
        {
            l.Clear();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '000' DNo, A.EmpCode, A.FirstGrdLvlCode GrdLvlCode, A.DeptCode, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When Right(A.JoinDt, 2) != '01' ");
            SQL.AppendLine("        Then Replace( Date_Add( ");
            SQL.AppendLine("            Date_Add(A.JoinDt, Interval 1 Month), ");
            SQL.AppendLine("            Interval (Cast(Right(A.JoinDt, 2) As Int)-1)*-1 Day ");
            SQL.AppendLine("        ), '-','') ");
            SQL.AppendLine("    Else A.JoinDt ");
            SQL.AppendLine("End As SystemJoinDt, ");
            SQL.AppendLine("'N' IsAlreadyPromotion, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When Right(A.LeaveStartDt, 2) != '01' ");
            SQL.AppendLine("        Then Replace( Date_Add( ");
            SQL.AppendLine("            Date_Add(A.LeaveStartDt, Interval 1 Month), ");
            SQL.AppendLine("            Interval (Cast(Right(A.LeaveStartDt, 2) As Int)-1)*-1 Day ");
            SQL.AppendLine("        ), '-','') ");
            SQL.AppendLine("    Else A.LeaveStartDt ");
            SQL.AppendLine("End As GrdLvlIncreaseDt, ");
            SQL.AppendLine("0.00 WorkPeriodYr, 0.00 WorkPeriodMth ");
            SQL.AppendLine("From TblEmployee A ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("	Select EmpCode, If(X1.GrdLvlCodeOld is null, X1.GrdLvlCodeNew, X1.GrdLvlCodeOld) GrdLvlCodeOld ");
            //SQL.AppendLine("	From TblPPS X1 ");
            //SQL.AppendLine("	Where CancelInd = 'N' ");
            //SQL.AppendLine("        And Status = 'A' ");
            //SQL.AppendLine("		And CreateDt = (Select MIN(CreateDt) From TblPPS Where EmpCode = X1.EmpCode And CancelInd = 'N' And Status = 'A') ");
            //SQL.AppendLine(")B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Where A.FirstGrdLvlCode Is Not Null And A.LeaveStartDt Is Not Null ");
            SQL.AppendLine("    And A.SiteCode = @SiteCode ");
            SQL.AppendLine("    And ((A.ResignDt Is Not Null And A.ResignDt>=(Select concat(replace(curdate(), '-', '')))) Or A.ResignDt Is NULL) ");
            SQL.AppendLine("	And TimeStampDiff(Year, A.LeaveStartDt, Concat('" + Sm.GetLue(LueYr) + "', '" + Sm.GetLue(LueMth) + "', '01')) >= "+mNoOfYrGradeLevelChange+" ");
            SQL.AppendLine("	And Not Exists( ");
            SQL.AppendLine("	    Select 1  ");
            SQL.AppendLine("	    From TblPeriodicGradeAdvancement ");
            SQL.AppendLine("	    Where EmpCode = A.EmpCode ");
            SQL.AppendLine("	) ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select A.DNo, A.EmpCode, A.GrdLvlCode, B.DeptCode, A.SystemJoinDt, 'Y' IsAlreadyPromotion, ");
            SQL.AppendLine("Concat(A.GrdLvlIncreaseYr, A.GrdLvlIncreaseMth, '01') GrdLvlIncreaseDt, A.WorkPeriodYr, A.WorkPeriodMth ");
            SQL.AppendLine("From TblPeriodicGradeAdvancement A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Where B.SiteCode = @SiteCode ");
            SQL.AppendLine("    And ((B.ResignDt Is Not Null And B.ResignDt>=(Select concat(replace(curdate(), '-', '')))) Or B.ResignDt Is NULL) ");
            SQL.AppendLine("    AND A.DNo = (SELECT MAX(DNo) FROM TblPeriodicGradeAdvancement WHERE EmpCode = A.EmpCode) ");
            SQL.AppendLine("	And TimeStampDiff(Year, Concat(GrdLvlIncreaseYr, GrdLvlIncreaseMth, '01'), Concat('"+Sm.GetLue(LueYr)+ "', '" + Sm.GetLue(LueMth) + "', '01')) >= "+mNoOfYrGradeLevelChange+" ");
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "EmpCode",

                    //1-5
                    "DNo",
                    "GrdLvlCode",
                    "DeptCode",
                    "SystemJoinDt",
                    "GrdLvlIncreaseDt",
                    
                    //6-7
                    "WorkPeriodYr",
                    "WorkPeriodMth",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1]),
                            GrdLvlCode = Sm.DrStr(dr, c[2]),
                            DeptCode = Sm.DrStr(dr, c[3]),
                            SystemJoinDt = Sm.DrStr(dr, c[4]),
                            GrdLvlIncreaseDt = Sm.DrStr(dr, c[5]),
                            WorkPeriodYr = Sm.DrDec(dr, c[6]),
                            WorkPeriodMth = Sm.DrDec(dr, c[7]),
                        }) ;
                    }
                }
                dr.Close();
            }
        }

        private void ProcessData2(ref List<Employee> l, ref List<GradeIncrease> l2)
        {
            l2.Clear();
            foreach(var x in l)
            {
                string GrdLvlCode = 
                    string.Empty,
                    GrdLvlIncreaseYr = string.Empty,
                    GrdLvlIncreaseMth = string.Empty, 
                    Yr = string.Empty,
                    Mth = string.Empty,
                    PeriodicPromotionSettingDocNo = string.Empty;
                bool IsFirst = true;
                int 
                    WorkPeriodYr = 0,
                    WorkPeriodMth = 0,
                    Subtractor = 0,
                    JoinDtYr = 0,
                    JoinDtMth = 0;

                for (DateTime date = Sm.ConvertDate(x.GrdLvlIncreaseDt).AddYears(mNoOfYrGradeLevelChange); date <= Sm.ConvertDate(Sm.GetLue(LueYr)+Sm.GetLue(LueMth)+"31"); date = date.AddYears(mNoOfYrGradeLevelChange))
                {
                    Subtractor = GetSubtractor((IsFirst ? x.GrdLvlCode : GrdLvlCode));
                    PeriodicPromotionSettingDocNo = GetPeriodicPromotionSettingDocNo((IsFirst ? x.GrdLvlCode : GrdLvlCode));
                    GrdLvlCode = GetNewGradeLevel((IsFirst ? x.GrdLvlCode : GrdLvlCode));
                    Yr = date.Year.ToString();
                    Mth = Sm.Right("00"+date.Month.ToString(), 2);
                    WorkPeriodYr = (IsFirst ? Convert.ToInt32(x.WorkPeriodYr) : WorkPeriodYr);
                    WorkPeriodMth = (IsFirst ? Convert.ToInt32(x.WorkPeriodMth) : WorkPeriodMth);
                    JoinDtYr = Convert.ToInt32(Sm.Left(x.SystemJoinDt, 4));
                    JoinDtMth = Convert.ToInt32(x.SystemJoinDt.Substring(4, 2));

                    #region Get GrdLvlIncreaseDt
                    if (Mth == "01" || Mth == "07")
                    {
                        GrdLvlIncreaseYr = Yr;
                        GrdLvlIncreaseMth = Mth;
                    }
                    else if(decimal.Parse(Mth) > 1 && Decimal.Parse(Mth) <= 7)
                    {
                        GrdLvlIncreaseYr = Yr;
                        GrdLvlIncreaseMth = "07";
                    }
                    else
                    {
                        GrdLvlIncreaseYr = (Decimal.Parse(Yr)+1).ToString();
                        GrdLvlIncreaseMth = "01";
                    }
                    #endregion

                    #region Get Working Period
                    if(WorkPeriodYr == 0)
                    {
                        if (Convert.ToInt32(GrdLvlIncreaseMth) > JoinDtMth)
                        {
                            WorkPeriodYr = (Convert.ToInt32(GrdLvlIncreaseYr) - JoinDtYr) - Subtractor;
                            WorkPeriodMth =Convert.ToInt32(GrdLvlIncreaseMth) - JoinDtMth;
                        }
                        else
                        {
                            WorkPeriodYr = (Convert.ToInt32(GrdLvlIncreaseYr) - JoinDtYr - 1) - Subtractor;
                            WorkPeriodMth = 12 + Convert.ToInt32(GrdLvlIncreaseMth) - JoinDtMth;
                        }
                    }
                    else
                    {
                        WorkPeriodYr = WorkPeriodYr + mNoOfYrGradeLevelChange - Subtractor;
                        WorkPeriodMth = WorkPeriodMth;
                    }
                    #endregion

                    IsFirst = false;
                }

                //bool IsGradeExists = Sm.IsDataExist("Select 1 From TblEmployee Where EmpCode=@Param1 And GrdLvlCode = @Param2;", x.EmpCode, GrdLvlCode, string.Empty);
                bool IsEmpPPSExists = Sm.IsDataExist("Select 1 From TblPPS Where EmpCode=@Param1 And StartDt >= @Param2 And CancelInd = 'N';", x.EmpCode, Sm.FormatDate(Sm.ConvertDate(GrdLvlIncreaseYr + GrdLvlIncreaseMth + "21").AddMonths(-2)), string.Empty);
                if (GrdLvlIncreaseYr.Length > 0 && GrdLvlIncreaseMth.Length > 0 && GrdLvlCode.Length > 0)
                {
                    l2.Add(new GradeIncrease()
                    {
                        EmpCode = x.EmpCode,
                        DNo = Sm.Right("00" + (Decimal.Parse(x.DNo) + 1), 3), 
                        SystemJoinDt = x.SystemJoinDt,
                        GrdLvlCode = GrdLvlCode,
                        GrdLvlIncreaseDt = GrdLvlIncreaseYr + GrdLvlIncreaseMth + "21",
                        GrdLvlIncreaseYr = GrdLvlIncreaseYr,
                        GrdLvlIncreaseMth = GrdLvlIncreaseMth,
                        WorkPeriodYr = WorkPeriodYr,
                        WorkPeriodMth = WorkPeriodMth,
                        Subtractor = Subtractor,
                        PeriodicPromotionSettingDocNo = PeriodicPromotionSettingDocNo,
                        IsAutoCreatePPS = (!IsEmpPPSExists)
                    });
                }
            }
        }

        private void ProcessSaveData(ref List<GradeIncrease> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Date = Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01";
            string 
                PPSDocNo = string.Empty,
                PGALogCode = string.Empty, 
                LastPPSDt = string.Empty,
                EmpGrdHistoryDNo = string.Empty;
            bool IsFirst = true;
            int i = 0, j = 0;

            PGALogCode = Sm.GetValue("Select MAX(PGALogCode) From TblPeriodicGradeAdvancementLog");
            PGALogCode = Sm.Right("000" + (Decimal.Parse(PGALogCode.Length > 0 ? PGALogCode : "0") + 1).ToString(), 3);

            foreach (var x in l2)
            {
                if (IsFirst)
                {
                    SQL.AppendLine("INSERT INTO TblPeriodicGradeAdvancement ");
                    SQL.AppendLine("(EmpCode, DNo, SystemJoinDt, GrdLvlCode, GrdLvlIncreaseYr, GrdLvlIncreaseMth, WorkPeriodYr, ");
                    SQL.AppendLine("WorkPeriodMth, Subtractor, PeriodicPromotionSettingDocNo, CreateBy, CreateDt) ");

                    SQL.AppendLine("Values");
                    IsFirst = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@EmpCode_" + i.ToString() + ", @DNo_" + i.ToString() + ", @SystemJoinDt_" + i.ToString() + ", @GrdLvlCode_" + i.ToString() + ", ");
                SQL.AppendLine("@GrdLvlIncreaseYr_" + i.ToString() + ", @GrdLvlIncreaseMth_" + i.ToString() + ", @WorkPeriodYr_" + i.ToString() + ", ");
                SQL.AppendLine("@WorkPeriodMth_" + i.ToString() + ", @Subtractor_" + i.ToString() + ", @PeriodicPromotionSettingDocNo_" + i.ToString() + ", ");
                SQL.AppendLine("@UserCode, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@EmpCode_" + i.ToString(), x.EmpCode);
                Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), x.DNo);
                Sm.CmParam<String>(ref cm, "@SystemJoinDt_" + i.ToString(), x.SystemJoinDt);
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + i.ToString(), x.GrdLvlCode);
                Sm.CmParam<String>(ref cm, "@GrdLvlIncreaseYr_" + i.ToString(), x.GrdLvlIncreaseYr);
                Sm.CmParam<String>(ref cm, "@GrdLvlIncreaseMth_" + i.ToString(), x.GrdLvlIncreaseMth);
                Sm.CmParam<Decimal>(ref cm, "@WorkPeriodYr_" + i.ToString(), x.WorkPeriodYr);
                Sm.CmParam<Decimal>(ref cm, "@WorkPeriodMth_" + i.ToString(), x.WorkPeriodMth);
                Sm.CmParam<Decimal>(ref cm, "@Subtractor_" + i.ToString(), x.Subtractor);
                Sm.CmParam<String>(ref cm, "@PeriodicPromotionSettingDocNo_" + i.ToString(), x.PeriodicPromotionSettingDocNo);

                i++;
                
            }

            SQL.AppendLine("; ");

            //save pps
            foreach (var x in l2)
            {
                if(x.IsAutoCreatePPS)
                {
                    PPSDocNo = Sm.GenerateDocNo(Date, "PPS", "TblPPS", (j + 1).ToString());
                    LastPPSDt = Sm.GetValue("Select StartDt From TblPPS Where EmpCode = @Param Order By StartDt Desc Limit 1;", x.EmpCode);
                    EmpGrdHistoryDNo = Sm.GetValue("Select DNo from TblEmployeeGradeHistory Where EmpCode = @Param Order By DNo Desc Limit 1;", x.EmpCode);
                    EmpGrdHistoryDNo = Sm.Right("000" + (Decimal.Parse(EmpGrdHistoryDNo.Length > 0 ? EmpGrdHistoryDNo : "0") + 1).ToString(), 3);

                    //save pps
                    SQL.AppendLine("Insert Into TblPPS ");
                    SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, Jobtransfer, StartDt, EmployeeRequestDocNo, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, ");
                    SQL.AppendLine("DeptCodeOld, PosCodeOld, PositionStatusCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, PGCodeOld, SiteCodeOld, ResignDtOld, ");
                    SQL.AppendLine("SectionCodeOld, LevelCodeOld, DeptCodeNew, PosCodeNew, PositionStatusCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, ");
                    SQL.AppendLine("SiteCodeNew, ResignDtNew, SectionCodeNew, LevelCodeNew, UpdLeaveStartDtInd, WorkPeriodYr, WorkPeriodMth, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @PPSDocNo_" + j.ToString() + ", @DocDt_" + j.ToString() + ", 'N', 'A', 'GA', @StartDt_" + j.ToString() + ", Null, EmpCode, Null, Null, ");
                    SQL.AppendLine("DeptCode, PosCode, PositionStatusCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, ResignDt, ");
                    SQL.AppendLine("SectionCode, LevelCode, DeptCode, PosCode, PositionStatusCode, @GrdLvlCodeNew_" + j.ToString() + ", EmploymentStatus, SystemType, PayrunPeriod, PGCode, ");
                    SQL.AppendLine("SiteCode, ResignDt, SectionCode, LevelCode, 'N', IfNUll(@DurationYr_" + j.ToString() + ", '0'), IfNull(@DurationMth_" + j.ToString() + ", '0'), Null, @UserCode, CurrentDateTime() ");
                    SQL.AppendLine("From TblEmployee ");
                    SQL.AppendLine("Where EmpCode = '" + x.EmpCode + "' ;");

                    //update employee
                    SQL.AppendLine("Update TblEmployee Set ");
                    SQL.AppendLine("    GrdLvlCode=@GrdLvlCodeNew_" + j.ToString() + ", LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where EmpCode='" + x.EmpCode + "' ;");

                    //insert TblEmployeeGradeHistory
                    //SQL.AppendLine("Insert Into TblEmployeeGradeHistory ");
                    //SQL.AppendLine("(EmpCode, DNo, DocDt, DecisionLetter, Duration, GrdLvlCode, Amt, PeriodicSettingInd, Remark, CreateBy, CreateDt) ");
                    //SQL.AppendLine("Select '" + x.EmpCode + "', Right(Concat('00000', CAST(IfNull(B.DNo, '0') AS INT)+1), 5), @StartDt_" + j.ToString() + ", NULL, Concat(IfNUll(@DurationYr_" + j.ToString() + ", '0'), ' | ', IfNull(@DurationMth_" + j.ToString() + ", '0')), @GrdLvlCodeNew_" + j.ToString() + ", A.Amt, 'Y', NULL, @UserCode, CurrentDateTime() ");
                    //SQL.AppendLine("From TblGradeSalaryDtl A");
                    //SQL.AppendLine("Left Join ( ");
                    //SQL.AppendLine("    Select Max(DNo) DNO From TblEmployeeGradeHistory Where EmpCode = '" + x.EmpCode + "' ");
                    //SQL.AppendLine(") B On 0 = 0");
                    //SQL.AppendLine("Where A.GrdSalaryCode = @GrdLvlCodeNew_" + j.ToString() + " And A.WorkPeriod = @DurationYr_" + j.ToString() + " And A.Month = @DurationMth_" + j.ToString() + "; ");

                    //update employeepps
                    SQL.AppendLine("Update TblEmployeePPS Set ");
                    SQL.AppendLine("    EndDt=@EndDt_" + j.ToString() + " ");
                    SQL.AppendLine("Where EmpCode='" + x.EmpCode + "' ");
                    SQL.AppendLine("And StartDt=( ");
                    SQL.AppendLine("    Select StartDt From (");
                    SQL.AppendLine("        Select Max(StartDt) StartDt ");
                    SQL.AppendLine("        From TblEmployeePPS ");
                    SQL.AppendLine("        Where EmpCode='" + x.EmpCode + "' ");
                    SQL.AppendLine("        And StartDt<@StartDt_" + j.ToString() + " ");
                    SQL.AppendLine("    ) T );");

                    SQL.AppendLine("Insert Into TblEmployeePPS(EmpCode, StartDt, EndDt, ");
                    SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, SectionCode, LevelCode, ");
                    SQL.AppendLine("CreateBy, CreateDt) ");
                    SQL.AppendLine("Select EmpCode, @StartDt_" + j.ToString() + ", Null,");
                    SQL.AppendLine("DeptCode, PosCode, @GrdLvlCodeNew_" + j.ToString() + ", EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, SectionCode, LevelCode, ");
                    SQL.AppendLine("@UserCode, CurrentDateTime() ");
                    SQL.AppendLine("From TblEmployee ");
                    SQL.AppendLine("Where EmpCode = '" + x.EmpCode + "' ;");

                    SQL.AppendLine("Update TblPeriodicGradeAdvancement ");
                    SQL.AppendLine("Set PPSDocNo = @PPSDocNo_" + j.ToString());
                    SQL.AppendLine(" Where EmpCode = '" + x.EmpCode + "' And DNo = '" + x.DNo + "'; ");

                    Sm.CmParam<String>(ref cm, "@PPSDocNo_" + j.ToString(), PPSDocNo);
                    Sm.CmParam<String>(ref cm, "@DocDt_" + j.ToString(), Date);
                    Sm.CmParam<String>(ref cm, "@EndDt_" + j.ToString(), Sm.Left(Sm.FormatDate(Sm.ConvertDate(x.GrdLvlIncreaseDt).AddMonths(-2).AddDays(-1)), 8));
                    Sm.CmParam<String>(ref cm, "@StartDt_" + j.ToString(), Sm.Left(Sm.FormatDate(Sm.ConvertDate(x.GrdLvlIncreaseDt).AddMonths(-2)), 8));
                    Sm.CmParam<String>(ref cm, "@GrdLvlCodeNew_" + j.ToString(), x.GrdLvlCode);
                    Sm.CmParam<Decimal>(ref cm, "@DurationYr_" + j.ToString(), x.WorkPeriodYr);
                    Sm.CmParam<String>(ref cm, "@DurationMth_" + j.ToString(), Sm.Right("00" + x.WorkPeriodMth, 2));

                    j++;
                }
                else
                {
                    string LastPPSDocNo = string.Empty;
                    LastPPSDocNo = Sm.GetValue("select DocNo From TblPPS Where EmpCode = @Param And CancelInd = 'N' Order By StartDt Desc Limit 1; ", x.EmpCode);

                    SQL.AppendLine("Update TblPeriodicGradeAdvancement ");
                    SQL.AppendLine("Set PPSDocNo = '"+LastPPSDocNo+"' ");
                    SQL.AppendLine(" Where EmpCode = '" + x.EmpCode + "' And DNo = '" + x.DNo + "'; ");
                }

                //save TblEmployeeGradeHistory
                SQL.AppendLine("Insert Into TblEmployeeGradeHistory ");
                SQL.AppendLine("(EmpCode, DNo, DocDt, DecisionLetter, Duration, GrdLvlCode, Amt, PeriodicSettingInd, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("SELECT A.EmpCode, Right(Concat('00000', CAST(IfNull(C.DNo, '0') AS INT)+1), 5), B.DocDt, A.PPSDocNo, ");
                SQL.AppendLine("CONCAT(CONVERT(B.WorkPeriodYr, INT), ' | ', RIGHT(CONCAT('00', CONVERT(B.WorkPeriodMth, INT)), 2)), B.GrdLvlCodeNew, ");
                SQL.AppendLine("IfNull(D.Amt, 0.00), '" + (x.IsAutoCreatePPS ? "Y" : "N") + "', 'Grade Advancement', @UserCode, CurrentDateTime()  ");
                SQL.AppendLine("FROM TblPeriodicGradeAdvancement A ");
                SQL.AppendLine("INNER JOIN TblPPS B ON A.PPSDocNo = B.DocNo AND B.CancelInd = 'N' ");
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("	SELECT MAX(DNo) DNo ");
                SQL.AppendLine("	FROM TblEmployeeGradeHistory ");
                SQL.AppendLine("	WHERE EmpCode = '" + x.EmpCode + "' ");
                SQL.AppendLine(") C ON 0=0 ");
                SQL.AppendLine("LEFT JOIN TblGradeSalaryDtl D ON B.GrdLvlCodeNew = D.GrdSalaryCode ");
                SQL.AppendLine("	AND B.WorkPeriodYr = D.WorkPeriod ");
                SQL.AppendLine("	AND Right(CONCAT('00', CONVERT(B.WorkPeriodMth, INT)), 2) = D.Month ");
                SQL.AppendLine("WHERE A.EmpCode = '" + x.EmpCode + "' And A.DNo = '" + x.DNo + "'; ");
            }

            //save log
            SQL.AppendLine("Insert Into TblPeriodicGradeAdvancementLog ");
            SQL.AppendLine("(PGALogCode, PGALogDt, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Value ");
            SQL.AppendLine("(@PGALogCode, @PGALogDt, @SiteCode, @UserCode, CurrentDateTime()); ");

            Sm.CmParam<String>(ref cm, "@PGALogCode", PGALogCode);
            Sm.CmParam<String>(ref cm, "@PGALogDt", Sm.GetLue(LueYr)+Sm.GetLue(LueMth)+"21");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();
            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'NoOfYrGradeLevelChange' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            case "NoOfYrGradeLevelChange": mNoOfYrGradeLevelChange = (ParValue.Length > 0 ? int.Parse(ParValue) : 0); break;
                        }
                    }
                }
                dr.Close();
            }

        }

        private bool IsProcessDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsDataAlreadyProcessed();
        }

        private bool IsDataAlreadyProcessed()
        {
            string LastDtOfProcess = Sm.GetValue("Select PGALogDt From TblPeriodicGradeAdvancementLog Where SiteCode=@Param Order By PGALogCode Desc Limit 1", Sm.GetLue(LueSiteCode));
            string LastYrMthOfProcess = (LastDtOfProcess.Length > 0 ? Sm.Left(LastDtOfProcess, 6) : string.Empty);
            string CurrentYrMth = Sm.GetLue(LueYr) + Sm.GetLue(LueMth); 
            
            if(LastYrMthOfProcess.Length > 0 && Decimal.Parse(LastYrMthOfProcess) >= Decimal.Parse(CurrentYrMth))
            {
                Sm.StdMsg(mMsgType.Warning, "You should process data after " + Sm.MonthName(Sm.Right(LastYrMthOfProcess, 2)) + " " + Sm.Left(LastYrMthOfProcess, 4) + ". ");
                return true;
            }

            return false;
        }

        private string GetPeriodicPromotionSettingDocNo(string GrdLvlCode)
        {
            return
               Sm.GetValue("Select A.DocNo " +
               "From TblPeriodicPromotionSettingHdr A " +
               "Inner Join TblPeriodicPromotionSettingDtl B On A.DocNo = B.DocNo" +
               "   And A.CancelInd = 'N' " +
               "Where B.GrdLvlCode = @Param ;", GrdLvlCode);
        }
        private int GetSubtractor(string GrdLvlCode)
        {
            Decimal Subtractor = Sm.GetValueDec("Select Subtractor " +
                "From TblPeriodicPromotionSettingHdr A " +
                "Inner Join TblPeriodicPromotionSettingDtl B On A.DocNo = B.DocNo" +
                "   And A.CancelInd = 'N' " +
                "Where B.GrdLvlCode = @Param ;", GrdLvlCode);

            return Convert.ToInt32(Subtractor);
        }

        private string GetNewGradeLevel(string GrdLvlCode)
        {
            return
                Sm.GetValue("Select GrdLvlCode2 " +
                "From TblPeriodicPromotionSettingHdr A " +
                "Inner Join TblPeriodicPromotionSettingDtl B On A.DocNo = B.DocNo" +
                "   And A.CancelInd = 'N' " +
                "Where B.GrdLvlCode = @Param ;", GrdLvlCode);
        }

        #endregion

        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string DeptCode { get; set; }
            public string SystemJoinDt { get; set; }
            public string GrdLvlCode { get; set; }
            public string GrdLvlIncreaseDt { get; set; }
            public decimal WorkPeriodYr { get; set; }
            public decimal WorkPeriodMth { get; set; }
        }

        private class GradeIncrease
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string SystemJoinDt { get; set; }
            public string GrdLvlCode { get; set; }
            public string GrdLvlIncreaseDt { get; set; }
            public string GrdLvlIncreaseYr { get; set; }
            public string GrdLvlIncreaseMth { get; set; }
            public decimal WorkPeriodYr { get; set; }
            public decimal WorkPeriodMth { get; set; }
            public string PeriodicPromotionSettingDocNo { get; set; }
            public decimal Subtractor { get; set; }
            public bool IsAutoCreatePPS { get; set; }
        }

        #endregion
    }
}
