﻿#region Update
/*
    19/10/2017 [TKG] menghilangkan grade level, year, dan amount.
    04/11/2020 [ICA/PHT] Tambah Field Plavon Period
    05/04/2021 [IBL/PHT] Tambah Field COA berdasarkan parameter IsClaimUseCOA
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmClaim : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmClaimFind FrmFind;
        internal bool
            mIsCOAUseAlias = false,
            mIsClaimUseCOA = false;

        #endregion

        #region Constructor

        public FrmClaim(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
                if (!mIsClaimUseCOA)
                    LblAcNo.Visible = TxtAcNo.Visible = TxtAcDesc.Visible = BtnAcNo.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtClaimCode, ChkActInd, TxtClaimName, TxtPlavonPeriod }, true);
                    BtnAcNo.Enabled = false;
                    TxtClaimCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtClaimCode, TxtClaimName, TxtPlavonPeriod }, false);
                    BtnAcNo.Enabled = true;
                    TxtClaimCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtClaimName, ChkActInd, TxtPlavonPeriod }, false);
                    BtnAcNo.Enabled = true;
                    TxtClaimName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtClaimCode, TxtClaimName, TxtAcNo, TxtAcDesc});
            ChkActInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPlavonPeriod }, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsClaimUseCOA = Sm.GetParameterBoo("IsClaimUseCOA");
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmClaimFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtClaimCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtClaimCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { CommandText = "Delete From TblClaim Where ClaimCode=@Claim" };
                Sm.CmParam<String>(ref cm, "@ClaimCode", TxtClaimCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblClaim(ClaimCode, ActInd, ClaimName, PlavonPeriod, AcNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ClaimCode, @ActInd, @ClaimName, @PlavonPeriod, @AcNo, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ");
                SQL.AppendLine("        ActInd=@ActInd, ");
                SQL.AppendLine("        ClaimName=@ClaimName, ");
                SQL.AppendLine("        PlavonPeriod=@PlavonPeriod, ");
                SQL.AppendLine("        AcNo=@AcNo, ");
                SQL.AppendLine("        LastUpBy=@UserCode, ");
                SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@ClaimCode", TxtClaimCode.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ClaimName", TxtClaimName.Text);
                Sm.CmParam<Decimal>(ref cm, "@PlavonPeriod", Decimal.Parse(TxtPlavonPeriod.Text));
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtClaimCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ClaimCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ClaimCode", ClaimCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.ClaimCode, A.ClaimName, A.ActInd, A.PlavonPeriod, A.AcNo, " +
                        "B.AcDesc, B.Alias " +
                        "From TblClaim A Left Join TblCOA B On A.AcNo = B.AcNo " +
                        "Where A.ClaimCode = @ClaimCode; ",

                        new string[] 
                        { "ClaimCode", "ClaimName", "ActInd", "PlavonPeriod", "AcNo", "AcDesc", "Alias" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtClaimCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtClaimName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            TxtPlavonPeriod.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]),0);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtAcDesc.EditValue = (mIsCOAUseAlias ? Sm.DrStr(dr, c[5])+" ["+Sm.DrStr(dr, c[6])+"] " : Sm.DrStr(dr, c[5]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtClaimCode, "Claim code", false) ||
                Sm.IsTxtEmpty(TxtClaimName, "Claim name", false) ||
                IsClaimExisted();
        }

        private bool IsClaimExisted()
        {
            if (!TxtClaimCode.Properties.ReadOnly)
            {
                return
                    Sm.IsDataExist(
                    "Select 1 From TblClaim Where ClaimCode=@Param;",
                    TxtClaimCode.Text,
                    "Claim ( " + TxtClaimCode.Text + " ) already existed."
                    );
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtClaimCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtClaimCode);
        }

        private void TxtClaimName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtClaimName);
        }

        private void TxtPlavonPeriod_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPlavonPeriod, 0);
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmClaimDlg(this));
        }

        #endregion

       #endregion

    }
}
