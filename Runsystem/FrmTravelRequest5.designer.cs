namespace RunSystem
{
    partial class FrmTravelRequest5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTravelRequest5));
            this.panel3 = new System.Windows.Forms.Panel();
            this.LueCity = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkCompleteInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueCityDestination = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCityDestination = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LueRegionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueSiteCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPICName = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtPICCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnPIC = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.LueTransport = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.MeeResult = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeTravelService = new DevExpress.XtraEditors.MemoExEdit();
            this.LblTravelService = new System.Windows.Forms.Label();
            this.LblExpectedResult = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TmeEnd = new DevExpress.XtraEditors.TimeEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TmeStart = new DevExpress.XtraEditors.TimeEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueSite = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtArrivalStatus = new DevExpress.XtraEditors.TextEdit();
            this.LblArrivalStatus = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgMeal = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgDaily = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgCityTransport = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgTransport = new System.Windows.Forms.TabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgAccomodation = new System.Windows.Forms.TabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgOther = new System.Windows.Forms.TabPage();
            this.LueAllowance = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgApproval = new System.Windows.Forms.TabPage();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgArrival = new System.Windows.Forms.TabPage();
            this.TxtArrivalDtTm = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtArrivalChecker = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TpgBudget = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblVoucherDocNo = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCompleteInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityDestination.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRegionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTransport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeResult.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeTravelService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArrivalStatus.Properties)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgMeal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgDaily.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgCityTransport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgTransport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpgAccomodation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpgOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAllowance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpgApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.TpgArrival.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArrivalDtTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArrivalChecker.Properties)).BeginInit();
            this.TpgBudget.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(889, 0);
            this.panel1.Size = new System.Drawing.Size(70, 623);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(889, 623);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtVoucherDocNo);
            this.panel3.Controls.Add(this.LblVoucherDocNo);
            this.panel3.Controls.Add(this.LueCity);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.TmeEnd);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.TmeStart);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.DteEndDt);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.DteStartDt);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueSite);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(889, 228);
            this.panel3.TabIndex = 9;
            // 
            // LueCity
            // 
            this.LueCity.EnterMoveNextControl = true;
            this.LueCity.Location = new System.Drawing.Point(149, 93);
            this.LueCity.Margin = new System.Windows.Forms.Padding(5);
            this.LueCity.Name = "LueCity";
            this.LueCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.Appearance.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity.Properties.DropDownRows = 30;
            this.LueCity.Properties.NullText = "[Empty]";
            this.LueCity.Properties.PopupWidth = 350;
            this.LueCity.Size = new System.Drawing.Size(244, 20);
            this.LueCity.TabIndex = 20;
            this.LueCity.ToolTip = "F4 : Show/hide list";
            this.LueCity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCity.EditValueChanged += new System.EventHandler(this.LueCity_EditValueChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(149, 49);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 50;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(244, 20);
            this.TxtStatus.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(101, 52);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 14;
            this.label12.Text = "Status";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkCompleteInd);
            this.panel4.Controls.Add(this.LueCityDestination);
            this.panel4.Controls.Add(this.LblCityDestination);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.LueRegionCode);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.LueSiteCode2);
            this.panel4.Controls.Add(this.TxtPICName);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.TxtPICCode);
            this.panel4.Controls.Add(this.BtnPIC);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.LblRemark);
            this.panel4.Controls.Add(this.LueTransport);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.MeeResult);
            this.panel4.Controls.Add(this.MeeTravelService);
            this.panel4.Controls.Add(this.LblTravelService);
            this.panel4.Controls.Add(this.LblExpectedResult);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(460, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(429, 228);
            this.panel4.TabIndex = 50;
            // 
            // ChkCompleteInd
            // 
            this.ChkCompleteInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCompleteInd.Location = new System.Drawing.Point(139, 200);
            this.ChkCompleteInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCompleteInd.Name = "ChkCompleteInd";
            this.ChkCompleteInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCompleteInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCompleteInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCompleteInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCompleteInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCompleteInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCompleteInd.Properties.Caption = "Completed";
            this.ChkCompleteInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCompleteInd.Size = new System.Drawing.Size(93, 22);
            this.ChkCompleteInd.TabIndex = 52;
            // 
            // LueCityDestination
            // 
            this.LueCityDestination.EnterMoveNextControl = true;
            this.LueCityDestination.Location = new System.Drawing.Point(141, 156);
            this.LueCityDestination.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityDestination.Name = "LueCityDestination";
            this.LueCityDestination.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityDestination.Properties.Appearance.Options.UseFont = true;
            this.LueCityDestination.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityDestination.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityDestination.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityDestination.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityDestination.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityDestination.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityDestination.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityDestination.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityDestination.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityDestination.Properties.DropDownRows = 30;
            this.LueCityDestination.Properties.NullText = "[Empty]";
            this.LueCityDestination.Properties.PopupWidth = 350;
            this.LueCityDestination.Size = new System.Drawing.Size(278, 20);
            this.LueCityDestination.TabIndex = 49;
            this.LueCityDestination.ToolTip = "F4 : Show/hide list";
            this.LueCityDestination.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityDestination.EditValueChanged += new System.EventHandler(this.LueCityDestination_EditValueChanged);
            // 
            // LblCityDestination
            // 
            this.LblCityDestination.AutoSize = true;
            this.LblCityDestination.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCityDestination.ForeColor = System.Drawing.Color.Red;
            this.LblCityDestination.Location = new System.Drawing.Point(38, 159);
            this.LblCityDestination.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCityDestination.Name = "LblCityDestination";
            this.LblCityDestination.Size = new System.Drawing.Size(92, 14);
            this.LblCityDestination.TabIndex = 48;
            this.LblCityDestination.Text = "City Destination";
            this.LblCityDestination.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(89, 138);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 14);
            this.label17.TabIndex = 46;
            this.label17.Text = "Region";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueRegionCode
            // 
            this.LueRegionCode.EnterMoveNextControl = true;
            this.LueRegionCode.Location = new System.Drawing.Point(141, 135);
            this.LueRegionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueRegionCode.Name = "LueRegionCode";
            this.LueRegionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRegionCode.Properties.Appearance.Options.UseFont = true;
            this.LueRegionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRegionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRegionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRegionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRegionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRegionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRegionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRegionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRegionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRegionCode.Properties.DropDownRows = 30;
            this.LueRegionCode.Properties.NullText = "[Empty]";
            this.LueRegionCode.Properties.PopupWidth = 350;
            this.LueRegionCode.Size = new System.Drawing.Size(278, 20);
            this.LueRegionCode.TabIndex = 47;
            this.LueRegionCode.ToolTip = "F4 : Show/hide list";
            this.LueRegionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRegionCode.EditValueChanged += new System.EventHandler(this.LueRegionCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(5, 118);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(128, 14);
            this.label16.TabIndex = 44;
            this.label16.Text = "Site / Unit Destination";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode2
            // 
            this.LueSiteCode2.EnterMoveNextControl = true;
            this.LueSiteCode2.Location = new System.Drawing.Point(141, 114);
            this.LueSiteCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode2.Name = "LueSiteCode2";
            this.LueSiteCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode2.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode2.Properties.DropDownRows = 30;
            this.LueSiteCode2.Properties.NullText = "[Empty]";
            this.LueSiteCode2.Properties.PopupWidth = 350;
            this.LueSiteCode2.Size = new System.Drawing.Size(278, 20);
            this.LueSiteCode2.TabIndex = 45;
            this.LueSiteCode2.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode2.EditValueChanged += new System.EventHandler(this.LueSiteCode2_EditValueChanged);
            // 
            // TxtPICName
            // 
            this.TxtPICName.EnterMoveNextControl = true;
            this.TxtPICName.Location = new System.Drawing.Point(141, 49);
            this.TxtPICName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICName.Name = "TxtPICName";
            this.TxtPICName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPICName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICName.Properties.Appearance.Options.UseFont = true;
            this.TxtPICName.Properties.MaxLength = 250;
            this.TxtPICName.Properties.ReadOnly = true;
            this.TxtPICName.Size = new System.Drawing.Size(278, 20);
            this.TxtPICName.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(73, 53);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 14);
            this.label15.TabIndex = 38;
            this.label15.Text = "PIC Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPICCode
            // 
            this.TxtPICCode.EnterMoveNextControl = true;
            this.TxtPICCode.Location = new System.Drawing.Point(141, 28);
            this.TxtPICCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICCode.Name = "TxtPICCode";
            this.TxtPICCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPICCode.Properties.MaxLength = 250;
            this.TxtPICCode.Properties.ReadOnly = true;
            this.TxtPICCode.Size = new System.Drawing.Size(249, 20);
            this.TxtPICCode.TabIndex = 36;
            // 
            // BtnPIC
            // 
            this.BtnPIC.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPIC.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPIC.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPIC.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPIC.Appearance.Options.UseBackColor = true;
            this.BtnPIC.Appearance.Options.UseFont = true;
            this.BtnPIC.Appearance.Options.UseForeColor = true;
            this.BtnPIC.Appearance.Options.UseTextOptions = true;
            this.BtnPIC.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPIC.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPIC.Image = ((System.Drawing.Image)(resources.GetObject("BtnPIC.Image")));
            this.BtnPIC.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPIC.Location = new System.Drawing.Point(392, 28);
            this.BtnPIC.Name = "BtnPIC";
            this.BtnPIC.Size = new System.Drawing.Size(24, 21);
            this.BtnPIC.TabIndex = 37;
            this.BtnPIC.ToolTip = "Add PIC";
            this.BtnPIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPIC.ToolTipTitle = "Run System";
            this.BtnPIC.Click += new System.EventHandler(this.BtnPIC_Click);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(141, 177);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 800;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(278, 22);
            this.MeeRemark.TabIndex = 51;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.ForeColor = System.Drawing.Color.Red;
            this.LblRemark.Location = new System.Drawing.Point(86, 180);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 50;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTransport
            // 
            this.LueTransport.EnterMoveNextControl = true;
            this.LueTransport.Location = new System.Drawing.Point(141, 93);
            this.LueTransport.Margin = new System.Windows.Forms.Padding(5);
            this.LueTransport.Name = "LueTransport";
            this.LueTransport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransport.Properties.Appearance.Options.UseFont = true;
            this.LueTransport.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransport.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTransport.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransport.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTransport.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransport.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTransport.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransport.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTransport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTransport.Properties.DropDownRows = 30;
            this.LueTransport.Properties.NullText = "[Empty]";
            this.LueTransport.Properties.PopupWidth = 350;
            this.LueTransport.Size = new System.Drawing.Size(278, 20);
            this.LueTransport.TabIndex = 43;
            this.LueTransport.ToolTip = "F4 : Show/hide list";
            this.LueTransport.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTransport.EditValueChanged += new System.EventHandler(this.LueTransport_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(46, 97);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 14);
            this.label8.TabIndex = 42;
            this.label8.Text = "Transportation";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(105, 32);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 14);
            this.label7.TabIndex = 35;
            this.label7.Text = "PIC";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeResult
            // 
            this.MeeResult.EnterMoveNextControl = true;
            this.MeeResult.Location = new System.Drawing.Point(141, 70);
            this.MeeResult.Margin = new System.Windows.Forms.Padding(5);
            this.MeeResult.Name = "MeeResult";
            this.MeeResult.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeResult.Properties.Appearance.Options.UseFont = true;
            this.MeeResult.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeResult.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeResult.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeResult.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeResult.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeResult.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeResult.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeResult.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeResult.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeResult.Properties.MaxLength = 400;
            this.MeeResult.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeResult.Properties.ShowIcon = false;
            this.MeeResult.Size = new System.Drawing.Size(278, 22);
            this.MeeResult.TabIndex = 41;
            this.MeeResult.ToolTip = "F4 : Show/hide text";
            this.MeeResult.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeResult.ToolTipTitle = "Run System";
            // 
            // MeeTravelService
            // 
            this.MeeTravelService.EnterMoveNextControl = true;
            this.MeeTravelService.Location = new System.Drawing.Point(141, 5);
            this.MeeTravelService.Margin = new System.Windows.Forms.Padding(5);
            this.MeeTravelService.Name = "MeeTravelService";
            this.MeeTravelService.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTravelService.Properties.Appearance.Options.UseFont = true;
            this.MeeTravelService.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTravelService.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeTravelService.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTravelService.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeTravelService.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTravelService.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeTravelService.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeTravelService.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeTravelService.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeTravelService.Properties.MaxLength = 400;
            this.MeeTravelService.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeTravelService.Properties.ShowIcon = false;
            this.MeeTravelService.Size = new System.Drawing.Size(278, 22);
            this.MeeTravelService.TabIndex = 34;
            this.MeeTravelService.ToolTip = "F4 : Show/hide text";
            this.MeeTravelService.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeTravelService.ToolTipTitle = "Run System";
            // 
            // LblTravelService
            // 
            this.LblTravelService.AutoSize = true;
            this.LblTravelService.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTravelService.Location = new System.Drawing.Point(50, 8);
            this.LblTravelService.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTravelService.Name = "LblTravelService";
            this.LblTravelService.Size = new System.Drawing.Size(83, 14);
            this.LblTravelService.TabIndex = 33;
            this.LblTravelService.Text = "Travel Service";
            this.LblTravelService.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblExpectedResult
            // 
            this.LblExpectedResult.AutoSize = true;
            this.LblExpectedResult.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExpectedResult.ForeColor = System.Drawing.Color.Black;
            this.LblExpectedResult.Location = new System.Drawing.Point(37, 74);
            this.LblExpectedResult.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblExpectedResult.Name = "LblExpectedResult";
            this.LblExpectedResult.Size = new System.Drawing.Size(96, 14);
            this.LblExpectedResult.TabIndex = 40;
            this.LblExpectedResult.Text = "Expected Result";
            this.LblExpectedResult.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(265, 141);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "Time";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeEnd
            // 
            this.TmeEnd.EditValue = null;
            this.TmeEnd.EnterMoveNextControl = true;
            this.TmeEnd.Location = new System.Drawing.Point(305, 138);
            this.TmeEnd.Name = "TmeEnd";
            this.TmeEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeEnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeEnd.Properties.Appearance.Options.UseFont = true;
            this.TmeEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeEnd.Properties.Mask.EditMask = "HH:mm";
            this.TmeEnd.Size = new System.Drawing.Size(88, 20);
            this.TmeEnd.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(265, 119);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 14);
            this.label11.TabIndex = 23;
            this.label11.Text = "Time";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeStart
            // 
            this.TmeStart.EditValue = null;
            this.TmeStart.EnterMoveNextControl = true;
            this.TmeStart.Location = new System.Drawing.Point(305, 116);
            this.TmeStart.Name = "TmeStart";
            this.TmeStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeStart.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeStart.Properties.Appearance.Options.UseFont = true;
            this.TmeStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeStart.Properties.Mask.EditMask = "HH:mm";
            this.TmeStart.Size = new System.Drawing.Size(88, 20);
            this.TmeStart.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(85, 139);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 14);
            this.label5.TabIndex = 25;
            this.label5.Text = "End Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(149, 137);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.MaxLength = 8;
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(109, 20);
            this.DteEndDt.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(79, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 14);
            this.label4.TabIndex = 21;
            this.label4.Text = "Start Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(149, 115);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.MaxLength = 8;
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(109, 20);
            this.DteStartDt.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(116, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "City";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSite
            // 
            this.LueSite.EnterMoveNextControl = true;
            this.LueSite.Location = new System.Drawing.Point(149, 159);
            this.LueSite.Margin = new System.Windows.Forms.Padding(5);
            this.LueSite.Name = "LueSite";
            this.LueSite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.Appearance.Options.UseFont = true;
            this.LueSite.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSite.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSite.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSite.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSite.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSite.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSite.Properties.DropDownRows = 30;
            this.LueSite.Properties.NullText = "[Empty]";
            this.LueSite.Properties.PopupWidth = 350;
            this.LueSite.Size = new System.Drawing.Size(244, 20);
            this.LueSite.TabIndex = 30;
            this.LueSite.ToolTip = "F4 : Show/hide list";
            this.LueSite.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSite.EditValueChanged += new System.EventHandler(this.LueSite_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(115, 162);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 14);
            this.label14.TabIndex = 29;
            this.label14.Text = "Site";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(149, 71);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 500;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(244, 20);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(397, 71);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(60, 22);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 75);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 16;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(149, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 50;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(70, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(110, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(149, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 13;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // TxtArrivalStatus
            // 
            this.TxtArrivalStatus.EnterMoveNextControl = true;
            this.TxtArrivalStatus.Location = new System.Drawing.Point(91, 8);
            this.TxtArrivalStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtArrivalStatus.Name = "TxtArrivalStatus";
            this.TxtArrivalStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtArrivalStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtArrivalStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtArrivalStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtArrivalStatus.Properties.MaxLength = 50;
            this.TxtArrivalStatus.Properties.ReadOnly = true;
            this.TxtArrivalStatus.Size = new System.Drawing.Size(244, 20);
            this.TxtArrivalStatus.TabIndex = 52;
            // 
            // LblArrivalStatus
            // 
            this.LblArrivalStatus.AutoSize = true;
            this.LblArrivalStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblArrivalStatus.ForeColor = System.Drawing.Color.Black;
            this.LblArrivalStatus.Location = new System.Drawing.Point(7, 10);
            this.LblArrivalStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblArrivalStatus.Name = "LblArrivalStatus";
            this.LblArrivalStatus.Size = new System.Drawing.Size(78, 14);
            this.LblArrivalStatus.TabIndex = 51;
            this.LblArrivalStatus.Text = "Arrival Status";
            this.LblArrivalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 228);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(889, 395);
            this.splitContainer1.SplitterDistance = 141;
            this.splitContainer1.TabIndex = 10;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(889, 141);
            this.Grd1.TabIndex = 48;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgMeal);
            this.tabControl1.Controls.Add(this.TpgDaily);
            this.tabControl1.Controls.Add(this.TpgCityTransport);
            this.tabControl1.Controls.Add(this.TpgTransport);
            this.tabControl1.Controls.Add(this.TpgAccomodation);
            this.tabControl1.Controls.Add(this.TpgOther);
            this.tabControl1.Controls.Add(this.TpgApproval);
            this.tabControl1.Controls.Add(this.TpgArrival);
            this.tabControl1.Controls.Add(this.TpgBudget);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(889, 250);
            this.tabControl1.TabIndex = 50;
            // 
            // TpgMeal
            // 
            this.TpgMeal.Controls.Add(this.Grd2);
            this.TpgMeal.Location = new System.Drawing.Point(4, 26);
            this.TpgMeal.Name = "TpgMeal";
            this.TpgMeal.Size = new System.Drawing.Size(881, 220);
            this.TpgMeal.TabIndex = 3;
            this.TpgMeal.Text = "Meal Allowance";
            this.TpgMeal.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(881, 220);
            this.Grd2.TabIndex = 50;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpgDaily
            // 
            this.TpgDaily.BackColor = System.Drawing.Color.SkyBlue;
            this.TpgDaily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgDaily.Controls.Add(this.Grd3);
            this.TpgDaily.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgDaily.Location = new System.Drawing.Point(4, 26);
            this.TpgDaily.Name = "TpgDaily";
            this.TpgDaily.Size = new System.Drawing.Size(881, 220);
            this.TpgDaily.TabIndex = 0;
            this.TpgDaily.Text = "Daily Allowance";
            this.TpgDaily.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(877, 216);
            this.Grd3.TabIndex = 50;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgCityTransport
            // 
            this.TpgCityTransport.BackColor = System.Drawing.Color.SkyBlue;
            this.TpgCityTransport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgCityTransport.Controls.Add(this.Grd4);
            this.TpgCityTransport.Location = new System.Drawing.Point(4, 26);
            this.TpgCityTransport.Name = "TpgCityTransport";
            this.TpgCityTransport.Size = new System.Drawing.Size(881, 220);
            this.TpgCityTransport.TabIndex = 2;
            this.TpgCityTransport.Text = "City Transport";
            this.TpgCityTransport.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(877, 216);
            this.Grd4.TabIndex = 50;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd4_AfterCommitEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgTransport
            // 
            this.TpgTransport.Controls.Add(this.Grd5);
            this.TpgTransport.Location = new System.Drawing.Point(4, 26);
            this.TpgTransport.Name = "TpgTransport";
            this.TpgTransport.Padding = new System.Windows.Forms.Padding(3);
            this.TpgTransport.Size = new System.Drawing.Size(881, 220);
            this.TpgTransport.TabIndex = 6;
            this.TpgTransport.Text = "Transport";
            this.TpgTransport.UseVisualStyleBackColor = true;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(3, 3);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(875, 214);
            this.Grd5.TabIndex = 50;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpgAccomodation
            // 
            this.TpgAccomodation.Controls.Add(this.Grd6);
            this.TpgAccomodation.Location = new System.Drawing.Point(4, 26);
            this.TpgAccomodation.Name = "TpgAccomodation";
            this.TpgAccomodation.Padding = new System.Windows.Forms.Padding(3);
            this.TpgAccomodation.Size = new System.Drawing.Size(881, 220);
            this.TpgAccomodation.TabIndex = 7;
            this.TpgAccomodation.Text = "Accomodation";
            this.TpgAccomodation.UseVisualStyleBackColor = true;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(3, 3);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(875, 214);
            this.Grd6.TabIndex = 50;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // TpgOther
            // 
            this.TpgOther.Controls.Add(this.LueAllowance);
            this.TpgOther.Controls.Add(this.Grd7);
            this.TpgOther.Location = new System.Drawing.Point(4, 26);
            this.TpgOther.Name = "TpgOther";
            this.TpgOther.Padding = new System.Windows.Forms.Padding(3);
            this.TpgOther.Size = new System.Drawing.Size(881, 220);
            this.TpgOther.TabIndex = 8;
            this.TpgOther.Text = "Other Allowance";
            this.TpgOther.UseVisualStyleBackColor = true;
            // 
            // LueAllowance
            // 
            this.LueAllowance.EnterMoveNextControl = true;
            this.LueAllowance.Location = new System.Drawing.Point(286, 25);
            this.LueAllowance.Margin = new System.Windows.Forms.Padding(5);
            this.LueAllowance.Name = "LueAllowance";
            this.LueAllowance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAllowance.Properties.Appearance.Options.UseFont = true;
            this.LueAllowance.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAllowance.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAllowance.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAllowance.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAllowance.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAllowance.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAllowance.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAllowance.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAllowance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAllowance.Properties.DropDownRows = 20;
            this.LueAllowance.Properties.NullText = "[Empty]";
            this.LueAllowance.Properties.PopupWidth = 500;
            this.LueAllowance.Size = new System.Drawing.Size(172, 20);
            this.LueAllowance.TabIndex = 51;
            this.LueAllowance.ToolTip = "F4 : Show/hide list";
            this.LueAllowance.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAllowance.EditValueChanged += new System.EventHandler(this.LueAllowance_EditValueChanged);
            this.LueAllowance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueAllowance_KeyDown);
            this.LueAllowance.Leave += new System.EventHandler(this.LueAllowance_Leave);
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(3, 3);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(875, 214);
            this.Grd7.TabIndex = 50;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd7_AfterCommitEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // TpgApproval
            // 
            this.TpgApproval.Controls.Add(this.Grd8);
            this.TpgApproval.Location = new System.Drawing.Point(4, 26);
            this.TpgApproval.Name = "TpgApproval";
            this.TpgApproval.Padding = new System.Windows.Forms.Padding(3);
            this.TpgApproval.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TpgApproval.Size = new System.Drawing.Size(881, 220);
            this.TpgApproval.TabIndex = 9;
            this.TpgApproval.Text = "Approval";
            this.TpgApproval.UseVisualStyleBackColor = true;
            // 
            // Grd8
            // 
            this.Grd8.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd8.BackColorOddRows = System.Drawing.Color.White;
            this.Grd8.DefaultAutoGroupRow.Height = 21;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.FrozenArea.ColCount = 3;
            this.Grd8.FrozenArea.SortFrozenRows = true;
            this.Grd8.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd8.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd8.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd8.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.UseXPStyles = false;
            this.Grd8.Location = new System.Drawing.Point(3, 3);
            this.Grd8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd8.Name = "Grd8";
            this.Grd8.ProcessTab = false;
            this.Grd8.ReadOnly = true;
            this.Grd8.RowTextStartColNear = 3;
            this.Grd8.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd8.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd8.SearchAsType.SearchCol = null;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(875, 214);
            this.Grd8.TabIndex = 25;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgArrival
            // 
            this.TpgArrival.BackColor = System.Drawing.Color.White;
            this.TpgArrival.Controls.Add(this.TxtArrivalDtTm);
            this.TpgArrival.Controls.Add(this.label20);
            this.TpgArrival.Controls.Add(this.TxtArrivalChecker);
            this.TpgArrival.Controls.Add(this.label19);
            this.TpgArrival.Controls.Add(this.TxtArrivalStatus);
            this.TpgArrival.Controls.Add(this.LblArrivalStatus);
            this.TpgArrival.Location = new System.Drawing.Point(4, 26);
            this.TpgArrival.Name = "TpgArrival";
            this.TpgArrival.Size = new System.Drawing.Size(881, 220);
            this.TpgArrival.TabIndex = 10;
            this.TpgArrival.Text = "Arrival";
            this.TpgArrival.UseVisualStyleBackColor = true;
            // 
            // TxtArrivalDtTm
            // 
            this.TxtArrivalDtTm.EnterMoveNextControl = true;
            this.TxtArrivalDtTm.Location = new System.Drawing.Point(91, 50);
            this.TxtArrivalDtTm.Margin = new System.Windows.Forms.Padding(5);
            this.TxtArrivalDtTm.Name = "TxtArrivalDtTm";
            this.TxtArrivalDtTm.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtArrivalDtTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtArrivalDtTm.Properties.Appearance.Options.UseBackColor = true;
            this.TxtArrivalDtTm.Properties.Appearance.Options.UseFont = true;
            this.TxtArrivalDtTm.Properties.MaxLength = 50;
            this.TxtArrivalDtTm.Properties.ReadOnly = true;
            this.TxtArrivalDtTm.Size = new System.Drawing.Size(244, 20);
            this.TxtArrivalDtTm.TabIndex = 56;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(21, 52);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 14);
            this.label20.TabIndex = 55;
            this.label20.Text = "Date Time";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtArrivalChecker
            // 
            this.TxtArrivalChecker.EnterMoveNextControl = true;
            this.TxtArrivalChecker.Location = new System.Drawing.Point(91, 29);
            this.TxtArrivalChecker.Margin = new System.Windows.Forms.Padding(5);
            this.TxtArrivalChecker.Name = "TxtArrivalChecker";
            this.TxtArrivalChecker.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtArrivalChecker.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtArrivalChecker.Properties.Appearance.Options.UseBackColor = true;
            this.TxtArrivalChecker.Properties.Appearance.Options.UseFont = true;
            this.TxtArrivalChecker.Properties.MaxLength = 50;
            this.TxtArrivalChecker.Properties.ReadOnly = true;
            this.TxtArrivalChecker.Size = new System.Drawing.Size(244, 20);
            this.TxtArrivalChecker.TabIndex = 54;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(34, 31);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 14);
            this.label19.TabIndex = 53;
            this.label19.Text = "Checker";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgBudget
            // 
            this.TpgBudget.Controls.Add(this.panel5);
            this.TpgBudget.Location = new System.Drawing.Point(4, 26);
            this.TpgBudget.Name = "TpgBudget";
            this.TpgBudget.Size = new System.Drawing.Size(881, 220);
            this.TpgBudget.TabIndex = 11;
            this.TpgBudget.Text = "Budget";
            this.TpgBudget.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label34);
            this.panel5.Controls.Add(this.TxtRemainingBudget);
            this.panel5.Controls.Add(this.label35);
            this.panel5.Controls.Add(this.LueBCCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(881, 220);
            this.panel5.TabIndex = 0;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(7, 32);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(97, 14);
            this.label34.TabIndex = 54;
            this.label34.Text = "Available Budget";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(111, 29);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(229, 20);
            this.TxtRemainingBudget.TabIndex = 55;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(4, 10);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 14);
            this.label35.TabIndex = 52;
            this.label35.Text = "Budget Category";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(111, 7);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(311, 20);
            this.LueBCCode.TabIndex = 53;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 601);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 8;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(149, 181);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 50;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtVoucherDocNo.TabIndex = 32;
            // 
            // LblVoucherDocNo
            // 
            this.LblVoucherDocNo.AutoSize = true;
            this.LblVoucherDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVoucherDocNo.ForeColor = System.Drawing.Color.Black;
            this.LblVoucherDocNo.Location = new System.Drawing.Point(82, 184);
            this.LblVoucherDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblVoucherDocNo.Name = "LblVoucherDocNo";
            this.LblVoucherDocNo.Size = new System.Drawing.Size(62, 14);
            this.LblVoucherDocNo.TabIndex = 31;
            this.LblVoucherDocNo.Text = "Voucher#";
            this.LblVoucherDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmTravelRequest5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 623);
            this.Name = "FrmTravelRequest5";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCompleteInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityDestination.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRegionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTransport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeResult.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeTravelService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArrivalStatus.Properties)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgMeal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgDaily.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgCityTransport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgTransport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpgAccomodation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpgOther.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueAllowance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpgApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.TpgArrival.ResumeLayout(false);
            this.TpgArrival.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArrivalDtTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtArrivalChecker.Properties)).EndInit();
            this.TpgBudget.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.MemoExEdit MeeTravelService;
        private System.Windows.Forms.Label LblTravelService;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label LblExpectedResult;
        protected internal DevExpress.XtraEditors.LookUpEdit LueTransport;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.MemoExEdit MeeResult;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label LblRemark;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgMeal;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabPage TpgDaily;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgCityTransport;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.TabPage TpgTransport;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.TabPage TpgAccomodation;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private System.Windows.Forms.TabPage TpgOther;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private DevExpress.XtraEditors.LookUpEdit LueAllowance;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.TextEdit TxtPICCode;
        public DevExpress.XtraEditors.SimpleButton BtnPIC;
        public DevExpress.XtraEditors.TextEdit TxtPICName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        protected internal DevExpress.XtraEditors.LookUpEdit LueSiteCode2;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.LookUpEdit LueRegionCode;
        private System.Windows.Forms.TabPage TpgApproval;
        protected TenTec.Windows.iGridLib.iGrid Grd8;
        internal DevExpress.XtraEditors.TextEdit TxtArrivalStatus;
        private System.Windows.Forms.Label LblArrivalStatus;
        private System.Windows.Forms.TabPage TpgArrival;
        internal DevExpress.XtraEditors.TextEdit TxtArrivalDtTm;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtArrivalChecker;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage TpgBudget;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label label35;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TimeEdit TmeEnd;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TimeEdit TmeStart;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        protected internal DevExpress.XtraEditors.LookUpEdit LueSite;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label LblCityDestination;
        internal DevExpress.XtraEditors.LookUpEdit LueCity;
        internal DevExpress.XtraEditors.LookUpEdit LueCityDestination;
        private DevExpress.XtraEditors.CheckEdit ChkCompleteInd;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        private System.Windows.Forms.Label LblVoucherDocNo;
    }
}