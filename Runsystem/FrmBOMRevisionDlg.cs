﻿#region Update
/*
    21/11/2019 [DITA/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOMRevisionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBOMRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBOMRevisionDlg(FrmBOMRevision FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
        }

        #endregion

        #region Standard Methods

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*");
            SQL.AppendLine("From(");
            SQL.AppendLine("Select A.DocNo, A.DocDt, (A.SubTotal + A.TotalBOM) Amt, IfNull(C.ProjectCode, D.ProjectCode2) ProjectCode, IfNull(C.ProjectName, B.ProjectName) ProjectName ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo = B.DocNo And A.ActInd = 'Y' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.ProcessInd = 'F' And A.Status = 'A' ");
            SQL.AppendLine("Left Join TblProjectGroup C On B.PGCode = C.PGCode ");
            SQL.AppendLine("Left Join TblSOContractHdr D On A.DocNo = D.BOQDocNo ");
            SQL.AppendLine(")T");


            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Amount",
                    "Project Code",
                    "Project Name"

                    
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    140, 100, 100, 100, 200

                }
            );
          
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2,3, 4, 5});
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

       
        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProject.Text, new string[] { "T.ProjectCode", "T.ProjectName" });
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocDt Desc, T.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "Amt", "ProjectCode", "ProjectName"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtBOQDocNo.Text = (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.TxtProjectCode.Text = (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4));
                mFrmParent.TxtProjectName.Text = (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5));
                mFrmParent.ShowBOMData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtProject_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProject_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion



        #endregion

    }
}
