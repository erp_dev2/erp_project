﻿namespace RunSystem
{
    partial class FrmRecvRawMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtVdCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtCreateBy = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.TxtTTCode = new DevExpress.XtraEditors.TextEdit();
            this.DteUnloadEndDt = new DevExpress.XtraEditors.DateEdit();
            this.TmeUnloadStartTm = new DevExpress.XtraEditors.TimeEdit();
            this.DteUnloadStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TmeUnloadEndTm = new DevExpress.XtraEditors.TimeEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtLegalDocVerifyDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LueQueueNo = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueEmpCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEmpCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEmpCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEmpCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LueItCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.LueWhsCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtTotal1 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.LueItCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.LueWhsCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtTotal2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.LueItCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.LueWhsCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtTotal3 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LueItCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.LueWhsCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtTotal4 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.LueItCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.LueWhsCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtTotal5 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.LueItCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.LueWhsCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtTotal6 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.LueItCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.LueWhsCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtTotal7 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.LueItCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label50 = new System.Windows.Forms.Label();
            this.LueWhsCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtTotal8 = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.LueItCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.LueWhsCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtTotal9 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.LueItCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label52 = new System.Windows.Forms.Label();
            this.LueWhsCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf10 = new DevExpress.XtraEditors.LookUpEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtTotal10 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.LueItCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd21 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.LueWhsCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf11 = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtTotal11 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.LueItCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd22 = new TenTec.Windows.iGridLib.iGrid();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.LueWhsCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf12 = new DevExpress.XtraEditors.LookUpEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtTotal12 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.LueItCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd23 = new TenTec.Windows.iGridLib.iGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.LueWhsCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtTotal13 = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.LueItCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd24 = new TenTec.Windows.iGridLib.iGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label56 = new System.Windows.Forms.Label();
            this.LueWhsCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtTotal14 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.LueItCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd25 = new TenTec.Windows.iGridLib.iGrid();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.LueWhsCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtTotal15 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.LueItCode16 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd26 = new TenTec.Windows.iGridLib.iGrid();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label58 = new System.Windows.Forms.Label();
            this.LueWhsCode16 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtTotal16 = new DevExpress.XtraEditors.TextEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.LueItCode17 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd27 = new TenTec.Windows.iGridLib.iGrid();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label61 = new System.Windows.Forms.Label();
            this.LueWhsCode17 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.TxtTotal17 = new DevExpress.XtraEditors.TextEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.LueItCode18 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd28 = new TenTec.Windows.iGridLib.iGrid();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.LueWhsCode18 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf18 = new DevExpress.XtraEditors.LookUpEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtTotal18 = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.LueItCode19 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd29 = new TenTec.Windows.iGridLib.iGrid();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.LueWhsCode19 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf19 = new DevExpress.XtraEditors.LookUpEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.TxtTotal19 = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.LueItCode20 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd30 = new TenTec.Windows.iGridLib.iGrid();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.LueWhsCode20 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf20 = new DevExpress.XtraEditors.LookUpEdit();
            this.label71 = new System.Windows.Forms.Label();
            this.TxtTotal20 = new DevExpress.XtraEditors.TextEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.LueItCode21 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd31 = new TenTec.Windows.iGridLib.iGrid();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.LueWhsCode21 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf21 = new DevExpress.XtraEditors.LookUpEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.TxtTotal21 = new DevExpress.XtraEditors.TextEdit();
            this.label75 = new System.Windows.Forms.Label();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.LueItCode22 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd32 = new TenTec.Windows.iGridLib.iGrid();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label76 = new System.Windows.Forms.Label();
            this.LueWhsCode22 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf22 = new DevExpress.XtraEditors.LookUpEdit();
            this.label77 = new System.Windows.Forms.Label();
            this.TxtTotal22 = new DevExpress.XtraEditors.TextEdit();
            this.label78 = new System.Windows.Forms.Label();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.LueItCode23 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd33 = new TenTec.Windows.iGridLib.iGrid();
            this.panel29 = new System.Windows.Forms.Panel();
            this.label79 = new System.Windows.Forms.Label();
            this.LueWhsCode23 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf23 = new DevExpress.XtraEditors.LookUpEdit();
            this.label80 = new System.Windows.Forms.Label();
            this.TxtTotal23 = new DevExpress.XtraEditors.TextEdit();
            this.label81 = new System.Windows.Forms.Label();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.LueItCode24 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd34 = new TenTec.Windows.iGridLib.iGrid();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label82 = new System.Windows.Forms.Label();
            this.LueWhsCode24 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf24 = new DevExpress.XtraEditors.LookUpEdit();
            this.label83 = new System.Windows.Forms.Label();
            this.TxtTotal24 = new DevExpress.XtraEditors.TextEdit();
            this.label84 = new System.Windows.Forms.Label();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.LueItCode25 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd35 = new TenTec.Windows.iGridLib.iGrid();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label85 = new System.Windows.Forms.Label();
            this.LueWhsCode25 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf25 = new DevExpress.XtraEditors.LookUpEdit();
            this.label86 = new System.Windows.Forms.Label();
            this.TxtTotal25 = new DevExpress.XtraEditors.TextEdit();
            this.label87 = new System.Windows.Forms.Label();
            this.tabPageReturn = new System.Windows.Forms.TabPage();
            this.LueLengthCode = new DevExpress.XtraEditors.LookUpEdit();
            this.GrdReturn = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode2.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreateBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeUnloadStartTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeUnloadEndTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLegalDocVerifyDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQueueNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel7.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal16.Properties)).BeginInit();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd27)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal17.Properties)).BeginInit();
            this.tabPage18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd28)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal18.Properties)).BeginInit();
            this.tabPage19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd29)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal19.Properties)).BeginInit();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd30)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal20.Properties)).BeginInit();
            this.tabPage21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd31)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal21.Properties)).BeginInit();
            this.tabPage22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd32)).BeginInit();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal22.Properties)).BeginInit();
            this.tabPage23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd33)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal23.Properties)).BeginInit();
            this.tabPage24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd34)).BeginInit();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal24.Properties)).BeginInit();
            this.tabPage25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd35)).BeginInit();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal25.Properties)).BeginInit();
            this.tabPageReturn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLengthCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.TxtVdCode2);
            this.panel3.Controls.Add(this.label42);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.TxtVdCode);
            this.panel3.Controls.Add(this.TxtLegalDocVerifyDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.LueQueueNo);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 134);
            this.panel3.TabIndex = 10;
            // 
            // TxtVdCode2
            // 
            this.TxtVdCode2.EditValue = "";
            this.TxtVdCode2.EnterMoveNextControl = true;
            this.TxtVdCode2.Location = new System.Drawing.Point(117, 109);
            this.TxtVdCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode2.Name = "TxtVdCode2";
            this.TxtVdCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode2.Properties.MaxLength = 16;
            this.TxtVdCode2.Size = new System.Drawing.Size(248, 20);
            this.TxtVdCode2.TabIndex = 23;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(22, 112);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(93, 14);
            this.label42.TabIndex = 22;
            this.label42.Text = "Pemilik Legalitas";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtCreateBy);
            this.panel6.Controls.Add(this.label41);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.TxtTotal);
            this.panel6.Controls.Add(this.TxtTTCode);
            this.panel6.Controls.Add(this.DteUnloadEndDt);
            this.panel6.Controls.Add(this.TmeUnloadStartTm);
            this.panel6.Controls.Add(this.DteUnloadStartDt);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.TmeUnloadEndTm);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(369, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(403, 134);
            this.panel6.TabIndex = 24;
            // 
            // TxtCreateBy
            // 
            this.TxtCreateBy.EnterMoveNextControl = true;
            this.TxtCreateBy.Location = new System.Drawing.Point(100, 88);
            this.TxtCreateBy.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreateBy.Name = "TxtCreateBy";
            this.TxtCreateBy.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreateBy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreateBy.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreateBy.Properties.Appearance.Options.UseFont = true;
            this.TxtCreateBy.Properties.MaxLength = 16;
            this.TxtCreateBy.Size = new System.Drawing.Size(297, 20);
            this.TxtCreateBy.TabIndex = 33;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(25, 91);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(71, 14);
            this.label41.TabIndex = 33;
            this.label41.Text = "Dibuat Oleh";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(61, 70);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 14);
            this.label28.TabIndex = 31;
            this.label28.Text = "Total";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EditValue = "";
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(100, 67);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Size = new System.Drawing.Size(167, 20);
            this.TxtTotal.TabIndex = 32;
            // 
            // TxtTTCode
            // 
            this.TxtTTCode.EditValue = "";
            this.TxtTTCode.EnterMoveNextControl = true;
            this.TxtTTCode.Location = new System.Drawing.Point(100, 4);
            this.TxtTTCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTTCode.Name = "TxtTTCode";
            this.TxtTTCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTTCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTTCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTTCode.Properties.MaxLength = 16;
            this.TxtTTCode.Size = new System.Drawing.Size(297, 20);
            this.TxtTTCode.TabIndex = 24;
            // 
            // DteUnloadEndDt
            // 
            this.DteUnloadEndDt.EditValue = null;
            this.DteUnloadEndDt.EnterMoveNextControl = true;
            this.DteUnloadEndDt.Location = new System.Drawing.Point(100, 46);
            this.DteUnloadEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUnloadEndDt.Name = "DteUnloadEndDt";
            this.DteUnloadEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteUnloadEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUnloadEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUnloadEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUnloadEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUnloadEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUnloadEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUnloadEndDt.Size = new System.Drawing.Size(105, 20);
            this.DteUnloadEndDt.TabIndex = 29;
            // 
            // TmeUnloadStartTm
            // 
            this.TmeUnloadStartTm.EditValue = null;
            this.TmeUnloadStartTm.EnterMoveNextControl = true;
            this.TmeUnloadStartTm.Location = new System.Drawing.Point(209, 25);
            this.TmeUnloadStartTm.Name = "TmeUnloadStartTm";
            this.TmeUnloadStartTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeUnloadStartTm.Properties.Appearance.Options.UseFont = true;
            this.TmeUnloadStartTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeUnloadStartTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeUnloadStartTm.Size = new System.Drawing.Size(58, 20);
            this.TmeUnloadStartTm.TabIndex = 27;
            // 
            // DteUnloadStartDt
            // 
            this.DteUnloadStartDt.EditValue = null;
            this.DteUnloadStartDt.EnterMoveNextControl = true;
            this.DteUnloadStartDt.Location = new System.Drawing.Point(100, 25);
            this.DteUnloadStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUnloadStartDt.Name = "DteUnloadStartDt";
            this.DteUnloadStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteUnloadStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUnloadStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUnloadStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUnloadStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUnloadStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUnloadStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUnloadStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUnloadStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUnloadStartDt.Size = new System.Drawing.Size(105, 20);
            this.DteUnloadStartDt.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(23, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Transportasi";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(100, 109);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(297, 20);
            this.MeeRemark.TabIndex = 34;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TmeUnloadEndTm
            // 
            this.TmeUnloadEndTm.EditValue = null;
            this.TmeUnloadEndTm.EnterMoveNextControl = true;
            this.TmeUnloadEndTm.Location = new System.Drawing.Point(209, 46);
            this.TmeUnloadEndTm.Name = "TmeUnloadEndTm";
            this.TmeUnloadEndTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeUnloadEndTm.Properties.Appearance.Options.UseFont = true;
            this.TmeUnloadEndTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeUnloadEndTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeUnloadEndTm.Size = new System.Drawing.Size(58, 20);
            this.TmeUnloadEndTm.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(5, 49);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "Selesai Bongkar";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 14);
            this.label5.TabIndex = 34;
            this.label5.Text = "Keterangan";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(15, 28);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Mulai Bongkar";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EditValue = "";
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(117, 88);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 16;
            this.TxtVdCode.Size = new System.Drawing.Size(248, 20);
            this.TxtVdCode.TabIndex = 21;
            // 
            // TxtLegalDocVerifyDocNo
            // 
            this.TxtLegalDocVerifyDocNo.EnterMoveNextControl = true;
            this.TxtLegalDocVerifyDocNo.Location = new System.Drawing.Point(117, 67);
            this.TxtLegalDocVerifyDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLegalDocVerifyDocNo.Name = "TxtLegalDocVerifyDocNo";
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLegalDocVerifyDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLegalDocVerifyDocNo.Properties.MaxLength = 16;
            this.TxtLegalDocVerifyDocNo.Size = new System.Drawing.Size(248, 20);
            this.TxtLegalDocVerifyDocNo.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 14);
            this.label1.TabIndex = 18;
            this.label1.Text = "Dokumen Verifikasi";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(315, 3);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Batal";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(55, 22);
            this.ChkCancelInd.TabIndex = 13;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(117, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(126, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(31, 28);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "Tgl. Dokumen";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(117, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(196, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(33, 6);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 14);
            this.label18.TabIndex = 11;
            this.label18.Text = "No. Dokumen";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(68, 91);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 20;
            this.label2.Text = "Vendor";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueQueueNo
            // 
            this.LueQueueNo.EnterMoveNextControl = true;
            this.LueQueueNo.Location = new System.Drawing.Point(117, 46);
            this.LueQueueNo.Margin = new System.Windows.Forms.Padding(5);
            this.LueQueueNo.Name = "LueQueueNo";
            this.LueQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.Appearance.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueQueueNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueQueueNo.Properties.DropDownRows = 20;
            this.LueQueueNo.Properties.NullText = "[Empty]";
            this.LueQueueNo.Properties.PopupWidth = 300;
            this.LueQueueNo.Size = new System.Drawing.Size(248, 20);
            this.LueQueueNo.TabIndex = 17;
            this.LueQueueNo.ToolTip = "F4 : Show/hide list";
            this.LueQueueNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueQueueNo.EditValueChanged += new System.EventHandler(this.LueQueueNo_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(29, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Nomor Antrian";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.LueEmpCode4);
            this.panel4.Controls.Add(this.LueEmpCode3);
            this.panel4.Controls.Add(this.LueEmpCode2);
            this.panel4.Controls.Add(this.LueEmpCode1);
            this.panel4.Controls.Add(this.Grd4);
            this.panel4.Controls.Add(this.Grd3);
            this.panel4.Controls.Add(this.Grd2);
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 134);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 76);
            this.panel4.TabIndex = 16;
            // 
            // LueEmpCode4
            // 
            this.LueEmpCode4.EnterMoveNextControl = true;
            this.LueEmpCode4.Location = new System.Drawing.Point(598, 22);
            this.LueEmpCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode4.Name = "LueEmpCode4";
            this.LueEmpCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode4.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode4.Properties.DropDownRows = 20;
            this.LueEmpCode4.Properties.NullText = "[Empty]";
            this.LueEmpCode4.Properties.PopupWidth = 500;
            this.LueEmpCode4.Size = new System.Drawing.Size(152, 20);
            this.LueEmpCode4.TabIndex = 42;
            this.LueEmpCode4.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode4.EditValueChanged += new System.EventHandler(this.LueEmpCode4_EditValueChanged);
            this.LueEmpCode4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmpCode4_KeyDown);
            this.LueEmpCode4.Leave += new System.EventHandler(this.LueEmpCode4_Leave);
            // 
            // LueEmpCode3
            // 
            this.LueEmpCode3.EnterMoveNextControl = true;
            this.LueEmpCode3.Location = new System.Drawing.Point(406, 22);
            this.LueEmpCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode3.Name = "LueEmpCode3";
            this.LueEmpCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode3.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode3.Properties.DropDownRows = 20;
            this.LueEmpCode3.Properties.NullText = "[Empty]";
            this.LueEmpCode3.Properties.PopupWidth = 500;
            this.LueEmpCode3.Size = new System.Drawing.Size(152, 20);
            this.LueEmpCode3.TabIndex = 40;
            this.LueEmpCode3.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode3.EditValueChanged += new System.EventHandler(this.LueEmpCode3_EditValueChanged);
            this.LueEmpCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmpCode3_KeyDown);
            this.LueEmpCode3.Leave += new System.EventHandler(this.LueEmpCode3_Leave);
            // 
            // LueEmpCode2
            // 
            this.LueEmpCode2.EnterMoveNextControl = true;
            this.LueEmpCode2.Location = new System.Drawing.Point(214, 22);
            this.LueEmpCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode2.Name = "LueEmpCode2";
            this.LueEmpCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode2.Properties.DropDownRows = 20;
            this.LueEmpCode2.Properties.NullText = "[Empty]";
            this.LueEmpCode2.Properties.PopupWidth = 500;
            this.LueEmpCode2.Size = new System.Drawing.Size(152, 20);
            this.LueEmpCode2.TabIndex = 38;
            this.LueEmpCode2.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode2.EditValueChanged += new System.EventHandler(this.LueEmpCode2_EditValueChanged);
            this.LueEmpCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmpCode2_KeyDown);
            this.LueEmpCode2.Leave += new System.EventHandler(this.LueEmpCode2_Leave);
            // 
            // LueEmpCode1
            // 
            this.LueEmpCode1.EnterMoveNextControl = true;
            this.LueEmpCode1.Location = new System.Drawing.Point(20, 22);
            this.LueEmpCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode1.Name = "LueEmpCode1";
            this.LueEmpCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode1.Properties.DropDownRows = 20;
            this.LueEmpCode1.Properties.NullText = "[Empty]";
            this.LueEmpCode1.Properties.PopupWidth = 500;
            this.LueEmpCode1.Size = new System.Drawing.Size(152, 20);
            this.LueEmpCode1.TabIndex = 36;
            this.LueEmpCode1.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode1.EditValueChanged += new System.EventHandler(this.LueEmpCode1_EditValueChanged);
            this.LueEmpCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEmpCode1_KeyDown);
            this.LueEmpCode1.Leave += new System.EventHandler(this.LueEmpCode1_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(579, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(193, 76);
            this.Grd4.TabIndex = 41;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Left;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(386, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(193, 76);
            this.Grd3.TabIndex = 39;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Left;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(193, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(193, 76);
            this.Grd2.TabIndex = 37;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Left;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(193, 76);
            this.Grd1.TabIndex = 35;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.tabControl1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 210);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(772, 263);
            this.panel7.TabIndex = 55;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Controls.Add(this.tabPage17);
            this.tabControl1.Controls.Add(this.tabPage18);
            this.tabControl1.Controls.Add(this.tabPage19);
            this.tabControl1.Controls.Add(this.tabPage20);
            this.tabControl1.Controls.Add(this.tabPage21);
            this.tabControl1.Controls.Add(this.tabPage22);
            this.tabControl1.Controls.Add(this.tabPage23);
            this.tabControl1.Controls.Add(this.tabPage24);
            this.tabControl1.Controls.Add(this.tabPage25);
            this.tabControl1.Controls.Add(this.tabPageReturn);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(772, 263);
            this.tabControl1.TabIndex = 43;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LueItCode1);
            this.tabPage1.Controls.Add(this.Grd11);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(764, 236);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "No.1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LueItCode1
            // 
            this.LueItCode1.EnterMoveNextControl = true;
            this.LueItCode1.Location = new System.Drawing.Point(22, 55);
            this.LueItCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode1.Name = "LueItCode1";
            this.LueItCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.Appearance.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode1.Properties.DropDownRows = 20;
            this.LueItCode1.Properties.NullText = "[Empty]";
            this.LueItCode1.Properties.PopupWidth = 500;
            this.LueItCode1.Size = new System.Drawing.Size(152, 20);
            this.LueItCode1.TabIndex = 52;
            this.LueItCode1.ToolTip = "F4 : Show/hide list";
            this.LueItCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode1.EditValueChanged += new System.EventHandler(this.LueItCode1_EditValueChanged);
            this.LueItCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode1_KeyDown);
            this.LueItCode1.Leave += new System.EventHandler(this.LueItCode1_Leave);
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(3, 33);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(758, 200);
            this.Grd11.TabIndex = 51;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd11_ColHdrDoubleClick);
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd11_AfterCommitEdit);
            this.Grd11.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd11_CustomDrawCellForeground);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.label43);
            this.panel8.Controls.Add(this.LueWhsCode1);
            this.panel8.Controls.Add(this.LueShelf1);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.TxtTotal1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(758, 30);
            this.panel8.TabIndex = 44;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(424, 7);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(69, 14);
            this.label43.TabIndex = 49;
            this.label43.Text = "Warehouse";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode1
            // 
            this.LueWhsCode1.EnterMoveNextControl = true;
            this.LueWhsCode1.Location = new System.Drawing.Point(498, 4);
            this.LueWhsCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode1.Name = "LueWhsCode1";
            this.LueWhsCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode1.Properties.DropDownRows = 20;
            this.LueWhsCode1.Properties.NullText = "[Empty]";
            this.LueWhsCode1.Properties.PopupWidth = 500;
            this.LueWhsCode1.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode1.TabIndex = 50;
            this.LueWhsCode1.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode1.EditValueChanged += new System.EventHandler(this.LueWhsCode1_EditValueChanged);
            // 
            // LueShelf1
            // 
            this.LueShelf1.EnterMoveNextControl = true;
            this.LueShelf1.Location = new System.Drawing.Point(41, 5);
            this.LueShelf1.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf1.Name = "LueShelf1";
            this.LueShelf1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.Appearance.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf1.Properties.DropDownRows = 20;
            this.LueShelf1.Properties.NullText = "[Empty]";
            this.LueShelf1.Properties.PopupWidth = 500;
            this.LueShelf1.Size = new System.Drawing.Size(245, 20);
            this.LueShelf1.TabIndex = 51;
            this.LueShelf1.ToolTip = "F4 : Show/hide list";
            this.LueShelf1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf1.EditValueChanged += new System.EventHandler(this.LueShelf1_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(291, 7);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 47;
            this.label8.Text = "Total";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal1
            // 
            this.TxtTotal1.EditValue = "";
            this.TxtTotal1.EnterMoveNextControl = true;
            this.TxtTotal1.Location = new System.Drawing.Point(328, 4);
            this.TxtTotal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal1.Name = "TxtTotal1";
            this.TxtTotal1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal1.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal1.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(13, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 14);
            this.label11.TabIndex = 45;
            this.label11.Text = "Rak";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.LueItCode2);
            this.tabPage2.Controls.Add(this.Grd12);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 0);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "No.2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // LueItCode2
            // 
            this.LueItCode2.EnterMoveNextControl = true;
            this.LueItCode2.Location = new System.Drawing.Point(24, 56);
            this.LueItCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode2.Name = "LueItCode2";
            this.LueItCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.Appearance.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode2.Properties.DropDownRows = 20;
            this.LueItCode2.Properties.NullText = "[Empty]";
            this.LueItCode2.Properties.PopupWidth = 500;
            this.LueItCode2.Size = new System.Drawing.Size(163, 20);
            this.LueItCode2.TabIndex = 52;
            this.LueItCode2.ToolTip = "F4 : Show/hide list";
            this.LueItCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode2.EditValueChanged += new System.EventHandler(this.LueItCode2_EditValueChanged);
            this.LueItCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode2_KeyDown);
            this.LueItCode2.Leave += new System.EventHandler(this.LueItCode2_Leave);
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(3, 35);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(758, 0);
            this.Grd12.TabIndex = 51;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd12_ColHdrDoubleClick);
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd12_AfterCommitEdit);
            this.Grd12.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd12_CustomDrawCellForeground);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.label44);
            this.panel9.Controls.Add(this.LueWhsCode2);
            this.panel9.Controls.Add(this.LueShelf2);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.TxtTotal2);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(758, 32);
            this.panel9.TabIndex = 44;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(422, 9);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(69, 14);
            this.label44.TabIndex = 49;
            this.label44.Text = "Warehouse";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode2
            // 
            this.LueWhsCode2.EnterMoveNextControl = true;
            this.LueWhsCode2.Location = new System.Drawing.Point(496, 6);
            this.LueWhsCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode2.Name = "LueWhsCode2";
            this.LueWhsCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode2.Properties.DropDownRows = 20;
            this.LueWhsCode2.Properties.NullText = "[Empty]";
            this.LueWhsCode2.Properties.PopupWidth = 500;
            this.LueWhsCode2.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode2.TabIndex = 50;
            this.LueWhsCode2.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode2.EditValueChanged += new System.EventHandler(this.LueWhsCode2_EditValueChanged);
            // 
            // LueShelf2
            // 
            this.LueShelf2.EnterMoveNextControl = true;
            this.LueShelf2.Location = new System.Drawing.Point(39, 6);
            this.LueShelf2.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf2.Name = "LueShelf2";
            this.LueShelf2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.Appearance.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf2.Properties.DropDownRows = 20;
            this.LueShelf2.Properties.NullText = "[Empty]";
            this.LueShelf2.Properties.PopupWidth = 500;
            this.LueShelf2.Size = new System.Drawing.Size(245, 20);
            this.LueShelf2.TabIndex = 46;
            this.LueShelf2.ToolTip = "F4 : Show/hide list";
            this.LueShelf2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf2.EditValueChanged += new System.EventHandler(this.LueShelf2_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(288, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 47;
            this.label9.Text = "Total";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal2
            // 
            this.TxtTotal2.EnterMoveNextControl = true;
            this.TxtTotal2.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2.Name = "TxtTotal2";
            this.TxtTotal2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal2.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal2.TabIndex = 48;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(9, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 14);
            this.label13.TabIndex = 45;
            this.label13.Text = "Rak";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.LueItCode3);
            this.tabPage3.Controls.Add(this.Grd13);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 0);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "No.3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // LueItCode3
            // 
            this.LueItCode3.EnterMoveNextControl = true;
            this.LueItCode3.Location = new System.Drawing.Point(21, 55);
            this.LueItCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode3.Name = "LueItCode3";
            this.LueItCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.Appearance.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode3.Properties.DropDownRows = 20;
            this.LueItCode3.Properties.NullText = "[Empty]";
            this.LueItCode3.Properties.PopupWidth = 500;
            this.LueItCode3.Size = new System.Drawing.Size(163, 20);
            this.LueItCode3.TabIndex = 52;
            this.LueItCode3.ToolTip = "F4 : Show/hide list";
            this.LueItCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode3.EditValueChanged += new System.EventHandler(this.LueItCode3_EditValueChanged);
            this.LueItCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode3_KeyDown);
            this.LueItCode3.Leave += new System.EventHandler(this.LueItCode3_Leave);
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 34);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(764, 0);
            this.Grd13.TabIndex = 51;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd13_ColHdrDoubleClick);
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd13_AfterCommitEdit);
            this.Grd13.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd13_CustomDrawCellForeground);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.label45);
            this.panel10.Controls.Add(this.LueWhsCode3);
            this.panel10.Controls.Add(this.LueShelf3);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.TxtTotal3);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(764, 34);
            this.panel10.TabIndex = 44;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(420, 9);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(69, 14);
            this.label45.TabIndex = 49;
            this.label45.Text = "Warehouse";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode3
            // 
            this.LueWhsCode3.EnterMoveNextControl = true;
            this.LueWhsCode3.Location = new System.Drawing.Point(494, 6);
            this.LueWhsCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode3.Name = "LueWhsCode3";
            this.LueWhsCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode3.Properties.DropDownRows = 20;
            this.LueWhsCode3.Properties.NullText = "[Empty]";
            this.LueWhsCode3.Properties.PopupWidth = 500;
            this.LueWhsCode3.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode3.TabIndex = 50;
            this.LueWhsCode3.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode3.EditValueChanged += new System.EventHandler(this.LueWhsCode3_EditValueChanged);
            // 
            // LueShelf3
            // 
            this.LueShelf3.EnterMoveNextControl = true;
            this.LueShelf3.Location = new System.Drawing.Point(34, 6);
            this.LueShelf3.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf3.Name = "LueShelf3";
            this.LueShelf3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.Appearance.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf3.Properties.DropDownRows = 20;
            this.LueShelf3.Properties.NullText = "[Empty]";
            this.LueShelf3.Properties.PopupWidth = 500;
            this.LueShelf3.Size = new System.Drawing.Size(245, 20);
            this.LueShelf3.TabIndex = 46;
            this.LueShelf3.ToolTip = "F4 : Show/hide list";
            this.LueShelf3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf3.EditValueChanged += new System.EventHandler(this.LueShelf3_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(284, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 14);
            this.label10.TabIndex = 47;
            this.label10.Text = "Total";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal3
            // 
            this.TxtTotal3.EnterMoveNextControl = true;
            this.TxtTotal3.Location = new System.Drawing.Point(321, 6);
            this.TxtTotal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3.Name = "TxtTotal3";
            this.TxtTotal3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal3.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal3.TabIndex = 48;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(8, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Rak";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LueItCode4);
            this.tabPage4.Controls.Add(this.Grd14);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(764, 0);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "No.4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LueItCode4
            // 
            this.LueItCode4.EnterMoveNextControl = true;
            this.LueItCode4.Location = new System.Drawing.Point(19, 54);
            this.LueItCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode4.Name = "LueItCode4";
            this.LueItCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.Appearance.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode4.Properties.DropDownRows = 20;
            this.LueItCode4.Properties.NullText = "[Empty]";
            this.LueItCode4.Properties.PopupWidth = 500;
            this.LueItCode4.Size = new System.Drawing.Size(163, 20);
            this.LueItCode4.TabIndex = 52;
            this.LueItCode4.ToolTip = "F4 : Show/hide list";
            this.LueItCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode4.EditValueChanged += new System.EventHandler(this.LueItCode4_EditValueChanged);
            this.LueItCode4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode4_KeyDown);
            this.LueItCode4.Leave += new System.EventHandler(this.LueItCode4_Leave);
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 32);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(764, 0);
            this.Grd14.TabIndex = 51;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd14_ColHdrDoubleClick);
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd14_AfterCommitEdit);
            this.Grd14.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd14_CustomDrawCellForeground);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.label46);
            this.panel11.Controls.Add(this.LueWhsCode4);
            this.panel11.Controls.Add(this.LueShelf4);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.TxtTotal4);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(764, 32);
            this.panel11.TabIndex = 44;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(418, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(69, 14);
            this.label46.TabIndex = 49;
            this.label46.Text = "Warehouse";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode4
            // 
            this.LueWhsCode4.EnterMoveNextControl = true;
            this.LueWhsCode4.Location = new System.Drawing.Point(492, 6);
            this.LueWhsCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode4.Name = "LueWhsCode4";
            this.LueWhsCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode4.Properties.DropDownRows = 20;
            this.LueWhsCode4.Properties.NullText = "[Empty]";
            this.LueWhsCode4.Properties.PopupWidth = 500;
            this.LueWhsCode4.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode4.TabIndex = 50;
            this.LueWhsCode4.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode4.EditValueChanged += new System.EventHandler(this.LueWhsCode4_EditValueChanged);
            // 
            // LueShelf4
            // 
            this.LueShelf4.EnterMoveNextControl = true;
            this.LueShelf4.Location = new System.Drawing.Point(35, 6);
            this.LueShelf4.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf4.Name = "LueShelf4";
            this.LueShelf4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.Appearance.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf4.Properties.DropDownRows = 20;
            this.LueShelf4.Properties.NullText = "[Empty]";
            this.LueShelf4.Properties.PopupWidth = 500;
            this.LueShelf4.Size = new System.Drawing.Size(245, 20);
            this.LueShelf4.TabIndex = 46;
            this.LueShelf4.ToolTip = "F4 : Show/hide list";
            this.LueShelf4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf4.EditValueChanged += new System.EventHandler(this.LueShelf4_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(284, 9);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 14);
            this.label12.TabIndex = 47;
            this.label12.Text = "Total";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal4
            // 
            this.TxtTotal4.EnterMoveNextControl = true;
            this.TxtTotal4.Location = new System.Drawing.Point(321, 6);
            this.TxtTotal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal4.Name = "TxtTotal4";
            this.TxtTotal4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal4.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal4.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal4.TabIndex = 48;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(8, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 14);
            this.label19.TabIndex = 45;
            this.label19.Text = "Rak";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.LueItCode5);
            this.tabPage5.Controls.Add(this.Grd15);
            this.tabPage5.Controls.Add(this.panel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(764, 0);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "No.5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // LueItCode5
            // 
            this.LueItCode5.EnterMoveNextControl = true;
            this.LueItCode5.Location = new System.Drawing.Point(21, 54);
            this.LueItCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode5.Name = "LueItCode5";
            this.LueItCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.Appearance.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode5.Properties.DropDownRows = 20;
            this.LueItCode5.Properties.NullText = "[Empty]";
            this.LueItCode5.Properties.PopupWidth = 500;
            this.LueItCode5.Size = new System.Drawing.Size(163, 20);
            this.LueItCode5.TabIndex = 52;
            this.LueItCode5.ToolTip = "F4 : Show/hide list";
            this.LueItCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode5.EditValueChanged += new System.EventHandler(this.LueItCode5_EditValueChanged);
            this.LueItCode5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode5_KeyDown);
            this.LueItCode5.Leave += new System.EventHandler(this.LueItCode5_Leave);
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 32);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(764, 0);
            this.Grd15.TabIndex = 51;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd15_ColHdrDoubleClick);
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd15_AfterCommitEdit);
            this.Grd15.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd15_CustomDrawCellForeground);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15_KeyDown);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.label47);
            this.panel12.Controls.Add(this.LueWhsCode5);
            this.panel12.Controls.Add(this.LueShelf5);
            this.panel12.Controls.Add(this.label14);
            this.panel12.Controls.Add(this.TxtTotal5);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(764, 32);
            this.panel12.TabIndex = 44;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(418, 9);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(69, 14);
            this.label47.TabIndex = 49;
            this.label47.Text = "Warehouse";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode5
            // 
            this.LueWhsCode5.EnterMoveNextControl = true;
            this.LueWhsCode5.Location = new System.Drawing.Point(492, 6);
            this.LueWhsCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode5.Name = "LueWhsCode5";
            this.LueWhsCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode5.Properties.DropDownRows = 20;
            this.LueWhsCode5.Properties.NullText = "[Empty]";
            this.LueWhsCode5.Properties.PopupWidth = 500;
            this.LueWhsCode5.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode5.TabIndex = 50;
            this.LueWhsCode5.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode5.EditValueChanged += new System.EventHandler(this.LueWhsCode5_EditValueChanged);
            // 
            // LueShelf5
            // 
            this.LueShelf5.EnterMoveNextControl = true;
            this.LueShelf5.Location = new System.Drawing.Point(34, 6);
            this.LueShelf5.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf5.Name = "LueShelf5";
            this.LueShelf5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.Appearance.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf5.Properties.DropDownRows = 20;
            this.LueShelf5.Properties.NullText = "[Empty]";
            this.LueShelf5.Properties.PopupWidth = 500;
            this.LueShelf5.Size = new System.Drawing.Size(245, 20);
            this.LueShelf5.TabIndex = 46;
            this.LueShelf5.ToolTip = "F4 : Show/hide list";
            this.LueShelf5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf5.EditValueChanged += new System.EventHandler(this.LueShelf5_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(284, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 14);
            this.label14.TabIndex = 47;
            this.label14.Text = "Total";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal5
            // 
            this.TxtTotal5.EnterMoveNextControl = true;
            this.TxtTotal5.Location = new System.Drawing.Point(321, 6);
            this.TxtTotal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal5.Name = "TxtTotal5";
            this.TxtTotal5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal5.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal5.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal5.TabIndex = 48;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(8, 9);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(26, 14);
            this.label21.TabIndex = 45;
            this.label21.Text = "Rak";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.LueItCode6);
            this.tabPage6.Controls.Add(this.Grd16);
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(764, 0);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "No.6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // LueItCode6
            // 
            this.LueItCode6.EnterMoveNextControl = true;
            this.LueItCode6.Location = new System.Drawing.Point(21, 53);
            this.LueItCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode6.Name = "LueItCode6";
            this.LueItCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.Appearance.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode6.Properties.DropDownRows = 20;
            this.LueItCode6.Properties.NullText = "[Empty]";
            this.LueItCode6.Properties.PopupWidth = 500;
            this.LueItCode6.Size = new System.Drawing.Size(163, 20);
            this.LueItCode6.TabIndex = 52;
            this.LueItCode6.ToolTip = "F4 : Show/hide list";
            this.LueItCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode6.EditValueChanged += new System.EventHandler(this.LueItCode6_EditValueChanged);
            this.LueItCode6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode6_KeyDown);
            this.LueItCode6.Leave += new System.EventHandler(this.LueItCode6_Leave);
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(0, 33);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(764, 0);
            this.Grd16.TabIndex = 51;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd16_ColHdrDoubleClick);
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd16_AfterCommitEdit);
            this.Grd16.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd16_CustomDrawCellForeground);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.label48);
            this.panel13.Controls.Add(this.LueWhsCode6);
            this.panel13.Controls.Add(this.LueShelf6);
            this.panel13.Controls.Add(this.label16);
            this.panel13.Controls.Add(this.TxtTotal6);
            this.panel13.Controls.Add(this.label23);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(764, 33);
            this.panel13.TabIndex = 44;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Red;
            this.label48.Location = new System.Drawing.Point(428, 9);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(69, 14);
            this.label48.TabIndex = 49;
            this.label48.Text = "Warehouse";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode6
            // 
            this.LueWhsCode6.EnterMoveNextControl = true;
            this.LueWhsCode6.Location = new System.Drawing.Point(502, 6);
            this.LueWhsCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode6.Name = "LueWhsCode6";
            this.LueWhsCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode6.Properties.DropDownRows = 20;
            this.LueWhsCode6.Properties.NullText = "[Empty]";
            this.LueWhsCode6.Properties.PopupWidth = 500;
            this.LueWhsCode6.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode6.TabIndex = 50;
            this.LueWhsCode6.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode6.EditValueChanged += new System.EventHandler(this.LueWhsCode6_EditValueChanged);
            // 
            // LueShelf6
            // 
            this.LueShelf6.EnterMoveNextControl = true;
            this.LueShelf6.Location = new System.Drawing.Point(35, 6);
            this.LueShelf6.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf6.Name = "LueShelf6";
            this.LueShelf6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.Appearance.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf6.Properties.DropDownRows = 20;
            this.LueShelf6.Properties.NullText = "[Empty]";
            this.LueShelf6.Properties.PopupWidth = 500;
            this.LueShelf6.Size = new System.Drawing.Size(245, 20);
            this.LueShelf6.TabIndex = 46;
            this.LueShelf6.ToolTip = "F4 : Show/hide list";
            this.LueShelf6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf6.EditValueChanged += new System.EventHandler(this.LueShelf6_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(285, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 14);
            this.label16.TabIndex = 47;
            this.label16.Text = "Total";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal6
            // 
            this.TxtTotal6.EnterMoveNextControl = true;
            this.TxtTotal6.Location = new System.Drawing.Point(322, 6);
            this.TxtTotal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal6.Name = "TxtTotal6";
            this.TxtTotal6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal6.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal6.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal6.TabIndex = 48;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(8, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(26, 14);
            this.label23.TabIndex = 45;
            this.label23.Text = "Rak";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.LueItCode7);
            this.tabPage7.Controls.Add(this.Grd17);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(764, 0);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "No.7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // LueItCode7
            // 
            this.LueItCode7.EnterMoveNextControl = true;
            this.LueItCode7.Location = new System.Drawing.Point(19, 54);
            this.LueItCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode7.Name = "LueItCode7";
            this.LueItCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.Appearance.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode7.Properties.DropDownRows = 20;
            this.LueItCode7.Properties.NullText = "[Empty]";
            this.LueItCode7.Properties.PopupWidth = 500;
            this.LueItCode7.Size = new System.Drawing.Size(163, 20);
            this.LueItCode7.TabIndex = 52;
            this.LueItCode7.ToolTip = "F4 : Show/hide list";
            this.LueItCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode7.EditValueChanged += new System.EventHandler(this.LueItCode7_EditValueChanged);
            this.LueItCode7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode7_KeyDown);
            this.LueItCode7.Leave += new System.EventHandler(this.LueItCode7_Leave);
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(0, 32);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(764, 0);
            this.Grd17.TabIndex = 51;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd17_ColHdrDoubleClick);
            this.Grd17.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd17_RequestEdit);
            this.Grd17.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd17_AfterCommitEdit);
            this.Grd17.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd17_CustomDrawCellForeground);
            this.Grd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd17_KeyDown);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.label49);
            this.panel14.Controls.Add(this.LueWhsCode7);
            this.panel14.Controls.Add(this.LueShelf7);
            this.panel14.Controls.Add(this.label20);
            this.panel14.Controls.Add(this.TxtTotal7);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(764, 32);
            this.panel14.TabIndex = 44;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Red;
            this.label49.Location = new System.Drawing.Point(420, 9);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(69, 14);
            this.label49.TabIndex = 49;
            this.label49.Text = "Warehouse";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode7
            // 
            this.LueWhsCode7.EnterMoveNextControl = true;
            this.LueWhsCode7.Location = new System.Drawing.Point(494, 6);
            this.LueWhsCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode7.Name = "LueWhsCode7";
            this.LueWhsCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode7.Properties.DropDownRows = 20;
            this.LueWhsCode7.Properties.NullText = "[Empty]";
            this.LueWhsCode7.Properties.PopupWidth = 500;
            this.LueWhsCode7.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode7.TabIndex = 50;
            this.LueWhsCode7.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode7.EditValueChanged += new System.EventHandler(this.LueWhsCode7_EditValueChanged);
            // 
            // LueShelf7
            // 
            this.LueShelf7.EnterMoveNextControl = true;
            this.LueShelf7.Location = new System.Drawing.Point(36, 6);
            this.LueShelf7.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf7.Name = "LueShelf7";
            this.LueShelf7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.Appearance.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf7.Properties.DropDownRows = 20;
            this.LueShelf7.Properties.NullText = "[Empty]";
            this.LueShelf7.Properties.PopupWidth = 500;
            this.LueShelf7.Size = new System.Drawing.Size(245, 20);
            this.LueShelf7.TabIndex = 46;
            this.LueShelf7.ToolTip = "F4 : Show/hide list";
            this.LueShelf7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf7.EditValueChanged += new System.EventHandler(this.LueShelf7_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(285, 9);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 14);
            this.label20.TabIndex = 47;
            this.label20.Text = "Total";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal7
            // 
            this.TxtTotal7.EnterMoveNextControl = true;
            this.TxtTotal7.Location = new System.Drawing.Point(322, 6);
            this.TxtTotal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal7.Name = "TxtTotal7";
            this.TxtTotal7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal7.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal7.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal7.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal7.TabIndex = 48;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(8, 9);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(26, 14);
            this.label25.TabIndex = 45;
            this.label25.Text = "Rak";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.LueItCode8);
            this.tabPage8.Controls.Add(this.Grd18);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 23);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(764, 0);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "No.8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // LueItCode8
            // 
            this.LueItCode8.EnterMoveNextControl = true;
            this.LueItCode8.Location = new System.Drawing.Point(20, 54);
            this.LueItCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode8.Name = "LueItCode8";
            this.LueItCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.Appearance.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode8.Properties.DropDownRows = 20;
            this.LueItCode8.Properties.NullText = "[Empty]";
            this.LueItCode8.Properties.PopupWidth = 500;
            this.LueItCode8.Size = new System.Drawing.Size(163, 20);
            this.LueItCode8.TabIndex = 52;
            this.LueItCode8.ToolTip = "F4 : Show/hide list";
            this.LueItCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode8.EditValueChanged += new System.EventHandler(this.LueItCode8_EditValueChanged);
            this.LueItCode8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode8_KeyDown);
            this.LueItCode8.Leave += new System.EventHandler(this.LueItCode8_Leave);
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 21;
            this.Grd18.Location = new System.Drawing.Point(0, 32);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(764, 0);
            this.Grd18.TabIndex = 51;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd18.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd18_ColHdrDoubleClick);
            this.Grd18.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd18_RequestEdit);
            this.Grd18.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd18_AfterCommitEdit);
            this.Grd18.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd18_CustomDrawCellForeground);
            this.Grd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd18_KeyDown);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel15.Controls.Add(this.label50);
            this.panel15.Controls.Add(this.LueWhsCode8);
            this.panel15.Controls.Add(this.LueShelf8);
            this.panel15.Controls.Add(this.label22);
            this.panel15.Controls.Add(this.TxtTotal8);
            this.panel15.Controls.Add(this.label27);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(764, 32);
            this.panel15.TabIndex = 44;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(424, 9);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(69, 14);
            this.label50.TabIndex = 49;
            this.label50.Text = "Warehouse";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode8
            // 
            this.LueWhsCode8.EnterMoveNextControl = true;
            this.LueWhsCode8.Location = new System.Drawing.Point(498, 6);
            this.LueWhsCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode8.Name = "LueWhsCode8";
            this.LueWhsCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode8.Properties.DropDownRows = 20;
            this.LueWhsCode8.Properties.NullText = "[Empty]";
            this.LueWhsCode8.Properties.PopupWidth = 500;
            this.LueWhsCode8.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode8.TabIndex = 50;
            this.LueWhsCode8.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode8.EditValueChanged += new System.EventHandler(this.LueWhsCode8_EditValueChanged);
            // 
            // LueShelf8
            // 
            this.LueShelf8.EnterMoveNextControl = true;
            this.LueShelf8.Location = new System.Drawing.Point(37, 6);
            this.LueShelf8.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf8.Name = "LueShelf8";
            this.LueShelf8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.Appearance.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf8.Properties.DropDownRows = 20;
            this.LueShelf8.Properties.NullText = "[Empty]";
            this.LueShelf8.Properties.PopupWidth = 500;
            this.LueShelf8.Size = new System.Drawing.Size(245, 20);
            this.LueShelf8.TabIndex = 46;
            this.LueShelf8.ToolTip = "F4 : Show/hide list";
            this.LueShelf8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf8.EditValueChanged += new System.EventHandler(this.LueShelf8_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(288, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 14);
            this.label22.TabIndex = 47;
            this.label22.Text = "Total";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal8
            // 
            this.TxtTotal8.EnterMoveNextControl = true;
            this.TxtTotal8.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal8.Name = "TxtTotal8";
            this.TxtTotal8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal8.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal8.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal8.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal8.TabIndex = 48;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(8, 9);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(26, 14);
            this.label27.TabIndex = 45;
            this.label27.Text = "Rak";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.LueItCode9);
            this.tabPage9.Controls.Add(this.Grd19);
            this.tabPage9.Controls.Add(this.panel16);
            this.tabPage9.Location = new System.Drawing.Point(4, 23);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(764, 0);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "No.9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // LueItCode9
            // 
            this.LueItCode9.EnterMoveNextControl = true;
            this.LueItCode9.Location = new System.Drawing.Point(21, 54);
            this.LueItCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode9.Name = "LueItCode9";
            this.LueItCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.Appearance.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode9.Properties.DropDownRows = 20;
            this.LueItCode9.Properties.NullText = "[Empty]";
            this.LueItCode9.Properties.PopupWidth = 500;
            this.LueItCode9.Size = new System.Drawing.Size(163, 20);
            this.LueItCode9.TabIndex = 52;
            this.LueItCode9.ToolTip = "F4 : Show/hide list";
            this.LueItCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode9.EditValueChanged += new System.EventHandler(this.LueItCode9_EditValueChanged);
            this.LueItCode9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode9_KeyDown);
            this.LueItCode9.Leave += new System.EventHandler(this.LueItCode9_Leave);
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 21;
            this.Grd19.Location = new System.Drawing.Point(0, 32);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(764, 0);
            this.Grd19.TabIndex = 51;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd19.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd19_ColHdrDoubleClick);
            this.Grd19.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd19_RequestEdit);
            this.Grd19.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd19_AfterCommitEdit);
            this.Grd19.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd19_CustomDrawCellForeground);
            this.Grd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd19_KeyDown);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel16.Controls.Add(this.label51);
            this.panel16.Controls.Add(this.LueWhsCode9);
            this.panel16.Controls.Add(this.LueShelf9);
            this.panel16.Controls.Add(this.label24);
            this.panel16.Controls.Add(this.TxtTotal9);
            this.panel16.Controls.Add(this.label29);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(764, 32);
            this.panel16.TabIndex = 44;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Red;
            this.label51.Location = new System.Drawing.Point(418, 9);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(69, 14);
            this.label51.TabIndex = 49;
            this.label51.Text = "Warehouse";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode9
            // 
            this.LueWhsCode9.EnterMoveNextControl = true;
            this.LueWhsCode9.Location = new System.Drawing.Point(492, 6);
            this.LueWhsCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode9.Name = "LueWhsCode9";
            this.LueWhsCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode9.Properties.DropDownRows = 20;
            this.LueWhsCode9.Properties.NullText = "[Empty]";
            this.LueWhsCode9.Properties.PopupWidth = 500;
            this.LueWhsCode9.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode9.TabIndex = 50;
            this.LueWhsCode9.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode9.EditValueChanged += new System.EventHandler(this.LueWhsCode9_EditValueChanged);
            // 
            // LueShelf9
            // 
            this.LueShelf9.EnterMoveNextControl = true;
            this.LueShelf9.Location = new System.Drawing.Point(35, 6);
            this.LueShelf9.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf9.Name = "LueShelf9";
            this.LueShelf9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.Appearance.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf9.Properties.DropDownRows = 20;
            this.LueShelf9.Properties.NullText = "[Empty]";
            this.LueShelf9.Properties.PopupWidth = 500;
            this.LueShelf9.Size = new System.Drawing.Size(245, 20);
            this.LueShelf9.TabIndex = 46;
            this.LueShelf9.ToolTip = "F4 : Show/hide list";
            this.LueShelf9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf9.EditValueChanged += new System.EventHandler(this.LueShelf9_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(285, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 14);
            this.label24.TabIndex = 47;
            this.label24.Text = "Total";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal9
            // 
            this.TxtTotal9.EnterMoveNextControl = true;
            this.TxtTotal9.Location = new System.Drawing.Point(322, 6);
            this.TxtTotal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal9.Name = "TxtTotal9";
            this.TxtTotal9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal9.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal9.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal9.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal9.TabIndex = 48;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(8, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(26, 14);
            this.label29.TabIndex = 45;
            this.label29.Text = "Rak";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.LueItCode10);
            this.tabPage10.Controls.Add(this.Grd20);
            this.tabPage10.Controls.Add(this.panel17);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(764, 0);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "No.10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // LueItCode10
            // 
            this.LueItCode10.EnterMoveNextControl = true;
            this.LueItCode10.Location = new System.Drawing.Point(20, 54);
            this.LueItCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode10.Name = "LueItCode10";
            this.LueItCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.Appearance.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode10.Properties.DropDownRows = 20;
            this.LueItCode10.Properties.NullText = "[Empty]";
            this.LueItCode10.Properties.PopupWidth = 500;
            this.LueItCode10.Size = new System.Drawing.Size(163, 20);
            this.LueItCode10.TabIndex = 52;
            this.LueItCode10.TabStop = false;
            this.LueItCode10.ToolTip = "F4 : Show/hide list";
            this.LueItCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode10.EditValueChanged += new System.EventHandler(this.LueItCode10_EditValueChanged);
            this.LueItCode10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode10_KeyDown);
            this.LueItCode10.Leave += new System.EventHandler(this.LueItCode10_Leave);
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 21;
            this.Grd20.Location = new System.Drawing.Point(0, 33);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(764, 0);
            this.Grd20.TabIndex = 51;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd20.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd20_ColHdrDoubleClick);
            this.Grd20.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd20_RequestEdit);
            this.Grd20.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd20_AfterCommitEdit);
            this.Grd20.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd20_CustomDrawCellForeground);
            this.Grd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd20_KeyDown);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel17.Controls.Add(this.label52);
            this.panel17.Controls.Add(this.LueWhsCode10);
            this.panel17.Controls.Add(this.LueShelf10);
            this.panel17.Controls.Add(this.label26);
            this.panel17.Controls.Add(this.TxtTotal10);
            this.panel17.Controls.Add(this.label31);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(764, 33);
            this.panel17.TabIndex = 44;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Red;
            this.label52.Location = new System.Drawing.Point(421, 9);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(69, 14);
            this.label52.TabIndex = 49;
            this.label52.Text = "Warehouse";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode10
            // 
            this.LueWhsCode10.EnterMoveNextControl = true;
            this.LueWhsCode10.Location = new System.Drawing.Point(495, 6);
            this.LueWhsCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode10.Name = "LueWhsCode10";
            this.LueWhsCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode10.Properties.DropDownRows = 20;
            this.LueWhsCode10.Properties.NullText = "[Empty]";
            this.LueWhsCode10.Properties.PopupWidth = 500;
            this.LueWhsCode10.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode10.TabIndex = 50;
            this.LueWhsCode10.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode10.EditValueChanged += new System.EventHandler(this.LueWhsCode10_EditValueChanged);
            // 
            // LueShelf10
            // 
            this.LueShelf10.EnterMoveNextControl = true;
            this.LueShelf10.Location = new System.Drawing.Point(38, 6);
            this.LueShelf10.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf10.Name = "LueShelf10";
            this.LueShelf10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.Appearance.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf10.Properties.DropDownRows = 20;
            this.LueShelf10.Properties.NullText = "[Empty]";
            this.LueShelf10.Properties.PopupWidth = 500;
            this.LueShelf10.Size = new System.Drawing.Size(245, 20);
            this.LueShelf10.TabIndex = 46;
            this.LueShelf10.ToolTip = "F4 : Show/hide list";
            this.LueShelf10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf10.EditValueChanged += new System.EventHandler(this.LueShelf10_EditValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(288, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 14);
            this.label26.TabIndex = 47;
            this.label26.Text = "Total";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal10
            // 
            this.TxtTotal10.EnterMoveNextControl = true;
            this.TxtTotal10.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal10.Name = "TxtTotal10";
            this.TxtTotal10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal10.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal10.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal10.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal10.TabIndex = 48;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(8, 9);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 14);
            this.label31.TabIndex = 45;
            this.label31.Text = "Rak";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.LueItCode11);
            this.tabPage11.Controls.Add(this.Grd21);
            this.tabPage11.Controls.Add(this.panel5);
            this.tabPage11.Location = new System.Drawing.Point(4, 23);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(764, 0);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "No.11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // LueItCode11
            // 
            this.LueItCode11.EnterMoveNextControl = true;
            this.LueItCode11.Location = new System.Drawing.Point(21, 55);
            this.LueItCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode11.Name = "LueItCode11";
            this.LueItCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.Appearance.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode11.Properties.DropDownRows = 20;
            this.LueItCode11.Properties.NullText = "[Empty]";
            this.LueItCode11.Properties.PopupWidth = 500;
            this.LueItCode11.Size = new System.Drawing.Size(163, 20);
            this.LueItCode11.TabIndex = 52;
            this.LueItCode11.TabStop = false;
            this.LueItCode11.ToolTip = "F4 : Show/hide list";
            this.LueItCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode11.EditValueChanged += new System.EventHandler(this.LueItCode11_EditValueChanged);
            this.LueItCode11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode11_KeyDown);
            this.LueItCode11.Leave += new System.EventHandler(this.LueItCode11_Leave);
            // 
            // Grd21
            // 
            this.Grd21.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd21.DefaultRow.Height = 20;
            this.Grd21.DefaultRow.Sortable = false;
            this.Grd21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd21.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd21.Header.Height = 21;
            this.Grd21.Location = new System.Drawing.Point(0, 33);
            this.Grd21.Name = "Grd21";
            this.Grd21.RowHeader.Visible = true;
            this.Grd21.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd21.SingleClickEdit = true;
            this.Grd21.Size = new System.Drawing.Size(764, 0);
            this.Grd21.TabIndex = 51;
            this.Grd21.TreeCol = null;
            this.Grd21.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd21.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd21_ColHdrDoubleClick);
            this.Grd21.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd21_RequestEdit);
            this.Grd21.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd21_AfterCommitEdit);
            this.Grd21.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd21_CustomDrawCellForeground);
            this.Grd21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd21_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.LueWhsCode11);
            this.panel5.Controls.Add(this.LueShelf11);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.TxtTotal11);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(764, 33);
            this.panel5.TabIndex = 44;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Location = new System.Drawing.Point(424, 9);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(69, 14);
            this.label53.TabIndex = 49;
            this.label53.Text = "Warehouse";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode11
            // 
            this.LueWhsCode11.EnterMoveNextControl = true;
            this.LueWhsCode11.Location = new System.Drawing.Point(498, 6);
            this.LueWhsCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode11.Name = "LueWhsCode11";
            this.LueWhsCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode11.Properties.DropDownRows = 20;
            this.LueWhsCode11.Properties.NullText = "[Empty]";
            this.LueWhsCode11.Properties.PopupWidth = 500;
            this.LueWhsCode11.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode11.TabIndex = 50;
            this.LueWhsCode11.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode11.EditValueChanged += new System.EventHandler(this.LueWhsCode11_EditValueChanged);
            // 
            // LueShelf11
            // 
            this.LueShelf11.EnterMoveNextControl = true;
            this.LueShelf11.Location = new System.Drawing.Point(38, 6);
            this.LueShelf11.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf11.Name = "LueShelf11";
            this.LueShelf11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.Appearance.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf11.Properties.DropDownRows = 20;
            this.LueShelf11.Properties.NullText = "[Empty]";
            this.LueShelf11.Properties.PopupWidth = 500;
            this.LueShelf11.Size = new System.Drawing.Size(245, 20);
            this.LueShelf11.TabIndex = 46;
            this.LueShelf11.ToolTip = "F4 : Show/hide list";
            this.LueShelf11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf11.EditValueChanged += new System.EventHandler(this.LueShelf11_EditValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(288, 9);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 14);
            this.label30.TabIndex = 47;
            this.label30.Text = "Total";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal11
            // 
            this.TxtTotal11.EnterMoveNextControl = true;
            this.TxtTotal11.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal11.Name = "TxtTotal11";
            this.TxtTotal11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal11.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal11.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal11.TabIndex = 48;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(8, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 14);
            this.label32.TabIndex = 45;
            this.label32.Text = "Rak";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.LueItCode12);
            this.tabPage12.Controls.Add(this.Grd22);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Location = new System.Drawing.Point(4, 23);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(764, 0);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "No.12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // LueItCode12
            // 
            this.LueItCode12.EnterMoveNextControl = true;
            this.LueItCode12.Location = new System.Drawing.Point(64, 53);
            this.LueItCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode12.Name = "LueItCode12";
            this.LueItCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.Appearance.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode12.Properties.DropDownRows = 20;
            this.LueItCode12.Properties.NullText = "[Empty]";
            this.LueItCode12.Properties.PopupWidth = 500;
            this.LueItCode12.Size = new System.Drawing.Size(163, 20);
            this.LueItCode12.TabIndex = 52;
            this.LueItCode12.TabStop = false;
            this.LueItCode12.ToolTip = "F4 : Show/hide list";
            this.LueItCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode12.EditValueChanged += new System.EventHandler(this.LueItCode12_EditValueChanged);
            this.LueItCode12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode12_KeyDown);
            this.LueItCode12.Leave += new System.EventHandler(this.LueItCode12_Leave);
            // 
            // Grd22
            // 
            this.Grd22.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd22.DefaultRow.Height = 20;
            this.Grd22.DefaultRow.Sortable = false;
            this.Grd22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd22.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd22.Header.Height = 21;
            this.Grd22.Location = new System.Drawing.Point(0, 33);
            this.Grd22.Name = "Grd22";
            this.Grd22.RowHeader.Visible = true;
            this.Grd22.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd22.SingleClickEdit = true;
            this.Grd22.Size = new System.Drawing.Size(764, 0);
            this.Grd22.TabIndex = 51;
            this.Grd22.TreeCol = null;
            this.Grd22.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd22.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd22_ColHdrDoubleClick);
            this.Grd22.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd22_RequestEdit);
            this.Grd22.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd22_AfterCommitEdit);
            this.Grd22.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd22_CustomDrawCellForeground);
            this.Grd22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd22_KeyDown);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel18.Controls.Add(this.label54);
            this.panel18.Controls.Add(this.LueWhsCode12);
            this.panel18.Controls.Add(this.LueShelf12);
            this.panel18.Controls.Add(this.label33);
            this.panel18.Controls.Add(this.TxtTotal12);
            this.panel18.Controls.Add(this.label34);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(764, 33);
            this.panel18.TabIndex = 44;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Red;
            this.label54.Location = new System.Drawing.Point(424, 9);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(69, 14);
            this.label54.TabIndex = 49;
            this.label54.Text = "Warehouse";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode12
            // 
            this.LueWhsCode12.EnterMoveNextControl = true;
            this.LueWhsCode12.Location = new System.Drawing.Point(498, 6);
            this.LueWhsCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode12.Name = "LueWhsCode12";
            this.LueWhsCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode12.Properties.DropDownRows = 20;
            this.LueWhsCode12.Properties.NullText = "[Empty]";
            this.LueWhsCode12.Properties.PopupWidth = 500;
            this.LueWhsCode12.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode12.TabIndex = 50;
            this.LueWhsCode12.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode12.EditValueChanged += new System.EventHandler(this.LueWhsCode12_EditValueChanged);
            // 
            // LueShelf12
            // 
            this.LueShelf12.EnterMoveNextControl = true;
            this.LueShelf12.Location = new System.Drawing.Point(38, 6);
            this.LueShelf12.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf12.Name = "LueShelf12";
            this.LueShelf12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.Appearance.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf12.Properties.DropDownRows = 20;
            this.LueShelf12.Properties.NullText = "[Empty]";
            this.LueShelf12.Properties.PopupWidth = 500;
            this.LueShelf12.Size = new System.Drawing.Size(245, 20);
            this.LueShelf12.TabIndex = 46;
            this.LueShelf12.ToolTip = "F4 : Show/hide list";
            this.LueShelf12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf12.EditValueChanged += new System.EventHandler(this.LueShelf12_EditValueChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(288, 9);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 14);
            this.label33.TabIndex = 47;
            this.label33.Text = "Total";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal12
            // 
            this.TxtTotal12.EnterMoveNextControl = true;
            this.TxtTotal12.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal12.Name = "TxtTotal12";
            this.TxtTotal12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal12.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal12.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal12.TabIndex = 48;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(8, 9);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 14);
            this.label34.TabIndex = 45;
            this.label34.Text = "Rak";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.LueItCode13);
            this.tabPage13.Controls.Add(this.Grd23);
            this.tabPage13.Controls.Add(this.panel19);
            this.tabPage13.Location = new System.Drawing.Point(4, 23);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(764, 0);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "No.13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // LueItCode13
            // 
            this.LueItCode13.EnterMoveNextControl = true;
            this.LueItCode13.Location = new System.Drawing.Point(19, 54);
            this.LueItCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode13.Name = "LueItCode13";
            this.LueItCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.Appearance.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode13.Properties.DropDownRows = 20;
            this.LueItCode13.Properties.NullText = "[Empty]";
            this.LueItCode13.Properties.PopupWidth = 500;
            this.LueItCode13.Size = new System.Drawing.Size(163, 20);
            this.LueItCode13.TabIndex = 52;
            this.LueItCode13.TabStop = false;
            this.LueItCode13.ToolTip = "F4 : Show/hide list";
            this.LueItCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode13.EditValueChanged += new System.EventHandler(this.LueItCode13_EditValueChanged);
            this.LueItCode13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode13_KeyDown);
            this.LueItCode13.Leave += new System.EventHandler(this.LueItCode13_Leave);
            // 
            // Grd23
            // 
            this.Grd23.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd23.DefaultRow.Height = 20;
            this.Grd23.DefaultRow.Sortable = false;
            this.Grd23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd23.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd23.Header.Height = 21;
            this.Grd23.Location = new System.Drawing.Point(0, 33);
            this.Grd23.Name = "Grd23";
            this.Grd23.RowHeader.Visible = true;
            this.Grd23.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd23.SingleClickEdit = true;
            this.Grd23.Size = new System.Drawing.Size(764, 0);
            this.Grd23.TabIndex = 51;
            this.Grd23.TreeCol = null;
            this.Grd23.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd23.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd23_ColHdrDoubleClick);
            this.Grd23.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd23_RequestEdit);
            this.Grd23.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd23_AfterCommitEdit);
            this.Grd23.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd23_CustomDrawCellForeground);
            this.Grd23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd23_KeyDown);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.label55);
            this.panel19.Controls.Add(this.LueWhsCode13);
            this.panel19.Controls.Add(this.LueShelf13);
            this.panel19.Controls.Add(this.label35);
            this.panel19.Controls.Add(this.TxtTotal13);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(764, 33);
            this.panel19.TabIndex = 44;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Red;
            this.label55.Location = new System.Drawing.Point(423, 9);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(69, 14);
            this.label55.TabIndex = 49;
            this.label55.Text = "Warehouse";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode13
            // 
            this.LueWhsCode13.EnterMoveNextControl = true;
            this.LueWhsCode13.Location = new System.Drawing.Point(497, 6);
            this.LueWhsCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode13.Name = "LueWhsCode13";
            this.LueWhsCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode13.Properties.DropDownRows = 20;
            this.LueWhsCode13.Properties.NullText = "[Empty]";
            this.LueWhsCode13.Properties.PopupWidth = 500;
            this.LueWhsCode13.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode13.TabIndex = 50;
            this.LueWhsCode13.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode13.EditValueChanged += new System.EventHandler(this.LueWhsCode13_EditValueChanged);
            // 
            // LueShelf13
            // 
            this.LueShelf13.EnterMoveNextControl = true;
            this.LueShelf13.Location = new System.Drawing.Point(38, 6);
            this.LueShelf13.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf13.Name = "LueShelf13";
            this.LueShelf13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.Appearance.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf13.Properties.DropDownRows = 20;
            this.LueShelf13.Properties.NullText = "[Empty]";
            this.LueShelf13.Properties.PopupWidth = 500;
            this.LueShelf13.Size = new System.Drawing.Size(245, 20);
            this.LueShelf13.TabIndex = 46;
            this.LueShelf13.ToolTip = "F4 : Show/hide list";
            this.LueShelf13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf13.EditValueChanged += new System.EventHandler(this.LueShelf13_EditValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(288, 9);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 14);
            this.label35.TabIndex = 47;
            this.label35.Text = "Total";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal13
            // 
            this.TxtTotal13.EnterMoveNextControl = true;
            this.TxtTotal13.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal13.Name = "TxtTotal13";
            this.TxtTotal13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal13.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal13.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal13.TabIndex = 48;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(8, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(26, 14);
            this.label36.TabIndex = 45;
            this.label36.Text = "Rak";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.LueItCode14);
            this.tabPage14.Controls.Add(this.Grd24);
            this.tabPage14.Controls.Add(this.panel20);
            this.tabPage14.Location = new System.Drawing.Point(4, 23);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(764, 0);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "No.14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // LueItCode14
            // 
            this.LueItCode14.EnterMoveNextControl = true;
            this.LueItCode14.Location = new System.Drawing.Point(64, 53);
            this.LueItCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode14.Name = "LueItCode14";
            this.LueItCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.Appearance.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode14.Properties.DropDownRows = 20;
            this.LueItCode14.Properties.NullText = "[Empty]";
            this.LueItCode14.Properties.PopupWidth = 500;
            this.LueItCode14.Size = new System.Drawing.Size(163, 20);
            this.LueItCode14.TabIndex = 52;
            this.LueItCode14.TabStop = false;
            this.LueItCode14.ToolTip = "F4 : Show/hide list";
            this.LueItCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode14.EditValueChanged += new System.EventHandler(this.LueItCode14_EditValueChanged);
            this.LueItCode14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode14_KeyDown);
            this.LueItCode14.Leave += new System.EventHandler(this.LueItCode14_Leave);
            // 
            // Grd24
            // 
            this.Grd24.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd24.DefaultRow.Height = 20;
            this.Grd24.DefaultRow.Sortable = false;
            this.Grd24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd24.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd24.Header.Height = 21;
            this.Grd24.Location = new System.Drawing.Point(0, 33);
            this.Grd24.Name = "Grd24";
            this.Grd24.RowHeader.Visible = true;
            this.Grd24.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd24.SingleClickEdit = true;
            this.Grd24.Size = new System.Drawing.Size(764, 0);
            this.Grd24.TabIndex = 51;
            this.Grd24.TreeCol = null;
            this.Grd24.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd24.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd24_ColHdrDoubleClick);
            this.Grd24.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd24_RequestEdit);
            this.Grd24.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd24_AfterCommitEdit);
            this.Grd24.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd24_CustomDrawCellForeground);
            this.Grd24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd24_KeyDown);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel20.Controls.Add(this.label56);
            this.panel20.Controls.Add(this.LueWhsCode14);
            this.panel20.Controls.Add(this.LueShelf14);
            this.panel20.Controls.Add(this.label37);
            this.panel20.Controls.Add(this.TxtTotal14);
            this.panel20.Controls.Add(this.label38);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(764, 33);
            this.panel20.TabIndex = 44;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Red;
            this.label56.Location = new System.Drawing.Point(427, 9);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(69, 14);
            this.label56.TabIndex = 49;
            this.label56.Text = "Warehouse";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode14
            // 
            this.LueWhsCode14.EnterMoveNextControl = true;
            this.LueWhsCode14.Location = new System.Drawing.Point(501, 6);
            this.LueWhsCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode14.Name = "LueWhsCode14";
            this.LueWhsCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode14.Properties.DropDownRows = 20;
            this.LueWhsCode14.Properties.NullText = "[Empty]";
            this.LueWhsCode14.Properties.PopupWidth = 500;
            this.LueWhsCode14.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode14.TabIndex = 50;
            this.LueWhsCode14.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode14.EditValueChanged += new System.EventHandler(this.LueWhsCode14_EditValueChanged);
            // 
            // LueShelf14
            // 
            this.LueShelf14.EnterMoveNextControl = true;
            this.LueShelf14.Location = new System.Drawing.Point(38, 6);
            this.LueShelf14.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf14.Name = "LueShelf14";
            this.LueShelf14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.Appearance.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf14.Properties.DropDownRows = 20;
            this.LueShelf14.Properties.NullText = "[Empty]";
            this.LueShelf14.Properties.PopupWidth = 500;
            this.LueShelf14.Size = new System.Drawing.Size(245, 20);
            this.LueShelf14.TabIndex = 46;
            this.LueShelf14.ToolTip = "F4 : Show/hide list";
            this.LueShelf14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf14.EditValueChanged += new System.EventHandler(this.LueShelf14_EditValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(288, 9);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 14);
            this.label37.TabIndex = 47;
            this.label37.Text = "Total";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal14
            // 
            this.TxtTotal14.EnterMoveNextControl = true;
            this.TxtTotal14.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal14.Name = "TxtTotal14";
            this.TxtTotal14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal14.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal14.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal14.TabIndex = 48;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(8, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(26, 14);
            this.label38.TabIndex = 45;
            this.label38.Text = "Rak";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.LueItCode15);
            this.tabPage15.Controls.Add(this.Grd25);
            this.tabPage15.Controls.Add(this.panel21);
            this.tabPage15.Location = new System.Drawing.Point(4, 23);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(764, 236);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "No.15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // LueItCode15
            // 
            this.LueItCode15.EnterMoveNextControl = true;
            this.LueItCode15.Location = new System.Drawing.Point(19, 53);
            this.LueItCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode15.Name = "LueItCode15";
            this.LueItCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.Appearance.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode15.Properties.DropDownRows = 20;
            this.LueItCode15.Properties.NullText = "[Empty]";
            this.LueItCode15.Properties.PopupWidth = 500;
            this.LueItCode15.Size = new System.Drawing.Size(163, 20);
            this.LueItCode15.TabIndex = 52;
            this.LueItCode15.TabStop = false;
            this.LueItCode15.ToolTip = "F4 : Show/hide list";
            this.LueItCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode15.EditValueChanged += new System.EventHandler(this.LueItCode15_EditValueChanged);
            this.LueItCode15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode15_KeyDown);
            this.LueItCode15.Leave += new System.EventHandler(this.LueItCode15_Leave);
            // 
            // Grd25
            // 
            this.Grd25.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd25.DefaultRow.Height = 20;
            this.Grd25.DefaultRow.Sortable = false;
            this.Grd25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd25.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd25.Header.Height = 21;
            this.Grd25.Location = new System.Drawing.Point(0, 33);
            this.Grd25.Name = "Grd25";
            this.Grd25.RowHeader.Visible = true;
            this.Grd25.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd25.SingleClickEdit = true;
            this.Grd25.Size = new System.Drawing.Size(764, 203);
            this.Grd25.TabIndex = 51;
            this.Grd25.TreeCol = null;
            this.Grd25.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd25.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd25_ColHdrDoubleClick);
            this.Grd25.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd25_RequestEdit);
            this.Grd25.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd25_AfterCommitEdit);
            this.Grd25.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd25_CustomDrawCellForeground);
            this.Grd25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd25_KeyDown);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel21.Controls.Add(this.label57);
            this.panel21.Controls.Add(this.LueWhsCode15);
            this.panel21.Controls.Add(this.LueShelf15);
            this.panel21.Controls.Add(this.label39);
            this.panel21.Controls.Add(this.TxtTotal15);
            this.panel21.Controls.Add(this.label40);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(764, 33);
            this.panel21.TabIndex = 44;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Location = new System.Drawing.Point(430, 9);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(69, 14);
            this.label57.TabIndex = 49;
            this.label57.Text = "Warehouse";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode15
            // 
            this.LueWhsCode15.EnterMoveNextControl = true;
            this.LueWhsCode15.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode15.Name = "LueWhsCode15";
            this.LueWhsCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode15.Properties.DropDownRows = 20;
            this.LueWhsCode15.Properties.NullText = "[Empty]";
            this.LueWhsCode15.Properties.PopupWidth = 500;
            this.LueWhsCode15.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode15.TabIndex = 50;
            this.LueWhsCode15.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode15.EditValueChanged += new System.EventHandler(this.LueWhsCode15_EditValueChanged);
            // 
            // LueShelf15
            // 
            this.LueShelf15.EnterMoveNextControl = true;
            this.LueShelf15.Location = new System.Drawing.Point(38, 6);
            this.LueShelf15.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf15.Name = "LueShelf15";
            this.LueShelf15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.Appearance.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf15.Properties.DropDownRows = 20;
            this.LueShelf15.Properties.NullText = "[Empty]";
            this.LueShelf15.Properties.PopupWidth = 500;
            this.LueShelf15.Size = new System.Drawing.Size(245, 20);
            this.LueShelf15.TabIndex = 46;
            this.LueShelf15.ToolTip = "F4 : Show/hide list";
            this.LueShelf15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf15.EditValueChanged += new System.EventHandler(this.LueShelf15_EditValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(288, 9);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 14);
            this.label39.TabIndex = 47;
            this.label39.Text = "Total";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal15
            // 
            this.TxtTotal15.EnterMoveNextControl = true;
            this.TxtTotal15.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal15.Name = "TxtTotal15";
            this.TxtTotal15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal15.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal15.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal15.TabIndex = 48;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Red;
            this.label40.Location = new System.Drawing.Point(8, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(26, 14);
            this.label40.TabIndex = 45;
            this.label40.Text = "Rak";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.LueItCode16);
            this.tabPage16.Controls.Add(this.Grd26);
            this.tabPage16.Controls.Add(this.panel22);
            this.tabPage16.Location = new System.Drawing.Point(4, 23);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(764, 236);
            this.tabPage16.TabIndex = 17;
            this.tabPage16.Text = "No.16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // LueItCode16
            // 
            this.LueItCode16.EnterMoveNextControl = true;
            this.LueItCode16.Location = new System.Drawing.Point(19, 53);
            this.LueItCode16.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode16.Name = "LueItCode16";
            this.LueItCode16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.Appearance.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode16.Properties.DropDownRows = 20;
            this.LueItCode16.Properties.NullText = "[Empty]";
            this.LueItCode16.Properties.PopupWidth = 500;
            this.LueItCode16.Size = new System.Drawing.Size(163, 20);
            this.LueItCode16.TabIndex = 53;
            this.LueItCode16.TabStop = false;
            this.LueItCode16.ToolTip = "F4 : Show/hide list";
            this.LueItCode16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode16.EditValueChanged += new System.EventHandler(this.LueItCode16_EditValueChanged);
            this.LueItCode16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode16_KeyDown);
            this.LueItCode16.Leave += new System.EventHandler(this.LueItCode16_Leave);
            // 
            // Grd26
            // 
            this.Grd26.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd26.DefaultRow.Height = 20;
            this.Grd26.DefaultRow.Sortable = false;
            this.Grd26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd26.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd26.Header.Height = 21;
            this.Grd26.Location = new System.Drawing.Point(0, 33);
            this.Grd26.Name = "Grd26";
            this.Grd26.RowHeader.Visible = true;
            this.Grd26.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd26.SingleClickEdit = true;
            this.Grd26.Size = new System.Drawing.Size(764, 203);
            this.Grd26.TabIndex = 52;
            this.Grd26.TreeCol = null;
            this.Grd26.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd26.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd26_ColHdrDoubleClick);
            this.Grd26.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd26_RequestEdit);
            this.Grd26.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd26_AfterCommitEdit);
            this.Grd26.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd26_CustomDrawCellForeground);
            this.Grd26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd26_KeyDown);
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel22.Controls.Add(this.label58);
            this.panel22.Controls.Add(this.LueWhsCode16);
            this.panel22.Controls.Add(this.LueShelf16);
            this.panel22.Controls.Add(this.label59);
            this.panel22.Controls.Add(this.TxtTotal16);
            this.panel22.Controls.Add(this.label60);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(764, 33);
            this.panel22.TabIndex = 45;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Red;
            this.label58.Location = new System.Drawing.Point(430, 9);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(69, 14);
            this.label58.TabIndex = 49;
            this.label58.Text = "Warehouse";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode16
            // 
            this.LueWhsCode16.EnterMoveNextControl = true;
            this.LueWhsCode16.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode16.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode16.Name = "LueWhsCode16";
            this.LueWhsCode16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode16.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode16.Properties.DropDownRows = 20;
            this.LueWhsCode16.Properties.NullText = "[Empty]";
            this.LueWhsCode16.Properties.PopupWidth = 500;
            this.LueWhsCode16.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode16.TabIndex = 50;
            this.LueWhsCode16.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode16.EditValueChanged += new System.EventHandler(this.LueWhsCode16_EditValueChanged);
            // 
            // LueShelf16
            // 
            this.LueShelf16.EnterMoveNextControl = true;
            this.LueShelf16.Location = new System.Drawing.Point(38, 6);
            this.LueShelf16.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf16.Name = "LueShelf16";
            this.LueShelf16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf16.Properties.Appearance.Options.UseFont = true;
            this.LueShelf16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf16.Properties.DropDownRows = 20;
            this.LueShelf16.Properties.NullText = "[Empty]";
            this.LueShelf16.Properties.PopupWidth = 500;
            this.LueShelf16.Size = new System.Drawing.Size(245, 20);
            this.LueShelf16.TabIndex = 46;
            this.LueShelf16.ToolTip = "F4 : Show/hide list";
            this.LueShelf16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf16.EditValueChanged += new System.EventHandler(this.LueShelf16_EditValueChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(288, 9);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(35, 14);
            this.label59.TabIndex = 47;
            this.label59.Text = "Total";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal16
            // 
            this.TxtTotal16.EnterMoveNextControl = true;
            this.TxtTotal16.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal16.Name = "TxtTotal16";
            this.TxtTotal16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal16.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal16.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal16.TabIndex = 48;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Red;
            this.label60.Location = new System.Drawing.Point(8, 9);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(26, 14);
            this.label60.TabIndex = 45;
            this.label60.Text = "Rak";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.LueItCode17);
            this.tabPage17.Controls.Add(this.Grd27);
            this.tabPage17.Controls.Add(this.panel23);
            this.tabPage17.Location = new System.Drawing.Point(4, 23);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Size = new System.Drawing.Size(764, 236);
            this.tabPage17.TabIndex = 16;
            this.tabPage17.Text = "No.17";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // LueItCode17
            // 
            this.LueItCode17.EnterMoveNextControl = true;
            this.LueItCode17.Location = new System.Drawing.Point(19, 53);
            this.LueItCode17.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode17.Name = "LueItCode17";
            this.LueItCode17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.Appearance.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode17.Properties.DropDownRows = 20;
            this.LueItCode17.Properties.NullText = "[Empty]";
            this.LueItCode17.Properties.PopupWidth = 500;
            this.LueItCode17.Size = new System.Drawing.Size(163, 20);
            this.LueItCode17.TabIndex = 54;
            this.LueItCode17.TabStop = false;
            this.LueItCode17.ToolTip = "F4 : Show/hide list";
            this.LueItCode17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode17.EditValueChanged += new System.EventHandler(this.LueItCode17_EditValueChanged);
            this.LueItCode17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode17_KeyDown);
            this.LueItCode17.Leave += new System.EventHandler(this.LueItCode17_Leave);
            // 
            // Grd27
            // 
            this.Grd27.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd27.DefaultRow.Height = 20;
            this.Grd27.DefaultRow.Sortable = false;
            this.Grd27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd27.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd27.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd27.Header.Height = 21;
            this.Grd27.Location = new System.Drawing.Point(0, 33);
            this.Grd27.Name = "Grd27";
            this.Grd27.RowHeader.Visible = true;
            this.Grd27.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd27.SingleClickEdit = true;
            this.Grd27.Size = new System.Drawing.Size(764, 203);
            this.Grd27.TabIndex = 53;
            this.Grd27.TreeCol = null;
            this.Grd27.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd27.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd27_ColHdrDoubleClick);
            this.Grd27.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd27_RequestEdit);
            this.Grd27.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd27_AfterCommitEdit);
            this.Grd27.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd27_CustomDrawCellForeground);
            this.Grd27.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd27_KeyDown);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel23.Controls.Add(this.label61);
            this.panel23.Controls.Add(this.LueWhsCode17);
            this.panel23.Controls.Add(this.LueShelf17);
            this.panel23.Controls.Add(this.label62);
            this.panel23.Controls.Add(this.TxtTotal17);
            this.panel23.Controls.Add(this.label63);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(764, 33);
            this.panel23.TabIndex = 46;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Red;
            this.label61.Location = new System.Drawing.Point(430, 9);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(69, 14);
            this.label61.TabIndex = 49;
            this.label61.Text = "Warehouse";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode17
            // 
            this.LueWhsCode17.EnterMoveNextControl = true;
            this.LueWhsCode17.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode17.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode17.Name = "LueWhsCode17";
            this.LueWhsCode17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode17.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode17.Properties.DropDownRows = 20;
            this.LueWhsCode17.Properties.NullText = "[Empty]";
            this.LueWhsCode17.Properties.PopupWidth = 500;
            this.LueWhsCode17.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode17.TabIndex = 50;
            this.LueWhsCode17.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode17.EditValueChanged += new System.EventHandler(this.LueWhsCode17_EditValueChanged);
            // 
            // LueShelf17
            // 
            this.LueShelf17.EnterMoveNextControl = true;
            this.LueShelf17.Location = new System.Drawing.Point(38, 6);
            this.LueShelf17.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf17.Name = "LueShelf17";
            this.LueShelf17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf17.Properties.Appearance.Options.UseFont = true;
            this.LueShelf17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf17.Properties.DropDownRows = 20;
            this.LueShelf17.Properties.NullText = "[Empty]";
            this.LueShelf17.Properties.PopupWidth = 500;
            this.LueShelf17.Size = new System.Drawing.Size(245, 20);
            this.LueShelf17.TabIndex = 46;
            this.LueShelf17.ToolTip = "F4 : Show/hide list";
            this.LueShelf17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf17.EditValueChanged += new System.EventHandler(this.LueShelf17_EditValueChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(288, 9);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(35, 14);
            this.label62.TabIndex = 47;
            this.label62.Text = "Total";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal17
            // 
            this.TxtTotal17.EnterMoveNextControl = true;
            this.TxtTotal17.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal17.Name = "TxtTotal17";
            this.TxtTotal17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal17.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal17.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal17.TabIndex = 48;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(8, 9);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(26, 14);
            this.label63.TabIndex = 45;
            this.label63.Text = "Rak";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.LueItCode18);
            this.tabPage18.Controls.Add(this.Grd28);
            this.tabPage18.Controls.Add(this.panel24);
            this.tabPage18.Location = new System.Drawing.Point(4, 23);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Size = new System.Drawing.Size(764, 0);
            this.tabPage18.TabIndex = 18;
            this.tabPage18.Text = "No.18";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // LueItCode18
            // 
            this.LueItCode18.EnterMoveNextControl = true;
            this.LueItCode18.Location = new System.Drawing.Point(19, 53);
            this.LueItCode18.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode18.Name = "LueItCode18";
            this.LueItCode18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.Appearance.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode18.Properties.DropDownRows = 20;
            this.LueItCode18.Properties.NullText = "[Empty]";
            this.LueItCode18.Properties.PopupWidth = 500;
            this.LueItCode18.Size = new System.Drawing.Size(163, 20);
            this.LueItCode18.TabIndex = 55;
            this.LueItCode18.TabStop = false;
            this.LueItCode18.ToolTip = "F4 : Show/hide list";
            this.LueItCode18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode18.EditValueChanged += new System.EventHandler(this.LueItCode18_EditValueChanged);
            this.LueItCode18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode18_KeyDown);
            this.LueItCode18.Leave += new System.EventHandler(this.LueItCode18_Leave);
            // 
            // Grd28
            // 
            this.Grd28.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd28.DefaultRow.Height = 20;
            this.Grd28.DefaultRow.Sortable = false;
            this.Grd28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd28.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd28.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd28.Header.Height = 21;
            this.Grd28.Location = new System.Drawing.Point(0, 33);
            this.Grd28.Name = "Grd28";
            this.Grd28.RowHeader.Visible = true;
            this.Grd28.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd28.SingleClickEdit = true;
            this.Grd28.Size = new System.Drawing.Size(764, 0);
            this.Grd28.TabIndex = 54;
            this.Grd28.TreeCol = null;
            this.Grd28.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd28.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd28_ColHdrDoubleClick);
            this.Grd28.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd28_RequestEdit);
            this.Grd28.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd28_AfterCommitEdit);
            this.Grd28.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd28_CustomDrawCellForeground);
            this.Grd28.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd28_KeyDown);
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel24.Controls.Add(this.label64);
            this.panel24.Controls.Add(this.LueWhsCode18);
            this.panel24.Controls.Add(this.LueShelf18);
            this.panel24.Controls.Add(this.label65);
            this.panel24.Controls.Add(this.TxtTotal18);
            this.panel24.Controls.Add(this.label66);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(764, 33);
            this.panel24.TabIndex = 47;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Red;
            this.label64.Location = new System.Drawing.Point(430, 9);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(69, 14);
            this.label64.TabIndex = 49;
            this.label64.Text = "Warehouse";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode18
            // 
            this.LueWhsCode18.EnterMoveNextControl = true;
            this.LueWhsCode18.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode18.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode18.Name = "LueWhsCode18";
            this.LueWhsCode18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode18.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode18.Properties.DropDownRows = 20;
            this.LueWhsCode18.Properties.NullText = "[Empty]";
            this.LueWhsCode18.Properties.PopupWidth = 500;
            this.LueWhsCode18.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode18.TabIndex = 50;
            this.LueWhsCode18.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode18.EditValueChanged += new System.EventHandler(this.LueWhsCode18_EditValueChanged);
            // 
            // LueShelf18
            // 
            this.LueShelf18.EnterMoveNextControl = true;
            this.LueShelf18.Location = new System.Drawing.Point(38, 6);
            this.LueShelf18.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf18.Name = "LueShelf18";
            this.LueShelf18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf18.Properties.Appearance.Options.UseFont = true;
            this.LueShelf18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf18.Properties.DropDownRows = 20;
            this.LueShelf18.Properties.NullText = "[Empty]";
            this.LueShelf18.Properties.PopupWidth = 500;
            this.LueShelf18.Size = new System.Drawing.Size(245, 20);
            this.LueShelf18.TabIndex = 46;
            this.LueShelf18.ToolTip = "F4 : Show/hide list";
            this.LueShelf18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf18.EditValueChanged += new System.EventHandler(this.LueShelf18_EditValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(288, 9);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(35, 14);
            this.label65.TabIndex = 47;
            this.label65.Text = "Total";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal18
            // 
            this.TxtTotal18.EnterMoveNextControl = true;
            this.TxtTotal18.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal18.Name = "TxtTotal18";
            this.TxtTotal18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal18.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal18.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal18.TabIndex = 48;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(8, 9);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(26, 14);
            this.label66.TabIndex = 45;
            this.label66.Text = "Rak";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.LueItCode19);
            this.tabPage19.Controls.Add(this.Grd29);
            this.tabPage19.Controls.Add(this.panel25);
            this.tabPage19.Location = new System.Drawing.Point(4, 23);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Size = new System.Drawing.Size(764, 0);
            this.tabPage19.TabIndex = 19;
            this.tabPage19.Text = "No.19";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // LueItCode19
            // 
            this.LueItCode19.EnterMoveNextControl = true;
            this.LueItCode19.Location = new System.Drawing.Point(19, 53);
            this.LueItCode19.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode19.Name = "LueItCode19";
            this.LueItCode19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.Appearance.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode19.Properties.DropDownRows = 20;
            this.LueItCode19.Properties.NullText = "[Empty]";
            this.LueItCode19.Properties.PopupWidth = 500;
            this.LueItCode19.Size = new System.Drawing.Size(163, 20);
            this.LueItCode19.TabIndex = 56;
            this.LueItCode19.TabStop = false;
            this.LueItCode19.ToolTip = "F4 : Show/hide list";
            this.LueItCode19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode19.EditValueChanged += new System.EventHandler(this.LueItCode19_EditValueChanged);
            this.LueItCode19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode19_KeyDown);
            this.LueItCode19.Leave += new System.EventHandler(this.LueItCode19_Leave);
            // 
            // Grd29
            // 
            this.Grd29.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd29.DefaultRow.Height = 20;
            this.Grd29.DefaultRow.Sortable = false;
            this.Grd29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd29.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd29.Header.Height = 21;
            this.Grd29.Location = new System.Drawing.Point(0, 33);
            this.Grd29.Name = "Grd29";
            this.Grd29.RowHeader.Visible = true;
            this.Grd29.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd29.SingleClickEdit = true;
            this.Grd29.Size = new System.Drawing.Size(764, 0);
            this.Grd29.TabIndex = 55;
            this.Grd29.TreeCol = null;
            this.Grd29.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd29.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd29_ColHdrDoubleClick);
            this.Grd29.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd29_RequestEdit);
            this.Grd29.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd29_AfterCommitEdit);
            this.Grd29.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd29_CustomDrawCellForeground);
            this.Grd29.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd29_KeyDown);
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel25.Controls.Add(this.label67);
            this.panel25.Controls.Add(this.LueWhsCode19);
            this.panel25.Controls.Add(this.LueShelf19);
            this.panel25.Controls.Add(this.label68);
            this.panel25.Controls.Add(this.TxtTotal19);
            this.panel25.Controls.Add(this.label69);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(764, 33);
            this.panel25.TabIndex = 48;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Red;
            this.label67.Location = new System.Drawing.Point(430, 9);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(69, 14);
            this.label67.TabIndex = 49;
            this.label67.Text = "Warehouse";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode19
            // 
            this.LueWhsCode19.EnterMoveNextControl = true;
            this.LueWhsCode19.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode19.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode19.Name = "LueWhsCode19";
            this.LueWhsCode19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode19.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode19.Properties.DropDownRows = 20;
            this.LueWhsCode19.Properties.NullText = "[Empty]";
            this.LueWhsCode19.Properties.PopupWidth = 500;
            this.LueWhsCode19.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode19.TabIndex = 50;
            this.LueWhsCode19.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode19.EditValueChanged += new System.EventHandler(this.LueWhsCode19_EditValueChanged);
            // 
            // LueShelf19
            // 
            this.LueShelf19.EnterMoveNextControl = true;
            this.LueShelf19.Location = new System.Drawing.Point(38, 6);
            this.LueShelf19.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf19.Name = "LueShelf19";
            this.LueShelf19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf19.Properties.Appearance.Options.UseFont = true;
            this.LueShelf19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf19.Properties.DropDownRows = 20;
            this.LueShelf19.Properties.NullText = "[Empty]";
            this.LueShelf19.Properties.PopupWidth = 500;
            this.LueShelf19.Size = new System.Drawing.Size(245, 20);
            this.LueShelf19.TabIndex = 46;
            this.LueShelf19.ToolTip = "F4 : Show/hide list";
            this.LueShelf19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf19.EditValueChanged += new System.EventHandler(this.LueShelf19_EditValueChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(288, 9);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(35, 14);
            this.label68.TabIndex = 47;
            this.label68.Text = "Total";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal19
            // 
            this.TxtTotal19.EnterMoveNextControl = true;
            this.TxtTotal19.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal19.Name = "TxtTotal19";
            this.TxtTotal19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal19.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal19.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal19.TabIndex = 48;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(8, 9);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(26, 14);
            this.label69.TabIndex = 45;
            this.label69.Text = "Rak";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.LueItCode20);
            this.tabPage20.Controls.Add(this.Grd30);
            this.tabPage20.Controls.Add(this.panel26);
            this.tabPage20.Location = new System.Drawing.Point(4, 23);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Size = new System.Drawing.Size(764, 0);
            this.tabPage20.TabIndex = 20;
            this.tabPage20.Text = "No.20";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // LueItCode20
            // 
            this.LueItCode20.EnterMoveNextControl = true;
            this.LueItCode20.Location = new System.Drawing.Point(19, 53);
            this.LueItCode20.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode20.Name = "LueItCode20";
            this.LueItCode20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.Appearance.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode20.Properties.DropDownRows = 20;
            this.LueItCode20.Properties.NullText = "[Empty]";
            this.LueItCode20.Properties.PopupWidth = 500;
            this.LueItCode20.Size = new System.Drawing.Size(163, 20);
            this.LueItCode20.TabIndex = 57;
            this.LueItCode20.TabStop = false;
            this.LueItCode20.ToolTip = "F4 : Show/hide list";
            this.LueItCode20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode20.EditValueChanged += new System.EventHandler(this.LueItCode20_EditValueChanged);
            this.LueItCode20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode20_KeyDown);
            this.LueItCode20.Leave += new System.EventHandler(this.LueItCode20_Leave);
            // 
            // Grd30
            // 
            this.Grd30.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd30.DefaultRow.Height = 20;
            this.Grd30.DefaultRow.Sortable = false;
            this.Grd30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd30.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd30.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd30.Header.Height = 21;
            this.Grd30.Location = new System.Drawing.Point(0, 33);
            this.Grd30.Name = "Grd30";
            this.Grd30.RowHeader.Visible = true;
            this.Grd30.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd30.SingleClickEdit = true;
            this.Grd30.Size = new System.Drawing.Size(764, 0);
            this.Grd30.TabIndex = 56;
            this.Grd30.TreeCol = null;
            this.Grd30.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd30.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd30_ColHdrDoubleClick);
            this.Grd30.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd30_RequestEdit);
            this.Grd30.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd30_AfterCommitEdit);
            this.Grd30.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd30_CustomDrawCellForeground);
            this.Grd30.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd30_KeyDown);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel26.Controls.Add(this.label70);
            this.panel26.Controls.Add(this.LueWhsCode20);
            this.panel26.Controls.Add(this.LueShelf20);
            this.panel26.Controls.Add(this.label71);
            this.panel26.Controls.Add(this.TxtTotal20);
            this.panel26.Controls.Add(this.label72);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(764, 33);
            this.panel26.TabIndex = 49;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(430, 9);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(69, 14);
            this.label70.TabIndex = 49;
            this.label70.Text = "Warehouse";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode20
            // 
            this.LueWhsCode20.EnterMoveNextControl = true;
            this.LueWhsCode20.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode20.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode20.Name = "LueWhsCode20";
            this.LueWhsCode20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode20.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode20.Properties.DropDownRows = 20;
            this.LueWhsCode20.Properties.NullText = "[Empty]";
            this.LueWhsCode20.Properties.PopupWidth = 500;
            this.LueWhsCode20.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode20.TabIndex = 50;
            this.LueWhsCode20.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode20.EditValueChanged += new System.EventHandler(this.LueWhsCode20_EditValueChanged);
            // 
            // LueShelf20
            // 
            this.LueShelf20.EnterMoveNextControl = true;
            this.LueShelf20.Location = new System.Drawing.Point(38, 6);
            this.LueShelf20.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf20.Name = "LueShelf20";
            this.LueShelf20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf20.Properties.Appearance.Options.UseFont = true;
            this.LueShelf20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf20.Properties.DropDownRows = 20;
            this.LueShelf20.Properties.NullText = "[Empty]";
            this.LueShelf20.Properties.PopupWidth = 500;
            this.LueShelf20.Size = new System.Drawing.Size(245, 20);
            this.LueShelf20.TabIndex = 46;
            this.LueShelf20.ToolTip = "F4 : Show/hide list";
            this.LueShelf20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf20.EditValueChanged += new System.EventHandler(this.LueShelf20_EditValueChanged);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(288, 9);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 14);
            this.label71.TabIndex = 47;
            this.label71.Text = "Total";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal20
            // 
            this.TxtTotal20.EnterMoveNextControl = true;
            this.TxtTotal20.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal20.Name = "TxtTotal20";
            this.TxtTotal20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal20.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal20.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal20.TabIndex = 48;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(8, 9);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(26, 14);
            this.label72.TabIndex = 45;
            this.label72.Text = "Rak";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.LueItCode21);
            this.tabPage21.Controls.Add(this.Grd31);
            this.tabPage21.Controls.Add(this.panel27);
            this.tabPage21.Location = new System.Drawing.Point(4, 23);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Size = new System.Drawing.Size(764, 0);
            this.tabPage21.TabIndex = 21;
            this.tabPage21.Text = "No.21";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // LueItCode21
            // 
            this.LueItCode21.EnterMoveNextControl = true;
            this.LueItCode21.Location = new System.Drawing.Point(19, 53);
            this.LueItCode21.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode21.Name = "LueItCode21";
            this.LueItCode21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.Appearance.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode21.Properties.DropDownRows = 20;
            this.LueItCode21.Properties.NullText = "[Empty]";
            this.LueItCode21.Properties.PopupWidth = 500;
            this.LueItCode21.Size = new System.Drawing.Size(163, 20);
            this.LueItCode21.TabIndex = 58;
            this.LueItCode21.TabStop = false;
            this.LueItCode21.ToolTip = "F4 : Show/hide list";
            this.LueItCode21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode21.EditValueChanged += new System.EventHandler(this.LueItCode21_EditValueChanged);
            this.LueItCode21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode21_KeyDown);
            this.LueItCode21.Leave += new System.EventHandler(this.LueItCode21_Leave);
            // 
            // Grd31
            // 
            this.Grd31.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd31.DefaultRow.Height = 20;
            this.Grd31.DefaultRow.Sortable = false;
            this.Grd31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd31.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd31.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd31.Header.Height = 21;
            this.Grd31.Location = new System.Drawing.Point(0, 33);
            this.Grd31.Name = "Grd31";
            this.Grd31.RowHeader.Visible = true;
            this.Grd31.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd31.SingleClickEdit = true;
            this.Grd31.Size = new System.Drawing.Size(764, 0);
            this.Grd31.TabIndex = 57;
            this.Grd31.TreeCol = null;
            this.Grd31.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd31.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd31_ColHdrDoubleClick);
            this.Grd31.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd31_RequestEdit);
            this.Grd31.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd31_AfterCommitEdit);
            this.Grd31.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd31_CustomDrawCellForeground);
            this.Grd31.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd31_KeyDown);
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel27.Controls.Add(this.label73);
            this.panel27.Controls.Add(this.LueWhsCode21);
            this.panel27.Controls.Add(this.LueShelf21);
            this.panel27.Controls.Add(this.label74);
            this.panel27.Controls.Add(this.TxtTotal21);
            this.panel27.Controls.Add(this.label75);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(764, 33);
            this.panel27.TabIndex = 50;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(430, 9);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(69, 14);
            this.label73.TabIndex = 49;
            this.label73.Text = "Warehouse";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode21
            // 
            this.LueWhsCode21.EnterMoveNextControl = true;
            this.LueWhsCode21.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode21.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode21.Name = "LueWhsCode21";
            this.LueWhsCode21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode21.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode21.Properties.DropDownRows = 20;
            this.LueWhsCode21.Properties.NullText = "[Empty]";
            this.LueWhsCode21.Properties.PopupWidth = 500;
            this.LueWhsCode21.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode21.TabIndex = 50;
            this.LueWhsCode21.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode21.EditValueChanged += new System.EventHandler(this.LueWhsCode21_EditValueChanged);
            // 
            // LueShelf21
            // 
            this.LueShelf21.EnterMoveNextControl = true;
            this.LueShelf21.Location = new System.Drawing.Point(38, 6);
            this.LueShelf21.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf21.Name = "LueShelf21";
            this.LueShelf21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf21.Properties.Appearance.Options.UseFont = true;
            this.LueShelf21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf21.Properties.DropDownRows = 20;
            this.LueShelf21.Properties.NullText = "[Empty]";
            this.LueShelf21.Properties.PopupWidth = 500;
            this.LueShelf21.Size = new System.Drawing.Size(245, 20);
            this.LueShelf21.TabIndex = 46;
            this.LueShelf21.ToolTip = "F4 : Show/hide list";
            this.LueShelf21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf21.EditValueChanged += new System.EventHandler(this.LueShelf21_EditValueChanged);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(288, 9);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 14);
            this.label74.TabIndex = 47;
            this.label74.Text = "Total";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal21
            // 
            this.TxtTotal21.EnterMoveNextControl = true;
            this.TxtTotal21.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal21.Name = "TxtTotal21";
            this.TxtTotal21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal21.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal21.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal21.TabIndex = 48;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Red;
            this.label75.Location = new System.Drawing.Point(8, 9);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(26, 14);
            this.label75.TabIndex = 45;
            this.label75.Text = "Rak";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.LueItCode22);
            this.tabPage22.Controls.Add(this.Grd32);
            this.tabPage22.Controls.Add(this.panel28);
            this.tabPage22.Location = new System.Drawing.Point(4, 23);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Size = new System.Drawing.Size(764, 0);
            this.tabPage22.TabIndex = 22;
            this.tabPage22.Text = "No.22";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // LueItCode22
            // 
            this.LueItCode22.EnterMoveNextControl = true;
            this.LueItCode22.Location = new System.Drawing.Point(19, 53);
            this.LueItCode22.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode22.Name = "LueItCode22";
            this.LueItCode22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.Appearance.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode22.Properties.DropDownRows = 20;
            this.LueItCode22.Properties.NullText = "[Empty]";
            this.LueItCode22.Properties.PopupWidth = 500;
            this.LueItCode22.Size = new System.Drawing.Size(163, 20);
            this.LueItCode22.TabIndex = 59;
            this.LueItCode22.TabStop = false;
            this.LueItCode22.ToolTip = "F4 : Show/hide list";
            this.LueItCode22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode22.EditValueChanged += new System.EventHandler(this.LueItCode22_EditValueChanged);
            this.LueItCode22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode22_KeyDown);
            this.LueItCode22.Leave += new System.EventHandler(this.LueItCode22_Leave);
            // 
            // Grd32
            // 
            this.Grd32.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd32.DefaultRow.Height = 20;
            this.Grd32.DefaultRow.Sortable = false;
            this.Grd32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd32.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd32.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd32.Header.Height = 21;
            this.Grd32.Location = new System.Drawing.Point(0, 33);
            this.Grd32.Name = "Grd32";
            this.Grd32.RowHeader.Visible = true;
            this.Grd32.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd32.SingleClickEdit = true;
            this.Grd32.Size = new System.Drawing.Size(764, 0);
            this.Grd32.TabIndex = 58;
            this.Grd32.TreeCol = null;
            this.Grd32.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd32.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd32_ColHdrDoubleClick);
            this.Grd32.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd32_RequestEdit);
            this.Grd32.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd32_AfterCommitEdit);
            this.Grd32.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd32_CustomDrawCellForeground);
            this.Grd32.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd32_KeyDown);
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel28.Controls.Add(this.label76);
            this.panel28.Controls.Add(this.LueWhsCode22);
            this.panel28.Controls.Add(this.LueShelf22);
            this.panel28.Controls.Add(this.label77);
            this.panel28.Controls.Add(this.TxtTotal22);
            this.panel28.Controls.Add(this.label78);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(764, 33);
            this.panel28.TabIndex = 51;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Red;
            this.label76.Location = new System.Drawing.Point(430, 9);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(69, 14);
            this.label76.TabIndex = 49;
            this.label76.Text = "Warehouse";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode22
            // 
            this.LueWhsCode22.EnterMoveNextControl = true;
            this.LueWhsCode22.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode22.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode22.Name = "LueWhsCode22";
            this.LueWhsCode22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode22.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode22.Properties.DropDownRows = 20;
            this.LueWhsCode22.Properties.NullText = "[Empty]";
            this.LueWhsCode22.Properties.PopupWidth = 500;
            this.LueWhsCode22.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode22.TabIndex = 50;
            this.LueWhsCode22.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode22.EditValueChanged += new System.EventHandler(this.LueWhsCode22_EditValueChanged);
            // 
            // LueShelf22
            // 
            this.LueShelf22.EnterMoveNextControl = true;
            this.LueShelf22.Location = new System.Drawing.Point(38, 6);
            this.LueShelf22.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf22.Name = "LueShelf22";
            this.LueShelf22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf22.Properties.Appearance.Options.UseFont = true;
            this.LueShelf22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf22.Properties.DropDownRows = 20;
            this.LueShelf22.Properties.NullText = "[Empty]";
            this.LueShelf22.Properties.PopupWidth = 500;
            this.LueShelf22.Size = new System.Drawing.Size(245, 20);
            this.LueShelf22.TabIndex = 46;
            this.LueShelf22.ToolTip = "F4 : Show/hide list";
            this.LueShelf22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf22.EditValueChanged += new System.EventHandler(this.LueShelf22_EditValueChanged);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(288, 9);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 14);
            this.label77.TabIndex = 47;
            this.label77.Text = "Total";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal22
            // 
            this.TxtTotal22.EnterMoveNextControl = true;
            this.TxtTotal22.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal22.Name = "TxtTotal22";
            this.TxtTotal22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal22.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal22.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal22.TabIndex = 48;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(8, 9);
            this.label78.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(26, 14);
            this.label78.TabIndex = 45;
            this.label78.Text = "Rak";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.LueItCode23);
            this.tabPage23.Controls.Add(this.Grd33);
            this.tabPage23.Controls.Add(this.panel29);
            this.tabPage23.Location = new System.Drawing.Point(4, 23);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Size = new System.Drawing.Size(764, 0);
            this.tabPage23.TabIndex = 23;
            this.tabPage23.Text = "No.23";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // LueItCode23
            // 
            this.LueItCode23.EnterMoveNextControl = true;
            this.LueItCode23.Location = new System.Drawing.Point(19, 53);
            this.LueItCode23.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode23.Name = "LueItCode23";
            this.LueItCode23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.Appearance.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode23.Properties.DropDownRows = 20;
            this.LueItCode23.Properties.NullText = "[Empty]";
            this.LueItCode23.Properties.PopupWidth = 500;
            this.LueItCode23.Size = new System.Drawing.Size(163, 20);
            this.LueItCode23.TabIndex = 60;
            this.LueItCode23.TabStop = false;
            this.LueItCode23.ToolTip = "F4 : Show/hide list";
            this.LueItCode23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode23.EditValueChanged += new System.EventHandler(this.LueItCode23_EditValueChanged);
            this.LueItCode23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode23_KeyDown);
            this.LueItCode23.Leave += new System.EventHandler(this.LueItCode23_Leave);
            // 
            // Grd33
            // 
            this.Grd33.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd33.DefaultRow.Height = 20;
            this.Grd33.DefaultRow.Sortable = false;
            this.Grd33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd33.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd33.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd33.Header.Height = 21;
            this.Grd33.Location = new System.Drawing.Point(0, 33);
            this.Grd33.Name = "Grd33";
            this.Grd33.RowHeader.Visible = true;
            this.Grd33.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd33.SingleClickEdit = true;
            this.Grd33.Size = new System.Drawing.Size(764, 0);
            this.Grd33.TabIndex = 59;
            this.Grd33.TreeCol = null;
            this.Grd33.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd33.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd33_ColHdrDoubleClick);
            this.Grd33.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd33_RequestEdit);
            this.Grd33.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd33_AfterCommitEdit);
            this.Grd33.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd33_CustomDrawCellForeground);
            this.Grd33.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd33_KeyDown);
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel29.Controls.Add(this.label79);
            this.panel29.Controls.Add(this.LueWhsCode23);
            this.panel29.Controls.Add(this.LueShelf23);
            this.panel29.Controls.Add(this.label80);
            this.panel29.Controls.Add(this.TxtTotal23);
            this.panel29.Controls.Add(this.label81);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(764, 33);
            this.panel29.TabIndex = 52;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Red;
            this.label79.Location = new System.Drawing.Point(430, 9);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(69, 14);
            this.label79.TabIndex = 49;
            this.label79.Text = "Warehouse";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode23
            // 
            this.LueWhsCode23.EnterMoveNextControl = true;
            this.LueWhsCode23.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode23.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode23.Name = "LueWhsCode23";
            this.LueWhsCode23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode23.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode23.Properties.DropDownRows = 20;
            this.LueWhsCode23.Properties.NullText = "[Empty]";
            this.LueWhsCode23.Properties.PopupWidth = 500;
            this.LueWhsCode23.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode23.TabIndex = 50;
            this.LueWhsCode23.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode23.EditValueChanged += new System.EventHandler(this.LueWhsCode23_EditValueChanged);
            // 
            // LueShelf23
            // 
            this.LueShelf23.EnterMoveNextControl = true;
            this.LueShelf23.Location = new System.Drawing.Point(38, 6);
            this.LueShelf23.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf23.Name = "LueShelf23";
            this.LueShelf23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf23.Properties.Appearance.Options.UseFont = true;
            this.LueShelf23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf23.Properties.DropDownRows = 20;
            this.LueShelf23.Properties.NullText = "[Empty]";
            this.LueShelf23.Properties.PopupWidth = 500;
            this.LueShelf23.Size = new System.Drawing.Size(245, 20);
            this.LueShelf23.TabIndex = 46;
            this.LueShelf23.ToolTip = "F4 : Show/hide list";
            this.LueShelf23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf23.EditValueChanged += new System.EventHandler(this.LueShelf23_EditValueChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(288, 9);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(35, 14);
            this.label80.TabIndex = 47;
            this.label80.Text = "Total";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal23
            // 
            this.TxtTotal23.EnterMoveNextControl = true;
            this.TxtTotal23.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal23.Name = "TxtTotal23";
            this.TxtTotal23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal23.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal23.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal23.TabIndex = 48;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Red;
            this.label81.Location = new System.Drawing.Point(8, 9);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(26, 14);
            this.label81.TabIndex = 45;
            this.label81.Text = "Rak";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage24
            // 
            this.tabPage24.Controls.Add(this.LueItCode24);
            this.tabPage24.Controls.Add(this.Grd34);
            this.tabPage24.Controls.Add(this.panel30);
            this.tabPage24.Location = new System.Drawing.Point(4, 23);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Size = new System.Drawing.Size(764, 0);
            this.tabPage24.TabIndex = 24;
            this.tabPage24.Text = "No.24";
            this.tabPage24.UseVisualStyleBackColor = true;
            // 
            // LueItCode24
            // 
            this.LueItCode24.EnterMoveNextControl = true;
            this.LueItCode24.Location = new System.Drawing.Point(19, 53);
            this.LueItCode24.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode24.Name = "LueItCode24";
            this.LueItCode24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.Appearance.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode24.Properties.DropDownRows = 20;
            this.LueItCode24.Properties.NullText = "[Empty]";
            this.LueItCode24.Properties.PopupWidth = 500;
            this.LueItCode24.Size = new System.Drawing.Size(163, 20);
            this.LueItCode24.TabIndex = 61;
            this.LueItCode24.TabStop = false;
            this.LueItCode24.ToolTip = "F4 : Show/hide list";
            this.LueItCode24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode24.EditValueChanged += new System.EventHandler(this.LueItCode24_EditValueChanged);
            this.LueItCode24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode24_KeyDown);
            this.LueItCode24.Leave += new System.EventHandler(this.LueItCode24_Leave);
            // 
            // Grd34
            // 
            this.Grd34.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd34.DefaultRow.Height = 20;
            this.Grd34.DefaultRow.Sortable = false;
            this.Grd34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd34.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd34.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd34.Header.Height = 21;
            this.Grd34.Location = new System.Drawing.Point(0, 33);
            this.Grd34.Name = "Grd34";
            this.Grd34.RowHeader.Visible = true;
            this.Grd34.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd34.SingleClickEdit = true;
            this.Grd34.Size = new System.Drawing.Size(764, 0);
            this.Grd34.TabIndex = 60;
            this.Grd34.TreeCol = null;
            this.Grd34.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd34.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd34_ColHdrDoubleClick);
            this.Grd34.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd34_RequestEdit);
            this.Grd34.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd34_AfterCommitEdit);
            this.Grd34.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd34_CustomDrawCellForeground);
            this.Grd34.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd34_KeyDown);
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel30.Controls.Add(this.label82);
            this.panel30.Controls.Add(this.LueWhsCode24);
            this.panel30.Controls.Add(this.LueShelf24);
            this.panel30.Controls.Add(this.label83);
            this.panel30.Controls.Add(this.TxtTotal24);
            this.panel30.Controls.Add(this.label84);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(764, 33);
            this.panel30.TabIndex = 53;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Red;
            this.label82.Location = new System.Drawing.Point(430, 9);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(69, 14);
            this.label82.TabIndex = 49;
            this.label82.Text = "Warehouse";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode24
            // 
            this.LueWhsCode24.EnterMoveNextControl = true;
            this.LueWhsCode24.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode24.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode24.Name = "LueWhsCode24";
            this.LueWhsCode24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode24.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode24.Properties.DropDownRows = 20;
            this.LueWhsCode24.Properties.NullText = "[Empty]";
            this.LueWhsCode24.Properties.PopupWidth = 500;
            this.LueWhsCode24.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode24.TabIndex = 50;
            this.LueWhsCode24.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode24.EditValueChanged += new System.EventHandler(this.LueWhsCode24_EditValueChanged);
            // 
            // LueShelf24
            // 
            this.LueShelf24.EnterMoveNextControl = true;
            this.LueShelf24.Location = new System.Drawing.Point(38, 6);
            this.LueShelf24.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf24.Name = "LueShelf24";
            this.LueShelf24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf24.Properties.Appearance.Options.UseFont = true;
            this.LueShelf24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf24.Properties.DropDownRows = 20;
            this.LueShelf24.Properties.NullText = "[Empty]";
            this.LueShelf24.Properties.PopupWidth = 500;
            this.LueShelf24.Size = new System.Drawing.Size(245, 20);
            this.LueShelf24.TabIndex = 46;
            this.LueShelf24.ToolTip = "F4 : Show/hide list";
            this.LueShelf24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf24.EditValueChanged += new System.EventHandler(this.LueShelf24_EditValueChanged);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(288, 9);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 14);
            this.label83.TabIndex = 47;
            this.label83.Text = "Total";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal24
            // 
            this.TxtTotal24.EnterMoveNextControl = true;
            this.TxtTotal24.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal24.Name = "TxtTotal24";
            this.TxtTotal24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal24.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal24.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal24.TabIndex = 48;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Red;
            this.label84.Location = new System.Drawing.Point(8, 9);
            this.label84.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(26, 14);
            this.label84.TabIndex = 45;
            this.label84.Text = "Rak";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.LueItCode25);
            this.tabPage25.Controls.Add(this.Grd35);
            this.tabPage25.Controls.Add(this.panel31);
            this.tabPage25.Location = new System.Drawing.Point(4, 23);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Size = new System.Drawing.Size(764, 0);
            this.tabPage25.TabIndex = 25;
            this.tabPage25.Text = "No.25";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // LueItCode25
            // 
            this.LueItCode25.EnterMoveNextControl = true;
            this.LueItCode25.Location = new System.Drawing.Point(19, 53);
            this.LueItCode25.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode25.Name = "LueItCode25";
            this.LueItCode25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.Appearance.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode25.Properties.DropDownRows = 20;
            this.LueItCode25.Properties.NullText = "[Empty]";
            this.LueItCode25.Properties.PopupWidth = 500;
            this.LueItCode25.Size = new System.Drawing.Size(163, 20);
            this.LueItCode25.TabIndex = 62;
            this.LueItCode25.TabStop = false;
            this.LueItCode25.ToolTip = "F4 : Show/hide list";
            this.LueItCode25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode25.EditValueChanged += new System.EventHandler(this.LueItCode25_EditValueChanged);
            this.LueItCode25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode25_KeyDown);
            this.LueItCode25.Leave += new System.EventHandler(this.LueItCode25_Leave);
            // 
            // Grd35
            // 
            this.Grd35.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd35.DefaultRow.Height = 20;
            this.Grd35.DefaultRow.Sortable = false;
            this.Grd35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd35.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd35.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd35.Header.Height = 21;
            this.Grd35.Location = new System.Drawing.Point(0, 33);
            this.Grd35.Name = "Grd35";
            this.Grd35.RowHeader.Visible = true;
            this.Grd35.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd35.SingleClickEdit = true;
            this.Grd35.Size = new System.Drawing.Size(764, 0);
            this.Grd35.TabIndex = 61;
            this.Grd35.TreeCol = null;
            this.Grd35.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd35.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd35_ColHdrDoubleClick);
            this.Grd35.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd35_RequestEdit);
            this.Grd35.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd35_AfterCommitEdit);
            this.Grd35.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd35_CustomDrawCellForeground);
            this.Grd35.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd35_KeyDown);
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel31.Controls.Add(this.label85);
            this.panel31.Controls.Add(this.LueWhsCode25);
            this.panel31.Controls.Add(this.LueShelf25);
            this.panel31.Controls.Add(this.label86);
            this.panel31.Controls.Add(this.TxtTotal25);
            this.panel31.Controls.Add(this.label87);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(764, 33);
            this.panel31.TabIndex = 54;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Red;
            this.label85.Location = new System.Drawing.Point(430, 9);
            this.label85.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(69, 14);
            this.label85.TabIndex = 49;
            this.label85.Text = "Warehouse";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode25
            // 
            this.LueWhsCode25.EnterMoveNextControl = true;
            this.LueWhsCode25.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode25.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode25.Name = "LueWhsCode25";
            this.LueWhsCode25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode25.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode25.Properties.DropDownRows = 20;
            this.LueWhsCode25.Properties.NullText = "[Empty]";
            this.LueWhsCode25.Properties.PopupWidth = 500;
            this.LueWhsCode25.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode25.TabIndex = 50;
            this.LueWhsCode25.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode25.EditValueChanged += new System.EventHandler(this.LueWhsCode25_EditValueChanged);
            // 
            // LueShelf25
            // 
            this.LueShelf25.EnterMoveNextControl = true;
            this.LueShelf25.Location = new System.Drawing.Point(38, 6);
            this.LueShelf25.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf25.Name = "LueShelf25";
            this.LueShelf25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf25.Properties.Appearance.Options.UseFont = true;
            this.LueShelf25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf25.Properties.DropDownRows = 20;
            this.LueShelf25.Properties.NullText = "[Empty]";
            this.LueShelf25.Properties.PopupWidth = 500;
            this.LueShelf25.Size = new System.Drawing.Size(245, 20);
            this.LueShelf25.TabIndex = 46;
            this.LueShelf25.ToolTip = "F4 : Show/hide list";
            this.LueShelf25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf25.EditValueChanged += new System.EventHandler(this.LueShelf25_EditValueChanged);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(288, 9);
            this.label86.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(35, 14);
            this.label86.TabIndex = 47;
            this.label86.Text = "Total";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal25
            // 
            this.TxtTotal25.EnterMoveNextControl = true;
            this.TxtTotal25.Location = new System.Drawing.Point(325, 6);
            this.TxtTotal25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal25.Name = "TxtTotal25";
            this.TxtTotal25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal25.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal25.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal25.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal25.TabIndex = 48;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Red;
            this.label87.Location = new System.Drawing.Point(8, 9);
            this.label87.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(26, 14);
            this.label87.TabIndex = 45;
            this.label87.Text = "Rak";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPageReturn
            // 
            this.tabPageReturn.Controls.Add(this.LueLengthCode);
            this.tabPageReturn.Controls.Add(this.GrdReturn);
            this.tabPageReturn.Location = new System.Drawing.Point(4, 23);
            this.tabPageReturn.Name = "tabPageReturn";
            this.tabPageReturn.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReturn.Size = new System.Drawing.Size(764, 0);
            this.tabPageReturn.TabIndex = 15;
            this.tabPageReturn.Text = "Return";
            this.tabPageReturn.UseVisualStyleBackColor = true;
            // 
            // LueLengthCode
            // 
            this.LueLengthCode.EnterMoveNextControl = true;
            this.LueLengthCode.Location = new System.Drawing.Point(94, 30);
            this.LueLengthCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLengthCode.Name = "LueLengthCode";
            this.LueLengthCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthCode.Properties.Appearance.Options.UseFont = true;
            this.LueLengthCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLengthCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLengthCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLengthCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLengthCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLengthCode.Properties.DropDownRows = 20;
            this.LueLengthCode.Properties.NullText = "[Empty]";
            this.LueLengthCode.Properties.PopupWidth = 500;
            this.LueLengthCode.Size = new System.Drawing.Size(171, 20);
            this.LueLengthCode.TabIndex = 53;
            this.LueLengthCode.ToolTip = "F4 : Show/hide list";
            this.LueLengthCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLengthCode.EditValueChanged += new System.EventHandler(this.LueLengthCode_EditValueChanged);
            this.LueLengthCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLengthCode_KeyDown);
            this.LueLengthCode.Leave += new System.EventHandler(this.LueLengthCode_Leave);
            // 
            // GrdReturn
            // 
            this.GrdReturn.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.GrdReturn.DefaultRow.Height = 20;
            this.GrdReturn.DefaultRow.Sortable = false;
            this.GrdReturn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdReturn.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GrdReturn.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.GrdReturn.Header.Height = 21;
            this.GrdReturn.Location = new System.Drawing.Point(3, 3);
            this.GrdReturn.Name = "GrdReturn";
            this.GrdReturn.RowHeader.Visible = true;
            this.GrdReturn.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.GrdReturn.SingleClickEdit = true;
            this.GrdReturn.Size = new System.Drawing.Size(758, 0);
            this.GrdReturn.TabIndex = 52;
            this.GrdReturn.TreeCol = null;
            this.GrdReturn.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.GrdReturn.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.GrdReturn_RequestEdit);
            this.GrdReturn.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdReturn_AfterCommitEdit);
            this.GrdReturn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdReturn_KeyDown);
            // 
            // FrmRecvRawMaterial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRecvRawMaterial";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode2.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreateBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeUnloadStartTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUnloadStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeUnloadEndTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLegalDocVerifyDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQueueNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal16.Properties)).EndInit();
            this.tabPage17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd27)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal17.Properties)).EndInit();
            this.tabPage18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd28)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal18.Properties)).EndInit();
            this.tabPage19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd29)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal19.Properties)).EndInit();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd30)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal20.Properties)).EndInit();
            this.tabPage21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd31)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal21.Properties)).EndInit();
            this.tabPage22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd32)).EndInit();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal22.Properties)).EndInit();
            this.tabPage23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd33)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal23.Properties)).EndInit();
            this.tabPage24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd34)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal24.Properties)).EndInit();
            this.tabPage25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd35)).EndInit();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal25.Properties)).EndInit();
            this.tabPageReturn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueLengthCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdReturn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueQueueNo;
        internal DevExpress.XtraEditors.TextEdit TxtLegalDocVerifyDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteUnloadEndDt;
        internal DevExpress.XtraEditors.DateEdit DteUnloadStartDt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TimeEdit TmeUnloadEndTm;
        internal DevExpress.XtraEditors.TimeEdit TmeUnloadStartTm;
        protected System.Windows.Forms.Panel panel6;
        protected System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        protected System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        protected System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        protected System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label19;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        protected System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label21;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        protected System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label23;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        protected System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label25;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        protected System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label27;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        protected System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label29;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        protected System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode4;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode3;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode2;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode1;
        private DevExpress.XtraEditors.LookUpEdit LueItCode3;
        private DevExpress.XtraEditors.LookUpEdit LueItCode4;
        private DevExpress.XtraEditors.LookUpEdit LueItCode5;
        private DevExpress.XtraEditors.LookUpEdit LueItCode7;
        private DevExpress.XtraEditors.LookUpEdit LueItCode8;
        private DevExpress.XtraEditors.LookUpEdit LueItCode9;
        private DevExpress.XtraEditors.LookUpEdit LueItCode10;
        internal DevExpress.XtraEditors.TextEdit TxtTTCode;
        private DevExpress.XtraEditors.LookUpEdit LueItCode2;
        private DevExpress.XtraEditors.LookUpEdit LueItCode6;
        private DevExpress.XtraEditors.LookUpEdit LueItCode1;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtTotal1;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtTotal4;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtTotal5;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtTotal6;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtTotal7;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtTotal8;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtTotal9;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtTotal10;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private DevExpress.XtraEditors.LookUpEdit LueShelf1;
        private DevExpress.XtraEditors.LookUpEdit LueShelf2;
        private DevExpress.XtraEditors.LookUpEdit LueShelf3;
        private DevExpress.XtraEditors.LookUpEdit LueShelf4;
        private DevExpress.XtraEditors.LookUpEdit LueShelf5;
        private DevExpress.XtraEditors.LookUpEdit LueShelf6;
        private DevExpress.XtraEditors.LookUpEdit LueShelf7;
        private DevExpress.XtraEditors.LookUpEdit LueShelf8;
        private DevExpress.XtraEditors.LookUpEdit LueShelf9;
        private DevExpress.XtraEditors.LookUpEdit LueShelf10;
        private System.Windows.Forms.TabPage tabPage11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd21;
        protected System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.LookUpEdit LueShelf11;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtTotal11;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage15;
        private DevExpress.XtraEditors.LookUpEdit LueItCode11;
        private DevExpress.XtraEditors.LookUpEdit LueItCode12;
        protected internal TenTec.Windows.iGridLib.iGrid Grd22;
        protected System.Windows.Forms.Panel panel18;
        private DevExpress.XtraEditors.LookUpEdit LueShelf12;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtTotal12;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.LookUpEdit LueItCode13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd23;
        protected System.Windows.Forms.Panel panel19;
        private DevExpress.XtraEditors.LookUpEdit LueShelf13;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtTotal13;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.LookUpEdit LueItCode14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd24;
        protected System.Windows.Forms.Panel panel20;
        private DevExpress.XtraEditors.LookUpEdit LueShelf14;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtTotal14;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LookUpEdit LueItCode15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd25;
        protected System.Windows.Forms.Panel panel21;
        private DevExpress.XtraEditors.LookUpEdit LueShelf15;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtTotal15;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtCreateBy;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode2;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode1;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode2;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode3;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode4;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode5;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode6;
        private System.Windows.Forms.Label label49;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode7;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode8;
        private System.Windows.Forms.Label label51;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode9;
        private System.Windows.Forms.Label label52;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode10;
        private System.Windows.Forms.Label label53;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode11;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode12;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode13;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode14;
        private System.Windows.Forms.Label label57;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode15;
        private System.Windows.Forms.TabPage tabPageReturn;
        protected internal TenTec.Windows.iGridLib.iGrid GrdReturn;
        private DevExpress.XtraEditors.LookUpEdit LueLengthCode;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.TabPage tabPage25;
        private DevExpress.XtraEditors.LookUpEdit LueItCode16;
        protected internal TenTec.Windows.iGridLib.iGrid Grd26;
        protected System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label58;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode16;
        private DevExpress.XtraEditors.LookUpEdit LueShelf16;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.TextEdit TxtTotal16;
        private System.Windows.Forms.Label label60;
        private DevExpress.XtraEditors.LookUpEdit LueItCode17;
        protected internal TenTec.Windows.iGridLib.iGrid Grd27;
        protected System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode17;
        private DevExpress.XtraEditors.LookUpEdit LueShelf17;
        private System.Windows.Forms.Label label62;
        internal DevExpress.XtraEditors.TextEdit TxtTotal17;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.LookUpEdit LueItCode18;
        protected internal TenTec.Windows.iGridLib.iGrid Grd28;
        protected System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label64;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode18;
        private DevExpress.XtraEditors.LookUpEdit LueShelf18;
        private System.Windows.Forms.Label label65;
        internal DevExpress.XtraEditors.TextEdit TxtTotal18;
        private System.Windows.Forms.Label label66;
        private DevExpress.XtraEditors.LookUpEdit LueItCode19;
        protected internal TenTec.Windows.iGridLib.iGrid Grd29;
        protected System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label67;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode19;
        private DevExpress.XtraEditors.LookUpEdit LueShelf19;
        private System.Windows.Forms.Label label68;
        internal DevExpress.XtraEditors.TextEdit TxtTotal19;
        private System.Windows.Forms.Label label69;
        private DevExpress.XtraEditors.LookUpEdit LueItCode20;
        protected internal TenTec.Windows.iGridLib.iGrid Grd30;
        protected System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label70;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode20;
        private DevExpress.XtraEditors.LookUpEdit LueShelf20;
        private System.Windows.Forms.Label label71;
        internal DevExpress.XtraEditors.TextEdit TxtTotal20;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraEditors.LookUpEdit LueItCode21;
        protected internal TenTec.Windows.iGridLib.iGrid Grd31;
        protected System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label73;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode21;
        private DevExpress.XtraEditors.LookUpEdit LueShelf21;
        private System.Windows.Forms.Label label74;
        internal DevExpress.XtraEditors.TextEdit TxtTotal21;
        private System.Windows.Forms.Label label75;
        private DevExpress.XtraEditors.LookUpEdit LueItCode22;
        protected internal TenTec.Windows.iGridLib.iGrid Grd32;
        protected System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label76;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode22;
        private DevExpress.XtraEditors.LookUpEdit LueShelf22;
        private System.Windows.Forms.Label label77;
        internal DevExpress.XtraEditors.TextEdit TxtTotal22;
        private System.Windows.Forms.Label label78;
        private DevExpress.XtraEditors.LookUpEdit LueItCode23;
        protected internal TenTec.Windows.iGridLib.iGrid Grd33;
        protected System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label label79;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode23;
        private DevExpress.XtraEditors.LookUpEdit LueShelf23;
        private System.Windows.Forms.Label label80;
        internal DevExpress.XtraEditors.TextEdit TxtTotal23;
        private System.Windows.Forms.Label label81;
        private DevExpress.XtraEditors.LookUpEdit LueItCode24;
        protected internal TenTec.Windows.iGridLib.iGrid Grd34;
        protected System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label82;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode24;
        private DevExpress.XtraEditors.LookUpEdit LueShelf24;
        private System.Windows.Forms.Label label83;
        internal DevExpress.XtraEditors.TextEdit TxtTotal24;
        private System.Windows.Forms.Label label84;
        private DevExpress.XtraEditors.LookUpEdit LueItCode25;
        protected internal TenTec.Windows.iGridLib.iGrid Grd35;
        protected System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label85;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode25;
        private DevExpress.XtraEditors.LookUpEdit LueShelf25;
        private System.Windows.Forms.Label label86;
        internal DevExpress.XtraEditors.TextEdit TxtTotal25;
        private System.Windows.Forms.Label label87;
    }
}