﻿#region Update
/* 
    04/04/2017 [TKG] Perubahan perhitungan proses annual leave
    02/09/2017 [TKG] Perhitungan cuti tahunan berdasarkan employee's initial date.
    12/09/2017 [TKG] tambah long service leave
    28/10/2017 [TKG] perhitungan cuti besar utk HIN
    28/01/2018 [TKG] tambah info dan filter payrun period
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAnnualLeave : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mLeaveCode = string.Empty,
            mRLPType = string.Empty,
            mAnnualLeaveCode = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty;
            
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;
        
        #endregion

        #region Constructor

        public FrmRptAnnualLeave(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueLeaveCode(ref LueLeaveCode, mAnnualLeaveCode);
                if (mLongServiceLeaveCode.Length==0)
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueLeaveCode }, true);
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Leave",
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's"+Environment.NewLine+"Name",
                        "Old Code",
                        "Department",

                        //6-10
                        "Position",
                        "Payrun"+Environment.NewLine+"Period",
                        "Permanent Date",
                        "Year",
                        "Number"+Environment.NewLine+"of Days",
                        
                        //11-15
                        "Balance",
                        "January",
                        "February",
                        "March",
                        "April",
                        
                        //16-20
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        
                        //21-23
                        "October",
                        "November",
                        "December"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 150, 200, 100, 150, 
                        
                        //6-10
                        150, 150, 100, 80, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 100, 
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-23
                        100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, false);
            if (mLongServiceLeaveCode.Length==0) Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            if (mRLPType == "1" || mRLPType == "3")
                Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, !ChkHideInfoInGrd.Checked);
            if (mRLPType == "2")
                Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueLeaveCode, "Leave")) return;

            var lEmpAnnualLeave = new List<EmpAnnualLeave>();
            var lEmployeeResidualLeave = new List<EmployeeResidualLeave>();
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lEmpAnnualLeave, ref lEmployeeResidualLeave);
                if (lEmpAnnualLeave.Count > 0)
                {
                    if (Sm.CompareStr(LeaveCode, mAnnualLeaveCode))
                    {
                        //Annual Leave 
                        Sm.GetEmpAnnualLeave(ref lEmployeeResidualLeave, Sm.GetLue(LueYr));
                        Process2(ref lEmpAnnualLeave, ref lEmployeeResidualLeave);
                        if (mRLPType == "1" || mRLPType == "3") Process3(ref lEmpAnnualLeave); // karena perhitungan tiap karyawan per bulannya beda-beda.
                    }

                    if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode))
                    {
                        // Long Service Leave
                        Sm.GetEmpLongServiceLeave(ref lEmployeeResidualLeave, Sm.GetLue(LueYr));
                        Process2(ref lEmpAnnualLeave, ref lEmployeeResidualLeave);
                    }

                    if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode2))
                    {
                        // Long Service Leave
                        Sm.GetEmpLongServiceLeave2(ref lEmployeeResidualLeave, Sm.GetLue(LueYr));
                        Process2(ref lEmpAnnualLeave, ref lEmployeeResidualLeave);
                    }

                    Process4(ref lEmpAnnualLeave);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                lEmpAnnualLeave.Clear();
                lEmployeeResidualLeave.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void GetParameter()
        {
            mLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mRLPType = Sm.GetParameter("RLPType"); //1=IOK 2=KMI 3=HIN
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetLueLeaveCode(ref LookUpEdit Lue, string LeaveCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LeaveCode As Col1, LeaveName As Col2 ");
            SQL.AppendLine("From TblLeave ");
            SQL.AppendLine("Where (LeaveCode=@LeaveCode1 ");
            if (mLongServiceLeaveCode.Length > 0)
                SQL.AppendLine("Or LeaveCode=@LeaveCode2 ");
            if (mLongServiceLeaveCode2.Length > 0)
                SQL.AppendLine("Or LeaveCode=@LeaveCode3 ");
            SQL.AppendLine(") Order By LeaveName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@LeaveCode1", mAnnualLeaveCode);
            if (mLongServiceLeaveCode.Length > 0)
                Sm.CmParam<String>(ref cm, "@LeaveCode2", mLongServiceLeaveCode);
            if (mLongServiceLeaveCode2.Length>0)
                Sm.CmParam<String>(ref cm, "@LeaveCode3", mLongServiceLeaveCode2);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (LeaveCode.Length > 0) Sm.SetLue(LueLeaveCode, LeaveCode);
        }

        private void Process1(ref List<EmpAnnualLeave> l, ref List<EmployeeResidualLeave> l2)
        {
            string Filter = " ";
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DeptName, C.PosName, E.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("Case When IfNull(D.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, A.JoinDt) ");
            SQL.AppendLine("Else A.JoinDt ");
            SQL.AppendLine("End As JoinDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblParameter D On D.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Left Join TblOption E On A.PayrunPeriod=E.OptCode And E.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Where ((ResignDt Is Not Null And ResignDt>=@Dt) Or ResignDt Is Null) ");
            
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "A.PayrunPeriod", true);
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName" });
            Sm.CmParam<String>(ref cm, "@Dt", Sm.Left(Sm.ServerCurrentDateTime(), 8));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + Filter + " Order By A.EmpCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName", "EmpCodeOld", "DeptName", "PosName", "PayrunPeriodDesc", 
                    
                    //6
                    "JoinDt"
                });
                if (dr.HasRows)
                {
                    var EmpCodeTemp = string.Empty;
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        l.Add(new EmpAnnualLeave()
                        {
                            EmpCode = EmpCodeTemp,
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            DeptName = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            PayrunPeriod = Sm.DrStr(dr, c[5]),
                            JoinDt = Sm.DrStr(dr, c[6]),
                            Mth01 = 0m,
                            Mth02 = 0m,
                            Mth03 = 0m,
                            Mth04 = 0m,
                            Mth05 = 0m,
                            Mth06 = 0m,
                            Mth07 = 0m,
                            Mth08 = 0m,
                            Mth09 = 0m,
                            Mth10 = 0m,
                            Mth11 = 0m,
                            Mth12 = 0m
                        });

                        l2.Add(new EmployeeResidualLeave(){ EmpCode = EmpCodeTemp });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<EmpAnnualLeave> l, ref List<EmployeeResidualLeave> l2)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (Sm.CompareStr(l[i].EmpCode, l2[i].EmpCode))
                {
                    l[i].LeaveDay = l2[i].LeaveDay;
                    l[i].RemainingDay = l2[i].RemainingDay;
                }
            }
        }

        private void Process3(ref List<EmpAnnualLeave> l)
        {
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), l[i].EmpCode);
            }

            Filter = " Where (" + Filter + ") ";

            SQL.AppendLine("Select A.EmpCode, B.Mth, IfNull(C.CountDays, 0)+IfNull(D.CountDays, 0) As CountDays ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select '01' As Mth Union All ");
            SQL.AppendLine("    Select '02' As Mth Union All ");
            SQL.AppendLine("    Select '03' As Mth Union All ");
            SQL.AppendLine("    Select '04' As Mth Union All ");
            SQL.AppendLine("    Select '05' As Mth Union All ");
            SQL.AppendLine("    Select '06' As Mth Union All ");
            SQL.AppendLine("    Select '07' As Mth Union All ");
            SQL.AppendLine("    Select '08' As Mth Union All ");
            SQL.AppendLine("    Select '09' As Mth Union All ");
            SQL.AppendLine("    Select '10' As Mth Union All ");
            SQL.AppendLine("    Select '11' As Mth Union All ");
            SQL.AppendLine("    Select '12' As Mth ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, Substring(T2.LeaveDt, 5, 2) As Mth, Count(T2.LeaveDt) As CountDays ");
            SQL.AppendLine("    From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And Left(T2.LeaveDt, 4)=@Yr ");
            SQL.AppendLine(Filter.Replace("A.", "T1."));
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And T1.LeaveCode=@LeaveCode ");
            SQL.AppendLine("    Group By T1.EmpCode, Substring(T2.LeaveDt, 5, 2) ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode And B.Mth=C.Mth ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Substring(T3.LeaveDt, 5, 2) As Mth, Count(T3.LeaveDt) As CountDays ");
            SQL.AppendLine("    From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine((Filter.Replace("A.", "T2.")).Replace(" Where ", " And "));
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 T3 On T1.DocNo=T3.DocNo And Left(T3.LeaveDt, 4)=@Yr ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And T1.LeaveCode=@LeaveCode ");
            SQL.AppendLine("    Group By T2.EmpCode, Substring(T3.LeaveDt, 5, 2) ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode And B.Mth=D.Mth ");
            SQL.AppendLine(Filter);
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(" Order By A.EmpCode, B.Mth;");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@LeaveCode", mLeaveCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "EmpCode", "Mth", "CountDays" });
                if (dr.HasRows)
                {
                    var EmpCodeTemp = string.Empty;
                    var Mth = string.Empty;
                    var CountDays = 0m;
                    int Temp = 0;
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        Mth = Sm.DrStr(dr, c[1]);
                        CountDays = Sm.DrDec(dr, c[2]);
                        for (int i = Temp; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCodeTemp))
                            {
                                switch (Mth)
                                {
                                    case "01":
                                        l[i].Mth01 += CountDays;
                                        break;
                                    case "02":
                                        l[i].Mth02 += CountDays;
                                        break;
                                    case "03":
                                        l[i].Mth03 += CountDays;
                                        break;
                                    case "04":
                                        l[i].Mth04 += CountDays;
                                        break;
                                    case "05":
                                        l[i].Mth05 += CountDays;
                                        break;
                                    case "06":
                                        l[i].Mth06 += CountDays;
                                        break;
                                    case "07":
                                        l[i].Mth07 += CountDays;
                                        break;
                                    case "08":
                                        l[i].Mth08 += CountDays;
                                        break;
                                    case "09":
                                        l[i].Mth09 += CountDays;
                                        break;
                                    case "10":
                                        l[i].Mth10 += CountDays;
                                        break;
                                    case "11":
                                        l[i].Mth11 += CountDays;
                                        break;
                                    case "12":
                                        l[i].Mth12 += CountDays;
                                        break;
                                }
                                Temp = i;
                                break;
                            }
                        }
                        
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<EmpAnnualLeave> l)
        {
            var Yr = Sm.GetLue(LueYr);
            var LeaveName = LueLeaveCode.GetColumnValue("Col2");
            iGRow r;

            Grd1.BeginUpdate();

            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = LeaveName;
                r.Cells[2].Value = l[i].EmpCode;
                r.Cells[3].Value = l[i].EmpName;
                r.Cells[4].Value = l[i].EmpCodeOld;
                r.Cells[5].Value = l[i].DeptName;
                r.Cells[6].Value = l[i].PosName;
                r.Cells[7].Value = l[i].PayrunPeriod;
                r.Cells[8].Value = Sm.ConvertDate(l[i].JoinDt);
                r.Cells[9].Value = Yr;
                r.Cells[10].Value = l[i].LeaveDay;
                r.Cells[11].Value = l[i].RemainingDay;
                r.Cells[12].Value = l[i].Mth01;
                r.Cells[13].Value = l[i].Mth02;
                r.Cells[14].Value = l[i].Mth03;
                r.Cells[15].Value = l[i].Mth04;
                r.Cells[16].Value = l[i].Mth05;
                r.Cells[17].Value = l[i].Mth06;
                r.Cells[18].Value = l[i].Mth07;
                r.Cells[19].Value = l[i].Mth08;
                r.Cells[20].Value = l[i].Mth09;
                r.Cells[21].Value = l[i].Mth10;
                r.Cells[22].Value = l[i].Mth11;
                r.Cells[23].Value = l[i].Mth12;
            }
            Grd1.EndUpdate();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
   
        #endregion       

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueLeaveCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue2(SetLueLeaveCode), string.Empty);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        #endregion

        #endregion

        #region Class

        private class EmpAnnualLeave
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string PayrunPeriod { get; set; }
            public string JoinDt { get; set; }
            public decimal LeaveDay { get; set; }
            public decimal RemainingDay { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth02 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth04 { get; set; }
            public decimal Mth05 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth07 { get; set; }
            public decimal Mth08 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth10 { get; set; }
            public decimal Mth11 { get; set; }
            public decimal Mth12 { get; set; }
        }

        #endregion
    }
}
