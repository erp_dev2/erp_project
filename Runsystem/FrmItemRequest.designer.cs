﻿namespace RunSystem
{
    partial class FrmItemRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtItCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkPlanningItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFixedItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkPurchaseItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkSalesItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkInventoryItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtForeignName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LblItGrpCode = new System.Windows.Forms.Label();
            this.LueItGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeSpecification = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkTaxLiableInd = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueItBrCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueItScCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPlanningItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFixedItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPurchaseItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSalesItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInventoryItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxLiableInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItBrCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItScCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 287);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.LblItGrpCode);
            this.panel2.Controls.Add(this.LueItGrpCode);
            this.panel2.Controls.Add(this.MeeSpecification);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ChkTaxLiableInd);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueItBrCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueItScCode);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.LueItCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.TxtItCodeInternal);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.TxtForeignName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 287);
            // 
            // TxtItCodeInternal
            // 
            this.TxtItCodeInternal.EnterMoveNextControl = true;
            this.TxtItCodeInternal.Location = new System.Drawing.Point(141, 89);
            this.TxtItCodeInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCodeInternal.Name = "TxtItCodeInternal";
            this.TxtItCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtItCodeInternal.Properties.MaxLength = 30;
            this.TxtItCodeInternal.Size = new System.Drawing.Size(356, 20);
            this.TxtItCodeInternal.TabIndex = 19;
            this.TxtItCodeInternal.Validated += new System.EventHandler(this.TxtItCodeInternal_Validated);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(42, 92);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(96, 14);
            this.label36.TabIndex = 18;
            this.label36.Text = "Item Local Code";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.ChkPlanningItemInd);
            this.panel4.Controls.Add(this.ChkFixedItemInd);
            this.panel4.Controls.Add(this.ChkPurchaseItemInd);
            this.panel4.Controls.Add(this.ChkSalesItemInd);
            this.panel4.Controls.Add(this.ChkInventoryItemInd);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(574, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(198, 287);
            this.panel4.TabIndex = 35;
            // 
            // ChkPlanningItemInd
            // 
            this.ChkPlanningItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPlanningItemInd.Location = new System.Drawing.Point(79, 92);
            this.ChkPlanningItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPlanningItemInd.Name = "ChkPlanningItemInd";
            this.ChkPlanningItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPlanningItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPlanningItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPlanningItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPlanningItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPlanningItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPlanningItemInd.Properties.Caption = "Planning Item";
            this.ChkPlanningItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPlanningItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkPlanningItemInd.TabIndex = 41;
            // 
            // ChkFixedItemInd
            // 
            this.ChkFixedItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkFixedItemInd.Location = new System.Drawing.Point(79, 70);
            this.ChkFixedItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkFixedItemInd.Name = "ChkFixedItemInd";
            this.ChkFixedItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkFixedItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFixedItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkFixedItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkFixedItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkFixedItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkFixedItemInd.Properties.Caption = "Fixed Item";
            this.ChkFixedItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFixedItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkFixedItemInd.TabIndex = 40;
            // 
            // ChkPurchaseItemInd
            // 
            this.ChkPurchaseItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPurchaseItemInd.Location = new System.Drawing.Point(79, 48);
            this.ChkPurchaseItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPurchaseItemInd.Name = "ChkPurchaseItemInd";
            this.ChkPurchaseItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPurchaseItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPurchaseItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPurchaseItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPurchaseItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPurchaseItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPurchaseItemInd.Properties.Caption = "Purchase Item";
            this.ChkPurchaseItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPurchaseItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkPurchaseItemInd.TabIndex = 39;
            // 
            // ChkSalesItemInd
            // 
            this.ChkSalesItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkSalesItemInd.Location = new System.Drawing.Point(79, 26);
            this.ChkSalesItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkSalesItemInd.Name = "ChkSalesItemInd";
            this.ChkSalesItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkSalesItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSalesItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkSalesItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkSalesItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkSalesItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkSalesItemInd.Properties.Caption = "Sales Item";
            this.ChkSalesItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSalesItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkSalesItemInd.TabIndex = 38;
            // 
            // ChkInventoryItemInd
            // 
            this.ChkInventoryItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkInventoryItemInd.Location = new System.Drawing.Point(79, 4);
            this.ChkInventoryItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkInventoryItemInd.Name = "ChkInventoryItemInd";
            this.ChkInventoryItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkInventoryItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInventoryItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkInventoryItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkInventoryItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkInventoryItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkInventoryItemInd.Properties.Caption = "Inventory Item";
            this.ChkInventoryItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInventoryItemInd.Size = new System.Drawing.Size(109, 22);
            this.ChkInventoryItemInd.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 36;
            this.label6.Text = "Item Type";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtForeignName
            // 
            this.TxtForeignName.EnterMoveNextControl = true;
            this.TxtForeignName.Location = new System.Drawing.Point(141, 110);
            this.TxtForeignName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtForeignName.Name = "TxtForeignName";
            this.TxtForeignName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtForeignName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtForeignName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtForeignName.Properties.Appearance.Options.UseFont = true;
            this.TxtForeignName.Properties.MaxLength = 250;
            this.TxtForeignName.Size = new System.Drawing.Size(356, 20);
            this.TxtForeignName.TabIndex = 21;
            this.TxtForeignName.Validated += new System.EventHandler(this.TxtForeignName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(56, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 20;
            this.label3.Text = "Foreign Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(141, 68);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(356, 20);
            this.TxtItName.TabIndex = 17;
            this.TxtItName.Validated += new System.EventHandler(this.TxtItName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(105, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(65, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(141, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(70, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Item Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblItGrpCode
            // 
            this.LblItGrpCode.AutoSize = true;
            this.LblItGrpCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItGrpCode.ForeColor = System.Drawing.Color.Black;
            this.LblItGrpCode.Location = new System.Drawing.Point(98, 196);
            this.LblItGrpCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItGrpCode.Name = "LblItGrpCode";
            this.LblItGrpCode.Size = new System.Drawing.Size(40, 14);
            this.LblItGrpCode.TabIndex = 28;
            this.LblItGrpCode.Text = "Group";
            this.LblItGrpCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItGrpCode
            // 
            this.LueItGrpCode.EnterMoveNextControl = true;
            this.LueItGrpCode.Location = new System.Drawing.Point(141, 194);
            this.LueItGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItGrpCode.Name = "LueItGrpCode";
            this.LueItGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItGrpCode.Properties.DropDownRows = 20;
            this.LueItGrpCode.Properties.NullText = "[Empty]";
            this.LueItGrpCode.Properties.PopupWidth = 500;
            this.LueItGrpCode.Size = new System.Drawing.Size(270, 20);
            this.LueItGrpCode.TabIndex = 29;
            this.LueItGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueItGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItGrpCode.EditValueChanged += new System.EventHandler(this.LueItGrpCode_EditValueChanged);
            // 
            // MeeSpecification
            // 
            this.MeeSpecification.EnterMoveNextControl = true;
            this.MeeSpecification.Location = new System.Drawing.Point(141, 215);
            this.MeeSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSpecification.Name = "MeeSpecification";
            this.MeeSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.Appearance.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSpecification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSpecification.Properties.MaxLength = 400;
            this.MeeSpecification.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSpecification.Properties.ShowIcon = false;
            this.MeeSpecification.Size = new System.Drawing.Size(356, 20);
            this.MeeSpecification.TabIndex = 31;
            this.MeeSpecification.ToolTip = "F4 : Show/hide text";
            this.MeeSpecification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSpecification.ToolTipTitle = "Run System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(63, 218);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 30;
            this.label9.Text = "Specification";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(141, 236);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(356, 20);
            this.MeeRemark.TabIndex = 33;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(91, 238);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 32;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTaxLiableInd
            // 
            this.ChkTaxLiableInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxLiableInd.Location = new System.Drawing.Point(139, 257);
            this.ChkTaxLiableInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxLiableInd.Name = "ChkTaxLiableInd";
            this.ChkTaxLiableInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxLiableInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxLiableInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxLiableInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxLiableInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxLiableInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxLiableInd.Properties.Caption = "Tax Liable";
            this.ChkTaxLiableInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxLiableInd.Size = new System.Drawing.Size(85, 22);
            this.ChkTaxLiableInd.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(100, 175);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 14);
            this.label7.TabIndex = 26;
            this.label7.Text = "Brand";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItBrCode
            // 
            this.LueItBrCode.EnterMoveNextControl = true;
            this.LueItBrCode.Location = new System.Drawing.Point(141, 173);
            this.LueItBrCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItBrCode.Name = "LueItBrCode";
            this.LueItBrCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.Appearance.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItBrCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItBrCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItBrCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItBrCode.Properties.DropDownRows = 20;
            this.LueItBrCode.Properties.NullText = "[Empty]";
            this.LueItBrCode.Properties.PopupWidth = 500;
            this.LueItBrCode.Size = new System.Drawing.Size(270, 20);
            this.LueItBrCode.TabIndex = 27;
            this.LueItBrCode.ToolTip = "F4 : Show/hide list";
            this.LueItBrCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItBrCode.EditValueChanged += new System.EventHandler(this.LueItBrCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(57, 154);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 14);
            this.label5.TabIndex = 24;
            this.label5.Text = "Sub Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItScCode
            // 
            this.LueItScCode.EnterMoveNextControl = true;
            this.LueItScCode.Location = new System.Drawing.Point(141, 152);
            this.LueItScCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItScCode.Name = "LueItScCode";
            this.LueItScCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.Appearance.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItScCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItScCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItScCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItScCode.Properties.DropDownRows = 20;
            this.LueItScCode.Properties.NullText = "[Empty]";
            this.LueItScCode.Properties.PopupWidth = 500;
            this.LueItScCode.Size = new System.Drawing.Size(270, 20);
            this.LueItScCode.TabIndex = 25;
            this.LueItScCode.ToolTip = "F4 : Show/hide list";
            this.LueItScCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItScCode.EditValueChanged += new System.EventHandler(this.LueItScCode_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(82, 133);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 14);
            this.label10.TabIndex = 22;
            this.label10.Text = "Category";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(141, 131);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 20;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 500;
            this.LueItCtCode.Size = new System.Drawing.Size(270, 20);
            this.LueItCtCode.TabIndex = 23;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(141, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(163, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 50);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(133, 14);
            this.label14.TabIndex = 13;
            this.label14.Text = "Reason for Cancellation";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(141, 47);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(356, 20);
            this.MeeCancelReason.TabIndex = 14;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(506, 47);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 15;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // FrmItemRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 287);
            this.Name = "FrmItemRequest";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPlanningItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFixedItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPurchaseItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSalesItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInventoryItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxLiableInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItBrCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItScCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtItCodeInternal;
        private System.Windows.Forms.Label label36;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkPlanningItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkFixedItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkPurchaseItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkSalesItemInd;
        private DevExpress.XtraEditors.CheckEdit ChkInventoryItemInd;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtForeignName;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblItGrpCode;
        private DevExpress.XtraEditors.LookUpEdit LueItGrpCode;
        private DevExpress.XtraEditors.MemoExEdit MeeSpecification;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.CheckEdit ChkTaxLiableInd;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueItBrCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueItScCode;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
    }
}