﻿#region Update
/*
    01/10/2017 [TKG] Document movement history (KMI)
    18/02/2019 [WED] Relayout -> dibuat mirip PO To Voucher, ketambahan DODR/DOD
    21/02/2019 [WED] Bug tampilan DO saat Asset ataupun Display Name ada yang NULL
    25/02/2019 [WED] pisah kolom saat excel
    06/03/2019 [WED] query performance
    15/03/2019 [WED] BUG query Voucher OP
    26/03/2019 [WED] rombak tampilan reporting.
    12/04/2019 [MEY] Bug ada data RVPO# yang tampil berulang 
    16/05/2019 [TKG] bug do qty
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDocMovementHistory : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDocMovementHistory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -14);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

	        SQL.AppendLine("    Select A.DocNo, B.DNo, Date_Format(A.DocDt, '%d/%b/%Y') DocDt, A.SiteCode, C.SiteName, B.DeptCode, B.DeptName, A.VdCode, D.VdName, B.ItCode, E.ItName, E.ForeignName, E.ItCtCode, F.ItCtName, ");
	        SQL.AppendLine("    B.Qty, A.CurCode, B.UPrice, (B.Qty * B.UPrice) As POAmtBefTax,  ");
	        SQL.AppendLine("    ((B.Qty * B.UPrice) +  ");
	        SQL.AppendLine("    ((B.Qty * B.UPrice) * 0.01 * Case When A.TaxCode1 Is Not Null Then (Select TaxRate From TblTax Where TaxCode = A.TaxCode1) Else 0 End ) + ");
	        SQL.AppendLine("    ((B.Qty * B.UPrice) * 0.01 * Case When A.TaxCode2 Is Not Null Then (Select TaxRate From TblTax Where TaxCode = A.TaxCode2) Else 0 End ) + ");
	        SQL.AppendLine("    ((B.Qty * B.UPrice) * 0.01 * Case When A.TaxCode3 Is Not Null Then (Select TaxRate From TblTax Where TaxCode = A.TaxCode3) Else 0 End ) ");
	        SQL.AppendLine("    ) As POAmtAfterTax, ");
            SQL.AppendLine("    G.RecvVdDocNo, G.RecvVdDNo, G.RecvVdDocDt, G.WhsCode, G.WhsName, IfNull(Convert(Format(G.RecvVdQty, 2) using utf8), 0.00) RecvVdQty, ");
             SQL.AppendLine("    J.DODocNo, J.DODNo, J.DODocDt, J.DOWhsName, J.DODeptName, J.AssetName, J.DisplayName, ");
            SQL.AppendLine("    IfNull(Convert(Format(J.DOQty, 2) using utf8), 0.00) DOQty, ");
	        SQL.AppendLine("    I.VCDocNoAP, I.VCDocDtAP, I.VCCurCodeAP, ");
	        SQL.AppendLine("    H.VCDocNoOP, H.VCDocDtOP, H.VCCurCodeOP ");
	        SQL.AppendLine("    From TblPOHdr A ");
	        SQL.AppendLine("    Inner Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select X1.DocNo, X1.DNo, X3.ItCode, X1.Qty, X1.Discount, X1.DiscountAmt, X1.RoundingValue, X4.UPrice, ");
            SQL.AppendLine("        X5.DeptCode, X6.DeptName ");
		    SQL.AppendLine("        From TblPODtl X1 ");
            SQL.AppendLine("        Inner Join TblPOHdr X7 On X1.DocNo= X7.DocNo And (X7.DocDt Between @DocDt1 And @DocDt2) ");
            if (TxtPODocNo.Text.Length > 0)
                SQL.AppendLine("        And X1.DocNo Like @DocNo ");
		    SQL.AppendLine("        Inner Join TblPORequestDtl X2 On X1.PORequestDocNo = X2.DocNo And X1.PORequestDNo = X2.DNo And X1.CancelInd = 'N' ");
		    SQL.AppendLine("        Inner Join TblMaterialRequestDtl X3 On X2.MaterialRequestDocNo = X3.DocNo And X2.MaterialRequestDNo = X3.DNo ");
		    SQL.AppendLine("        Inner Join TblQtDtl X4 On X2.QtDocNo = X4.DocNo And X2.QtDNo = X4.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestHdr X5 On X3.DocNo = X5.DocNo ");
            SQL.AppendLine("        Inner Join TblDepartment X6 On X5.DeptCode = X6.DeptCode ");
	        SQL.AppendLine("    ) B On A.DocNo = B.DocNo  ");

	        SQL.AppendLine("    Inner Join TblSite C On A.SiteCode = C.SiteCode ");
	        SQL.AppendLine("    Inner Join TblVendor D On A.VdCode = D.VdCode ");
	        SQL.AppendLine("    Inner Join TblItem E On B.ItCode = E.ItCode ");
            if (TxtItCode.Text.Length > 0)
            {
                SQL.AppendLine("    And (E.ItCode Like @ItCode Or E.ItName Like @ItCode Or E.ForeignName Like @ItCode) ");
            }
	        SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode = F.ItCtCode ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Distinct Concat(X2.DocNo, X2.DNo), X1.DocNo As RecvVdDocNo, Date_Format(X1.DocDt, '%d/%b/%Y') As RecvVdDocDt, X2.DNo As RecvVdDNo, X2.PODocNo, X2.PODNo, X2.Qty As RecvVdQty, X1.WhsCode, X3.WhsName ");
		    SQL.AppendLine("        From TblRecvVdHdr X1 ");
		    SQL.AppendLine("        Inner Join TblRecvVdDtl X2 On X1.DocNo = X2.DocNo And X2.CancelInd = 'N' And X2.Status In ('O', 'A') ");
            SQL.AppendLine("        And X2.PODocNo In (Select DocNo From TblPOHdr Where DocDt Between @DocDt1 And @DocDt2)  ");
		    SQL.AppendLine("        Inner Join TblWarehouse X3 On X1.WhsCode = X3.WhsCode ");
	        SQL.AppendLine("    ) G On B.DocNo = G.PODocNo And B.DNo = G.PODNo ");

            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("   Select Distinct X.VCDocNoOP, W2.RecvVdDocno, W2.RecvVdDNo, Date_Format(X.VCDocDtOP, '%d/%b/%Y') VCDocDtOP, X.VCCurCodeOP ");
            SQL.AppendLine("   From TblPOHdr C1 ");
            SQL.AppendLine("   Inner Join TblPODtl C2 On C1.DocNo = C2.DocNo And (C1.DocDt Between @DocDt1 And @DocDt2) And C2.CancelInd = 'N' ");
            SQL.AppendLine("   Inner Join TblRecvVdDtl C3 On C2.DocNo = C3.PODocNo And C2.DNo = C3.PODNo And C3.CancelInd = 'N' And C3.Status In ('O', 'A') ");
            SQL.AppendLine("   Inner Join TblPurchaseInvoiceDtl W2 On W2.RecvVdDocNo = C3.DocNo And W2.RecvVdDNo = C3.DNo ");
            SQL.AppendLine("   Inner Join TblPurchaseinvoicehdr W On W2.DocNo = W.DocNo And W.CancelInd = 'N' ");
            SQL.AppendLine("   Inner Join   ");
            SQL.AppendLine("   (  ");
            SQL.AppendLine("       Select Y2.invoiceDocNo, Z.VCDocnoOP, Z.VCDocDtOP, Z.VCCurCodeOP  ");
            SQL.AppendLine("       From tbloutgoingpaymentHdr Y  ");
            SQL.AppendLine("       Inner Join TblOutgoingpaymentDtl Y2  on Y.Docno = Y2.DocNo And Y.CancelInd = 'N' ");
            SQL.AppendLine("       Inner Join   ");
            SQL.AppendLine("       (  ");
            SQL.AppendLine("           Select A.DocNo As VRDocNoOP, A.DocDt VRDocDtOP, B.Docno As VCDocNoOP, B.DocDt As VCDocDtOP, B.CurCode As VCCurCodeOP  ");
            SQL.AppendLine("           From tblVoucherRequesthdr A  ");
            SQL.AppendLine("           Inner Join TblVoucherHdr B On A.VoucherDocno = B.DocNo And B.Cancelind = 'N'   ");
            SQL.AppendLine("           Where A.CancelInd = 'N' And Status = 'A'  ");
            SQL.AppendLine("       ) Z On Y.VoucherRequestDocNo = Z.VRDocNoOP  ");
            SQL.AppendLine("   )X On W.DocNo = X.InvoiceDocno  ");
            SQL.AppendLine(")H On G.RecvVdDocNo = H.RecvVdDocNo And G.RecvVdDNo = H.RecvVdDNo ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Distinct X3.DocNo VCDocNoAP2, X1.DocNo, X3.DocNo As VCDocNoAP, Date_Format(X3.DocDt, '%d/%b/%Y') As VCDocDtAP, X3.CurCode As VCCurCodeAP ");
		    SQL.AppendLine("        From TblPOHdr X1 ");
		    SQL.AppendLine("        Inner Join TblAPDownpayment X2 On X1.DocNo = X2.PODocNo And (X1.DocDt Between @DocDt1 And @DocDt2) And X2.CancelInd = 'N' And X2.Status = 'A' ");
		    SQL.AppendLine("        Inner Join TblVoucherHdr X3 On X2.VoucherRequestDocNo = X3.VoucherRequestDocNo And X3.CancelInd = 'N' And X3.DocType = '04' ");
            SQL.AppendLine("        Where X1.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("    ) I On A.DocNo = I.DocNo ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X2.DocNo As PODocNo, X2.DNo As PODNo, ");
            SQL.AppendLine("        X4.DocNo As DODocNo, X4.DNo As DODNo, ");
            SQL.AppendLine("        Date_Format(X5.DocDt, '%d/%b/%Y') As DODocDt, ");
            SQL.AppendLine("        X6.WhsName As DOWhsName, X7.DeptName As DODeptName, X8.AssetName, X8.DisplayName, ");
            SQL.AppendLine("        Sum(X4.Qty) As DOQty ");
            SQL.AppendLine("        From TblPOHdr X1 ");
            SQL.AppendLine("        Inner Join TblPODtl X2 On X1.DocNo = X2.DocNo And (X1.DocDt Between @DocDt1 And @DocDt2) And X2.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl X3 On X2.DocNo = X3.PODocNo And X2.DNo = X3.PODNo And X3.CancelInd = 'N' And X3.Status = 'A' ");
            SQL.AppendLine("        Inner Join TblDODeptDtl X4 On X3.Source = X4.Source And X4.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join TblDODeptHdr X5 On X4.DocNo = X5.DocNo ");
            SQL.AppendLine("        Inner Join TblWarehouse X6 On X5.WhsCode = X6.WhsCode ");
            SQL.AppendLine("        Inner Join TblDepartment X7 On X5.DeptCode = X7.DeptCode ");
            SQL.AppendLine("        Left Join TblAsset X8 On X4.AssetCode = X8.AssetCode ");
            SQL.AppendLine("        Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Group By X2.DocNo, X2.DNo, X4.DocNo, X4.DNo, X5.DocDt, X6.WhsName, X7.DeptName, X8.AssetName, X8.DisplayName ");

	        SQL.AppendLine("    ) J On A.DocNo = J.PODocNo And B.DNo = J.PODNo ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            if (TxtRecvVdDocNo.Text.Length > 0)
            {
                SQL.AppendLine("     And A.DocNo In ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("         Select Distinct PODocNo ");
                SQL.AppendLine("         From TblRecvVdDtl ");
                SQL.AppendLine("         Where DocNo Like @RecvVdDocNo ");
                SQL.AppendLine("     ) ");
            }

            if (TxtDODept.Text.Length > 0)
            {
                SQL.AppendLine("     And A.DocNo In ");
                SQL.AppendLine("     ( ");
                SQL.AppendLine("         Select Distinct A.PODocNo ");
                SQL.AppendLine("         From TblRecvVdDtl A ");
                SQL.AppendLine("         Inner Join TblDODeptDtl B On A.Source = B.Source And B.CancelInd = 'N' And B.DocNo Like @DODocNo ");
                SQL.AppendLine("     ) ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "PO#",
                    "PO Date",
                    "Site Code",
                    "Site",
                    "Department Code",

                    //6-10
                    "Department",
                    "Vendor Code",
                    "Vendor",
                    "Item's Code",
                    "Item's Name",
                    
                    //11-15
                    "Foreign Name",
                    "Item's"+Environment.NewLine+"Category Code",
                    "Item's"+Environment.NewLine+"Category",
                    "PO's Quantity",
                    "Currency",

                    //16-20
                    "Price",
                    "PO Amount Before Tax",
                    "PO Amount After Tax",
                    "RVPO#",
                    "Date (RVPO)",

                    //21-25
                    "Warehouse (RVPO)",
                    "Quantity (RVPO)",
                    "DO#",
                    "Date (DO)",
                    "Warehouse (DO)",

                    //26-30
                    "Department (DO)",
                    "Asset Name (DO)",
                    "Asset Display Name (DO)",
                    "Quantity (DO)",
                    "VC APDP#",

                    //31-35
                    "Date (APDP)",
                    "Currency (APDP)",
                    "VC OP#",
                    "Date (OP)",
                    "Currency (OP)"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 80, 100, 250, 100,

                    //6-10
                    250, 100, 250, 100, 150,

                    //11-15
                    250, 100, 180, 100, 80,

                    //16-20
                    120, 150, 150, 180, 120,

                    //21-25
                    250, 120, 150, 120, 250,

                    //26-30
                    200, 180, 180, 120, 100,

                    //31-35
                    100, 100, 100, 100, 100,

                    //36-39
                    100, 100, 100, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 17, 18, 22, 29 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7, 9, 12 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7, 9, 12 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<DocMovementHistory>();
                PrepData(ref l);
                if (l.Count > 0)
                {
                    FetchData(ref l);
                    ShowDataInGrd(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void PrepData(ref List<DocMovementHistory> l)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = GetSQL() + " Order By A.DocDt, A.DocNo, B.DNo, E.ForeignName, E.ItName, G.RecvVdDocDt, G.RecvVdDocNo, G.RecvVdDNo, J.DODocDt, J.DODocNo, I.VCDocDtAP, I.VCDocNoAP, H.VCDocDtOP, H.VCDocNoOP; ";
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@DocNo", string.Concat("%", TxtPODocNo.Text, "%"));
                Sm.CmParam<String>(ref cm, "@RecvVdDocNo", string.Concat("%", TxtRecvVdDocNo.Text, "%"));
                Sm.CmParam<String>(ref cm, "@DODocNo", string.Concat("%", TxtDODept.Text, "%"));
                Sm.CmParam<String>(ref cm, "@ItCode", string.Concat("%", TxtItCode.Text, "%"));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "SiteCode", "SiteName", "DeptCode", "DeptName",

                    //6-10
                    "VdCode", "VdName", "ItCode", "ItName", "ForeignName",

                    //11-15
                    "ItCtCode", "ItCtName", "Qty", "CurCode", "UPrice",

                    //16-20
                    "POAmtBefTax", "POAmtAfterTax", "RecvVdDocNo", "RecvVdDNo", "RecvVdDocDt", 
                    
                    //21-25
                    "WhsName", "RecvVdQty", "DODocNo", "DODNo", "DODocDt", 
                    
                    //26-30
                    "DOWhsName", "DODeptName", "AssetName", "DisplayName", "DOQty",

                    //31-35
                    "VCDocNoAP", "VCDocDtAP", "VCCurCodeAP", "VCDocNoOP", "VCDocDtOP", 
                    
                    //36
                    "VCCurCodeOP"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DocMovementHistory()
                        {
                            PODocNo = Sm.DrStr(dr, c[0]),

                            PODocDt = Sm.DrStr(dr, c[1]),
                            SiteCode = Sm.DrStr(dr, c[2]),
                            SiteName = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            DeptName = Sm.DrStr(dr, c[5]),

                            VdCode = Sm.DrStr(dr, c[6]),
                            VdName = Sm.DrStr(dr, c[7]),
                            ItCode = Sm.DrStr(dr, c[8]),
                            ItName = Sm.DrStr(dr, c[9]),
                            ForeignName = Sm.DrStr(dr, c[10]),

                            ItCtCode = Sm.DrStr(dr, c[11]),
                            ItCtName = Sm.DrStr(dr, c[12]),
                            POQty = Sm.DrDec(dr, c[13]),
                            CurCode = Sm.DrStr(dr, c[14]),
                            Price = Sm.DrDec(dr, c[15]),

                            POAmtBefTax = Sm.DrDec(dr, c[16]),
                            POAmtAftTax = Sm.DrDec(dr, c[17]),
                            RVPODocNo = Sm.DrStr(dr, c[18]).Length > 0 ? string.Concat(Sm.DrStr(dr, c[18]), " (", Sm.DrStr(dr, c[19]), ")") : string.Empty,
                            RVPODocDt = Sm.DrStr(dr, c[20]),
                            RVPOWhsName = Sm.DrStr(dr, c[21]),

                            RVPOQty = Sm.DrDec(dr, c[22]),
                            DODocNo = Sm.DrStr(dr, c[23]).Length > 0 ? Sm.DrStr(dr, c[23]) : string.Empty,
                            DODocDt = Sm.DrStr(dr, c[25]),
                            DOWhsName = Sm.DrStr(dr, c[26]),
                            DODeptName = Sm.DrStr(dr, c[27]),

                            DOAssetName = Sm.DrStr(dr, c[28]),
                            DODisplayName = Sm.DrStr(dr, c[29]),
                            DOQty = Sm.DrDec(dr, c[30]),
                            VCAPDocNo = Sm.DrStr(dr, c[31]),
                            VCAPDocDt = Sm.DrStr(dr, c[32]),

                            VCAPCurCode = Sm.DrStr(dr, c[33]),
                            VCOPDocNo = Sm.DrStr(dr, c[34]),
                            VCOPDocDt = Sm.DrStr(dr, c[35]),
                            VCOPCurCode = Sm.DrStr(dr, c[36])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void FetchData(ref List<DocMovementHistory> l)
        {
            string
                mPODocNo = string.Empty,
                mItCode = string.Empty,
                mRVPODocNo = string.Empty,
                mDODocNo = string.Empty,
                mVCOPDocNo = string.Empty,
                mVCAPDocNo = string.Empty,
                mRVPODocDt = string.Empty
               // mRVPOWhsName = string.Empty

                ;

            for (int i = 0; i < l.Count; i++)
            {
                if (mPODocNo == l[i].PODocNo && mItCode == l[i].ItCode )
                {
                    l[i].PODocNo = string.Empty;
                    l[i].PODocDt = string.Empty;
                    l[i].SiteCode = string.Empty;
                    l[i].SiteName = string.Empty;
                    l[i].DeptCode = string.Empty;
                    l[i].DeptName = string.Empty;
                    l[i].VdCode = string.Empty;
                    l[i].VdName = string.Empty;
                    l[i].ItCode = string.Empty;
                    l[i].ItName = string.Empty;
                    l[i].ForeignName = string.Empty;
                    l[i].ItCtCode = string.Empty;
                    l[i].ItCtName = string.Empty;
                    l[i].POQty = 0m;
                    l[i].CurCode = string.Empty;
                    l[i].Price = 0m;
                    l[i].POAmtAftTax = 0m;
                    l[i].POAmtBefTax = 0m;

                    if (mRVPODocNo == l[i].RVPODocNo)
                    {
                        l[i].RVPODocNo = string.Empty;
                        l[i].RVPODocDt = string.Empty;
                        l[i].RVPOWhsName = string.Empty;
                        l[i].RVPOQty = 0m;
                    }
                    else
                    {
                        mRVPODocNo  = l[i].RVPODocNo;
                        mRVPODocDt = l[i].RVPODocDt;
                    }

                    if (mDODocNo == l[i].DODocNo)
                    {
                        l[i].DODocNo = string.Empty;
                        l[i].DODocDt = string.Empty;
                        l[i].DODeptName = string.Empty;
                        l[i].DODisplayName = string.Empty;
                        l[i].DOAssetName = string.Empty;
                        l[i].DOWhsName = string.Empty;
                        l[i].DOQty = 0m;
                    }
                    else
                    {
                        mDODocNo = l[i].DODocNo;
                    }

                    if (mVCAPDocNo == l[i].VCAPDocNo)
                    {
                        l[i].VCAPDocNo = string.Empty;
                        l[i].VCAPDocDt = string.Empty;
                        l[i].VCAPCurCode = string.Empty;
                    }

                    if (mVCOPDocNo == l[i].VCOPDocNo)
                    {
                        l[i].VCOPDocNo = string.Empty;
                        l[i].VCOPDocDt = string.Empty;
                        l[i].VCOPCurCode = string.Empty;
                    }
                }
                else
                {
                    mPODocNo = l[i].PODocNo;
                    mItCode = l[i].ItCode;
                    mRVPODocNo = l[i].RVPODocNo;
                    //mDODocNo = l[i].DODocNo;
                    mVCOPDocNo = l[i].VCOPDocNo;
                    mVCAPDocNo = l[i].VCAPDocNo;
                }                
            }
        }

        private void ShowDataInGrd(ref List<DocMovementHistory> l2)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            int mNo = 0;
            for (int i = 0; i < l2.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = ++mNo;
                r.Cells[1].Value = l2[i].PODocNo;
                r.Cells[2].Value = l2[i].PODocDt;
                r.Cells[3].Value = l2[i].SiteCode;
                r.Cells[4].Value = l2[i].SiteName;
                r.Cells[5].Value = l2[i].DeptCode;
                r.Cells[6].Value = l2[i].DeptName;
                r.Cells[7].Value = l2[i].VdCode;
                r.Cells[8].Value = l2[i].VdName;
                r.Cells[9].Value = l2[i].ItCode;
                r.Cells[10].Value = l2[i].ItName;
                r.Cells[11].Value = l2[i].ForeignName;
                r.Cells[12].Value = l2[i].ItCtCode;
                r.Cells[13].Value = l2[i].ItCtName;
                r.Cells[14].Value = l2[i].POQty;
                r.Cells[15].Value = l2[i].CurCode;
                r.Cells[16].Value = l2[i].Price;
                r.Cells[17].Value = l2[i].POAmtBefTax;
                r.Cells[18].Value = l2[i].POAmtAftTax;
                r.Cells[19].Value = l2[i].RVPODocNo;
                r.Cells[20].Value = l2[i].RVPODocDt;
                r.Cells[21].Value = l2[i].RVPOWhsName;
                r.Cells[22].Value = l2[i].RVPOQty;
                r.Cells[23].Value = l2[i].DODocNo;
                r.Cells[24].Value = l2[i].DODocDt;
                r.Cells[25].Value = l2[i].DOWhsName;
                r.Cells[26].Value = l2[i].DODeptName;
                r.Cells[27].Value = l2[i].DOAssetName;
                r.Cells[28].Value = l2[i].DODisplayName;
                r.Cells[29].Value = l2[i].DOQty;
                r.Cells[30].Value = l2[i].VCAPDocNo;
                r.Cells[31].Value = l2[i].VCAPDocDt;
                r.Cells[32].Value = l2[i].VCAPCurCode;
                r.Cells[33].Value = l2[i].VCOPDocNo;
                r.Cells[34].Value = l2[i].VCOPDocDt;
                r.Cells[35].Value = l2[i].VCOPCurCode;
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtDODept_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDODept_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void TxtRecvVdDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRecvVdDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

        #region Class

        private class DocMovementHistory
        {
            public string PODocNo { get; set; }
            public string PODocDt { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string DeptCode { get; set; }

            public string DeptName { get; set; }
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }

            public string ForeignName { get; set; }
            public string ItCtCode { get; set; }
            public string ItCtName { get; set; }
            public decimal POQty { get; set; }
            public string CurCode { get; set; }

            public decimal Price { get; set; }
            public decimal POAmtBefTax { get; set; }
            public decimal POAmtAftTax { get; set; }
            public string RVPODocNo { get; set; }
            public string RVPODocDt { get; set; }

            public string RVPOWhsName { get; set; }
            public decimal RVPOQty { get; set; }
            public string DODocNo { get; set; }
            public string DODocDt { get; set; }
            public string DOWhsName { get; set; }

            public string DODeptName { get; set; }
            public string DOAssetName { get; set; }
            public string DODisplayName { get; set; }
            public decimal DOQty { get; set; }
            public string VCAPDocNo { get; set; }

            public string VCAPDocDt { get; set; }
            public string VCAPCurCode { get; set; }
            public string VCOPDocNo { get; set; }
            public string VCOPDocDt { get; set; }
            public string VCOPCurCode { get; set; }
        }

        #endregion

    }
}
