﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBinInfo : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptBinInfo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.Bin, A.ActInd, B.OptDesc As StatusDesc, C.WhsName, D.DocNo ");
            SQL.AppendLine("From TblBin A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat='BinStatus' And A.Status=B.OptCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.Bin, Group_Concat(T.WhsName Order By T.WhsName Separator ', ') As WhsName ");
	        SQL.AppendLine("    From ( ");
	        SQL.AppendLine("        Select Distinct T1.Bin, T2.WhsName ");
	        SQL.AppendLine("        From TblStockSummary T1 ");
	        SQL.AppendLine("        Inner Join TblWarehouse T2 On T1.WhsCode=T2.WhsCode ");
	        SQL.AppendLine("        Where T1.Qty>0 Or T1.Qty2>0 Or T1.Qty3>0 ");
	        SQL.AppendLine("        ) T Group By T.Bin ");
            SQL.AppendLine("    ) C On A.Bin=C.Bin ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.Bin, Group_Concat(T.DocNo Order By T.DocNo Separator ', ') As DocNo ");
            SQL.AppendLine("        From ( ");
	        SQL.AppendLine("            Select Bin, DocNo ");
	        SQL.AppendLine("            From TblBinTransferRequestDtl ");
	        SQL.AppendLine("            Where CancelInd='N' And ProcessInd='O' ");
	        SQL.AppendLine("        ) T Group By T.Bin ");
            SQL.AppendLine("    ) D On A.Bin=D.Bin ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Bin", 
                        "Active",
                        "Status",   
                        "Warehouse", 
                        "Outstanding Request"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 50, 100, 330, 190
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Grd1.Rows.AutoHeight();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                if (ChkActivedBin.Checked) Filter = " Where ActInd='Y' ";
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", true);
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.Bin;",
                new string[]
                    {
                        //0
                        "Bin", 

                        //1-4
                        "ActInd", "StatusDesc", "WhsName", "DocNo"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
