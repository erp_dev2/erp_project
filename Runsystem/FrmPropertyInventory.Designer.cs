﻿namespace RunSystem
{
    partial class FrmPropertyInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPropertyInventory));
            this.TxtPropertyCode = new DevExpress.XtraEditors.TextEdit();
            this.LblPropertyCode = new System.Windows.Forms.Label();
            this.LblPropertyName = new System.Windows.Forms.Label();
            this.LblDisplayName = new System.Windows.Forms.Label();
            this.LblParent = new System.Windows.Forms.Label();
            this.LblPropertyMutationSource = new System.Windows.Forms.Label();
            this.LblExternalCode = new System.Windows.Forms.Label();
            this.TxtPropertyName = new DevExpress.XtraEditors.TextEdit();
            this.TxtPropertyMutationDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.TxtParent = new DevExpress.XtraEditors.TextEdit();
            this.TxtExternalCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.LblStatus = new System.Windows.Forms.Label();
            this.LblCancelReason = new System.Windows.Forms.Label();
            this.TxtCancelReason = new DevExpress.XtraEditors.TextEdit();
            this.LblDeactivationReason = new System.Windows.Forms.Label();
            this.TxtDeactivationReason = new DevExpress.XtraEditors.TextEdit();
            this.ChkCompleteInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TcPropertyInventory = new DevExpress.XtraTab.XtraTabControl();
            this.TpGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.BtnCCCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRemStockValue = new DevExpress.XtraEditors.TextEdit();
            this.LblRemainingStockValue = new System.Windows.Forms.Label();
            this.TxtRemStockQty = new DevExpress.XtraEditors.TextEdit();
            this.LblRemStockQty = new System.Windows.Forms.Label();
            this.TxtUPrice = new DevExpress.XtraEditors.TextEdit();
            this.LblUPrice = new System.Windows.Forms.Label();
            this.TxtInventoryQty = new DevExpress.XtraEditors.TextEdit();
            this.LblInventoryQty = new System.Windows.Forms.Label();
            this.TxtUoM = new DevExpress.XtraEditors.TextEdit();
            this.LblUoM = new System.Windows.Forms.Label();
            this.TxtPropertyInventoryValue = new DevExpress.XtraEditors.TextEdit();
            this.LblPropertyInventoryValue = new System.Windows.Forms.Label();
            this.BtnSiteCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCCCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.LblShortCode = new System.Windows.Forms.Label();
            this.LblPropertyCategoryCode = new System.Windows.Forms.Label();
            this.LuePropertyCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.LblRegistrationDt = new System.Windows.Forms.Label();
            this.DteRegistrationDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.LblItem = new System.Windows.Forms.Label();
            this.LblSite = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.BtnItCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TpPropertyCostComponent = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpFairValue = new DevExpress.XtraTab.XtraTabPage();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpMutationDocumentRef = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpPropertyTransferRef = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpCertificateInformation = new DevExpress.XtraTab.XtraTabPage();
            this.MeeRegistrationBasis = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCertExpDt = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAreaBasedOnSurveyDoc = new DevExpress.XtraEditors.TextEdit();
            this.LblAreaBasedOnSurveyDoc = new System.Windows.Forms.Label();
            this.TxtSurveyDocDt = new DevExpress.XtraEditors.TextEdit();
            this.LblSurveyDocDt = new System.Windows.Forms.Label();
            this.TxtSurveyDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblSurveyDocNo = new System.Windows.Forms.Label();
            this.TxtBasisDocumentDt = new DevExpress.XtraEditors.TextEdit();
            this.LblBasisDocumentDt = new System.Windows.Forms.Label();
            this.TxtRegistrationBasis = new DevExpress.XtraEditors.TextEdit();
            this.LblRegistrationBasis = new System.Windows.Forms.Label();
            this.TxtNIB = new DevExpress.XtraEditors.TextEdit();
            this.LblNIB = new System.Windows.Forms.Label();
            this.TxtCertificateIssuedLoc = new DevExpress.XtraEditors.TextEdit();
            this.LblCertificateIssuedLoc = new System.Windows.Forms.Label();
            this.TxtCertificateExpiredDt = new DevExpress.XtraEditors.TextEdit();
            this.LblCertificateExpiredDt = new System.Windows.Forms.Label();
            this.TxtCertificateIssuedDt = new DevExpress.XtraEditors.TextEdit();
            this.LblCertificateIssuedDt = new System.Windows.Forms.Label();
            this.TxtCertificateNo = new DevExpress.XtraEditors.TextEdit();
            this.LblCertificateNo = new System.Windows.Forms.Label();
            this.TxtCertificateType = new DevExpress.XtraEditors.TextEdit();
            this.LblCertificateType = new System.Windows.Forms.Label();
            this.TpHistoricalInformation = new DevExpress.XtraTab.XtraTabPage();
            this.MeeVillage = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeDistrict = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeRightsReleaseBasis = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtSpatialInfoPage = new DevExpress.XtraEditors.TextEdit();
            this.LblSpatialInfoPage = new System.Windows.Forms.Label();
            this.TxtIndicativePotentialArea = new DevExpress.XtraEditors.TextEdit();
            this.LblIndicativePotentialArea = new System.Windows.Forms.Label();
            this.LblRightsReleaseBasis = new System.Windows.Forms.Label();
            this.TxtDecreeNo = new DevExpress.XtraEditors.TextEdit();
            this.LblDecreeNo = new System.Windows.Forms.Label();
            this.DteDecreeDt = new DevExpress.XtraEditors.DateEdit();
            this.LblDecreeDt = new System.Windows.Forms.Label();
            this.DteRightsExpiredDt = new DevExpress.XtraEditors.DateEdit();
            this.LblRightsExpiredDt = new System.Windows.Forms.Label();
            this.TxtRightsHolder = new DevExpress.XtraEditors.TextEdit();
            this.LblRightsHolder = new System.Windows.Forms.Label();
            this.DteCertificatePublishDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtRightsNumberAndNIB = new DevExpress.XtraEditors.TextEdit();
            this.TxtVillage = new DevExpress.XtraEditors.TextEdit();
            this.TxtDistrict = new DevExpress.XtraEditors.TextEdit();
            this.LblCertificatePublishDt = new System.Windows.Forms.Label();
            this.LblRightsNumberAndNIB = new System.Windows.Forms.Label();
            this.LblVillage = new System.Windows.Forms.Label();
            this.LblDistrict = new System.Windows.Forms.Label();
            this.LblCity = new System.Windows.Forms.Label();
            this.LueCity = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProvince = new System.Windows.Forms.Label();
            this.LueProvince = new DevExpress.XtraEditors.LookUpEdit();
            this.LblAcquisitionType = new System.Windows.Forms.Label();
            this.LueAcquisitionType = new DevExpress.XtraEditors.LookUpEdit();
            this.TpUploadFile = new DevExpress.XtraTab.XtraTabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpApprovalInformation = new DevExpress.XtraTab.XtraTabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnParent = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPropertyMutationDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtCopyData = new DevExpress.XtraEditors.TextEdit();
            this.LblCopyData = new System.Windows.Forms.Label();
            this.BtnCopyData = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.BtnExcel = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyMutationDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExternalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeactivationReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCompleteInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcPropertyInventory)).BeginInit();
            this.TcPropertyInventory.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemStockValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemStockQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUoM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRegistrationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRegistrationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            this.TpPropertyCostComponent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpFairValue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpMutationDocumentRef.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpPropertyTransferRef.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpCertificateInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRegistrationBasis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCertExpDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAreaBasedOnSurveyDoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSurveyDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSurveyDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBasisDocumentDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegistrationBasis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateIssuedLoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateExpiredDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateIssuedDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateType.Properties)).BeginInit();
            this.TpHistoricalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeVillage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRightsReleaseBasis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpatialInfoPage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndicativePotentialArea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDecreeNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDecreeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDecreeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRightsExpiredDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRightsExpiredDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRightsHolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertificatePublishDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertificatePublishDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRightsNumberAndNIB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProvince.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcquisitionType.Properties)).BeginInit();
            this.TpUploadFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpApprovalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCopyData.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnExcel);
            this.panel1.Location = new System.Drawing.Point(1103, 0);
            this.panel1.Size = new System.Drawing.Size(70, 537);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.BtnExcel, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 120);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 96);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2);
            this.BtnSave.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 72);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 48);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 24);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 24);
            this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2);
            this.BtnFind.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 144);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 24);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.BtnPropertyMutationDocNo);
            this.panel2.Controls.Add(this.BtnParent);
            this.panel2.Controls.Add(this.TxtExternalCode);
            this.panel2.Controls.Add(this.TxtParent);
            this.panel2.Controls.Add(this.TxtDisplayName);
            this.panel2.Controls.Add(this.TxtPropertyMutationDocNo);
            this.panel2.Controls.Add(this.TxtPropertyName);
            this.panel2.Controls.Add(this.LblExternalCode);
            this.panel2.Controls.Add(this.LblPropertyMutationSource);
            this.panel2.Controls.Add(this.LblParent);
            this.panel2.Controls.Add(this.LblDisplayName);
            this.panel2.Controls.Add(this.LblPropertyName);
            this.panel2.Controls.Add(this.TxtPropertyCode);
            this.panel2.Controls.Add(this.LblPropertyCode);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Size = new System.Drawing.Size(1103, 149);
            // 
            // TxtPropertyCode
            // 
            this.TxtPropertyCode.EnterMoveNextControl = true;
            this.TxtPropertyCode.Location = new System.Drawing.Point(155, 5);
            this.TxtPropertyCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyCode.Name = "TxtPropertyCode";
            this.TxtPropertyCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPropertyCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyCode.Properties.MaxLength = 40;
            this.TxtPropertyCode.Size = new System.Drawing.Size(263, 20);
            this.TxtPropertyCode.TabIndex = 12;
            // 
            // LblPropertyCode
            // 
            this.LblPropertyCode.AutoSize = true;
            this.LblPropertyCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyCode.ForeColor = System.Drawing.Color.Black;
            this.LblPropertyCode.Location = new System.Drawing.Point(67, 8);
            this.LblPropertyCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyCode.Name = "LblPropertyCode";
            this.LblPropertyCode.Size = new System.Drawing.Size(86, 14);
            this.LblPropertyCode.TabIndex = 11;
            this.LblPropertyCode.Text = "Property Code";
            this.LblPropertyCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPropertyName
            // 
            this.LblPropertyName.AutoSize = true;
            this.LblPropertyName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyName.ForeColor = System.Drawing.Color.Red;
            this.LblPropertyName.Location = new System.Drawing.Point(64, 31);
            this.LblPropertyName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyName.Name = "LblPropertyName";
            this.LblPropertyName.Size = new System.Drawing.Size(89, 14);
            this.LblPropertyName.TabIndex = 13;
            this.LblPropertyName.Text = "Property Name";
            this.LblPropertyName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDisplayName
            // 
            this.LblDisplayName.AutoSize = true;
            this.LblDisplayName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDisplayName.ForeColor = System.Drawing.Color.Black;
            this.LblDisplayName.Location = new System.Drawing.Point(74, 53);
            this.LblDisplayName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDisplayName.Name = "LblDisplayName";
            this.LblDisplayName.Size = new System.Drawing.Size(78, 14);
            this.LblDisplayName.TabIndex = 14;
            this.LblDisplayName.Text = "Display Name";
            this.LblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblParent
            // 
            this.LblParent.AutoSize = true;
            this.LblParent.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblParent.ForeColor = System.Drawing.Color.Black;
            this.LblParent.Location = new System.Drawing.Point(110, 75);
            this.LblParent.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblParent.Name = "LblParent";
            this.LblParent.Size = new System.Drawing.Size(43, 14);
            this.LblParent.TabIndex = 15;
            this.LblParent.Text = "Parent";
            this.LblParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPropertyMutationSource
            // 
            this.LblPropertyMutationSource.AutoSize = true;
            this.LblPropertyMutationSource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyMutationSource.ForeColor = System.Drawing.Color.Black;
            this.LblPropertyMutationSource.Location = new System.Drawing.Point(5, 97);
            this.LblPropertyMutationSource.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyMutationSource.Name = "LblPropertyMutationSource";
            this.LblPropertyMutationSource.Size = new System.Drawing.Size(148, 14);
            this.LblPropertyMutationSource.TabIndex = 16;
            this.LblPropertyMutationSource.Text = "Property Mutation Source";
            this.LblPropertyMutationSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblExternalCode
            // 
            this.LblExternalCode.AutoSize = true;
            this.LblExternalCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExternalCode.ForeColor = System.Drawing.Color.Black;
            this.LblExternalCode.Location = new System.Drawing.Point(69, 119);
            this.LblExternalCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblExternalCode.Name = "LblExternalCode";
            this.LblExternalCode.Size = new System.Drawing.Size(83, 14);
            this.LblExternalCode.TabIndex = 17;
            this.LblExternalCode.Text = "External Code";
            this.LblExternalCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyName
            // 
            this.TxtPropertyName.EnterMoveNextControl = true;
            this.TxtPropertyName.Location = new System.Drawing.Point(155, 28);
            this.TxtPropertyName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyName.Name = "TxtPropertyName";
            this.TxtPropertyName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPropertyName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyName.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyName.Properties.MaxLength = 40;
            this.TxtPropertyName.Size = new System.Drawing.Size(263, 20);
            this.TxtPropertyName.TabIndex = 18;
            // 
            // TxtPropertyMutationDocNo
            // 
            this.TxtPropertyMutationDocNo.EnterMoveNextControl = true;
            this.TxtPropertyMutationDocNo.Location = new System.Drawing.Point(155, 94);
            this.TxtPropertyMutationDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyMutationDocNo.Name = "TxtPropertyMutationDocNo";
            this.TxtPropertyMutationDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPropertyMutationDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyMutationDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyMutationDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyMutationDocNo.Properties.MaxLength = 40;
            this.TxtPropertyMutationDocNo.Size = new System.Drawing.Size(263, 20);
            this.TxtPropertyMutationDocNo.TabIndex = 19;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(155, 50);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 40;
            this.TxtDisplayName.Size = new System.Drawing.Size(263, 20);
            this.TxtDisplayName.TabIndex = 20;
            // 
            // TxtParent
            // 
            this.TxtParent.EnterMoveNextControl = true;
            this.TxtParent.Location = new System.Drawing.Point(155, 72);
            this.TxtParent.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParent.Name = "TxtParent";
            this.TxtParent.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParent.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParent.Properties.Appearance.Options.UseFont = true;
            this.TxtParent.Properties.MaxLength = 40;
            this.TxtParent.Size = new System.Drawing.Size(263, 20);
            this.TxtParent.TabIndex = 21;
            // 
            // TxtExternalCode
            // 
            this.TxtExternalCode.EnterMoveNextControl = true;
            this.TxtExternalCode.Location = new System.Drawing.Point(155, 116);
            this.TxtExternalCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExternalCode.Name = "TxtExternalCode";
            this.TxtExternalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExternalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExternalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExternalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtExternalCode.Properties.MaxLength = 40;
            this.TxtExternalCode.Size = new System.Drawing.Size(263, 20);
            this.TxtExternalCode.TabIndex = 22;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(145, 5);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 40;
            this.TxtStatus.Size = new System.Drawing.Size(207, 20);
            this.TxtStatus.TabIndex = 23;
            // 
            // LblStatus
            // 
            this.LblStatus.AutoSize = true;
            this.LblStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblStatus.ForeColor = System.Drawing.Color.Black;
            this.LblStatus.Location = new System.Drawing.Point(100, 8);
            this.LblStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(42, 14);
            this.LblStatus.TabIndex = 24;
            this.LblStatus.Text = "Status";
            this.LblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblCancelReason
            // 
            this.LblCancelReason.AutoSize = true;
            this.LblCancelReason.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCancelReason.ForeColor = System.Drawing.Color.Black;
            this.LblCancelReason.Location = new System.Drawing.Point(57, 30);
            this.LblCancelReason.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCancelReason.Name = "LblCancelReason";
            this.LblCancelReason.Size = new System.Drawing.Size(85, 14);
            this.LblCancelReason.TabIndex = 26;
            this.LblCancelReason.Text = "Cancel Reason";
            this.LblCancelReason.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCancelReason
            // 
            this.TxtCancelReason.EnterMoveNextControl = true;
            this.TxtCancelReason.Location = new System.Drawing.Point(145, 27);
            this.TxtCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCancelReason.Name = "TxtCancelReason";
            this.TxtCancelReason.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCancelReason.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCancelReason.Properties.Appearance.Options.UseFont = true;
            this.TxtCancelReason.Properties.MaxLength = 40;
            this.TxtCancelReason.Size = new System.Drawing.Size(207, 20);
            this.TxtCancelReason.TabIndex = 25;
            // 
            // LblDeactivationReason
            // 
            this.LblDeactivationReason.AutoSize = true;
            this.LblDeactivationReason.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeactivationReason.ForeColor = System.Drawing.Color.Black;
            this.LblDeactivationReason.Location = new System.Drawing.Point(25, 52);
            this.LblDeactivationReason.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeactivationReason.Name = "LblDeactivationReason";
            this.LblDeactivationReason.Size = new System.Drawing.Size(117, 14);
            this.LblDeactivationReason.TabIndex = 28;
            this.LblDeactivationReason.Text = "Deactivation Reason";
            this.LblDeactivationReason.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeactivationReason
            // 
            this.TxtDeactivationReason.EnterMoveNextControl = true;
            this.TxtDeactivationReason.Location = new System.Drawing.Point(145, 49);
            this.TxtDeactivationReason.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeactivationReason.Name = "TxtDeactivationReason";
            this.TxtDeactivationReason.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeactivationReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeactivationReason.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeactivationReason.Properties.Appearance.Options.UseFont = true;
            this.TxtDeactivationReason.Properties.MaxLength = 40;
            this.TxtDeactivationReason.Size = new System.Drawing.Size(207, 20);
            this.TxtDeactivationReason.TabIndex = 27;
            // 
            // ChkCompleteInd
            // 
            this.ChkCompleteInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCompleteInd.Location = new System.Drawing.Point(361, 5);
            this.ChkCompleteInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCompleteInd.Name = "ChkCompleteInd";
            this.ChkCompleteInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCompleteInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCompleteInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCompleteInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCompleteInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCompleteInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCompleteInd.Properties.Caption = "Complete";
            this.ChkCompleteInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCompleteInd.Size = new System.Drawing.Size(111, 22);
            this.ChkCompleteInd.TabIndex = 29;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(361, 27);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(111, 22);
            this.ChkCancelInd.TabIndex = 30;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(361, 49);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(111, 22);
            this.ChkActInd.TabIndex = 31;
            // 
            // TcPropertyInventory
            // 
            this.TcPropertyInventory.Appearance.Options.UseTextOptions = true;
            this.TcPropertyInventory.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TcPropertyInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcPropertyInventory.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcPropertyInventory.Location = new System.Drawing.Point(0, 0);
            this.TcPropertyInventory.Name = "TcPropertyInventory";
            this.TcPropertyInventory.SelectedTabPage = this.TpGeneral;
            this.TcPropertyInventory.Size = new System.Drawing.Size(1103, 388);
            this.TcPropertyInventory.TabIndex = 32;
            this.TcPropertyInventory.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpGeneral,
            this.TpPropertyCostComponent,
            this.TpFairValue,
            this.TpMutationDocumentRef,
            this.TpPropertyTransferRef,
            this.TpCertificateInformation,
            this.TpHistoricalInformation,
            this.TpUploadFile,
            this.TpApprovalInformation});
            // 
            // TpGeneral
            // 
            this.TpGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGeneral.Controls.Add(this.BtnCCCode);
            this.TpGeneral.Controls.Add(this.TxtRemStockValue);
            this.TpGeneral.Controls.Add(this.LblRemainingStockValue);
            this.TpGeneral.Controls.Add(this.TxtRemStockQty);
            this.TpGeneral.Controls.Add(this.LblRemStockQty);
            this.TpGeneral.Controls.Add(this.TxtUPrice);
            this.TpGeneral.Controls.Add(this.LblUPrice);
            this.TpGeneral.Controls.Add(this.TxtInventoryQty);
            this.TpGeneral.Controls.Add(this.LblInventoryQty);
            this.TpGeneral.Controls.Add(this.TxtUoM);
            this.TpGeneral.Controls.Add(this.LblUoM);
            this.TpGeneral.Controls.Add(this.TxtPropertyInventoryValue);
            this.TpGeneral.Controls.Add(this.LblPropertyInventoryValue);
            this.TpGeneral.Controls.Add(this.BtnSiteCode);
            this.TpGeneral.Controls.Add(this.TxtCCCode);
            this.TpGeneral.Controls.Add(this.TxtShortCode);
            this.TpGeneral.Controls.Add(this.LblShortCode);
            this.TpGeneral.Controls.Add(this.LblPropertyCategoryCode);
            this.TpGeneral.Controls.Add(this.LuePropertyCategory);
            this.TpGeneral.Controls.Add(this.TxtItCode);
            this.TpGeneral.Controls.Add(this.LblRegistrationDt);
            this.TpGeneral.Controls.Add(this.DteRegistrationDt);
            this.TpGeneral.Controls.Add(this.TxtSiteCode);
            this.TpGeneral.Controls.Add(this.LblItem);
            this.TpGeneral.Controls.Add(this.LblSite);
            this.TpGeneral.Controls.Add(this.TxtItName);
            this.TpGeneral.Controls.Add(this.BtnItCode);
            this.TpGeneral.Controls.Add(this.TxtAcDesc);
            this.TpGeneral.Controls.Add(this.TxtAcNo);
            this.TpGeneral.Controls.Add(this.LblAcNo);
            this.TpGeneral.Controls.Add(this.label9);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Size = new System.Drawing.Size(1097, 360);
            this.TpGeneral.Text = "General";
            // 
            // BtnCCCode
            // 
            this.BtnCCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCCCode.Appearance.Options.UseBackColor = true;
            this.BtnCCCode.Appearance.Options.UseFont = true;
            this.BtnCCCode.Appearance.Options.UseForeColor = true;
            this.BtnCCCode.Appearance.Options.UseTextOptions = true;
            this.BtnCCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCCCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCCCode.Image")));
            this.BtnCCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCCCode.Location = new System.Drawing.Point(711, 91);
            this.BtnCCCode.Name = "BtnCCCode";
            this.BtnCCCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCCCode.TabIndex = 95;
            this.BtnCCCode.ToolTip = "Find Item";
            this.BtnCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCCCode.ToolTipTitle = "Run System";
            this.BtnCCCode.Click += new System.EventHandler(this.BtnCCCode_Click);
            // 
            // TxtRemStockValue
            // 
            this.TxtRemStockValue.EnterMoveNextControl = true;
            this.TxtRemStockValue.Location = new System.Drawing.Point(158, 298);
            this.TxtRemStockValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemStockValue.Name = "TxtRemStockValue";
            this.TxtRemStockValue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemStockValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemStockValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemStockValue.Properties.Appearance.Options.UseFont = true;
            this.TxtRemStockValue.Properties.MaxLength = 16;
            this.TxtRemStockValue.Properties.ReadOnly = true;
            this.TxtRemStockValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtRemStockValue.Size = new System.Drawing.Size(286, 20);
            this.TxtRemStockValue.TabIndex = 94;
            // 
            // LblRemainingStockValue
            // 
            this.LblRemainingStockValue.AutoSize = true;
            this.LblRemainingStockValue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemainingStockValue.ForeColor = System.Drawing.Color.Black;
            this.LblRemainingStockValue.Location = new System.Drawing.Point(23, 301);
            this.LblRemainingStockValue.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemainingStockValue.Name = "LblRemainingStockValue";
            this.LblRemainingStockValue.Size = new System.Drawing.Size(131, 14);
            this.LblRemainingStockValue.TabIndex = 93;
            this.LblRemainingStockValue.Text = "Remaining Stock Value";
            this.LblRemainingStockValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemStockQty
            // 
            this.TxtRemStockQty.EnterMoveNextControl = true;
            this.TxtRemStockQty.Location = new System.Drawing.Point(158, 276);
            this.TxtRemStockQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemStockQty.Name = "TxtRemStockQty";
            this.TxtRemStockQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemStockQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemStockQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemStockQty.Properties.Appearance.Options.UseFont = true;
            this.TxtRemStockQty.Properties.MaxLength = 16;
            this.TxtRemStockQty.Properties.ReadOnly = true;
            this.TxtRemStockQty.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtRemStockQty.Size = new System.Drawing.Size(286, 20);
            this.TxtRemStockQty.TabIndex = 92;
            // 
            // LblRemStockQty
            // 
            this.LblRemStockQty.AutoSize = true;
            this.LblRemStockQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemStockQty.ForeColor = System.Drawing.Color.Black;
            this.LblRemStockQty.Location = new System.Drawing.Point(6, 279);
            this.LblRemStockQty.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemStockQty.Name = "LblRemStockQty";
            this.LblRemStockQty.Size = new System.Drawing.Size(148, 14);
            this.LblRemStockQty.TabIndex = 91;
            this.LblRemStockQty.Text = "Remaining Stock Quantity";
            this.LblRemStockQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUPrice
            // 
            this.TxtUPrice.EnterMoveNextControl = true;
            this.TxtUPrice.Location = new System.Drawing.Point(158, 254);
            this.TxtUPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice.Name = "TxtUPrice";
            this.TxtUPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice.Properties.MaxLength = 16;
            this.TxtUPrice.Properties.ReadOnly = true;
            this.TxtUPrice.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtUPrice.Size = new System.Drawing.Size(286, 20);
            this.TxtUPrice.TabIndex = 90;
            // 
            // LblUPrice
            // 
            this.LblUPrice.AutoSize = true;
            this.LblUPrice.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUPrice.ForeColor = System.Drawing.Color.Black;
            this.LblUPrice.Location = new System.Drawing.Point(95, 257);
            this.LblUPrice.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblUPrice.Name = "LblUPrice";
            this.LblUPrice.Size = new System.Drawing.Size(59, 14);
            this.LblUPrice.TabIndex = 89;
            this.LblUPrice.Text = "Unit Price";
            this.LblUPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInventoryQty
            // 
            this.TxtInventoryQty.EnterMoveNextControl = true;
            this.TxtInventoryQty.Location = new System.Drawing.Point(158, 232);
            this.TxtInventoryQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryQty.Name = "TxtInventoryQty";
            this.TxtInventoryQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInventoryQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryQty.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryQty.Properties.MaxLength = 16;
            this.TxtInventoryQty.Properties.ReadOnly = true;
            this.TxtInventoryQty.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtInventoryQty.Size = new System.Drawing.Size(286, 20);
            this.TxtInventoryQty.TabIndex = 88;
            // 
            // LblInventoryQty
            // 
            this.LblInventoryQty.AutoSize = true;
            this.LblInventoryQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInventoryQty.ForeColor = System.Drawing.Color.Black;
            this.LblInventoryQty.Location = new System.Drawing.Point(43, 235);
            this.LblInventoryQty.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInventoryQty.Name = "LblInventoryQty";
            this.LblInventoryQty.Size = new System.Drawing.Size(111, 14);
            this.LblInventoryQty.TabIndex = 87;
            this.LblInventoryQty.Text = "Inventory Quantity";
            this.LblInventoryQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUoM
            // 
            this.TxtUoM.EnterMoveNextControl = true;
            this.TxtUoM.Location = new System.Drawing.Point(158, 210);
            this.TxtUoM.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUoM.Name = "TxtUoM";
            this.TxtUoM.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUoM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUoM.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUoM.Properties.Appearance.Options.UseFont = true;
            this.TxtUoM.Properties.MaxLength = 16;
            this.TxtUoM.Properties.ReadOnly = true;
            this.TxtUoM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtUoM.Size = new System.Drawing.Size(286, 20);
            this.TxtUoM.TabIndex = 86;
            // 
            // LblUoM
            // 
            this.LblUoM.AutoSize = true;
            this.LblUoM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUoM.ForeColor = System.Drawing.Color.Black;
            this.LblUoM.Location = new System.Drawing.Point(123, 213);
            this.LblUoM.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblUoM.Name = "LblUoM";
            this.LblUoM.Size = new System.Drawing.Size(31, 14);
            this.LblUoM.TabIndex = 85;
            this.LblUoM.Text = "UoM";
            this.LblUoM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropertyInventoryValue
            // 
            this.TxtPropertyInventoryValue.EnterMoveNextControl = true;
            this.TxtPropertyInventoryValue.Location = new System.Drawing.Point(158, 188);
            this.TxtPropertyInventoryValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropertyInventoryValue.Name = "TxtPropertyInventoryValue";
            this.TxtPropertyInventoryValue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPropertyInventoryValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyInventoryValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropertyInventoryValue.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyInventoryValue.Properties.MaxLength = 16;
            this.TxtPropertyInventoryValue.Properties.ReadOnly = true;
            this.TxtPropertyInventoryValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtPropertyInventoryValue.Size = new System.Drawing.Size(286, 20);
            this.TxtPropertyInventoryValue.TabIndex = 84;
            // 
            // LblPropertyInventoryValue
            // 
            this.LblPropertyInventoryValue.AutoSize = true;
            this.LblPropertyInventoryValue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyInventoryValue.ForeColor = System.Drawing.Color.Black;
            this.LblPropertyInventoryValue.Location = new System.Drawing.Point(9, 191);
            this.LblPropertyInventoryValue.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyInventoryValue.Name = "LblPropertyInventoryValue";
            this.LblPropertyInventoryValue.Size = new System.Drawing.Size(145, 14);
            this.LblPropertyInventoryValue.TabIndex = 83;
            this.LblPropertyInventoryValue.Text = "Property Inventory Value";
            this.LblPropertyInventoryValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSiteCode
            // 
            this.BtnSiteCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSiteCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSiteCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSiteCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSiteCode.Appearance.Options.UseBackColor = true;
            this.BtnSiteCode.Appearance.Options.UseFont = true;
            this.BtnSiteCode.Appearance.Options.UseForeColor = true;
            this.BtnSiteCode.Appearance.Options.UseTextOptions = true;
            this.BtnSiteCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSiteCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSiteCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnSiteCode.Image")));
            this.BtnSiteCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSiteCode.Location = new System.Drawing.Point(331, 157);
            this.BtnSiteCode.Name = "BtnSiteCode";
            this.BtnSiteCode.Size = new System.Drawing.Size(24, 21);
            this.BtnSiteCode.TabIndex = 82;
            this.BtnSiteCode.ToolTip = "Find Item";
            this.BtnSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSiteCode.ToolTipTitle = "Run System";
            this.BtnSiteCode.Click += new System.EventHandler(this.BtnSiteCode_Click);
            // 
            // TxtCCCode
            // 
            this.TxtCCCode.EnterMoveNextControl = true;
            this.TxtCCCode.Location = new System.Drawing.Point(158, 92);
            this.TxtCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCCCode.Name = "TxtCCCode";
            this.TxtCCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCode.Properties.MaxLength = 16;
            this.TxtCCCode.Properties.ReadOnly = true;
            this.TxtCCCode.Size = new System.Drawing.Size(550, 20);
            this.TxtCCCode.TabIndex = 81;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(158, 29);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 30;
            this.TxtShortCode.Size = new System.Drawing.Size(170, 20);
            this.TxtShortCode.TabIndex = 28;
            // 
            // LblShortCode
            // 
            this.LblShortCode.AutoSize = true;
            this.LblShortCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShortCode.ForeColor = System.Drawing.Color.Black;
            this.LblShortCode.Location = new System.Drawing.Point(85, 32);
            this.LblShortCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblShortCode.Name = "LblShortCode";
            this.LblShortCode.Size = new System.Drawing.Size(69, 14);
            this.LblShortCode.TabIndex = 27;
            this.LblShortCode.Text = "Short Code";
            this.LblShortCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPropertyCategoryCode
            // 
            this.LblPropertyCategoryCode.AutoSize = true;
            this.LblPropertyCategoryCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyCategoryCode.ForeColor = System.Drawing.Color.Red;
            this.LblPropertyCategoryCode.Location = new System.Drawing.Point(47, 11);
            this.LblPropertyCategoryCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyCategoryCode.Name = "LblPropertyCategoryCode";
            this.LblPropertyCategoryCode.Size = new System.Drawing.Size(107, 14);
            this.LblPropertyCategoryCode.TabIndex = 25;
            this.LblPropertyCategoryCode.Text = "Property Category";
            this.LblPropertyCategoryCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePropertyCategory
            // 
            this.LuePropertyCategory.EnterMoveNextControl = true;
            this.LuePropertyCategory.Location = new System.Drawing.Point(158, 8);
            this.LuePropertyCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LuePropertyCategory.Name = "LuePropertyCategory";
            this.LuePropertyCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.Appearance.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePropertyCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePropertyCategory.Properties.DropDownRows = 30;
            this.LuePropertyCategory.Properties.NullText = "[Empty]";
            this.LuePropertyCategory.Properties.PopupWidth = 450;
            this.LuePropertyCategory.Size = new System.Drawing.Size(432, 20);
            this.LuePropertyCategory.TabIndex = 26;
            this.LuePropertyCategory.ToolTip = "F4 : Show/hide list";
            this.LuePropertyCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePropertyCategory.EditValueChanged += new System.EventHandler(this.LuePropertyCategory_EditValueChanged);
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(158, 50);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(170, 20);
            this.TxtItCode.TabIndex = 30;
            // 
            // LblRegistrationDt
            // 
            this.LblRegistrationDt.AutoSize = true;
            this.LblRegistrationDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRegistrationDt.ForeColor = System.Drawing.Color.Red;
            this.LblRegistrationDt.Location = new System.Drawing.Point(38, 74);
            this.LblRegistrationDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblRegistrationDt.Name = "LblRegistrationDt";
            this.LblRegistrationDt.Size = new System.Drawing.Size(116, 14);
            this.LblRegistrationDt.TabIndex = 33;
            this.LblRegistrationDt.Text = "Date of Registration";
            this.LblRegistrationDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteRegistrationDt
            // 
            this.DteRegistrationDt.EditValue = null;
            this.DteRegistrationDt.EnterMoveNextControl = true;
            this.DteRegistrationDt.Location = new System.Drawing.Point(158, 71);
            this.DteRegistrationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRegistrationDt.Name = "DteRegistrationDt";
            this.DteRegistrationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRegistrationDt.Properties.Appearance.Options.UseFont = true;
            this.DteRegistrationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRegistrationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRegistrationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRegistrationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRegistrationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRegistrationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRegistrationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRegistrationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRegistrationDt.Properties.MaxLength = 8;
            this.DteRegistrationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRegistrationDt.Size = new System.Drawing.Size(170, 20);
            this.DteRegistrationDt.TabIndex = 33;
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(158, 158);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 16;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(170, 20);
            this.TxtSiteCode.TabIndex = 52;
            // 
            // LblItem
            // 
            this.LblItem.AutoSize = true;
            this.LblItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItem.ForeColor = System.Drawing.Color.Red;
            this.LblItem.Location = new System.Drawing.Point(121, 53);
            this.LblItem.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItem.Name = "LblItem";
            this.LblItem.Size = new System.Drawing.Size(33, 14);
            this.LblItem.TabIndex = 29;
            this.LblItem.Text = "Item";
            this.LblItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSite
            // 
            this.LblSite.AutoSize = true;
            this.LblSite.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSite.ForeColor = System.Drawing.Color.Red;
            this.LblSite.Location = new System.Drawing.Point(126, 160);
            this.LblSite.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSite.Name = "LblSite";
            this.LblSite.Size = new System.Drawing.Size(28, 14);
            this.LblSite.TabIndex = 51;
            this.LblSite.Text = "Site";
            this.LblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(356, 49);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(352, 20);
            this.TxtItName.TabIndex = 32;
            // 
            // BtnItCode
            // 
            this.BtnItCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode.Appearance.Options.UseBackColor = true;
            this.BtnItCode.Appearance.Options.UseFont = true;
            this.BtnItCode.Appearance.Options.UseForeColor = true;
            this.BtnItCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode.Image")));
            this.BtnItCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode.Location = new System.Drawing.Point(331, 49);
            this.BtnItCode.Name = "BtnItCode";
            this.BtnItCode.Size = new System.Drawing.Size(24, 21);
            this.BtnItCode.TabIndex = 31;
            this.BtnItCode.ToolTip = "Find Item";
            this.BtnItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode.ToolTipTitle = "Run System";
            this.BtnItCode.Click += new System.EventHandler(this.BtnItCode_Click);
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(158, 136);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 16;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(550, 20);
            this.TxtAcDesc.TabIndex = 50;
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(158, 114);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 16;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(286, 20);
            this.TxtAcNo.TabIndex = 48;
            // 
            // LblAcNo
            // 
            this.LblAcNo.AutoSize = true;
            this.LblAcNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo.Location = new System.Drawing.Point(52, 117);
            this.LblAcNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo.Name = "LblAcNo";
            this.LblAcNo.Size = new System.Drawing.Size(102, 14);
            this.LblAcNo.TabIndex = 47;
            this.LblAcNo.Text = "COA\'s Account# ";
            this.LblAcNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(50, 94);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 14);
            this.label9.TabIndex = 45;
            this.label9.Text = "Initial Cost Center";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpPropertyCostComponent
            // 
            this.TpPropertyCostComponent.Appearance.Header.Options.UseTextOptions = true;
            this.TpPropertyCostComponent.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpPropertyCostComponent.Controls.Add(this.Grd1);
            this.TpPropertyCostComponent.Name = "TpPropertyCostComponent";
            this.TpPropertyCostComponent.Size = new System.Drawing.Size(1097, 360);
            this.TpPropertyCostComponent.Text = "Property Cost Component";
            // 
            // Grd1
            // 
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(1097, 360);
            this.Grd1.TabIndex = 21;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.GrdEllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.GrdRequestEdit);
            // 
            // TpFairValue
            // 
            this.TpFairValue.Controls.Add(this.Grd7);
            this.TpFairValue.Name = "TpFairValue";
            this.TpFairValue.Size = new System.Drawing.Size(1097, 360);
            this.TpFairValue.Text = "Fair Value Cost Component";
            // 
            // Grd7
            // 
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.UseXPStyles = false;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(1097, 360);
            this.Grd7.TabIndex = 22;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd7_EllipsisButtonClick);
            // 
            // TpMutationDocumentRef
            // 
            this.TpMutationDocumentRef.Appearance.Header.Options.UseTextOptions = true;
            this.TpMutationDocumentRef.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpMutationDocumentRef.Controls.Add(this.Grd2);
            this.TpMutationDocumentRef.Name = "TpMutationDocumentRef";
            this.TpMutationDocumentRef.Size = new System.Drawing.Size(1097, 360);
            this.TpMutationDocumentRef.Text = "Mutation Document Ref.";
            // 
            // Grd2
            // 
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(1097, 360);
            this.Grd2.TabIndex = 21;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            // 
            // TpPropertyTransferRef
            // 
            this.TpPropertyTransferRef.Appearance.Header.Options.UseTextOptions = true;
            this.TpPropertyTransferRef.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpPropertyTransferRef.Controls.Add(this.Grd3);
            this.TpPropertyTransferRef.Name = "TpPropertyTransferRef";
            this.TpPropertyTransferRef.Size = new System.Drawing.Size(1097, 360);
            this.TpPropertyTransferRef.Text = "Property Transfer Ref";
            // 
            // Grd3
            // 
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.UseXPStyles = false;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(1097, 360);
            this.Grd3.TabIndex = 22;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            // 
            // TpCertificateInformation
            // 
            this.TpCertificateInformation.Appearance.Header.Options.UseTextOptions = true;
            this.TpCertificateInformation.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCertificateInformation.Controls.Add(this.MeeRegistrationBasis);
            this.TpCertificateInformation.Controls.Add(this.MeeCertExpDt);
            this.TpCertificateInformation.Controls.Add(this.TxtAreaBasedOnSurveyDoc);
            this.TpCertificateInformation.Controls.Add(this.LblAreaBasedOnSurveyDoc);
            this.TpCertificateInformation.Controls.Add(this.TxtSurveyDocDt);
            this.TpCertificateInformation.Controls.Add(this.LblSurveyDocDt);
            this.TpCertificateInformation.Controls.Add(this.TxtSurveyDocNo);
            this.TpCertificateInformation.Controls.Add(this.LblSurveyDocNo);
            this.TpCertificateInformation.Controls.Add(this.TxtBasisDocumentDt);
            this.TpCertificateInformation.Controls.Add(this.LblBasisDocumentDt);
            this.TpCertificateInformation.Controls.Add(this.TxtRegistrationBasis);
            this.TpCertificateInformation.Controls.Add(this.LblRegistrationBasis);
            this.TpCertificateInformation.Controls.Add(this.TxtNIB);
            this.TpCertificateInformation.Controls.Add(this.LblNIB);
            this.TpCertificateInformation.Controls.Add(this.TxtCertificateIssuedLoc);
            this.TpCertificateInformation.Controls.Add(this.LblCertificateIssuedLoc);
            this.TpCertificateInformation.Controls.Add(this.TxtCertificateExpiredDt);
            this.TpCertificateInformation.Controls.Add(this.LblCertificateExpiredDt);
            this.TpCertificateInformation.Controls.Add(this.TxtCertificateIssuedDt);
            this.TpCertificateInformation.Controls.Add(this.LblCertificateIssuedDt);
            this.TpCertificateInformation.Controls.Add(this.TxtCertificateNo);
            this.TpCertificateInformation.Controls.Add(this.LblCertificateNo);
            this.TpCertificateInformation.Controls.Add(this.TxtCertificateType);
            this.TpCertificateInformation.Controls.Add(this.LblCertificateType);
            this.TpCertificateInformation.Name = "TpCertificateInformation";
            this.TpCertificateInformation.Size = new System.Drawing.Size(1097, 360);
            this.TpCertificateInformation.Text = "Certificate Information";
            // 
            // MeeRegistrationBasis
            // 
            this.MeeRegistrationBasis.EnterMoveNextControl = true;
            this.MeeRegistrationBasis.Location = new System.Drawing.Point(205, 145);
            this.MeeRegistrationBasis.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRegistrationBasis.Name = "MeeRegistrationBasis";
            this.MeeRegistrationBasis.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeRegistrationBasis.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.Appearance.Options.UseBackColor = true;
            this.MeeRegistrationBasis.Properties.Appearance.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRegistrationBasis.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRegistrationBasis.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRegistrationBasis.Properties.MaxLength = 1000;
            this.MeeRegistrationBasis.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRegistrationBasis.Properties.ReadOnly = true;
            this.MeeRegistrationBasis.Properties.ShowIcon = false;
            this.MeeRegistrationBasis.Size = new System.Drawing.Size(286, 22);
            this.MeeRegistrationBasis.TabIndex = 108;
            this.MeeRegistrationBasis.ToolTip = "F4 : Show/hide text";
            this.MeeRegistrationBasis.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRegistrationBasis.ToolTipTitle = "Run System";
            // 
            // MeeCertExpDt
            // 
            this.MeeCertExpDt.EnterMoveNextControl = true;
            this.MeeCertExpDt.Location = new System.Drawing.Point(205, 76);
            this.MeeCertExpDt.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCertExpDt.Name = "MeeCertExpDt";
            this.MeeCertExpDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeCertExpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.Appearance.Options.UseBackColor = true;
            this.MeeCertExpDt.Properties.Appearance.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCertExpDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCertExpDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCertExpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCertExpDt.Properties.MaxLength = 1000;
            this.MeeCertExpDt.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCertExpDt.Properties.ReadOnly = true;
            this.MeeCertExpDt.Properties.ShowIcon = false;
            this.MeeCertExpDt.Size = new System.Drawing.Size(286, 22);
            this.MeeCertExpDt.TabIndex = 107;
            this.MeeCertExpDt.ToolTip = "F4 : Show/hide text";
            this.MeeCertExpDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCertExpDt.ToolTipTitle = "Run System";
            // 
            // TxtAreaBasedOnSurveyDoc
            // 
            this.TxtAreaBasedOnSurveyDoc.EnterMoveNextControl = true;
            this.TxtAreaBasedOnSurveyDoc.Location = new System.Drawing.Point(205, 238);
            this.TxtAreaBasedOnSurveyDoc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAreaBasedOnSurveyDoc.Name = "TxtAreaBasedOnSurveyDoc";
            this.TxtAreaBasedOnSurveyDoc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAreaBasedOnSurveyDoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAreaBasedOnSurveyDoc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAreaBasedOnSurveyDoc.Properties.Appearance.Options.UseFont = true;
            this.TxtAreaBasedOnSurveyDoc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAreaBasedOnSurveyDoc.Properties.MaxLength = 16;
            this.TxtAreaBasedOnSurveyDoc.Properties.ReadOnly = true;
            this.TxtAreaBasedOnSurveyDoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtAreaBasedOnSurveyDoc.Size = new System.Drawing.Size(286, 20);
            this.TxtAreaBasedOnSurveyDoc.TabIndex = 106;
            // 
            // LblAreaBasedOnSurveyDoc
            // 
            this.LblAreaBasedOnSurveyDoc.AutoSize = true;
            this.LblAreaBasedOnSurveyDoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAreaBasedOnSurveyDoc.ForeColor = System.Drawing.Color.Black;
            this.LblAreaBasedOnSurveyDoc.Location = new System.Drawing.Point(7, 241);
            this.LblAreaBasedOnSurveyDoc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAreaBasedOnSurveyDoc.Name = "LblAreaBasedOnSurveyDoc";
            this.LblAreaBasedOnSurveyDoc.Size = new System.Drawing.Size(193, 14);
            this.LblAreaBasedOnSurveyDoc.TabIndex = 105;
            this.LblAreaBasedOnSurveyDoc.Text = "Luas Berdasarkan Surat Ukur (m2)";
            this.LblAreaBasedOnSurveyDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSurveyDocDt
            // 
            this.TxtSurveyDocDt.EnterMoveNextControl = true;
            this.TxtSurveyDocDt.Location = new System.Drawing.Point(205, 215);
            this.TxtSurveyDocDt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSurveyDocDt.Name = "TxtSurveyDocDt";
            this.TxtSurveyDocDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSurveyDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSurveyDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSurveyDocDt.Properties.Appearance.Options.UseFont = true;
            this.TxtSurveyDocDt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSurveyDocDt.Properties.MaxLength = 16;
            this.TxtSurveyDocDt.Properties.ReadOnly = true;
            this.TxtSurveyDocDt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSurveyDocDt.Size = new System.Drawing.Size(286, 20);
            this.TxtSurveyDocDt.TabIndex = 104;
            // 
            // LblSurveyDocDt
            // 
            this.LblSurveyDocDt.AutoSize = true;
            this.LblSurveyDocDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSurveyDocDt.ForeColor = System.Drawing.Color.Black;
            this.LblSurveyDocDt.Location = new System.Drawing.Point(88, 218);
            this.LblSurveyDocDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSurveyDocDt.Name = "LblSurveyDocDt";
            this.LblSurveyDocDt.Size = new System.Drawing.Size(112, 14);
            this.LblSurveyDocDt.TabIndex = 103;
            this.LblSurveyDocDt.Text = "Tanggal Surat Ukur";
            this.LblSurveyDocDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSurveyDocNo
            // 
            this.TxtSurveyDocNo.EnterMoveNextControl = true;
            this.TxtSurveyDocNo.Location = new System.Drawing.Point(205, 192);
            this.TxtSurveyDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSurveyDocNo.Name = "TxtSurveyDocNo";
            this.TxtSurveyDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSurveyDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSurveyDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSurveyDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSurveyDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSurveyDocNo.Properties.MaxLength = 16;
            this.TxtSurveyDocNo.Properties.ReadOnly = true;
            this.TxtSurveyDocNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtSurveyDocNo.Size = new System.Drawing.Size(286, 20);
            this.TxtSurveyDocNo.TabIndex = 102;
            // 
            // LblSurveyDocNo
            // 
            this.LblSurveyDocNo.AutoSize = true;
            this.LblSurveyDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSurveyDocNo.ForeColor = System.Drawing.Color.Black;
            this.LblSurveyDocNo.Location = new System.Drawing.Point(112, 195);
            this.LblSurveyDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSurveyDocNo.Name = "LblSurveyDocNo";
            this.LblSurveyDocNo.Size = new System.Drawing.Size(88, 14);
            this.LblSurveyDocNo.TabIndex = 101;
            this.LblSurveyDocNo.Text = "No. Surat Ukur";
            this.LblSurveyDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBasisDocumentDt
            // 
            this.TxtBasisDocumentDt.EnterMoveNextControl = true;
            this.TxtBasisDocumentDt.Location = new System.Drawing.Point(205, 169);
            this.TxtBasisDocumentDt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBasisDocumentDt.Name = "TxtBasisDocumentDt";
            this.TxtBasisDocumentDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBasisDocumentDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBasisDocumentDt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBasisDocumentDt.Properties.Appearance.Options.UseFont = true;
            this.TxtBasisDocumentDt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBasisDocumentDt.Properties.MaxLength = 16;
            this.TxtBasisDocumentDt.Properties.ReadOnly = true;
            this.TxtBasisDocumentDt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtBasisDocumentDt.Size = new System.Drawing.Size(286, 20);
            this.TxtBasisDocumentDt.TabIndex = 100;
            // 
            // LblBasisDocumentDt
            // 
            this.LblBasisDocumentDt.AutoSize = true;
            this.LblBasisDocumentDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBasisDocumentDt.ForeColor = System.Drawing.Color.Black;
            this.LblBasisDocumentDt.Location = new System.Drawing.Point(47, 172);
            this.LblBasisDocumentDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBasisDocumentDt.Name = "LblBasisDocumentDt";
            this.LblBasisDocumentDt.Size = new System.Drawing.Size(153, 14);
            this.LblBasisDocumentDt.TabIndex = 99;
            this.LblBasisDocumentDt.Text = "Tanggal Dasar Pendaftaran";
            this.LblBasisDocumentDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRegistrationBasis
            // 
            this.TxtRegistrationBasis.EnterMoveNextControl = true;
            this.TxtRegistrationBasis.Location = new System.Drawing.Point(562, 146);
            this.TxtRegistrationBasis.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRegistrationBasis.Name = "TxtRegistrationBasis";
            this.TxtRegistrationBasis.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRegistrationBasis.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRegistrationBasis.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRegistrationBasis.Properties.Appearance.Options.UseFont = true;
            this.TxtRegistrationBasis.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRegistrationBasis.Properties.MaxLength = 16;
            this.TxtRegistrationBasis.Properties.ReadOnly = true;
            this.TxtRegistrationBasis.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtRegistrationBasis.Size = new System.Drawing.Size(286, 20);
            this.TxtRegistrationBasis.TabIndex = 98;
            this.TxtRegistrationBasis.Visible = false;
            // 
            // LblRegistrationBasis
            // 
            this.LblRegistrationBasis.AutoSize = true;
            this.LblRegistrationBasis.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRegistrationBasis.ForeColor = System.Drawing.Color.Black;
            this.LblRegistrationBasis.Location = new System.Drawing.Point(94, 149);
            this.LblRegistrationBasis.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRegistrationBasis.Name = "LblRegistrationBasis";
            this.LblRegistrationBasis.Size = new System.Drawing.Size(106, 14);
            this.LblRegistrationBasis.TabIndex = 97;
            this.LblRegistrationBasis.Text = "Dasar Pendaftaran";
            this.LblRegistrationBasis.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNIB
            // 
            this.TxtNIB.EnterMoveNextControl = true;
            this.TxtNIB.Location = new System.Drawing.Point(205, 123);
            this.TxtNIB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNIB.Name = "TxtNIB";
            this.TxtNIB.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNIB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNIB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNIB.Properties.Appearance.Options.UseFont = true;
            this.TxtNIB.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNIB.Properties.MaxLength = 16;
            this.TxtNIB.Properties.ReadOnly = true;
            this.TxtNIB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtNIB.Size = new System.Drawing.Size(286, 20);
            this.TxtNIB.TabIndex = 96;
            // 
            // LblNIB
            // 
            this.LblNIB.AutoSize = true;
            this.LblNIB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNIB.ForeColor = System.Drawing.Color.Black;
            this.LblNIB.Location = new System.Drawing.Point(23, 126);
            this.LblNIB.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblNIB.Name = "LblNIB";
            this.LblNIB.Size = new System.Drawing.Size(177, 14);
            this.LblNIB.TabIndex = 95;
            this.LblNIB.Text = "Nomor Identifikasi Bidang (NIB)";
            this.LblNIB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertificateIssuedLoc
            // 
            this.TxtCertificateIssuedLoc.EnterMoveNextControl = true;
            this.TxtCertificateIssuedLoc.Location = new System.Drawing.Point(205, 100);
            this.TxtCertificateIssuedLoc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCertificateIssuedLoc.Name = "TxtCertificateIssuedLoc";
            this.TxtCertificateIssuedLoc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCertificateIssuedLoc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertificateIssuedLoc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertificateIssuedLoc.Properties.Appearance.Options.UseFont = true;
            this.TxtCertificateIssuedLoc.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCertificateIssuedLoc.Properties.MaxLength = 16;
            this.TxtCertificateIssuedLoc.Properties.ReadOnly = true;
            this.TxtCertificateIssuedLoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCertificateIssuedLoc.Size = new System.Drawing.Size(286, 20);
            this.TxtCertificateIssuedLoc.TabIndex = 94;
            // 
            // LblCertificateIssuedLoc
            // 
            this.LblCertificateIssuedLoc.AutoSize = true;
            this.LblCertificateIssuedLoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificateIssuedLoc.ForeColor = System.Drawing.Color.Black;
            this.LblCertificateIssuedLoc.Location = new System.Drawing.Point(125, 103);
            this.LblCertificateIssuedLoc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCertificateIssuedLoc.Name = "LblCertificateIssuedLoc";
            this.LblCertificateIssuedLoc.Size = new System.Drawing.Size(75, 14);
            this.LblCertificateIssuedLoc.TabIndex = 93;
            this.LblCertificateIssuedLoc.Text = "Letak Tanah";
            this.LblCertificateIssuedLoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertificateExpiredDt
            // 
            this.TxtCertificateExpiredDt.EnterMoveNextControl = true;
            this.TxtCertificateExpiredDt.Location = new System.Drawing.Point(552, 74);
            this.TxtCertificateExpiredDt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCertificateExpiredDt.Name = "TxtCertificateExpiredDt";
            this.TxtCertificateExpiredDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCertificateExpiredDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertificateExpiredDt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertificateExpiredDt.Properties.Appearance.Options.UseFont = true;
            this.TxtCertificateExpiredDt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCertificateExpiredDt.Properties.MaxLength = 16;
            this.TxtCertificateExpiredDt.Properties.ReadOnly = true;
            this.TxtCertificateExpiredDt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCertificateExpiredDt.Size = new System.Drawing.Size(286, 20);
            this.TxtCertificateExpiredDt.TabIndex = 92;
            this.TxtCertificateExpiredDt.Visible = false;
            // 
            // LblCertificateExpiredDt
            // 
            this.LblCertificateExpiredDt.AutoSize = true;
            this.LblCertificateExpiredDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificateExpiredDt.ForeColor = System.Drawing.Color.Black;
            this.LblCertificateExpiredDt.Location = new System.Drawing.Point(60, 80);
            this.LblCertificateExpiredDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCertificateExpiredDt.Name = "LblCertificateExpiredDt";
            this.LblCertificateExpiredDt.Size = new System.Drawing.Size(140, 14);
            this.LblCertificateExpiredDt.TabIndex = 91;
            this.LblCertificateExpiredDt.Text = "Tanggal Berakhirnya Hak";
            this.LblCertificateExpiredDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertificateIssuedDt
            // 
            this.TxtCertificateIssuedDt.EnterMoveNextControl = true;
            this.TxtCertificateIssuedDt.Location = new System.Drawing.Point(205, 54);
            this.TxtCertificateIssuedDt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCertificateIssuedDt.Name = "TxtCertificateIssuedDt";
            this.TxtCertificateIssuedDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCertificateIssuedDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertificateIssuedDt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertificateIssuedDt.Properties.Appearance.Options.UseFont = true;
            this.TxtCertificateIssuedDt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCertificateIssuedDt.Properties.MaxLength = 16;
            this.TxtCertificateIssuedDt.Properties.ReadOnly = true;
            this.TxtCertificateIssuedDt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCertificateIssuedDt.Size = new System.Drawing.Size(286, 20);
            this.TxtCertificateIssuedDt.TabIndex = 90;
            // 
            // LblCertificateIssuedDt
            // 
            this.LblCertificateIssuedDt.AutoSize = true;
            this.LblCertificateIssuedDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificateIssuedDt.ForeColor = System.Drawing.Color.Black;
            this.LblCertificateIssuedDt.Location = new System.Drawing.Point(79, 57);
            this.LblCertificateIssuedDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCertificateIssuedDt.Name = "LblCertificateIssuedDt";
            this.LblCertificateIssuedDt.Size = new System.Drawing.Size(121, 14);
            this.LblCertificateIssuedDt.TabIndex = 89;
            this.LblCertificateIssuedDt.Text = "Penerbitan Sertipikat";
            this.LblCertificateIssuedDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertificateNo
            // 
            this.TxtCertificateNo.EnterMoveNextControl = true;
            this.TxtCertificateNo.Location = new System.Drawing.Point(205, 31);
            this.TxtCertificateNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCertificateNo.Name = "TxtCertificateNo";
            this.TxtCertificateNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCertificateNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertificateNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertificateNo.Properties.Appearance.Options.UseFont = true;
            this.TxtCertificateNo.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCertificateNo.Properties.MaxLength = 16;
            this.TxtCertificateNo.Properties.ReadOnly = true;
            this.TxtCertificateNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCertificateNo.Size = new System.Drawing.Size(286, 20);
            this.TxtCertificateNo.TabIndex = 88;
            // 
            // LblCertificateNo
            // 
            this.LblCertificateNo.AutoSize = true;
            this.LblCertificateNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificateNo.ForeColor = System.Drawing.Color.Black;
            this.LblCertificateNo.Location = new System.Drawing.Point(119, 34);
            this.LblCertificateNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCertificateNo.Name = "LblCertificateNo";
            this.LblCertificateNo.Size = new System.Drawing.Size(81, 14);
            this.LblCertificateNo.TabIndex = 87;
            this.LblCertificateNo.Text = "No. Sertipikat";
            this.LblCertificateNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCertificateType
            // 
            this.TxtCertificateType.EnterMoveNextControl = true;
            this.TxtCertificateType.Location = new System.Drawing.Point(205, 8);
            this.TxtCertificateType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCertificateType.Name = "TxtCertificateType";
            this.TxtCertificateType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCertificateType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCertificateType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCertificateType.Properties.Appearance.Options.UseFont = true;
            this.TxtCertificateType.Properties.MaxLength = 16;
            this.TxtCertificateType.Properties.ReadOnly = true;
            this.TxtCertificateType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtCertificateType.Size = new System.Drawing.Size(286, 20);
            this.TxtCertificateType.TabIndex = 86;
            // 
            // LblCertificateType
            // 
            this.LblCertificateType.AutoSize = true;
            this.LblCertificateType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificateType.ForeColor = System.Drawing.Color.Black;
            this.LblCertificateType.Location = new System.Drawing.Point(112, 11);
            this.LblCertificateType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCertificateType.Name = "LblCertificateType";
            this.LblCertificateType.Size = new System.Drawing.Size(88, 14);
            this.LblCertificateType.TabIndex = 85;
            this.LblCertificateType.Text = "Jenis Sertipikat";
            this.LblCertificateType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpHistoricalInformation
            // 
            this.TpHistoricalInformation.Appearance.Header.Options.UseTextOptions = true;
            this.TpHistoricalInformation.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpHistoricalInformation.Controls.Add(this.MeeVillage);
            this.TpHistoricalInformation.Controls.Add(this.MeeDistrict);
            this.TpHistoricalInformation.Controls.Add(this.MeeRightsReleaseBasis);
            this.TpHistoricalInformation.Controls.Add(this.TxtSpatialInfoPage);
            this.TpHistoricalInformation.Controls.Add(this.LblSpatialInfoPage);
            this.TpHistoricalInformation.Controls.Add(this.TxtIndicativePotentialArea);
            this.TpHistoricalInformation.Controls.Add(this.LblIndicativePotentialArea);
            this.TpHistoricalInformation.Controls.Add(this.LblRightsReleaseBasis);
            this.TpHistoricalInformation.Controls.Add(this.TxtDecreeNo);
            this.TpHistoricalInformation.Controls.Add(this.LblDecreeNo);
            this.TpHistoricalInformation.Controls.Add(this.DteDecreeDt);
            this.TpHistoricalInformation.Controls.Add(this.LblDecreeDt);
            this.TpHistoricalInformation.Controls.Add(this.DteRightsExpiredDt);
            this.TpHistoricalInformation.Controls.Add(this.LblRightsExpiredDt);
            this.TpHistoricalInformation.Controls.Add(this.TxtRightsHolder);
            this.TpHistoricalInformation.Controls.Add(this.LblRightsHolder);
            this.TpHistoricalInformation.Controls.Add(this.DteCertificatePublishDt);
            this.TpHistoricalInformation.Controls.Add(this.TxtRightsNumberAndNIB);
            this.TpHistoricalInformation.Controls.Add(this.TxtVillage);
            this.TpHistoricalInformation.Controls.Add(this.TxtDistrict);
            this.TpHistoricalInformation.Controls.Add(this.LblCertificatePublishDt);
            this.TpHistoricalInformation.Controls.Add(this.LblRightsNumberAndNIB);
            this.TpHistoricalInformation.Controls.Add(this.LblVillage);
            this.TpHistoricalInformation.Controls.Add(this.LblDistrict);
            this.TpHistoricalInformation.Controls.Add(this.LblCity);
            this.TpHistoricalInformation.Controls.Add(this.LueCity);
            this.TpHistoricalInformation.Controls.Add(this.LblProvince);
            this.TpHistoricalInformation.Controls.Add(this.LueProvince);
            this.TpHistoricalInformation.Controls.Add(this.LblAcquisitionType);
            this.TpHistoricalInformation.Controls.Add(this.LueAcquisitionType);
            this.TpHistoricalInformation.Name = "TpHistoricalInformation";
            this.TpHistoricalInformation.Size = new System.Drawing.Size(1097, 360);
            this.TpHistoricalInformation.Text = "Historical Information";
            // 
            // MeeVillage
            // 
            this.MeeVillage.EnterMoveNextControl = true;
            this.MeeVillage.Location = new System.Drawing.Point(196, 96);
            this.MeeVillage.Margin = new System.Windows.Forms.Padding(5);
            this.MeeVillage.Name = "MeeVillage";
            this.MeeVillage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.MeeVillage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVillage.Properties.Appearance.Options.UseBackColor = true;
            this.MeeVillage.Properties.Appearance.Options.UseFont = true;
            this.MeeVillage.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVillage.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeVillage.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVillage.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeVillage.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVillage.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeVillage.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeVillage.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeVillage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeVillage.Properties.MaxLength = 1000;
            this.MeeVillage.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeVillage.Properties.ShowIcon = false;
            this.MeeVillage.Size = new System.Drawing.Size(298, 20);
            this.MeeVillage.TabIndex = 109;
            this.MeeVillage.ToolTip = "F4 : Show/hide text";
            this.MeeVillage.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeVillage.ToolTipTitle = "Run System";
            // 
            // MeeDistrict
            // 
            this.MeeDistrict.EnterMoveNextControl = true;
            this.MeeDistrict.Location = new System.Drawing.Point(196, 73);
            this.MeeDistrict.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDistrict.Name = "MeeDistrict";
            this.MeeDistrict.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.MeeDistrict.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDistrict.Properties.Appearance.Options.UseBackColor = true;
            this.MeeDistrict.Properties.Appearance.Options.UseFont = true;
            this.MeeDistrict.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDistrict.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDistrict.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDistrict.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDistrict.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDistrict.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDistrict.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDistrict.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDistrict.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDistrict.Properties.MaxLength = 1000;
            this.MeeDistrict.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDistrict.Properties.ShowIcon = false;
            this.MeeDistrict.Size = new System.Drawing.Size(298, 20);
            this.MeeDistrict.TabIndex = 108;
            this.MeeDistrict.ToolTip = "F4 : Show/hide text";
            this.MeeDistrict.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDistrict.ToolTipTitle = "Run System";
            // 
            // MeeRightsReleaseBasis
            // 
            this.MeeRightsReleaseBasis.EditValue = "";
            this.MeeRightsReleaseBasis.EnterMoveNextControl = true;
            this.MeeRightsReleaseBasis.Location = new System.Drawing.Point(196, 250);
            this.MeeRightsReleaseBasis.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRightsReleaseBasis.Name = "MeeRightsReleaseBasis";
            this.MeeRightsReleaseBasis.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRightsReleaseBasis.Properties.Appearance.Options.UseFont = true;
            this.MeeRightsReleaseBasis.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRightsReleaseBasis.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRightsReleaseBasis.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRightsReleaseBasis.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRightsReleaseBasis.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRightsReleaseBasis.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRightsReleaseBasis.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRightsReleaseBasis.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRightsReleaseBasis.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRightsReleaseBasis.Properties.MaxLength = 1000;
            this.MeeRightsReleaseBasis.Properties.PopupFormSize = new System.Drawing.Size(600, 70);
            this.MeeRightsReleaseBasis.Properties.ShowIcon = false;
            this.MeeRightsReleaseBasis.Size = new System.Drawing.Size(298, 20);
            this.MeeRightsReleaseBasis.TabIndex = 60;
            this.MeeRightsReleaseBasis.ToolTip = "F4 : Show/hide text";
            this.MeeRightsReleaseBasis.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRightsReleaseBasis.ToolTipTitle = "Run System";
            // 
            // TxtSpatialInfoPage
            // 
            this.TxtSpatialInfoPage.EnterMoveNextControl = true;
            this.TxtSpatialInfoPage.Location = new System.Drawing.Point(196, 295);
            this.TxtSpatialInfoPage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSpatialInfoPage.Name = "TxtSpatialInfoPage";
            this.TxtSpatialInfoPage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSpatialInfoPage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpatialInfoPage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpatialInfoPage.Properties.Appearance.Options.UseFont = true;
            this.TxtSpatialInfoPage.Properties.MaxLength = 400;
            this.TxtSpatialInfoPage.Size = new System.Drawing.Size(298, 20);
            this.TxtSpatialInfoPage.TabIndex = 59;
            // 
            // LblSpatialInfoPage
            // 
            this.LblSpatialInfoPage.AutoSize = true;
            this.LblSpatialInfoPage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSpatialInfoPage.ForeColor = System.Drawing.Color.Black;
            this.LblSpatialInfoPage.Location = new System.Drawing.Point(89, 298);
            this.LblSpatialInfoPage.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSpatialInfoPage.Name = "LblSpatialInfoPage";
            this.LblSpatialInfoPage.Size = new System.Drawing.Size(103, 14);
            this.LblSpatialInfoPage.TabIndex = 58;
            this.LblSpatialInfoPage.Text = "Spatial Info. Page";
            this.LblSpatialInfoPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIndicativePotentialArea
            // 
            this.TxtIndicativePotentialArea.EnterMoveNextControl = true;
            this.TxtIndicativePotentialArea.Location = new System.Drawing.Point(196, 272);
            this.TxtIndicativePotentialArea.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndicativePotentialArea.Name = "TxtIndicativePotentialArea";
            this.TxtIndicativePotentialArea.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIndicativePotentialArea.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndicativePotentialArea.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndicativePotentialArea.Properties.Appearance.Options.UseFont = true;
            this.TxtIndicativePotentialArea.Properties.MaxLength = 40;
            this.TxtIndicativePotentialArea.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtIndicativePotentialArea.Size = new System.Drawing.Size(298, 20);
            this.TxtIndicativePotentialArea.TabIndex = 57;
            // 
            // LblIndicativePotentialArea
            // 
            this.LblIndicativePotentialArea.AutoSize = true;
            this.LblIndicativePotentialArea.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblIndicativePotentialArea.ForeColor = System.Drawing.Color.Black;
            this.LblIndicativePotentialArea.Location = new System.Drawing.Point(39, 275);
            this.LblIndicativePotentialArea.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblIndicativePotentialArea.Name = "LblIndicativePotentialArea";
            this.LblIndicativePotentialArea.Size = new System.Drawing.Size(153, 14);
            this.LblIndicativePotentialArea.TabIndex = 56;
            this.LblIndicativePotentialArea.Text = "Luas Potensi Indikatif (m2)";
            this.LblIndicativePotentialArea.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblRightsReleaseBasis
            // 
            this.LblRightsReleaseBasis.AutoSize = true;
            this.LblRightsReleaseBasis.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRightsReleaseBasis.ForeColor = System.Drawing.Color.Red;
            this.LblRightsReleaseBasis.Location = new System.Drawing.Point(73, 253);
            this.LblRightsReleaseBasis.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRightsReleaseBasis.Name = "LblRightsReleaseBasis";
            this.LblRightsReleaseBasis.Size = new System.Drawing.Size(118, 14);
            this.LblRightsReleaseBasis.TabIndex = 53;
            this.LblRightsReleaseBasis.Text = "Dasar Pelepasan Hak";
            this.LblRightsReleaseBasis.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDecreeNo
            // 
            this.TxtDecreeNo.EnterMoveNextControl = true;
            this.TxtDecreeNo.Location = new System.Drawing.Point(196, 228);
            this.TxtDecreeNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDecreeNo.Name = "TxtDecreeNo";
            this.TxtDecreeNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDecreeNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDecreeNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDecreeNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDecreeNo.Properties.MaxLength = 400;
            this.TxtDecreeNo.Size = new System.Drawing.Size(298, 20);
            this.TxtDecreeNo.TabIndex = 52;
            // 
            // LblDecreeNo
            // 
            this.LblDecreeNo.AutoSize = true;
            this.LblDecreeNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDecreeNo.ForeColor = System.Drawing.Color.Black;
            this.LblDecreeNo.Location = new System.Drawing.Point(106, 231);
            this.LblDecreeNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDecreeNo.Name = "LblDecreeNo";
            this.LblDecreeNo.Size = new System.Drawing.Size(85, 14);
            this.LblDecreeNo.TabIndex = 51;
            this.LblDecreeNo.Text = "Nomor SK Hak";
            this.LblDecreeNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDecreeDt
            // 
            this.DteDecreeDt.EditValue = null;
            this.DteDecreeDt.EnterMoveNextControl = true;
            this.DteDecreeDt.Location = new System.Drawing.Point(196, 206);
            this.DteDecreeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDecreeDt.Name = "DteDecreeDt";
            this.DteDecreeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDecreeDt.Properties.Appearance.Options.UseFont = true;
            this.DteDecreeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDecreeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDecreeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDecreeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDecreeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDecreeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDecreeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDecreeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDecreeDt.Properties.MaxLength = 8;
            this.DteDecreeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDecreeDt.Size = new System.Drawing.Size(170, 20);
            this.DteDecreeDt.TabIndex = 50;
            // 
            // LblDecreeDt
            // 
            this.LblDecreeDt.AutoSize = true;
            this.LblDecreeDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDecreeDt.ForeColor = System.Drawing.Color.Black;
            this.LblDecreeDt.Location = new System.Drawing.Point(100, 209);
            this.LblDecreeDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDecreeDt.Name = "LblDecreeDt";
            this.LblDecreeDt.Size = new System.Drawing.Size(92, 14);
            this.LblDecreeDt.TabIndex = 49;
            this.LblDecreeDt.Text = "Tanggal SK Hak";
            this.LblDecreeDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteRightsExpiredDt
            // 
            this.DteRightsExpiredDt.EditValue = null;
            this.DteRightsExpiredDt.EnterMoveNextControl = true;
            this.DteRightsExpiredDt.Location = new System.Drawing.Point(196, 184);
            this.DteRightsExpiredDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRightsExpiredDt.Name = "DteRightsExpiredDt";
            this.DteRightsExpiredDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRightsExpiredDt.Properties.Appearance.Options.UseFont = true;
            this.DteRightsExpiredDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRightsExpiredDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRightsExpiredDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRightsExpiredDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRightsExpiredDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRightsExpiredDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRightsExpiredDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRightsExpiredDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRightsExpiredDt.Properties.MaxLength = 8;
            this.DteRightsExpiredDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRightsExpiredDt.Size = new System.Drawing.Size(170, 20);
            this.DteRightsExpiredDt.TabIndex = 48;
            // 
            // LblRightsExpiredDt
            // 
            this.LblRightsExpiredDt.AutoSize = true;
            this.LblRightsExpiredDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRightsExpiredDt.ForeColor = System.Drawing.Color.Black;
            this.LblRightsExpiredDt.Location = new System.Drawing.Point(52, 187);
            this.LblRightsExpiredDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRightsExpiredDt.Name = "LblRightsExpiredDt";
            this.LblRightsExpiredDt.Size = new System.Drawing.Size(140, 14);
            this.LblRightsExpiredDt.TabIndex = 47;
            this.LblRightsExpiredDt.Text = "Tanggal Berakhirnya Hak";
            this.LblRightsExpiredDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRightsHolder
            // 
            this.TxtRightsHolder.EnterMoveNextControl = true;
            this.TxtRightsHolder.Location = new System.Drawing.Point(196, 162);
            this.TxtRightsHolder.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRightsHolder.Name = "TxtRightsHolder";
            this.TxtRightsHolder.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRightsHolder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRightsHolder.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRightsHolder.Properties.Appearance.Options.UseFont = true;
            this.TxtRightsHolder.Properties.MaxLength = 400;
            this.TxtRightsHolder.Size = new System.Drawing.Size(298, 20);
            this.TxtRightsHolder.TabIndex = 46;
            // 
            // LblRightsHolder
            // 
            this.LblRightsHolder.AutoSize = true;
            this.LblRightsHolder.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRightsHolder.ForeColor = System.Drawing.Color.Black;
            this.LblRightsHolder.Location = new System.Drawing.Point(103, 165);
            this.LblRightsHolder.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRightsHolder.Name = "LblRightsHolder";
            this.LblRightsHolder.Size = new System.Drawing.Size(89, 14);
            this.LblRightsHolder.TabIndex = 45;
            this.LblRightsHolder.Text = "Pemegang Hak";
            this.LblRightsHolder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteCertificatePublishDt
            // 
            this.DteCertificatePublishDt.EditValue = null;
            this.DteCertificatePublishDt.EnterMoveNextControl = true;
            this.DteCertificatePublishDt.Location = new System.Drawing.Point(196, 140);
            this.DteCertificatePublishDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteCertificatePublishDt.Name = "DteCertificatePublishDt";
            this.DteCertificatePublishDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCertificatePublishDt.Properties.Appearance.Options.UseFont = true;
            this.DteCertificatePublishDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCertificatePublishDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteCertificatePublishDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteCertificatePublishDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteCertificatePublishDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCertificatePublishDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteCertificatePublishDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCertificatePublishDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteCertificatePublishDt.Properties.MaxLength = 8;
            this.DteCertificatePublishDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteCertificatePublishDt.Size = new System.Drawing.Size(170, 20);
            this.DteCertificatePublishDt.TabIndex = 44;
            // 
            // TxtRightsNumberAndNIB
            // 
            this.TxtRightsNumberAndNIB.EnterMoveNextControl = true;
            this.TxtRightsNumberAndNIB.Location = new System.Drawing.Point(196, 118);
            this.TxtRightsNumberAndNIB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRightsNumberAndNIB.Name = "TxtRightsNumberAndNIB";
            this.TxtRightsNumberAndNIB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRightsNumberAndNIB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRightsNumberAndNIB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRightsNumberAndNIB.Properties.Appearance.Options.UseFont = true;
            this.TxtRightsNumberAndNIB.Properties.MaxLength = 400;
            this.TxtRightsNumberAndNIB.Size = new System.Drawing.Size(298, 20);
            this.TxtRightsNumberAndNIB.TabIndex = 43;
            // 
            // TxtVillage
            // 
            this.TxtVillage.EnterMoveNextControl = true;
            this.TxtVillage.Location = new System.Drawing.Point(551, 115);
            this.TxtVillage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVillage.Name = "TxtVillage";
            this.TxtVillage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVillage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVillage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVillage.Properties.Appearance.Options.UseFont = true;
            this.TxtVillage.Properties.MaxLength = 40;
            this.TxtVillage.Size = new System.Drawing.Size(298, 20);
            this.TxtVillage.TabIndex = 42;
            this.TxtVillage.Visible = false;
            // 
            // TxtDistrict
            // 
            this.TxtDistrict.EnterMoveNextControl = true;
            this.TxtDistrict.Location = new System.Drawing.Point(551, 93);
            this.TxtDistrict.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDistrict.Name = "TxtDistrict";
            this.TxtDistrict.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDistrict.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDistrict.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDistrict.Properties.Appearance.Options.UseFont = true;
            this.TxtDistrict.Properties.MaxLength = 40;
            this.TxtDistrict.Size = new System.Drawing.Size(298, 20);
            this.TxtDistrict.TabIndex = 41;
            this.TxtDistrict.Visible = false;
            // 
            // LblCertificatePublishDt
            // 
            this.LblCertificatePublishDt.AutoSize = true;
            this.LblCertificatePublishDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCertificatePublishDt.ForeColor = System.Drawing.Color.Black;
            this.LblCertificatePublishDt.Location = new System.Drawing.Point(3, 143);
            this.LblCertificatePublishDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCertificatePublishDt.Name = "LblCertificatePublishDt";
            this.LblCertificatePublishDt.Size = new System.Drawing.Size(189, 14);
            this.LblCertificatePublishDt.TabIndex = 37;
            this.LblCertificatePublishDt.Text = "Tanggal Penerbitan Sertifikat Hak";
            this.LblCertificatePublishDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblRightsNumberAndNIB
            // 
            this.LblRightsNumberAndNIB.AutoSize = true;
            this.LblRightsNumberAndNIB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRightsNumberAndNIB.ForeColor = System.Drawing.Color.Black;
            this.LblRightsNumberAndNIB.Location = new System.Drawing.Point(78, 121);
            this.LblRightsNumberAndNIB.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRightsNumberAndNIB.Name = "LblRightsNumberAndNIB";
            this.LblRightsNumberAndNIB.Size = new System.Drawing.Size(114, 14);
            this.LblRightsNumberAndNIB.TabIndex = 35;
            this.LblRightsNumberAndNIB.Text = "Nomor Hak dan NIB";
            this.LblRightsNumberAndNIB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblVillage
            // 
            this.LblVillage.AutoSize = true;
            this.LblVillage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVillage.ForeColor = System.Drawing.Color.Black;
            this.LblVillage.Location = new System.Drawing.Point(101, 99);
            this.LblVillage.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblVillage.Name = "LblVillage";
            this.LblVillage.Size = new System.Drawing.Size(91, 14);
            this.LblVillage.TabIndex = 33;
            this.LblVillage.Text = "Desa/Kelurahan";
            this.LblVillage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDistrict
            // 
            this.LblDistrict.AutoSize = true;
            this.LblDistrict.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDistrict.ForeColor = System.Drawing.Color.Black;
            this.LblDistrict.Location = new System.Drawing.Point(125, 77);
            this.LblDistrict.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDistrict.Name = "LblDistrict";
            this.LblDistrict.Size = new System.Drawing.Size(67, 14);
            this.LblDistrict.TabIndex = 31;
            this.LblDistrict.Text = "Kecamatan";
            this.LblDistrict.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblCity
            // 
            this.LblCity.AutoSize = true;
            this.LblCity.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCity.ForeColor = System.Drawing.Color.Red;
            this.LblCity.Location = new System.Drawing.Point(165, 54);
            this.LblCity.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCity.Name = "LblCity";
            this.LblCity.Size = new System.Drawing.Size(27, 14);
            this.LblCity.TabIndex = 29;
            this.LblCity.Text = "City";
            this.LblCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCity
            // 
            this.LueCity.EnterMoveNextControl = true;
            this.LueCity.Location = new System.Drawing.Point(196, 51);
            this.LueCity.Margin = new System.Windows.Forms.Padding(5);
            this.LueCity.Name = "LueCity";
            this.LueCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.Appearance.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity.Properties.DropDownRows = 30;
            this.LueCity.Properties.NullText = "[Empty]";
            this.LueCity.Properties.PopupWidth = 450;
            this.LueCity.Size = new System.Drawing.Size(298, 20);
            this.LueCity.TabIndex = 30;
            this.LueCity.ToolTip = "F4 : Show/hide list";
            this.LueCity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCity.EditValueChanged += new System.EventHandler(this.LueCity_EditValueChanged);
            // 
            // LblProvince
            // 
            this.LblProvince.AutoSize = true;
            this.LblProvince.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProvince.ForeColor = System.Drawing.Color.Red;
            this.LblProvince.Location = new System.Drawing.Point(139, 30);
            this.LblProvince.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProvince.Name = "LblProvince";
            this.LblProvince.Size = new System.Drawing.Size(53, 14);
            this.LblProvince.TabIndex = 27;
            this.LblProvince.Text = "Province";
            this.LblProvince.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProvince
            // 
            this.LueProvince.EnterMoveNextControl = true;
            this.LueProvince.Location = new System.Drawing.Point(196, 27);
            this.LueProvince.Margin = new System.Windows.Forms.Padding(5);
            this.LueProvince.Name = "LueProvince";
            this.LueProvince.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvince.Properties.Appearance.Options.UseFont = true;
            this.LueProvince.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvince.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProvince.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvince.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProvince.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvince.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProvince.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvince.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProvince.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProvince.Properties.DropDownRows = 30;
            this.LueProvince.Properties.NullText = "[Empty]";
            this.LueProvince.Properties.PopupWidth = 450;
            this.LueProvince.Size = new System.Drawing.Size(298, 20);
            this.LueProvince.TabIndex = 28;
            this.LueProvince.ToolTip = "F4 : Show/hide list";
            this.LueProvince.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProvince.EditValueChanged += new System.EventHandler(this.LueProvince_EditValueChanged);
            // 
            // LblAcquisitionType
            // 
            this.LblAcquisitionType.AutoSize = true;
            this.LblAcquisitionType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcquisitionType.ForeColor = System.Drawing.Color.Red;
            this.LblAcquisitionType.Location = new System.Drawing.Point(102, 6);
            this.LblAcquisitionType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcquisitionType.Name = "LblAcquisitionType";
            this.LblAcquisitionType.Size = new System.Drawing.Size(91, 14);
            this.LblAcquisitionType.TabIndex = 25;
            this.LblAcquisitionType.Text = "Aquisition Type";
            this.LblAcquisitionType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAcquisitionType
            // 
            this.LueAcquisitionType.EnterMoveNextControl = true;
            this.LueAcquisitionType.Location = new System.Drawing.Point(196, 3);
            this.LueAcquisitionType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcquisitionType.Name = "LueAcquisitionType";
            this.LueAcquisitionType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcquisitionType.Properties.Appearance.Options.UseFont = true;
            this.LueAcquisitionType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcquisitionType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcquisitionType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcquisitionType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcquisitionType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcquisitionType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcquisitionType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcquisitionType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcquisitionType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcquisitionType.Properties.DropDownRows = 30;
            this.LueAcquisitionType.Properties.NullText = "[Empty]";
            this.LueAcquisitionType.Properties.PopupWidth = 450;
            this.LueAcquisitionType.Size = new System.Drawing.Size(298, 20);
            this.LueAcquisitionType.TabIndex = 26;
            this.LueAcquisitionType.ToolTip = "F4 : Show/hide list";
            this.LueAcquisitionType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcquisitionType.EditValueChanged += new System.EventHandler(this.LueAquisitionType_EditValueChanged);
            // 
            // TpUploadFile
            // 
            this.TpUploadFile.Appearance.Header.Options.UseTextOptions = true;
            this.TpUploadFile.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpUploadFile.Controls.Add(this.Grd5);
            this.TpUploadFile.Name = "TpUploadFile";
            this.TpUploadFile.Size = new System.Drawing.Size(1097, 360);
            this.TpUploadFile.Text = "Upload File";
            // 
            // Grd5
            // 
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.UseXPStyles = false;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(1097, 360);
            this.Grd5.TabIndex = 25;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            this.Grd5.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd5_RequestCellToolTipText);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpApprovalInformation
            // 
            this.TpApprovalInformation.Appearance.Header.Options.UseTextOptions = true;
            this.TpApprovalInformation.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpApprovalInformation.Controls.Add(this.Grd6);
            this.TpApprovalInformation.Name = "TpApprovalInformation";
            this.TpApprovalInformation.Size = new System.Drawing.Size(1097, 360);
            this.TpApprovalInformation.Text = "Approval Information";
            // 
            // Grd6
            // 
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.UseXPStyles = false;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(1097, 360);
            this.Grd6.TabIndex = 24;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnParent
            // 
            this.BtnParent.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnParent.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnParent.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnParent.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnParent.Appearance.Options.UseBackColor = true;
            this.BtnParent.Appearance.Options.UseFont = true;
            this.BtnParent.Appearance.Options.UseForeColor = true;
            this.BtnParent.Appearance.Options.UseTextOptions = true;
            this.BtnParent.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnParent.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnParent.Image = ((System.Drawing.Image)(resources.GetObject("BtnParent.Image")));
            this.BtnParent.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnParent.Location = new System.Drawing.Point(419, 73);
            this.BtnParent.Name = "BtnParent";
            this.BtnParent.Size = new System.Drawing.Size(24, 21);
            this.BtnParent.TabIndex = 83;
            this.BtnParent.ToolTip = "Find Item";
            this.BtnParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnParent.ToolTipTitle = "Run System";
            this.BtnParent.Click += new System.EventHandler(this.BtnParent_Click);
            // 
            // BtnPropertyMutationDocNo
            // 
            this.BtnPropertyMutationDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPropertyMutationDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPropertyMutationDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPropertyMutationDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPropertyMutationDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPropertyMutationDocNo.Appearance.Options.UseFont = true;
            this.BtnPropertyMutationDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPropertyMutationDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPropertyMutationDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPropertyMutationDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPropertyMutationDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPropertyMutationDocNo.Image")));
            this.BtnPropertyMutationDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPropertyMutationDocNo.Location = new System.Drawing.Point(419, 93);
            this.BtnPropertyMutationDocNo.Name = "BtnPropertyMutationDocNo";
            this.BtnPropertyMutationDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPropertyMutationDocNo.TabIndex = 84;
            this.BtnPropertyMutationDocNo.ToolTip = "Find Item";
            this.BtnPropertyMutationDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPropertyMutationDocNo.ToolTipTitle = "Run System";
            this.BtnPropertyMutationDocNo.Click += new System.EventHandler(this.BtnPropertyMutationDocNo_Click);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtCopyData);
            this.panel3.Controls.Add(this.LblCopyData);
            this.panel3.Controls.Add(this.BtnCopyData);
            this.panel3.Controls.Add(this.LblStatus);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.TxtCancelReason);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.LblCancelReason);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.TxtDeactivationReason);
            this.panel3.Controls.Add(this.ChkCompleteInd);
            this.panel3.Controls.Add(this.LblDeactivationReason);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(653, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(450, 149);
            this.panel3.TabIndex = 33;
            // 
            // TxtCopyData
            // 
            this.TxtCopyData.EnterMoveNextControl = true;
            this.TxtCopyData.Location = new System.Drawing.Point(146, 71);
            this.TxtCopyData.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCopyData.Name = "TxtCopyData";
            this.TxtCopyData.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCopyData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCopyData.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCopyData.Properties.Appearance.Options.UseFont = true;
            this.TxtCopyData.Properties.MaxLength = 16;
            this.TxtCopyData.Properties.ReadOnly = true;
            this.TxtCopyData.Size = new System.Drawing.Size(206, 20);
            this.TxtCopyData.TabIndex = 33;
            // 
            // LblCopyData
            // 
            this.LblCopyData.AutoSize = true;
            this.LblCopyData.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCopyData.ForeColor = System.Drawing.Color.Black;
            this.LblCopyData.Location = new System.Drawing.Point(79, 74);
            this.LblCopyData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCopyData.Name = "LblCopyData";
            this.LblCopyData.Size = new System.Drawing.Size(63, 14);
            this.LblCopyData.TabIndex = 32;
            this.LblCopyData.Text = "Copy Data";
            this.LblCopyData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCopyData
            // 
            this.BtnCopyData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyData.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopyData.Appearance.Options.UseBackColor = true;
            this.BtnCopyData.Appearance.Options.UseFont = true;
            this.BtnCopyData.Appearance.Options.UseForeColor = true;
            this.BtnCopyData.Appearance.Options.UseTextOptions = true;
            this.BtnCopyData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyData.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopyData.Image")));
            this.BtnCopyData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyData.Location = new System.Drawing.Point(358, 70);
            this.BtnCopyData.Name = "BtnCopyData";
            this.BtnCopyData.Size = new System.Drawing.Size(24, 21);
            this.BtnCopyData.TabIndex = 34;
            this.BtnCopyData.ToolTip = "Find Item";
            this.BtnCopyData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyData.ToolTipTitle = "Run System";
            this.BtnCopyData.Click += new System.EventHandler(this.BtnCopyData_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TcPropertyInventory);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 149);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1103, 388);
            this.panel4.TabIndex = 8;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnExcel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnExcel.Location = new System.Drawing.Point(0, 168);
            this.BtnExcel.Name = "BtnExcel";
            this.BtnExcel.Size = new System.Drawing.Size(70, 31);
            this.BtnExcel.TabIndex = 8;
            this.BtnExcel.Text = "  E&xcel";
            this.BtnExcel.ToolTip = "Export to excel";
            this.BtnExcel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnExcel.ToolTipTitle = "Run System";
            this.BtnExcel.Click += new System.EventHandler(this.BtnExcel_Click);
            // 
            // FrmPropertyInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1173, 537);
            this.Controls.Add(this.panel4);
            this.Name = "FrmPropertyInventory";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyMutationDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExternalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeactivationReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCompleteInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcPropertyInventory)).EndInit();
            this.TcPropertyInventory.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemStockValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemStockQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUoM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRegistrationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRegistrationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            this.TpPropertyCostComponent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpFairValue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpMutationDocumentRef.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpPropertyTransferRef.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpCertificateInformation.ResumeLayout(false);
            this.TpCertificateInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRegistrationBasis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCertExpDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAreaBasedOnSurveyDoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSurveyDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSurveyDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBasisDocumentDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegistrationBasis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateIssuedLoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateExpiredDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateIssuedDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCertificateType.Properties)).EndInit();
            this.TpHistoricalInformation.ResumeLayout(false);
            this.TpHistoricalInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeVillage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRightsReleaseBasis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpatialInfoPage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndicativePotentialArea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDecreeNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDecreeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDecreeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRightsExpiredDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRightsExpiredDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRightsHolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertificatePublishDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCertificatePublishDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRightsNumberAndNIB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProvince.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcquisitionType.Properties)).EndInit();
            this.TpUploadFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpApprovalInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCopyData.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtExternalCode;
        internal DevExpress.XtraEditors.TextEdit TxtParent;
        internal DevExpress.XtraEditors.TextEdit TxtDisplayName;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyMutationDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyName;
        private System.Windows.Forms.Label LblExternalCode;
        private System.Windows.Forms.Label LblPropertyMutationSource;
        private System.Windows.Forms.Label LblParent;
        private System.Windows.Forms.Label LblDisplayName;
        private System.Windows.Forms.Label LblPropertyName;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyCode;
        private System.Windows.Forms.Label LblPropertyCode;
        private System.Windows.Forms.Label LblDeactivationReason;
        internal DevExpress.XtraEditors.TextEdit TxtDeactivationReason;
        private System.Windows.Forms.Label LblCancelReason;
        internal DevExpress.XtraEditors.TextEdit TxtCancelReason;
        private System.Windows.Forms.Label LblStatus;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.CheckEdit ChkCompleteInd;
        private DevExpress.XtraTab.XtraTabControl TcPropertyInventory;
        private DevExpress.XtraTab.XtraTabPage TpPropertyCostComponent;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraTab.XtraTabPage TpGeneral;
        internal DevExpress.XtraEditors.TextEdit TxtShortCode;
        private System.Windows.Forms.Label LblShortCode;
        private System.Windows.Forms.Label LblPropertyCategoryCode;
        private DevExpress.XtraEditors.LookUpEdit LuePropertyCategory;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label LblRegistrationDt;
        internal DevExpress.XtraEditors.DateEdit DteRegistrationDt;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label LblItem;
        private System.Windows.Forms.Label LblSite;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        public DevExpress.XtraEditors.SimpleButton BtnItCode;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label LblAcNo;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraTab.XtraTabPage TpMutationDocumentRef;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage TpHistoricalInformation;
        private System.Windows.Forms.Label LblCertificatePublishDt;
        private System.Windows.Forms.Label LblRightsNumberAndNIB;
        private System.Windows.Forms.Label LblVillage;
        private System.Windows.Forms.Label LblDistrict;
        private System.Windows.Forms.Label LblCity;
        private DevExpress.XtraEditors.LookUpEdit LueCity;
        private System.Windows.Forms.Label LblProvince;
        private DevExpress.XtraEditors.LookUpEdit LueProvince;
        private System.Windows.Forms.Label LblAcquisitionType;
        private DevExpress.XtraEditors.LookUpEdit LueAcquisitionType;
        private DevExpress.XtraTab.XtraTabPage TpApprovalInformation;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private DevExpress.XtraTab.XtraTabPage TpPropertyTransferRef;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraTab.XtraTabPage TpCertificateInformation;
        private DevExpress.XtraTab.XtraTabPage TpUploadFile;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        internal DevExpress.XtraEditors.TextEdit TxtCCCode;
        internal DevExpress.XtraEditors.TextEdit TxtRemStockValue;
        private System.Windows.Forms.Label LblRemainingStockValue;
        internal DevExpress.XtraEditors.TextEdit TxtRemStockQty;
        private System.Windows.Forms.Label LblRemStockQty;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice;
        private System.Windows.Forms.Label LblUPrice;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryQty;
        private System.Windows.Forms.Label LblInventoryQty;
        internal DevExpress.XtraEditors.TextEdit TxtUoM;
        private System.Windows.Forms.Label LblUoM;
        internal DevExpress.XtraEditors.TextEdit TxtPropertyInventoryValue;
        private System.Windows.Forms.Label LblPropertyInventoryValue;
        public DevExpress.XtraEditors.SimpleButton BtnSiteCode;
        internal DevExpress.XtraEditors.TextEdit TxtDistrict;
        internal DevExpress.XtraEditors.TextEdit TxtVillage;
        internal DevExpress.XtraEditors.TextEdit TxtRightsNumberAndNIB;
        internal DevExpress.XtraEditors.DateEdit DteCertificatePublishDt;
        internal DevExpress.XtraEditors.TextEdit TxtSpatialInfoPage;
        private System.Windows.Forms.Label LblSpatialInfoPage;
        internal DevExpress.XtraEditors.TextEdit TxtIndicativePotentialArea;
        private System.Windows.Forms.Label LblIndicativePotentialArea;
        private System.Windows.Forms.Label LblRightsReleaseBasis;
        internal DevExpress.XtraEditors.TextEdit TxtDecreeNo;
        private System.Windows.Forms.Label LblDecreeNo;
        internal DevExpress.XtraEditors.DateEdit DteDecreeDt;
        private System.Windows.Forms.Label LblDecreeDt;
        internal DevExpress.XtraEditors.DateEdit DteRightsExpiredDt;
        private System.Windows.Forms.Label LblRightsExpiredDt;
        internal DevExpress.XtraEditors.TextEdit TxtRightsHolder;
        private System.Windows.Forms.Label LblRightsHolder;
        public DevExpress.XtraEditors.SimpleButton BtnPropertyMutationDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnParent;
        public DevExpress.XtraEditors.SimpleButton BtnCCCode;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        internal DevExpress.XtraEditors.TextEdit TxtAreaBasedOnSurveyDoc;
        private System.Windows.Forms.Label LblAreaBasedOnSurveyDoc;
        internal DevExpress.XtraEditors.TextEdit TxtSurveyDocDt;
        private System.Windows.Forms.Label LblSurveyDocDt;
        internal DevExpress.XtraEditors.TextEdit TxtSurveyDocNo;
        private System.Windows.Forms.Label LblSurveyDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtBasisDocumentDt;
        private System.Windows.Forms.Label LblBasisDocumentDt;
        internal DevExpress.XtraEditors.TextEdit TxtRegistrationBasis;
        private System.Windows.Forms.Label LblRegistrationBasis;
        internal DevExpress.XtraEditors.TextEdit TxtNIB;
        private System.Windows.Forms.Label LblNIB;
        internal DevExpress.XtraEditors.TextEdit TxtCertificateIssuedLoc;
        private System.Windows.Forms.Label LblCertificateIssuedLoc;
        internal DevExpress.XtraEditors.TextEdit TxtCertificateExpiredDt;
        private System.Windows.Forms.Label LblCertificateExpiredDt;
        internal DevExpress.XtraEditors.TextEdit TxtCertificateIssuedDt;
        private System.Windows.Forms.Label LblCertificateIssuedDt;
        internal DevExpress.XtraEditors.TextEdit TxtCertificateNo;
        private System.Windows.Forms.Label LblCertificateNo;
        internal DevExpress.XtraEditors.TextEdit TxtCertificateType;
        private System.Windows.Forms.Label LblCertificateType;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.MemoExEdit MeeRightsReleaseBasis;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeCertExpDt;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeDistrict;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeVillage;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeRegistrationBasis;
        public DevExpress.XtraEditors.SimpleButton BtnExcel;
        internal DevExpress.XtraEditors.TextEdit TxtCopyData;
        private System.Windows.Forms.Label LblCopyData;
        public DevExpress.XtraEditors.SimpleButton BtnCopyData;
        private DevExpress.XtraTab.XtraTabPage TpFairValue;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
    }
}