﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmActivityCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmActivityCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmActivityCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtActivityCode, TxtActivityName 
                    }, true);
                    TxtActivityCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtActivityCode, TxtActivityName
                    }, false);
                    TxtActivityCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtActivityName
                    }, false);
                    TxtActivityName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtActivityCode, TxtActivityName
            });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmActivityCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtActivityCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtActivityCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAcivityCategory Where ActCtCode=@ActCtCode" };
                Sm.CmParam<String>(ref cm, "@ActCtCode", TxtActivityCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                SaveActivity();
                ShowData(TxtActivityCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtActivityCode, "Activity Code", false) ||
                Sm.IsTxtEmpty(TxtActivityName, "Activity Name", false) ||
                IsActivityCodeExisted();
        }

        private bool IsActivityCodeExisted()
        {
            if (!TxtActivityCode.Properties.ReadOnly && Sm.IsDataExist("Select ActCtCode From TblActivityCategory Where ActCtCode='" + TxtActivityCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Activity code ( " + TxtActivityCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveActivity()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblActivityCategory(ActCtCode, ActCtName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ActCtCode, @ActCtName, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update ActCtName=@ActCtName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ActCtCode", TxtActivityCode.Text);
            Sm.CmParam<String>(ref cm, "@ActCtName", TxtActivityName.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string ActCtCode)
        {
            try
            {
                ClearData();
                ShowActivity(ActCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowActivity(string ActCtCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ActCtCode", ActCtCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select * From TblActivityCategory Where ActCtCode=@ActCtCode",
                    new string[] 
                    {
                        "ActCtCode", 
                        "ActCtName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtActivityCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtActivityName.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
        }

        #endregion

        #endregion

        #region Event
        private void TxtActivityCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtActivityCode);
        }


        private void TxtActivityName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtActivityName);

        }
        #endregion
    }
}
