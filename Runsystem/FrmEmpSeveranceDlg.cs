﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSeveranceDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpSeverance mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpSeveranceDlg(FrmEmpSeverance FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Employee";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetLueType(ref LueType);
            SetGrd();
            SetSQL();
            Sl.SetLueDeptCode(ref LueDeptCode);
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

             SQL.AppendLine("Select * From ");
             SQL.AppendLine("( ");
	        
             SQL.AppendLine("   Select '01' As Code, A.EmpCode,A.EmpName, A.DeptCode, C.DeptName, D.PosName, A.BirthDt, A.JoinDt, A.ResignDt,  ");
             SQL.AppendLine("   TimeStampdiff(YEAR, ifnull(A.LeaveStartDt, A.JoinDt), A.ResignDt) WorkYr, ");
             SQL.AppendLine("   IF(TimeStampdiff(YEAR, ifnull(A.LeaveStartDt, A.JoinDt), A.ResignDt)=0, timestampdiff(MONTH, A.JoinDt, A.ResignDt), '0') WorkMth, ifnull(G.Amt, 0) AS Salary, ");
             SQL.AppendLine("   ifnull(H.Amt, 0)*ifnull(G.Amt, 0) As SeveranceAmt, I.Amt As AmtPension, ");
             SQL.AppendLine("   left(Replace(CurDate(), '-', ''), 4) - Left(A.BirthDt, 4) As Age ");
             SQL.AppendLine("   From TblEmployee A  ");
             SQL.AppendLine("   Left Join tbluser B On A.UserCode=B.UserCode   ");
             SQL.AppendLine("   left Join tbldepartment C On A.DeptCode=C.DeptCode  ");
             SQL.AppendLine("   Left Join tblposition D On A.PosCode=D.PosCode  ");
             SQL.AppendLine("   Left Join ");
             SQL.AppendLine("   ( ");
	         SQL.AppendLine("       Select A.EmpCode, A.DocNo, C.Dno, C.Amt ");
	         SQL.AppendLine("       from TblEmpsalaryHdr A ");
	         SQL.AppendLine("       Inner Join  ");
	         SQL.AppendLine("       ( ");
	         SQL.AppendLine("           Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  ");
	         SQL.AppendLine("           From TblEmpSalaryHdr ");
	         SQL.AppendLine("           Group BY EmpCode ");
	         SQL.AppendLine("       )B On Concat(A.DocDt, A.DocNo) = B.KeyCode  ");
	         SQL.AppendLine("       Inner Join TblEmpsalaryDtl C On A.DocNo = C.DocNo ");
	         SQL.AppendLine("       Inner Join ");
	         SQL.AppendLine("       ( ");
		     SQL.AppendLine("           Select DocNo, MAX(Dno) Dno From TblEmpsalaryDtl ");
		     SQL.AppendLine("           Group BY DocNo ");
	         SQL.AppendLine("       )D on C.DocNo = D.DocNo And C.Dno = D.Dno ");
	         SQL.AppendLine("       Where A.Status = 'A' ");
             SQL.AppendLine("   )G On A.EMpCode = G.EmpCode ");
             SQL.AppendLine("   Left Join TblSeverance H On TimeStampdiff(YEAR, A.JoinDt, A.ResignDt) = H.YearsWorked ");
             SQL.AppendLine("   Left Join  ");
             SQL.AppendLine("   ( ");
   	         SQL.AppendLine("      Select A.EmpCode, B.Amt ");
		     SQL.AppendLine("       From TblEmpsalaryHdr A ");
		     SQL.AppendLine("       Inner Join TblEmpSalary3Dtl B On A.DocNo = B.DocNo  ");
             SQL.AppendLine("       And B.SSPCode = (Select Parvalue From Tblparameter Where parCode='SSProgramForPension') ");
		     SQL.AppendLine("       Inner Join  ");
		     SQL.AppendLine("       ( ");
		     SQL.AppendLine("         Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  ");
		     SQL.AppendLine("         From TblEmpSalaryHdr ");
		     SQL.AppendLine("         Group BY EmpCode ");
		     SQL.AppendLine("       )C On Concat(A.DocDt, A.DocNo) = C.KeyCode  ");
             SQL.AppendLine("  )I On A.EMpCode = I.EmpCode ");
             SQL.AppendLine("  Inner Join  ");
             SQL.AppendLine("  ( ");
  		     SQL.AppendLine("       Select A.EmpCode From tblPPS A ");
		     SQL.AppendLine("       Inner Join  ");
		     SQL.AppendLine("       ( ");
			 SQL.AppendLine("           Select X.EmpCode, MAX(Concat(X.DocDt, X.DocNo)) As KeyCode  ");
			 SQL.AppendLine("           From TblPPS X ");
			 SQL.AppendLine("           Where X.Status = 'A' And X.CancelInd = 'N' ");
			 SQL.AppendLine("           Group by X.EmpCOde ");
		     SQL.AppendLine("       )B On Concat(A.DocDt, A.DocNo) = B.KeyCode ");
		     SQL.AppendLine("       Where A.ResignDtnew is not null  ");
             SQL.AppendLine("  )J On  A.EMpCode = J.EmpCode ");


	         SQL.AppendLine("   UNION ALL ");
	         SQL.AppendLine("   Select * from ( ");
             SQL.AppendLine("   Select '02' As Code, A.EmpCode,A.EmpName, A.DeptCode, C.DeptName, D.PosName, A.BirthDt, A.JoinDt, Replace(CurDate(), '-', '') DtNow,  ");
             SQL.AppendLine("   TimeStampdiff(YEAR, ifnull(A.LeaveStartDt, A.JoinDt), Replace(CurDate(), '-', '')) WorkYr, ");
             SQL.AppendLine("   IF(TimeStampdiff(YEAR, ifnull(A.LeaveStartDt, A.JoinDt), Replace(CurDate(), '-', ''))=0, timestampdiff(MONTH, A.BirthDt, Replace(CurDate(), '-', '')), '0') WorkMth, ");
             SQL.AppendLine("   ifnull(G.Amt, 0) AS Salary, ifnull(H.Amt, 0)*ifnull(G.Amt, 0) As SeveranceAmt, I.Amt As AmtPension, ");
             SQL.AppendLine("   left(Replace(CurDate(), '-', ''), 4) - Left(A.BirthDt, 4) As Age ");
             SQL.AppendLine("   From TblEmployee A  ");
             SQL.AppendLine("   Left Join tbluser B On A.UserCode=B.UserCode   ");
             SQL.AppendLine("   left Join tbldepartment C On A.DeptCode=C.DeptCode  ");
             SQL.AppendLine("   Left Join tblposition D On A.PosCode=D.PosCode  ");
             SQL.AppendLine("   Left Join tbloption E On A.Gender=E.OptCode and E.OptCat='Gender'  ");
             SQL.AppendLine("   Left Join TblGradeLevelHdr F On A.GrdlvlCode = F.GrdLvlCode ");
             SQL.AppendLine("    Left Join ");
             SQL.AppendLine("   ( ");
	         SQL.AppendLine("       Select A.EmpCode, A.DocNo, C.Dno, C.Amt  ");
	         SQL.AppendLine("       from TblEmpsalaryHdr A ");
	         SQL.AppendLine("       Inner Join  ");
	         SQL.AppendLine("       ( ");
	         SQL.AppendLine("           Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  ");
	         SQL.AppendLine("           From TblEmpSalaryHdr ");
	         SQL.AppendLine("           Group BY EmpCode ");
	         SQL.AppendLine("       )B On Concat(A.DocDt, A.DocNo) = B.KeyCode ");
	         SQL.AppendLine("       Inner Join TblEmpsalaryDtl C On A.DocNo = C.DocNo ");
	         SQL.AppendLine("       Inner Join ");
	         SQL.AppendLine("       ( ");
		     SQL.AppendLine("           Select DocNo, MAX(Dno) Dno From TblEmpsalaryDtl ");
		     SQL.AppendLine("           Group BY DocNo ");
	         SQL.AppendLine("       )D on C.DocNo = D.DocNo And C.Dno = D.Dno ");
	         SQL.AppendLine("       Where A.Status = 'A' ");
             SQL.AppendLine("   )G On A.EMpCode = G.EmpCode ");
             SQL.AppendLine("   Left Join TblSeverance H On TimeStampdiff(YEAR, A.BirthDt, Replace(CurDate(), '-', '')) = H.YearsWorked  ");
             SQL.AppendLine("   Left Join  ");
             SQL.AppendLine("   ( ");
             SQL.AppendLine("      Select A.EmpCode, B.Amt ");
             SQL.AppendLine("       From TblEmpsalaryHdr A ");
             SQL.AppendLine("       Inner Join TblEmpSalary3Dtl B On A.DocNo = B.DocNo  ");
             SQL.AppendLine("       And B.SSPCode = (Select Parvalue From Tblparameter Where parCode='SSProgramForPension') ");
             SQL.AppendLine("       Inner Join  ");
             SQL.AppendLine("       ( ");
             SQL.AppendLine("         Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  ");
             SQL.AppendLine("         From TblEmpSalaryHdr ");
             SQL.AppendLine("         Group BY EmpCode ");
             SQL.AppendLine("       )C On Concat(A.DocDt, A.DocNo) = C.KeyCode  ");
             SQL.AppendLine("   )I On A.EmpCode = I.EmpCode ");
             SQL.AppendLine("   Where A.BirthDt is not null And (left(Replace(CurDate(), '-', ''), 4) - Left(A.BirthDt, 4))>=56 ");
             SQL.AppendLine("   )X ");
             
            SQL.AppendLine(")Z ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            string Dt1 = string.Empty;
            string Dt2 = string.Empty;

            if (Sm.GetLue(LueType) == "01")
                Dt1 = "Join Date";
            else
                Dt1 = "Join Date";

            if (Sm.GetLue(LueType) == "02")
                Dt2 = "Resign Date";
            else
                Dt2 = "Date Now";


            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",
                        //1-5
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee"+Environment.NewLine+"Name",
                        "DeptCode",
                        "Department",
                        "Position",
                        //6-10
                        "Birth Date",
                        ""+Dt1+"",
                        ""+Dt2+"",
                        "Years Worked",
                        "Month Worked",
                        //11-13
                        "Age",
                        "Salary",
                        "Pension"
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] {9, 10, 11, 12, 13}, 0);
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueType, "Type")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "Z.EmpCode", "Z.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Z.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "Z.Code", true);
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By Z.EmpName ",
                        new string[] 
                        { 
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName", "DeptCode", "DeptName", "PosName", "BirthDt",  
                            //6-10
                            "JoinDt", "ResignDt", "WorkYr", "WorkMth", "Age",  
                            //11
                            "Salary", "AmtPension"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.ClearData2();
                mFrmParent.TxtProcess.EditValue = LueType.Text;
                mFrmParent.ShowEmpData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.TxtYearsWorked.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 9), 0);
                mFrmParent.TxtDtlPension.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 13), 0);
                mFrmParent.TxtDtlNonPension.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12) - Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 13), 0);
                mFrmParent.TxtHdrSalary.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                mFrmParent.TxtDtlSalary.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                mFrmParent.TxtDtlSalary1.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                mFrmParent.TxtDtlSalary2.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                mFrmParent.TxtDtlSalary3.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 12), 0);
                mFrmParent.SetServeranceGrp();
                this.Close();
            }

        }


        public static void SetLueType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '01' As Col1, 'Resign' As Col2 "+
                "Union ALL "+
                "Select '02' As Col1, 'Retired' As Col2;",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }
        #endregion
    }
}
