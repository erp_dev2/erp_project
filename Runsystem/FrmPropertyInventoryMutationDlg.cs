﻿#region Update
/*
    28/12/2022 [IBL/BBT] New Apps
    12/01/2023 [IBL/BBT] Judul dialog belum sesuai
    12/01/2023 [IBL/BBT] Validasi Remaining Stock Value > 0 dihilangkan
    11/04/2023 [WED/BBT] item selanjutnya untuk from nya langsung tervalidasi PropertyCategory, CostCenter, Site sama dengan di baris pertama main form nya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryMutationDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPropertyInventoryMutation mFrmParent;
        private string mSQL = string.Empty;
        private string parentPropertyCategoryCode = string.Empty, parentCostCenterCode = string.Empty, parentSiteCode = string.Empty, parentPropertyCode = string.Empty;
        private int mCurRow = 0;

        #endregion

        #region Constructor

        public FrmPropertyInventoryMutationDlg(FrmPropertyInventoryMutation FrmParent, int CurRow)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "List of Master Property Inventory";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLuePropertyCategoryCode(ref LuePropertyCategoryCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                SetGrd();

                parentPropertyCategoryCode = string.Empty; parentCostCenterCode = string.Empty; parentSiteCode = string.Empty; parentPropertyCode = string.Empty;
                LuePropertyCategoryCode.Properties.ReadOnly = LueSiteCode.Properties.ReadOnly = ChkPropertyCategoryCode.Properties.ReadOnly = ChkSiteCode.Properties.ReadOnly = false;

                if (mFrmParent.Grd1.Rows.Count > 1)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd1, 0, 2).Length > 0)
                    {
                        parentPropertyCategoryCode = Sm.GetGrdStr(mFrmParent.Grd1, 0, 22);
                        parentCostCenterCode = Sm.GetGrdStr(mFrmParent.Grd1, 0, 23);
                        parentSiteCode = Sm.GetGrdStr(mFrmParent.Grd1, 0, 25);

                        if (parentPropertyCategoryCode.Length > 0)
                        {
                            Sm.SetLue(LuePropertyCategoryCode, parentPropertyCategoryCode);
                            LuePropertyCategoryCode.Properties.ReadOnly = ChkPropertyCategoryCode.Properties.ReadOnly = true;
                        }

                        if (parentSiteCode.Length > 0)
                        {
                            Sm.SetLue(LueSiteCode, parentSiteCode);
                            LueSiteCode.Properties.ReadOnly = ChkSiteCode.Properties.ReadOnly = true;
                        }
                    }

                    for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count - 1; ++Row)
                    {
                        if (parentPropertyCode.Length > 0) parentPropertyCode += ",";
                        parentPropertyCode += Sm.GetGrdStr(mFrmParent.Grd1, Row, 2);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.RegistrationDt, A.PropertyCode, A.PropertyName, C.CCName, D.SiteName, IfNull(A.PropertyInventoryValue, 0.00) As PropertyInventoryValue, ");
            SQL.AppendLine("B.PropertyCategoryCode, B.PropertyCategoryName, IfNull(A.InventoryQty, 0.00) As InventoryQty, F.InventoryUOMCode, ");
            SQL.AppendLine("IfNull(A.RemStockQty, 0.00) AS RemStockQty, IfNull(A.RemStockValue, 0.00) As RemStockValue, A.AcNo, E.AcDesc ");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory B On A.PropertyCategoryCode = B.PropertyCategoryCode ");
            if (parentPropertyCategoryCode.Length > 0) SQL.AppendLine("    And A.PropertyCategoryCode = @PropertyCategoryCode ");
            if (parentCostCenterCode.Length > 0) SQL.AppendLine("    And A.CCCode = @CCCode ");
            if (parentSiteCode.Length > 0) SQL.AppendLine("    And A.SiteCode = @SiteCode ");
            if (parentPropertyCode.Length > 0) SQL.AppendLine("   And Not Find_In_Set(A.PropertyCode, @PropertyCode) ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCode ");
            SQL.AppendLine("Inner Join TblSite D On A.SiteCode = D.SiteCode ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo = E.AcNo ");
            SQL.AppendLine("Inner Join TblItem F On A.ItCode = F.ItCode ");
            SQL.AppendLine("Where A.Status = 'A' ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.CompleteInd = 'Y' ");
            SQL.AppendLine("And A.ActInd = 'Y' ");
            SQL.AppendLine("And A.RemStockQty > 0 ");
            //SQL.AppendLine("And A.RemStockValue > 0 ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Date Of" + Environment.NewLine + "Registration",
                        "Property Code",
                        "Property Name",
                        "Initial Cost Center",
                        "Site", 

                        //6-10
                        "Property" + Environment.NewLine + "Inventory Value",
                        "Property" + Environment.NewLine + "Category Code",
                        "Property Category",
                        "Inventory Quantity",
                        "UoM",

                        //11-14
                        "Remaining" + Environment.NewLine + "Stock Quantity",
                        "Remaining" + Environment.NewLine + "Stock Value",
                        "COA#",
                        "COA Description",
                    },
                     new int[]
                    {
                        //0
                        50,
                        
                        //1-5
                        120, 150, 200, 200, 200, 
                        
                        //6-10
                        120, 120, 150, 120, 80, 
                        
                        //11-14
                        120, 120, 200, 200,
                    }
                );

            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 9, 11, 12 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = " ";

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePropertyCategoryCode), "B.PropertyCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPropertyCode.Text, new string[] { "A.PropertyCode", "A.PropertyName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "D.SiteCode", true);

                Sm.CmParam<String>(ref cm, "@PropertyCategoryCode", parentPropertyCategoryCode);
                Sm.CmParam<String>(ref cm, "@CCCode", parentCostCenterCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", parentSiteCode);
                Sm.CmParam<String>(ref cm, "@PropertyCode", parentPropertyCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter + " Order By A.PropertyName;",
                        new string[]
                        {
                            //0
                            "RegistrationDt",
                            //1-5
                            "PropertyCode", "PropertyName", "CCName", "SiteName", "PropertyInventoryValue",
                            //6-10
                            "PropertyCategoryCode", "PropertyCategoryName", "InventoryQty", "InventoryUOMCode", "RemStockQty",
                            //11-13
                            "RemStockValue", "AcNo", "AcDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                int Row = Grd1.CurRow.Index;
                //ClearData();
                mFrmParent.ShowPropertyInventory(mCurRow, Sm.GetGrdStr(Grd1, Row, 2));
                mFrmParent.Grd1AfterCommitEdit(mCurRow, 11);
                mFrmParent.Grd1.Rows.Add();
                Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 14, 15, 17, 18, 19, 20, 21 });
                this.Close();
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{mFrmParent.TxtTotal1A, mFrmParent.TxtTotal2A, mFrmParent.TxtTotal1B, mFrmParent.TxtTotal2B });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { mFrmParent.TxtTotal1A, mFrmParent.TxtTotal2A, mFrmParent.TxtTotal1B, mFrmParent.TxtTotal2B }, 0);
            Sm.ClearGrd(mFrmParent.Grd2, true);
            Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 9, 11, 12, 15, 16 });
        }

        #endregion

        #region Grid Method
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }
        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method
        private void SetLuePropertyCategoryCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.PropertyCategoryCode As Col1, T.PropertyCategoryName As Col2 ");
            SQL.AppendLine("From TblPropertyInventoryCategory T ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("Order By T.PropertyCategoryName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        #endregion

        #endregion

        #region Event
        #region Misc Control Event
        private void LuePropertyCategoryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCategoryCode, new Sm.RefreshLue1(SetLuePropertyCategoryCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPropertyCategoryCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Property Category");
        }

        private void TxtPropertyCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropertyCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property Name");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        #endregion

        #region Grid Event
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
