﻿#region Update
/*
    26/01/2021 [VIN/PHT] menampilkan site berdasarkan IsWarehouseUseSite  
    27/01/2021 [ICA/PHT] Menambah filter Warehouse Category dan kolomnya
    26/07/2021 [TKG/IMS] tambah indikator MAP
    08/02/2022 [VIN/ALL] Bug filter
    24/01/2023 [HAR/PHT] tambah informasi whs old code
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWarehouseFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmWarehouse mFrmParent;

        #endregion

        #region Constructor

        public FrmWarehouseFind(FrmWarehouse FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueWhsCtCode(ref LueWhsCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Moving Average",
                        "Category",
                        "Cost Center",
                        
                        //6-10
                        "Cost Category",
                        "Mutations's Cost Category"+Environment.NewLine+"Account#",
                        "Mutations's Cost Category"+Environment.NewLine+"Account Description",
                        "Site",   
                        "Warehouse"+Environment.NewLine+"Old Code",
                        
                        //11-15
                        "Created By",
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        //16
                        "Last Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColInvisible(Grd1, new int[] {  11, 12, 13, 14, 15,16 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, mFrmParent.mIsWarehouseUseSite);
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, mFrmParent.mIsMovingAvgEnabled);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.FilterStr(ref Filter, ref cm, TxtWhsName.Text, new string[] { "A.WhsCode", "A.WhsName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCtCode), "A.WhsCtCode", false);

                SQL.AppendLine("Select A.WhsCode, A.WhsName, ");
                if (mFrmParent.mIsMovingAvgEnabled)
                    SQL.AppendLine("A.MovingAvgInd, ");
                else
                    SQL.AppendLine("'N' As MovingAvgInd, ");
                SQL.AppendLine("F.WhsCtName, B.CCName, C.CCtName, A.AcNo, D.AcDesc, E.SiteName, A.WhsCodeOld, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblWarehouse A ");
                SQL.AppendLine("Left Join TblCostCenter B On A.CCCode=B.CCCode ");
                SQL.AppendLine("Left Join TblCostCategory C On A.CCtCode=C.CCtCode ");
                SQL.AppendLine("Left Join TblCOA D On A.AcNo=D.AcNo ");
                SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("Left Join TblWarehouseCategory F On A.WhsCtCode = F.WhsCtCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By WhsName");

                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(), 
                        new string[]
                        {
                            //0
                            "WhsCode", 
                                
                            //1-5
                            "WhsName", "MovingAvgInd", "WhsCtName", "CCName", "CCtName", 
                            
                            //6-10
                            "AcNo",  "AcDesc", "SiteName", "WhsCodeOld", "CreateBy", 
                            
                            //11-13
                            "CreateDt",  "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWhsName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Warehouse");
        }

        private void LueWhsCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCtCode, new Sm.RefreshLue1(Sl.SetLueWhsCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse's Category");
        }

        #endregion

        #endregion
    }
}
