﻿#region Update
    // 30/05/2017 [HAR] query EquipCode, EquipName diubah menjadi EquipmentCode, EquipmentName
    // 14/09/2017 [ARI] tambah maintenance status, Down time
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOMMaintenance : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, 
            mAssetCode = string.Empty;
        internal int mNumberOfInventoryUomCode = 1;
        internal FrmBOMMaintenanceFind FrmFind;

        #endregion

        #region Constructor

        public FrmBOMMaintenance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Bill of Material Maintenance";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueOption(ref LueMtcType, "MaintenanceType");
                SetLueMtcStatus(ref LueMtcStatus);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Internal Code",

                        //6-10
                        "Foreign Name",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        
                        //11-12
                        "Quantity",
                        "UoM",
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 100, 20, 250, 100,  
                        
                        //6-10
                        150, 100, 100, 100, 100, 
                        
                        //11-12
                        100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 9, 10, 11, 12 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 8, 10, 12 });

            ShowInventoryUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 8;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        
                        //6-7
                        "Department",
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        
                        //1-5
                        20, 100, 250, 100, 180,  
                        
                        //6-7
                        200, 200
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd, DteDocDt, TxtBomName, LueEquipmentCode, LueMtcType, LueMtcStatus, TmeDown, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 7, 9, 11 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1 });
                    BtnEquipCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, TxtBomName, LueMtcType, LueMtcStatus, TmeDown, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 7, 9, 11 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    BtnEquipCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtBomName, LueEquipmentCode, LueMtcType, LueMtcStatus, TmeDown, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 7, 9 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mAssetCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtBomName, TxtAssetCode, LueEquipmentCode, LueMtcType, LueMtcStatus, TmeDown, MeeRemark
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 9, 11 });
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBOMMaintenanceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOMMaintenance", "TblBOMMaintenanceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBOMMaintenanceHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) cml.Add(SaveBOMMaintenanceDtl(DocNo, r));
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0) cml.Add(SaveBOMMaintenanceDtl2(DocNo, r));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBomName, "Bill of material name", false) ||
                Sm.IsTxtEmpty(TxtAssetCode, "Asset name", false) ||
                Sm.IsLueEmpty(LueEquipmentCode, "Equipment") ||
                Sm.IsLueEmpty(LueMtcType, "Maintenance type") ||
                Sm.IsLueEmpty(LueMtcStatus, "Maintenance status") ||
                Sm.IsTmeEmpty(TmeDown, "Down Time") ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee data entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var Msg = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 4, false, "Item is empty.")) return true;
                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine + Environment.NewLine;


                if (Sm.GetGrdDec(Grd1, r, 7) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[9].Visible && Sm.GetGrdDec(Grd1, r, 9) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[11].Visible && Sm.GetGrdDec(Grd1, r, 11) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }
            }

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, r, 2, false, "Employee is empty.")) return true;
            }

            return false;
        }

        private MySqlCommand SaveBOMMaintenanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOMMaintenanceHdr ");
            SQL.AppendLine("(DocNo, DocDt, ActInd, BomName, AssetCode, EquipmentCode, MtcType, MtcStatus, DownTm, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @BomName, @AssetCode, @EquipmentCode, @MtcType, @MtcStatus, @DownTm, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@BomName", TxtBomName.Text);
            Sm.CmParam<String>(ref cm, "@AssetCode", mAssetCode);
            Sm.CmParam<String>(ref cm, "@EquipmentCode", Sm.GetLue(LueEquipmentCode));
            Sm.CmParam<String>(ref cm, "@MtcType", Sm.GetLue(LueMtcType));
            Sm.CmParam<String>(ref cm, "@MtcStatus", Sm.GetLue(LueMtcStatus));
            Sm.CmParam<String>(ref cm, "@DownTm", Sm.GetTme(TmeDown));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBOMMaintenanceDtl(string DocNo, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBOMMaintenanceDtl(DocNo, DNo, ItCode,Qty, Qty2, Qty3, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ItCode, @Qty, @Qty2, @Qty3, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBOMMaintenanceDtl2(string DocNo, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBOMMaintenanceDtl2(DocNo, DNo, EmpCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, r, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBOMMaintenanceHdr());
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) cml.Add(SaveBOMMaintenanceDtl(TxtDocNo.Text, r));
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0) cml.Add(SaveBOMMaintenanceDtl2(TxtDocNo.Text, r));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsTxtEmpty(TxtBomName, "Bill of material name", false) ||
                Sm.IsTxtEmpty(TxtAssetCode, "Asset name", false) ||
                Sm.IsLueEmpty(LueEquipmentCode, "Equipment") ||
                 Sm.IsLueEmpty(LueMtcType, "Maintenance type") ||
                Sm.IsLueEmpty(LueMtcStatus, "Maintenance status") ||
                Sm.IsTmeEmpty(TmeDown, "Down Time") ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private MySqlCommand EditBOMMaintenanceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOMMaintenanceHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, ");
            SQL.AppendLine("    BomName=@BomName, ");
            SQL.AppendLine("    AssetCode=@AssetCode, ");
            SQL.AppendLine("    EquipmentCode=@EquipmentCode, ");
            SQL.AppendLine("    MtcType=@MtcType, ");
            SQL.AppendLine("    MtcStatus=@MtcStatus, ");
            SQL.AppendLine("    DownTm=@DownTm, ");
            SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Delete From TblBOMMaintenanceDtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblBOMMaintenanceDtl2 Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@BomName", TxtBomName.Text);
            Sm.CmParam<String>(ref cm, "@AssetCode", mAssetCode);
            Sm.CmParam<String>(ref cm, "@EquipmentCode", Sm.GetLue(LueEquipmentCode));
            Sm.CmParam<String>(ref cm, "@MtcType", Sm.GetLue(LueMtcType));
            Sm.CmParam<String>(ref cm, "@MtcStatus", Sm.GetLue(LueMtcStatus));
            Sm.CmParam<String>(ref cm, "@DownTm", Sm.GetTme(TmeDown));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBOMMaintenanceHdr(DocNo);
                ShowBOMMaintenanceDtl(DocNo);
                ShowBOMMaintenanceDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBOMMaintenanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, A.BomName, ");
            SQL.AppendLine("A.AssetCode, B.AssetName, A.EquipmentCode, A.MtcType, A.MtcStatus, A.DownTm, A.Remark ");
            SQL.AppendLine("From TblBOMMaintenanceHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ActInd", "BomName", "AssetCode", "AssetName", 
                        //6-10
                        "EquipmentCode", "MtcType", "MtcStatus", "DownTm", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtBomName.EditValue = Sm.DrStr(dr, c[3]);
                        mAssetCode = Sm.DrStr(dr, c[4]);
                        TxtAssetCode.EditValue = Sm.DrStr(dr, c[5]);
                        SetLueEquipCode(ref LueEquipmentCode, string.Empty, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueMtcType, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueMtcStatus, Sm.DrStr(dr, c[8]));
                        Sm.SetTme(TmeDown, Sm.DrStr(dr, c[9]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

        private void ShowBOMMaintenanceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ItCodeInternal, B.ForeignName, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3 ");
            SQL.AppendLine("From TblBOMMaintenanceDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "ItCode", "ItName", "ItCodeInternal", "ForeignName", "Qty", 
                    "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 11 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowBOMMaintenanceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, B.EmpCodeOld, ");
            SQL.AppendLine("C.PosName, D.DeptName, E.SiteName ");
            SQL.AppendLine("From TblBOMMaintenanceDtl2 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.deptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptName", 
                    "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mNumberOfInventoryUomCode = (NumberOfInventoryUomCode.Length == 0) ? 1 : int.Parse(NumberOfInventoryUomCode);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12 }, true);
        }

        internal void SetLueEquipCode(ref DXE.LookUpEdit Lue, string AssetCode, string EquipCode)
        {
            try
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                
                SQL.AppendLine("Select EquipmentCode As Col1, EquipmentName As Col2 ");
                SQL.AppendLine("From TblEquipment ");

                if (AssetCode.Length > 0 && EquipCode.Length == 0)
                {
                    SQL.AppendLine("Where EquipmentCode In ( ");
                    SQL.AppendLine("Select EquipmentCode From TblTODtl Where AssetCode=@AssetCode) ");
                    
                    Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
                }

                if (EquipCode.Length > 0)
                {
                    SQL.AppendLine("Where EquipmentCode=@EquipCode ");

                    Sm.CmParam<String>(ref cm, "@EquipCode", EquipCode);
                }

                if (AssetCode.Length == 0 && EquipCode.Length == 0)
                    SQL.AppendLine("Where 0=1 ");

                SQL.AppendLine("Order By EquipmentName;");

                cm.CommandText = SQL.ToString();

                Sm.SetLue2(ref Lue, ref cm, 0, 50, false, true, "Code", "Name", "Col2", "Col1");

                if (EquipCode.Length > 0) Sm.SetLue(LueEquipmentCode, EquipCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private static void SetLueMtcStatus(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
               "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'MaintenanceStatus' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnEquipCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBOMMaintenanceDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void TxtBomName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBomName);
        }

        private void LueEquipCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEquipmentCode, new Sm.RefreshLue3(SetLueEquipCode), mAssetCode, string.Empty);
        }

        private void LueMtcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMtcType, new Sm.RefreshLue2(Sl.SetLueOption), "MaintenanceType");
        }

        private void LueMtcStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMtcStatus, new Sm.RefreshLue1(SetLueMtcStatus));
        }
      
        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmBOMMaintenanceDlg2(this));
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBOMMaintenanceDlg2(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 7, 9, 11 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 11 });
                    }
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7, 9, 11 }, e);

            if (e.ColIndex == 7)
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 7, 9, 11, 8, 10, 12);

            if (e.ColIndex == 9)
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 9, 7, 11, 10, 8, 12);

            if (e.ColIndex == 7 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), Sm.GetGrdStr(Grd1, e.RowIndex, 10)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 9, Grd1, e.RowIndex, 7);

            if (e.ColIndex == 7 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), Sm.GetGrdStr(Grd1, e.RowIndex, 12)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 11, Grd1, e.RowIndex, 7);

            if (e.ColIndex == 9 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 10), Sm.GetGrdStr(Grd1, e.RowIndex, 12)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 11, Grd1, e.RowIndex, 9);
        }

        #endregion

        #region Grid 2

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBOMMaintenanceDlg3(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmBOMMaintenanceDlg3(this));
        }

        #endregion

        #endregion

        #endregion
    }
}
