﻿#region update

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesPerformanceItem : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptSalesPerformanceItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, Sm.ServerCurrentDateTime().Substring(4, 2));
                Sl.SetLueSPCode(ref LueSPCode);
                SetGrd();
               
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Sales Code", 
                       
                        //1-5
                        "Sales person",
                        "Group Code",
                        "Item Price Group",
                        "Total",
                        "January",
                        //6-10
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        //11-15
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        //16
                        "December",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        50, 
                        //1-5
                        120, 80, 180, 150, 100,   
                        //6-10
                        100, 100, 100, 100, 100, 
                        //11-15
                        100, 100, 100, 100, 100, 
                        //16
                        100, 20 
                         
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColReadOnly(Grd1, new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
        }

        private  void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            
            var lOmsetSalesPerson = new List<OmsetSalesPerson>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lOmsetSalesPerson);
                if (lOmsetSalesPerson.Count > 0)
                {
                    Process2(ref lOmsetSalesPerson);
                    Process3(ref lOmsetSalesPerson);
                }            
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                lOmsetSalesPerson.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
                Process4();
            }
        }

        #region Additional Method

        private void Process1(ref List<OmsetSalesPerson> l)
        {
            string Filter = " And 0=0 ";

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * ");
            SQL.AppendLine("From (  ");
	        SQL.AppendLine("    Select Z.SpCode, Z.SpName, '1' As GrpCode,  '500.000 - 999.9999' As Grpname ");
	        SQL.AppendLine("    From TblSalesPerson Z ");
	        SQL.AppendLine("    union All ");
	        SQL.AppendLine("    Select Z.SpCode, Z.SpName, '2' As GrpCode,  '>1.000.000' As Grpname ");
	        SQL.AppendLine("    From TblSalesPerson Z ");
            SQL.AppendLine(")X  ");
            SQL.AppendLine("Where X.SpCode In (Select SpCode From tblSalesPersonDtl Where UserCode = @UserCode ) ");

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSPCode), "X.SPCode", true);

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + Filter + " order By X.SpCode, GrpCode ;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SpCode",

                    //1-5
                    "Spname", "GrpCode", "GrpName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OmsetSalesPerson()
                        {
                            SPCode = Sm.DrStr(dr, c[0]),
                            SPName = Sm.DrStr(dr, c[1]),
                            GroupCode = Sm.DrStr(dr, c[2]),
                            Group = Sm.DrStr(dr, c[3]),
                            Total = 0m,
                            Mth01 = 0m,
                            Mth02 = 0m,
                            Mth03 = 0m,
                            Mth04 = 0m,
                            Mth05 = 0m,
                            Mth06 = 0m,
                            Mth07 = 0m,
                            Mth08 = 0m,
                            Mth09 = 0m,
                            Mth10 = 0m,
                            Mth11 = 0m,
                            Mth12 = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<OmsetSalesPerson> l)
        {
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (  ");
            SQL.AppendLine("    Select Z.SpCode, Z1.SpName, Z.Yr, Z.Mth, Z.typeTrx, If(Z.typeTrx='1', '500.000-999.999', '>=1.000.000') typeTrx2, SUM(Z.Amt) Amt from (  ");
            SQL.AppendLine("        Select * From (  ");
            SQL.AppendLine("            Select T1.SIDocNo, T1.SpCode, T1.Yr, T1.Mth, SUM(T1.Qty) Qty, SUM(T1.UPrice) UPrice, SUm(T1.Amt) Amt, T1.TypeTrx ");
			SQL.AppendLine("	            From ");
			SQL.AppendLine("	            ( ");
			SQL.AppendLine("		            Select F.DocNo As SIDocNo,  ifnull(L.SpCode, A.SpCode) As SpCode, Left(F.DocDt, 4) As yr, substring(F.DocDt, 5, 2) As Mth,  ");
			SQL.AppendLine("		            A.DocNo As SODocNo, B.Qty, D.Uprice, (B.Qty*D.Uprice) Amt, '1' As TypeTrx   ");
			SQL.AppendLine("		            From TblSOhdr A   ");
			SQL.AppendLine("		            Inner Join TblSODtl B On A.DocNo = B.DocNo   ");
			SQL.AppendLine("		            Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo   ");
			SQL.AppendLine("		            Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno   ");
			SQL.AppendLine("		            Inner Join TblSalesInvoiceHdr F On A.DocNo = F.SODocNO And F.CancelInd = 'N' And F.SODocNo is not null   ");
			SQL.AppendLine("		            Inner Join tblSalesInvoiceDtl G On F.DocNO = G.DocNO And B.DocNo = G.DOCtDocNo And B.Dno = G.DOCtDno   ");
			SQL.AppendLine("		            Left Join tblSalesPerson L On LEFT(F.salesname,LOCATE(' ',F.Salesname) - 1)  = L.UserCode  ");
			SQL.AppendLine("		            Where A.CancelInd = 'N' And Left(F.DocDt, 4) = @Yr And D.Uprice between 500000 And 999999 ");
			SQL.AppendLine("	            )T1 ");
			SQL.AppendLine("	            Group BY  T1.SIDocNo, T1.SpCode, T1.Yr, T1.Mth, T1.SODocNo, T1.TypeTrx  ");
            SQL.AppendLine("            Union All  ");
            SQL.AppendLine("            Select T2.SiDocNo, T2.SpCode, T2.Yr, T2.Mth, SUM(T2.Qty) Qty, SUM(T2.UPrice) UPrice, SUm(T2.Amt) Amt, T2.TypeTrx  ");
			SQL.AppendLine("	            From  ");
			SQL.AppendLine("	            ( ");
			SQL.AppendLine("		            Select H.SIDocNo, ifnull(I.SpCode, A.SpCode) As SpCode, Left(H.DocDt, 4) As yr, substring(H.DocDt, 5, 2) As Mth,  "); 
			SQL.AppendLine("		            A.DocNo, B.Qty, D.Uprice, (B.Qty*D.Uprice) Amt, '1' As TypeTrx   ");
			SQL.AppendLine("		            From TblSOhdr A   ");
			SQL.AppendLine("		            Inner Join TblSODtl B On A.DocNo = B.DocNo   ");
			SQL.AppendLine("		            Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo   ");
			SQL.AppendLine("		            Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno   ");
			SQL.AppendLine("		            Inner Join TblDRDtl F On B.DocNo = F.SoDocNo And B.DNo = F.SODNo   ");
			SQL.AppendLine("		            Inner Join TblDRHdr G On F.DocNo = G.DocNo And G.CancelInd= 'N'  ");
			SQL.AppendLine("		            Inner Join   ");
			SQL.AppendLine("		            (  ");
			SQL.AppendLine("		                Select X.DocNo, X2.Dno, X.DrDocNO, X2.DrDno, X.DocDt, X.SalesName, X.SiDocNo   ");
			SQL.AppendLine("		                from (  ");
			SQL.AppendLine("		                    Select Distinct A.DocNO, A.DRDocNo, D.DocDt, D.SalesName, D.DocNo As SIDocNO  ");
			SQL.AppendLine("		                    from TblDOCt2Hdr A  ");
			SQL.AppendLine("		                    Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo  ");
			SQL.AppendLine("		                    Inner Join tblSalesInvoiceDtl C On B.DocNo = C.DOCtDocNo And B.Dno = C.DOCtDno  ");
			SQL.AppendLine("		                    Inner Join TblSalesInvoiceHdr D On C.DocNo = D.DocNO And D.CancelInd = 'N' And D.SODocNo is null    ");                 
			SQL.AppendLine("		                )X  ");
			SQL.AppendLine("		                Inner Join TblDOCt2Dtl2 X2 On X.DocNo = X2.DocnO  ");
			SQL.AppendLine("		            )H On F.DocNo = H.DRDocNo And F.DNo = H.DRDno      ");
			SQL.AppendLine("		            Left Join tblSalesPerson I On LEFT(H.salesname,LOCATE(' ',H.Salesname) - 1)  = I.UserCode  ");
			SQL.AppendLine("		            Where A.CancelInd = 'N' And Left(H.DocDt, 4) = @Yr And D.UPrice between 500000 And 999999  ");
			SQL.AppendLine("	            )T2 ");
			SQL.AppendLine("	            Group BY  T2.SIDocNo, T2.SpCode, T2.Yr, T2.Mth, T2.TypeTrx ");
            SQL.AppendLine("            UNION ALL    ");
		    SQL.AppendLine("              Select  X1.SpCode, X1.Yr, X1.mth, X1.TrnNo, X1.Qty, X1.UPrice, (X1.Qty*(X1.PayAmtNett+X1.DiscAmt)) PayAmtNett, '1'   "); 
			SQL.AppendLine("	            From (    ");
			SQL.AppendLine("	                  Select A.TrnNo, A.BSDate, C.SpCode, Left(A.BSDate, 4) Yr, B.DiscAmt,  ");
			SQL.AppendLine("			            Substring(A.BSDate, 5, 2) Mth, B.Qty, B.UPrice, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett2, '3' As TypeDoc    ");
			SQL.AppendLine("	                  From TblPostrnhdr A    ");
			SQL.AppendLine("	                  Inner Join TblPostrnDtl B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo     ");
			SQL.AppendLine("	                  Inner Join Tblsalesperson C On A.UserCode = C.UserCode   ");
			SQL.AppendLine("	                    Where B.UPrice between 500000 And 999999  ");
			SQL.AppendLine("	            )X1 Where Left(X1.BSDate, 4) = @Yr    ");
            SQL.AppendLine("        )T3                       ");
            SQL.AppendLine("        UNion All   ");
            SQL.AppendLine("        Select * From (  ");
            SQL.AppendLine("            Select T1.SIDocNo, T1.SpCode, T1.Yr, T1.Mth, SUM(T1.Qty) Qty, SUM(T1.UPrice) UPrice, SUm(T1.Amt) Amt, T1.TypeTrx ");
			SQL.AppendLine("	            From ");
			SQL.AppendLine("	            ( ");
			SQL.AppendLine("		            Select F.DocNo As SIDocNo,  ifnull(L.SpCode, A.SpCode) As SpCode, Left(F.DocDt, 4) As yr, substring(F.DocDt, 5, 2) As Mth,  ");
			SQL.AppendLine("		            A.DocNo As SODocNo, B.Qty, D.Uprice, (B.Qty*D.Uprice) Amt, '2' As TypeTrx   ");
			SQL.AppendLine("		            From TblSOhdr A   ");
			SQL.AppendLine("		            Inner Join TblSODtl B On A.DocNo = B.DocNo   ");
			SQL.AppendLine("		            Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo   ");
			SQL.AppendLine("		            Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno   ");
			SQL.AppendLine("		            Inner Join TblSalesInvoiceHdr F On A.DocNo = F.SODocNO And F.CancelInd = 'N' And F.SODocNo is not null   ");
			SQL.AppendLine("		            Inner Join tblSalesInvoiceDtl G On F.DocNO = G.DocNO And B.DocNo = G.DOCtDocNo And B.Dno = G.DOCtDno   ");
			SQL.AppendLine("		            Left Join tblSalesPerson L On LEFT(F.salesname,LOCATE(' ',F.Salesname) - 1)  = L.UserCode  ");
			SQL.AppendLine("		            Where A.CancelInd = 'N' And Left(F.DocDt, 4) = @Yr And D.Uprice > 999999 ");
			SQL.AppendLine("	            )T1 ");
			SQL.AppendLine("	            Group BY  T1.SIDocNo, T1.SpCode, T1.Yr, T1.Mth, T1.SODocNo, T1.TypeTrx "); 
            SQL.AppendLine("            Union All  ");
            SQL.AppendLine("            Select T2.SiDocNo, T2.SpCode, T2.Yr, T2.Mth, SUM(T2.Qty) Qty, SUM(T2.UPrice) UPrice, SUm(T2.Amt) Amt, T2.TypeTrx  ");
			SQL.AppendLine("	            From  ");
			SQL.AppendLine("	            ( ");
			SQL.AppendLine("		            Select H.SIDocNo, ifnull(I.SpCode, A.SpCode) As SpCode, Left(H.DocDt, 4) As yr, substring(H.DocDt, 5, 2) As Mth,  "); 
			SQL.AppendLine("		            A.DocNo, B.Qty, D.Uprice, (B.Qty*D.Uprice) Amt, '2' As TypeTrx   ");
			SQL.AppendLine("		            From TblSOhdr A   ");
			SQL.AppendLine("		            Inner Join TblSODtl B On A.DocNo = B.DocNo   ");
			SQL.AppendLine("		            Inner Join tblCtQtHdr C On A.CtQtDocNo = C.DocNo   ");
			SQL.AppendLine("		            Inner Join TblCtQtDtl D On A.CtQtDocNo = D.DocNo And B.CtQtDno = D.Dno   ");
			SQL.AppendLine("		            Inner Join TblDRDtl F On B.DocNo = F.SoDocNo And B.DNo = F.SODNo   ");
			SQL.AppendLine("		            Inner Join TblDRHdr G On F.DocNo = G.DocNo And G.CancelInd= 'N'  ");
			SQL.AppendLine("		            Inner Join   ");
			SQL.AppendLine("		            (  ");
			SQL.AppendLine("		                Select X.DocNo, X2.Dno, X.DrDocNO, X2.DrDno, X.DocDt, X.SalesName, X.SiDocNo   ");
			SQL.AppendLine("		                from (  ");
			SQL.AppendLine("		                    Select Distinct A.DocNO, A.DRDocNo, D.DocDt, D.SalesName, D.DocNo As SIDocNO  ");
			SQL.AppendLine("		                    from TblDOCt2Hdr A  ");
			SQL.AppendLine("		                    Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo  ");
			SQL.AppendLine("		                    Inner Join tblSalesInvoiceDtl C On B.DocNo = C.DOCtDocNo And B.Dno = C.DOCtDno  ");
			SQL.AppendLine("		                    Inner Join TblSalesInvoiceHdr D On C.DocNo = D.DocNO And D.CancelInd = 'N' And D.SODocNo is null  ");                   
			SQL.AppendLine("		                )X  ");
			SQL.AppendLine("		                Inner Join TblDOCt2Dtl2 X2 On X.DocNo = X2.DocnO  ");
			SQL.AppendLine("		            )H On F.DocNo = H.DRDocNo And F.DNo = H.DRDno      ");
			SQL.AppendLine("		            Left Join tblSalesPerson I On LEFT(H.salesname,LOCATE(' ',H.Salesname) - 1)  = I.UserCode  ");
			SQL.AppendLine("		            Where A.CancelInd = 'N' And Left(H.DocDt, 4) = @Yr And D.UPrice > 999999  ");
			SQL.AppendLine("	            )T2 ");
            SQL.AppendLine("            UNION ALL    ");
	        SQL.AppendLine("             Select  X1.SpCode, X1.Yr, X1.mth, X1.TrnNo, X1.Qty, X1.UPrice, (X1.Qty*(X1.PayAmtNett+X1.DiscAmt)) PayAmtNett, '2'  ");  
			SQL.AppendLine("	            From (    ");
			SQL.AppendLine("	                  Select A.TrnNo, A.BSDate, C.SpCode, Left(A.BSDate, 4) Yr, B.DiscAmt,  ");
			SQL.AppendLine("			            Substring(A.BSDate, 5, 2) Mth, B.Qty, B.UPrice, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett, if(A.SlsType='S', B.UPrice, -1*B.UPrice) As PayAmtNett2, '3' As TypeDoc    ");
			SQL.AppendLine("	                  From TblPostrnhdr A    ");
			SQL.AppendLine("	                  Inner Join TblPostrnDtl B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo    "); 
			SQL.AppendLine("	                  Inner Join Tblsalesperson C On A.UserCode = C.UserCode   ");
			SQL.AppendLine("	                  Where B.UPrice >999999 And Left(A.BSDate, 4) = @Yr   ");
			SQL.AppendLine("	            )X1 Where Left(X1.BSDate, 4) = @Yr     ");
            SQL.AppendLine("        )T3  ");
            SQL.AppendLine("    )Z   ");
            SQL.AppendLine("    Inner Join Tblsalesperson Z1 On Z.SpCode = Z1.SpCode  ");
            SQL.AppendLine("    Group BY Z.SpCode, Z.Yr, Z.mth, Z.typeTrx  ");
            SQL.AppendLine("    order by SpCode  ");
            SQL.AppendLine(")X  ");
           

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SPCode", "Mth", "typeTrx", "Amt" });
                if (dr.HasRows)
                {
                    var SpCode = string.Empty;
                    var TypeTrx = string.Empty;
                    var Mth = string.Empty;
                    //var CountDays = 0m;
                    int Temp = 0;
                    while (dr.Read())
                    {
                        SpCode = Sm.DrStr(dr, c[0]);
                        TypeTrx = Sm.DrStr(dr, c[2]);
                        for (int i = Temp; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].SPCode, SpCode)&& Sm.CompareStr(l[i].GroupCode, TypeTrx))
                            {
                                switch (Sm.DrStr(dr, c[1]))
                                {
                                    case "01":
                                        l[i].Mth01 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "02":
                                        l[i].Mth02 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "03":
                                        l[i].Mth03 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "04":
                                        l[i].Mth04 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "05":
                                        l[i].Mth05 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "06":
                                        l[i].Mth06 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "07":
                                        l[i].Mth07 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "08":
                                        l[i].Mth08 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "09":
                                        l[i].Mth09 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "10":
                                        l[i].Mth10 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "11":
                                        l[i].Mth11 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                    case "12":
                                        l[i].Mth12 = Sm.DrDec(dr, c[3]);
                                        l[i].Total += Sm.DrDec(dr, c[3]);
                                        break;
                                }
                               //Temp = i;
                                break;
                            }
                        }
                        Grd1.GroupObject.Add(1);
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<OmsetSalesPerson> l)
        {
            var Yr = Sm.GetLue(LueYr);
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                //r.Cells[0].Value = i + 1;
                r.Cells[0].Value = l[i].SPCode;
                r.Cells[1].Value = l[i].SPName;
                r.Cells[2].Value = l[i].GroupCode;
                r.Cells[3].Value = l[i].Group;
                r.Cells[4].Value = l[i].Total;
                r.Cells[5].Value = l[i].Mth01;
                r.Cells[6].Value = l[i].Mth02;
                r.Cells[7].Value = l[i].Mth03;
                r.Cells[8].Value = l[i].Mth04;
                r.Cells[9].Value = l[i].Mth05;
                r.Cells[10].Value = l[i].Mth06;
                r.Cells[11].Value = l[i].Mth07;
                r.Cells[12].Value = l[i].Mth08;
                r.Cells[13].Value = l[i].Mth09;
                r.Cells[14].Value = l[i].Mth10;
                r.Cells[15].Value = l[i].Mth11;
                r.Cells[16].Value = l[i].Mth12;
            }
            Grd1.EndUpdate();
            //l.Clear();
            AdjustSubtotals();
            
        }

        private void Process4()
        {
            Grd1.BeginUpdate();
            string value = Sm.GetLue(LueMth);
            if (Sm.GetLue(LueMth).Length > 0)
            {
                switch (value)
                {
                    case "01":
                        Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
                        break;
                    case "02":
                        Sm.GrdColInvisible(Grd1, new int[] { 6 }, true);
                        break;
                    case "03":
                        Sm.GrdColInvisible(Grd1, new int[] { 7 }, true);
                        break;
                    case "04":
                        Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
                        break;
                    case "05":
                        Sm.GrdColInvisible(Grd1, new int[] { 9 }, true);
                        break;
                    case "06":
                        Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                        break;
                    case "07":
                        Sm.GrdColInvisible(Grd1, new int[] { 11 }, true);
                        break;
                    case "08":
                        Sm.GrdColInvisible(Grd1, new int[] { 12 }, true);
                        break;
                    case "09":
                        Sm.GrdColInvisible(Grd1, new int[] { 13 }, true);
                        break;
                    case "10":
                        Sm.GrdColInvisible(Grd1, new int[] { 14 }, true);
                        break;
                    case "11":
                        Sm.GrdColInvisible(Grd1, new int[] { 15 }, true);
                        break;
                    case "12":
                        Sm.GrdColInvisible(Grd1, new int[] { 16 }, true);
                        break;
                }

            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, true);
            }
            Grd1.EndUpdate();
        }

        private void AdjustSubtotals()
        {
            Grd1.GroupObject.Add(1);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #endregion

        #endregion

        #region Event
        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(Sl.SetLueSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSPCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sales person");
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                var f = new FrmRptSalesPerformanceItemDlg("", Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetLue(LueYr));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                 f.LblYear.Text = Sm.GetLue(LueYr);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                e.DoDefault = false;
                var f = new FrmRptSalesPerformanceItemDlg("", Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetLue(LueYr));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.LblYear.Text = Sm.GetLue(LueYr);
                f.ShowDialog();
            }
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            SetGrd();
            ShowData();
        }

        #endregion

        #region class

        private class OmsetSalesPerson
        {
            public string SPCode { get; set; }
            public string SPName { get; set; }
            public string GroupCode { get; set; }
            public string Group { get; set; }
            public decimal Total { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth02 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth04 { get; set; }
            public decimal Mth05 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth07 { get; set; }
            public decimal Mth08 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth10 { get; set; }
            public decimal Mth11 { get; set; }
            public decimal Mth12 { get; set; }
        }

        #endregion
     
    }
}
