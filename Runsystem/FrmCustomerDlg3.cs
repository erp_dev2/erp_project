﻿#region update
/*
 *  [03/10/2021] New apps 
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        private FrmCustomer mFrmParent;

        #endregion

        #region Constructor

        public FrmCustomerDlg3(FrmCustomer FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueCtCtCode(ref LueCtCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode, A.CtName, A.CtShortCode, B.CtCtName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Left Join TblCustomerCategory B On A.CtCtCode = B.CtCtCode ");
            SQL.AppendLine("Where 1=1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        
                        //1-5
                        "Customer's Code",
                        "",
                        "Customer's Name",
                        "Customer's Short Code",
                        "Customer Category",

                        //6-10
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 

                        //11
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        120, 20, 200, 120, 200, 
                        
                        //6-10
                        100, 100, 100, 100, 100, 

                        //11
                        100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCtName.Text, new string[] { "A.CtCode", "A.CtName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtCode), new string[] { "A.CtCtCode" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CtName;",
                    new string[] 
                    { 
                         //0
                         "CtCode", 
                         
                         //1-5
                         "CtName", 
                         "CtShortCode", 
                         "CtCtName",
                         "CreateBy",
                         "CreateDt", 

                         //6-7
                         "LastUpBy", 
                         "LastUpDt"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 7);

                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.mCopyDataActive = true;
                mFrmParent.CopyData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                var f = new FrmCustomer("");
                f.Tag = "";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCtCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.DoDefault = false;
                var f = new FrmCustomer("");
                f.Tag = "";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCtCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer Category");
        }

        #endregion

    }
}
