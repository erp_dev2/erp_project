﻿#region Update
/*
    01/03/2022 [BRI/PHT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPeriodicPromotionSetting : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmPeriodicPromotionSettingFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPeriodicPromotionSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Periodic Promotion Setting";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                Sl.SetLueGrdLvlCode(ref LueGradeCode);
                Sl.SetLueGrdLvlCode(ref LueGradeCode2);
                Sl.SetLueOption(ref LueGradeOddEven, "GradeOddEven");
                LueGradeCode.Visible = false;
                LueGradeCode2.Visible = false;
                LueGradeOddEven.Visible = false;

                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "GradeLevelCode",

                    //1-5
                    "Grade From",
                    "GradeLevelCode2",
                    "Grade To",
                    "Subtractor",
                    "ScaleCode",

                    //6
                    "Scale"
                },
                new int[]
                {
                    //0
                    0,

                    //1-5
                    100, 0, 100, 100, 0,

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 1);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5 });
        }

        internal void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, ChkCancelInd, LueGradeCode, LueGradeCode2, LueGradeOddEven

                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueGradeCode, LueGradeCode2, LueGradeOddEven

                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 4, 6 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, LueGradeCode, LueGradeCode2, LueGradeOddEven
            });
            ChkCancelInd.Checked = false;
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPeriodicPromotionSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo, DocDt, CancelInd ");
                SQL.AppendLine("From TblPeriodicPromotionSettingHdr ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-2
                            "DocDt", "CancelInd",

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        }, true
                    );
                ShowPeriodicPromotionSettingDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPeriodicPromotionSettingDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.GrdLvlCode, B.GrdLvlName, A.GrdLvlCode2, C.GrdLvlName GrdLvlName2, A.Subtractor, A.Scale, D.OptDesc ");
            SQL.AppendLine("From TblPeriodicPromotionSettingDtl A ");
            SQL.AppendLine("Inner Join TblGradeLevelHdr B ON A.GrdLvlCode = B.GrdLvlCode ");
            SQL.AppendLine("Inner Join TblGradeLevelHdr C ON A.GrdLvlCode2 = C.GrdLvlCode ");
            SQL.AppendLine("Inner Join TblOption D ON A.Scale = D.OptCode AND D.OptCat = 'GradeOddEven' ");
            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "GrdLvlCode", "GrdLvlName", "GrdLvlCode2", "GrdLvlName2", "Subtractor", "Scale", "OptDesc" },
                (MySqlDataReader dr, iGrid Grd1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PeriodicPromotionSetting", "TblPeriodicPromotionSettingHdr");

            cml.Add(SavePeriodicPromotionSettingHdr(DocNo));

            cml.Add(SavePeriodicPromotionSettingDtl(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdValueNotValid() ||
                IsGradeNotValid();
        }

        private MySqlCommand SavePeriodicPromotionSettingHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            //Cancel Document
            SQL.AppendLine("Update TblPeriodicPromotionSettingHdr ");
            SQL.AppendLine("SET CancelInd = 'Y', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("WHERE CancelInd = 'N'; ");

            SQL.AppendLine("Insert Into TblPeriodicPromotionSettingHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @CreateBy, CurrentDateTime()); ");

            #endregion


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePeriodicPromotionSettingDtl(string DocNo)
        {
            bool IsFirst = true;
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();

            SQLDtl.AppendLine("Insert Into TblPeriodicPromotionSettingDtl(DocNo, DNo, GrdLvlCode, GrdLvlCode2, Subtractor, Scale, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl.AppendLine(", ");
                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @GrdLvlCode_" + r.ToString() + ", @GrdLvlCode2_" + r.ToString() + ", @Subtractor_" + r.ToString() + ", @Scale_" + r.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                Sm.CmParam<String>(ref cm, "@GrdLvlCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                Sm.CmParam<Decimal>(ref cm, "@Subtractor_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                Sm.CmParam<String>(ref cm, "@Scale_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
            }
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            cm.CommandText = SQLDtl.ToString();

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPeriodicPromotionSetting());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private MySqlCommand EditPeriodicPromotionSetting()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPeriodicPromotionSettingHdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Grade From is empty")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Grade To is empty")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Scale is empty")) return true;
            }
            return false;
        }

        private bool IsGradeNotValid()
        {
            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                for (int r2 = 0; r2 < Grd1.Rows.Count - 1; r2++)
                {
                    if (r1 != r2 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r1, 1), Sm.GetGrdStr(Grd1, r2, 1)))
                    {
                        int rt1 = r1+1;
                        int rt2 = r2+1;
                        Sm.StdMsg(mMsgType.Warning, "Grade from row " + rt2 + " should not be same as row " + rt1);
                        Sm.FocusGrd(Grd1, r2, 1);
                        return true;
                    }

                    if (r1 != r2 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r1, 3), Sm.GetGrdStr(Grd1, r2, 3)))
                    {
                        int rt1 = r1+1;
                        int rt2 = r2+1;
                        Sm.StdMsg(mMsgType.Warning, "Grade to row " + rt2 + " should not be same as row " + rt1);
                        Sm.FocusGrd(Grd1, r2, 3);
                        return true;
                    }

                    if (r1 == r2 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r1, 1), Sm.GetGrdStr(Grd1, r2, 3)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Grade from should not be same as grade to ");
                        Sm.FocusGrd(Grd1, r2, 3);
                        return true;
                    }
                }  
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblPeriodicPromotionSettingHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueGradeCode_Validated(object sender, EventArgs e)
        {
            LueGradeCode.Visible = false;
        }

        private void LueGradeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGradeCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }

        private void LueGradeCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueGradeCode_Leave(object sender, EventArgs e)
        {
            if (LueGradeCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (LueGradeCode.Text.Trim().Length == 1)
                    Grd1.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueGradeCode).Trim();
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueGradeCode.GetColumnValue("Col2");
                }
            }
        }

        private void LueGradeCode2_Validated(object sender, EventArgs e)
        {
            LueGradeCode2.Visible = false;
        }

        private void LueGradeCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGradeCode2, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }

        private void LueGradeCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueGradeCode2_Leave(object sender, EventArgs e)
        {
            if (LueGradeCode2.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (LueGradeCode2.Text.Trim().Length == 1)
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueGradeCode2).Trim();
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueGradeCode2.GetColumnValue("Col2");
                }
            }
        }

        private void LueGradeOddEven_Validated(object sender, EventArgs e)
        {
            LueGradeOddEven.Visible = false;
        }

        private void LueGradeOddEven_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGradeOddEven, new Sm.RefreshLue2(Sl.SetLueOption), "GradeOddEven");
        }

        private void LueGradeOddEven_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueGradeOddEven_Leave(object sender, EventArgs e)
        {
            if (LueGradeOddEven.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (LueGradeOddEven.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueGradeOddEven).Trim();
                    Grd1.Cells[fCell.RowIndex, 6].Value = LueGradeOddEven.GetColumnValue("Col2");
                }
            }
        }

        #endregion

        #region Grid Event

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        Sm.LueRequestEdit(ref Grd1, ref LueGradeCode, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }

                    if (e.ColIndex == 3)
                    {
                        Sm.LueRequestEdit(ref Grd1, ref LueGradeCode2, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }

                    if (e.ColIndex == 6)
                    {
                        Sm.LueRequestEdit(ref Grd1, ref LueGradeOddEven, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
