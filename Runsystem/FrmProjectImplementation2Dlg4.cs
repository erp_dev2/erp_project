﻿#region Update
/*
    28/05/2020 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementation2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProjectImplementation2 mFrmParent;
        private int mRow = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementation2Dlg4(FrmProjectImplementation2 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T3.ItCode, T3.ItName, T3.ItGrpCode, T4.ItGrpName ");
            SQL.AppendLine("From (Select SOCDocNo From TblSOContractRevisionHdr Where DocNo = @SOCRDocNo) T1 ");
            SQL.AppendLine("Inner Join TblSOContractDtl T2 On T1.SOCDocNo = T2.DocNo ");
            SQL.AppendLine("Inner Join TblItem T3 On T2.ItCode = T3.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup T4 On T3.ItGrpCode = T4.ItGrpCode ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "DocType",
                    "BOQ#",
                    "BOM#",
                    "BOM Detail#",

                    //6-10
                    "",
                    "Item Code",
                    "Item Name",
                    "Group",
                    "ItGrpCode"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 0, 180, 180, 100, 
                    
                    //6-10
                    20, 120, 280, 200, 0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SOCRDocNo", mFrmParent.TxtSOContractDocNo.Text);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T3.ItCode", "T3.ItName", "T3.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T3.ItName; ",
                    new string[]
                    {
                        //0
                        "ItCode", 

                        //1-3
                        "ItName", "ItGrpName", "ItGrpCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 7))
            {
                mFrmParent.Grd2.Cells[mRow, 20].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.Grd2.Cells[mRow, 21].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                this.Close();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
