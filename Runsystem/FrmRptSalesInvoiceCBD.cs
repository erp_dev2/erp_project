﻿#region Update
/*
    11/02/2018 [TKG] Reporting sales invoice TOP MSI
    21/02/2018 [HAR] tambah button ke SO dan invoice
    30/01/2019 [TKG] bug kolom tergeser karena tambahan button
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesInvoiceCBD : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private List<Bank> lBank;
        private List<Vendor> lVendor;
        int mFixedCol = 15;
        int mBankColStart = 16;
        int mVendorColStart = 16;

        #endregion

        #region Constructor

        public FrmRptSalesInvoiceCBD(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                lBank = new List<Bank>();
                SetBank(ref lBank);

                lVendor = new List<Vendor>();
                SetVendor(ref lVendor);

                SetGrd();

                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Invoice#",
                        "",
                        "Date",
                        "Customer",
                        "Sales Person",
                        
                        //6-10
                        "Sales Order",
                        "",
                        "Currency",
                        "Sales",
                        "Tax",

                        //11-15
                        "Downpayment",
                        "Invoice",
                        "Freight"+Environment.NewLine+"(Debit)",
                        "Freight"+Environment.NewLine+"(Credit)",
                        "Outstanding"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 80, 200, 200,  
                        
                        //6-10
                        180, 20, 60, 120, 120,   
                        
                        //11-15
                        120, 120, 120, 120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2, 7 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15 });
                    
            Grd1.Cols.Count += (lBank.Count + lVendor.Count);

            int Col = mBankColStart - 1;
            for (int i = 0; i < lBank.Count; i++)
            {
                Col += 1;
                Grd1.Cols[Col].Text = "Bank" + Environment.NewLine + lBank[i].BankName.Replace(" ", Environment.NewLine);
                Grd1.Cols[Col].Width = 150;
                Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[Col].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[Col].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[Col].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
                Grd1.Cols[Col].CellStyle.ReadOnly = iGBool.True;

            }

            Col = mVendorColStart - 1;
            for (int i = 0; i < lVendor.Count; i++)
            {
                Col += 1;
                Grd1.Cols[Col].Text = "Expedition" + Environment.NewLine + lVendor[i].VdName.Replace(" ", Environment.NewLine);
                Grd1.Cols[Col].Width = 150;
                Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[Col].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[Col].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[Col].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
                Grd1.Cols[Col].CellStyle.ReadOnly = iGBool.True;
            }
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data3>();
            try
            {
                Process1(ref l1);
                if (l1.Count > 0)
                {
                    Process2(ref l1, ref l2);
                    Process3(ref l1, ref l3);
                    Process4(ref l1);
                    Process5(ref l2, ref l3);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                l3.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        private void Process1(ref List<Data1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            if (ChkCtCode.Checked) Sm.CmParam<string>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CtName, A.SalesName, A.SODocNo, A.CurCode, ");
            SQL.AppendLine("A.TotalAmt, A.TotalTax, A.DownPayment, A.Amt, IfNull(C.DAmt, 0.00) As DFreight, IfNull(D.CAmt, 0.00) As CFreight ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblSalesInvoiceDtl2 C On A.DocNo=C.DocNo And C.OptAcDesc='1' And C.DAmt<>'0' ");
            SQL.AppendLine("Left Join TblSalesInvoiceDtl2 D On A.DocNo=D.DocNo And D.OptAcDesc='1' And D.CAmt<>'0' ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.SODocNo Is Not Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            if (ChkCtCode.Checked) SQL.AppendLine("And A.CtCode=@CtCode ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CtName", "SalesName", "SODocNo", "CurCode", 

                    //6-10
                    "TotalAmt", "TotalTax", "DownPayment", "Amt", "DFreight",

                    //11
                    "CFreight"

                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data1()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            CtName = Sm.DrStr(dr, c[2]),
                            SalesName = Sm.DrStr(dr, c[3]),
                            SODocNo = Sm.DrStr(dr, c[4]),
                            CurCode = Sm.DrStr(dr, c[5]),
                            TotalAmt = Sm.DrDec(dr, c[6]),
                            TotalTax = Sm.DrDec(dr, c[7]),
                            DownPayment = Sm.DrDec(dr, c[8]),
                            Amt = Sm.DrDec(dr, c[9]),
                            Outstanding = 0m,
                            DFreight = Sm.DrDec(dr, c[10]),
                            CFreight = Sm.DrDec(dr, c[11])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Data1> l1, ref List<Data2> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            
            for (int i = 0; i < l1.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(T1.DocNo=@DocNo0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@DocNo0" + i.ToString(), l1[i].DocNo);
            }
            
            if (Filter.Length>0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1;";

            SQL.AppendLine("Select T1.DocNo, IfNull(T5.BankCode, 'CASH') As BankCode, ");
            SQL.AppendLine("Sum(T2.Amt) As Amt ");
            SQL.AppendLine("From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl T2 "); 
	        SQL.AppendLine("    On T1.DocNo=T2.InvoiceDocNo "); 
	        SQL.AppendLine("    And T2.InvoiceType='1' ");
            SQL.AppendLine("Inner Join TblIncomingPaymentHdr T3 ");
	        SQL.AppendLine("    On T2.DocNo=T3.DocNo "); 
	        SQL.AppendLine("    And T3.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblVoucherHdr T4 ");
	        SQL.AppendLine("    On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo ");
	        SQL.AppendLine("    And T3.CancelInd='N' ");
            SQL.AppendLine("Inner join TblBankAccount T5 On T4.BankAcCode=T5.BankAcCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By T1.DocNo, IfNull(T5.BankCode, 'CASH') ");
            SQL.AppendLine("Order By T1.DocNo, IfNull(T5.BankCode, 'CASH');");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "DocNo", "BankCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Data2()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            BankCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }

        }

        private void Process3(ref List<Data1> l1, ref List<Data3> l3)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            for (int i = 0; i < l1.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(T1.DocNo=@DocNo0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@DocNo0" + i.ToString(), l1[i].DocNo);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select Distinct T1.DocNo, T3.ExpVdCode As VdCode ");
	        SQL.AppendLine("From TblSalesInvoiceHdr T1 ");
	        SQL.AppendLine("Inner Join TblDRDtl T2 On T1.SODocNo=T2.SODocNo ");
            SQL.AppendLine("Inner Join TblDRHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.ExpVdCode Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By T1.DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "VdCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new Data3()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            VdCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Data1> l)
        {
            iGRow r;
            int n = 0;

            Grd1.BeginUpdate();
            foreach (var x in l)
            {
                r = Grd1.Rows.Add();
                n++;
                r.Cells[0].Value = n;
                r.Cells[1].Value = x.DocNo;
                r.Cells[3].Value = Sm.ConvertDate(x.DocDt);
                r.Cells[4].Value = x.CtName;
                r.Cells[5].Value = x.SalesName;
                r.Cells[6].Value = x.SODocNo;
                r.Cells[8].Value = x.CurCode;
                r.Cells[9].Value = x.TotalAmt;
                r.Cells[10].Value = x.TotalTax;
                r.Cells[11].Value = x.DownPayment;
                r.Cells[12].Value = x.Amt;
                r.Cells[13].Value = x.DFreight;
                r.Cells[14].Value = x.CFreight;
                r.Cells[15].Value = 0m;
            }

            Grd1.EndUpdate();
        }

        private void Process5(ref List<Data2> l2, ref List<Data3> l3)
        {
            decimal Amt = 0m;
            string DocNo = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                Grd1.BeginUpdate();
                for (int r = 0; r<Grd1.Rows.Count; r++)
                {
                    DocNo=Sm.GetGrdStr(Grd1, r, 1);
                    Amt = Sm.GetGrdDec(Grd1, r, 12);
                    for (int c = mBankColStart; c < Grd1.Cols.Count; c++)
                        Grd1.Cells[r, c].Value = 0m;

                    if (DocNo.Length != 0)
                    {
                        foreach (var x in l2.Where(w=>Sm.CompareStr(w.DocNo, DocNo)))
                        {
                            foreach (var i in lBank.Where(w => Sm.CompareStr(w.BankCode, x.BankCode)))
                            {
                                Amt -= x.Amt;
                                Grd1.Cells[r, i.Col].Value = x.Amt;
                            }
                        }
                        foreach (var x in l3.Where(w => Sm.CompareStr(w.DocNo, DocNo)))
                        {
                            foreach (var i in lVendor.Where(w => Sm.CompareStr(w.VdCode, x.VdCode)))
                                Grd1.Cells[r, i.Col].Value = Sm.GetGrdDec(Grd1, r, 14);
                        }
                        Grd1.Cells[r, 15].Value = Amt<0?0m:Amt;
                    }
                }
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 15 });

                bool IsExisted = false;
                for (int c = mVendorColStart; c < Grd1.Cols.Count; c++)
                {
                    IsExisted = false;
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdDec(Grd1, r, c) != 0)
                        {
                            IsExisted = true;
                            break;
                        }
                    }
                    Grd1.Cols[c].Visible = IsExisted;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { c });
                }
                Grd1.EndUpdate();
            }
        }

        private void SetBank(ref List<Bank> l)
        {
            mBankColStart = mFixedCol + 1;
            var GrdCol = mBankColStart-1;
            l.Add(new Bank()
            {
                BankCode = "CASH",
                BankName = "CASH",
                Col = GrdCol
            });
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select BankCode, IfNull(BankShNm, BankName) As BankName From TblBank Order By IfNull(BankShNm, BankName);";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BankCode", "BankName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 1;
                        l.Add(new Bank()
                        {
                            BankCode = Sm.DrStr(dr, c[0]),
                            BankName = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetVendor(ref List<Vendor> l)
        {
            mVendorColStart = mFixedCol + lBank.Count + 1;
            var GrdCol = mVendorColStart-1;
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = 
                    "Select VdCode, IfNull(ShortName, VdName) As VdName From TblVendor " +
                    "Where ActInd='Y' " +
                    "And VdCtCode In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VdCtCodeExpedition') " +
                    "Order By IfNull(ShortName, VdName);";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "VdName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 1;
                        l.Add(new Vendor()
                        {
                            VdCode = Sm.DrStr(dr, c[0]),
                            VdName = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Invoice#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSalesInvoice2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSalesInvoice2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class Bank
        {
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public int Col { get; set; }
        }

        private class Vendor
        {
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public int Col { get; set; }
        }

        private class Data1
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string SalesName { get; set; }
            public string SODocNo { get; set; }
            public string CurCode { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal DownPayment { get; set; }
            public decimal Amt { get; set; }
            public decimal Outstanding { get; set; }
            public decimal DFreight { get; set; }
            public decimal CFreight { get; set; }
        }

        private class Data2
        {
            public string DocNo { get; set; }
            public string BankCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class Data3
        {
            public string DocNo { get; set; }
            public string VdCode { get; set; }
        }

        #endregion

        
    }
}
