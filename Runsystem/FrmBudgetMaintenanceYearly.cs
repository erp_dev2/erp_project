﻿#region Update
/* 
    01/02/2021 [VIN/PHT] new apps
    29/04/2021 [WED/PHT] bisa edit data. data yg di edit adalah bulan di detail nya. hanya bisa edit bulan yg lebih besar dari bulan di sistem.
                         waktu save, akan save dan update ke budget summary berdasarkan parameter IsBudgetMaintenanceYearlyActived
    18/05/2021 [VIN/PHT] seqno 8 digit 
    20/05/2021 [HAR/PHT] mempercepat saat save budget summary
    08/06/2021 [TKG/PHT] budget request yearly# yg sudah digunakan tidak bisa digunakan kembali
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetMaintenanceYearly : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCCCode = string.Empty;
        internal FrmBudgetMaintenanceYearlyFind FrmFind;
        private string mReqTypeForNonBudget = string.Empty, mCurrYrMth = string.Empty;
        internal bool mIsBudget2YearlyFormat = false;
        private bool mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudgetShowPreviousAmt = false,
            mIsCASUsedForBudget = false,
            mIsBudgetMaintenanceYearlyActived = false
            ;
        internal bool mIsBudgetRequestUseItemAndQty = false;
        private int[]
            mRequestedCols = { 3, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33 },
            mBudgetCols = { 5, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 },
            mUsageBudgetCols = { 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 }
            ;

        #endregion

        #region Constructor

        public FrmBudgetMaintenanceYearly(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Budget";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                mCurrYrMth = Sm.GetValue("Select Date_Format(Now(), '%Y%m') ");
                SetGrd();
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 48;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "Budget Request DNo",

                    //1-5
                    "Budget"+Environment.NewLine+"Category",
                    "Budget"+Environment.NewLine+"Category",
                    "Requested Amount "+Environment.NewLine+"01",
                    "Previous" + Environment.NewLine + "Budget Amount "+Environment.NewLine+"",
                    "Budget Amount "+Environment.NewLine+"01",

                    //6-10
                    "Usage",
                    "Remark",
                    "Local"+Environment.NewLine+"Code",
                    "Item's Code",
                    "Item's Name",

                    //11-15
                    "",
                    "Quantity",
                    "Requested Amount"+Environment.NewLine+"02",
                    "Budget Amount "+Environment.NewLine+"02",
                    "Requested Amount "+Environment.NewLine+"03",

                    //16-20
                    "Budget Amount "+Environment.NewLine+"03",
                    "Requested Amount "+Environment.NewLine+"04",
                    "Budget Amount "+Environment.NewLine+"04",
                    "Requested Amount "+Environment.NewLine+"05",
                    "Budget Amount "+Environment.NewLine+"05",

                    //21-25
                    "Requested Amount "+Environment.NewLine+"06",
                    "Budget Amount "+Environment.NewLine+"06",
                    "Requested Amount "+Environment.NewLine+"07",
                    "Budget Amount "+Environment.NewLine+"07",
                    "Requested Amount "+Environment.NewLine+"08",

                    //25-30
                    "Budget Amount "+Environment.NewLine+"08",
                    "Requested Amount "+Environment.NewLine+"09",
                    "Budget Amount "+Environment.NewLine+"09",
                    "Requested Amount "+Environment.NewLine+"10",
                    "Budget Amount "+Environment.NewLine+"10",

                    //31-35
                    "Requested Amount "+Environment.NewLine+"11",
                    "Budget Amount "+Environment.NewLine+"11",
                    "Requested Amount "+Environment.NewLine+"12",
                    "Budget Amount "+Environment.NewLine+"12",
                    "",

                    //36-40
                    "Usage Amt 01",
                    "Usage Amt 02",
                    "Usage Amt 03",
                    "Usage Amt 04",
                    "Usage Amt 05",

                    //41-45
                    "Usage Amt 06",
                    "Usage Amt 07",
                    "Usage Amt 08",
                    "Usage Amt 09",
                    "Usage Amt 10",

                    //46-47
                    "Usage Amt 11",
                    "Usage Amt 12"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    0, 200, 130, 130, 130,

                    //6-10
                    130, 300, 100, 120, 150,

                    //11-15
                    20, 100, 130, 130, 130,

                    //16-20
                    130, 130, 130, 130, 130,

                    //21-25
                    130, 130, 130, 130, 130,

                    //26-30
                    130, 130, 130, 130, 130,

                    //31-35
                    130, 130, 130, 130, 20,

                    //36-40
                    0, 0, 0, 0, 0, 

                    //41-45
                    0, 0, 0, 0, 0, 

                    //46-47
                    0, 0
                }
            );
            Grd1.Cols[7].Move(35);
            Grd1.Cols[35].Move(3);

            Sm.GrdColButton(Grd1, new int[] { 11, 35 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4, 6, 8, 9, 10, 11, 12, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 8, 9, 10, 12, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33 });
        }

        override protected void HideInfoInGrd()
        {
            if (BtnSave.Enabled) Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeRemark }, true);
                    BtnBudgetRequestDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 5, 7, 11, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 });
                    //Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteDocDt, MeeRemark }, false);
                    BtnBudgetRequestDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 7, 11, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 });
                    Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    SetReadOnlyColumn();
                    break;
            }
        }

        private void ClearData()
        {
            mCCCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, 
                DteDocDt, TxtBudgetRequestDocNo, TxtYr, TxtCCCode,TxtStatus, 
                MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBudgetRequestAmt, TxtAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetMaintenanceYearlyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;
            
            if (Decimal.Parse(Sm.Left(mCurrYrMth, 4)) > Decimal.Parse(TxtYr.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't edit old document."); return;
            }
            if (Sm.Left(mCurrYrMth, 4) == TxtYr.Text && Sm.Right(mCurrYrMth, 2) == "12")
            {
                Sm.StdMsg(mMsgType.Warning, "You are already at the end of the year."); return;
            }

            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7 }, e);
            if (e.ColIndex == 5 || e.ColIndex == 14 || e.ColIndex == 16 || e.ColIndex == 18 || e.ColIndex == 20 || e.ColIndex == 22
                || e.ColIndex == 24 || e.ColIndex == 26 || e.ColIndex == 28 || e.ColIndex == 30 || e.ColIndex == 32 || e.ColIndex == 34) ComputeTotalAmount();
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
            if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string BCCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                string mBudgetRequestDocNo = TxtBudgetRequestDocNo.Text;
                string mSeqNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                var f = new FrmBudgetMaintenanceYearlyDlg2(this, e.RowIndex, TxtDocNo.Text, mSeqNo, BCCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(BtnSave.Enabled && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0 && Sm.IsGrdColSelected(new int[] { 5, 7, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 }, e.ColIndex)))
                e.DoDefault = false;

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
            if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                string BCCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                string mBudgetRequestDocNo = TxtBudgetRequestDocNo.Text;
                string mSeqNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                var f = new FrmBudgetMaintenanceYearlyDlg2(this, e.RowIndex, TxtDocNo.Text, mSeqNo, BCCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Budget", "TblBudgetMaintenanceYearlyHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBudgetHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count - 1; ++r)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveBudgetDtl(DocNo, r));

            if (mIsBudgetMaintenanceYearlyActived) cml.Add(SaveBudgetSummary(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget request#", false) ||
                IsBudgetRequestAlreadyCancelled() ||
                IsBudgetRequestYearlyAlreadyProcessed() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid2()||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 budget category.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            string Msg = string.Empty;

            decimal 
                BudgetRequestAmt01 = 0m,
                BudgetRequestAmt02 = 0m,
                BudgetRequestAmt03 = 0m,
                BudgetRequestAmt04 = 0m,
                BudgetRequestAmt05 = 0m,
                BudgetRequestAmt06 = 0m,
                BudgetRequestAmt07 = 0m,
                BudgetRequestAmt08 = 0m,
                BudgetRequestAmt09 = 0m,
                BudgetRequestAmt10 = 0m,
                BudgetRequestAmt11 = 0m,
                BudgetRequestAmt12 = 0m,
                BudgetAmt01 = 0m,
                BudgetAmt02 = 0m,
                BudgetAmt03 = 0m,
                BudgetAmt04 = 0m,
                BudgetAmt05 = 0m,
                BudgetAmt06 = 0m,
                BudgetAmt07 = 0m,
                BudgetAmt08 = 0m,
                BudgetAmt09 = 0m,
                BudgetAmt10 = 0m,
                BudgetAmt11 = 0m,
                BudgetAmt12 = 0m
                ;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                BudgetRequestAmt01 = Sm.GetGrdDec(Grd1, Row, 3);
                BudgetRequestAmt02 = Sm.GetGrdDec(Grd1, Row, 13);
                BudgetRequestAmt03 = Sm.GetGrdDec(Grd1, Row, 15);
                BudgetRequestAmt04 = Sm.GetGrdDec(Grd1, Row, 17);
                BudgetRequestAmt05 = Sm.GetGrdDec(Grd1, Row, 19);
                BudgetRequestAmt06 = Sm.GetGrdDec(Grd1, Row, 21);
                BudgetRequestAmt07 = Sm.GetGrdDec(Grd1, Row, 23);
                BudgetRequestAmt08 = Sm.GetGrdDec(Grd1, Row, 25);
                BudgetRequestAmt09 = Sm.GetGrdDec(Grd1, Row, 27);
                BudgetRequestAmt10 = Sm.GetGrdDec(Grd1, Row, 29);
                BudgetRequestAmt11 = Sm.GetGrdDec(Grd1, Row, 31);
                BudgetRequestAmt12 = Sm.GetGrdDec(Grd1, Row, 33);
                BudgetAmt01 = Sm.GetGrdDec(Grd1, Row, 5);
                BudgetAmt02 = Sm.GetGrdDec(Grd1, Row, 14);
                BudgetAmt03 = Sm.GetGrdDec(Grd1, Row, 16);
                BudgetAmt04 = Sm.GetGrdDec(Grd1, Row, 18);
                BudgetAmt05 = Sm.GetGrdDec(Grd1, Row, 20);
                BudgetAmt06 = Sm.GetGrdDec(Grd1, Row, 22);
                BudgetAmt07 = Sm.GetGrdDec(Grd1, Row, 24);
                BudgetAmt08 = Sm.GetGrdDec(Grd1, Row, 26);
                BudgetAmt09 = Sm.GetGrdDec(Grd1, Row, 28);
                BudgetAmt10 = Sm.GetGrdDec(Grd1, Row, 30);
                BudgetAmt11 = Sm.GetGrdDec(Grd1, Row, 32);
                BudgetAmt12 = Sm.GetGrdDec(Grd1, Row, 34);

                if (BudgetRequestAmt01 == 0 && BudgetAmt01 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt01, 0) 
                        );
                    Sm.FocusGrd(Grd1, Row, 5);
                    return true;
                }
                if (BudgetRequestAmt02 == 0 && BudgetAmt02 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt02, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 14);
                    return true;
                }
                if (BudgetRequestAmt03 == 0 && BudgetAmt03 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt03, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 16);
                    return true;
                }
                if (BudgetRequestAmt04 == 0 && BudgetAmt04 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt04, 0) 
                        );
                    Sm.FocusGrd(Grd1, Row, 18);
                    return true;
                }
                if (BudgetRequestAmt05 == 0 && BudgetAmt05 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt05, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 20);
                    return true;
                }
                if (BudgetRequestAmt06 == 0 && BudgetAmt06 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt06, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 22);
                    return true;
                }
                if (BudgetRequestAmt07 == 0 && BudgetAmt07 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt07, 0) 
                        );
                    Sm.FocusGrd(Grd1, Row, 24);
                    return true;
                }
                if (BudgetRequestAmt08 == 0 && BudgetAmt08 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt08, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 26);
                    return true;
                }
                if (BudgetRequestAmt09 == 0 && BudgetAmt09 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt09, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 28);
                    return true;

                }
                if (BudgetRequestAmt10 == 0 && BudgetAmt10 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt10, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 30);
                    return true;
                }
                if (BudgetRequestAmt11 == 0 && BudgetAmt11 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt11, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 32);
                    return true;
                }
                if (BudgetRequestAmt12 == 0 && BudgetAmt12 > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "You can't fill this column if requested amount is 0. budget amount: " + Sm.FormatNum(BudgetAmt12, 0)  
                        );
                    Sm.FocusGrd(Grd1, Row, 34);
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            decimal BudgetRequestAmt = 0m, BudgetAmt = 0m, UsageAmt = 0m;
            
            //RecomputeUsage();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Budget category is empty.")) return true;
                Msg = "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine;

                BudgetRequestAmt = Sm.GetGrdDec(Grd1, Row, 3);
                BudgetAmt = Sm.GetGrdDec(Grd1, Row, 5);
                UsageAmt = Sm.GetGrdDec(Grd1, Row, 6);

                //if (BudgetAmt > BudgetRequestAmt)
                //{
                //    Sm.StdMsg(mMsgType.Warning, 
                //        Msg +
                //        "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                //        "Budget amount : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                //        "Requested amount : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine + Environment.NewLine +
                //        "Budget amount is bigger than requested amount."
                //        );
                //}
                
                if (BudgetAmt < UsageAmt)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Budget category : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Budget amount : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                        "Used amount : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + Environment.NewLine +
                        "Budget amount is smaller than used amount."
                        );
                    return true;
                }

            }
            return false;
        }

        private bool IsBudgetRequestAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblBudgetRequestYearlyHdr Where DocNo=@Param And CancelInd='Y';", 
                TxtBudgetRequestDocNo.Text, 
                "Budget Request# : " + TxtBudgetRequestDocNo.Text + Environment.NewLine + 
                "This budget request already cancelled."
                );
        }

        private bool IsBudgetRequestYearlyAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select 1 From TblBudgetMaintenanceYearlyHdr Where BudgetRequestDocNo=@Param And Status In ('O', 'A') Limit 1;",
                TxtBudgetRequestDocNo.Text,
                "Budget Request Yearly# : " + TxtBudgetRequestDocNo.Text + Environment.NewLine +
                "This document already processed."
                );
        }

        private MySqlCommand SaveBudgetHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetMaintenanceYearlyHdr(DocNo, DocDt, Status, BudgetRequestDocNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @BudgetRequestDocNo, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BudgetMaintenanceYearly' ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*1 ");
                SQL.AppendLine("    From TblBudgetMaintenanceYearlyHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblBudgetMaintenanceYearlyHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BudgetMaintenanceYearly' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set BudgetDocNo = @DocNo ");
            SQL.AppendLine("Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BudgetMaintenanceYearly' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBudgetDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetMaintenanceYearlyDtl(DocNo, SeqNo, BudgetRequestDocNo, BudgetRequestSeqNo, Amt01, ");
            SQL.AppendLine("Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @BudgetRequestDocNo, @BudgetRequestDNo, @Amt01, ");
            SQL.AppendLine("@Amt02, @Amt03, @Amt04, @Amt05, @Amt06, @Amt07, @Amt08, @Amt09, @Amt10, @Amt11, @Amt12, @Remark, @UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000" + (Row + 1).ToString(), 8));
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 30));
            Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 32));
            Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 34));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd1, Row, 3));
            
            return cm;
        }

        private MySqlCommand SaveBudgetSummary(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete A From TblBudgetSummary A ");
             SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select C.Yr, D.Mth, E.DeptCode, D.BCCode ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyHdr A ");
            SQL.AppendLine("    Inner Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '01' As Mth, Amt01 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '02' As Mth, Amt02 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '03' As Mth, Amt03 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '04' As Mth, Amt04 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '05' As Mth, Amt05 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '06' As Mth, Amt06 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '07' As Mth, Amt07 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '08' As Mth, Amt08 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '09' As Mth, Amt09 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '10' As Mth, Amt10 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '11' As Mth, Amt11 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '12' As Mth, Amt12 As Amt2 ");
            SQL.AppendLine("        From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    ) B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestYearlyHdr C On A.DocNo = C.BudgetDocNo ");
            SQL.AppendLine("    Inner Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '01' As Mth, Amt01 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '02' As Mth, Amt02 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '03' As Mth, Amt03 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '04' As Mth, Amt04 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '05' As Mth, Amt05 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '06' As Mth, Amt06 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '07' As Mth, Amt07 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '08' As Mth, Amt08 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '09' As Mth, Amt09 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '10' As Mth, Amt10 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '11' As Mth, Amt11 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select DocNo, BCCode, SeqNo, '12' As Mth, Amt12 As Amt1 ");
            SQL.AppendLine("        From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    ) D On C.DocNo = D.DocNo And B.BudgetRequestDocNo = D.DocNo ");
            SQL.AppendLine("        And B.BudgetRequestSeqNo = D.SeqNo ");
            SQL.AppendLine("        And B.Mth = D.Mth ");
            SQL.AppendLine("    Inner Join TblBudgetCategory E On D.BCCode = E.BCCode ");
            SQL.AppendLine("    Where A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine(" )B On A.Yr = B.Yr And A.Mth = B.Mth And A.DeptCode = B.DeptCode And A.BCCode = B.BcCode ; ");

            SQL.AppendLine("Insert Into TblBudgetSummary(Yr, Mth, DeptCode, BCCode, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Select C.Yr, D.Mth, E.DeptCode, D.BCCode, D.Amt1, B.Amt2, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblBudgetMaintenanceYearlyHdr A ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '01' As Mth, Amt01 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '02' As Mth, Amt02 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '03' As Mth, Amt03 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '04' As Mth, Amt04 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '05' As Mth, Amt05 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '06' As Mth, Amt06 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '07' As Mth, Amt07 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '08' As Mth, Amt08 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '09' As Mth, Amt09 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '10' As Mth, Amt10 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '11' As Mth, Amt11 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BudgetRequestDocNo, BudgetRequestSeqNo, '12' As Mth, Amt12 As Amt2 ");
            SQL.AppendLine("    From TblBudgetMaintenanceYearlyDtl Where DocNo = @DocNo ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBudgetRequestYearlyHdr C On A.DocNo = C.BudgetDocNo ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '01' As Mth, Amt01 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '02' As Mth, Amt02 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '03' As Mth, Amt03 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '04' As Mth, Amt04 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '05' As Mth, Amt05 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '06' As Mth, Amt06 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '07' As Mth, Amt07 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '08' As Mth, Amt08 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '09' As Mth, Amt09 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '10' As Mth, Amt10 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '11' As Mth, Amt11 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo, BCCode, SeqNo, '12' As Mth, Amt12 As Amt1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyDtl Where DocNo = @BudgetRequestDocNo ");
            SQL.AppendLine(") D On C.DocNo = D.DocNo And B.BudgetRequestDocNo = D.DocNo ");
            SQL.AppendLine("    And B.BudgetRequestSeqNo = D.SeqNo ");
            SQL.AppendLine("    And B.Mth = D.Mth ");
            SQL.AppendLine("Inner Join TblBudgetCategory E On D.BCCode = E.BCCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBudgetHdr());
            cml.Add(DeleteBudgetDtl());

            for (int r = 0; r < Grd1.Rows.Count - 1; ++r)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveBudgetDtl(TxtDocNo.Text, r));

            if (mIsBudgetMaintenanceYearlyActived) cml.Add(SaveBudgetSummary(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            ProcessUsageBudget();

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsBudgetAmtInvalid()
                ;
        }

        private bool IsBudgetAmtInvalid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                for (int j = 0; j < mBudgetCols.Count(); ++j)
                {
                    if (Sm.GetGrdDec(Grd1, i, mBudgetCols[j]) < Sm.GetGrdDec(Grd1, i, mUsageBudgetCols[j]))
                    {
                        var Msg = new StringBuilder();

                        Msg.AppendLine("Budget Category : " + Sm.GetGrdStr(Grd1, i, 2));
                        Msg.AppendLine("Month           : " + Sm.Right(string.Concat("00", (j+1).ToString()), 2));
                        Msg.AppendLine("Budget Amount   : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, mBudgetCols[j]), 0));
                        Msg.AppendLine("Usage Budget    : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, mUsageBudgetCols[j]), 0));
                        Msg.AppendLine(Environment.NewLine);
                        Msg.AppendLine("Budget amount is less than current usage amount.");

                        Sm.StdMsg(mMsgType.Warning, Msg.ToString());
                        Sm.FocusGrd(Grd1, i, mBudgetCols[j]);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand EditBudgetHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBudgetMaintenanceYearlyHdr ");
            SQL.AppendLine("Set Amt = @Amt ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            return cm;
        }

        private MySqlCommand DeleteBudgetDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblBudgetMaintenanceYearlyDtl ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBudgetHdr(DocNo);
                ShowBudgetDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBudgetHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.BudgetRequestDocNo, B.Yr, '' Mth, ");
            SQL.AppendLine("B.CCCode, C.CCName, B.Amt As BudgetRequestAmt, A.Amt, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As Status, '' EntCode ");
            SQL.AppendLine("From TblBudgetMaintenanceYearlyHdr A ");
            SQL.AppendLine("Inner Join TblBudgetRequestYearlyHdr B On A.BudgetRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");


            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "BudgetRequestDocNo", "Yr", "Mth", "CCCode", 
                        //6-10
                        "CCName", "BudgetRequestAmt", "Amt", "Remark","Status",

                        //11
                        "EntCode",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtBudgetRequestDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtYr.EditValue = Sm.DrStr(dr, c[3]);
                        mCCCode = Sm.DrStr(dr, c[5]);
                        TxtCCCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtBudgetRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[10]);
                        
                    }, true
                );
        }

        private void ShowBudgetDtl(string DocNo)
        {
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BudgetRequestSeqNo, B.BCCode, C.LocalCode, C.BCName, B.Amt01 As BudgetRequestAmt01, A.Amt01, A.Remark, 0 Amt2, 0 PrevAmt,  ");
            SQL.AppendLine("G.ItCode, G.ItName, 0 Qty, ");
            SQL.AppendLine("A.Amt02, A.Amt03, A.Amt04, A.Amt05, A.Amt06, A.Amt07, A.Amt08, A.Amt09, A.Amt10, A.Amt11, A.Amt12,  ");
            SQL.AppendLine("B.Amt02 BudgetRequestAmt02 , B.Amt03 BudgetRequestAmt03, B.Amt04 BudgetRequestAmt04, B.Amt05 BudgetRequestAmt05, B.Amt06 BudgetRequestAmt06, B.Amt07 BudgetRequestAmt07,  "); 
            SQL.AppendLine("B.Amt08 BudgetRequestAmt08, B.Amt09 BudgetRequestAmt09, B.Amt10 BudgetRequestAmt10, B.Amt11 BudgetRequestAmt11, B.Amt12 BudgetRequestAmt12  ");
            SQL.AppendLine("From TblBudgetMaintenanceYearlyDtl A ");
            SQL.AppendLine("Left Join TblBudgetRequestYearlyDtl B On A.BudgetRequestDocNo=B.DocNo And A.BudgetRequestSeqNo=B.SeqNo ");
            SQL.AppendLine("Left Join TblBudgetCategory C On B.BCCode=C.BCCode ");
            SQL.AppendLine("Left Join TblBudgetRequestYearlyDtl2 C2 On B.DocNo=C2.DocNo and B.BCCode=C2.BCCode ");
            SQL.AppendLine("Left Join TblItem G On C2.ItCode = G.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo AND  B.DocNo=@BudgetRequestDocNo GROUP BY C.BCCode Order By A.SeqNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BudgetRequestSeqNo",
                    //1-5
                    "BCCode", "BCName", "BudgetRequestAmt01", "Amt01", "Remark",
                    //6-10
                    "LocalCode", "Amt2", "PrevAmt", "ItCode", "ItName",
                    //11-15
                    "Qty", "BudgetRequestAmt02", "Amt02", "BudgetRequestAmt03", "Amt03", 
                    //16-20
                    "BudgetRequestAmt04", "Amt04", "BudgetRequestAmt05", "Amt05", "BudgetRequestAmt06", 
                    //21-25
                    "Amt06", "BudgetRequestAmt07", "Amt07", "BudgetRequestAmt08", "Amt08", 
                    //26-30
                    "BudgetRequestAmt09","Amt09", "BudgetRequestAmt10", "Amt10","BudgetRequestAmt11",   
                    //31 - 33
                    "Amt11", "BudgetRequestAmt12", "Amt12" 
                    
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    if (mIsBudgetShowPreviousAmt) Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 8);
                    else Grd.Cells[Row, 4].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 29);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 31);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 32);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);
                    
                    //Grd.Cells[Row, 6].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
            23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Additional Method

        private void ProcessUsageBudget()
        {
            var l = new List<UsageBudget>();
            PrepData(ref l);
            if (l.Count > 0) SetUsageBudgetAmt(ref l);
            l.Clear();
        }

        private void PrepData(ref List<UsageBudget> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T1.BCCode, Right(T1.YrMth, 2) Mth, Sum(T1.Amt) Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select BCCode, DeptCode, Left(DocDt, 6) As YrMth, ");
            if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                SQL.AppendLine("    (B.Qty * B.EstPrice) As Amt ");
            else
                SQL.AppendLine("    (B.Qty*B.UPrice) As Amt ");
            SQL.AppendLine("    From TblMaterialRequestHdr A ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where B.cancelind = 'N' ");
            SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select BCCode, DeptCode, Left(DocDt, 6) YrMth, ");
            SQL.AppendLine("    Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where ReqType Is Not Null ");
            SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            SQL.AppendLine("    And Find_In_Set(DocType, ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 6) As YrMth, ");
            SQL.AppendLine("    (B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
            SQL.AppendLine("    From TblTravelRequestHdr A ");
            SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            SQL.AppendLine("    Where A.Cancelind = 'N'  ");
            SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T.BcCode, T.DeptCode, T.YrMth, T.Amt From ( ");
            SQL.AppendLine("        Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 6) As YrMth, ");
            SQL.AppendLine("        Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
            SQL.AppendLine("        From tblvoucherhdr A ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("    	(");
            SQL.AppendLine("    	    SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
            SQL.AppendLine("    		From tblvoucherhdr X1 ");
            SQL.AppendLine("    		INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("    		    AND X1.DocType = '58' ");
            SQL.AppendLine("                AND X1.CancelInd = 'N' ");
            SQL.AppendLine("                AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("    	) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("        Inner Join ");
            SQL.AppendLine("    	( ");
            SQL.AppendLine("	        SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("	    	From tblvoucherhdr X1  ");
            SQL.AppendLine("	    	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("	    	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("	    	    AND X1.DocType = '56' ");
            SQL.AppendLine("	    	    AND X1.CancelInd = 'N'  ");
            SQL.AppendLine("                AND X3.Status In ('O', 'A')  ");
            SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        Where Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("    ) T ");
            if (mIsCASUsedForBudget)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select C.BCCode, C.DeptCode, Left(A.DocDt, 6) YrMth, B.Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And A.`Status` = 'A' And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.DocStatus = 'F' ");
                SQL.AppendLine("        And B.CCtCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                SQL.AppendLine("        And C.CCtCode Is Not Null ");
            }
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Group By T1.BCCode, Right(T1.YrMth, 2); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new UsageBudget()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }

                dr.Close();
            }
        }

        private void SetUsageBudgetAmt(ref List<UsageBudget> l)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                foreach (var x in l.Where(w => w.BCCode == Sm.GetGrdStr(Grd1, i, 1)))
                {
                    for (int j = 0; j < mUsageBudgetCols.Count(); ++j)
                    {
                        if (j == (Int32.Parse(x.Mth) - 1))
                            Grd1.Cells[i, mUsageBudgetCols[j]].Value = x.Amt;
                    }
                }
            }
        }

        private void SetReadOnlyColumn()
        {
            if (TxtDocNo.Text.Length > 0)
            {
                if (Sm.Left(mCurrYrMth, 4) == TxtYr.Text)
                {
                    for (int i = Int32.Parse(Sm.Right(mCurrYrMth, 2)); i < mBudgetCols.Count(); ++i)
                    {
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { mBudgetCols[i] });
                    }
                    Sm.FocusGrd(Grd1, 0, mBudgetCols[Int32.Parse(Sm.Right(mCurrYrMth, 2))]);
                }

                if (Decimal.Parse(Sm.Left(mCurrYrMth, 4)) < Decimal.Parse(TxtYr.Text))
                {
                    for (int i = 0; i < mBudgetCols.Count(); ++i)
                    {
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { mBudgetCols[i] });
                    }
                    Sm.FocusGrd(Grd1, 0, mBudgetCols[0]);
                }
            }
        }

        private void GetParameter()
        {
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsBudgetShowPreviousAmt = Sm.GetParameterBoo("IsBudgetShowPreviousAmt");
            mIsBudgetRequestUseItemAndQty = Sm.GetParameterBoo("IsBudgetRequestUseItemAndQty");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsBudgetMaintenanceYearlyActived = Sm.GetParameterBoo("IsBudgetMaintenanceYearlyActived");
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where Doctype = 'Budget2'  Limit 1; ");
        }
        
        
        private void ComputeTotalAmount()
        {
            decimal Amt = 0m;
            decimal Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    Amt += (
                            Sm.GetGrdDec(Grd1, r, 3)+
                            Sm.GetGrdDec(Grd1, r, 13)+
                            Sm.GetGrdDec(Grd1, r, 15)+
                            Sm.GetGrdDec(Grd1, r, 17)+
                            Sm.GetGrdDec(Grd1, r, 19)+
                            Sm.GetGrdDec(Grd1, r, 21)+
                            Sm.GetGrdDec(Grd1, r, 23)+
                            Sm.GetGrdDec(Grd1, r, 25)+
                            Sm.GetGrdDec(Grd1, r, 27)+
                            Sm.GetGrdDec(Grd1, r, 29)+
                            Sm.GetGrdDec(Grd1, r, 31)+
                            Sm.GetGrdDec(Grd1, r, 33)
                            );
            TxtBudgetRequestAmt.Text = Sm.FormatNum(Amt, 0);

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    Amt2 += (
                        Sm.GetGrdDec(Grd1, r, 5)+
                        Sm.GetGrdDec(Grd1, r, 14) +
                        Sm.GetGrdDec(Grd1, r, 16) +
                        Sm.GetGrdDec(Grd1, r, 18) +
                        Sm.GetGrdDec(Grd1, r, 20) +
                        Sm.GetGrdDec(Grd1, r, 22) +
                        Sm.GetGrdDec(Grd1, r, 24) +
                        Sm.GetGrdDec(Grd1, r, 26) +
                        Sm.GetGrdDec(Grd1, r, 28) +
                        Sm.GetGrdDec(Grd1, r, 30) +
                        Sm.GetGrdDec(Grd1, r, 32) +
                        Sm.GetGrdDec(Grd1, r, 34)
                        );
            TxtAmt.Text = Sm.FormatNum(Amt2, 0);
        }

        internal void ShowBudgetRequestInfo()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ifnull(A.Amt01, 0) Amt01, ifnull(A.Amt02, 0) Amt02, ifnull(A.Amt03, 0) Amt03, ifnull(A.Amt04, 0) Amt04, ifnull(A.Amt05, 0)Amt05, ifnull(A.Amt06, 0)Amt06, ifnull(A.Amt07,0) Amt07,  ");
            SQL.AppendLine("ifnull(A.Amt08,0) Amt08, ifnull(A.Amt09,0) Amt09, ifnull(A.Amt10,0)Amt10, ifnull(A.Amt11,0) Amt11, ifnull(A.Amt12,0) Amt12, A.SeqNo DNo, A.BCCode, B.BCName,  NULL AS LocalCode, 0.00 As Amt2, 0.00 as Used, 0.00 as Qty01, NULL AS ItCode, NULL AS ItName ");
            SQL.AppendLine("From TblBudgetRequestYearlyDtl A ");
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.SeqNo; ");
            
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", TxtYr.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
            Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "BCCode", "BCName", "Amt01", "Amt2", "Used",

                    //6-9
                    "LocalCode", "ItCode", "ItName", "Qty01", "Amt02",

                    //11-15
                    "Amt03", "Amt04", "Amt05", "Amt06", "Amt07",

                    //16-20
                    "Amt08", "Amt09", "Amt10", "Amt11", "Amt12",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 20);
                    Grd.Cells[Row, 7].Value = null;
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 5, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 });

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 });
            Sm.FocusGrd(Grd1, 0, 2);
            ComputeTotalAmount();

        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnBudgetRequestDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBudgetMaintenanceYearlyDlg(this));
        }

        private void BtnBudgetRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget request#", false))
            {
                var f1 = new FrmBudgetRequestYearly(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtBudgetRequestDocNo.Text;
                f1.ShowDialog();
            }
        }
       
        #endregion

        #endregion

        #region Class

        private class UsageBudget
        {
            public string BCCode { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}

