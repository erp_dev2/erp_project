﻿#region Update
/*
    06/06/2018 [TKG] filter department tidak jalan.
    08/06/2018 [TKG] tambah info dan filter payrun period 
    11/04/2023 [HAR/GSS] penyesuaian printout
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHASlip : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private bool mIsNotFilterByAuthorization = false;

        #endregion

        #region Constructor

        public FrmRHASlip(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method
        
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                BtnSave.Text = "  &Print";
                GetParameter();
                SetGrd();
                SetLueAGCode(ref LueAGCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name", 
                        "Old Code",
                        "Position",
                        "Department",
                        
                        //6-8
                        "Payrun Period",
                        "Join",
                        "Resign"
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-5
                        100, 250, 100, 150, 150, 
                        
                        //6-8
                        130, 80, 80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsDteEmpty(DteHolidayDt, "Holiday date")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, G.OptDesc As PayrunPeriodDesc, A.JoinDt, A.ResignDt ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 &&
                    !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And F.AGCode=@AGCode ");
                SQL.AppendLine("Left Join TblOption G On A.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
                SQL.AppendLine("Where A.EmpCode In ( ");
                SQL.AppendLine("    Select B.EmpCode ");
                SQL.AppendLine("    From TblRHAHdr A, TblRHADtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A' ");
                SQL.AppendLine("    And A.HolidayDt=@HolidayDt ");
                SQL.AppendLine(") ");
                if (
                    Sm.GetLue(LueAGCode).Length > 0 &&
                    Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup")
                    )
                {
                    SQL.AppendLine("And ( ");
                    SQL.AppendLine("    A.EmpCode Not In ( ");
                    SQL.AppendLine("        Select B.EmpCode ");
                    SQL.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                    SQL.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(") ");
                }
                if (Sm.GetLue(LueDeptCode).Length > 0)
                    SQL.AppendLine("And A.DeptCode=@DeptCode ");

                if (ChkPayrunPeriod.Checked)
                    SQL.AppendLine("And A.PayrunPeriod=@PayrunPeriod ");

                if (!mIsNotFilterByAuthorization)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select EmpCode From TblEmployee ");
                    SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                    SQL.AppendLine("    And GrdLvlCode In ( ");
                    SQL.AppendLine("        Select T2.GrdLvlCode ");
                    SQL.AppendLine("        From TblPPAHdr T1 ");
                    SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }

                SQL.AppendLine("Order By A.EmpName;");
                
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (ChkPayrunPeriod.Checked)
                    Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
                Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
                if (Sm.GetLue(LueAGCode).Length > 0)
                    Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
                if (Sm.GetLue(LueDeptCode).Length>0)
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString(),
                        new string[] { 
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "PayrunPeriodDesc", 
                            
                            //6-7
                            "JoinDt", "ResignDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return 
                Sm.IsDteEmpty(DteHolidayDt, "Holiday date") ||
                IsDataNotExisted();
        }

        private bool IsDataNotExisted()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to print minimum 1 record.");
            return true;
        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string Filter = string.Empty;
            List<IList> myLists = new List<IList>();
            var l = new List<RHA>();
            var cm = new MySqlCommand();
            string[] TableName = { "RHA" };
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1); 
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length>0)
                Filter = " And (" + Filter + ")";

            //SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail',");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            //SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d-%M-%y')As DocDt, B.EmpCode, C.EmpName, C.EmpCodeOld, D.DeptName, ");
            //SQL.AppendLine("B.Amt, B.Tax, (B.Amt+B.Tax)Total,  ");
            //SQL.AppendLine("From TblRHAHdr A ");
            //SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
            //SQL.AppendLine("Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
            //SQL.AppendLine("Where A.HolidayDt=@HolidayDt ");
            //SQL.AppendLine("And A.CancelInd='N' ");
            //SQL.AppendLine("And A.Status='A' ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine("Order By C.EmpName;");



            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode = 'ReportTitle1') As 'CompanyName',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode = 'ReportTitle2') As 'CompanyAddress',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode = 'ReportTitle3') As 'CompanyAddressCity',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode = 'ReportTitle4') As 'CompanyPhone',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode = 'ReportTitle5') As 'CompanyEmail',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode = 'CompanyLocation2') As 'CompLocation2',  ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt, '%d-%M-%y')As DocDt, B.EmpCode, C.EmpName, C.EmpCodeOld, D.DeptName,  ");
            SQL.AppendLine("B.Amt, B.Tax, (B.Amt + B.Tax)Total, E.PosName,  C.JoinDt,   ");          
            SQL.AppendLine("case ");
            SQL.AppendLine("when C.PgCode = '2' Then ifnull(G.value, 0.00)  ");
            SQL.AppendLine("when C.PgCode = '3' Then ifnull(G.value, 0.00)   ");
            SQL.AppendLine("ELSE IfNull(H.BasicsALARY, 0.00)   ");
            SQL.AppendLine("end As basicsalary, ");
            SQL.AppendLine("F.TunjJabat,    ");
            SQL.AppendLine("I.TunjFungsi,   ");
            SQL.AppendLine("J.TunjKeahlian,   ");
            SQL.AppendLine("K.Tunjdaerah,   ");
            SQL.AppendLine("L.Tunjinisiatif, A.Remark   ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode = C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On C.DeptCode = D.DeptCode ");
            SQL.AppendLine("LEFT JOIN tblposition E ON C.PosCode = E.PosCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T1.EmpCode, T1.AdCode, t2.AdName, Sum(T1.Amt) As TunjJabat ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction T2 On T1.ADCode= T2.ADCode And T2.ADType= 'A' And T2.AmtType= '1' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt <= @HolidayDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt <= T1.EndDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt <= @HolidayDt And @HolidayDt <= T1.EndDt) ");
            SQL.AppendLine(")  AND T1.ADCode = '003' ");
            SQL.AppendLine("GROUP BY T1.EmpCode, T1.AdCode, t2.AdName ");
            SQL.AppendLine(")F ON C.EmpCode = F.EmpCode ");
            SQL.AppendLine("Left JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T2.EmpCode, T2.Amt As Value ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select EmpCode, Max(StartDt) StartDt ");
            SQL.AppendLine("From TblEmployeeSalary ");
            SQL.AppendLine("Where StartDt<= @HolidayDt ");
            SQL.AppendLine("Group By EmpCode  ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployeeSalary T2 On T1.EmpCode = T2.EmpCode And T1.StartDt = T2.StartDt ");
            SQL.AppendLine(")G On C.EmpCode = G.EmpCode  ");
            SQL.AppendLine("LEFT JOIN tblgradelevelhdr H  ON C.grdlvlcode = H.GrdLvlCode  ");
            SQL.AppendLine("LEFT JOIN  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T1.EmpCode, T1.AdCode, t2.AdName, Sum(T1.Amt) As TunjFungsi ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction T2 On T1.ADCode= T2.ADCode And T2.ADType= 'A' And T2.AmtType= '1' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt <= @HolidayDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt <= T1.EndDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt <= @HolidayDt And @HolidayDt <= T1.EndDt) ");
            SQL.AppendLine(")  AND T1.ADCode = '002' ");
            SQL.AppendLine("GROUP BY T1.EmpCode, T1.AdCode, t2.AdName ");
            SQL.AppendLine(")I ON C.EmpCode = I.EmpCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T1.EmpCode, T1.AdCode, t2.AdName, Sum(T1.Amt) As TunjKeahlian ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction T2 On T1.ADCode= T2.ADCode And T2.ADType= 'A' And T2.AmtType= '1' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt <= @HolidayDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt <= T1.EndDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt <= @HolidayDt And @HolidayDt <= T1.EndDt) ");
            SQL.AppendLine(")  AND T1.ADCode = '004' ");
            SQL.AppendLine("GROUP BY T1.EmpCode, T1.AdCode, t2.AdName ");
            SQL.AppendLine(")J ON C.EmpCode = J.EmpCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T1.EmpCode, T1.AdCode, t2.AdName, Sum(T1.Amt) As Tunjdaerah ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction T2 On T1.ADCode= T2.ADCode And T2.ADType= 'A' And T2.AmtType= '1' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt <= @HolidayDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt <= T1.EndDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt <= @HolidayDt And @HolidayDt <= T1.EndDt) ");
            SQL.AppendLine(")  AND T1.ADCode = '010' ");
            SQL.AppendLine("GROUP BY T1.EmpCode, T1.AdCode, t2.AdName ");
            SQL.AppendLine(")K ON C.EmpCode = K.EmpCode ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select T1.EmpCode, T1.AdCode, t2.AdName, Sum(T1.Amt) As Tunjinisiatif ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction T2 On T1.ADCode= T2.ADCode And T2.ADType= 'A' And T2.AmtType= '1' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt <= @HolidayDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt <= T1.EndDt) Or ");
            SQL.AppendLine("(T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt <= @HolidayDt And @HolidayDt <= T1.EndDt) ");
            SQL.AppendLine(")  AND T1.ADCode = '011' ");
            SQL.AppendLine("GROUP BY T1.EmpCode, T1.AdCode, t2.AdName ");
            SQL.AppendLine(")L ON C.EmpCode = L.EmpCode ");
            SQL.AppendLine("Where A.HolidayDt = @HolidayDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("Order By C.EmpName;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                    //0
                     "CompanyLogo",

                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressCity",
                     "CompanyPhone",
                     "CompanyEmail",

                     //6-10
                     "DocNo",
                     "DocDt",
                     "EmpCode",
                     "EmpName",
                     "EmpCodeOld",

                     //11-15
                     "DeptName",
                     "Amt",
                     "Tax",
                     "Total",
                     "PosName",
                     //16-20
                     "JoinDt",
                     "basicsalary",
                     "TunjJabat",
                     "TunjFungsi",
                     "TunjKeahlian",
                     //21-22
                     "Tunjdaerah",
                     "Tunjinisiatif",
                     "Remark"
    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RHA()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            EmpCode = Sm.DrStr(dr, c[8]),
                            EmpName = Sm.DrStr(dr, c[9]),
                            EmpCodeOld = Sm.DrStr(dr, c[10]),
                            DeptName = Sm.DrStr(dr, c[11]),
                            Amt = Sm.DrDec(dr, c[12]),
                            Tax = Sm.DrDec(dr, c[13]),
                            Total = Sm.DrDec(dr, c[14]),
                            Position = Sm.DrStr(dr, c[15]),
                            JoinDt = Sm.DrStr(dr, c[16]),
                            BasicSalary = Sm.DrDec(dr, c[17]),
                            TunjJabat = Sm.DrDec(dr, c[18]),
                            TunjFungsi = Sm.DrDec(dr, c[19]),
                            TunjKeahlian = Sm.DrDec(dr, c[20]),
                            Tunjdaerah = Sm.DrDec(dr, c[21]),
                            Tunjinisiatif = Sm.DrDec(dr, c[22]),
                            Remark = Sm.DrStr(dr, c[23]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            if(Doctitle == "GSS")
                Sm.PrintReport("PaySlipRHAGSS", myLists, TableName, false);
            else
                Sm.PrintReport("PaySlipRHA", myLists, TableName, false);
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0) e.DoDefault = false;
        }

        #endregion

        internal void SetLueAGCode(ref LookUpEdit Lue, string DeptCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')=@DeptCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AllEmployeeWithNoAttendanceGroup' As Col1, 'All Employee With No Attendance Group' As Col2 ");
            SQL.AppendLine("Order By Col2;");

            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            cm.CommandText = SQL.ToString();

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Event

        private void DteHolidayDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.SetLue(LueAGCode, "<Refresh>");
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), Sm.GetLue(LueDeptCode));
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), Sm.GetLue(LueDeptCode));
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        #endregion

        #region Class

        class RHA
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal Amt { get; set; }
            public decimal Tax { get; set; }
            public decimal Total { get; set; }
            public string PrintBy { get; set; }
            public string Position { get; set; }
            public string JoinDt { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal TunjJabat { get; set; }
            public decimal TunjFungsi { get; set; }
            public decimal TunjKeahlian { get; set; }
            public decimal Tunjdaerah { get; set; }
            public decimal Tunjinisiatif { get; set; }

            public string Remark { get; set; }
        }

        #endregion
    }
}
