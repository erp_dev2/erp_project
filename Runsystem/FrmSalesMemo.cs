﻿#region Update
/*
    12/03/2020 [DITA/KSM] new apps
    31/03/2020 [VIN/KSM] Printout SalesMemo
    27/04/2020 [IBL/IMS] Tambah Document Approval
    05/05/2020 [IBL/KSM] Tambah tanda tangan otomatis di printout
    05/05/2020 [IBL/KSM] Print out berdasarkan Site (Spinning dan Weaving). Dengan parameter mSpinningSiteCode dan mWeavingSiteCode
    08/05/2020 [IBL/KSM] Menyesuaikan printout nama penjual
    12/05/2020 [DITA/KSM] Penomoran dokumen berdasarkan site
    26/05/2020 [VIN/KSM] Tambah Product Name dan Tax Indikator di header
    16/06/2020 [IBL/KSM] nomor dokumen site weaving dibedakan antara ppn dan non ppn
    25/06/2020 [IBL/KSM] pada saat edit, item di detail tidak boleh dihapus
    25/06/2020 [IBL/KSM] tidak bisa cancel dokumen selama dipakai di DRSC yang belum cancel
    16/06/2020 [IBL/KSM] nomor dokumen site spinning dibedakan antara ppn dan non ppn
    03/12/2020 [VIN/KSM] LocalDocNo tergenerate otomatis berdasarkan parameter=IsSalesMemoGenerateLocalDocNo
    11/12/2020 [VIN/KSM] Penyesuaian Printout
    14/12/2020 [TKG/KSM] IsDocumentAlreadyProceedToDRSC() masih ada bug
    15/12/2020 [VIN/KSM] Generate local DocNo, tidak reset berdasarkan customer short code
    28/12/2020 [VIN/KSM] bug printout: DocNo --> Local DocNo
    30/12/2020 [VIN/KSM] bug saat save 
    02/02/2021 [VIN/KSM] Printout Sales Memo ttd Create By bukan dr approval level 1
    02/02/2021 [WED/KSM] warning tambahan jika include PPN tidak di centang
 *  11/02/2021 [ICA/KSM] Local docno masih bisa di edit dan disimpan berdasarkan yg diisikan
 *  18/02/2021 [RDH/KSM] Menambahkan identitas Customer's Category pada Combo Box dan FIND
    24/03/2021 [VIN/KSM] penyesuaian menu
    24/03/2021 [VIN/KSM] tambah unit price after tax
    29/03/2021 [VIN/KSM] membedakan source harga di printout PPn dan non PPN 
    09/11/2021 [ARI/KSM] Membuat field currency menjadi mandatory
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmSalesMemo : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = string.Empty;
        internal bool mIsFilterBySite = false,
            mIsCustomerComboShowCategory = false;
        private bool mIsDocNoFormatBasedOnSite = false;
        private bool mIsInsert = false,
            mIsSalesMemoGenerateLocalDocNo = false,
            mIsClearData = false;
        
        iGCell fCell;
        bool fAccept;
        internal FrmSalesMemoFind FrmFind;

        internal string 
            mSpinningSiteCode = string.Empty, 
            mWeavingSiteCode = string.Empty,
            mPPNSiteCodeForSalesMemo = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesMemo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Sales Memo";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueSiteCode(ref LueSiteCode, string.Empty);
                SetLueCtCode(ref LueCtCode, string.Empty);
                
                Sl.SetLueSPCode(ref LueSPCode);
                SetGrd();
                SetFormControl(mState.View);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "Local Code",
                        "",
                        "Item's Name",
                        
                        //6-10
                        "Product Name",
                        "Quantity",
                        "UoM",
                        "Unit Price",
                        "Total",
                        

                        //11-14
                        "Delivery Date",
                        "Remark", 
                        "Unit Price After Tax",
                        "Total After Tax"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        20, 80, 80, 20, 200, 
                        
                        //6-10
                        150, 100, 100, 100, 150,   
                        
                        //11-14
                        100, 200, 130, 130
                    }
                );
            Grd1.Cols[13].Move(11);
            Grd1.Cols[14].Move(12);
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 10, 13, 14 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 4});
            Sm.GrdFormatDate(Grd1, new int[] { 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, MeeCancelReason, DteDocDt, LueCtCode,LueSiteCode, LueCurCode, 
                       LueSPCode, TxtAmt, TxtTotalAmt, MeeRemark, ChkTaxInd
                    }, true);
                   
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10 ,11, 12, 13, 14 });
                    ChkCancelInd.Enabled = false;
                    ChkTaxInd.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueCtCode,LueSiteCode, LueCurCode, LueSPCode, MeeRemark, 
                       // ChkTaxInd  
                    }, false);
                    mIsInsert = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 6, 7, 9, 11, 12 });
                    
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                      MeeCancelReason
                    }, false);
                    ChkCancelInd.Enabled = true;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mIsClearData = true;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, MeeCancelReason, DteDocDt, LueCtCode,LueSiteCode, LueCurCode, LueSPCode, MeeRemark, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtAmt, TxtTotalAmt
            }, 0);
            
            ChkTaxInd.Checked = false;
            ChkCancelInd.Checked = false;

            ClearGrd();
           
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 9 , 10, 13, 14});
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesMemoFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                mIsClearData = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            mIsInsert = false;
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData(sender, e);
                    mIsInsert = false;
                }
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint();
        }
        

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmSalesMemoDlg(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 7, 9, 10, 11, 13, 14 }, e.ColIndex))
                    {
                        if (e.ColIndex == 11) Sm.DteRequestEdit(Grd1, DteDeliveryDt, ref fCell, ref fAccept, e);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 10, 13, 14 });
                    }

                }
              
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (mIsInsert)
            {
                if (BtnSave.Enabled && !Grd1.ReadOnly)
                {
                    Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeAmt();
                    ComputeTotalAmtAfterTax();
                }
            }
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) //
        {
            
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 )
                Sm.FormShowDialog(new FrmSalesMemoDlg(this));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
          
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7, 9 }, e);
            if (e.ColIndex == 7 || e.ColIndex == 9 || e.ColIndex == 13)
            {
                ComputeTotal(e.RowIndex);
                ComputeAmt();
                ComputeTotalAmtAfterTax();
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            string SiteCode = LueSiteCode.Text;
            bool PPNChecked = ChkTaxInd.Checked;

            if (!ChkTaxInd.Checked) if (Sm.StdMsgYN("Question", "Include PPN is untick. Do you want to proceed ?") == DialogResult.No) { ChkTaxInd.Focus(); return; }
            
            if(IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            if (mIsDocNoFormatBasedOnSite)
            {
                if (Sm.Find_In_Set(Sm.GetLue(LueSiteCode), mPPNSiteCodeForSalesMemo) && !PPNChecked)
                {
                    DocNo = GenerateDocNo3(Sm.GetDte(DteDocDt), "SalesMemo", "TblSalesMemoHdr");
                }
                else
                {
                    DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "SalesMemo", "TblSalesMemoHdr");
                }

            }
            else
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesMemo", "TblSalesMemoHdr");
            }

            if (TxtLocalDocNo.Text.Length == 0) GenerateLocalDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesMemoHdr(DocNo, TxtLocalDocNo.Text));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0)
                {
                    cml.Add(SaveSalesMemoDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);
            
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LueCurCode, "Currency")||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 7, true, "Quantity should be bigger than 0.00.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, true, "Price should be bigger than 0.00.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 11, false, "Delivery date is empty.") ||
                    IsDeliveryDtNotValid(DocDt, Row) 
                    ) return true;
            }
            return false;
        }

        private bool IsDeliveryDtNotValid(string DocDt, int Row)
        {
            var  DeliveryDt = Sm.GetGrdDate(Grd1, Row, 11);
            if (Sm.CompareDtTm(DeliveryDt, DocDt) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Document Date : " + Sm.FormatDate2("dd/MMM/yyyy", DocDt) + Environment.NewLine +
                    "Delivery Date : " + Sm.FormatDate2("dd/MMM/yyyy", DeliveryDt) + Environment.NewLine + Environment.NewLine +
                    "Delivery date is earlier than document date.");
                Sm.FocusGrd(Grd1, Row, 11);
                return true;
            }
            return false;
        }
       
        private MySqlCommand SaveSalesMemoHdr(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesMemoHdr(DocNo, Status, CancelInd, CancelReason, DocDt, LocalDocNo, ");
            SQL.AppendLine("CtCode, SiteCode, SPCode, CurCode, Amt, TotalAmt, TaxInd, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, 'O', 'N', @CancelReason, @DocDt, @LocalDocNo, ");
            SQL.AppendLine("@CtCode, @SiteCode, @SPCode, @CurCode, @Amt, @TotalAmt, @TaxInd, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='SalesMemo' ");
            SQL.AppendLine("And T.SiteCode In ( ");
            SQL.AppendLine("    Select SiteCode From TblSalesMemoHdr Where DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblSalesMemoHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SalesMemo' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@TaxInd", ChkTaxInd.Checked ? "Y" : "N");

            return cm;
        }

        private MySqlCommand SaveSalesMemoDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesMemoDtl(DocNo, DNo, ItCode, ProductName, Qty, UPrice, UPriceAfterTax, DeliveryDt, ProcessInd, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @ProductName, @Qty, @UPrice, @UPriceAfterTax, @DeliveryDt, 'O',@Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProductName", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        
        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesMemoHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Sales Memo#", false) ||
                IsDocumentAlreadyCancel() ||
                IsDocumentAlreadyProceedToDRSC();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblSalesMemoHdr Where DocNo=@DocNo And CancelInd='Y'"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

        private bool IsDocumentAlreadyProceedToDRSC()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT 1 ");
            SQL.AppendLine("FROM TblDRHDr A ");
            SQL.AppendLine("INNER JOIN TblDRDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblSalesMemoDtl C ON B.SODocNo = C.DocNo AND B.SODNo = C.DNo And C.DocNo=@Param ");
            SQL.AppendLine("WHERE A.DocType = '3' ");
            SQL.AppendLine("AND A.CancelInd = 'N'; ");

            return Sm.IsDataExist(
                SQL.ToString(),
                TxtDocNo.Text,
                "This document already proceed to DRSC."
                );
        }

        private MySqlCommand EditSalesMemoHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesMemoHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason,");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSalesMemoHdr(DocNo);
                ShowSalesMemoDtl(DocNo);
                ComputeAmt();
                ComputeTotalAmtAfterTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesMemoHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, Cancelind, LocalDocNo,");
            SQL.AppendLine("CtCode, SiteCode, SPCode, CurCode, Amt, TotalAmt, TaxInd, Remark ");
            SQL.AppendLine("From TblSalesMemoHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "LocalDocNo", "CtCode", 
                        
                        //6-10
                        "SiteCode", "SPCode", "CurCode", "Amt", "Remark",

                        //11-12
                        "TaxInd", "TotalAmt"
 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                        ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[11]), "Y");
                    }, true
                );
        }


        private void ShowSalesMemoDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, A.ProductName, B.ItCodeInternal, B.ItName, A.Qty, B.SalesUomCode,  ");
            SQL.AppendLine("A.DeliveryDt, A.UPrice, (A.Qty*A.UPrice) As Total, A.UPriceAfterTax, (A.Qty*A.UPriceAfterTax) As TotalPrice,  A.Remark ");
            SQL.AppendLine("From TblSalesMemoDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "ItCode", "ItCodeInternal", "ItName", "Qty", "SalesUomCode", 
                    
                    //6-10
                    "UPrice", "Total", "DeliveryDt", "Remark", "ProductName", 

                    //11-12
                    "UPriceAfterTax", "TotalPrice"
 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 12);

                  
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 10, 13, 14  });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method
        private string GetSiteCode()
        {
            string mSiteCode = string.Empty;
            if (Sm.Find_In_Set(Sm.GetLue(LueSiteCode), mPPNSiteCodeForSalesMemo))
                mSiteCode = Sm.GetLue(LueSiteCode);
            return mSiteCode;
        }

        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),
            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select CtShortCode From TblCustomer WHERE CtCOde = @Param; ", Sm.GetLue(LueCtCode)),
            SiteShortCode = Sm.GetValue("Select ShortCode From TblSite Where SiteCode = @Param; ", Sm.GetLue(LueSiteCode)),
            DocTitle = Sm.GetParameter("DocTitle");

            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, LENGTH(CONCAT(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)  ");
            SQL.Append("       And (TaxInd = 'Y' Or (TaxInd = 'N' And SiteCode != '" + GetSiteCode() + "') Or (TaxInd = 'N' And localdocno not like '%KSM%')) ");
            SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private string GenerateDocNo3(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string mDocNo = string.Empty,
            SiteCode = LueSiteCode.Text,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),
            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select CtShortCode From TblCustomer WHERE CtCOde = @Param; ", Sm.GetLue(LueCtCode)),
            SiteShortCode = Sm.GetValue("Select ShortCode From TblSite Where SiteCode = @Param; ", Sm.GetLue(LueSiteCode)),
            DocTitle = Sm.GetParameter("DocTitle");
            
            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, LENGTH(CONCAT(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)  ");
            SQL.Append("       And TaxInd = 'N' And SiteCode = '" + GetSiteCode() + "' and docno not like '%KSM%' ");
            SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            SQL.Append(", '/'");
            SQL.Append(", IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private void GenerateLocalDocNo()
        {
            string LocalDocNo = string.Empty;
            bool PPNChecked = ChkTaxInd.Checked;
            var mDocnoLength = TxtDocNo.Text.Length;

            if (mIsDocNoFormatBasedOnSite)
            {
                if (Sm.Find_In_Set(Sm.GetLue(LueSiteCode), mPPNSiteCodeForSalesMemo) && !PPNChecked)
                {
                    if (mIsSalesMemoGenerateLocalDocNo)
                        LocalDocNo = GenerateLocalDocNo3(Sm.GetDte(DteDocDt), "SalesMemo", "TblSalesMemoHdr");
                    else
                        LocalDocNo = TxtLocalDocNo.Text;
                }
                else
                {
                    if (mIsSalesMemoGenerateLocalDocNo)
                        LocalDocNo = GenerateLocalDocNo2(Sm.GetDte(DteDocDt), "SalesMemo", "TblSalesMemoHdr");
                    else
                        LocalDocNo = TxtLocalDocNo.Text;
                }

            }
            else
            {
                LocalDocNo = TxtLocalDocNo.Text;
            }

            TxtLocalDocNo.Text = LocalDocNo;
        }

        private string GenerateLocalDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),
            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select CtShortCode From TblCustomer WHERE CtCOde = @Param; ", Sm.GetLue(LueCtCode)),
            SiteShortCode = Sm.GetValue("Select ShortCode From TblSite Where SiteCode = @Param; ", Sm.GetLue(LueSiteCode)),
            DocTitle = Sm.GetParameter("DocTitle");

            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(LocalDocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(LocalDocNo, 4), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Where Right(LocalDocNo, LENGTH(CONCAT(@Mth,'/', @Yr)))=Concat(@Mth,'/', @Yr) ");
            SQL.Append("       And Substring(LocalDocNo, 10, LENGTH(IfNull(@SiteShortCode, '')))=IfNull(@SiteShortCode, '') ");
            SQL.Append("       And (TaxInd = 'Y' Or (TaxInd = 'N' And SiteCode != '" + GetSiteCode() + "')) ");
            SQL.Append("       Order By Left(LocalDocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As LocalDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LocalDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private string GenerateLocalDocNo3(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string mDocNo = string.Empty,
            SiteCode = LueSiteCode.Text,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),
            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select CtShortCode From TblCustomer WHERE CtCOde = @Param; ", Sm.GetLue(LueCtCode)),
            SiteShortCode = Sm.GetValue("Select ShortCode From TblSite Where SiteCode = @Param; ", Sm.GetLue(LueSiteCode)),
            DocTitle = Sm.GetParameter("DocTitle");

            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(LocalDocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(LocalDocNo, 4), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Where Right(LocalDocNo, LENGTH(CONCAT(@Mth,'/', @Yr)))=Concat(@Mth,'/', @Yr) ");
            SQL.Append("       And Substring(LocalDocNo, 6, LENGTH(IfNull(@SiteShortCode, '')))=IfNull(@SiteShortCode, '') ");
            SQL.Append("       And TaxInd = 'N' And SiteCode = '" + GetSiteCode() + "' ");
            SQL.Append("       Order By Left(LocalDocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            SQL.Append(", '/'");
            SQL.Append(", IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As LocalDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LocalDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }


        internal string ComputeDeliveryTime(int Row)
        {
            DateTime DocDt = Convert.ToDateTime(DteDocDt.Text);
            DateTime DeliveryDt = Convert.ToDateTime(Sm.GetGrdStr(Grd1, Row, 11));
            TimeSpan diff = DeliveryDt - DocDt;
            int days = (int)Math.Ceiling(diff.TotalDays);

            return (days == 1 || days == 0) ? days.ToString() + " Day" : days.ToString() + " Days";
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m, UPriceAfterTAx =0m;

            if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 7);
            if (Sm.GetGrdStr(Grd1, Row, 9).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 9);
            if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0) UPriceAfterTAx = Sm.GetGrdDec(Grd1, Row, 13);
            Grd1.Cells[Row, 10].Value = Qty * UPrice;
            Grd1.Cells[Row, 14].Value = Qty * UPriceAfterTAx;

        }

        internal void NumValueZero(int Row)
        {
            Grd1.Cells[Row, 13].Value = 0m;
            Grd1.Cells[Row, 14].Value = 0m;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtTotalAmt
            }, 0);
        }

        internal void ComputeAmt()
        {
            decimal TotalAmt =  0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
                    TotalAmt += Sm.GetGrdDec(Grd1, Row, 10);

            TxtAmt.Text = Sm.FormatNum(TotalAmt, 0);
        }

        internal void ComputeTotalAmtAfterTax()
        {
            decimal TotalAmtAfterTax = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 14).Length != 0)
                    TotalAmtAfterTax += Sm.GetGrdDec(Grd1, Row, 14);

            TxtTotalAmt.Text = Sm.FormatNum(TotalAmtAfterTax, 0);
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT A.CtCode As Col1, ");
            if (mIsCustomerComboShowCategory)
            {
                SQL.AppendLine("CONCAT(A.CtName,' [',IFNULL(B.CtCtName,' '),']') As Col2 ");
            }
            else
            {
                SQL.AppendLine("A.CtName As Col2 ");                
            }
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("LEFT JOIN tblcustomercategory B ON A.CtCtCode = B.CtCtCode ");
            
            if (CtCode.Length <= 0)
                SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By CtName; ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mSpinningSiteCode = Sm.GetParameter("SpinningSiteCode");
            mWeavingSiteCode = Sm.GetParameter("WeavingSiteCode");
            mIsDocNoFormatBasedOnSite = Sm.GetParameterBoo("IsDocNoFormatBasedOnSite");
            mPPNSiteCodeForSalesMemo = Sm.GetParameter("PPNSiteCodeForSalesMemo");
            mIsSalesMemoGenerateLocalDocNo = Sm.GetParameterBoo("IsSalesMemoGenerateLocalDocNo");
            mIsCustomerComboShowCategory = Sm.GetParameterBoo("IsCustomerComboShowCategory");
            
        }

        private void ParPrint()
        {
            var l = new List<SalesMemoHdr>();
            var ldtl = new List<SalesMemoDtl>();
            var lsign = new List<SignatureKSM>();
            string[] TableName = { "SalesMemoHdr", "SalesMemoDtl", "SignatureKSM" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;

            #region Header
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            SQL.AppendLine("Select (Select  ParValue From TblParameter Where Parcode='SalesMemoPrintOutSignName') As 'SalesMemoPrintOutSignName',  ");
            SQL.AppendLine("B.SPName, A.CreateBy, ");
            SQL.AppendLine("A.LocalDocNo, TaxInd IsInclTax ");
            SQL.AppendLine("FROM TblSalesMemoHdr A ");
            SQL.AppendLine("Left Join TblSalesPerson B On A.SPCode = B.SPCode ");
            SQL.AppendLine("WHERE A.Docno=@DocNo ");
           
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "LocalDocNo",

                    //1-4
                    "SPName",
                    "SalesMemoPrintOutSignName",
                    "CreateBy",
                    "IsInclTax"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SalesMemoHdr()
                        {
                            DocNo = TxtLocalDocNo.Text,
                            DocDt = DteDocDt.Text, 
                            Customer = LueCtCode.Text,
                            SPName = Sm.DrStr(dr, c[1]),
                            SalesMemoPrintOutSignName = Sm.DrStr(dr, c[2]),
                            CreateBy = Sm.DrStr(dr, c[3]),
                            IsInclTax = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    if (ChkTaxInd.Checked)
                    {
                        ldtl.Add(new SalesMemoDtl()
                        {
                            Number = Nomor,

                            JenisBarang = Sm.GetGrdStr(Grd1, i, 3),
                            NamaBarang = Sm.GetGrdStr(Grd1, i, 5),
                            ProductName = Sm.GetGrdStr(Grd1, i, 6),
                            Qty = Sm.GetGrdDec(Grd1, i, 7),
                            Price = Sm.GetGrdDec(Grd1, i, 13),

                            Remark = Sm.GetGrdStr(Grd1, i, 12),
                            Delivery = ComputeDeliveryTime(i),
                            ItLocalCode = Sm.GetGrdStr(Grd1, i, 3),
                            UOM = Sm.GetGrdStr(Grd1, i, 8)
                        });
                        Nomor += 1;
                    }
                    else
                    {
                        ldtl.Add(new SalesMemoDtl()
                        {
                            Number = Nomor,

                            JenisBarang = Sm.GetGrdStr(Grd1, i, 3),
                            NamaBarang = Sm.GetGrdStr(Grd1, i, 5),
                            ProductName = Sm.GetGrdStr(Grd1, i, 6),
                            Qty = Sm.GetGrdDec(Grd1, i, 7),
                            Price = Sm.GetGrdDec(Grd1, i, 9),

                            Remark = Sm.GetGrdStr(Grd1, i, 12),
                            Delivery = ComputeDeliveryTime(i),
                            ItLocalCode = Sm.GetGrdStr(Grd1, i, 3),
                            UOM = Sm.GetGrdStr(Grd1, i, 8)
                        });
                        Nomor += 1;
                    }

                }
            }
            myLists.Add(ldtl);

            #endregion

            #region SignatureKSM
            bool PPNChecked = ChkTaxInd.Checked;

            var SQL2 = new StringBuilder();
            if (PPNChecked)
                SQL2.AppendLine("SELECT T1.EmpPict1, T1.Description1, T1.UserName1, T1.ApproveDt1, T2.EmpPict2, T2.Description2, T2.UserName2, T2.ApproveDt2, T3.EmpPict3, T3.Description3, T3.Username3, T3.ApproveDt3 ");
            else
                SQL2.AppendLine("SELECT T1.EmpPict1, T1.Description1, T1.UserName1, T1.ApproveDt1, '' EmpPict2, T2.Description2, T2.UserName2, T2.ApproveDt2, '' EmpPict3, T3.Description3, T3.Username3, T3.ApproveDt3 ");

            SQL2.AppendLine("FROM ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("    SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict1, ");
            SQL2.AppendLine("    'Dibuat Oleh' AS Description1, C.UserName AS UserName1, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt1 ");
            SQL2.AppendLine("    FROM TblDocApproval A ");
            SQL2.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQL2.AppendLine("        AND A.docno = @DocNo ");
            SQL2.AppendLine("        AND B.DNo = A.ApprovalDNo ");
            SQL2.AppendLine("        And B.Level = 1 ");
            SQL2.AppendLine("        AND A.Status = 'A' ");
            SQL2.AppendLine("    INNER JOIN TblUser C ON A.CreateBy = C.UserCode ");
            SQL2.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine(") T1 ");
            SQL2.AppendLine("INNER JOIN ");
            SQL2.AppendLine("( ");
	        SQL2.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict2, 'Diterima Oleh' AS Description2, C.UserName AS UserName2, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt2 ");
	        SQL2.AppendLine("     FROM TblDocApproval A ");
	        SQL2.AppendLine("     INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
	        SQL2.AppendLine("         AND A.docno = @DocNo ");
	        SQL2.AppendLine("         AND B.DNo = A.ApprovalDNo ");
	        SQL2.AppendLine("         And B.Level = 2 ");
	        SQL2.AppendLine("         AND A.Status = 'A' ");
	        SQL2.AppendLine("     INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
	        SQL2.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
            SQL2.AppendLine("INNER JOIN ");
            SQL2.AppendLine("( ");
	        SQL2.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict3, ");
            SQL2.AppendLine("     'Mengetahui' AS Description3, C.UserName AS UserName3, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt3 ");
            SQL2.AppendLine("    FROM TblDocApproval A ");
            SQL2.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQL2.AppendLine("        AND A.DocNo = @DocNo ");
            SQL2.AppendLine("        AND B.DNo = A.ApprovalDNo ");
            SQL2.AppendLine("        And B.Level = 3 ");
            SQL2.AppendLine("        AND A.Status = 'A' ");
            SQL2.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
            SQL2.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine(")T3 ON T1.DocNo = T3.DocNo ");


            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm.Connection = cn2;
                cm.CommandText = SQL2.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpPict1",
                    "Description1",
                    "Username1",
                    "ApproveDt1",

                    "EmpPict2",
                    "Description2",
                    "Username2",
                    "ApproveDt2",

                    "EmpPict3",
                    "Description3",
                    "Username3",
                    "ApproveDt3",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lsign.Add(new SignatureKSM()
                        {
                            EmpPict1    = Sm.DrStr(dr, c[0]),
                            Description1= Sm.DrStr(dr, c[1]),
                            UserName1   = Sm.DrStr(dr, c[2]),
                            ApproveDt1  = Sm.DrStr(dr, c[3]),
                            EmpPict2    = Sm.DrStr(dr, c[4]),
                            Description2= Sm.DrStr(dr, c[5]),
                            UserName2   = Sm.DrStr(dr, c[6]),
                            ApproveDt2  = Sm.DrStr(dr, c[7]),
                            EmpPict3    = Sm.DrStr(dr, c[8]),
                            Description3= Sm.DrStr(dr, c[9]),
                            UserName3   = Sm.DrStr(dr, c[10]),
                            ApproveDt3  = Sm.DrStr(dr, c[11]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(lsign);

            #endregion

            if (Sm.GetLue(LueSiteCode) == mSpinningSiteCode)
                Sm.PrintReport("SalesMemo_Spinning", myLists, TableName, false);
            if (Sm.GetLue(LueSiteCode) == mWeavingSiteCode)
                Sm.PrintReport("SalesMemo_Weaving", myLists, TableName, false);
            
            
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event


        private void DteDeliveryDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeliveryDt, ref fCell, ref fAccept);
        }

        private void DteDeliveryDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }
        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            if (Sm.Find_In_Set(Sm.GetLue(LueSiteCode), mPPNSiteCodeForSalesMemo))
            {
                ChkTaxInd.Enabled = true;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkTaxInd  
                    }, false);
            }
            else
            {
                ChkTaxInd.Checked = false;
                ChkTaxInd.Enabled = false;
            }
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && !mIsClearData) GenerateLocalDocNo();
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && !mIsClearData) GenerateLocalDocNo();
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(Sl.SetLueSPCode));

        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void ChkTaxInd_CheckedChanged(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && !mIsClearData) GenerateLocalDocNo();
            if (ChkTaxInd.Checked)
                Sm.GrdColReadOnly(false, true, Grd1, new int[] { 13 });
            else
            {
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 13 });
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    NumValueZero(Row);
            }

        }

        #endregion

        #endregion

        #region Class
        private class SalesMemoHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Customer { get; set; }
            public string SPName { get; set; }
            public string SalesMemoPrintOutSignName { get; set; }
            public string CreateBy { get; set; }
            public string IsInclTax { get; set; }

        }

        private class SignatureKSM
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }
            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }
        }

        private class SalesMemoDtl
        {
            public decimal Number { get; set; }
            public string JenisBarang { get; set; }
            public string NamaBarang { get; set; }
            public decimal Qty { get; set; }
            public decimal Price { get; set; }
            public string Remark { get; set; }
            public string Delivery { get; set; }
            public string ProductName { get; set; }
            public string ItLocalCode { get; set; }
            public string UOM { get; set; }
        }
        #endregion 

    }
}
