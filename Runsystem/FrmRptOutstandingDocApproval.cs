﻿#region Update

#region Old
/*
    22/09/2017 [TKG] Bug fixing dokumen yg sudah dicancel tetap muncul.
    10/10/2017 [HAR] approval baru travel request
    27/11/2017 [WED] approval Annual Leave Allowance
    28/11/2017 [HAR] approval employee severance
    08/12/2017 [HAR] approval employee reward
    04/01/2018 [TKG] approval SCI Deduction
    15/02/2018 [WED] approval Return ARDownpayment
    29/05/2018 [TKG] approval PO
    06/06/2018 [WED] approval PO nggak pakai cancelInd
    24/07/2018 [WED] approval Request for Loan to Partner
    25/07/2018 [WED] approval Survey
    06/08/2018 [WED] approval Partner's Loan Payment
    10/08/2018 [WED] approval DO To Customer
    14/08/2018 [WED] approval BOQ. di SetSubQueryH di tambah if DocType == BOQ, untuk ngecek ActInd nya
    14/08/2018 [WED] approval LOP
    21/08/2018 [WED] approval Project Implementation
    28/08/2018 [HAR] approval Training Request
    06/09/2018 [WED] approval Bonus
    12/11/2018 [WED/YK] approval SO Contract Revision
    11/01/2019 [HAR/YK] approval Project Implementation validasi hanya yang proses indicator F (berdasarkan doc approval)
    09/08/2018 [WED/YK] approval Dropping request
    09/08/2019 [DITA/YK] approval Budget Request
    09/08/2019 [DITA/YK] approval Budget
    09/09/2019 [WED/YK] approval APDownpayment
    23/09/2019 [DITA/IMS] approval Notice To Proceed
    24/09/2019 [WED/YK] approval Dropping Payment
    03/10/2019 [WED/YK] approval Voucher Request External Payroll
    04/10/2019 [TKG/TWC] approval Material Request Routine
    15/10/2019 [TKG/YK] approval Cash Advance Settlement
    21/10/2019 [WED/TWC] param berdasarkan SplitView
    23/10/2019 [WED/YK] approval Project implementation Revision
    23/12/2019 [WED/YK] approval Project Delivery
    06/01/2020 [TKG/SIER] approval Asset Transfer
    15/01/2020 [WED/SIER] approval Asset Transfer nggak pakai CancelInd
    15/01/2020 [WED/SIER] approval Stock Inbound
    15/01/2020 [WED/SIER] approval Stock Outbound
    28/04/2020 [IBL/KSM] approval Sales Memo
    09/06/2020 [HAR/ALL] tambah info department site dan budget category khusus untuk approval voucher request
    29/06/2020 [DITA/IMS] tambah approval Drawing Approval
    23/07/2020 [TKG/IMS] tambah approval VR Payroll
    02/12/2020 [DITA/IMS] tambah approval Project Budget Resource
    07/12/2020 [WED/IMS] tambah approval BOM3
    30/09/2021 [BRI/ALL] tambah approval Journal Memorial
    08/10/2021 [ICA/PHT] tambah approval Company Budget Plan Profit Loss Revision
    10/11/2021 [TYO/AMKA] filter approval DocType = ApDownPayment || DocType = VoucherRequestCashAdvance by group
    12/11/2021 [ARI/AMKA] filter approval DocType = ReturnARDownPayment by group
    15/11/2021 [RIS/AMKA] filter approval DocType = Outgoing Payment & Voucher Request Switching Bank Account by group department
    15/11/2021 [ISD/AMKA] filter approval DocType = Cash Advance Settlement by group based on param IsRptOutstandingDocApprovalDeptFilteredByGroup
    16/11/2021 [DEV/AMKA] filter approval DocType = Journal Memorial by group based on param IsRptOutstandingDocApprovalDeptFilteredByGroup
    17/11/2021 [YOG/AMKA] filter approval DocType = Voucher Request Manual by group based on param IsRptOutstandingDocApprovalDeptFilteredByGroup
    17/11/2021 [BRI/ALL] memunculkan departemen dari masing-masing transaksi
    15/12/2021 [VIN/ALL] tambah approval type MaterialRequestSPPJB
    21/12/2021 [MYA/ALL] Adanya Approval setelah mengisi data pada menu KPI
    23/12/2021 [YOG/AMKA] Membuat Department di Journal Memorial tercapture di menu Monitoring Outstanding Journal Memorial
    12/05/2022 [TYO/HIN] adanya approval setelah mengisi data pada menu Goals Setting
    23/05/2022 [MYA/HIN] Menambahkan type document approval menu baru untuk approval menu  Performance Review
    03/06/2022 [TKG/PHT] Menambahkan type document approval vr casba+switching cost center
    27/07/2022 [IBL/SIER] Menambahkan type document approval purchase invoice
    26/10/2022 [MAU/PRODUCT] Menambahkan type document approval vr petty cash
    07/11/2022 [HPH/PRODUCT] BUG: Muncul warning pada Monitoring Outstanding Document Approval
    08/11/2022 [IBL/PRODUCT] Menambahkan type document approval petty cash disbursement
    07/12/2022 [WED/HIN] tambah type GoalProcess
    29/12/2022 [ICA/MNET] tambah type Net Off Payment
    29/12/2022 [DITA/BBT] tambah type Property Inventory
 */
#endregion

/*
    03/01/2023 [DITA/BBT] tambah type Property Inventory Cost Component
    09/01/2023 [IBL/BBT] tambah type Property Inventory Mutation
    10/01/2023 [SET/BBT] tambah type Property Inventory Transfer
    26/01/2023 [MAU/BBT] menambah kolom User's Name dan Setting Level , rename kolom User menjadi User's Code
    09/02/2023 [IBL/BBT] tambah type Yearly Closing Journal Entry
    15/02/2023 [WED/HEX] tambah type Budget Transfer, Service Delivery
    17/02/2023 [SET/HEX] tambah type Approval Sheet
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmRptOutstandingDocApproval : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mDocTitle = string.Empty;
        private bool mIsApprovalByDeptEnabled = false, mIsMREximSplitDocument = false, mIsRptOutstandingDocApprovalDeptFilteredByGroup = false;
        #endregion

        #region Constructor

        public FrmRptOutstandingDocApproval(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                mIsApprovalByDeptEnabled = IsApprovalByDeptEnabled();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteCreateDt, ref DteCreateDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsApprovalByDeptEnabled()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='MaterialRequest2' And DeptCode Is Not Null Limit 1; ");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocType, Replace(Trim(Both '#' From B.UserCode),'#',', ')As UserCode, C.UserName, B.`Level`, ");
            SQL.AppendLine("A.DocNo, Tbl.DeptName, Tbl.SiteName, Tbl.BCName, A.CreateBy, Left(A.CreateDt, 8) As CreateDt  ");
            SQL.AppendLine("From TblDocApproval A  ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On A.DocType=B.DocType And A.ApprovalDNo=B.DNo ");
            SQL.AppendLine("LEFT JOIN tbluser C ON Replace(Trim(Both '#' From B.UserCode),'#',', ') = C.UserCode ");
            SQL.AppendLine("Left Join ( ");
            SetAdditonalInfo(ref SQL2, "VoucherRequest", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestCashAdvance", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestManual", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestSwitchingBankAcc", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestSwitchingCostCenter", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestCASBA", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "APDownpayment", "TblAPDownpayment");
            SetAdditonalInfo(ref SQL2, "ARDownpayment", "TblARDownpayment");
            SetAdditonalInfo(ref SQL2, "BOM3", "TblBOM2Hdr");
            SetAdditonalInfo(ref SQL2, "BudgetRequest2", "TblBudgetRequestHdr");
            SetAdditonalInfo(ref SQL2, "DroppingRequest", "TblDroppingRequestHdr");
            SetAdditonalInfo(ref SQL2, "EmpLeave2", "TblEmpLeave2Hdr");
            SetAdditonalInfo(ref SQL2, "EmployeeRequest", "TblEmployeeRequest");
            SetAdditonalInfo(ref SQL2, "EmpWS", "TblEmpWSHdr");
            SetAdditonalInfo(ref SQL2, "MaterialRequest2", "TblMaterialRequestHdr");
            SetAdditonalInfo(ref SQL2, "OTRequest", "TblOTRequestHdr");
            SetAdditonalInfo(ref SQL2, "OutgoingPayment", "TblOutgoingPaymentHdr");
            SetAdditonalInfo(ref SQL2, "OutgoingPaymentNoInvoice", "TblOutgoingPaymentHdr");
            SetAdditonalInfo(ref SQL2, "PO", "TblPOHdr");
            SetAdditonalInfo(ref SQL2, "PORevision", "TblPORevision");
            SetAdditonalInfo(ref SQL2, "ReturnAPDownpayment", "TblReturnAPDownpayment");
            SetAdditonalInfo(ref SQL2, "ReturnARDownpayment", "TblReturnARDownpayment");
            SetAdditonalInfo(ref SQL2, "RHA", "TblRHAHdr");
            SetAdditonalInfo(ref SQL2, "RLP", "TblRLPHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestExternalPayroll", "TblVoucherRequestExternalPayrollHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestTax", "TblVoucherRequestTax");
            SetAdditonalInfo(ref SQL2, "VoucherRequestSS", "TblVoucherRequestSSHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestPPN", "TblVoucherRequestPPNHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestPayroll", "TblVoucherRequestPayrollHdr");
            SetAdditonalInfo(ref SQL2, "CashAdvanceSettlement", "TblCashAdvanceSettlementHdr");
            SetAdditonalInfo(ref SQL2, "JournalMemorial", "TblJournalMemorialHdr");
            SetAdditonalInfo(ref SQL2, "PurchaseInvoice", "TblPurchaseInvoiceHdr");
            SetAdditonalInfo(ref SQL2, "VoucherRequestPettyCash", "TblVoucherRequestHdr");
            SetAdditonalInfo(ref SQL2, "PettyCashDisbursement", "TblPettyCashDisbursementHdr");
            SetAdditonalInfo(ref SQL2, "NetOffPayment", "TblNetOffPaymentHdr");
            SetAdditonalInfo(ref SQL2, "PropertyInventory", "TblPropertyInventoryHdr");
            SetAdditonalInfo(ref SQL2, "PropertyInventoryCostComponent", "TblPropertyInventoryCostComponentHdr");
            SetAdditonalInfo(ref SQL2, "PropertyInventoryMutation", "TblPropertyInventoryMutationHdr");
            SetAdditonalInfo(ref SQL2, "PropertyInventoryTransfer", "TblPropertyInventoryTransferHdr");
            SetAdditonalInfo(ref SQL2, "YearlyClosingJournalEntry", "TblYearlyClosingJournalEntryHdr");
            SetAdditonalInfo(ref SQL2, "ApprovalSheet", "TblApprovalSheetHdr");
            if (Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmServiceDelivery'")) SetAdditonalInfo(ref SQL2, "ServiceDelivery", "TblServiceDeliveryHdr");
            if (Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmBudgetTransfer'")) SetAdditonalInfo(ref SQL2, "BudgetTransfer", "TblBudgetTransferHdr");
            if (Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmGoalsProcess'")) SetAdditonalInfo(ref SQL2, "GoalsProcess", "TblGoalsProcessHdr");
            SQL.AppendLine(SQL2.ToString());
            SQL.AppendLine(") Tbl on A.DocNo=Tbl.DocNo ");
            SQL.AppendLine("Where A.UserCode Is Null ");
            SQL.AppendLine("And Left(A.CreateDt, 8) Between @CreateDt And @CreateDt2 ");

            SetSubQueryH(ref SQL, "AdvancePayment", "TblAdvancePaymentHdr", true);
            SetSubQueryH(ref SQL, "AdvancePaymentRevision", "TblAdvancePaymentRevisionHdr", false);
            SetSubQueryH(ref SQL, "AnnualLeaveAllowance", "TblAnnualLeaveAllowanceHdr", true);
            SetSubQueryH(ref SQL, "APDownpayment", "TblAPDownpayment", true);
            SetSubQueryH(ref SQL, "ARDownpayment", "TblARDownpayment", true);
            SetSubQueryH(ref SQL, "AssetTransfer", "TblAssetTransfer", false);
            SetSubQueryH(ref SQL, "Bonus", "TblBonusHdr", true);
            SetSubQueryH(ref SQL, "BOM3", "TblBOM2Hdr", true);
            SetSubQueryH(ref SQL, "BOQ", "TblBOQHdr", false);
            SetSubQueryH(ref SQL, "Budget2", "TblBudgetHdr", false);
            SetSubQueryH(ref SQL, "BudgetRequest2", "TblBudgetRequestHdr", true);
            SetSubQueryH(ref SQL, "CashAdvanceSettlement", "TblCashAdvanceSettlementHdr", true);
            SetSubQueryH(ref SQL, "CompanyBudgetPlanProfitLossRev", "TblCompanyBudgetPlanHdr", true);
            SetSubQueryH(ref SQL, "CtQt", "TblCtQtHdr", false);
            SetSubQueryH(ref SQL, "DOCt", "TblDOCtHdr", false);
            SetSubQueryD(ref SQL, "DORequestDept", "TblDORequestDeptDtl", true);
            SetSubQueryD(ref SQL, "DORequestDeptWO", "TblDORequestDeptDtl", true);
            SetSubQueryH(ref SQL, "DOWhs", "TblDOWhsHdr", true);
            SetSubQueryH(ref SQL, "DOWhs2", "TblDOWhsHdr", true);
            SetSubQueryH(ref SQL, "DroppingPayment", "TblDroppingPaymentHdr", true);
            SetSubQueryH(ref SQL, "DroppingRequest", "TblDroppingRequestHdr", true);
            SetSubQueryH(ref SQL, "EmpClaim", "TblEmpClaimHdr", true);
            SetSubQueryH(ref SQL, "EmpLeave", "TblEmpLeaveHdr", true);
            SetSubQueryH(ref SQL, "EmpLeave2", "TblEmpLeave2Hdr", true);
            SetSubQueryH(ref SQL, "EmployeeRequest", "TblEmployeeRequest", true);
            SetSubQueryH(ref SQL, "EmpReward", "TblEmpReward", true);
            SetSubQueryH(ref SQL, "EmpSalary", "TblEmpSalaryHdr", false);
            SetSubQueryH(ref SQL, "EmpSeverance", "TblEmpSeverance", true);
            SetSubQueryH(ref SQL, "EmpWS", "TblEmpWSHdr", false);
            SetSubQueryH(ref SQL, "IncomingPayment", "TblIncomingPaymentHdr", true);
            SetSubQueryH(ref SQL, "JournalMemorial", "TblJournalMemorialHdr", true);
            SetSubQueryH(ref SQL, "LOP", "TblLOPHdr", true);
            SetSubQueryH(ref SQL, "MakeToStock", "TblMakeToStockHdr", true);
            SetSubQueryD(ref SQL, "MaterialRequest", "TblMaterialRequestDtl", true);
            SetSubQueryD(ref SQL, "MaterialRequestSPPJB", "TblMaterialRequestDtl", true);
            //if (mIsApprovalByDeptEnabled)
            if (mIsMREximSplitDocument)
                SetSubQueryD(ref SQL, "MaterialRequest2", "TblMaterialRequestDtl", true);
            else
                SetSubQueryH(ref SQL, "MaterialRequest2", "TblMaterialRequestHdr", true);
            SetSubQueryD(ref SQL, "MaterialRequestWO", "TblMaterialRequestDtl", true);
            SetSubQueryH(ref SQL, "NTP", "TblNoticeToProceed", true);
            SetSubQueryH(ref SQL, "DrawingApproval", "TblDrawingApprovalHdr", true);
            SetSubQueryH(ref SQL, "OTAdjustment", "TblOTAdjustment", true);
            SetSubQueryH(ref SQL, "OTRequest", "TblOTRequestHdr", true);
            SetSubQueryH(ref SQL, "OutgoingPayment", "TblOutgoingPaymentHdr", true);
            SetSubQueryH(ref SQL, "OutgoingPaymentNoInvoice", "TblOutgoingPaymentHdr", true);
            SetSubQueryH(ref SQL, "PLP", "TblPLPHdr", true);
            SetSubQueryH(ref SQL, "PO", "TblPOHdr", false);
            SetSubQueryD(ref SQL, "PORequest", "TblPORequestDtl", true);
            SetSubQueryH(ref SQL, "PORevision", "TblPORevision", true);
            SetSubQueryH(ref SQL, "PPS", "TblPPS", true);
            SetSubQueryH(ref SQL, "ProductionOrderRevision", "TblProductionOrderRevision", false);
            SetSubQueryH(ref SQL, "ProjectBudgetResource", "TblProjectBudgetResourceHdr", true);
            SetSubQueryH(ref SQL, "ProjectImplementation", "TblProjectImplementationHdr", true);
            SetSubQueryH(ref SQL, "ProjectImplementationRev", "TblProjectImplementationRevisionHdr", false);
            SetSubQueryH(ref SQL, "ProjectDelivery", "TblProjectDeliveryHdr", true);
            SetSubQueryH(ref SQL, "Qt", "TblQtHdr", true);
            SetSubQueryD(ref SQL, "RecvVd", "TblRecvVdDtl", true);
            SetSubQueryD(ref SQL, "RecvVd2", "TblRecvVdDtl", true);
            SetSubQueryD(ref SQL, "RecvWhs2", "TblRecvWhs2Dtl", true);
            SetSubQueryH(ref SQL, "RequestLP", "TblRequestLP", true);
            SetSubQueryH(ref SQL, "ReturnAPDownpayment", "TblReturnAPDownpayment", true);
            SetSubQueryH(ref SQL, "ReturnARDownpayment", "TblReturnARDownpayment", true);
            SetSubQueryH(ref SQL, "RHA", "TblRHAHdr", true);
            SetSubQueryH(ref SQL, "RLP", "TblRLPHdr", true);
            SetSubQueryH(ref SQL, "SalesMemo", "TblSalesMemoHdr", true);
            SetSubQueryH(ref SQL, "SCIDeduction", "TblSCIDeductionHdr", true);
            SetSubQueryH(ref SQL, "SOCRev", "TblSOContractRevisionHdr", false);
            SetSubQueryH(ref SQL, "StockInbound", "TblStockInboundHdr", false);
            SetSubQueryH(ref SQL, "StockOutbound", "TblStockOutboundHdr", false);
            SetSubQueryH(ref SQL, "StockOpname", "TblStockOpnameHdr", false);
            SetSubQueryH(ref SQL, "Survey", "TblSurvey", true);
            SetSubQueryD(ref SQL, "TrainingRequest", "TblTrainingRequestDtl2", false);
            SetSubQueryH(ref SQL, "TransferRequestWhs", "TblTransferRequestWhsHdr", true);
            SetSubQueryH(ref SQL, "TravelRequest", "TblTravelRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequest", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestCashAdvance", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestExternalPayroll", "TblVoucherRequestExternalPayrollHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestManual", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestTax", "TblVoucherRequestTax", true);
            SetSubQueryH(ref SQL, "VoucherRequestSS", "TblVoucherRequestSSHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestSwitchingBankAcc", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestSwitchingCostCenter", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestCASBA", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestPPN", "TblVoucherRequestPPNHdr", true);
            SetSubQueryH(ref SQL, "VoucherRequestPayroll", "TblVoucherRequestPayrollHdr", true);
            SetSubQueryH(ref SQL, "WORequest", "TblWOR", true);
            SetSubQueryH(ref SQL, "KPI", "TblKPIHdr", false);
            SetSubQueryH(ref SQL, "GoalsSetting", "TblGoalsSettingHdr", false);
            SetSubQueryH(ref SQL, "PerformanceReview2", "TblNewPerformanceReviewHdr", true);
            SetSubQueryH(ref SQL, "PurchaseInvoice", "TblPurchaseInvoiceHdr", true);
            SetSubQueryH(ref SQL, "VourcherRequestPetyCash", "TblVoucherRequestHdr", true);
            SetSubQueryH(ref SQL, "PettyCashDisbursement", "TblPettyCashDisbursementHdr", true);
            SetSubQueryH(ref SQL, "NetOffPayment", "TblNetOffPaymentHdr", true);
            SetSubQueryH(ref SQL, "PropertyInventory", "TblPropertyInventoryHdr", true);
            SetSubQueryH(ref SQL, "PropertyInventoryCostComponent", "TblPropertyInventoryCostComponentHdr", true);
            SetSubQueryH(ref SQL, "PropertyInventoryMutation", "TblPropertyInventoryMutationHdr", true);
            SetSubQueryH(ref SQL, "PropertyInventoryTransfer", "TblPropertyInventoryTransferHdr", true);
            SetSubQueryH(ref SQL, "YearlyClosingJournalEntry", "TblYearlyClosingJournalEntryHdr", true);
            SetSubQueryH(ref SQL, "ApprovalSheet", "TblApprovalSheetHdr", true);
            if (Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmGoalsProcess'")) SetSubQueryH(ref SQL, "GoalsProcess", "TblGoalsProcessHdr", true);
            if (Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmServiceDelivery'")) SetSubQueryH(ref SQL, "ServiceDelivery", "TblServiceDeliveryHdr", false);
            if (Sm.IsDataExist("Select 1 From TblMenu Where Param = 'FrmBudgetTransfer'")) SetSubQueryH(ref SQL, "BudgetTransfer", "TblBudgetTransferHdr", true);

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",
                        "User's Code",
                        "Document#",
                        "Department",
                        "Site",
                        //6-10
                        "Budget Category",
                        "Created By",
                        "Created Date",
                        "User's Name",
                        "Setting Level"
                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        200, 200, 150, 150, 150, 
                        //6-10
                        150, 150, 120, 200, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[9].Move(4);
            Grd1.Cols[10].Move(5);


            if (mDocTitle != "PHT")
            {
                Grd1.Cols[9].Visible = false;
                Grd1.Cols[10].Visible = false;
            }
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteCreateDt, "Start date") ||
                Sm.IsDteEmpty(DteCreateDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteCreateDt, ref DteCreateDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@CreateDt", Sm.GetDte(DteCreateDt));
                Sm.CmParamDt(ref cm, "@CreateDt2", Sm.GetDte(DteCreateDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "DocType", 

                            //1-5
                            "UserCode", "DocNo",  "DeptName", "SiteName", "BCName", 
                            //6-7
                            "CreateBy", "CreateDt", "UserName", "Level"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsMREximSplitDocument = Sm.GetParameterBoo("IsMREximSplitDocument");
            mIsRptOutstandingDocApprovalDeptFilteredByGroup = Sm.GetParameterBoo("IsRptOutstandingDocApprovalDeptFilteredByGroup");
        }

        private void SetAdditonalInfo(ref StringBuilder SQL2, string DocType, string TableName)
        {
            if (DocType == "VoucherRequest" || DocType == "VoucherRequestCashAdvance" || DocType == "VoucherRequestManual" || 
                DocType == "VoucherRequestSwitchingBankAcc" ||
                DocType == "VoucherRequestSwitchingCostCenter" ||
                DocType == "VoucherRequestCASBA"
                )
            {
                if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                SQL2.AppendLine("Select T1.DocNo, T2.DeptName, T3.SiteName, T4.BCName From " + TableName + " T1  ");
                SQL2.AppendLine("Inner Join TblDepartment T2 On T1.DeptCode = T2.DeptCode");
                SQL2.AppendLine("Left Join TblSite T3 On T1.SiteCode = T3.SiteCode");
                SQL2.AppendLine("Left Join TblBudgetCategory T4 On T1.BCCode = T4.BCCode");
            }
            else
            {
                if (DocType == "APDownpayment" || DocType == "ARDownpayment" || DocType == "BOM3" || DocType == "BudgetRequest2" || DocType == "DroppingRequest" || DocType == "EmpLeave2" ||
                DocType == "EmployeeRequest" || DocType == "EmpWS" || DocType == "MaterialRequest2" || DocType == "OTRequest" || DocType == "OutgoingPayment" || DocType == "OutgoingPaymentNoInvoice" ||
                DocType == "PO" || DocType == "PORevision" || DocType == "ReturnAPDownpayment" || DocType == "ReturnARDownpayment" || DocType == "RHA" || DocType == "RLP" ||
                DocType == "VoucherRequestExternalPayroll" || DocType == "VoucherRequestTax" || DocType == "VoucherRequestSS" || DocType == "VoucherRequestPPN" || DocType == "VoucherRequestPayroll" || DocType == "CashAdvanceSettlement" ||
                DocType == "PurchaseInvoice" || DocType == "PettyCashDisbursement" || DocType == "NetOffPayment" || DocType == "ApprovalSheet"
                )
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T2.DeptName, Null As SiteName, Null As BcName From " + TableName + " T1  ");
                    SQL2.AppendLine("Inner Join TblDepartment T2 On T1.DeptCode = T2.DeptCode");

                }

                else if (DocType == "JournalMemorial")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T3.DeptName, Null As SiteName, Null As BcName From " + TableName + " T1  ");
                    SQL2.AppendLine("Left Join tblcostcenter T2 On T2.CCCode = T1.CCCode ");
                    SQL2.AppendLine("Inner Join TblDepartment T3 On T2.DeptCode = T3.DeptCode");
                }

                else if (DocType == "PropertyInventory")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.PropertyCode As DocNo, T3.DeptName, T4.SiteName, Null As BcName From " + TableName + " T1  ");
                    SQL2.AppendLine("Left Join tblcostcenter T2 On T2.CCCode = T1.CCCode ");
                    SQL2.AppendLine("Inner Join TblDepartment T3 On T2.DeptCode = T3.DeptCode");
                    SQL2.AppendLine("Inner Join TblSite T4 On T1.SiteCode = T4.SiteCode");
                }

                else if (DocType == "PropertyInventoryCostComponent")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T4.DeptName, T5.SiteName, Null As BcName From " + TableName + " T1  ");
                    SQL2.AppendLine("Inner Join TblPropertyInventoryHdr T2 On T1.PropertyCode = T2.PropertyCode ");
                    SQL2.AppendLine("Left Join tblcostcenter T3 On T2.CCCode = T3.CCCode ");
                    SQL2.AppendLine("Inner Join TblDepartment T4 On T3.DeptCode = T4.DeptCode");
                    SQL2.AppendLine("Inner Join TblSite T5 On T2.SiteCode = T5.SiteCode");
                }

                else if (DocType == "PropertyInventoryMutation")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T5.DeptName, T6.SiteName, Null As BcName From " + TableName + " T1  ");
                    SQL2.AppendLine("Inner Join TblPropertyInventoryMutationDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL2.AppendLine("Inner Join TblPropertyInventoryHdr T3 On T2.PropertyCode = T3.PropertyCode ");
                    SQL2.AppendLine("Inner Join tblcostcenter T4 On T3.CCCode = T4.CCCode ");
                    SQL2.AppendLine("Inner Join TblDepartment T5 On T4.DeptCode = T5.DeptCode");
                    SQL2.AppendLine("Inner Join TblSite T6 On T3.SiteCode = T6.SiteCode");
                }
                else if (DocType == "PropertyInventoryTransfer")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T5.DeptName, Null As SiteName, Null As BcName From TblPropertyInventoryTransferHdr T1 ");
                    SQL2.AppendLine("INNER JOIN tblpropertyinventorytransferdtl T2 ON T1.DocNo = T2.DocNo ");
                    SQL2.AppendLine("INNER JOIN tblpropertyinventoryhdr T3 ON T2.propertycode = T3.propertycode ");
                    SQL2.AppendLine("INNER JOIN tblcostcenter T4 ON T3.CCCode = T4.CCCode ");
                    SQL2.AppendLine("Inner Join TblDepartment T5 ON T4.DeptCode = T5.DeptCode ");
                }

                else if (DocType == "BudgetTransfer")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T2.DeptName, Null As SiteName, Null as BCName ");
                    SQL2.AppendLine("From TblBudgetTransferHdr T1 ");
                    SQL2.AppendLine("Inner Join TblDepartment T2 On T1.DeptCode = T2.DeptCode ");
                    SQL2.AppendLine("    And T1.Status = 'O' And T1.CancelInd = 'N' ");
                }

                else if (DocType == "ServiceDelivery")
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select T1.DocNo, T3.DeptName, Null As SiteName, Null as BCName ");
                    SQL2.AppendLine("From TblServiceDeliveryHdr T1 ");
                    SQL2.AppendLine("Inner Join TblCostCenter T2 On T1.CCCode = T2.CCCode ");
                    SQL2.AppendLine("    And T1.Status = 'O' ");
                    SQL2.AppendLine("Inner Join TblDepartment T3 On T2.DeptCode = T3.DeptCode ");
                }

                else 
                {
                    if (SQL2.Length > 0) SQL2.AppendLine("Union ");
                    SQL2.AppendLine("Select null as DocNo, null As DeptName, null As SiteName, null As BcName ");
                }
            }
        }

        private void SetSubQueryH(ref StringBuilder SQL, string DocType, string TableName, bool IsUseCancelInd)
        {
            SQL.AppendLine("And 1=Case When A.DocType='" + DocType + "' Then ");
            SQL.AppendLine("Case When Exists( ");
            SQL.AppendLine("    Select 1 From " + TableName);
            if(DocType == "PropertyInventory")
                SQL.AppendLine("    Where Status='O' And PropertyCode=A.DocNo ");
            else
                SQL.AppendLine("    Where Status='O' And DocNo=A.DocNo ");
            if (IsUseCancelInd)
                SQL.AppendLine("   And CancelInd='N' ");
            if (DocType == "BOQ" || DocType == "KPI")
            {
                SQL.AppendLine("   And ActInd = 'Y' ");
            }
            else if (DocType == "ProjectImplementation")
            {
                SQL.AppendLine("   And ProcessInd = 'F' ");
            }
            if (mIsRptOutstandingDocApprovalDeptFilteredByGroup)
            {
                if (DocType == "APDownpayment" || DocType == "VoucherRequestCashAdvance" || DocType == "ReturnARDownpayment" || DocType == "OutgoingPayment" || 
                    DocType == "VoucherRequestSwitchingBankAcc" ||
                    DocType == "VoucherRequestSwitchingCostCenter" ||
                    DocType == "VoucherRequestCASBA" ||
                    DocType == "CashAdvanceSettlement" || DocType == "VoucherRequestManual")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=" + TableName + ".DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                else if (DocType == "JournalMemorial")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                    SQL.AppendLine("    Where CCCODE=" + TableName + ".CCCODE ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("    ) Then 1 Else 0 End ");
            SQL.AppendLine("Else 1 End ");
        }

        private void SetSubQueryD(ref StringBuilder SQL, string DocType, string TableName, bool IsUseCancelInd)
        {
            SQL.AppendLine("And 1=Case When A.DocType='" + DocType + "' Then ");
            SQL.AppendLine("Case When Exists( ");
            SQL.AppendLine("    Select 1 From " + TableName);
            SQL.AppendLine("    Where Status='O' And DocNo=A.DocNo And DNo=A.DNo ");
            if (IsUseCancelInd)
                SQL.AppendLine("   And CancelInd='N' ");
            SQL.AppendLine("    ) Then 1 Else 0 End ");
            SQL.AppendLine("Else 1 End ");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteCreateDt_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteCreateDt2).Length == 0) DteCreateDt2.EditValue = DteCreateDt.EditValue;
        }

        #endregion

        #endregion
    }
}
