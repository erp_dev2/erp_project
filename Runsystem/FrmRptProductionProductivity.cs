﻿#region Update
/*
    20/09/2017 [ARI] tambah kolom Remark
    02/11/2017 [WED] tambah kolom Batch
    25/05/2018 [HAR] tambah prosesntase uom2 dari  sfc result2 dibagi total item consumption bom2
    27/06/2018 [HAR] bug remark tidak muncul
    19/07/2018 [ARI] tambah kolom dan filter item category
    05/09/2018 [TKG] tambah filter item
    13/02/2019 [MEY] Menambahkan kolom Machine disamping Result (SFC)
 *  20/10/2020 [ICA/MGI] memunculkan highlight warna hijau untuk dokumen yang nilai percentage UoM nya > 100% berdasarkan parameter IsPercentageUOMAbove100HighlightedProductionProductivity
 *  20/10/2020 [ICA/MGI] memunculkan produktivitas per shift dan group kerja nya
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionProductivity : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private decimal mTotal =0m, mTotal2 = 0m;
        private bool 
            mIsProductionProductivityUseShiftAndGroup = false,
            mIsProductionProductivityPercentageUOMHighlighted = false;
      
        #endregion

        #region Constructor

        public FrmRptProductionProductivity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            Sl.SetLueDeptCode(ref LueDeptCode);
            SetGrd();
            base.FrmLoad(sender, e);
            Sl.SetLueUomCode(ref LueUomCode);
            Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
        }

        private void GetParameter()
        {
            mIsProductionProductivityUseShiftAndGroup = Sm.GetParameterBoo("IsProductionProductivityUseShiftAndGroup");
            mIsProductionProductivityPercentageUOMHighlighted = Sm.GetParameterBoo("IsProductionProductivityPercentageUOMHighlighted");
        }

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("    Select ");
            SQL.AppendLine("    T1.DocNo, T2.DocDt, T2.WorkCenterDocNo, T3.DocName, T4.DeptName, T1.ItCode, T5.ItName, ");
            SQL.AppendLine("    T7.Remark, T1.BatchNo, T5.ItCtCode, T6.ItCtName, T1.CreateBy, ");
            SQL.AppendLine("    T1.QtyBom, Case When DocType='1' Then T5.PlanningUomCode Else Null End As UomBom, ");
            SQL.AppendLine("    T1.QtyBom2, Case When DocType='1' Then T5.PlanningUomCode2 Else Null End As UomBom2, ");
            SQL.AppendLine("    T1.QtySFC, Case When DocType='2' Then T5.PlanningUomCode Else Null End As UomSFC, ");
            SQL.AppendLine("    T1.QtySFC2, Case When DocType='2' Then T5.PlanningUomCode2 Else Null End As UomSFC2, T8.AssetName ");
            SQL.AppendLine("    , T9.OptDesc as ProductionWorkGroup, T10.ProductionShiftName as Shift ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select '1' As DocType, A.DocNo, B.ItCode, B.BatchNo, A.CreateBy, ");
            SQL.AppendLine("        0.00 As QtySFC, 0.00 As QtySFC2, ");
            SQL.AppendLine("        Sum(B.Qty) As QtyBom, Sum(B.Qty2) As QtyBom2 ");
            SQL.AppendLine("        From TblShopFloorControlHdr A ");
            SQL.AppendLine("        Inner Join TblShopFloorControl3Dtl B On A.docno = B.DocNo ");
            SQL.AppendLine("        Where A.CancelInd = 'N'  ");
            SQL.AppendLine("        And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Group By A.DocNo, A.DocNo, B.ItCode, B.BatchNo, A.CreateBy  ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select '2' As DocType, A.DocNo, B.ItCode, B.BatchNo, A.CreateBy, ");
            SQL.AppendLine("        Sum(B.Qty) As QtySFC, Sum(B.Qty2) As QtySFC2, ");
            SQL.AppendLine("        0.00 As QtyBom, 0.00 As QtyBom2 ");
            SQL.AppendLine("        From TblShopFloorControlHdr A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.docno = B.DocNo ");
            SQL.AppendLine("        Where A.CancelInd = 'N'  ");
            SQL.AppendLine("        And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Group By A.DocNo, A.DocNo, B.ItCode, B.BatchNo, A.CreateBy  ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControlHdr T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblWorkCenterHdr T3 on T2.WorkCenterDocNo = T3.DocNo ");
            SQL.AppendLine("    Left Join TblDepartment T4 on T3.DeptCode=T4.DeptCode ");
            SQL.AppendLine("    Inner Join TblItem T5 On T1.ItCode = T5.ItCode ");
            SQL.AppendLine("    Left Join TblItemCategory T6 On T5.ItCtCode=T6.ItCtCode  ");
            SQL.AppendLine("    Left Join TblPPHdr T7 On T2.PPDocNo=T7.DocNo ");
            SQL.AppendLine("    Left Join TblAsset T8 On T2.MachineCode = T8.AssetCode ");
            SQL.AppendLine("    Left Join TblOption T9 On T9.OptCat='ProductionWorkGroup' And T9.OptCode=T2.ProductionWorkGroup ");
            SQL.AppendLine("    Left Join TblProductionShift T10 On T2.ProductionShiftCode=T10.ProductionShiftCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("Order By WorkcenterDocNo, QtyBom Desc;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Work Center#", 
                        "Work Center",
                        "Department",
                        "SFC#",
                        "",

                        //6-10
                        "Date",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Item's Category",

                        //11-15
                        "Batch",
                        "Material"+Environment.NewLine+"Consumption (BOM)",
                        "UoM",
                        "Material"+Environment.NewLine+"Consumption 2"+Environment.NewLine+"(BOM)",
                        "UoM (2)",
                        
                        
                        //16-20
                        "Result"+Environment.NewLine+"(SFC)",
                        "UoM",
                        "Result 2"+Environment.NewLine+"(SFC)",
                        "UoM (2)",
                        "Percentage"+Environment.NewLine+"(%) UoM",
                        
                        //21-25
                        "Percentage"+Environment.NewLine+"(%) UoM 2",
                        "Remark",
                        "Created By",
                        "Machine", 
                        "Shift",

                        //26
                        "Group"
                    },
                    new int[]
                    {
                        40, 
                        130, 180, 150, 130, 20,
                        100, 80, 20, 230, 200,  
                        120, 130, 80, 130, 80, 
                        130, 80, 130, 80, 100, 
                        100, 150, 150, 180, 150,
                        200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 8, 14, 15, 18, 19, 21 }, false);
            Grd1.Cols[24].Move(5);
            Grd1.Cols[25].Move(3);
            Grd1.Cols[26].Move(4);
            if (!mIsProductionProductivityUseShiftAndGroup) Sm.GrdColInvisible(Grd1, new int[] { 25, 26 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 8, 14, 15, 18, 19, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                //SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty, Filter2 = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterName.Text, new string[] { "T3.DocNo", "T3.DocName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T5.ItCode", "T5.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T4.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "T5.ItCtCode", true);
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueUomCode), new string[] { "UomBom", "UomBom2", "UomSFC", "UomSFC2" });
                
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter, Filter2),
                        //"Group By X.DocNo, X.DocDt, X.WorkCenterDocNo, X.DocName, X.DeptName, " +
                        //"X.ItCode, X.ItName, " +
                        //"X.UomBom, X.UomBom2, X.UomSFC, X.UomSFC2, X.Remark, X.BatchNo " +
                        //"Order By X.WorkcenterDocNo, X.QtyBom Desc;",
                        new string[]
                        {
                            //0
                            "WorkCenterDocNo", 

                            //1-5
                            "DocName", "DeptName", "DocNo", "DocDt", "ItCode", 
                            
                            //6-10
                            "ItName", "QtyBom", "UomBom", "QtyBom2", "UomBom2", 
                            
                            //11-15
                            "QtySfc",  "UomSFC", "QtySFC2", "UomSFC2", "Remark",

                            //16-20
                            "BatchNo", "ItCtName", "CreateBy", "AssetName", "Shift",

                            //21
                            "ProductionWorkGroup"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                Grd1.GroupObject.Add(2);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14, 16, 18 });
                Compute();
                Compute2();
                Grd1.EndUpdate();
                if (mIsProductionProductivityPercentageUOMHighlighted)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    {
                        if (Sm.GetGrdDec(Grd1, i, 20) > 100)
                            Grd1.Rows[i].BackColor = Color.Green;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        //override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        //{
        //}

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
        }


        private void Compute()
        {
            string WCDocNo = string.Empty;
            int RowXX = 0;

            if (Grd1.Rows.Count > 3)
            {
                WCDocNo = Sm.GetGrdStr(Grd1, 1, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1) == WCDocNo)
                    {
                        WCDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                        RowXX = RowXX+1;
                    }
                    else
                    {
                        int RowSt = Row-(RowXX+1);
                        if (RowSt > 0)
                        {
                            ComputeRange(RowSt, Row);
                            WCDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                            RowXX = 0;
                            mTotal = 0m;
                        }
                    }
                }
            }
            else
            {

            }
        }

        private void Compute2()
        {
            string WCDocNo = string.Empty;
            int RowXX = 0;

            if (Grd1.Rows.Count > 3)
            {
                WCDocNo = Sm.GetGrdStr(Grd1, 1, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1) == WCDocNo)
                    {
                        WCDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                        RowXX = RowXX + 1;
                    }
                    else
                    {
                        int RowSt = Row - (RowXX + 1);
                        if (RowSt > 0)
                        {
                            ComputeRange2(RowSt, Row);
                            WCDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                            RowXX = 0;
                            mTotal2 = 0m;
                        }
                    }
                }
            }
            else
            {

            }
        }


        private void ComputeRange(int rowStart, int rowEnd)
        {
            for (int Row = rowStart; Row <= rowEnd; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                {
                    mTotal += Sm.GetGrdDec(Grd1, Row, 12);
                }
            }

            if (mTotal != 0)
            {
                for (int Row = rowStart; Row <= rowEnd; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        //decimal num = (((Sm.GetGrdDec(Grd1, Row, 14) / (mTotal / 2)) * 100));
                        decimal num = (((Sm.GetGrdDec(Grd1, Row, 16) / (mTotal)) * 100));
                        Grd1.Cells[Row, 20].Value = num == 0 ? Sm.FormatNum(num, 0) : Sm.FormatNum(Math.Round(num, 2), 0); 
                    }
                    else
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 12) != 0)
                        {
                            decimal num = (((Sm.GetGrdDec(Grd1, Row, 16) / Sm.GetGrdDec(Grd1, Row, 12)) * 100));
                            Grd1.Cells[Row, 20].Value = num == 0 ? Sm.FormatNum(num, 0) : Sm.FormatNum(Math.Round(num, 2), 0); 
                        }
                        else
                        {
                            Grd1.Cells[Row, 20].Value = Sm.FormatNum(0m, 0);
                        }
                    }
                }
            }
            else
            {
                for (int Row = rowStart; Row <= rowEnd; Row++)
                {
                    Grd1.Cells[Row, 20].Value = Sm.FormatNum(0m, 0);
                }
            }
        }


        private void ComputeRange2(int rowStart, int rowEnd)
        {
            for (int Row = rowStart; Row <= rowEnd; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                {
                    mTotal2 += Sm.GetGrdDec(Grd1, Row, 14);
                }
            }

            if (mTotal2 != 0)
            {
                for (int Row = rowStart; Row <= rowEnd; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        //decimal num = (((Sm.GetGrdDec(Grd1, Row, 14) / (mTotal / 2)) * 100));
                        decimal num = (((Sm.GetGrdDec(Grd1, Row, 18) / (mTotal2)) * 100));
                        Grd1.Cells[Row, 21].Value = num == 0 ? Sm.FormatNum(num, 0) : Sm.FormatNum(Math.Round(num, 2), 0);
                    }
                    else
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 12) != 0)
                        {
                            decimal num = (((Sm.GetGrdDec(Grd1, Row, 18) / Sm.GetGrdDec(Grd1, Row, 14)) * 100));
                            Grd1.Cells[Row, 21].Value = num == 0 ? Sm.FormatNum(num, 0) : Sm.FormatNum(Math.Round(num, 2), 0);
                        }
                        else
                        {
                            Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                        }
                    }
                }
            }
            else
            {
                for (int Row = rowStart; Row <= rowEnd; Row++)
                {
                    Grd1.Cells[Row, 21].Value = Sm.FormatNum(0m, 0);
                }
            }
        }

        #endregion


        #endregion

        #region Event

        #region Misc Control Event

        private void ChkWorkCenterName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work center");
        }

        private void TxtWorkCenterName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkUomCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Uom");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
