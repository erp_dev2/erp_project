﻿#region Update
/*
    14/02/2023 [IBL/BBT] New dialog. List of cancelled Property Inventory
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryDlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPropertyInventory mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor


        public FrmPropertyInventoryDlg4(FrmPropertyInventory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }


        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Property Code",
                        "Complete",
                        "Active",
                        "Status",
                        "Date of" + Environment.NewLine + "Registration",

                        //6-7
                        "Property Name",
                        "Site"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        200, 60, 50, 100, 80,

                        //6-7
                        150, 150
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdColCheck(Grd1, new int[] { 2, 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCode, A.CompleteInd, A.ActInd, Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As Status, ");
            SQL.AppendLine("A.RegistrationDt, A.PropertyName, B.SiteName ");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblSite B On A.SiteCode = B.SiteCode ");
            SQL.AppendLine("Where A.Status = 'C' ");
            SQL.AppendLine("And A.CompleteInd = 'Y' ");
            SQL.AppendLine("And A.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPropertyCode.Text, new string[] { "A.PropertyCode", "A.PropertyName" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteRegistrationDt1), Sm.GetDte(DteRegistrationDt2), "A.RegistrationDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.PropertyCode;",
                        new string[] {
                            "PropertyCode",
                            "CompleteInd", "ActInd", "Status", "RegistrationDt", "PropertyName",
                            "SiteName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowCopyData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtPropertyCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropertyCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property Inventory");
        }

        private void DteRegistrationDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteRegistrationDt2).Length == 0) DteRegistrationDt2.EditValue = DteRegistrationDt1.EditValue;
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkRegistrationDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Registration Date");
        }

        #endregion
    }
}
