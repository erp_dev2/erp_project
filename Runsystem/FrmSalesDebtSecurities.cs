﻿#region Update
/*
    22/06/2022 [RDA/PRODUCT] new menu
    06/07/2022 [SET/PRODUCT] add field Gain on Sale
*/
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmSalesDebtSecurities : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        iGCell fCell;
        bool fAccept;

        private string mDocType = "05";

        internal FrmSalesDebtSecuritiesFind FrmFind;

        internal string
            
            ChoosenBankAcc = string.Empty,
            ChoosenInvestmentDebtCode = string.Empty
            ;
        internal decimal ChoosenNominalAmt = 0m;

        internal string
            mBankAccountTypeForInvestment = string.Empty,
            mDebtInvestmentCtCode = string.Empty
            ;

        

        #endregion

        #region Constructor

        public FrmSalesDebtSecurities(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion
        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnDelete.Visible = false;
                BtnPrint.Visible = false;
                GetParameter();
                SetLueFinancialInstitutionCode(ref LueFinancialInstitutionCode, string.Empty);
                SetLueInvestmentCtCode(ref LueInvestmentCtCode, string.Empty);
                Sl.SetLueOption(ref LueInvestmentType, "InvestmentType");
                Sl.SetLueOption(ref LueAnnualDaysAssumption, "AnnualDaysAssumption");
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",
                    
                    //1-5
                    "Doc Type Code", "Document Type", "CoA Account", "CoA Name", "Doc Ref#", 

                    //6-10
                    "Doc Date", "Amount", "Remarks", "Rate", "Amt",

                    //11-14
                    "Tax", "DNo", "MultiplierField", ""

                },
                new int[]
                {
                    //0
                    20,

                    //1-5
                    100, 150, 100, 150, 120, 
                    
                    //6-10
                    100, 100, 100, 100, 100,

                    //11-14
                    30, 0, 100, 20

                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 11 });
            Sm.GrdColButton(Grd1, new int[] { 14 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 10 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 9, 10, 11, 12, 13 }, false);  
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { });
            Grd1.Cols[14].Move(3);
            Grd1.Cols[11].Move(7);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 5;
            //Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",
                    
                    //1-4
                    "User", "Status", "Date", "Remark",

                },
                new int[]
                {
                    //0
                    25,

                    //1-5
                    100, 100, 100, 100,

                }
            );
            //Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdFormatDec(Grd2, new int[] { }, 0);
            Sm.GrdColButton(Grd2, new int[] { });
            Sm.GrdColInvisible(Grd2, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, MeeCancelReason, LueFinancialInstitutionCode, TxtInvestmentBankAcc,
                        TxtStatus, TxtVoucher, TxtInvestmentCode, TxtInvestmentName, LueInvestmentType,
                        LueInvestmentCtCode, TxtNominalAmt, TxtMovingAveragePrice, TxtInvestmentCost, TxtMarketPrice,
                        TxtMarketValue, TxtSalesPrice, TxtSalesAmt, TxtInterestFrequency, DteTradeDt, 
                        DteSettlementDt, TxtCouponInterestRt, LueAnnualDaysAssumption, DteMaturityDt, DteLastCouponDt,
                        DteNextCouponDt, LueCurCode, TxtCurRate, TxtAccruedInterest, TxtTotalExpenses,
                        TxtTotalSalesAmt, MeeRemark, TxtGainSale, 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    ChkCancelInd.Enabled = false;
                    BtnRefreshData.Enabled = false;
                    BtnInvestmentBankAcc.Enabled = false;
                    BtnInvestmentCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        LueFinancialInstitutionCode, TxtNominalAmt, TxtSalesPrice, DteTradeDt, DteSettlementDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2, 5, 6, 7, 8, 11, 14 });
                    BtnRefreshData.Enabled = true;
                    BtnInvestmentBankAcc.Enabled = true;
                    BtnInvestmentCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, false);
                    BtnRefreshData.Enabled = false;
                    MeeCancelReason.Focus();
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt,  MeeCancelReason, LueFinancialInstitutionCode, TxtInvestmentBankAcc,
                TxtStatus, TxtVoucher, TxtInvestmentCode, TxtInvestmentName, LueInvestmentType,
                LueInvestmentCtCode, TxtNominalAmt, TxtMovingAveragePrice, TxtInvestmentCost, TxtMarketPrice,
                TxtMarketValue, TxtSalesPrice, TxtSalesAmt, TxtGainSale, TxtInterestFrequency, DteTradeDt,
                DteSettlementDt, TxtCouponInterestRt, LueAnnualDaysAssumption, DteMaturityDt, DteLastCouponDt,
                DteNextCouponDt, LueCurCode, TxtCurRate, TxtAccruedInterest, TxtTotalExpenses,
                TxtTotalSalesAmt, MeeRemark,
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        public void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 9, 10 });

            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueFinancialInstitutionCode(ref LueFinancialInstitutionCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteTradeDt);
                Sm.SetDteCurrentDate(DteSettlementDt);
                SetSettlementDate(Sm.GetDte(DteSettlementDt));
                FormatNumTxt(TxtNominalAmt, 0);
                FormatNumTxt(TxtMovingAveragePrice, 0);
                FormatNumTxt(TxtInvestmentCost, 0);
                FormatNumTxt(TxtMarketPrice, 0);
                FormatNumTxt(TxtMarketValue, 0);
                FormatNumTxt(TxtSalesPrice, 0);
                FormatNumTxt(TxtSalesAmt, 0);
                FormatNumTxt(TxtGainSale, 0);
                FormatNumTxt(TxtInterestFrequency, 0);
                FormatNumTxt(TxtCouponInterestRt, 0);
                FormatNumTxt(TxtCurRate, 0);
                FormatNumTxt(TxtAccruedInterest, 0);
                FormatNumTxt(TxtTotalExpenses, 0);
                FormatNumTxt(TxtTotalSalesAmt, 0);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            ChoosenBankAcc = string.Empty;
            ChoosenNominalAmt = 0m;
            ChoosenInvestmentDebtCode = string.Empty;
            SetFormControl(mState.View);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesDebtSecuritiesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) ||
                ChkCancelInd.Checked) return;
            SetFormControl(mState.Edit);
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesDebtSecurities", "TblSalesDebtSecuritiesHdr");

            cml.Add(SaveSalesEquitySecuritiesHdr(DocNo));
            cml.Add(SaveSalesDebtSecuritiesDtl(DocNo));
            cml.Add(SaveStock(DocNo));

            Sm.ExecCommands(cml);
            
            ChoosenBankAcc = string.Empty;
            ChoosenNominalAmt = 0m;
            ChoosenInvestmentDebtCode = string.Empty;

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueFinancialInstitutionCode, "Financial Institution") ||
                Sm.IsTxtEmpty(TxtInvestmentBankAcc, "Investment Bank Account", false) ||
                Sm.IsTxtEmpty(TxtInvestmentCode, "Investment Code", false) ||
                IsNominalAmtValid() ||
                Sm.IsTxtEmpty(TxtSalesPrice, "Sales Price", true) ||
                Sm.IsDteEmpty(DteTradeDt, "Trade Date") ||
                Sm.IsDteEmpty(DteSettlementDt, "Settlement Date") ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Document Type is empty")) return true;
            }

            return false;
        }

        private bool IsNominalAmtValid()
        {
            string Msg = string.Empty;
            if (decimal.Parse(TxtNominalAmt.Text) <= ChoosenNominalAmt)
            {
                return false;
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Nominal Amount entered must not exceed the Nominal Amount of the selected item from the dialogue.");
                FormatNumTxt(TxtNominalAmt, 0);
                return true;
            }
        }

        private MySqlCommand SaveSalesEquitySecuritiesHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblSalesDebtSecuritiesHdr (DocNo, DocDt, CancelInd, SekuritasCode, ");
            SQL.AppendLine("BankAcCode, Status, VoucherDocNo, InvestmentCode, InvestmentDebtCode, ");
            SQL.AppendLine("InvestmentType, InvestmentCtCode, NominalAmt, MovingAvgPrice, InvestmentCost, ");
            SQL.AppendLine("RecordedMarketPrice, RecordedMarketValue, SalesPrice, SalesAmt, InterestFrequency, ");
            SQL.AppendLine("TradeDt, SettlementDt, InterestRateAmt, AnnualDays, MaturityDt, ");
            SQL.AppendLine("LastCouponDt, NextCouponDt, CurCode, CurRt, AccruedInterest, ");
            SQL.AppendLine("TotalExpenses, TotalSalesAmt, Source, Remark, CreateBy, CreateDt)");

            SQL.AppendLine("VALUES(@DocNo, @DocDt, 'N', @SekuritasCode, ");
            SQL.AppendLine("@BankAcCode, @Status, @VoucherDocNo, @InvestmentCode, @InvestmentDebtCode, ");
            SQL.AppendLine("@InvestmentType, @InvestmentCtCode, @NominalAmt, @MovingAvgPrice, @InvestmentCost, ");
            SQL.AppendLine("@RecordedMarketPrice, @RecordedMarketValue, @SalesPrice, @SalesAmt, @InterestFrequency, ");
            SQL.AppendLine("@TradeDt, @SettlementDt, @InterestRateAmt, @AnnualDays, @MaturityDt, ");
            SQL.AppendLine("@LastCouponDt, @NextCouponDt, @CurCode, @CurRt, @AccruedInterest, ");
            SQL.AppendLine("@TotalExpenses, @TotalSalesAmt, CONCAT(@DocType,'*',@DocNo, '*','001'), @Remark, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SekuritasCode", Sm.GetLue(LueFinancialInstitutionCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", ChoosenBankAcc);
            Sm.CmParam<String>(ref cm, "@Status", "S");
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucher.Text);

            Sm.CmParam<String>(ref cm, "@InvestmentCode", TxtInvestmentCode.Text);
            Sm.CmParam<String>(ref cm, "@InvestmentDebtCode", ChoosenInvestmentDebtCode);
            Sm.CmParam<String>(ref cm, "@InvestmentType", Sm.GetLue(LueInvestmentType));
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", Sm.GetLue(LueInvestmentCtCode));
            Sm.CmParam<Decimal>(ref cm, "@NominalAmt", decimal.Parse(TxtNominalAmt.Text));

            Sm.CmParam<Decimal>(ref cm, "@MovingAvgPrice", decimal.Parse(TxtMovingAveragePrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@InvestmentCost", decimal.Parse(TxtInvestmentCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RecordedMarketPrice", decimal.Parse(TxtMarketPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@RecordedMarketValue", decimal.Parse(TxtMarketValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@SalesPrice", decimal.Parse(TxtSalesPrice.Text));

            Sm.CmParam<Decimal>(ref cm, "@SalesAmt", decimal.Parse(TxtSalesAmt.Text));
            Sm.CmParam<String>(ref cm, "@InterestFrequency", TxtInterestFrequency.Text);
            Sm.CmParamDt(ref cm, "@TradeDt", Sm.GetDte(DteTradeDt));
            Sm.CmParamDt(ref cm, "@SettlementDt", Sm.GetDte(DteSettlementDt));

            Sm.CmParam<Decimal>(ref cm, "@InterestRateAmt", decimal.Parse(TxtCouponInterestRt.Text));
            Sm.CmParam<String>(ref cm, "@AnnualDays", Sm.GetLue(LueAnnualDaysAssumption));
            Sm.CmParam<String>(ref cm, "@MaturityDt", Sm.Left(Sm.GetDte(DteMaturityDt), 8));

            Sm.CmParam<String>(ref cm, "@LastCouponDt", Sm.Left(Sm.GetDte(DteLastCouponDt),8));
            Sm.CmParam<String>(ref cm, "@NextCouponDt", Sm.Left(Sm.GetDte(DteNextCouponDt),8));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CurRt", decimal.Parse(TxtCurRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@AccruedInterest", decimal.Parse(TxtAccruedInterest.Text));

            Sm.CmParam<Decimal>(ref cm, "@TotalExpenses", decimal.Parse(TxtTotalExpenses.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalSalesAmt", decimal.Parse(TxtTotalSalesAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            return cm;
        }

        private MySqlCommand SaveSalesDebtSecuritiesDtl(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into tblsalesdebtsecuritiesdtl ");
            SQL.AppendLine("(DocNo, DNo, ExpensesCode, ExpensesDNo, ExpensesDocType, AcNo, TaxInd, DocDt, DocRef, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @ExpensesCode_" + r.ToString() + ", @ExpensesDNo_" + r.ToString() + ", ");
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    SQL.AppendLine("Null, Null, Null, ");
                else
                    SQL.AppendLine("@ExpensesDocType_" + r.ToString() + ", @AcNo_" + r.ToString() + ", @TaxInd_" + r.ToString() + ", ");
                SQL.AppendLine("@DocDt_" + r.ToString() + ", @DocRef_" + r.ToString() + ", @Amt_" + r.ToString() + ", @Remark_" + r.ToString() + ", ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ExpensesCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                Sm.CmParam<String>(ref cm, "@ExpensesDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                Sm.CmParam<String>(ref cm, "@ExpensesDocType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                Sm.CmParam<String>(ref cm, "@TaxInd_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 11) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@DocRef_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                Sm.CmParamDt(ref cm, "@DocDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 6));
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 7));
                Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
            }
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SalesDebtSecurities - Stock */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, InvestmentType, DocNo, DNo, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, BatchNo, Source, NominalAmt, Remark,  ");
            SQL.AppendLine("CreateBy, CreateDt)  ");
            SQL.AppendLine("Select @DocType, A.InvestmentType, A.DocNo, '001', A.DocDt, NULL, A.BankAcCode, '-', '-', A.InvestmentDebtCode, '-', A.Source, -1*A.NominalAmt,  ");
            SQL.AppendLine("A.Remark,  ");
            SQL.AppendLine("@UserCode, @Dt  ");
            SQL.AppendLine("From TblSalesDebtSecuritiesHdr A /*And A.Status='P' */  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N';  ");

            SQL.AppendLine("Insert Into TblInvestmentStockSummary (BankAcCode, Lot, Bin, Source, InvestmentCode, BatchNo, PropCode, NominalAmt, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Select A.BankAcCode, '-', '-', A.Source, A.InvestmentDebtCode, '-', '-', -1*A.NominalAmt, A.Remark, @UserCode, @Dt  ");
            SQL.AppendLine("From TblSalesDebtSecuritiesHdr A  ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            SQL.AppendLine("Insert Into TblInvestmentStockPrice(Source, InvestmentCode, BatchNo, PropCode, CurCode, UPrice, BasePrice, ExcRate, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Select A.Source, A.InvestmentDebtCode, '-', '-', A.CurCode, A.MovingAvgPrice, 0.00, A.CurRt, A.Remark, @UserCode, CurrentDateTime()  ");
            SQL.AppendLine("From TblSalesDebtSecuritiesHdr A  ");
            SQL.AppendLine("Where A.DocNo = @DocNo;  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            //Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            //Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, 0, 24));
            //Sm.CmParam<Decimal>(ref cm, "@BasePrice", Sm.GetGrdDec(Grd1, 0, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesEquitySecurities());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private MySqlCommand EditSalesEquitySecurities()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblsalesDebtsecuritieshdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, CancelReason = @CancelReason, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, InvestmentType, DocNo, DNo, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, BatchNo, Source, NominalAmt, Remark,  ");
            SQL.AppendLine("CreateBy, CreateDt)  ");
            SQL.AppendLine("Select @DocType, A.InvestmentType, A.DocNo, '001', A.DocDt, NULL, A.BankAcCode, '-', '-', A.InvestmentDebtCode, '-', A.Source, A.NominalAmt,  ");
            SQL.AppendLine("A.Remark,  ");
            SQL.AppendLine("@UserCode, CurrentDateTime()  ");
            SQL.AppendLine("From TblSalesDebtSecuritiesHdr A /*And A.Status='P' */  ");
            SQL.AppendLine("Where A.DocNo=@DocNo AND A.CancelInd = 'Y';  ");

            SQL.AppendLine("Update TblInvestmentStockSummary A  ");
            SQL.AppendLine("Inner Join TblSalesDebtSecuritiesHdr B On A.Source = B.Source And A.InvestmentCode = B.InvestmentDebtCode  ");
            SQL.AppendLine("Set A.NominalAmt=0, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where B.DocNo = @DocNo;  ");

            //SQL.AppendLine("Update TblInvestmentStockSummary As T1 ");
            //SQL.AppendLine("INNER JOIN ( ");
            //SQL.AppendLine("    Select A.BankAcCode, A.Lot, A.Bin, A.Source, A.InvestmentCode, ");
            //SQL.AppendLine("    Sum(A.Qty) As Qty ");
            //SQL.AppendLine("    From TblInvestmentStockMovement A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct BankAcCode, Lot, Bin, Source, InvestmentCode ");
            //SQL.AppendLine("        From TblInvestmentStockMovement ");
            //SQL.AppendLine("        Where DocType=@DocType ");
            //SQL.AppendLine("        And DocNo=@DocNo ");
            //SQL.AppendLine("        And CancelInd='N' ");
            //SQL.AppendLine("    ) B ");
            //SQL.AppendLine("    On A.BankAcCode=B.BankAcCode And A.InvestmentCode=B.InvestmentCode ");
            //SQL.AppendLine("    Where (A.Qty<>0) ");
            //SQL.AppendLine("    Group By A.BankAcCode, A.InvestmentCode ");
            //SQL.AppendLine(") T2 ");
            //SQL.AppendLine("    On T1.BankAcCode=T2.BankAcCode ");
            //SQL.AppendLine("    And T1.InvestmentCode=T2.InvestmentCode ");
            //SQL.AppendLine("Set ");
            //SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            //SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            //SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            //SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            //SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                //ClearData();

                ShowSalesDebtSecuritiesHdr(DocNo);
                ShowSalesDebtSecuritiesDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesDebtSecuritiesHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT  ");
            SQL.AppendLine("A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.SekuritasCode, B.BankAcNm,  ");
            SQL.AppendLine("CASE  ");
            SQL.AppendLine("    WHEN A.VoucherDocNo IS NULL AND A.Status = 'S' then 'Sold'  ");
            SQL.AppendLine("    WHEN A.VoucherDocNo IS NOT NULL AND A.Status = 'P' then 'Paid'  ");
            SQL.AppendLine("    ELSE NULL  ");
            SQL.AppendLine("END AS Status, A.VoucherDocNo, A.InvestmentCode, C.PortofolioName, A.InvestmentType, ");
            SQL.AppendLine("A.InvestmentCtCode, A.NominalAmt, A.MovingAvgPrice, A.InvestmentCost, A.RecordedMarketPrice, ");
            SQL.AppendLine("A.RecordedMarketValue, A.SalesPrice, A.SalesAmt, A.InterestFrequency, A.TradeDt, ");
            SQL.AppendLine("A.SettlementDt, A.InterestRateAmt, A.AnnualDays, A.MaturityDt, A.LastCouponDt, ");
            SQL.AppendLine("A.NextCouponDt, A.CurCode, A.CurRt, A.AccruedInterest, A.TotalExpenses, A.TotalSalesAmt, A.Remark ");
            SQL.AppendLine("FROM tblsalesdebtsecuritieshdr A ");
            SQL.AppendLine("INNER JOIN tblbankaccount B ON A.BankAcCode = B.BankAcCode   ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemdebt C ON A.InvestmentDebtCode = C.InvestmentDebtCode   ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId   ");
            SQL.AppendLine("-- INNER JOIN tbloption E ON A.InvestmentType = E.OptCode AND E.OptCat = 'InvestmentType'   ");
            SQL.AppendLine("-- INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode   ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "CancelReason", "SekuritasCode", "BankAcNm", 

                            //6-10
                            "Status", "VoucherDocNo", "InvestmentCode", "PortofolioName", "InvestmentType", 

                            //11-15
                            "InvestmentCtCode","NominalAmt","MovingAvgPrice","InvestmentCost","RecordedMarketPrice",

                            //16-20
                            "RecordedMarketValue", "SalesPrice", "SalesAmt", "InterestFrequency", "TradeDt",

                            //21-25
                            "SettlementDt","InterestRateAmt","AnnualDays","MaturityDt","LastCouponDt",

                            //26-30
                            "NextCouponDt","CurCode","CurRt","AccruedInterest","TotalExpenses",

                            //31-32
                            "TotalSalesAmt","Remark"

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        MeeCancelReason.Text = Sm.DrStr(dr, c[3]);
                        SetLueFinancialInstitutionCode(ref LueFinancialInstitutionCode, Sm.DrStr(dr, c[4]));
                        TxtInvestmentBankAcc.EditValue = Sm.DrStr(dr, c[5]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[6]);
                        TxtVoucher.EditValue = Sm.DrStr(dr, c[7]);

                        TxtInvestmentCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtInvestmentName.EditValue = Sm.DrStr(dr, c[9]);
                        //SetLueOption(ref LueInvestmentType, "InvestmentType", Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueInvestmentType, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueInvestmentCtCode, Sm.DrStr(dr, c[11]));
                        //SetLueInvestmentCtCode(ref LueInvestmentCtCode, Sm.DrStr(dr, c[11]));
                        TxtNominalAmt.EditValue = Sm.DrStr(dr, c[12]);
                        TxtMovingAveragePrice.EditValue = Sm.DrStr(dr, c[13]);
                        TxtInvestmentCost.EditValue = Sm.DrStr(dr, c[14]);
                        TxtMarketPrice.EditValue = Sm.DrStr(dr, c[15]);
                        TxtMarketValue.EditValue = Sm.DrStr(dr, c[16]);
                        TxtSalesPrice.EditValue = Sm.DrStr(dr, c[17]);
                        TxtSalesAmt.EditValue = Sm.DrStr(dr, c[18]);
                        TxtInterestFrequency.EditValue = Sm.DrStr(dr, c[19]);

                        Sm.SetDte(DteTradeDt, Sm.DrStr(dr, c[20]));
                        Sm.SetDte(DteSettlementDt, Sm.DrStr(dr, c[21]));
                        TxtCouponInterestRt.EditValue = Sm.DrStr(dr, c[22]);
                        //SetLueOption(ref LueAnnualDaysAssumption, "AnnualDaysAssumption", Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueAnnualDaysAssumption, Sm.DrStr(dr, c[23]));
                        Sm.SetDte(DteMaturityDt, Sm.DrStr(dr, c[24]));
                        Sm.SetDte(DteLastCouponDt, Sm.DrStr(dr, c[25]));
                        Sm.SetDte(DteNextCouponDt, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[27]));
                        TxtCurRate.EditValue = Sm.DrStr(dr, c[28]);
                        TxtAccruedInterest.EditValue = Sm.DrStr(dr, c[29]);
                        TxtTotalExpenses.EditValue = Sm.DrStr(dr, c[30]);
                        TxtTotalSalesAmt.EditValue = Sm.DrStr(dr, c[31]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[32]);

                        FormatNumTxt(TxtNominalAmt, 0);
                        FormatNumTxt(TxtMovingAveragePrice, 0);
                        FormatNumTxt(TxtInvestmentCost, 0);
                        FormatNumTxt(TxtMarketPrice, 0);
                        FormatNumTxt(TxtMarketValue, 0);
                        FormatNumTxt(TxtSalesPrice, 0);
                        FormatNumTxt(TxtSalesAmt, 0);
                        //FormatNumTxt(TxtInterestFrequency, 0);
                        FormatNumTxt(TxtCouponInterestRt, 0);
                        FormatNumTxt(TxtCurRate, 0);
                        FormatNumTxt(TxtAccruedInterest, 0);
                        FormatNumTxt(TxtTotalExpenses, 0);
                        FormatNumTxt(TxtTotalSalesAmt, 0);
                    }, true
                );
        }


        private void ShowSalesDebtSecuritiesDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ExpensesCode, IfNull(B.DocType, A.ExpensesDocType) As DocType, IfNull(B.AcNo, A.AcNo) As AcNo, ");
            SQL.AppendLine("IfNull(C.AcDesc, D.AcDesc) As AcDesc, A.DocRef, A.DocDt, A.Amt, A.Remark, B.Rate, B.Amt As Amount, IfNull(B.TaxInd, A.TaxInd) As TaxInd, ");
            SQL.AppendLine("A.ExpensesDNo, B.Formula ");
            SQL.AppendLine("From TblSalesDebtSecuritiesDtl A ");
            SQL.AppendLine("Left Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("	And A.ExpensesDNo = B.DNo ");
            SQL.AppendLine("Left Join TblCoa C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] {
                    "ExpensesCode",

                    "DocType", "AcNo", "AcDesc", "DocRef", "DocDt",

                    "Amt", "Remark", "Rate", "Amount", "TaxInd",

                    "ExpensesDNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
        }

        #endregion

        #region Additional Method

        public static string FormatNum(string Value, byte FormatType)
        {
            try
            {
                decimal NumValue = 0m;
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    //return String.Format("{0:#,##0.00##}", NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    case 2: return String.Format("{0:#,##0.00}", NumValue);
                    case 3: return String.Format("{0:#,##0.00#}", NumValue);
                    case 4: return String.Format("{0:#,##0.00##}", NumValue);
                    case 8: return String.Format("{0:#,##0.00######}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        public static string FormatNum(Decimal NumValue, byte FormatType)
        {
            try
            {
                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    case 2: return String.Format("{0:#,##0.00}", NumValue);
                    case 3: return String.Format("{0:#,##0.00#}", NumValue);
                    case 4: return String.Format("{0:#,##0.00##}", NumValue);
                    case 8: return String.Format("{0:#,##0.00######}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        public static void FormatNumTxt(DXE.TextEdit Txt, Byte FormatType)
        {
            Txt.EditValue = FormatNum(Txt.Text, FormatType);
        }

        private void SetLueOption(ref DXE.LookUpEdit Lue, string OptCat, string OptCode)
        {
            var cm = new MySqlCommand()
            { CommandText = "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat=@Param1 and OptCode=@Param2 Order By OptDesc;" };
            if (OptCat.Length > 0) Sm.CmParam<String>(ref cm, "@Param1", OptCat);
            if (OptCode.Length > 0) Sm.CmParam<String>(ref cm, "@Param2", OptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mBankAccountTypeForInvestment = Sm.GetParameter("BankAccountTypeForInvestment");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'BankAccountTypeForInvestment', 'DebtInvestmentCtCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "BankAccountTypeForInvestment": mBankAccountTypeForInvestment = ParValue; break;
                            case "DebtInvestmentCtCode": mDebtInvestmentCtCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetSettlementDate(string TradeDt)
        {
            String mTradeDt = Sm.Left(TradeDt, 8);
            String mSettlementDt = Sm.ConvertDate(mTradeDt).AddDays(2).ToString("yyyyMMdd");
            Sm.SetDte(DteSettlementDt, mSettlementDt);
        }

        internal void SetLueFinancialInstitutionCode(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SekuritasCode Col1, SekuritasName Col2 ");
                SQL.AppendLine("From TblInvestmentSekuritas ");
                if (Code.Length > 0)
                    SQL.AppendLine("Where SekuritasCode = @Code; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueInvestmentCtCode(ref DXE.LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        internal void ComputeAmt()
        {
            if (TxtNominalAmt.EditValue != null && TxtNominalAmt.Text.Length != 0 && decimal.Parse(TxtNominalAmt.Text) != 0)
            {
                //investment cost
                TxtInvestmentCost.Text = FormatNum(decimal.Parse(TxtNominalAmt.Text) * decimal.Parse(TxtMovingAveragePrice.Text), 0);

                //recorded market value
                TxtMarketValue.Text = FormatNum(decimal.Parse(TxtNominalAmt.Text) * decimal.Parse(TxtMarketPrice.Text), 0);

                //sales amount
                TxtSalesAmt.Text = FormatNum(decimal.Parse(TxtNominalAmt.Text) * decimal.Parse(TxtSalesPrice.Text), 0);

                //accrued interest
                if (DteSettlementDt.EditValue != null && DteLastCouponDt.EditValue != null)
                {
                    DateTime Date1 = Sm.ConvertDate(Sm.GetDte(DteSettlementDt));
                    DateTime Date2 = Sm.ConvertDate(Sm.GetDte(DteLastCouponDt));
                    Decimal datediff = (decimal)(Date1 - Date2).TotalDays;
                    TxtAccruedInterest.Text = FormatNum(decimal.Parse(TxtNominalAmt.Text) * (decimal.Parse(TxtCouponInterestRt.Text) / 100) / decimal.Parse(LueAnnualDaysAssumption.Text) * datediff, 0);
                }

                //expenses type
                ComputeAmtDtl();

                //total sales amount
                TxtTotalSalesAmt.Text = FormatNum(decimal.Parse(TxtSalesAmt.Text) + decimal.Parse(TxtTotalExpenses.Text), 0);
                TxtGainSale.Text = FormatNum(decimal.Parse(TxtSalesAmt.Text) - decimal.Parse(TxtInvestmentCost.Text), 0);

            }
        }


        //Tab Expenses
        internal void ComputeTotalExpenses()
        {
            decimal TotalExpenses = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //if (!Sm.GetGrdBool(Grd1, Row, 11))
                    TotalExpenses += Sm.GetGrdDec(Grd1, Row, 7);
            }
            TxtTotalExpenses.Text = Convert.ToString(TotalExpenses);
            //Sm.FormatNumTxt(TxtTotalExpenses, 0);
            FormatNumTxt(TxtTotalExpenses, 0);
        }

        internal void IsExpensesCodeExists()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length == 0)
                    GrdColsReadOnly(false, true, Grd1, r, new int[] { 0, 2, 11, 14 });
                else
                    GrdColsReadOnly(true, true, Grd1, r, new int[] { 0, 2, 11, 14 });
            }
        }

        private void GrdColsReadOnly(bool ReadOnly, bool IsChangeBackColor, iGrid Grd, int RowIndex, int[] ColIndex)
        {
            if (ReadOnly)
            {
                for (int i = 0; i < ColIndex.Length; i++)
                {
                    if (IsChangeBackColor) Grd.Cells[RowIndex, ColIndex[i]].BackColor = Color.FromArgb(224, 224, 224);
                    Grd.Cells[RowIndex, ColIndex[i]].ReadOnly = iGBool.True;
                }
            }
            else
            {
                for (int i = 0; i < ColIndex.Length; i++)
                {
                    if (IsChangeBackColor) Grd.Cells[RowIndex, ColIndex[i]].BackColor = Color.White;
                    Grd.Cells[RowIndex, ColIndex[i]].ReadOnly = iGBool.False;
                }
            }
        }

        internal string GetSelectedExpenses()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            '#' +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeAmtDtl()
        {
            decimal Rate = 0m, Amt = 0m;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                var a = Sm.GetGrdStr(Grd1, r, 13);
                var b = decimal.Parse(TxtNominalAmt.Text);
                var c = decimal.Parse(TxtAccruedInterest.Text);
                var d = decimal.Parse(TxtGainSale.Text);
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    Rate = Sm.GetGrdDec(Grd1, r, 9);
                    Amt = Sm.GetGrdDec(Grd1, r, 10);
                    if (Rate == 0)
                        Grd1.Cells[r, 7].Value = Amt;
                    else
                        //Grd1.Cells[r, 7].Value = (Rate / 100) * (Sm.GetGrdStr(Grd1, r, 13) == "1" ? decimal.Parse(TxtNominalAmt.Text) : decimal.Parse(TxtAccruedInterest.Text));
                        switch (Sm.GetGrdStr(Grd1, r, 13))
                        {
                            case "1":
                                Grd1.Cells[r, 7].Value = (Rate / 100) * decimal.Parse(TxtNominalAmt.Text);
                                break;
                            case "2":
                                Grd1.Cells[r, 7].Value = (Rate / 100) * decimal.Parse(TxtAccruedInterest.Text);
                                break;
                            case "3":
                                Grd1.Cells[r, 7].Value = (Rate / 100) * decimal.Parse(TxtGainSale.Text);
                                break;
                        }
                }
            }

            ComputeTotalExpenses();
        }

        internal void ComputeAmtDtl(List<String> ExpensesCode)
        {
            decimal Rate = 0m, Amt = 0m;

            if (ExpensesCode.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    for (int i = 0; i < ExpensesCode.Count; i++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 1) + Sm.GetGrdStr(Grd1, r, 12) == ExpensesCode[i])
                        {
                            Rate = Sm.GetGrdDec(Grd1, r, 9);
                            Amt = Sm.GetGrdDec(Grd1, r, 10);

                            if (Rate == 0)
                                Grd1.Cells[r, 7].Value = Amt;
                            if (Amt == 0)
                                Grd1.Cells[r, 7].Value = (Rate / 100) * (Sm.GetGrdStr(Grd1, r, 12) == "1" ? decimal.Parse(TxtNominalAmt.Text) : decimal.Parse(TxtAccruedInterest.Text));  
                        }
                    }
                }
            }

            ComputeTotalExpenses();
        }

        private void ShowExpensesData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ExpensesCode, B.DocType, B.AcNo, C.AcDesc, B.Rate, B.Amt, B.TaxInd, B.Formula, B.DNo ");
            SQL.AppendLine("From TblExpensesTypeHdr A ");
            SQL.AppendLine("Inner Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.TransactionCode = @TransactionCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TransactionCode", mMenuCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                {
                        //0
                        "ExpensesCode",

                        //1-5
                        "DocType", "AcNo", "AcDesc", "Rate",  "Amt", 

                        //6-8
                        "TaxInd", "DNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grid1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 5);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 8);
                    Grd1.Cells[Row, 6].Value = DteSettlementDt.Text;
                    Grd1.Cells[Row, 7].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 10 });
            IsExpensesCodeExists();
            ComputeAmtDtl();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Event

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Sm.IsTxtEmpty(TxtInvestmentCode, "Investment", false))
            {
                ShowExpensesData();
            }
            ComputeAmt();
        }

        private void BtnInvestmentBankAcc_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesDebtSecuritiesDlg(this));
        }

        private void BtnInvestmentCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesDebtSecuritiesDlg2(this));
        }

        private void LueAnnualDays_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAnnualDaysAssumption, new Sm.RefreshLue2(Sl.SetLueOption), "AnnualDaysAssumption");
        }

        private void DteSettlementDt_EditValueChanged(object sender, EventArgs e)
        {
            ComputeAmt();
        }

        private void TxtNominalAmt_Validated(object sender, EventArgs e)
        {
            FormatNumTxt(TxtNominalAmt, 0);
            ComputeAmt();
        }

        private void TxtSalesPrice_Validated(object sender, EventArgs e)
        {
            FormatNumTxt(TxtSalesPrice, 0);
            ComputeAmt();
        }

        private void DteTradeDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
            { 
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                {
                    TxtInvestmentCode, TxtInvestmentName, LueInvestmentType,
                    LueInvestmentCtCode, TxtNominalAmt, TxtMovingAveragePrice, TxtInvestmentCost, TxtMarketPrice,
                    TxtMarketValue, TxtSalesPrice, TxtSalesAmt, TxtGainSale, TxtInterestFrequency,
                    DteSettlementDt, TxtCouponInterestRt, LueAnnualDaysAssumption, DteMaturityDt, DteLastCouponDt,
                    DteNextCouponDt, LueCurCode, TxtCurRate, TxtAccruedInterest, TxtTotalExpenses,
                    TxtTotalSalesAmt, MeeRemark,
                });

                Sm.SetDteCurrentDate(DteSettlementDt);
                SetSettlementDate(Sm.GetDte(DteSettlementDt));
                FormatNumTxt(TxtNominalAmt, 0);
                FormatNumTxt(TxtMovingAveragePrice, 0);
                FormatNumTxt(TxtInvestmentCost, 0);
                FormatNumTxt(TxtMarketPrice, 0);
                FormatNumTxt(TxtMarketValue, 0);
                FormatNumTxt(TxtSalesPrice, 0);
                FormatNumTxt(TxtSalesAmt, 0);
                FormatNumTxt(TxtGainSale, 0);
                FormatNumTxt(TxtInterestFrequency, 0);
                FormatNumTxt(TxtCouponInterestRt, 0);
                FormatNumTxt(TxtCurRate, 0);
                FormatNumTxt(TxtAccruedInterest, 0);
                FormatNumTxt(TxtTotalExpenses, 0);
                FormatNumTxt(TxtTotalSalesAmt, 0);

                ClearGrd();
            }
        }

        private void TxtGainSale_Validated(object sender, EventArgs e)
        {
            
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Grid Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            ComputeAmt();
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7, 9, 10 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { }, e);
                if (Grd1.Rows.Count > 1)
                {
                    ComputeAmt();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 10 });
            if (e.ColIndex == 6) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
            if (Sm.IsGrdColSelected(new int[] { 2, 5, 6, 7, 8 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                IsExpensesCodeExists();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtInvestmentCode, "Investment", false))
            {
                if (e.ColIndex == 0)
                    Sm.FormShowDialog(new FrmSalesDebtSecuritiesDlg3(this, e.RowIndex));
                if (e.ColIndex == 14)
                    Sm.FormShowDialog(new FrmSalesDebtSecuritiesDlg4(this));
            }
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }


        #endregion



        #endregion
    }
}
