﻿#region Update
/*
    18/12/2019 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOutstandingBOMSOContract : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptOutstandingBOMSOContract(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("SELECT A.DocNo, D.CtName, A.PONo, IFNULL(G.ProjectCode, A.ProjectCode2) ProjectCode, IFNULL(G.ProjectName, F.ProjectName) ProjectName, ");
            SQL.AppendLine("B.ItCode, C.ItName, C.ItCodeInternal, C.Specification, H.BOMRevisionDocNo ");
            SQL.AppendLine("FROM TblSOContractHdr A ");
            SQL.AppendLine("INNER JOIN  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT DISTINCT CONCAT(T1.DocNo, T1.ItCode), T1.DocNo, T1.ItCode ");
            SQL.AppendLine("    FROM TblSOContractDtl T1 ");
            SQL.AppendLine("    INNER JOIN TblSOContractHdr T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        AND T2.CancelInd = 'N' ");
            SQL.AppendLine("        AND T2.Status IN ('O', 'A') ");
            SQL.AppendLine("        AND (T2.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("        AND T2.DocType = '2' ");
            SQL.AppendLine(") B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.Status IN ('O', 'A') ");
            SQL.AppendLine("    AND (A.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("    AND A.DocType = '2' ");
            SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN TblCustomer D ON A.CtCode = D.CtCode ");
            SQL.AppendLine("INNER JOIN TblBOQHdr E ON A.BOQDocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblLOPHdr F ON E.LOPDocNo = F.DocNo ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup G ON F.PGCode = G.PGCode ");
            SQL.AppendLine("LEFT JOIN  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(DISTINCT T2.DocNo Separator ', ') BOMRevisionDocNo ");
	        SQL.AppendLine("    FROM TblSOContractHdr T1 ");
	        SQL.AppendLine("    INNER JOIN TblBOMRevisionHdr T2 ON T1.BOQDocNo = T2.BOQDocNo ");
	        SQL.AppendLine("        AND T1.CancelInd = 'N' ");
	        SQL.AppendLine("        AND T1.Status IN ('O', 'A') ");
	        SQL.AppendLine("        AND (T1.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
	        SQL.AppendLine("        AND T1.DocType = '2' ");
	        SQL.AppendLine("        AND RIGHT(T2.DocNo, 4) != '0001' ");
	        SQL.AppendLine("    GROUP BY T1.DocNo ");
            SQL.AppendLine(") H ON A.DocNo = H.DocNo ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Customer", 
                    "Customer's PO#",
                    "Item Code", 
                    
                    //6-10
                    "",
                    "Item Name",
                    "Item Local Code",
                    "Specification",
                    "BOM Revision#"
                },
                new int[] 
                {
                    //0
                    20, 

                    //1-5
                    150, 20, 180, 120, 100,  
                    
                    //6-10
                    20, 250, 150, 180, 250
                }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 7, 8, 9, 10 });
            Sm.GrdColButton(Grd1, new int[] { 2, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "T.ProjectName", "T.ProjectCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, "T.PONo", false);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DocNo, T.ItName; ",
                    new string[]
                    {
                        //0
                        "DocNo",  
                        //1-5
                        "CtName", "PONo", "ItCode", "ItName", "ItCodeInternal", 
                        //6-7
                        "Specification", "BOMRevisionDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract2(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmSOContract2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmItem");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer's PO#");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        #endregion

        #endregion

    }
}
