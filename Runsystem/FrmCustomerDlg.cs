﻿#region update

/*
 * 14/06/2021 [SET/SIER] Kolom item category pada menu Customer Data terfilter berdasarkan grup log in
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCustomer mFrmParent;
        private bool mIsItCtFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmCustomerDlg(FrmCustomer FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsItCtFilteredByGroup = Sm.GetParameterBoo("IsItCtFilteredByGroup");
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "", 
                        "Item's Code", 
                        "Item's Name"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ", Filter2 = string.Empty;
                var Filter3 = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (mIsItCtFilteredByGroup)
                {
                    Filter3.AppendLine("And Exists( ");
                    Filter3.AppendLine("    Select 1 From TblGroupItemCategory ");
                    Filter3.AppendLine("    Where ItCtCode=A.ItCtCode ");
                    Filter3.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    Filter3.AppendLine("    ) ");
                }

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                if (mFrmParent.Grd5.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < mFrmParent.Grd5.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd5, Row, 1).Length != 0)
                        {
                            if (Filter2.Length > 0)
                            {
                                Filter2 += " And ";

                                Filter2 += " ItCode<>@ItCode" + No + " ";
                                Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(mFrmParent.Grd5, Row, 1));
                                No += 1;
                            }
                        }
                    }
                }
                if (Filter2.Length!=0) Filter2 = " And (" + Filter2 + ") ";
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select ItCode, ItName From TblItem A " +
                        "left join TblGroupItemCategory B on A.ItCtCode = B.ItCtCode " +
                        "left join tblitemcategory C on B.ItCtCode = C.ItCtCode " +
                        "Where SalesItemInd='Y' " + Filter + Filter2 + Filter3 + " Order By ItName;",
                        new string[]{ "ItCode", "ItName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd5.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 2, Grd1, Row2, 3);

                        mFrmParent.Grd5.Rows.Add();
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd5.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd5, Index, 1), ItCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
