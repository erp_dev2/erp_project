﻿#region Update
/*
    16/08/2019 [TKG] tambah site
 *  07/04/2020 [HAR/IMS] BUG copy data tidak bisa save  tambah indikator mCopyDataActive
 *  16/05/2022 [TYO/HIN] menarik data dari personal information saat copy data
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmployee mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmEmployeeDlg2(FrmEmployee FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        
                        //1-5
                        "Employee's Code",
                        "",
                        "Employee's Name",
                        "Old Code",
                        "Site",

                        //6-7
                        "Department", 
                        "Position"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        120, 20, 200, 100, 200, 
                        
                        //6-7
                        200, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 });

        }

        protected string SetSQL()
        {
            var SQL = new StringBuilder();

            if (ChkPersonalInformation.Checked == true)
            {
                SQL.AppendLine("Select A.DocNo As EmpCode, A.EmpName, A.EmpCodeOld, B.DeptName, C.PosName, D.SiteName ");
                SQL.AppendLine("From TblEmployeeRecruitment A ");
                SQL.AppendLine("Left Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("Left Join TblPosition C On A.PosCode = C.PosCode ");
                SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
                SQL.AppendLine("Where 1=1 ");
            }

            else
            {
                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DeptName, C.PosName, D.SiteName ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("Left Join TblPosition C On A.PosCode = C.PosCode ");
                SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
                SQL.AppendLine("Where 1=1 ");
            }

            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            if (ChkPersonalInformation.Checked == true)
                Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);

            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        Filter = " And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ";
                        break;
                    case "2":
                        Filter = " And A.ResignDt Is Not Null And A.ResignDt<=@CurrentDate ";
                        break;
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL() + Filter + " Order By D.SiteName, B.DeptName, A.EmpName;",
                    new string[] 
                    { 
                         //0
                         "EmpCode", 
                         
                         //1-4
                         "EmpName", 
                         "EmpCodeOld", 
                         "SiteName",
                         "DeptName",
                         "PosName"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                    
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if (ChkPersonalInformation.Checked == true) 
                    mFrmParent.mUsePersonalInformation = 2;
                else
                    mFrmParent.mUsePersonalInformation = 1;
                mFrmParent.mCopyDataActive = true;
                mFrmParent.CopyData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.DoDefault = false;
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employee's status");
        }

        private void ChkPersonalInformation_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkPersonalInformation.Checked == true)
            {
                Sm.ClearGrd(Grd1, true);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueStatus }, true);
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueStatus });
                Sm.GrdColInvisible(Grd1, new int[] { 1 });
            }

            else
            {
                Sm.ClearGrd(Grd1, true);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueStatus }, false);
                Sm.SetLue(LueStatus, "1");
            }
        }

        #endregion

        #endregion
    }
}
