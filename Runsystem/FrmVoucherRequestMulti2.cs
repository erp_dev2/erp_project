﻿#region Update
/*
    23/12/2020 [WED/PHT] New application
    26/01/2021 [HAR/PHT] generate docno
    02/06/2021 [TKG/PHT] mempercepat proses import
    02/06/2021 [ICA/PHT] membuat validasi 
    07/06/2021 [ICA/PHT] membuat validasi budget category available dan validasi budget maintenance available
    05/07/2021 [IBL/PHT] bisa insert data secara manual
    07/07/2021 [IBL/PHT] Custom bisa insert data detail secara manual. Budget ambil dari Budget Maintenance Yearly
    23/09/2021 [BRI/PHT] merubah source budget dari CBP profit loss monthly param MultiVRAvailableBudgetMaintenanceSource
    15/10/2021 [BRI/PHT] Year untuk available budget untuk type manual ambil dari field Year, untuk type CSV ambil dari DocDt
    18/11/2021 [SET/PHT] Feedback : Validasi CASBA cost center debit kredit dibuat berdasarkan bank group berdasar parameter IsFilterByBankAccount
    28/12/2021 [VIN/PHT] ubah generate docno year 2 digit 
    04/02/2022 [DITA/PHT] ubah tipe param MultiVRAvailableBudgetMaintenanceSource jadi internal suapaya bisa di call di child
    10/02/2022 [DITA/PHT] saat import via csv cost center tidak tergenerate otomatis sehingga mengakibatkan warning
    11/02/2022 [DITA/PHT] kolom available budget maintenance minta di hide dulu aja untuk kepentingan show ke pht
    11/02/2022 [DITA/PHY] sementara mematikan validasi budget untuk show pht
    21/04/2022 [TKG/PHT] mempercepat proses save
    17/05/2022 [TKG/PHT] tambah document approval berdasarkan department
    03/06/2022 [TKG/PHT] approval setting berdasarkan department menggunakan department dari  bank account (credit)
    24/06/2022 [TRI/PHT] bug ketika import csv deptcode di VR masih NULL
    21/02/2023 [WED/PHT] tambah validasi closing journal berdasarkan parameter IsClosingJournalBasedOnMultiProfitCenter
    15/03/2023 [DITA/PHT] validasi ketika amount yg diinputkan - (minus)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestMulti2 : RunSystem.FrmBase15
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mReqTypeForNonBudget = string.Empty;
        private bool
            mIsDocApprovalBasedOnDeptEnabled = false,
            mIsFilterByDept = false,
            mIsBudget2YearlyFormat = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsVRForBudgetUseAvailableBudget = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsAutoJournalActived = false
            ;
        internal bool mIsFilterByBankAccount = false;
        internal string
            mMultiVRAvailableBudgetMaintenanceSource = "1";

        #endregion

        #region Constructor

        public FrmVoucherRequestMulti2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueUserCode(ref LuePIC);
                SetLueMode(ref LueMode);
                Sm.SetLue(LueMode, "1");

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtPlafond, LueYr, LueMth
                }, true);

                BtnBCCode.Enabled = false;
                BtnImport.Enabled = true;

                ClearGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsDocApprovalBasedOnDeptEnabled = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='VoucherRequestCASBA' And DeptCode Is Not Null Limit 1;");

            //mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            //mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            //mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            //mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            //mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            //mIsVRForBudgetUseAvailableBudget = Sm.GetParameterBoo("IsVRForBudgetUseAvailableBudget");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ReqTypeForNonBudget', 'IsFilterByDept', 'IsBudget2YearlyFormat', 'IsMRShowEstimatedPrice', 'IsBudgetCalculateFromEstimatedPrice', ");
            SQL.AppendLine("'IsVRForBudgetUseAvailableBudget', 'MultiVRAvailableBudgetMaintenanceSource', 'IsFilterByBankAccount', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsAutoJournalActived' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                            case "IsBudgetCalculateFromEstimatedPrice": mIsBudgetCalculateFromEstimatedPrice = ParValue == "Y"; break;
                            case "IsVRForBudgetUseAvailableBudget": mIsVRForBudgetUseAvailableBudget = ParValue == "Y"; break;
                            case "IsFilterByBankAccount": mIsFilterByBankAccount = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;

                            //string
                            case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                            case "MultiVRAvailableBudgetMaintenanceSource": mMultiVRAvailableBudgetMaintenanceSource = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Bank Account (C)", 
                        "Bank Account (C)", 
                        "Bank Account (D)", 
                        "Bank Account (D)",
                        "Amount",

                        //6-10
                        "Site Code",
                        "Site",
                        "Department" + Environment.NewLine + "Code",
                        "Department",
                        "Budget Category" + Environment.NewLine + "Code",

                        //11-15
                        "Budget Category",
                        "Available Budget",
                        "Voucher Request#", 
                        "Cost Center",
                        "Available Budget" + Environment.NewLine + "Maintenance",

                        //16-20
                        "CCCode",
                        "Amount",
                        "",
                        "",
                        "",

                        //21-25
                        "Bank Code",
                        "Entity Code",
                        "Currency",
                        "ProfitCenterCode (C)",
                        "ProfitCenterCode (D)"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 300, 150, 300, 130,
                        
                        //6-10
                        100, 150, 100, 200, 100, 
                        
                        //11-15
                        180, 150, 0, 150, 150,

                        //16-20
                        0, 0, 20, 20, 20,

                        //21-25
                        0, 0, 0, 0, 0
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 24, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6, 8, 10, 12, 15, 16, 17, 18, 19, 20, 24, 25 }, false);
            Sm.GrdColButton(Grd1, new int[] { 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 12, 16 }, 0);
            Grd1.Cols[18].Move(1);
            Grd1.Cols[19].Move(2);
            Grd1.Cols[20].Move(5);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6, 8, 10, 12 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               LueYr, LueMth, TxtBCCode, TxtBCName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtPlafond }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            ProcessAvailableBudget();
            ProcessAvailableBudgetMaintenance();
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsFirst = true;
                var SQL = new StringBuilder();
                var SQL2 = new StringBuilder();
                var SQL3 = new StringBuilder();
                var SQL4 = new StringBuilder();
                var cm = new MySqlCommand();
                var cml = new List<MySqlCommand>();
                var SQLForNewVoucherRequestDocNo = Sm.GetNewVoucherRequestDocNo(Sm.Left(Sm.GetDte(DteDocDt), 8), "VoucherRequest", 1);

                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
                SQL.AppendLine("Set @MainCurCode:=(Select ParValue From TblParameter Where ParCode='MainCurCode' And ParValue Is Not Null); ");

                SQL2.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL2.AppendLine("Values");

                if (mIsDocApprovalBasedOnDeptEnabled)
                    SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        SQL.Append("Set @DocNo_" + r.ToString() + ":=" + SQLForNewVoucherRequestDocNo + ";");

                        SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                        SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, ");
                        SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, ");
                        SQL.AppendLine("BankCode, PIC, CurCode, Amt, CurCode2, ExcRate, ");
                        SQL.AppendLine("DocEnclosure, EntCode, Remark, SiteCode, MultiInd, ReqType, BCCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values(@DocNo_"+r.ToString() + ", @DocDt, 'N', 'O', 'N', @DeptCode_" + r.ToString() +", '70', ");
                        SQL.AppendLine("'C', @BankAcCode_" + r.ToString() + ", 'D', @BankAcCode2_" + r.ToString() + ", @PaymentType, ");
                        SQL.AppendLine("@BankCode_" + r.ToString() + ", @PIC, @CurCode_" + r.ToString() + ", @Amt_" + r.ToString() + ", @CurCode_" + r.ToString() + ", 1.00, ");
                        SQL.AppendLine("0.00, @EntCode_" + r.ToString() + ", @Remark, @SiteCode_" + r.ToString() + ", 'Y', '1', @BCCode_" + r.ToString() + ", @UserCode, @Dt);");

                        if (IsFirst)
                            IsFirst = false;
                        else
                        {
                            SQL2.AppendLine(", ");
                            if (mIsDocApprovalBasedOnDeptEnabled) SQL3.AppendLine(" Union All ");
                        }
                        
                        SQL2.AppendLine("(@DocNo_"+r.ToString() + ", '001', 'Multi Voucher Request', @Amt_" + r.ToString() + ", @Remark, @UserCode, @Dt) ");

                        if (mIsDocApprovalBasedOnDeptEnabled)
                        {
                            SQL3.AppendLine("Select T.DocType, @DocNo_" + r.ToString() + ", '001', T.DNo, @UserCode, @Dt ");
                            SQL3.AppendLine("From TblDocApprovalSetting T ");
                            SQL3.AppendLine("Where T.DocType='VoucherRequestCASBA' ");
                            //SQL3.AppendLine("And T.DeptCode=@DeptCode_" + r.ToString());
                            SQL3.AppendLine("And T.DeptCode=IfNull(( ");
                            SQL3.AppendLine("   Select Distinct B.DeptCode From TblBankAccount A, TblCostCenter B ");
                            SQL3.AppendLine("   Where A.CCCode = B.CCCode And B.DeptCode Is Not Null And A.BankAcCode = @BankAcCode_" + r.ToString() + " And A.CCCode Is Not Null Limit 1 ");
                            SQL3.AppendLine("),'***') ");
                            SQL3.AppendLine(" And (T.StartAmt=0 ");
                            SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                            SQL3.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                            SQL3.AppendLine("    From TblVoucherRequestHdr A ");
                            SQL3.AppendLine("    Left Join ( ");
                            SQL3.AppendLine("        Select B1.CurCode1, B1.Amt ");
                            SQL3.AppendLine("        From TblCurrencyRate B1 ");
                            SQL3.AppendLine("        Inner Join ( ");
                            SQL3.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                            SQL3.AppendLine("            From TblCurrencyRate ");
                            SQL3.AppendLine("            Where CurCode2=@MainCurCode ");
                            SQL3.AppendLine("            Group By CurCode1 ");
                            SQL3.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                            SQL3.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                            SQL3.AppendLine("    Where A.DocNo=@DocNo_" + r.ToString());
                            SQL3.AppendLine("), 0)) ");

                            SQL4.AppendLine("Update TblVoucherRequestHdr Set Status='A' Where DocNo=@DocNo_" + r.ToString());
                            SQL4.AppendLine(" And DocNo Not In (Select DocNo From TblDocApproval Where DocType='VoucherRequestCASBA' And DocNo=@DocNo_" + r.ToString() + "); ");
                        }
                        Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                        Sm.CmParam<String>(ref cm, "@BankAcCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                        Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                        Sm.CmParam<String>(ref cm, "@DeptCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                        Sm.CmParam<String>(ref cm, "@BCCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                        Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                        Sm.CmParam<String>(ref cm, "@BankCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 21));
                        Sm.CmParam<String>(ref cm, "@EntCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                        Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 23));
                    }
                }
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
                Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                cm.CommandText = string.Concat(
                    SQL.ToString(), 
                    SQL2.ToString() , ";" ,
                    (mIsDocApprovalBasedOnDeptEnabled ? string.Concat(SQL3.ToString(),";") : string.Empty),
                    SQL4.ToString());
                cml.Add(cm);
                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, "Process is completed.");
                ClearGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        public void ProcessProfitCenter(
            ref iGrid Grd, 
            int BankAcCodeCrCols, int BankAcCodeDbCols,
            int ProfitCenterCrCols, int ProfitCenterDbCols

            )
        {
            string ListOfBankAcCode = string.Empty;
            for (int i = 0; i < Grd.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd, i, BankAcCodeCrCols).Length > 0 && Sm.GetGrdStr(Grd, i, BankAcCodeDbCols).Length > 0)
                {
                    string BankAcCodeCr = Sm.GetGrdStr(Grd, i, BankAcCodeCrCols);
                    string BankAcCodeDb = Sm.GetGrdStr(Grd, i, BankAcCodeDbCols);

                    if (ListOfBankAcCode.Length > 0) ListOfBankAcCode += ",";
                    ListOfBankAcCode += string.Concat(BankAcCodeCr, ",", BankAcCodeDb);
                }
            }

            if (ListOfBankAcCode.Length > 0)
            {
                var l = new List<ProfitCenterBankAccount>();

                GetProfitCenterFromBankAccount(ref l, ListOfBankAcCode);

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd, i, BankAcCodeCrCols).Length > 0 && Sm.GetGrdStr(Grd, i, BankAcCodeDbCols).Length > 0)
                        {
                            string BankAcCodeCr = Sm.GetGrdStr(Grd, i, BankAcCodeCrCols);
                            string BankAcCodeDb = Sm.GetGrdStr(Grd, i, BankAcCodeDbCols);

                            foreach (var x in l.Where(w => w.BankAcCode == BankAcCodeCr))
                            {
                                Grd.Cells[i, ProfitCenterCrCols].Value = x.ProfitCenterCode;
                            }

                            foreach (var x in l.Where(w => w.BankAcCode == BankAcCodeDb))
                            {
                                Grd.Cells[i, ProfitCenterDbCols].Value = x.ProfitCenterCode;
                            }
                        }
                    }
                }

                l.Clear();
            }
        }

        private void GetProfitCenterFromBankAccount(ref List<ProfitCenterBankAccount> l, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode, B.ProfitCenterCode ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Where Find_In_Set(A.BankAcCode, @BankAcCodes) ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@BankAcCodes", BankAcCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BankAcCode", "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProfitCenterBankAccount()
                        {
                            BankAcCode = Sm.DrStr(dr, c[0]),
                            ProfitCenterCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        internal void ComputePlafondAmount()
        {
            if (Grd1.Rows.Count == 1) return;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++ )
            {
                Grd1.Cells[Row, 5].Value = (Convert.ToDecimal(TxtPlafond.Text) / 100) * Sm.GetGrdDec(Grd1, Row, 17);
            }
        }

        internal void ProcessAvailableBudget()
        {
            if (Grd1.Rows.Count == 1) return;

            string DeptBCCode = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (DeptBCCode.Length > 0) DeptBCCode += ",";
                DeptBCCode += string.Concat(Sm.GetGrdStr(Grd1, i, 8), "#", Sm.GetGrdStr(Grd1, i, 10));
            }

            if (DeptBCCode.Length > 0)
            {
                var l = new List<BudgetSummary>();
                CalculateAvailableBudget(DeptBCCode, ref l);
                if (l.Count > 0)
                {
                    ProcessAvailableBudget2(ref l);
                }
                l.Clear();
            }
        }

        private void ProcessAvailableBudget2(ref List<BudgetSummary> l)
        {
            bool IsCalc = false;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                IsCalc = false;
                foreach (var x in l.Where(w =>
                    w.DeptCode == Sm.GetGrdStr(Grd1, i, 8)
                    ))
                {
                    if (Sm.GetGrdStr(Grd1, i, 10).ToUpper() == "ALL")
                    {
                        foreach(var y in l
                            .Where(w => w.DeptCode == x.DeptCode)
                            .OrderByDescending(o => o.AvailableBudget))
                        {
                            Grd1.Cells[i, 12].Value = Sm.FormatNum(y.AvailableBudget, 0);
                            y.AvailableBudget -= Sm.GetGrdDec(Grd1, i, 5);
                            IsCalc = true;
                            break;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, i, 10) == x.BCCode)
                    {
                        Grd1.Cells[i, 12].Value = Sm.FormatNum(x.AvailableBudget, 0);
                        x.AvailableBudget -= Sm.GetGrdDec(Grd1, i, 5);
                        IsCalc = true;
                    }

                    if (IsCalc) break;
                }
            }
        }

        private void CalculateAvailableBudget(string DeptBCCode, ref List<BudgetSummary> l)
        {
            string
                YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
                Yr = string.Empty,
                Mth = string.Empty
                ;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetLue(LueMode) == "1")
            {
                Yr = YrMth.Substring(0, 4);
                Mth = YrMth.Substring(4, 2);
            }
            else
            {
                Yr = Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth);
            }

            if (mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("Select A.Yr, '00' As Mth, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("A.Amt2, (IfNull(D.Amt3, 0.00) + IfNull(E.Amt, 0.00) + IfNull(F.Amt, 0.00)+ IfNull(G.Amt, 0.00)) As Amt3 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2 ");
                SQL.AppendLine("    From TblBudgetSummary A ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine("    And A.Yr = @Yr ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.BCCode, A.DeptCode, Left(A.DocDt, 4) As Yr, ");
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
                else
                    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where B.cancelind = 'N' ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And A.DeptCOde Is Not Null And A.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
                SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, ");
                SQL.AppendLine("    Sum(Amt) Amt ");
                SQL.AppendLine("    From TblVoucherRequestHdr ");
                SQL.AppendLine("    Where ReqType Is Not Null ");
                SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("    And Find_In_Set(DocType, ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
                SQL.AppendLine("    And DeptCode Is Not Null And BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(DeptCode, '#', BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 4) ");
                SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, ");
                SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
                SQL.AppendLine("    From TblTravelRequestHdr A ");
                SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("    Where A.Cancelind = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And C.DeptCOde Is Not Null And B.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(C.DeptCOde, '#', B.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4) ");
                SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, SUM(T.Amt) As Amt From( ");
                SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, ");
                SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
                SQL.AppendLine("        From tblvoucherhdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("	    (");
                SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("			AND X1.DocType = '58' ");
                SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                SQL.AppendLine("	    ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("		  ( ");
                SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("			AND X1.DocType = '56' ");
                SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("            And X3.DeptCOde Is Not Null And X3.BCCode Is Not Null ");
                SQL.AppendLine("            And Find_In_set(Concat(X3.DeptCOde, '#', X3.BCCode), @DeptBCCode) ");
                SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("		  ) T ");
                SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4) ");
                SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr ");
                SQL.AppendLine("Order By A.Yr, B.DeptName, C.BCName; ");
            }
            else
            {
                SQL.AppendLine("Select A.Yr, A.Mth, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("A.Amt2, ");
                SQL.AppendLine("(IfNull(D.Amt3, 0) + IfNull(E.Amt, 0) + IfNull(F.Amt, 0)+ IfNull(G.Amt, 0)) As Amt3 ");
                SQL.AppendLine("From TblBudgetSummary A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    And A.Yr = @Yr ");
                SQL.AppendLine("    And A.Mth = @Mth ");
                SQL.AppendLine("    And A.DeptCOde Is Not Null And A.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.BCCode, A.DeptCode, Left(A.DocDt, 4) As Yr, ");
                SQL.AppendLine("    Substring(A.DocDt, 5, 2) As Mth, ");
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
                else
                    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where B.cancelind = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("    And A.DeptCOde Is Not Null And A.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
                SQL.AppendLine("    , Substring(A.DocDt, 5, 2) ");
                SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr And A.Mth=D.Mth ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, Substr(DocDt, 5, 2) Mth, ");
                SQL.AppendLine("    Sum(Amt) Amt ");
                SQL.AppendLine("    From TblVoucherRequestHdr ");
                SQL.AppendLine("    Where ReqType Is Not Null ");
                SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("    And Find_In_Set(DocType, ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substr(DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("    And DeptCOde Is Not Null And BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(DeptCOde, '#', BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 6) ");
                SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr And A.Mth=E.Mth ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, ");
                SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
                SQL.AppendLine("    From TblTravelRequestHdr A ");
                SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("    Where A.Cancelind = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("    And C.DeptCOde Is Not Null And B.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(C.DeptCOde, '#', B.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");
                SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr And A.Mth=F.Mth ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, T.Mth, SUM(T.Amt) As Amt From( ");
                SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, ");
                SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
                SQL.AppendLine("        From tblvoucherhdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("	    (");
                SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("			AND X1.DocType = '58' ");
                SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("		  ( ");
                SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("			AND X1.DocType = '56' ");
                SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("            And X3.DeptCOde Is Not Null And X3.BCCode Is Not Null ");
                SQL.AppendLine("            And Find_In_set(Concat(X3.DeptCOde, '#', X3.BCCode), @DeptBCCode) ");
                SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("		  ) T ");
                SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4), Substring(T.DocDt, 5, 2) ");
                SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr And A.Mth=G.Mth ");
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine("Order By A.Yr, A.Mth, B.DeptName, C.BCName; ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptBCCode", DeptBCCode);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "DeptCode", "BCCode", "Amt2", "Amt3" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetSummary()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            DeptCode = Sm.DrStr(dr, c[2]),
                            BCCode = Sm.DrStr(dr, c[3]),
                            Amt2 = Sm.DrDec(dr, c[4]),
                            Amt3 = Sm.DrDec(dr, c[5]),
                            AvailableBudget = Sm.DrDec(dr, c[4]) - Sm.DrDec(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        internal void ProcessAvailableBudgetMaintenance()
        {
            if (Grd1.Rows.Count == 1) return;

            string BCCode = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (BCCode.Length > 0) BCCode += ",";
                BCCode += Sm.GetGrdStr(Grd1, i, 10);
            }

            if (BCCode.Length > 0)
            {
                var l = new List<BudgetMaintenance>();
                CalculateAvailableBudgetMaintenance(BCCode, ref l);
                if (l.Count > 0)
                {
                    ProcessAvailableBudgetMaintenance2(ref l);
                }
                l.Clear();
            }
        }

        private void ProcessAvailableBudgetMaintenance2(ref List<BudgetMaintenance> l)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                foreach (var x in l.Where(w =>
                    w.BCCode == Sm.GetGrdStr(Grd1, i, 10)
                    ))
                {
                    if (Sm.GetGrdStr(Grd1, i, 10) == x.BCCode)
                    {
                        Grd1.Cells[i, 16].Value = x.CCCode;
                        Grd1.Cells[i, 15].Value = Sm.FormatNum(x.AvailableBudgetMaintenance, 0);
                        x.AvailableBudgetMaintenance -= Sm.GetGrdDec(Grd1, i, 5);
                    }
                }
            }
        }

        private void CalculateAvailableBudgetMaintenance(string BCCode, ref List<BudgetMaintenance> l)
        {
            string
                YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
                Yr = string.Empty,
                Mth = string.Empty
                ;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetLue(LueMode) == "1")
            {
                Yr = YrMth.Substring(0, 4);
                Mth = YrMth.Substring(4, 2);
            }
            else
            {
                Yr = Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth);
            }

            if (mMultiVRAvailableBudgetMaintenanceSource == "2")
            {
                SQL.AppendLine("Select T1.BCCode, T1.CCCode2, Sum(T1.Amt - IfNull(T2.Amt, 0.00) - IfNull(T3.Amt, 0.00)) Amt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select C.BCCode, A.CCCode, C.CCCode CCCode2, Sum(B.Amt" + Mth + ") Amt ");
                SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
                SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    	And A.CancelInd = 'N' ");
                SQL.AppendLine("    	And A.CCCode Is Not Null "); 
                SQL.AppendLine("    	And A.CompletedInd = 'Y' ");
                SQL.AppendLine("    	And A.Yr = @Yr ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.AcNo = C.AcNo And Find_In_set(C.BCCode, @BCCode) ");
                SQL.AppendLine("        And A.CCCode = C.CCCode ");
                SQL.AppendLine("    Group By C.BCCode, A.CCCode, C.CCCode ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select A.CCCode, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.CCCode Is Not Null ");
                SQL.AppendLine("    	And A.Status = 'A' ");
                SQL.AppendLine("    	And A.CancelInd = 'N' "); 
                SQL.AppendLine("    	And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
                SQL.AppendLine("    	And B.ItCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode And A.CCCode = C.CCCode ");
                SQL.AppendLine("    And Find_In_Set(C.BCCode, @BCCode) ");
                SQL.AppendLine("    Inner Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct X1.CCCode, X3.ItCode ");
                SQL.AppendLine("    	From TblCompanyBudgetPlanHdr X1 ");
                SQL.AppendLine("    	Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("    		And X1.CancelInd = 'N' ");
                SQL.AppendLine("    		And X1.CompletedInd = 'Y' ");
                SQL.AppendLine("            And X1.CCCode Is Not Null ");
                SQL.AppendLine("    		And X1.Yr = @Yr ");
                SQL.AppendLine("    	Inner Join TblCompanyBudgetPlanDtl2 X3 On X1.DocNo = X3.DocNo And X2.AcNo = X3.AcNo ");
                SQL.AppendLine("    	) D On A.CCCode = D.CCCode And B.ItCode = D.ItCode ");
                SQL.AppendLine("        Group By A.CCCode, C.BCCode ");
                SQL.AppendLine("    ) T2 On T1.CCCode = T2.CCCode AND T1.BCCode = T2.BCCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select A.CCCode, E.BCCode, Sum( ");
                SQL.AppendLine("    	Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End) As Amt ");
                SQL.AppendLine("    	From TblJournalHdr A ");
                SQL.AppendLine("    	Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    		And Left(A.DocDt, 6) = Concat(@Yr, @Mth) ");
                SQL.AppendLine("    		And A.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
                SQL.AppendLine("    		And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58') ");
                SQL.AppendLine("    		And A.CCCode Is Not Null ");
                SQL.AppendLine("    	Inner Join ");
                SQL.AppendLine("    	( ");
                SQL.AppendLine("    		Select Distinct X1.CCCode, X2.AcNo ");
                SQL.AppendLine("    		From TblCompanyBudgetPlanHdr X1 ");
                SQL.AppendLine("    		Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("    			And X1.CancelInd = 'N' ");
                SQL.AppendLine("    			And X1.CompletedInd = 'Y' ");
                SQL.AppendLine("    			And X1.CCCode Is Not Null ");
                SQL.AppendLine("    			And X1.Yr = @Yr ");
                SQL.AppendLine("    		    And X2.AcNo In (Select Distinct AcNo From TblBudgetCategory Where Find_In_Set(BCCode, @BCCode)) ");
                SQL.AppendLine("    	) C On A.CCCode = C.CCCode And B.AcNo = C.AcNo ");
                SQL.AppendLine("    	Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                SQL.AppendLine("    	Inner Join TblBudgetCategory E On B.AcNo = E.AcNo And Find_In_Set(E.BCCode, @BCCode) ");
                SQL.AppendLine("    	Group By A.CCCode, E.BCCode ");
                SQL.AppendLine("    ) T3 On T1.CCCode = T3.CCCode And T1.BCCode = T3.BCCode ");
                SQL.AppendLine("Group By T1.BCCode, T1.CCCode2 ");
            }
            else
            {
                SQL.AppendLine("SELECT T1.BCCode, T2.CCCode, T2.Amt ");
                SQL.AppendLine("From TblBudgetCategory T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    SELECT A.BCCode, A.CCCode, ");
                if (Sm.GetLue(LueMth) == "1")
                    SQL.AppendLine("    D.Amt" + Mth + " as Amt ");
                else
                    SQL.AppendLine("    Sum(D.Amt" + Mth + ") as Amt ");
                SQL.AppendLine("    FROM TblBudgetCategory A ");
                SQL.AppendLine("    INNER JOIN TblBudgetRequestYearlyHdr B ON A.CCCode = B.CCCode ");
                SQL.AppendLine("        AND B.CancelInd = 'N' ");
                SQL.AppendLine("        AND B.`Status` = 'A' ");
                SQL.AppendLine("        AND B.Yr = @Yr ");
                SQL.AppendLine("    INNER JOIN tblbudgetrequestyearlydtl C ON B.DocNo = C.DocNo AND A.BCCode = C.BCCode ");
                SQL.AppendLine("    INNER JOIN tblbudgetmaintenanceyearlydtl D ON B.DocNo = D.BudgetRequestDocNo AND C.SeqNo = D.SeqNo ");
                if (Sm.GetLue(LueMth) == "1")
                {
                    SQL.AppendLine("    WHERE D.CreateDt = ( ");
                    SQL.AppendLine("        SELECT X2.CreateDt ");
                    SQL.AppendLine("        FROM tblbudgetrequestyearlydtl X1 ");
                    SQL.AppendLine("        INNER JOIN tblbudgetmaintenanceyearlydtl X2 ON X1.DocNo = X2.BudgetRequestDocNo AND X1.SeqNo = X2.SeqNo ");
                    SQL.AppendLine("        WHERE X1.BCCode = C.BCCode ");
                    SQL.AppendLine("        ORDER BY X2.CreateDt DESC LIMIT 1 ");
                    SQL.AppendLine("    ) ");
                }
                else
                    SQL.AppendLine("Group By A.CCCode, A.BCCode ");
                    SQL.AppendLine(") T2 On T1.BCCode = T2.BCCode And T1.CCCode = T2.CCCode ");
                    SQL.AppendLine("Where Find_In_set(T1.BCCode, @BCCode) ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                //Sm.CmParam<String>(ref cm, "@SselectAmt", "D.Amt" + Mth);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "CCCode2", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetMaintenance()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            CCCode = Sm.DrStr(dr, c[1]),
                            AvailableBudgetMaintenance = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                string mCCCode = Sm.GetValue("Select CCCode From TblBudgetCategory Where BCCode = @Param", Sm.GetGrdStr(Grd1, Row, 10));
                Grd1.Cells[Row, 16].Value = mCCCode;
                Grd1.Cells[Row, 14].Value = Sm.GetValue("Select CCName From TblCostCenter Where CCCode = @Param", mCCCode);
                Grd1.Cells[Row, 15].Value = Sm.FormatNum(Sm.GetValue(SQL.ToString(), mCCCode, Yr, Sm.GetGrdStr(Grd1, Row, 10)), 0);
            }
        }

        private void Import1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string BankAcCodeTemp = string.Empty, BankAcCode2Temp = string.Empty, DeptCodeTemp = string.Empty, BCCodeTemp = string.Empty, SiteCodeTemp = string.Empty;
            var AmtTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            BankAcCodeTemp = arr[0].Trim();
                            BankAcCode2Temp = arr[1].Trim();
                            if (arr[2].Trim().Length > 0)
                                AmtTemp = decimal.Parse(arr[2].Trim());
                            else
                                AmtTemp = 0m;
                            if (AmtTemp < 0m)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Amount should not be less than 0.00 ");
                                return;
                            }
                            SiteCodeTemp = arr[3].Trim();
                            DeptCodeTemp = arr[4].Trim();
                            BCCodeTemp = arr[5].Trim();

                            l.Add(new Result()
                            {
                                BankAcCode = BankAcCodeTemp,
                                BankAcCode2 = BankAcCode2Temp,
                                BankAcNm = string.Empty,
                                BankAcNm2 = string.Empty,
                                Amt = AmtTemp,
                                SiteCode = SiteCodeTemp,
                                SiteName = string.Empty,
                                DeptCode = DeptCodeTemp,
                                DeptName = string.Empty,
                                BCCode = BCCodeTemp,
                                BCName = string.Empty,
                                AvailableBudget = 0m,
                                BankCode = string.Empty,
                                EntCode = string.Empty,
                                CurCode = string.Empty
                            });
                        }
                    }
                }
            }
        }

        private void Import2(ref List<BankAccount> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BankAcCode, BankAcName, BankCode, EntCode, CurCode From ( ");
            SQL.AppendLine("    Select BankAcCode, ");
            SQL.AppendLine("    Trim(Concat( ");
            SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
            SQL.AppendLine("    Case When A.BankAcNo Is Not Null Then Concat(A.BankAcNo) Else IfNull(A.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
            SQL.AppendLine(")) As BankAcName, A.BankCode, A.EntCode, A.CurCode  ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine(") T Order By BankAcName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ 
                    "BankAcCode", 
                    "BankAcName",
                    "BankCode",
                    "EntCode",
                    "CurCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BankAccount()
                        {
                            BankAcCode = Sm.DrStr(dr, c[0]),
                            BankAcName = Sm.DrStr(dr, c[1]),
                            BankCode = Sm.DrStr(dr, c[2]),
                            EntCode = Sm.DrStr(dr, c[3]),
                            CurCode = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import2_a(ref List<Department> l, ref List<Result> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var subQuery = new StringBuilder(); // budget category
            bool IsFirst = true;
            int i = 0;

            foreach (var x in l2.Select(c => new { c.DeptCode }).Distinct())
            {
                if (x.DeptCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        subQuery.AppendLine(", ");
                    subQuery.AppendLine("@DeptCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@DeptCode_" + i.ToString(), x.DeptCode);
                    i++;
                }
            }

            SQL.AppendLine("Select Distinct DeptCode, DeptName ");
            SQL.AppendLine("From TblDepartment ");
            if (subQuery.Length > 0)
            {
                SQL.AppendLine("Where DeptCode In (" + subQuery.ToString() + ") ");
                SQL.AppendLine("And ActInd = 'Y'; ");
            }
            else
                SQL.AppendLine("Where 1=0;");

            subQuery.Length = 0;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "DeptCode", "DeptName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Department()
                        {
                            DeptCode = Sm.DrStr(dr, c[0]),
                            DeptName = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import2_b(ref List<BudgetCategory> l, ref List<Result> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var subQuery = new StringBuilder(); // budget category
            bool IsFirst = true;
            int i = 0;
            
            foreach (var x in l2.Select(c => new { c.BCCode }).Distinct())
            {
                if (x.BCCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        subQuery.AppendLine(", ");
                    subQuery.AppendLine("@BCCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@BCCode_" + i.ToString(), x.BCCode);
                    i++;
                }
            }
            
            SQL.AppendLine("Select Distinct BCCode, BCName ");
            SQL.AppendLine("From TblBudgetCategory ");
            if (subQuery.Length > 0)
                SQL.AppendLine("Where BCCode In (" + subQuery.ToString() + "); ");
            else
                SQL.AppendLine("Where 1=0; ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select DeptCode, 'ALL' AS BCCode, 'ALL' As BCName ");
            //SQL.AppendLine("From TblDepartment ");
            //SQL.AppendLine("Where ActInd = 'Y';");
            
            subQuery.Length = 0;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ //"DeptCode", 
                    "BCCode", "BCName" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetCategory()
                        {
                            //DeptCode = Sm.DrStr(dr, c[0]),
                            BCCode = Sm.DrStr(dr, c[0]),
                            BCName = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import2_c(ref List<Site> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct SiteCode, SiteName ");
            SQL.AppendLine("From TblSite ");
            SQL.AppendLine("Where ActInd = 'Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Site()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            SiteName = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import3(ref List<Result> l, ref List<BankAccount> l2, ref List<Department> l3, ref List<BudgetCategory> l4, ref List<Site> l5)
        {
            byte v = 0;
            foreach (var i in l)
            {
                v = 0;
                foreach (var j in l2)
                {
                    if (Sm.CompareStr(i.BankAcCode, j.BankAcCode))
                    {
                        i.BankAcNm = j.BankAcName;
                        i.BankCode = j.BankCode;
                        i.EntCode = j.EntCode;
                        i.CurCode = j.CurCode;
                        v++;
                    }

                    if (Sm.CompareStr(i.BankAcCode2, j.BankAcCode))
                    {
                        i.BankAcNm2 = j.BankAcName;
                        v++;
                    }
                    if (v == 2) break;
                }

                foreach(var x in l3.Where(w => w.DeptCode == i.DeptCode))
                {
                    i.DeptName = x.DeptName;
                }

                foreach (var y in l4.Where(w => w.BCCode == i.BCCode))
                {
                    i.BCName = y.BCName;
                }

                foreach (var z in l5.Where(w => w.SiteCode == i.SiteCode && i.SiteCode.Length > 0))
                {
                    i.SiteName = z.SiteName;
                }
            }
        }

        private void Import4(ref List<Result> l)
        {
            int No = 1;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = No;
                r.Cells[1].Value = l[i].BankAcCode;
                r.Cells[2].Value = l[i].BankAcNm;
                r.Cells[3].Value = l[i].BankAcCode2;
                r.Cells[4].Value = l[i].BankAcNm2;
                r.Cells[5].Value = l[i].Amt;
                r.Cells[6].Value = l[i].SiteCode;
                r.Cells[7].Value = l[i].SiteName;
                r.Cells[8].Value = l[i].DeptCode;
                r.Cells[9].Value = l[i].DeptName;
                r.Cells[10].Value = l[i].BCCode;
                r.Cells[11].Value = l[i].BCName;
                r.Cells[12].Value = Sm.FormatNum(l[i].AvailableBudget, 0);
                r.Cells[13].Value = null;
                r.Cells[21].Value = l[i].BankCode;
                r.Cells[22].Value = l[i].EntCode;
                r.Cells[23].Value = l[i].CurCode;
                No++;
            }
            r = Grd1.Rows.Add();
            r.Cells[0].Value = No;
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 12 });
            Grd1.EndUpdate();
        }

        private bool IsInsertedDataNotValid()
        {
            if (mIsClosingJournalBasedOnMultiProfitCenter && mIsAutoJournalActived)
            {
                ProcessProfitCenter(ref Grd1, 1, 3, 24, 25);
            }
            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsClosingJournalForMultiVRInvalid(ref Grd1, DocDt, mIsClosingJournalBasedOnMultiProfitCenter, mIsAutoJournalActived, 1, 3, 24, 25)
                ;
        }

        public bool IsClosingJournalForMultiVRInvalid(
            ref iGrid Grd, string DocDt,
            bool mIsClosingJournalBasedOnMultiProfitCenter, bool mIsAutoJournalActived,
            int BankAcCodeCrCols, int BankAcCodeDbCols, int ProfitCenterCrCols, int ProfitCenterDbCols
            )
        {
            if (mIsClosingJournalBasedOnMultiProfitCenter && mIsAutoJournalActived)
            {
                for (int i = 0; i < Grd.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd, i, BankAcCodeCrCols).Length > 0 && Sm.GetGrdStr(Grd, i, BankAcCodeDbCols).Length > 0)
                    {
                        string ProfitCenterCodeCr = Sm.GetGrdStr(Grd, i, ProfitCenterCrCols);
                        string ProfitCenterCodeDb = Sm.GetGrdStr(Grd, i, ProfitCenterDbCols);

                        if (ProfitCenterCodeCr.Length > 0)
                        {
                            if (Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, DocDt, ProfitCenterCodeCr))
                            {
                                return true;
                            }
                        }

                        if (ProfitCenterCodeDb.Length > 0)
                        {
                            if (Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, DocDt, ProfitCenterCodeDb))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 bank account.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Bank Account (C) is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Bank Account (D) is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Amount is 0.00.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Department is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 11, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Budget Category is empty.")) return true;

                decimal Amt = 0m, AvailableBudget = 0m;
                Amt = Sm.GetGrdDec(Grd1, Row, 5);
                AvailableBudget = Sm.GetGrdDec(Grd1, Row, 12);

                if (mIsVRForBudgetUseAvailableBudget)
                {
                    if (Amt > AvailableBudget)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Amount is bigger than available budget (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) + ")");
                        Sm.FocusGrd(Grd1, Row, 5);
                        return true;
                    }
                }

                // REMARK BY DITA (SEMENTARA) 11/02/2022
                //if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0)
                //{
                //    if (Amt > Sm.GetGrdDec(Grd1, Row, 15))
                //    {
                //        Sm.StdMsg(mMsgType.Warning, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Amount is bigger than available budget maintenance (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 15), 0) + ")");
                //        Sm.FocusGrd(Grd1, Row, 5);
                //        return true;
                //    }
                //}
                //else
                //{
                    //Sm.StdMsg(mMsgType.Warning, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Budget " + Sm.GetGrdStr(Grd1, Row, 11) + " is not available because cost center is empty.");
                    //Sm.FocusGrd(Grd1, Row, 11);
                    //return true;
                //}
            }
            return false;
        }

        //private bool Process(int r, ref List<Result1> l1, ref List<Result2> l2, ref List<Result3> l3)
        //{
        //    var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");

        //    l1.Clear();
        //    l2.Clear();
        //    l3.Clear();

        //    Process1(ref r, ref l1);
        //    if (l1.Count > 0)
        //    {
        //        Process2(ref l1, ref l2);
        //        Process3(ref l1, ref l2, ref l3);
        //        if (IsInvalid(r, ref l1))
        //            return false;
        //        else
        //        {
        //            Process4(DocNo, ref l3, r);
        //            return true;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //private void Process1(ref int r, ref List<Result1> l)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Select B.DNo, A.WhsCode, B.ItCode, C.ItName, ");
        //    SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, B.UPrice ");
        //    SQL.AppendLine("From TblDOCtHdr A, TblDOCtDtl B, TblItem C ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo ");
        //    SQL.AppendLine("And A.DocNo=B.DocNo ");
        //    SQL.AppendLine("And B.CancelInd='N' ");
        //    SQL.AppendLine("And B.Qty>0.00 ");
        //    SQL.AppendLine("And B.ItCode=C.ItCode ");
        //    SQL.AppendLine("Order By B.DNo; ");

        //    Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 3));

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        { 
        //            "DNo", 
        //            "WhsCode", "ItCode", "ItName", "Qty", "Qty2", 
        //            "Qty3", "UPrice" 
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new Result1()
        //                {
        //                    DNo = Sm.DrStr(dr, c[0]),
        //                    WhsCode = Sm.DrStr(dr, c[1]),
        //                    ItCode = Sm.DrStr(dr, c[2]),
        //                    ItName = Sm.DrStr(dr, c[3]),
        //                    Qty = Sm.DrDec(dr, c[4]),
        //                    Qty2 = Sm.DrDec(dr, c[5]),
        //                    Qty3 = Sm.DrDec(dr, c[6]),
        //                    UPrice = Sm.DrDec(dr, c[7]),
        //                    Stock = 0m,
        //                    Stock2 = 0m,
        //                    Stock3 = 0m

        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void Process2(ref List<Result1> l1, ref List<Result2> l2)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    int i = 0;
        //    var Filter = string.Empty;

        //    SQL.AppendLine("Select Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3 ");
        //    SQL.AppendLine("From TblStockSummary ");
        //    SQL.AppendLine("Where Qty>0 ");
        //    SQL.AppendLine("And WhsCode=@WhsCode ");
        //    foreach (var x in l1)
        //    {
        //        if (i == 0) Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
        //        if (Filter.Length > 0) Filter += " Or ";
        //        Filter += " ItCode=@ItCode0" + i.ToString();
        //        Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), x.ItCode);
        //        ++i;
        //    }
        //    SQL.AppendLine(" And (" + Filter + "); ");


        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        { 
        //            "Lot", 
        //            "Bin", "ItCode", "PropCode", "BatchNo", "Source",
        //            "Qty", "Qty2", "Qty3" 
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l2.Add(new Result2()
        //                {
        //                    Lot = Sm.DrStr(dr, c[0]),
        //                    Bin = Sm.DrStr(dr, c[1]),
        //                    ItCode = Sm.DrStr(dr, c[2]),
        //                    PropCode = Sm.DrStr(dr, c[3]),
        //                    BatchNo = Sm.DrStr(dr, c[4]),
        //                    Source = Sm.DrStr(dr, c[5]),
        //                    Qty = Sm.DrDec(dr, c[6]),
        //                    Qty2 = Sm.DrDec(dr, c[7]),
        //                    Qty3 = Sm.DrDec(dr, c[8])
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void Process3(ref List<Result1> l1, ref List<Result2> l2, ref List<Result3> l3)
        //{
        //    decimal QtyTemp = 0m, QtyTemp2 = 0m, QtyTemp3 = 0m;
        //    foreach (var i in l1)
        //    {
        //        QtyTemp = i.Qty;
        //        QtyTemp2 = i.Qty2;
        //        QtyTemp3 = i.Qty3;
        //        foreach (var j in l2.Where(w => Sm.CompareStr(w.ItCode, i.ItCode) && w.Qty > 0m))
        //        {
        //            if (j.Qty >= QtyTemp)
        //            {
        //                l3.Add(new Result3()
        //                {
        //                    Lot = j.Lot,
        //                    Bin = j.Bin,
        //                    ItCode = j.ItCode,
        //                    PropCode = j.PropCode,
        //                    BatchNo = j.BatchNo,
        //                    Source = j.Source,
        //                    Qty = QtyTemp,
        //                    Qty2 = QtyTemp2,
        //                    Qty3 = QtyTemp3,
        //                    UPrice = i.UPrice
        //                });
        //                j.Qty -= QtyTemp;
        //                i.Stock += QtyTemp;
        //                QtyTemp = 0m;

        //                j.Qty2 -= QtyTemp2;
        //                i.Stock2 += QtyTemp2;
        //                QtyTemp2 = 0m;

        //                j.Qty3 -= QtyTemp3;
        //                i.Stock3 += QtyTemp3;
        //                QtyTemp3 = 0m;
        //            }
        //            else
        //            {
        //                l3.Add(new Result3()
        //                {
        //                    Lot = j.Lot,
        //                    Bin = j.Bin,
        //                    ItCode = j.ItCode,
        //                    PropCode = j.PropCode,
        //                    BatchNo = j.BatchNo,
        //                    Source = j.Source,
        //                    Qty = j.Qty,
        //                    Qty2 = j.Qty2,
        //                    Qty3 = j.Qty3,
        //                    UPrice = i.UPrice
        //                });
        //                i.Stock += j.Qty;
        //                QtyTemp -= j.Qty;
        //                j.Qty = 0m;

        //                i.Stock2 += j.Qty2;
        //                QtyTemp2 -= j.Qty2;
        //                j.Qty2 = 0m;

        //                i.Stock3 += j.Qty3;
        //                QtyTemp3 -= j.Qty3;
        //                j.Qty3 = 0m;
        //            }
        //            if (QtyTemp <= 0m) break;
        //        }
        //    }
        //}

        //private void Process4(string DocNo, ref List<Result3> l3, int r)
        //{
        //    var SQL = new StringBuilder();
        //    var cml = new List<MySqlCommand>();
        //    var cm = new MySqlCommand();
        //    int i = 0;

        //    SQL.AppendLine("Set @CurrentDt:=CurrentDateTime(); ");

        //    SQL.AppendLine("Insert Into TblDOCtHdr ");
        //    SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, DOCtDocNoSource, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
        //    SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocNo, @DocDt, @Status, @ProcessInd, @DOCtDocNo, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
        //    SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, Remark, @UserCode, @CurrentDt ");
        //    SQL.AppendLine("From TblDOCtHdr Where DocNo=@DOCtDocNo; ");

        //    if (mIsDOCtApprovalActived)
        //    {
        //        SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @CurrentDt ");
        //        SQL.AppendLine("From TblDocApprovalSetting T ");
        //        SQL.AppendLine("Where T.DocType='DOCt'; ");

        //        SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
        //        SQL.AppendLine("Where DocNo=@DocNo ");
        //        SQL.AppendLine("And Not Exists( ");
        //        SQL.AppendLine("    Select 1 From TblDocApproval ");
        //        SQL.AppendLine("    Where DocType='DOCt' ");
        //        SQL.AppendLine("    And DocNo=@DocNo ");
        //        SQL.AppendLine("    ); ");
        //    }

        //    foreach (var x in l3)
        //    {
        //        ++i;

        //        SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values(@DocNo, @DNo" + i.ToString() + ", 'N', @ItCode" + i.ToString() + ", @PropCode" + i.ToString() + ", @BatchNo" + i.ToString() + ", 'O', @Source" + i.ToString() + ", @Lot" + i.ToString() + ", @Bin" + i.ToString() + ", 0.00, Null, @Qty" + i.ToString() + ", @Qty2" + i.ToString() + ", @Qty3" + i.ToString() + ", @UPrice" + i.ToString() + ", Null, @UserCode, @CurrentDt); ");

        //        Sm.CmParam<String>(ref cm, "@DNo" + i.ToString(), Sm.Right("00" + (i + 1).ToString(), 3));
        //        Sm.CmParam<String>(ref cm, "@ItCode" + i.ToString(), x.ItCode);
        //        Sm.CmParam<String>(ref cm, "@PropCode" + i.ToString(), x.PropCode);
        //        Sm.CmParam<String>(ref cm, "@BatchNo" + i.ToString(), x.BatchNo);
        //        Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), x.Source);
        //        Sm.CmParam<String>(ref cm, "@Lot" + i.ToString(), x.Lot);
        //        Sm.CmParam<String>(ref cm, "@Bin" + i.ToString(), x.Bin);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty" + i.ToString(), x.Qty);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty2" + i.ToString(), x.Qty2);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty3" + i.ToString(), x.Qty3);
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice" + i.ToString(), x.UPrice);
        //    }

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@Status", mIsDOCtApprovalActived ? "O" : "A");
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseCtQtPrice ? "D" : "F");
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, r, 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", "07");

        //    if (!mIsDOCtUseCtQtPrice)
        //    {
        //        SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
        //        SQL.AppendLine("CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
        //        SQL.AppendLine("Case When A.Remark Is Null Then ");
        //        SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //        SQL.AppendLine("Else ");
        //        SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //        SQL.AppendLine("End As Remark, ");
        //        SQL.AppendLine("@UserCode, @CurrentDt ");
        //        SQL.AppendLine("From TblDOCtHdr A ");
        //        SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
        //        SQL.AppendLine("Where A.DocNo=@DocNo ");
        //        SQL.AppendLine("And A.Status='A'; ");

        //        SQL.AppendLine("Update TblStockSummary As A ");
        //        SQL.AppendLine("Inner Join ( ");
        //        SQL.AppendLine("    Select T1.WhsCode, T2.Lot, T2.Bin, T2.Source, ");
        //        SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
        //        SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
        //        SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
        //        SQL.AppendLine("    From TblDOCtHdr T1 ");
        //        SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
        //        SQL.AppendLine("    Where T1.DocNo=@DocNo ");
        //        SQL.AppendLine("    And T1.Status='A' ");
        //        SQL.AppendLine("    Group By T1.WhsCode, T2.Lot, T2.Bin, T2.Source ");
        //        SQL.AppendLine(") B ");
        //        SQL.AppendLine("    On A.WhsCode=B.WhsCode ");
        //        SQL.AppendLine("    And A.Lot=B.Lot ");
        //        SQL.AppendLine("    And A.Bin=B.Bin ");
        //        SQL.AppendLine("    And A.Source=B.Source ");
        //        SQL.AppendLine("Set ");
        //        SQL.AppendLine("    A.Qty=A.Qty-IfNull(B.Qty, 0.00), ");
        //        SQL.AppendLine("    A.Qty2=A.Qty2-IfNull(B.Qty2, 0.00), ");
        //        SQL.AppendLine("    A.Qty3=A.Qty3-IfNull(B.Qty3, 0.00), ");
        //        SQL.AppendLine("    A.LastUpBy=@UserCode, ");
        //        SQL.AppendLine("    A.LastUpDt=@CurrentDt; ");

        //        if (mIsAutoJournalActived)
        //        {
        //            SQL.AppendLine("Set @CurCode:=(Select CurCode From TblDOCtHdr Where DocNo=@DocNo); ");
        //            SQL.AppendLine("Set @EntCode:=(");
        //            SQL.AppendLine("    Select C.EntCode ");
        //            SQL.AppendLine("    From TblWarehouse A, TblCostCenter B, TblProfitCenter C ");
        //            SQL.AppendLine("    Where A.CCCode=B.CCCode And B.ProfitCenterCode=C.ProfitCenterCode ");
        //            SQL.AppendLine("    And A.WhsCode In (Select WhsCode From TblDOCtHdr Where DocNo=@DocNo) ");
        //            SQL.AppendLine("); ");

        //            SQL.AppendLine("Update TblDOCtHdr Set ");
        //            SQL.AppendLine("    JournalDocNo=");
        //            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
        //            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

        //            SQL.AppendLine("Insert Into TblJournalHdr ");
        //            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select A.JournalDocNo, ");
        //            SQL.AppendLine("A.DocDt, ");
        //            SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
        //            SQL.AppendLine("@MenuCode As MenuCode, ");
        //            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
        //            SQL.AppendLine("From TblDOCtHdr A ");
        //            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
        //            SQL.AppendLine("Where A.DocNo=@DocNo ");
        //            SQL.AppendLine("And A.Status='A'; ");

        //            SQL.AppendLine("Set @Row:=0; ");

        //            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
        //            SQL.AppendLine("From TblJournalHdr A ");
        //            SQL.AppendLine("Inner Join ( ");
        //            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

        //            if (mIsAcNoForSaleUseItemCategory)
        //            {
        //                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
        //                else
        //                    SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
        //                SQL.AppendLine("        0.00 As CAmt ");
        //                SQL.AppendLine("        From TblDOCtHdr A ");
        //                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
        //                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
        //                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
        //                SQL.AppendLine("        Where A.DocNo=@DocNo ");
        //                SQL.AppendLine("        Group By E.AcNo5 ");
        //            }
        //            else
        //            {
        //                SQL.AppendLine("        Select D.ParValue As AcNo, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
        //                else
        //                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
        //                SQL.AppendLine("        0.00 As CAmt ");
        //                SQL.AppendLine("        From TblDOCtHdr A ");
        //                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
        //                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
        //                SQL.AppendLine("        Where A.DocNo=@DocNo ");
        //            }

        //            SQL.AppendLine("        Union All ");
        //            SQL.AppendLine("        Select E.AcNo, ");
        //            SQL.AppendLine("        0.00 As DAmt, ");
        //            if (mIsDOCtAmtRounded)
        //                SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
        //            else
        //                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
        //            SQL.AppendLine("        From TblDOCtHdr A ");
        //            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
        //            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
        //            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
        //            SQL.AppendLine("        Where A.DocNo=@DocNo ");

        //            SQL.AppendLine("        Union All ");
        //            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
        //            if (mIsDOCtAmtRounded)
        //                SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
        //            else
        //                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
        //            SQL.AppendLine("        0.00 As CAmt ");
        //            SQL.AppendLine("        From ( ");
        //            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
        //            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
        //            SQL.AppendLine("            From TblDOCtHdr A ");
        //            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //            SQL.AppendLine("            Left Join ( ");
        //            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
        //            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
        //            SQL.AppendLine("            ) C On 0=0 ");
        //            SQL.AppendLine("            Where A.DocNo=@DocNo ");
        //            SQL.AppendLine("        ) X1 ");
        //            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
        //            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
        //            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
        //            SQL.AppendLine("        Union All ");

        //            if (mIsAcNoForSaleUseItemCategory)
        //            {
        //                SQL.AppendLine("        Select X.AcNo, ");
        //                SQL.AppendLine("        0.00 As DAmt, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
        //                else
        //                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
        //                SQL.AppendLine("        From ( ");
        //                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
        //                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
        //                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
        //                SQL.AppendLine("            As Amt ");
        //                SQL.AppendLine("            From TblDOCtHdr A ");
        //                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("            Left Join ( ");
        //                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
        //                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
        //                SQL.AppendLine("            ) C On 0=0 ");
        //                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
        //                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
        //                SQL.AppendLine("            Where A.DocNo=@DocNo ");
        //                SQL.AppendLine("        ) X ");
        //                SQL.AppendLine("        Group By X.AcNo ");
        //            }
        //            else
        //            {
        //                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
        //                SQL.AppendLine("        0.00 As DAmt, ");
        //                if (mIsDOCtAmtRounded)
        //                    SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
        //                else
        //                    SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
        //                SQL.AppendLine("        From ( ");
        //                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
        //                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
        //                SQL.AppendLine("            From TblDOCtHdr A ");
        //                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //                SQL.AppendLine("            Left Join ( ");
        //                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
        //                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
        //                SQL.AppendLine("            ) C On 1=1 ");
        //                SQL.AppendLine("            Where A.DocNo=@DocNo ");
        //                SQL.AppendLine("        ) X1 ");
        //                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
        //                SQL.AppendLine("        Group By X2.ParValue ");
        //            }

        //            SQL.AppendLine("    ) Tbl Group By AcNo ");
        //            SQL.AppendLine(") B On 1=1 ");
        //            SQL.AppendLine("Where A.DocNo In ( ");
        //            SQL.AppendLine("    Select JournalDocNo ");
        //            SQL.AppendLine("    From TblDOCtHdr ");
        //            SQL.AppendLine("    Where DocNo=@DocNo ");
        //            SQL.AppendLine("    And JournalDocNo Is Not Null ");
        //            SQL.AppendLine("    ); ");
        //        }


        //        Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
        //        Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
        //    }

        //    cm.CommandText = SQL.ToString();
        //    cml.Add(cm);
        //    Sm.ExecCommands(cml);
        //}

        //private bool IsInvalid(int r, ref List<Result1> l)
        //{
        //    var Msg = string.Empty;
        //    var SQL = new StringBuilder();

        //    Grd1.Cells[r, 2].Value = string.Empty;

        //    SQL.AppendLine("Select 1 ");
        //    SQL.AppendLine("From TblDOCtHdr ");
        //    SQL.AppendLine("Where DocNo In ( ");
        //    SQL.AppendLine("    Select Distinct T1.DOCtDocNoSource ");
        //    SQL.AppendLine("    From TblDOCtHdr T1, TblDOCtDtl T2 ");
        //    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
        //    SQL.AppendLine("    And T1.Status='A' ");
        //    SQL.AppendLine("    And T1.DOCtDocNoSource Is Not Null ");
        //    SQL.AppendLine("    And T2.CancelInd='N' ");
        //    SQL.AppendLine(") And DocNo=@Param;");

        //    if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, r, 3)))
        //    {
        //        Msg =
        //            "Document# : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
        //            "Warehouse : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
        //            "Customer : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine + Environment.NewLine +
        //            "This DO already copied.";
        //        Sm.StdMsg(mMsgType.Warning, Msg);
        //        Grd1.Cells[r, 2].Value = Msg;
        //        Grd1.Rows.AutoHeight();
        //        Sm.FocusGrd(Grd1, r, 2);
        //        return true;
        //    }
        //    foreach (var i in l)
        //    {
        //        if (i.Stock < i.Qty)
        //        {
        //            Msg =
        //                "Document# : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
        //                "Warehouse : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
        //                "Customer : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
        //                "Item's Code : " + i.ItCode + Environment.NewLine +
        //                "Item's Name : " + i.ItName + Environment.NewLine +
        //                "DO's Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
        //                "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
        //                "Not enough stock.";
        //            Sm.StdMsg(mMsgType.Warning, Msg);
        //            Msg =
        //                "Item's Code : " + i.ItCode + Environment.NewLine +
        //                "Item's Name : " + i.ItName + Environment.NewLine +
        //                "DO's Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
        //                "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
        //                "Not enough stock.";
        //            Grd1.Cells[r, 2].Value = Msg;
        //            Grd1.Rows.AutoHeight();
        //            Sm.FocusGrd(Grd1, r, 2);
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        private void SetLueMode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue,
             "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'VoucherRequestMultiMode' ",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
        }

        private void LueMode_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueMode) == "1")
            {
                ClearData();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtPlafond, LueYr, LueMth
                }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 18, 19, 20 });
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20 }, false);
                label3.ForeColor = label4.ForeColor = Color.Black;
                BtnBCCode.Enabled = false;
                BtnImport.Enabled = true;
            }
            else if (Sm.GetLue(LueMode) == "2")
            {
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPlafond }, 0);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPlafond, LueYr, LueMth
                    }, false);
                Sm.GrdColReadOnly(false, true, Grd1, new int[] { 18, 19, 20 });
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20 }, true);
                TxtPlafond.EditValue = Sm.FormatNum(100, 0);
                label3.ForeColor = label4.ForeColor = Color.Red;
                BtnBCCode.Enabled = true;
                BtnImport.Enabled = false;
            }
            else if (Sm.GetLue(LueMode) == "")
            {
                ClearData();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtPlafond, LueYr, LueMth
                }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 18, 19, 20 });
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20 }, false);
                label3.ForeColor = label4.ForeColor = Color.Black;
                BtnBCCode.Enabled = BtnImport.Enabled  = false;
                Sm.StdMsg(mMsgType.Warning, "You have to choose one of these mode (CSV or Manual)");
                Sm.SetLue(LueMode, "1");
            }
            Sm.RefreshLookUpEdit(LueMode, new Sm.RefreshLue1(SetLueMode));
        }

        private void TxtPlafond_Validated(object sender, EventArgs e)
        {
            decimal mPlafondAmt = Convert.ToDecimal(TxtPlafond.Text);
            if (mPlafondAmt > 100)
            {
                Sm.StdMsg(mMsgType.Warning, "Value should not more than 100.");
                TxtPlafond.EditValue = Sm.FormatNum(100, 0);
            }
            Sm.FormatNumTxt(TxtPlafond, 0);
            ComputePlafondAmount();
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void TxtBCCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void BtnBCCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg(this));
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Result>();
            var l2 = new List<BankAccount>();
            var l3 = new List<Department>();
            var l4 = new List<BudgetCategory>();
            var l5 = new List<Site>();

            ClearGrd();
            try
            {
                Import1(ref l);
                if (l.Count > 0)
                {
                    Import2(ref l2);
                    Import2_a(ref l3, ref l);
                    Import2_b(ref l4, ref l);
                    Import2_c(ref l5);
                    Import3(ref l, ref l2, ref l3, ref l4, ref l5);
                    Import4(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                ProcessAvailableBudget();
                ProcessAvailableBudgetMaintenance();
                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear();
                Cursor.Current = Cursors.Default;
            }
        }
        
        #endregion

        #region Grid Control Event

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 1 || e.ColIndex == 2)
            {
                if (Sm.GetGrdStr(Grd1, 0, 1).Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
                        {
                            Grd1.Cells[Row, 1].Value = Sm.GetGrdStr(Grd1, 0, 1);
                            Grd1.Cells[Row, 2].Value = Sm.GetGrdStr(Grd1, 0, 2);
                        }
                    }
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Sm.GetLue(LueMode) == "2")
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    Grd1.Cells[i, 0].Value = i + 1;
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 18)
            {
                if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
                Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg2(this, TxtBCCode.Text, Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            }
            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg3(this, e.RowIndex));
            }
            if (e.ColIndex == 20)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost Center still empty.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg4(this, e.RowIndex));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 18)
            {
                if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
                Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg2(this, TxtBCCode.Text, Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            }
            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }
                
                Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg3(this, e.RowIndex));
            }
            if (e.ColIndex == 20)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost Center still empty.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti2Dlg4(this, e.RowIndex));
            }
        }

        #endregion

        #endregion

        #region Class

        public class ProfitCenterBankAccount
        {
            public string BankAcCode { get; set; }
            public string ProfitCenterCode { get; set; }
        }

        private class Result
        {
            public string BankAcCode { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcCode2 { get; set; }
            public string BankAcNm2 { get; set; }
            public decimal Amt { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public decimal AvailableBudget { get; set; }
            public string BankCode { get; set; }
            public string EntCode { get; set; }
            public string CurCode { get; set; }
        }

        private class Site
        {
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
        }

        private class BankAccount
        {
            public string BankAcCode { get; set; }
            public string BankAcName { get; set; }
            public string BankCode { get; set; }
            public string EntCode { get; set; }
            public string CurCode { get; set; }
        }

        private class BudgetSummary
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string DeptCode { get; set; }
            public string BCCode { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal AvailableBudget { get; set; }
        }

        private class Department
        {
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
        }

        private class BudgetCategory
        {
            //public string DeptCode { get; set; }
            public string BCCode { get; set; }
            public string BCName { get; set; }
        }

        private class BudgetMaintenance
        {
            public string BCCode { get; set; }
            public string CCCode { get; set; }
            public decimal AvailableBudgetMaintenance { get; set; }
        }

        #endregion
    }
}
