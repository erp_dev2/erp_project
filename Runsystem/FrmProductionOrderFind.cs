﻿#region Update
/*
    14/01/2021 [WED/KSM] tambah tarik dari Sales Contract berdasarkan parameter IsSalesContractEnabled
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProductionOrderFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProductionOrder mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProductionOrderFind(FrmProductionOrder FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.UsageDt, A.DocType,  ");
            SQL.AppendLine("Case A.DocType When '1' then A.SODOcNo When '2' Then A.MakeToStockDocNo ");
            if (mFrmParent.mIsSalesContractEnabled)
                SQL.AppendLine("When '3' Then A.SCDocNo ");
            SQL.AppendLine("End As DocValue, ");
            SQL.AppendLine("A.ItCode, C.ItName, C.ItCodeInternal, C.Specification, ");
            if (mFrmParent.mIsSalesContractEnabled)
                SQL.AppendLine("Case A.DocType When '1' Then H.CtItCode When '3' Then K.CtItCode Else '' End As CtItCode, ");
            else
                SQL.AppendLine("H.CtItCode, ");
            SQL.AppendLine("A.Qty, C.PlanningUomCode, (A.Qty*C.PlanningUomCodeConvert12) As Qty2, C.PlanningUomCode2, ");
            SQL.AppendLine("A.ProductionRoutingDocNo, ");
            SQL.AppendLine("D.WorkCenterDocNo, E.DocName As WorkCenterDocName, B.BomDocNo, F.DocName As BomDocName, "); 
            SQL.AppendLine("A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");
            SQL.AppendLine("From TblProductionOrderHdr A  ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl D On A.ProductionRoutingDocNo=D.DocNo And B.ProductionRoutingDNo=D.DNo "); 
            SQL.AppendLine("Inner Join TblWorkCenterHdr E On D.WorkCenterDocNo=E.DocNo  ");
            SQL.AppendLine("Inner Join TblBomHdr F On B.BomDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblSOHdr G On A.SODOcNo=G.DocNo And G.DocNo Is Not Null ");
            SQL.AppendLine("Left Join TblCustomerItem H On G.CtCode=H.CtCode And A.ItCode=H.ItCode ");
            if (mFrmParent.mIsSalesContractEnabled)
            {
                SQL.AppendLine("Left Join TblSalesContract I On A.SCDocNo = I.DocNO ");
                SQL.AppendLine("Left Join TblSalesMemoHdr J On I.SalesMemoDocNo = J.DocNo ");
                SQL.AppendLine("Left Join TblCustomerItem K On J.CtCode = K.CtCode And A.ItCode = K.ItCode ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Usage Date",
                        "Type",
                        
                        //6-10
                        "SO#/MTS#",
                        "", 
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Item's Name",

                        //11-15
                        "Local Code",
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Quantity",
                        "UoM",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Routing#",
                        "",
                        "Work Center#",
                        "",
                        
                        //21-25
                        "Work Center"+Environment.NewLine+"Name",
                        "BOM#",
                        "",
                        "BOM"+Environment.NewLine+"Name",
                        "Remark",

                        //26-30
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        
                        //31-32
                        "Last"+Environment.NewLine+"Updated Time",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 100, 150, 
                        
                        //6-10
                        130, 20, 100, 20, 250,  
                        
                        //11-15
                        80, 80, 100, 60, 100,
                        
                        //16-20
                        60, 130, 20, 130, 20, 
                        
                        //21-25
                        180, 130, 20, 180, 150, 
                        
                        //26-30
                        100, 100, 100, 100, 100, 
                        
                        //31-32
                        100, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 7, 9, 18, 20, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 4, 27, 30 });
            Sm.GrdFormatTime(Grd1, new int[] { 28, 31 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32 });
            Grd1.Cols[32].Move(12);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8, 9, 11, 12, 17, 18, 19, 20, 22, 23, 26, 27, 28, 29, 30, 31, 32 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 11, 12, 17, 18, 19, 20, 22, 23, 26, 27, 28, 29, 30, 31, 32 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And A.CancelInd='N' ";

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                if (mFrmParent.mIsSalesContractEnabled)
                    Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, new string[] { "A.SODocNo", "A.SCDocNo" });
                else
                    Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "A.SODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBomDocNo.Text, "B.BomDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "C.ItCodeINternal", "C.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "UsageDt", "Doctype", "DocValue",  
                            
                            //6-10
                            "ItCode", "ItName", "ItCodeInternal", "CtItCode", "Qty", 

                            //11-15
                            "PlanningUomCode", "Qty2", "PlanningUomCode2", "ProductionRoutingDocNo", "WorkCenterDocNo", 
                            
                            //16-20
                            "WorkCenterDocName", "BomDocNo", "BomDocName", "Remark", "CreateBy", 
                            
                            //21-24
                            "CreateDt", "LastUpBy", "LastUpDt", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 28, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 31, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 24);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 5) == "1")
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 6));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 8));
                
            if (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 22).Length != 0)
            {
                var f = new FrmBom2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 22);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 5) == "1")
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 6));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 8));
            
            if (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
            if (e.ColIndex == 23 && Sm.GetGrdStr(Grd1, e.RowIndex, 22).Length != 0)
            {
                var f = new FrmBom2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 22);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Sales order#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBomDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBomDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bill of material#");
        }

        #endregion

        #endregion
    }
}
