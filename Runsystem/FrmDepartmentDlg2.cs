﻿#region Update
/*
    12/03/2020 [VIN/SRN] tambah tab COA
    04/05/2020 [TKG/TWC] tambah inputan COA utk THR
    30/06/2020 [VIN/IMS] tambah inputan COA utk Incentive
    23/07/2020 [WED/SIER] tambah inputan COA untuk Cash Advance Settlement
    28/10/2020 [TKG/IMS] tambah coa ss
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDepartmentDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDepartment mFrmParent;
        private string mSQL = string.Empty;
        private byte mType = 1;

        #endregion

        #region Constructor

        public FrmDepartmentDlg2(FrmDepartment FrmParent, byte Type)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mType = Type;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcType ");
            SQL.AppendLine("From TblCOA A");
            SQL.AppendLine("Inner Join TblOption B On A.AcType = B.OptCode And B.OptCat = 'AccountType' ");
	        SQL.AppendLine("Where A.ActInd = 'Y' ");
	        SQL.AppendLine("And A.Parent Is Not Null ");
	        SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-3
                    "Account#", 
                    "Desription",
                    "Type"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-3
                    150, 300, 100                    }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "A.AcNo", "A.AcDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo; ",
                        new string[] 
                        { 
                            //0
                            "AcNo",
                            //1-2
                            "AcDesc", "AcType"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (IsFindGridValid(Grd1, 1))
                {
                    if (mType == 1)
                    {
                        mFrmParent.TxtAcNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 2)
                    {
                        mFrmParent.TxtAcNo2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 3)
                    {
                        mFrmParent.TxtAcNo3.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc3.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 4)
                    {
                        mFrmParent.TxtAcNo4.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc4.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 5)
                    {
                        mFrmParent.TxtAcNo5.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc5.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 6)
                    {
                        mFrmParent.TxtAcNo6.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc6.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 7)
                    {
                        mFrmParent.TxtAcNo7.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc7.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 8)
                    {
                        mFrmParent.TxtAcNo8.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc8.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 9)
                    {
                        mFrmParent.TxtAcNo9.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc9.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                    if (mType == 10)
                    {
                        mFrmParent.TxtAcNo10.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                        mFrmParent.MeeAcDesc10.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    }
                }
                else
                {
                    if (mType == 1)
                    {
                        mFrmParent.TxtAcNo.EditValue = null;
                        mFrmParent.MeeAcDesc.EditValue = null;
                    }
                    if (mType == 2)
                    {
                        mFrmParent.TxtAcNo2.EditValue = null;
                        mFrmParent.MeeAcDesc2.EditValue = null;
                    }
                    if (mType == 3)
                    {
                        mFrmParent.TxtAcNo3.EditValue = null;
                        mFrmParent.MeeAcDesc3.EditValue = null;
                    }
                    if (mType == 4)
                    {
                        mFrmParent.TxtAcNo4.EditValue = null;
                        mFrmParent.MeeAcDesc4.EditValue = null;
                    }
                    if (mType == 5)
                    {
                        mFrmParent.TxtAcNo5.EditValue = null;
                        mFrmParent.MeeAcDesc5.EditValue = null;
                    }
                    if (mType == 6)
                    {
                        mFrmParent.TxtAcNo6.EditValue = null;
                        mFrmParent.MeeAcDesc6.EditValue = null;
                    }
                    if (mType == 7)
                    {
                        mFrmParent.TxtAcNo7.EditValue = null;
                        mFrmParent.MeeAcDesc7.EditValue = null;
                    }

                    if (mType == 8)
                    {
                        mFrmParent.TxtAcNo8.EditValue = null;
                        mFrmParent.MeeAcDesc8.EditValue = null;
                    }
                    if (mType == 9)
                    {
                        mFrmParent.TxtAcNo9.EditValue = null;
                        mFrmParent.MeeAcDesc9.EditValue = null;
                    }
                    if (mType == 10)
                    {
                        mFrmParent.TxtAcNo10.EditValue = null;
                        mFrmParent.MeeAcDesc10.EditValue = null;
                    }
                }
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsFindGridValid(iGrid Grd, int Col)
        {
            bool IsNotValid = false;

            if (Grd.CurRow == null) IsNotValid = true;

            if (!IsNotValid && Grd.Cells[Grd.CurRow.Index, Col].Value == null) IsNotValid = true;

            if (!IsNotValid && Grd.Rows[Grd.CurRow.Index].BackColor == Color.LightSalmon) IsNotValid = true;

            if (IsNotValid) return false;
            
            return true;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA's account");
        }

        #endregion

        #endregion

    }
}
