﻿#region Update
    //27/10/2020 [ICA/MGI] New App
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBOMRealization : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptBOMRealization(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetGrd();
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT J.ItCode, J.ItName, A.DocNo as SFCDocNo, B.Qty as QtySFC, J.PlanningUOMCode as UoM, I.Qty as QtyBOM ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControl3Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPPHdr C On A.PPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblPPDtl D On C.DocNo = D.DocNo And D.DocType = '1' ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr E On D.ProductionOrderDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl G On E.ProductionRoutingDocNo = G.DocNo ");
            SQL.AppendLine("    And F.ProductionRoutingDNo = G.DNo ");
            SQL.AppendLine("    And G.WorkCenterDocNo = A.WorkCenterDocNo ");
            SQL.AppendLine("Inner Join TblBOMHdr H On F.BOMDocNo = H.DocNo And H.ActiveInd = 'Y' ");
            SQL.AppendLine("Inner Join TblBOMDtl I On H.DocNo = I.DocNo And I.DocType <> '3' And I.DocCode = B.ItCode ");
            SQL.AppendLine("Inner Join TblItem J On B.ItCode = J.ItCode ");
            SQL.AppendLine("Where A.DocDt between @DocDt1 and @DocDt2 ");
            SQL.AppendLine("Order By A.DocNo, QtySFC Desc;");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Name",
                        "SFC#",
                        "Quantity"+Environment.NewLine+"(SFC)",
                        "UoM"+Environment.NewLine+"(SFC)",
                        "Quantity"+Environment.NewLine+"(BOM)",

                        //6-7
                        "UoM"+Environment.NewLine+"(BOM)",
                        "Percentage"
                    },
                    new int[]
                    {
                        40, 
                        150, 180, 100, 80, 100,
                        80, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 5, 7 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty, Filter2 = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtSFCDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "J.ItCode", "J.ItName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter,
                        new string[]
                        {
                            //0
                            "SFCDocNo", 

                            //1-4
                            "ItName", "QtySFC", "UoM", "QtyBOM"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        }, true, false, false, false
                    );
                compute();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void compute()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 5) != 0)
                {
                    decimal num = (((Sm.GetGrdDec(Grd1, Row, 3) / Sm.GetGrdDec(Grd1, Row, 5)) * 100));
                    Grd1.Cells[Row, 7].Value = num == 0 ? Sm.FormatNum(num, 0) : Sm.FormatNum(Math.Round(num, 2), 0);
                }
                else
                {
                    Grd1.Cells[Row, 7].Value = Sm.FormatNum(0m, 0);
                }
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSFCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSFCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SFC#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
