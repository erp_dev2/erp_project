﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShift : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmShiftFind FrmFind;

        #endregion

        #region Constructor

        public FrmShift(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtShiftCode, TxtShiftName, TmeStartTm, TmeEndTm
                    }, true);
                    TxtShiftCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtShiftCode, TxtShiftName, TmeStartTm, TmeEndTm
                    }, false);
                    TxtShiftCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtShiftName, TmeStartTm, TmeEndTm
                    }, false);
                    TxtShiftName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtShiftCode, TxtShiftName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmShiftFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtShiftCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtShiftCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblShift Where ShiftCode=@ShiftCode" };
                Sm.CmParam<String>(ref cm, "@ShiftCode", TxtShiftCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblShift(ShiftCode, ShiftName, StartTm, EndTm, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ShiftCode, @ShiftName, @StartTm, @EndTm, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ShiftName=@ShiftName, StartTm=@StartTm, EndTm=@EndTm, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ShiftCode", TxtShiftCode.Text);
                Sm.CmParam<String>(ref cm, "@ShiftName", TxtShiftName.Text);
                Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
                Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtShiftCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AcCtCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ShiftCode", AcCtCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblShift Where ShiftCode=@ShiftCode",
                        new string[] 
                        {
                            "ShiftCode", 
                            "ShiftName", "StartTm", "EndTm"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtShiftCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtShiftName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[2]));
                            Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[3]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtShiftCode, "Shift code", false) ||
                Sm.IsTxtEmpty(TxtShiftName, "Shift name", false) ||
                IsShiftCodeExisted();
        }

        private bool IsShiftCodeExisted()
        {
            if (!TxtShiftCode.Properties.ReadOnly && Sm.IsDataExist("Select ShiftCode From TblShift Where ShiftCode='" + TxtShiftCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Shift code ( " + TxtShiftCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtShiftCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShiftCode);
        }

        private void TxtShiftName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShiftName);
        }

        #endregion

        #endregion
    }
}
