﻿#region Update
/*
    03/10/2017 [TKG] Filter item category berdasarkan otorisasi group menu. 
    17/11/2017 [ARI] tambah lup MR,POR,PO
    29/11/2017 [ARI] Filter berdasarkan group untuk menampilkan lup MR,POR,PO
    13/12/2017 [ARI] Filter berdasarkan group untuk menampilkan lup Recv
    19/03/2018 [TKG] tambah site dan entity
    12/04/2018 [WED] site dan entity kebalik
    05/06/2018 [TKG] tambah informasi status approval PO
    09/10/2019 [DITA/MAI] tambah PIC 
    11/06/2020 [HAR/TWC] bug : reporting belum ambil approval materialrequest exim, dan ketika approve by system tidak muncul 
    27/20/2020 [ICA/IMS] mengubah Material Request menjadi Purchase Request
    19/20/2020 [IBL/IMS] tambah kolom specification berdasarkan parameter IsBOMShowSpecifications
    28/10/2021 [DEV/PHT] Menambah time approval pada loop di menu reporting MR, Purchasing and Receiving Monitoring Approval Status 
    25/11/2021 [DEV/IOK] Menampilkan informasi purchaser di detail menu berdasarkan pembuat dokumen Purchase Order (PO) dengan parameter IsRptProcurementStatusShowPurchaserInfo
    26/11/2021 [RIS/IOK] Menambah kolom POR Remark dengan parameter IsRptProcurementStatusShowPORRemarkInfo
    01/11/2021 [YOG/IOK] Pada menu reporting MR, Purchasing and Receiving Monitoring Approval Status (020405) menambahkan filter date range pada header menu dari tanggal dokumen PO dengan parameter IsRptProcurementStatusUsePODateFilter.
    11/01/2022 [MYA/IMS] Penambahan source PR for Service pada kolom PR# dan sourcer PO for Service pada kolom PO#
    19/01/2022 [DEV/IMS] Item reject tidak perlu masuk di reporting PR Purchasing and Receiving Monitoring Approval Status dengan parameter IsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem
    28/01/2022 [DEV/PHT] Menambahkan kolom Estimated Received Date Revision Pada reporting MR, Purchasing and Receiving Monitoring Approval Status yang mengambil dari kolom Estimated Received Date Revision di menu PO dengan parameter IsMRPurchasingAndReceivingMonitoringApprovalStatusUsePORevision
    28/01/2022 [DEV/PHT] Menambah kolom Revision Quantity Pada reporting MR, Purchasing and Receiving Monitoring Approval Status, dan kolom itu menarik dari quantity revision PO jika terjadi penambahan atau pengurangan quantity dari adendum yang paling akhir dengan parameter IsMRPurchasingAndReceivingMonitoringApprovalStatusUsePORevision
    15/02/2022 [VIN/ACADEMY] BUG : Process3 salah filter 
    14/04/2022 [RIS/PHT] Merubah parameter IsMRPurchasingAndReceivingMonitoringApprovalStatusUsePORev menjadi IsRptProcurementUsePORevision
    06/03/2023 [WED/HEX] tambah kolom terkait amount dan budget berdasarkan parameter IsMRUseApprovalSheet
    09/03/2023 [WED/HEX] bug fix kolom Request Type MR belum sesuai
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProcurementStatus : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, Doctitle = Sm.GetParameter("Doctitle");
        private bool
            mIsFilterByItCt = false,
            mIsRptProcurementStatusShowDocInfoByGrp = false,
            mIsSiteMandatory = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsBOMShowSpecifications = false,
            mIsRptProcurementStatusShowPurchaserInfo = false,
            mIsRptProcurementStatusShowPORRemarkInfo = false,
            mIsRptProcurementStatusUsePODateFilter = false,
            mIsUsePOForServiceDocNo = false,
            mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem = false,
            mIsRptProcurementUsePORevision = false,
            mIsMRUseApprovalSheet = false
            ;

        #endregion

        #region Constructor

        public FrmRptProcurementStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sm.SetDefaultPeriod(ref DtePODocDt1, ref DtePODocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                Sl.SetLueVdCode(ref LueVdCode);

                if (Doctitle == "IMS")
                {
                    label6.Text = "Purchase Request#";
                    label6.Location = new System.Drawing.Point(2, 29);
                }
                base.FrmLoad(sender, e);

                if (!mIsRptProcurementStatusUsePODateFilter)
                {
                    int ypoint = 21;
                    label10.Visible = false;
                    label11.Visible = false;
                    DtePODocDt1.Visible = false;
                    DtePODocDt2.Visible = false;
                    ChkPODocDt.Visible = false;
                    label6.Top = label6.Top - ypoint;
                    TxtMRDocNo.Top = TxtMRDocNo.Top - ypoint;
                    ChkMRDocNo.Top = ChkMRDocNo.Top - ypoint;
                    label5.Top = label5.Top - ypoint;
                    TxtPORDocNo.Top = TxtPORDocNo.Top - ypoint;
                    ChkPORDocNo.Top = ChkPORDocNo.Top - ypoint;
                    label9.Top = label9.Top - ypoint;
                    TxtPODocNo.Top = TxtPODocNo.Top - ypoint;
                    ChkPODocNo.Top = ChkPODocNo.Top - ypoint;
                    panel2.Height = panel2.Height - ypoint;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptProcurementStatusShowDocInfoByGrp = Sm.GetParameterBoo("IsRptProcurementStatusShowDocInfoByGrp");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsRptProcurementStatusShowPurchaserInfo = Sm.GetParameterBoo("IsRptProcurementStatusShowPurchaserInfo");
            mIsRptProcurementStatusShowPORRemarkInfo = Sm.GetParameterBoo("IsRptProcurementStatusShowPORRemarkInfo");
            mIsRptProcurementStatusUsePODateFilter = Sm.GetParameterBoo("IsRptProcurementStatusUsePODateFilter");
            mIsUsePOForServiceDocNo = Sm.GetParameterBoo("IsUsePOForServiceDocNo");
            mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem = Sm.GetParameterBoo("IsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");
            mIsMRUseApprovalSheet = Sm.GetParameterBoo("IsMRUseApprovalSheet");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 69;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Local Code",
                        "Foreign Name",
                        "Department",

                        //6-10
                        (Doctitle=="IMS") ? "PR#" : "MR#",
                        "",
                        "DNo",
                        "Local#",
                        "Date",
                        
                        //11-15
                        "Requested"+Environment.NewLine+"Quantity",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Usage"+Environment.NewLine+"Date",
                        "",
                        
                        //16-20
                        "MR Status",
                        "Level",
                        "Checked By",
                        "PO Request#",
                        "",
                       
                        //21-25
                        "DNo",
                        "Local#",
                        "Date",
                        "PO Request"+Environment.NewLine+"Quantity",
                        "",

                        //26-30
                        "Status",
                        "Level",
                        "Checked By",
                        "PO#",
                        "",
                       
                        //31-35
                        "PO Status",
                        "Level",
                        "Checked By",
                        "DNo",
                        "Local#",

                        //36-40
                        "Date",
                        "Vendor",
                        "PO"+Environment.NewLine+"Quantity",
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "Estimated"+Environment.NewLine+"Received Date",
                        
                        //41-45
                        "Received#",
                        "",
                        "DNo",
                        "Date",
                        "Received"+Environment.NewLine+"Quantity",
                        
                        //46-50
                        "Returned#",
                        "DNo",
                        "Date",
                        "Returned"+Environment.NewLine+"Quantity",
                        "Remark",
                        
                        //51-55
                        "Site",
                        "Entity",
                        "PIC Name",
                        "Specification",
                        "Purchaser",

                        //56-60
                        "POR Remark",
                        "Estimated Received"+Environment.NewLine+"Date Revision",
                        "Revision"+Environment.NewLine+"Quantity",
                        "Request Type",
                        "MR Type",

                        //61-65
                        "MR Unit Price",
                        "MR Amount",
                        "Quotation#",
                        "POR Unit Price",
                        "POR Amount",

                        //66-68
                        "PO Unit Price",
                        "PO Amount",
                        "Arrival Time Status"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 250, 80, 150, 200,

                        //6-10
                        150, 20, 0, 130, 80,  

                        //11-15
                        80, 80, 60, 80, 20, 

                        //16-20
                        80, 80, 200, 150, 20, 

                        //21-25
                        0, 130, 80, 80, 20, 

                        //26-30
                        80, 80, 200, 150, 20, 

                        //31-35
                        80, 80, 200, 0, 130, 
                        
                        //36-40
                        80, 200, 80, 80, 100, 
                        
                        //41-45
                        130, 20, 0, 80, 80, 
                        
                        //46-50
                        130, 0, 80, 100, 400, 
                        
                        //51-55
                        200, 200, 150, 200, 80,

                        //56-60
                        400, 115, 100, 100, 100,

                        //61-65
                        150, 150, 180, 150, 150, 

                        //66-68
                        150, 150, 120
                    }
                );
            Grd1.Cols[53].Move(8);
            Grd1.Cols[55].Move(1);
            Sm.GrdColButton(Grd1, new int[] { 7, 15, 20, 25, 30, 42 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 24, 38, 39, 45, 49, 58, 61, 62, 64, 65, 66, 67 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 17, 27, 32 }, 11);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 14, 23, 36, 40, 44, 48, 57 });
            Sm.GrdColReadOnly(Grd1, new int[] { 
                0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 
                11, 12, 13, 14, 16, 17, 18, 19, 
                21, 22, 23, 24, 26, 27, 28, 29,  
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                41, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52, 53, 54, 55, 56, 57, 58,
                59, 60, 61, 62, 63, 64, 65, 66,
                67, 68
            }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 15, 17, 20, 25, 27, 30, 34, 42, 43, 47, 54, 55 }, false);
            
            if (!Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='PO' Limit 1;"))
                Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33 }, false);

            if (mIsRptProcurementStatusShowDocInfoByGrp)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblUser " +
                    "Where UserCode=@Param " +
                    "And GrpCode Is Not Null " +
                    "And Find_In_Set(GrpCode, " +
                    "(Select ParValue From TblParameter " +
                    "Where ParCode='GrpCodeForRptProcurementStatusShowDocInfo' " +
                    "And ParValue Is Not Null)); ", 
                    Gv.CurrentUserCode))
                    Sm.GrdColInvisible(Grd1, new int[] { 7, 20, 30 }, true);
            }

            if (mIsRptProcurementStatusShowPurchaserInfo)
            {
                Grd1.Cols[55].Visible = true;
            }

            if (mIsSiteMandatory)
            {
                Grd1.Cols[51].Move(6);
                Grd1.Cols[52].Move(7);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 51, 52 });

            if (mIsBOMShowSpecifications)
            {
                Grd1.Cols[54].Visible = true;
                Grd1.Cols[54].Move(3);
                Grd1.Cols[3].Move(2);
            }

            if (!mIsRptProcurementStatusShowPORRemarkInfo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 56 }, false);
            }

            if (!mIsRptProcurementUsePORevision)
                Sm.GrdColInvisible(Grd1, new int[] { 57, 58 });
            Grd1.Cols[57].Move(46);
            Grd1.Cols[58].Move(17);

            Grd1.Cols[60].Move(10);
            Grd1.Cols[59].Move(10);
            Grd1.Cols[62].Move(21);
            Grd1.Cols[61].Move(21);
            Grd1.Cols[65].Move(35);
            Grd1.Cols[64].Move(35);
            Grd1.Cols[63].Move(35);
            Grd1.Cols[36].Move(43);
            Grd1.Cols[67].Move(53);
            Grd1.Cols[66].Move(53);
            Grd1.Cols[68].Move(62);

            if (!mIsMRUseApprovalSheet) Sm.GrdColInvisible(Grd1, new int[] { 60 });
            //else Grd1.Cols[60].Move(1);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 15, 17, 25, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsFilterByDateInvalid(ref DtePODocDt1, ref DtePODocDt2)
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var lr = new List<Result>();

            try
            {                
                Process1(ref lr);
                if (lr.Count > 0)
                {
                    Process2(ref lr);
                    Process3(ref lr);
                    Process4(ref lr);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lr.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result> l)
        { 
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = " ";

            Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, "A.DocNo", false);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "C.ItCodeInternal", "C.ItName", "C.ForeignName" });
            Sm.FilterStr(ref Filter, ref cm, TxtPORDocNo.Text, "D.DocNo", "PORequestDocNo", false);
            Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "F.DocNo", "PODocNo", false);
            Sm.FilterStr(ref Filter, ref cm, TxtRecvVdDocNo.Text, "J.DocNo", "RecvVdDocNo", false);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "G.VdCode", true);

            SQL.AppendLine("Select G.CreateBy, B.ItCode, C.ItName, C.ItCodeInternal, C.Specification, C.ForeignName, N.DeptName, "); 
            SQL.AppendLine("A.DocNo As MaterialRequestDocNo, B.DNo As MaterialRequestDNo, "); 
            SQL.AppendLine("A.LocalDocNo As MaterialRequestLocalDocNo, "); 
            SQL.AppendLine("A.DocDt As MaterialRequestDocDt, B.Qty As MaterialRequestQty, C.PurchaseUomCode, B.UsageDt, ");

            SQL.AppendLine("C1.OptDesc As ReqTypeDesc, B.EstPrice, B.TotalPrice, ");
            if (mIsMRUseApprovalSheet) SQL.AppendLine("C2.OptDesc As MRTypeDesc, ");
            else SQL.AppendLine("Null As MRTypeDesc, ");
            SQL.AppendLine("D.QtDocNo, IfNull(E1.UPrice, 0.00) PORUPrice, IfNull((D.Qty * E1.UPrice), 0.00) PORAmount, ");
            SQL.AppendLine("If(G.DocNo Is Null, 0.00, IfNull(E1.UPrice, 0.00)) POUPrice, If(G.DocNo Is Null, 0.00, IfNull(((((100-IfNull(F.Discount, 0.00))/100)*(IfNull(F.Qty, 0.00) * IfNull(E1.UPrice, 0.00))) - IfNull(F.DiscountAmt, 0.00) + IfNull(F.RoundingValue, 0.00)), 0.00)) POAmt, ");

            SQL.AppendLine("B.Status As MaterialRequestStatus, ");
            SQL.AppendLine("D.DocNo As PORequestDocNo, D.DNo As PORequestDNo, E.LocalDocNo As PORequestLocalDocNo, ");
            SQL.AppendLine("E.DocDt As PORequestDocDt, D.Qty As PORequestQty, D.Status As PORequestStatus, ");
            SQL.AppendLine("F.DocNo As PODocNo, G.Status As POStatus, V.POApprovalStatus, W.POApprovalLevel, F.DNo As PODNo, G.LocalDocNo As POLocalDocNo, ");
            if (mIsRptProcurementUsePORevision)
                SQL.AppendLine("F.EstTimeArrival, G1.Qty As RevisionQty, ");
            else
                SQL.AppendLine("Null As EstTimeArrival, Null As RevisionQty, ");
            SQL.AppendLine("G.DocDt As PODocDt, I.VdName, F.Qty As POQty, F.EstRecvDt, ");
            SQL.AppendLine("J.DocNo As RecvVdDocNo, J.DNo As RecvVdDNo, "); 
            SQL.AppendLine("K.DocDt As RecvVdDocDt, J.QtyPurchase As RecvVdQty, ");
            SQL.AppendLine("L.DocNo As DOVdDocNo, L.DNo As DOVdDNo, ");
            SQL.AppendLine("M.DocDt As DOVdDocDt, Case When J.Qty=0 Then 0 Else L.Qty*(J.QtyPurchase/J.Qty) End As DOVdQty, ");
            SQL.AppendLine("A.Remark As HRemark, B.Remark As DRemark, ");
            SQL.AppendLine("O.MaterialRequestApprovalStatus, P.MaterialRequestApprovalLevel, Q.PORequestApprovalStatus, R.PORequestApprovalLevel, ");
            SQL.AppendLine("S.SiteName, U.EntName, X.UserName As PICName, E.Remark As PORRemark, F.DocNo As PODocNo2 ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.cancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Left Join TblOption C1 On C1.OptCat = 'ReqType' And C1.OptCode = A.ReqType ");
            if (mIsMRUseApprovalSheet) SQL.AppendLine("Left Join TblOption C2 On C2.OptCat = 'MRType' And C2.OptCode = A.MRType ");

            SQL.AppendLine("Left Join TblPORequestDtl D On B.DocNo=D.MaterialRequestDocNo And B.DNo=D.MaterialRequestDNo And D.CancelInd='N' ");
            SQL.AppendLine("Left Join TblPORequestHdr E On D.DocNo=E.DocNo  ");

            SQL.AppendLine("Left Join TblQtDtl E1 On D.QtDocNo = E1.DocNo And D.QtDNo = E1.DNo ");

            SQL.AppendLine("Left Join TblPODtl F On D.DocNo=F.PORequestDocNo And D.DNo=F.PORequestDNo And F.CancelInd='N' ");
            SQL.AppendLine("Left Join TblPOHdr G On F.DocNo=G.DocNo  ");
            if (mIsRptProcurementUsePORevision)
                SQL.AppendLine("Left Join TblPORevision G1 On F.DocNo = G1.PODocNo And F.DNo = G1.PODNo ");
            SQL.AppendLine("Left Join TblVendor I On G.VdCode=I.VdCode ");
            if (mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem)
                SQL.AppendLine("Left Join TblRecvVdDtl J On F.DocNo=J.PODocNo And F.DNo=J.PODNo And J.CancelInd='N' And J.RejectedInd='N' ");
            else
                SQL.AppendLine("Left Join TblRecvVdDtl J On F.DocNo=J.PODocNo And F.DNo=J.PODNo And J.CancelInd='N' ");
            SQL.AppendLine("Left Join TblRecvVdHdr K On J.DocNo=K.DocNo ");
            SQL.AppendLine("Left Join TblDOVdDtl L On J.DocNo=L.RecvVdDocNo And J.DNo=L.RecvVdDNo And L.CancelInd='N' ");
            SQL.AppendLine("Left Join TblDOVdHdr M On L.DocNo=M.DocNo ");
            SQL.AppendLine("Left Join TblDepartment N On A.DeptCode=N.DeptCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, ");
            SQL.AppendLine("    Group_Concat(T2.UserName, ' on ', Concat(Substring(ifnull(T1.LastUpDt,T1.CreateDt), 7, 2), '/', Substring(ifnull(T1.LastUpDt,T1.CreateDt), 5, 2), '/', Left(ifnull(T1.LastUpDt,T1.CreateDt), 4)), ' (', ");
            SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End, ");
            SQL.AppendLine("    ')' Order By T1.ApprovalDNo)  ");
            SQL.AppendLine("    As MaterialRequestApprovalStatus ");
            SQL.AppendLine("    From TblDocApproval T1 ");
            SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode = T2.UserCode ");
            SQL.AppendLine("    Where (T1.DocType='MaterialRequest' or T1.DocType='MaterialRequest2') ");
            SQL.AppendLine("    And Exists(Select DocNo From TblMaterialRequestHdr Where (DocDt Between @DocDt1 And @DocDt2) And DocNo=T1.DocNo) ");
            SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
            SQL.AppendLine(") O On B.DocNo=O.DocNo And B.DNo=O.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, Count(T1.DocNo) As MaterialRequestApprovalLevel ");
            SQL.AppendLine("    From TblDocApproval T1 ");
            SQL.AppendLine("    Where (T1.DocType='MaterialRequest' or T1.DocType='MaterialRequest2') ");
            SQL.AppendLine("    And Exists(Select DocNo From TblMaterialRequestHdr Where (DocDt Between @DocDt1 And @DocDt2) And DocNo=T1.DocNo) ");
            SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
            SQL.AppendLine(") P On B.DocNo=P.DocNo And B.DNo=P.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, ");
            SQL.AppendLine("    Group_Concat(T2.UserName, ' on ', Concat(Substring(ifnull(T1.LastUpDt,T1.CreateDt), 7, 2), '/', Substring(ifnull(T1.LastUpDt,T1.CreateDt), 5, 2), '/', Left(ifnull(T1.LastUpDt,T1.CreateDt), 4)), ' (', ");
            SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End, ");
            SQL.AppendLine("    ')' Order By T1.ApprovalDNo)  ");
            SQL.AppendLine("    As PORequestApprovalStatus ");
            SQL.AppendLine("    From TblDocApproval T1 ");
            SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode = T2.UserCode ");
            SQL.AppendLine("    Where T1.DocType='PORequest' ");
            SQL.AppendLine("    And Exists(Select X1.DocNo From TblPORequestDtl X1, TblMaterialRequestHdr X2 Where X1.MaterialRequestDocNo=X2.DocNo And (X2.DocDt Between @DocDt1 And @DocDt2) And X1.DocNo=T1.DocNo) ");
            SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
            SQL.AppendLine(") Q On D.DocNo=Q.DocNo And D.DNo=Q.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, Count(T1.DocNo) As PORequestApprovalLevel ");
            SQL.AppendLine("    From TblDocApproval T1 ");
            SQL.AppendLine("    Where T1.DocType='PORequest' ");
            SQL.AppendLine("    And Exists(Select X1.DocNo From TblPORequestDtl X1, TblMaterialRequestHdr X2 Where X1.MaterialRequestDocNo=X2.DocNo And (X2.DocDt Between @DocDt1 And @DocDt2) And X1.DocNo=T1.DocNo) ");
            SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
            SQL.AppendLine(") R On D.DocNo=R.DocNo And D.DNo=R.DNo ");
            SQL.AppendLine("Left Join TblSite S On A.SiteCode=S.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter T On S.ProfitCenterCode=T.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity U On T.EntCode=U.EntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat( ");
            SQL.AppendLine("        T2.UserName, ' on ', Concat(Substring(ifnull(T1.LastUpDt,T1.CreateDt), 7, 2), '/', Substring(ifnull(T1.LastUpDt,T1.CreateDt), 5, 2), '/', Left(ifnull(T1.LastUpDt,T1.CreateDt), 4)), ' (', ");
            SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End, ");
            SQL.AppendLine("    ')' Order By T1.ApprovalDNo)  ");
            SQL.AppendLine("    As POApprovalStatus ");
            SQL.AppendLine("    From TblDocApproval T1 ");
            SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode=T2.UserCode ");
            SQL.AppendLine("    Where T1.DocType='PO' ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblPOHdr X1, TblPODtl X2, TblPORequestDtl X3, TblMaterialRequestHdr X4 ");
            SQL.AppendLine("        Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        And X2.PORequestDocNo=X3.DocNo ");
            SQL.AppendLine("        And X2.PORequestDNo=X3.DNo ");
            SQL.AppendLine("        And X3.MaterialRequestDocNo=X4.DocNo ");
            SQL.AppendLine("        And X4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And X1.DocNo=T1.DocNo ");
            SQL.AppendLine("        And X2.CancelInd='N' ");
            SQL.AppendLine("        And X1.Status In ('O', 'A') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") V On G.DocNo=V.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNo, Count(1) As POApprovalLevel ");
            SQL.AppendLine("    From TblDocApproval T ");
            SQL.AppendLine("    Where T.DocType='PO' ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblPOHdr X1, TblPODtl X2, TblPORequestDtl X3, TblMaterialRequestHdr X4 ");
            SQL.AppendLine("        Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        And X2.PORequestDocNo=X3.DocNo ");
            SQL.AppendLine("        And X2.PORequestDNo=X3.DNo ");
            SQL.AppendLine("        And X3.MaterialRequestDocNo=X4.DocNo ");
            SQL.AppendLine("        And X4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And X1.DocNo=T.DocNo ");
            SQL.AppendLine("        And X2.CancelInd='N' ");
            SQL.AppendLine("        And X1.Status In ('O', 'A') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T.DocNo ");
            SQL.AppendLine(") W On G.DocNo=W.DocNo ");
            SQL.AppendLine("Left Join TblUser X On X.UserCode=A.PICCode ");
            if (mIsRptProcurementStatusUsePODateFilter && ChkPODocDt.Checked)
                SQL.AppendLine("Where (G.DocDt Between @DocDt3 And @DocDt4)");
            else
                SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mIsUsePOForServiceDocNo)
            {
                SQL.AppendLine("AND B.MaterialRequestServiceDocNo IS NULL ");
            }
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(Filter);
            if (mIsUsePOForServiceDocNo)
            {
                SQL.AppendLine("Union All");

                SQL.AppendLine("Select G.CreateBy, B.ItCode, C.ItName, C.ItCodeInternal, C.Specification, C.ForeignName, N.DeptName, ");
                SQL.AppendLine("Y.DocNo As MaterialRequestDocNo, Y.DNo As MaterialRequestDNo, ");
                SQL.AppendLine("A.LocalDocNo As MaterialRequestLocalDocNo, ");
                SQL.AppendLine("A.DocDt As MaterialRequestDocDt, B.Qty As MaterialRequestQty, C.PurchaseUomCode, B.UsageDt, ");

                SQL.AppendLine("C1.OptDesc As ReqTypeDesc, B.EstPrice, B.TotalPrice, ");
                if (mIsMRUseApprovalSheet) SQL.AppendLine("C2.OptDesc As MRTypeDesc, ");
                else SQL.AppendLine("Null As MRTypeDesc, ");
                SQL.AppendLine("D.QtDocNo, IfNull(E1.UPrice, 0.00) PORUPrice, IfNull((D.Qty * E1.UPrice), 0.00) PORAmount, ");
                SQL.AppendLine("If(G.DocNo Is Null, 0.00, IfNull(E1.UPrice, 0.00)) POUPrice, If(G.DocNo Is Null, 0.00, IfNull(((((100-IfNull(F.Discount, 0.00))/100)*(IfNull(F.Qty, 0.00) * IfNull(E1.UPrice, 0.00))) - IfNull(F.DiscountAmt, 0.00) + IfNull(F.RoundingValue, 0.00)), 0.00)) POAmt, ");

                SQL.AppendLine("B.Status As MaterialRequestStatus, ");
                SQL.AppendLine("D.DocNo As PORequestDocNo, D.DNo As PORequestDNo, E.LocalDocNo As PORequestLocalDocNo, ");
                SQL.AppendLine("E.DocDt As PORequestDocDt, D.Qty As PORequestQty, D.Status As PORequestStatus, ");
                SQL.AppendLine("F.DocNo As PODocNo, G.Status As POStatus, V.POApprovalStatus, W.POApprovalLevel, F.DNo As PODNo, G.LocalDocNo As POLocalDocNo, ");
                if (mIsRptProcurementUsePORevision)
                    SQL.AppendLine("F.EstTimeArrival, G1.Qty As RevisionQty, ");
                else
                    SQL.AppendLine("Null As EstTimeArrival, Null As RevisionQty, ");
                SQL.AppendLine("G.DocDt As PODocDt, I.VdName, F.Qty As POQty, F.EstRecvDt, ");
                SQL.AppendLine("J.DocNo As RecvVdDocNo, J.DNo As RecvVdDNo, ");
                SQL.AppendLine("K.DocDt As RecvVdDocDt, J.QtyPurchase As RecvVdQty, ");
                SQL.AppendLine("L.DocNo As DOVdDocNo, L.DNo As DOVdDNo, ");
                SQL.AppendLine("M.DocDt As DOVdDocDt, Case When J.Qty=0 Then 0 Else L.Qty*(J.QtyPurchase/J.Qty) End As DOVdQty, ");
                SQL.AppendLine("A.Remark As HRemark, B.Remark As DRemark, ");
                SQL.AppendLine("O.MaterialRequestApprovalStatus, P.MaterialRequestApprovalLevel, Q.PORequestApprovalStatus, R.PORequestApprovalLevel, ");
                SQL.AppendLine("S.SiteName, U.EntName, X.UserName As PICName, E.Remark As PORRemark, B.DocNo As PODocNo2 ");
                SQL.AppendLine("From TblMaterialRequestHdr A ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.cancelInd='N' ");
                SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }

                SQL.AppendLine("Left Join TblOption C1 On C1.OptCat = 'ReqType' And C1.OptCode = A.DocType ");
                if (mIsMRUseApprovalSheet) SQL.AppendLine("Left Join TblOption C2 On C2.OptCat = 'MRType' And C2.OptCode = A.MRType ");

                SQL.AppendLine("Left Join TblPORequestDtl D On B.DocNo=D.MaterialRequestDocNo And B.DNo=D.MaterialRequestDNo And D.CancelInd='N' ");
                SQL.AppendLine("Left Join TblPORequestHdr E On D.DocNo=E.DocNo  ");

                SQL.AppendLine("Left Join TblQtDtl E1 On D.QtDocNo = E1.DocNo And D.QtDNo = E1.DNo ");

                SQL.AppendLine("Left Join TblPODtl F On D.DocNo=F.PORequestDocNo And D.DNo=F.PORequestDNo And F.CancelInd='N' ");
                SQL.AppendLine("Left Join TblPOHdr G On F.DocNo=G.DocNo  ");
                if (mIsRptProcurementUsePORevision)
                    SQL.AppendLine("Left Join TblPORevision G1 On F.DocNo = G1.PODocNo And F.DNo = G1.PODNo ");
                SQL.AppendLine("Left Join TblVendor I On G.VdCode=I.VdCode ");                
                if (mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem)
                    SQL.AppendLine("Left Join TblRecvVdDtl J On F.DocNo=J.PODocNo And F.DNo=J.PODNo And J.CancelInd='N' And J.RejectedInd='N' ");
                else
                    SQL.AppendLine("Left Join TblRecvVdDtl J On F.DocNo=J.PODocNo And F.DNo=J.PODNo And J.CancelInd='N' ");
                SQL.AppendLine("Left Join TblRecvVdHdr K On J.DocNo=K.DocNo ");
                SQL.AppendLine("Left Join TblDOVdDtl L On J.DocNo=L.RecvVdDocNo And J.DNo=L.RecvVdDNo And L.CancelInd='N' ");
                SQL.AppendLine("Left Join TblDOVdHdr M On L.DocNo=M.DocNo ");
                SQL.AppendLine("Left Join TblDepartment N On A.DeptCode=N.DeptCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.DNo, ");
                SQL.AppendLine("    Group_Concat(T2.UserName, ' on ', Concat(Substring(ifnull(T1.LastUpDt,T1.CreateDt), 7, 2), '/', Substring(ifnull(T1.LastUpDt,T1.CreateDt), 5, 2), '/', Left(ifnull(T1.LastUpDt,T1.CreateDt), 4)), ' (', ");
                SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End, ");
                SQL.AppendLine("    ')' Order By T1.ApprovalDNo)  ");
                SQL.AppendLine("    As MaterialRequestApprovalStatus ");
                SQL.AppendLine("    From TblDocApproval T1 ");
                SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode = T2.UserCode ");
                SQL.AppendLine("    Where (T1.DocType='MaterialRequest' or T1.DocType='MaterialRequest2') ");
                SQL.AppendLine("    And Exists(Select DocNo From TblMaterialRequestHdr Where (DocDt Between @DocDt1 And @DocDt2) And DocNo=T1.DocNo) ");
                SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
                SQL.AppendLine(") O On B.DocNo=O.DocNo And B.DNo=O.DNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.DNo, Count(T1.DocNo) As MaterialRequestApprovalLevel ");
                SQL.AppendLine("    From TblDocApproval T1 ");
                SQL.AppendLine("    Where (T1.DocType='MaterialRequest' or T1.DocType='MaterialRequest2') ");
                SQL.AppendLine("    And Exists(Select DocNo From TblMaterialRequestHdr Where (DocDt Between @DocDt1 And @DocDt2) And DocNo=T1.DocNo) ");
                SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
                SQL.AppendLine(") P On B.DocNo=P.DocNo And B.DNo=P.DNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.DNo, ");
                SQL.AppendLine("    Group_Concat(T2.UserName, ' on ', Concat(Substring(ifnull(T1.LastUpDt,T1.CreateDt), 7, 2), '/', Substring(ifnull(T1.LastUpDt,T1.CreateDt), 5, 2), '/', Left(ifnull(T1.LastUpDt,T1.CreateDt), 4)), ' (', ");
                SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End, ");
                SQL.AppendLine("    ')' Order By T1.ApprovalDNo)  ");
                SQL.AppendLine("    As PORequestApprovalStatus ");
                SQL.AppendLine("    From TblDocApproval T1 ");
                SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode = T2.UserCode ");
                SQL.AppendLine("    Where T1.DocType='PORequest' ");
                SQL.AppendLine("    And Exists(Select X1.DocNo From TblPORequestDtl X1, TblMaterialRequestHdr X2 Where X1.MaterialRequestDocNo=X2.DocNo And (X2.DocDt Between @DocDt1 And @DocDt2) And X1.DocNo=T1.DocNo) ");
                SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
                SQL.AppendLine(") Q On D.DocNo=Q.DocNo And D.DNo=Q.DNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T1.DNo, Count(T1.DocNo) As PORequestApprovalLevel ");
                SQL.AppendLine("    From TblDocApproval T1 ");
                SQL.AppendLine("    Where T1.DocType='PORequest' ");
                SQL.AppendLine("    And Exists(Select X1.DocNo From TblPORequestDtl X1, TblMaterialRequestHdr X2 Where X1.MaterialRequestDocNo=X2.DocNo And (X2.DocDt Between @DocDt1 And @DocDt2) And X1.DocNo=T1.DocNo) ");
                SQL.AppendLine("    Group By T1.DocNo, T1.DNo ");
                SQL.AppendLine(") R On D.DocNo=R.DocNo And D.DNo=R.DNo ");
                SQL.AppendLine("Left Join TblSite S On A.SiteCode=S.SiteCode ");
                SQL.AppendLine("Left Join TblProfitCenter T On S.ProfitCenterCode=T.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblEntity U On T.EntCode=U.EntCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, ");
                SQL.AppendLine("    Group_Concat( ");
                SQL.AppendLine("        T2.UserName, ' on ', Concat(Substring(ifnull(T1.LastUpDt,T1.CreateDt), 7, 2), '/', Substring(ifnull(T1.LastUpDt,T1.CreateDt), 5, 2), '/', Left(ifnull(T1.LastUpDt,T1.CreateDt), 4)), ' (', ");
                SQL.AppendLine("        Case T1.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End, ");
                SQL.AppendLine("    ')' Order By T1.ApprovalDNo)  ");
                SQL.AppendLine("    As POApprovalStatus ");
                SQL.AppendLine("    From TblDocApproval T1 ");
                SQL.AppendLine("    Inner Join TblUser T2 On T1.UserCode=T2.UserCode ");
                SQL.AppendLine("    Where T1.DocType='PO' ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblPOHdr X1, TblPODtl X2, TblPORequestDtl X3, TblMaterialRequestHdr X4 ");
                SQL.AppendLine("        Where X1.DocNo=X2.DocNo ");
                SQL.AppendLine("        And X2.PORequestDocNo=X3.DocNo ");
                SQL.AppendLine("        And X2.PORequestDNo=X3.DNo ");
                SQL.AppendLine("        And X3.MaterialRequestDocNo=X4.DocNo ");
                SQL.AppendLine("        And X4.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        And X1.DocNo=T1.DocNo ");
                SQL.AppendLine("        And X2.CancelInd='N' ");
                SQL.AppendLine("        And X1.Status In ('O', 'A') ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By T1.DocNo ");
                SQL.AppendLine(") V On G.DocNo=V.DocNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T.DocNo, Count(1) As POApprovalLevel ");
                SQL.AppendLine("    From TblDocApproval T ");
                SQL.AppendLine("    Where T.DocType='PO' ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblPOHdr X1, TblPODtl X2, TblPORequestDtl X3, TblMaterialRequestHdr X4 ");
                SQL.AppendLine("        Where X1.DocNo=X2.DocNo ");
                SQL.AppendLine("        And X2.PORequestDocNo=X3.DocNo ");
                SQL.AppendLine("        And X2.PORequestDNo=X3.DNo ");
                SQL.AppendLine("        And X3.MaterialRequestDocNo=X4.DocNo ");
                SQL.AppendLine("        And X4.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        And X1.DocNo=T.DocNo ");
                SQL.AppendLine("        And X2.CancelInd='N' ");
                SQL.AppendLine("        And X1.Status In ('O', 'A') ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By T.DocNo ");
                SQL.AppendLine(") W On G.DocNo=W.DocNo ");
                SQL.AppendLine("Left Join TblUser X On X.UserCode=A.PICCode ");
                SQL.AppendLine("INNER JOIN tblmaterialrequestservicedtl Y ON B.MaterialRequestServiceDocNo = Y.DocNo AND B.MaterialRequestServiceDNo = Y.DNo ");
                if (mIsRptProcurementStatusUsePODateFilter && ChkPODocDt.Checked)
                    SQL.AppendLine("Where (G.DocDt Between @DocDt3 And @DocDt4)");
                else
                    SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                if (mIsFilterByDept)
                {
                    SQL.AppendLine("And A.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine(Filter);
            }
            SQL.AppendLine(";");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParamDt(ref cm, "@DocDt3", Sm.GetDte(DtePODocDt1));
            Sm.CmParamDt(ref cm, "@DocDt4", Sm.GetDte(DtePODocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "ItCodeInternal", "ForeignName", "DeptName", "MaterialRequestDocNo", 
                    
                    //6-10
                    "MaterialRequestDNo", "MaterialRequestLocalDocNo", "MaterialRequestDocDt", "MaterialRequestQty", "PurchaseUomCode", 
                    
                    //11-15
                    "UsageDt", "MaterialRequestStatus", "PORequestDocNo", "PORequestDNo", "PORequestLocalDocNo", 
                    
                    //16-20
                    "PORequestDocDt", "PORequestQty", "PORequestStatus", "PODocNo", "POStatus",
                    
                    //21-25
                    "POApprovalStatus", "POApprovalLevel", "PODNo", "POLocalDocNo", "PODocDt", 
                    
                    //26-30
                    "VdName", "POQty", "EstRecvDt", "RecvVdDocNo", "RecvVdDNo", 
                    
                    //31-35
                    "RecvVdDocDt", "RecvVdQty", "DOVdDocNo", "DOVdDNo", "DOVdDocDt", 
                    
                    //36-40
                    "DOVdQty", "HRemark", "DRemark", "MaterialRequestApprovalStatus", "MaterialRequestApprovalLevel", 
                    
                    //41-45
                    "PORequestApprovalStatus", "PORequestApprovalLevel", "SiteName", "EntName", "PICName",

                    //46-50
                    "Specification", "CreateBy", "PORRemark", "PODocNo2", "EstTimeArrival",  

                    //51-55
                    "RevisionQty", "ReqTypeDesc", "EstPrice", "TotalPrice", "MRTypeDesc",

                    //56-60
                    "QtDocNo", "PORUPrice", "PORAmount", "POUPrice", "POAmt"
            });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            ItCode= Sm.DrStr(dr, c[0]),
                            ItName= Sm.DrStr(dr, c[1]),
                            ItCodeInternal= Sm.DrStr(dr, c[2]),
                            ForeignName= Sm.DrStr(dr, c[3]),
                            DeptName= Sm.DrStr(dr, c[4]),
                            MaterialRequestDocNo= Sm.DrStr(dr, c[5]),
                            MaterialRequestDNo= Sm.DrStr(dr, c[6]),
                            MaterialRequestLocalDocNo= Sm.DrStr(dr, c[7]),
                            MaterialRequestDt= Sm.DrStr(dr, c[8]),
                            MaterialRequestQty= Sm.DrDec(dr, c[9]),
                            PurchaseUomCode= Sm.DrStr(dr, c[10]),
                            UsageDt= Sm.DrStr(dr, c[11]),
                            MaterialRequestStatus= (Sm.DrStr(dr, c[12])=="A"?"Approved":(Sm.DrStr(dr, c[12])=="C"?"Cancelled":"Outstanding")),
                            PORequestDocNo= Sm.DrStr(dr, c[13]),
                            PORequestDNo= Sm.DrStr(dr, c[14]),
                            PORequestLocalDocNo= Sm.DrStr(dr, c[15]),
                            PORequestDt= Sm.DrStr(dr, c[16]),
                            PORequestQty= Sm.DrDec(dr, c[17]),
                            PORequestStatus= (Sm.DrStr(dr, c[18])=="A"?"Approved":(Sm.DrStr(dr, c[18])=="C"?"Cancelled":"Outstanding")),
                            PODocNo= Sm.DrStr(dr, c[19]),
                            POStatus = (Sm.DrStr(dr, c[20]) == "A" ? "Approved" : (Sm.DrStr(dr, c[20]) == "C" ? "Cancelled" : "Outstanding")),
                            POApprovalStatus = Sm.DrStr(dr, c[21]),
                            POApprovalLevel = Sm.DrInt(dr, c[22]),
                            PODNo= Sm.DrStr(dr, c[23]),
                            POLocalDocNo= Sm.DrStr(dr, c[24]),
                            PODt= Sm.DrStr(dr, c[25]),
                            VdName= Sm.DrStr(dr, c[26]),
                            POQty= Sm.DrDec(dr, c[27]),
                            EstRecvDt= Sm.DrStr(dr, c[28]),
                            RecvVdDocNo= Sm.DrStr(dr, c[29]),
                            RecvVdDNo= Sm.DrStr(dr, c[30]),
                            RecvVdDt= Sm.DrStr(dr, c[31]),
                            RecvVdQty = Sm.DrDec(dr, c[32]),
                            DOVdDocNo= Sm.DrStr(dr, c[33]),
                            DOVdDNo= Sm.DrStr(dr, c[34]),
                            DOVdDt= Sm.DrStr(dr, c[35]),
                            DOVdQty= Sm.DrDec(dr, c[36]),
                            Remark = Sm.DrStr(dr, c[37]).Length == 0 ? 
                                Sm.DrStr(dr, c[38]) : 
                                string.Concat(Sm.DrStr(dr, c[37])," ",Sm.DrStr(dr, c[38])).Trim(),
                            MaterialRequestCancelQty = 0m,
                            POCancelQty = 0m,
                            MaterialRequestApprovalStatus = Sm.DrStr(dr, c[39]),
                            MaterialRequestApprovalLevel = Sm.DrInt(dr, c[40]),
                            PORequestApprovalStatus = Sm.DrStr(dr, c[41]),
                            PORequestApprovalLevel = Sm.DrInt(dr, c[42]),
                            SiteName = Sm.DrStr(dr, c[43]),
                            EntName = Sm.DrStr(dr, c[44]),
                            PICName = Sm.DrStr(dr, c[45]),
                            Specification = Sm.DrStr(dr, c[46]),
                            CreateBy = Sm.DrStr(dr, c[47]),
                            PORRemark = Sm.DrStr(dr, c[48]),
                            PODocNo2 = Sm.DrStr(dr, c[49]),
                            EstTimeArrival = Sm.DrStr(dr, c[50]),
                            RevisionQty = Sm.DrDec(dr, c[51]),
                            ReqType = Sm.DrStr(dr, c[52]),
                            EstPrice = Sm.DrDec(dr, c[53]),
                            TotalPrice = Sm.DrDec(dr, c[54]),
                            MRTypeDesc = Sm.DrStr(dr, c[55]),
                            QtDocNo = Sm.DrStr(dr, c[56]),
                            PORUPrice = Sm.DrDec(dr, c[57]),
                            PORAmount = Sm.DrDec(dr, c[58]),
                            POUPrice = Sm.DrDec(dr, c[59]),
                            POAmt = Sm.DrDec(dr, c[60])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var Filter = new StringBuilder();
            
            for (int i = 0; i < l.Count; i++)
            {
                if (i!=0) Filter.AppendLine(" Or ");
                Filter.AppendLine("(MRDocNo=@DocNo" + i.ToString() + " And MRDNo=@DNo" + i.ToString() + ") ");
                Sm.CmParam<String>(ref cm, "@DocNo" + i.ToString(), l[i].MaterialRequestDocNo);
                Sm.CmParam<String>(ref cm, "@DNo" + i.ToString(), l[i].MaterialRequestDNo);
            }
        
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = 
                    "Select MRDocNo As DocNo, MRDNo As DNo, Qty " +
                    "From TblMRQtyCancel Where CancelInd='N' And ( " + Filter.ToString() + ");";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Qty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var r in l.Where(x =>
                            string.Compare(x.MaterialRequestDocNo, Sm.DrStr(dr, 0)) == 0 &&
                            string.Compare(x.MaterialRequestDNo, Sm.DrStr(dr, 1)) == 0))
                            r.MaterialRequestCancelQty += Sm.DrDec(dr, 2);
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var Filter = new StringBuilder();

            for (int i = 0; i < l.Count; i++)
            {
                if (i != 0) Filter.AppendLine(" Or ");
                Filter.AppendLine("(PODocNo=@DocNo" + i.ToString() + " And PODNo=@DNo" + i.ToString() + ") ");
                Sm.CmParam<String>(ref cm, "@DocNo" + i.ToString(), l[i].PODocNo);
                Sm.CmParam<String>(ref cm, "@DNo" + i.ToString(), l[i].PODNo);
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText =
                    "Select PODocNo As DocNo, PODNo As DNo, Qty " +
                    "From TblPOQtyCancel Where CancelInd='N' And ( " + Filter.ToString() + ");";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Qty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var r in l.Where(x =>
                            string.Compare(x.PODocNo, Sm.DrStr(dr, 0)) == 0 &&
                            string.Compare(x.PODNo, Sm.DrStr(dr, 1)) == 0))
                            r.POCancelQty += Sm.DrDec(dr, 2);
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Result> l)
        {
            var IsVisible = new bool[5] { false, false, false, false, false };

            //l.OrderBy(x => x.ItName);
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;

                r.Cells[1].Value = l[i].ItCode;
                r.Cells[2].Value = l[i].ItName;
                r.Cells[3].Value = l[i].ItCodeInternal;
                r.Cells[4].Value = l[i].ForeignName;
                r.Cells[5].Value = l[i].DeptName;
                r.Cells[6].Value = l[i].MaterialRequestDocNo;
                r.Cells[8].Value = l[i].MaterialRequestDNo;
                r.Cells[9].Value = l[i].MaterialRequestLocalDocNo;
                SetDt(r, 10, l[i].MaterialRequestDt);
                r.Cells[11].Value = l[i].MaterialRequestQty;
                r.Cells[12].Value = l[i].MaterialRequestCancelQty;
                r.Cells[13].Value = l[i].PurchaseUomCode;
                SetDt(r, 14, l[i].UsageDt);
                r.Cells[16].Value = l[i].MaterialRequestStatus;
                r.Cells[17].Value = l[i].MaterialRequestApprovalLevel;
                r.Cells[18].Value = l[i].MaterialRequestApprovalStatus;
                r.Cells[19].Value = l[i].PORequestDocNo;
                r.Cells[21].Value = l[i].PORequestDNo;
                r.Cells[22].Value = l[i].PORequestLocalDocNo;
                SetDt(r, 23, l[i].PORequestDt);
                r.Cells[24].Value = l[i].PORequestQty;
                r.Cells[26].Value = l[i].PORequestStatus;
                r.Cells[27].Value = l[i].PORequestApprovalLevel;
                r.Cells[28].Value = l[i].PORequestApprovalStatus;
                r.Cells[29].Value = l[i].PODocNo2;
                r.Cells[31].Value = l[i].POStatus;
                r.Cells[32].Value = l[i].POApprovalLevel;
                r.Cells[33].Value = l[i].POApprovalStatus;
                r.Cells[34].Value = l[i].PODNo;
                r.Cells[35].Value = l[i].POLocalDocNo;
                SetDt(r, 36, l[i].PODt);
                r.Cells[37].Value = l[i].VdName;
                r.Cells[38].Value = l[i].POQty;
                r.Cells[39].Value = l[i].POCancelQty;
                SetDt(r, 40, l[i].EstRecvDt);
                r.Cells[41].Value = l[i].RecvVdDocNo;
                r.Cells[43].Value = l[i].RecvVdDNo;
                SetDt(r, 44, l[i].RecvVdDt);
                r.Cells[45].Value = l[i].RecvVdQty;
                r.Cells[46].Value = l[i].DOVdDocNo;
                r.Cells[47].Value = l[i].DOVdDNo;
                SetDt(r, 48, l[i].DOVdDt);
                r.Cells[49].Value = l[i].DOVdQty;
                r.Cells[50].Value = l[i].Remark;
                r.Cells[51].Value = l[i].SiteName;
                r.Cells[52].Value = l[i].EntName;
                r.Cells[53].Value = l[i].PICName;
                r.Cells[54].Value = l[i].Specification;
                r.Cells[55].Value = l[i].CreateBy;
                r.Cells[56].Value = l[i].PORRemark;
                SetDt(r, 57, l[i].EstTimeArrival);
                r.Cells[58].Value = l[i].RevisionQty;
                if (!IsVisible[0] && l[i].ItCodeInternal.Length > 0) IsVisible[0] = true;
                if (!IsVisible[1] && l[i].ForeignName.Length > 0) IsVisible[1] = true;
                if (!IsVisible[2] && l[i].MaterialRequestLocalDocNo.Length > 0) IsVisible[2] = true;
                if (!IsVisible[3] && l[i].PORequestLocalDocNo.Length > 0) IsVisible[3] = true;
                if (!IsVisible[4] && l[i].POLocalDocNo.Length > 0) IsVisible[4] = true;

                r.Cells[59].Value = l[i].ReqType;
                r.Cells[60].Value = l[i].MRTypeDesc;
                r.Cells[61].Value = l[i].EstPrice;
                r.Cells[62].Value = l[i].TotalPrice;
                r.Cells[63].Value = l[i].QtDocNo;
                r.Cells[64].Value = l[i].PORUPrice;
                r.Cells[65].Value = l[i].PORAmount;
                r.Cells[66].Value = l[i].POUPrice;
                r.Cells[67].Value = l[i].POAmt;

                if (l[i].RecvVdDt.Length > 0 && l[i].EstRecvDt.Length > 0)
                {
                    string ArrivalTime = string.Empty;

                    if (l[i].RecvVdDt == l[i].EstRecvDt) ArrivalTime = "On Time";
                    else
                    {
                        int RecvDtInt = Int32.Parse(l[i].RecvVdDt);
                        int EstRecvDtInt = Int32.Parse(l[i].EstRecvDt);

                        if (RecvDtInt < EstRecvDtInt) ArrivalTime = "Early";
                        else ArrivalTime = "Late";
                    }

                    r.Cells[68].Value = ArrivalTime;
                }
            }

            Grd1.Cols[3].Visible = IsVisible[0];
            Grd1.Cols[4].Visible = IsVisible[1];
            Grd1.Cols[9].Visible = IsVisible[2];
            Grd1.Cols[22].Visible = IsVisible[3];
            Grd1.Cols[35].Visible = IsVisible[4];

            //Grd1.Rows.AutoHeight();
            Grd1.EndUpdate();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetDt(iGRow r, int c, string Dt)
        {
            if (Dt.Length == 0)
                r.Cells[c].Value = string.Empty;
            else
                r.Cells[c].Value = Sm.ConvertDate(Dt);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 15) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 25) ShowPORApprovalInfo(e.RowIndex);
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPORequest(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
            if (e.ColIndex == 30 && Sm.GetGrdStr(Grd1, e.RowIndex, 29).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 29);
                f.ShowDialog();
            }
            if (e.ColIndex == 42 && Sm.GetGrdStr(Grd1, e.RowIndex, 41).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 41);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 15) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 25) ShowPORApprovalInfo(e.RowIndex);
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmPORequest(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
            if (e.ColIndex == 30 && Sm.GetGrdStr(Grd1, e.RowIndex, 29).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 29);
                f.ShowDialog();
            }
            if (e.ColIndex == 42 && Sm.GetGrdStr(Grd1, e.RowIndex, 41).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 41);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void ShowMRApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then   "); 
            SQL.AppendLine("Concat(Substring(LastUpDt, 9, 2), ':', Substring(LastUpDt, 11, 2))  "); 
            SQL.AppendLine("Else Null End As LastUpTime,  "); 
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='MaterialRequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 8));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "StatusDesc", "LastUpDt", "LastUpTime", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Time : " + Sm.DrStr(dr, 3) + Environment.NewLine + 
                            "Remark : " + Sm.DrStr(dr, 4) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private void ShowPORApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='PORequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 21));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[]{ "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        #endregion

        #endregion

        #region Event

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, (Doctitle=="IMS") ? "Purchase Request#" : "Material Request#");
        }

        private void ChkPORDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO Request#");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DtePODocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DtePODocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkPODocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "PO Date");
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPORDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Purchase Order#");
        }

        private void TxtRecvVdDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRecvVdDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        #endregion

        #region Class

        private class Result
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string ForeignName { get; set; }
            public string DeptName { get; set; }
            public string MaterialRequestDocNo { get; set; }
            public string MaterialRequestDNo { get; set; }
            public string MaterialRequestLocalDocNo { get; set; }
            public string MaterialRequestDt { get; set; }
            public decimal MaterialRequestQty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string UsageDt { get; set; }
            public string MaterialRequestStatus { get; set; }
            public string PORequestDocNo { get; set; }
            public string PORequestDNo { get; set; }
            public string PORequestLocalDocNo { get; set; }
            public string PORequestDt { get; set; }
            public decimal PORequestQty { get; set; }
            public string PORequestStatus { get; set; }
            public string PODocNo { get; set; }
            public string POStatus { get; set; }
            public string POApprovalStatus { get; set; }
            public int POApprovalLevel { get; set; }
            public string PODNo { get; set; }
            public string POLocalDocNo { get; set; }
            public string PODt { get; set; }
            public string VdName { get; set; }
            public decimal POQty { get; set; }
            public string EstRecvDt { get; set; }
            public string RecvVdDocNo { get; set; }
            public string RecvVdDNo { get; set; }
            public string RecvVdDt { get; set; }
            public decimal RecvVdQty { get; set; }
            public string DOVdDocNo { get; set; }
            public string DOVdDNo { get; set; }
            public string DOVdDt { get; set; }
            public decimal DOVdQty { get; set; }
            public string Remark { get; set; }
            public decimal MaterialRequestCancelQty { get; set; }
            public decimal POCancelQty { get; set; }
            public int MaterialRequestApprovalLevel { get; set; }
            public string MaterialRequestApprovalStatus { get; set; }
            public int PORequestApprovalLevel { get; set; }
            public string PORequestApprovalStatus { get; set; }
            public string SiteName { get; set; }
            public string EntName { get; set; }
            public string PICName { get; set; }
            public string Specification { get; set; }
            public string CreateBy { get; set; }
            public string PORRemark { get; set; }
            public string PODocNo2 { get; set; }
            public string EstTimeArrival { get; set; }
            public decimal RevisionQty { get; set; }

            public string ReqType { get; set; }
            public decimal EstPrice { get; set; }
            public decimal TotalPrice { get; set; }
            public string MRTypeDesc { get; set; }
            public string QtDocNo { get; set; }
            public decimal PORUPrice { get; set; }
            public decimal PORAmount { get; set; }
            public decimal POUPrice { get; set; }
            public decimal POAmt { get; set; }
        }

        #endregion

    }
}
