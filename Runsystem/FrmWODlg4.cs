﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using TenTec.Windows.iGridLib;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWODlg4 : RunSystem.FrmBase9
    {
        #region Field

        private FrmWO mFrmParent;
        private string mSQL = string.Empty, mDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmWODlg4(FrmWO FrmParent, string DocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            TxtWODocNo.EditValue = mDocNo;
            SetGrd(); 
            ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWODocNo, 
                    }, true);
                    TxtWODocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWODocNo
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "DO Request",
                        "DO Request"+Environment.NewLine+"Date",
                        "",
                    },
                     new int[] 
                    {
                        40, 
                        150, 120, 20
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt ");
                SQL.AppendLine("From TblDoRequestDepthdr  A ");
                SQL.AppendLine("Where A.WODocNo is not null  ");

                mSQL = SQL.ToString();

                string Filter = "And  A.WODocNo= '" + mDocNo + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString() + Filter,
                        new string[] 
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        }, false, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDORequestDeptWO("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDORequestDeptWO("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        #endregion

      

        #endregion

       
    }
}
