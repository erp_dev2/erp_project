﻿namespace RunSystem
{
    partial class FrmPort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtPortName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPortCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkLoadingInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkDischargeInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLoadingInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDischargeInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkLoadingInd);
            this.panel2.Controls.Add(this.ChkDischargeInd);
            this.panel2.Controls.Add(this.TxtPortName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtPortCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtPortName
            // 
            this.TxtPortName.EnterMoveNextControl = true;
            this.TxtPortName.Location = new System.Drawing.Point(80, 37);
            this.TxtPortName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPortName.Name = "TxtPortName";
            this.TxtPortName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPortName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPortName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPortName.Properties.Appearance.Options.UseFont = true;
            this.TxtPortName.Properties.MaxLength = 200;
            this.TxtPortName.Size = new System.Drawing.Size(432, 20);
            this.TxtPortName.TabIndex = 12;
            this.TxtPortName.Validated += new System.EventHandler(this.TxtPortName_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(14, 38);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Port Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPortCode
            // 
            this.TxtPortCode.EnterMoveNextControl = true;
            this.TxtPortCode.Location = new System.Drawing.Point(80, 15);
            this.TxtPortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPortCode.Name = "TxtPortCode";
            this.TxtPortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPortCode.Properties.MaxLength = 30;
            this.TxtPortCode.Size = new System.Drawing.Size(170, 20);
            this.TxtPortCode.TabIndex = 10;
            this.TxtPortCode.Validated += new System.EventHandler(this.TxtPortCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(17, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Port Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkLoadingInd
            // 
            this.ChkLoadingInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkLoadingInd.Location = new System.Drawing.Point(80, 59);
            this.ChkLoadingInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkLoadingInd.Name = "ChkLoadingInd";
            this.ChkLoadingInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkLoadingInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLoadingInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkLoadingInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkLoadingInd.Properties.Appearance.Options.UseFont = true;
            this.ChkLoadingInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkLoadingInd.Properties.Caption = "Port Of Loading";
            this.ChkLoadingInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLoadingInd.Size = new System.Drawing.Size(132, 22);
            this.ChkLoadingInd.TabIndex = 13;
            // 
            // ChkDischargeInd
            // 
            this.ChkDischargeInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkDischargeInd.Location = new System.Drawing.Point(80, 80);
            this.ChkDischargeInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkDischargeInd.Name = "ChkDischargeInd";
            this.ChkDischargeInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkDischargeInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDischargeInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkDischargeInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkDischargeInd.Properties.Appearance.Options.UseFont = true;
            this.ChkDischargeInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkDischargeInd.Properties.Caption = "Port Of Discharge";
            this.ChkDischargeInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDischargeInd.Size = new System.Drawing.Size(127, 22);
            this.ChkDischargeInd.TabIndex = 14;
            // 
            // FrmPort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmPort";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLoadingInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDischargeInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtPortName;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtPortCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkLoadingInd;
        private DevExpress.XtraEditors.CheckEdit ChkDischargeInd;
    }
}