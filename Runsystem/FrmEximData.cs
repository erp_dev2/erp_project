﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using TenTec.Windows.iGridLib; 

#endregion

namespace RunSystem
{
    public partial class FrmEximData : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        string Server = Sm.ReadSetting("Server");
        private static string CurrentUserCode { get; set; }
        private static string Database { get; set; }
        private static string LogIn { get; set; }
        internal static string ServerLocal { get; set; }
        internal static string DatabaseLocal { get; set; }
        
        internal FrmEximDataFind FrmFind;

        #endregion

        #region Constructor

        public FrmEximData(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                SetLueOption(ref LueFrom);
                SetLueOption(ref LueTo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            BtnEdit.Visible = false;

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> {TxtDocNo, DteDocDt, LueFrom, LueTo }, true);
                    LueFrom.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueFrom, LueTo }, false);
                    break;
                case mState.Edit:                    
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, LueFrom, LueTo });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEximDataFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            string LocalIp = string.Empty;
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EximAtd", "TblEximAtd");

                var cml = new List<MySqlCommand>();

                #region send data to PC barcode
                if (Sm.GetLue(LueTo) != Server)
                {
                    var lEmployee = new List<Employee>();
                    Process1(ref lEmployee);

                    #region Send to all pc barCode

                    if (Sm.GetLue(LueTo) == "All")
                    {
                        var lIP = new List<IP>();
                        ListIP(ref lIP);
                        if (lIP.Count > 0)
                        {
                            for (var i = 0; i < lIP.Count; i++)
                            {
                                LocalIp = string.Empty;
                                if (lIP[i].LocalIP.Length > 0)
                                {
                                    LocalIp = lIP[i].LocalIP;
                                    SelectDataServer(ref lEmployee, LocalIp);
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.Warning, 
                                        "IP Address : " + LocalIp + Environment.NewLine +
                                        "Updating data is failed.");
                                }
                            }
                        }
                        lIP.Clear();
                        lEmployee.Clear();
                    }
                    #endregion

                    #region send to specific pc barcode
                    else
                    {
                        SelectDataServer(ref lEmployee, Sm.GetLue(LueTo));
                        SetFormControl(mState.View);
                    }
                    #endregion
                }
                #endregion

                #region pull data to server
                else
                {
                    #region Pull data From all pc barcode

                    if (Sm.GetLue(LueFrom) == "All")
                    {
                        var cm = new MySqlCommand();
                        
                        using (var cn = new MySqlConnection(Gv.ConnectionString))
                        {
                            cn.Open();
                            cm.Connection = cn;
                            cm.CommandText = "Select OptDesc From TblOption Where OptCat='LocalIP';";
                            var dr = cm.ExecuteReader();
                            var c = Sm.GetOrdinal(dr, new string[]{ "OptDesc" });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    LocalIp = dr.GetString(c[0]).ToString();
                                    SelectDataLocal(LocalIp);
                                }
                            }
                        }
                    }

                    #endregion

                    #region Pull data From specific pc barcode

                    else
                    {
                        SelectDataLocal(Sm.GetLue(LueFrom));
                        SetFormControl(mState.View);
                    }

                    #endregion
                }
                #endregion

                SaveExim(DocNo);
                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                ShowErrorMsg(LocalIp, Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowErrorMsg(string LocalIP, Exception Exc)
        {
            var Msg = "IP Address : " + LocalIP + Environment.NewLine; 
            switch (Exc.Message)
            {
                case "Fatal error encountered during command execution.":
                    Sm.StdMsg(mMsgType.Warning, Msg + "Unstable connection.");
                    break;
                case "Unable to connect to any of the specified MySQL hosts.":
                    Sm.StdMsg(mMsgType.Warning, Msg + "No connection to database server.");
                    break;
                default:
                    Sm.StdMsg(mMsgType.Warning, Msg + Exc.Message);
                    break;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion   

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date")||
                Sm.IsLueEmpty(LueFrom, "Option From") ||
                Sm.IsLueEmpty(LueTo, "Option To") ||
                IsSelectnotValid() ||
                IsSelectnotValid2();
        }

        private bool IsSelectnotValid()
        {
            if (Sm.GetLue(LueTo) != Server && Sm.GetLue(LueFrom) != Server)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose server.");
                return true;
            }
            return false;
        }

        private bool IsSelectnotValid2()
        {
            if (Sm.GetLue(LueTo) == Server && Sm.GetLue(LueFrom) == Server)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose different server.");
                return true;
            }
            return false;
        }

        private bool IsTimeNotValid()
        {
            string TimeNow = Sm.GetValue("Select Right(CurrentDateTime(), 4)");
            if (Decimal.Parse(TimeNow) > 730)
            {
                Sm.StdMsg(mMsgType.Warning, "you can't import / export data for this time .");
                return true;
            }
            else if (Decimal.Parse(TimeNow) < 630)
            {
                Sm.StdMsg(mMsgType.Warning, "you can't import / export data for this time .");
                return true;
            }
            return false;
        }

        public void SaveExim(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEximAtd(DocNo, DocDt, PCFrom, PCTo, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @PCFrom, @PCTo, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PCFrom", Sm.GetLue(LueFrom));
            Sm.CmParam<String>(ref cm, "@PCTo", Sm.GetLue(LueTo));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            Sm.ExecCommand(cm);
        }

        private MySqlCommand DeleteEmployee()
        {
            return new MySqlCommand() { CommandText = "Delete From TblEmpAtd;" };
        }

        #region Server - Local

        private void SelectDataServer(ref List<Employee> lE, string PCLocal)
        {
            ServerLocal = PCLocal;
            DatabaseLocal = "RunSystemIOKAtd";
            if (lE.Count > 0)
            {
                var cml2 = new List<MySqlCommand>();
                cml2.Add(DeleteEmployee());
                for (var i = 0; i < lE.Count; i++)
                    cml2.Add(SaveEmp(lE[i].EmpCode, lE[i].EmpName, lE[i].DeptName, lE[i].BarCodeCode));
                
                ExecCommandsExclude(cml2);
                Sm.StdMsg(mMsgType.Warning,
                    "IP Address : " + PCLocal + Environment.NewLine +
                    "Updating data is successed.");
            }
        }

        private  MySqlCommand SaveEmp(string EmpCode, string EmpName, string DeptName, string BarcodeCode)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblEmpAtd(EmpCode, EmpName, DeptName, BarcodeCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @EmpName, @DeptName, @BarcodeCode, @UserCode, CurrentDateTime());");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@EmpName", EmpName);
            Sm.CmParam<String>(ref cm, "@DeptName", DeptName);
            Sm.CmParam<String>(ref cm, "@BarcodeCode", BarcodeCode);
            Sm.CmParam<String>(ref cm, "@UserCode", "Sys");

            return cm;
        }
        
        #endregion

        #region Local - Server

        public void SelectDataLocal(string ServerLocals)
        {
            ServerLocal = ServerLocals;
            DatabaseLocal = "RunSystemIOKAtd";

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode, Dt, Tm, Machine, CreateBy, CreateDt ");
            SQL.AppendLine("From TblAttendanceLog ");
            SQL.AppendLine("Where ProcessInd='N' ");
            SQL.AppendLine("And Dt<='" + Sm.GetDte(DteDocDt).Substring(0, 8) + "';");

            using (var cn = new MySqlConnection(ConnectionStringExclude))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "EmpCode", "Dt", "Tm", "Machine", "CreateBy", "CreateDt"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string EmpCode = dr.GetString(c[0]).ToString();
                        string Dt = dr.GetString(c[1]).ToString();
                        string Tm = dr.GetString(c[2]).ToString();
                        string Machine = dr.GetString(c[3]).ToString();
                        string CreateBy = dr.GetString(c[4]).ToString();
                        string CreateDt = dr.GetString(c[5]).ToString();

                        var cml2 = new List<MySqlCommand>();
                        cml2.Add(SaveAtdLog(EmpCode, Dt, Tm, Machine, CreateBy, CreateDt));
                        Sm.ExecCommands(cml2);
                        
                        var cml = new List<MySqlCommand>();
                        cml.Add(UpdateInd(EmpCode, Dt, Tm, Machine, CreateBy, CreateDt));
                        ExecCommandsExclude(cml);
                    }
                }
                dr.Close();
            }
            //myLists.Add(l);
            
        }

        private static MySqlCommand SaveAtdLog(string EmpCode, string Dt, string Tm, string Machine, string CreateBy, string CreateDt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAttendanceLog(EmpCode, Dt, Tm, Machine, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @Dt, @Tm, @Machine, @CreateBy, @CreateDt);");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@Tm", Tm);
            Sm.CmParam<String>(ref cm, "@Machine", Machine);
            Sm.CmParam<String>(ref cm, "@CreateBy", CreateBy);
            Sm.CmParam<String>(ref cm, "@CreateDt", CreateDt);

            return cm;
        }

        private  MySqlCommand UpdateInd(string EmpCode, string Dt, string Tm, string Machine, string LastUpBy, string LastUpDt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAttendanceLog Set ProcessInd='Y', LastUpBy=@LastUpBy, LastUpDt=@LastUpDt ");
            SQL.AppendLine("Where EmpCode=@EmpCode And Dt=@Dt And Tm=@Tm And Machine=@Machine;");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@Tm", Tm);
            Sm.CmParam<String>(ref cm, "@Machine", Machine);
            Sm.CmParam<String>(ref cm, "@CreateBy", LastUpBy);
            Sm.CmParam<String>(ref cm, "@LastUpDt", LastUpDt);
            
            return cm;
        }

        #endregion

        #endregion

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select DocNo, DocDt, PCfrom, PCTo From TblEximAtd Where DocNo=@DocNo",
                        new string[] 
                        { 
                            "DocNo", 
                            "DocDt", "PCFrom", "PCTo" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            Sm.SetLue(LueFrom, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueTo, Sm.DrStr(dr, c[3]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method
       
        public static string ConnectionStringExclude
        {
            get 
            {
                return 
                    @"Server=" + ServerLocal.Trim() + ";Database=" + DatabaseLocal.Trim() + ";Uid=TheIOKAtd;Password=TheIOKAtdPwd;Allow User Variables=True;Connection Timeout=1200;";
            }
        }

        public static void ExecCommandsExclude(List<MySqlCommand> cml)
        {
            using (var cn = new MySqlConnection(ConnectionStringExclude))
            {
                cn.Open();
                var tr = cn.BeginTransaction();
                try
                {
                    cml.ForEach
                    (cm =>
                    {
                        cm.Connection = cn;
                        cm.CommandTimeout = 1200;
                        cm.Transaction = tr;
                        cm.ExecuteNonQuery();
                    }
                    );
                    //tr.Rollback();
                    tr.Commit();
                }
                catch (Exception Exc)
                {
                    tr.Rollback();
                    throw new Exception(Exc.Message);
                }
                finally
                {
                    tr = null;
                }
            }
        }

        private void SetLueOption(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptDesc As Col1, Concat('PC Local : ', OptDesc) As Col2 ");
            SQL.AppendLine("From TblOption Where optCat = 'LocalIP' ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("Select '" + Gv.Server + "' As Col1, 'Server' As Col2 ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("Select 'All' As Col1, 'All Machine' As Col2 "); 

            Sm.SetLue2(
                ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }    

        private void Process1(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.DeptName, ifnull(A.BarcodeCode, Concat(A.EmpCode, '01')) As BarcodeCode From tblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (ResignDt Is Not Null And  (A.ResignDt > '" + Sm.GetDte(DteDocDt).Substring(0, 8) + "' )))");
            SQL.AppendLine("Or ResignDt is null ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 1200;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode",
 
                    //1-3
                    "EmpName", "DeptName", "BarCodeCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            DeptName = Sm.DrStr(dr, c[2]),
                            BarCodeCode = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ListIP(ref List<IP> l)
        {
            l.Clear();
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 1200;
                cm.CommandText = "Select OptDesc From TblOption Where OptCat='LocalIP';";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new IP(){ LocalIP = Sm.DrStr(dr, c[0]) });
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFrom, new Sm.RefreshLue1(SetLueOption));
        }

        private void LueTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTo, new Sm.RefreshLue1(SetLueOption));

        }
        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { set; get; }
            public string DeptName { set; get; }
            public string BarCodeCode { set; get; }
        }

        private class IP
        {
            public string LocalIP { set; get; }
        }

        #endregion
    }
}
