﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmUpdatingItemPlanningGrp : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        private string mSQL = string.Empty;
        private int ColInd = 0;
        
        #endregion

        #region Constructor

        public FrmUpdatingItemPlanningGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void SetSQL()
        {
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, C.ItName, A.ItIndex, B.ItPlanninggrpName ");
            SQL.AppendLine("From TblItemIndex A ");
            SQL.AppendLine("Left Join TblItemPlanningGroup B On A.ItPlanningGrpCode=B.ItPlanningGrpCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");

            mSQL = SQL.ToString();
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetGrd();
            SetSQL();
            SetLueItPlanningGrp(ref LueItPlanningGrp);
            Sl.SetLueItCtCode(ref LueItCtCode);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-4
                        "Item Code",
                        "Item Name",
                        "Index",
                        "Item"+Environment.NewLine+"Planning Group"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //
                        80, 300, 100, 200
                    }

                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueItCtCode, TxtItCode, LueItPlanningGrp
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", Sm.GetLue(LueItPlanningGrp));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "C.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "",
                        new string[] { "ItCode", "ItName", "ItIndex", "ItPlanningGrpName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                //Sm.FocusGrd(Grd1, Col, 0);
                //Cursor.Current = Cursors.WaitCursor;
            }
        }

        override protected void SaveData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0) == true)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveItemCostCategory(Row));
                    ColInd = Row;
                }
            Sm.ExecCommands(cml);

            ShowData();
            FocusGrid(ColInd);
        }

        private void FocusGrid(int Col)
        {
            Sm.FocusGrd(Grd1, Col, 0);
            Cursor.Current = Cursors.WaitCursor;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueItPlanningGrp, "Item planning group") ||
                IsGrdEmpty();
        }

        private MySqlCommand SaveItemCostCategory(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemIndex(ItCode, ItIndex, ItPlanningGrpCode, CreateBy, CreateDt) " +
                    "Values(@ItCode, @ItIndex, @ItPlanningGrpCode, @CreateBy, CurrentDateTime()) " +
                    "   ON Duplicate Key Update ItCode=@ItCode, ItPlanningGrpCode=@ItPlanningGrpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();  "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ItIndex", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@ItPlanningGrpCode", Sm.GetLue(LueItPlanningGrp));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to select at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsChooseData()
        {
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0))
                    {
                        if (IsChoose == false) IsChoose = true;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
            }

            //if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
            return false;
        }

        #region setlue

        private void SetLueItPlanningGrp(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItPlanningGrpCode As Col1, ItPlanningGrpName As Col2 From TblItemPlanningGroup where ActInd='Y';",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

      

        #endregion

        #region Event

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void LueItPlanningGrp_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItPlanningGrp, new Sm.RefreshLue1(SetLueItPlanningGrp));
        }
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

    }
}
