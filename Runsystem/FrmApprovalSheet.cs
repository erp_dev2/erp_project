﻿#region Update
/*
    15/02/2023 [SET/HEX] Menu baru
    21/02/2023 [IBL/HEX] Approval berdasarkan item category. Berdasarkan parameter IsDocApprovalSettingUseItemCt
    22/02/2023 [SET/HEX] penyesuain source Uom berdasar item & ketika download file
    22/02/2023 [SET/HEX] validasi ketika MR menggunakan dokumen Approval Sheet
    27/02/2023 [SET/HEX] BUG Approval Sheet
    06/03/2023 [HAR/HEX] Penyesuaian Printout Approval Sheet
    07/03/2023 [SET/HEX] tambah ttd pada printout
    09/03/2023 [SET/HEX] penyesuain field Remark -> max length 1000
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Collections;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmApprovalSheet : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmApprovalSheetFind FrmFind;
        internal bool
            mIsSystemUseCostCenter = false,
            mIsFilterByCC = false,
            mIsDocApprovalSettingUseItemCt = false;
        internal bool mIsAssetTransferUseLocation = false;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormPrintOutApprovalSheet = string.Empty;
        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmApprovalSheet(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Approval Sheet";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueDeptCode(ref LueDeptCode);
                SetLuePICCode(ref LuePIC);
                LueCurCode.Visible = false;
                Tc.SelectedTab = TpGeneral;

                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Cancel",
                        
                        //1-5
                        "",
                        "Item's Code",
                        "Item's Name",
                        "Specification",
                        "Quantity",
                        //6-10
                        "UoMCode",
                        "UoM",
                        "CurCode",
                        "Currency",
                        "Estimated Price",
                        //11-12
                        "Total Price",
                        "Remark"
                    },
                     new int[] 
                    {
                        70, 
                        20, 200, 200, 100, 100, 
                        100, 100, 100, 100, 200, 
                        200, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 6, 7, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5, 9, 10, 11 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] {  });
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            #endregion

            #region Grd2
            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 100, 80, 200 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, LueItCtCode, LueReqType, LueDeptCode, LuePIC, DteDueDt, TxtGrandTotal,
                        MeeRemark, TxtFile1, ChkFile1
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnFile1.Enabled = false;
                    if (TxtFile1.Text.Length > 0)
                        BtnDownload1.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueItCtCode, LueReqType, LueDeptCode, LuePIC, DteDueDt, MeeRemark
                    }, false);
                    Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                    Sl.SetLueOption(ref LueReqType, "ReqType");
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 4, 5, 9, 10, 12 });
                    BtnFile1.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        MeeCancelReason 
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueItCtCode, LueReqType, MeeRemark, LuePIC, LueDeptCode, MeeCancelReason,
                TxtFile1, TxtStatus, TxtGrandTotal, DteDueDt, ChkFile1
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmApprovalSheetFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    Sm.StdMsgYN("Print", "") == DialogResult.No 
                    ) return;

                ParPrint(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ") && !IsGrdExceedMaxRecords())
                            Sm.FormShowDialog(new FrmApprovalSheetDlg(this, e.RowIndex, Sm.GetLue(LueItCtCode)));
                    }
                }
                if (Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
                {
                    if (e.ColIndex == 9)
                    {
                        SetLueCurCode(ref LueCurCode);
                        Sm.LueRequestEdit(ref Grd1, ref LueCurCode, ref fCell, ref fAccept, e);
                    }
                }
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete && Sm.GetGrdStr(Grd1, 0, 2).Length == 0)
                {
                    ClearData();
                    Sm.SetDteCurrentDate(DteDocDt);
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) 
            {
                //if(!IsGrdExceedMaxRecords())
                if(!Sm.IsLueEmpty(LueItCtCode, "Item's Category"))
                    Sm.FormShowDialog(new FrmApprovalSheetDlg(this, e.RowIndex, Sm.GetLue(LueItCtCode)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPropertyInventory(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPropertyCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }


        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5, 10, 11 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { }, e);
                if (Grd1.Rows.Count > 1)
                {
                    ComputeTotalPrice();
                    ComputeGrandTotal();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ApprovalSheet", "TblApprovalSheetHdr");
            var IsNeedApproval = IsDocNeedApproval();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveApprovalSheet(DocNo));
            if (IsNeedApproval)
                cml.Add(SaveDocApproval(DocNo));
            //if (!IsNeedApproval)
            //{
            //    cml.Add(SaveJournal(DocNo));
            //    cml.Add(SavePropertyInventory(DocNo));
            //}

            Sm.ExecCommands(cml);

            if (TxtFile1.Text.Length > 0 && TxtFile1.Text != "openFileDialog1")
                UploadFile1(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") || 
                Sm.IsLueEmpty(LueItCtCode, "Item's Category") ||
                Sm.IsLueEmpty(LueReqType, "Request Type") || 
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsDteEmpty(DteDueDt, "Due Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1 && Sm.GetGrdStr(Grd1, 0, 1).Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only fill in one Item data.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;

                Msg =
                    "Property Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Property Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine;
            }
            return false;
        }

        private MySqlCommand SaveApprovalSheet(string DocNo)
        {
            var cm = new MySqlCommand();
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var SQLDtl = new StringBuilder();

            //Hdr
            SQL.AppendLine("Insert Into TblApprovalSheetHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, CancelInd, ItCtCode, ReqType, DeptCode, PIC, DueDt, GrandTotal, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @ItCtCode, @ReqType, @DeptCode, @PIC, @DueDt, @GrandTotal, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            //Dtl
            SQLDtl.AppendLine("Insert Into tblApprovalSheetdtl ");
            SQLDtl.AppendLine("(DocNo, DNo, ItCode, Specification, Qty, OutstandingQty, UoMCode, CurCode, EstPrice, TotalPrice, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQLDtl.AppendLine(", ");

                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @ItCode_" + r.ToString() + ", @Specification_" + r.ToString() + ", @Qty_" + r.ToString() + ", @OutstandingQty_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@UoMCode_" + r.ToString() + ", @CurCode_" + r.ToString() + ", @EstPrice_" + r.ToString() + ", @TotalPrice_" + r.ToString() + ", @Remark_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                Sm.CmParam<String>(ref cm, "@Specification_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                Sm.CmParam<Decimal>(ref cm, "@OutstandingQty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                Sm.CmParam<String>(ref cm, "@UoMCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                Sm.CmParam<Decimal>(ref cm, "@EstPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                Sm.CmParam<Decimal>(ref cm, "@TotalPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
            }
            SQLDtl.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQLDtl.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Inner JOIN tblApprovalSheetHdr U ON U.DocNo = @DocNo ");
            //SQL.AppendLine("Inner JOIN tblcostcenter V ON U.CCCodeFrom = V.CCCode ");
            SQL.AppendLine("Where T.DocType='ApprovalSheet' ");
            SQL.AppendLine("And T.DeptCode = U.DeptCode ");
            if (mIsDocApprovalSettingUseItemCt)
            {
                SQL.AppendLine("And T.ItCtCode = ( ");
                SQL.AppendLine("    Select B.ItCtCode ");
                SQL.AppendLine("    From TblApprovalSheetDtl A ");
                SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("    Where A.DocNo = @DocNo And A.DNo = '001' ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update CreateBy=@UserCode, CreateDt=CurrentDateTime() ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblApprovalSheetHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType='ApprovalSheet' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        internal MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @row:= 0; ");
            SQL.AppendLine("Update TblPropertyInventoryTransferHdr Set ");
            SQL.AppendLine("    `Status`='A', JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Property Inventory Transfer : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, A.CCCodeFrom, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            //SQL.AppendLine("Inner Join tblpropertyinventoryhdr B on A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From ( ");
            SQL.AppendLine("		Select C.AcNo1 AcNo, D.RemStockValue As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("		From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryTransferDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCategory C ON A.PropertyCategoryCodeTo = C.PropertyCategoryCode ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryHdr D ON B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select C.AcNo1 AcNo, 0.00 As DAmt, D.RemStockValue As CAmt ");
            SQL.AppendLine("		From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryTransferDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryCategory C ON A.PropertyCategoryCodeFrom = C.PropertyCategoryCode ");
            SQL.AppendLine("		Inner Join TblPropertyInventoryHdr D ON B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("    )Tbl Where AcNo Is Not Null -- Group By AcNo ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.ServerCurrentDate(), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            return cm;
        }

        private void SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);

            SQL.AppendLine("Update TblPropertyInventoryTransferHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPropertyInventoryTransferHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPropertyInventoryTransferHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
           Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            var cm = new List<MySqlCommand>();
            //var IsNeedApproval = IsDocNeedApproval();
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditApprovalSheetCancel();
            //if (!IsNeedApproval)
            //    SaveJournal2();
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Approval Sheet Cancelled", false) ||
                //IsDataAlreadyReceived() ||
                IsCancelIndEditedAlready() || 
                IsApprovalSheetChooseInMR();
        }

        private bool IsCancelIndEditedAlready()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblApprovalSheetHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }
            return false;
        }
        
        private bool IsApprovalSheetChooseInMR()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblApprovalSheetDtl Where Concat(DocNo,DNo) In (" +
                "Select Concat(ApprovalSheetDocNo,ApprovalSheetDNo) From TblMaterialRequestDtl Where CancelInd='N' And `Status`<>'C'); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already use in Material Request");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyReceived()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblPropertyInventoryTransfer Where DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already Received.");
                return true;
            }
            return false;
        }

        private void EditApprovalSheetCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblApprovalSheetHdr Set CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;" 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                ShowApprovalSheetHdr(DocNo);
                ShowApprovalSheetDtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowApprovalSheetHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, case when Status = 'O' then 'Outstanding' when Status = 'A' then 'Approved' Else 'Cancel' End As Status, CancelInd, CancelReason, ItCtCode, ReqType, " +
                    "DeptCode, PIC, DueDt, GrandTotal, Remark, FileName " +
                    "From TblApprovalSheetHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "Status", "CancelInd", "CancelReason", "ItCtCode", 
                        //6-10
                        "ReqType", "DeptCode", "PIC", "DueDt", "GrandTotal", 
                        //11-15
                        "Remark", "FileName",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        Sm.SetLue(LueItCtCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LuePIC, Sm.DrStr(dr, c[8]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[9]));
                        TxtGrandTotal.EditValue = Sm.DrStr(dr, c[10]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        TxtFile1.EditValue = Sm.DrStr(dr, c[12]);
                        //TxtFile2.EditValue = Sm.DrStr(dr, c[13]);
                    }, true
                );
            Sm.FormatNumTxt(TxtGrandTotal, 0);
        }

        private void ShowApprovalSheetDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.ItCode, D.ItName, A.Specification, A.Qty, A.UoMCode, B.UoMName, A.CurCode, C.CurName, A.EstPrice, ");
            SQL.AppendLine("A.TotalPrice, A.Remark ");
            SQL.AppendLine("From TblApprovalSheetDtl A ");
            SQL.AppendLine("Inner Join TblUom B On A.UoMCode=B.UoMCode ");
            SQL.AppendLine("Inner Join Tblcurrency C On A.CurCode=C.CurCode ");
            SQL.AppendLine("Inner Join Tblitem D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0 
                    "ItCode", 
                    //1-5
                    "ItName", "Specification", "Qty", "UomCode", "UoMName", 
                    //6-10
                    "CurCode", "CurName", "EstPrice", "TotalPrice", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, IfNull(B.UserName, A.UserCode) As UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ApprovalSheet' ");
            SQL.AppendLine("And IfNull(A.Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd2.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select CurCode As Col1 From tblCurrency ", "Currency");
        }

        internal void SetLueUoMCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            try
            {
                //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                //ItCode = Sm.GetGrdStr(Grd1, Row, 1);

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.PurchaseUOMCode Col1, B.UomName Col2 ");
                SQL.AppendLine("FROM tblitem A ");
                SQL.AppendLine("INNER JOIN tbluom B ON A.PurchaseUOMCode = B.UomCode ");
                SQL.AppendLine("WHERE A.ItCode = '" + ItCode + "' ");
                SQL.AppendLine("UNION ");
                SQL.AppendLine("SELECT A.InventoryUOMCode Col1, B.UomName Col2 ");
                SQL.AppendLine("FROM tblitem A ");
                SQL.AppendLine("INNER JOIN tbluom B ON A.InventoryUOMCode = B.UomCode ");
                SQL.AppendLine("WHERE A.ItCode = '" + ItCode + "' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLuePICCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
                SQL.AppendLine("From TblUser ");
                //if (mIsPICGrouped)
                //    SQL.AppendLine("where UserCode=@UserCode ");
                SQL.AppendLine("Order By UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsAssetTransferUseLocation = Sm.GetParameter("IsAssetTransferUseLocation") == "Y";
            mIsSystemUseCostCenter = Sm.GetParameter("IsSystemUseCostCenter") == "Y";
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC"); 
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsDocApprovalSettingUseItemCt = Sm.GetParameterBoo("IsDocApprovalSettingUseItemCt");
            mFormPrintOutApprovalSheet = Sm.GetParameter("FormPrintOutApprovalSheet");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeTotalPrice()
        {
            decimal Qty = 0m, EstPrice = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Qty = Sm.GetGrdDec(Grd1, Row, 5);
                    EstPrice = Sm.GetGrdDec(Grd1, Row, 10);
                    Grd1.Cells[Row, 11].Value = Qty * EstPrice;
                }
            }
            //TxtAccruedInterest.EditValue = Sm.FormatNum(Decimal.Parse(TxtAccruedInterest.Text), 0);
        }
        
        internal void ComputeGrandTotal()
        {
            decimal TotalPrice = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    TotalPrice += Sm.GetGrdDec(Grd1, Row, 11);
                }
            }
            TxtGrandTotal.EditValue = TotalPrice;
            Sm.FormatNumTxt(TxtGrandTotal, 0);
        }

        private void UploadFile1(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile1.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile1.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload1.Invoke(
                    (MethodInvoker)delegate { PbUpload1.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload1.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload1.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryTransferFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdatePropertyInventoryTransferFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblApprovalSheetHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void DownloadFile1(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload1.Value = 0;
                PbUpload1.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload1.Value = PbUpload1.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload1.Value + bytesRead <= PbUpload1.Maximum)
                        {
                            PbUpload1.Value += bytesRead;

                            PbUpload1.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = mMenuCode + "-Approval Sheet";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private bool IsDocNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='ApprovalSheet' Limit 1;");
        }

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string PayslipPrintBy =
                "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}",
                Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));

            #region GSS
            if (Doctitle == "HEX")
            {
                var l = new List<APSheetHex>();

                string[] TableName = { "APSheetHex" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNo, C.OptDesc ReqType, D.DeptName,  ");
                SQL.AppendLine("E.UserName, DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, G.ItName, F.Specification, F.Qty, F.UoMCode, ");
                SQL.AppendLine("F.CurCode, F.EstPrice, F.TotalPrice, A.Remark As RemarkHdr, H.ItCtName, ");
                SQL.AppendLine("I.level1, I.Remark1, I.user1, I.ApproveDt1, ");
                SQL.AppendLine("J.level2, J.Remark2, J.user2, J.ApproveDt2,");
                SQL.AppendLine("K.level3, K.Remark3, K.user3, K.ApproveDt3,");
                SQL.AppendLine("L.level4, L.Remark4, L.user4, L.ApproveDt4,");
                SQL.AppendLine("M.level5, M.Remark5, M.user5, M.ApproveDt5,");
                SQL.AppendLine("N.level6, N.Remark6, N.user6, N.ApproveDt6,");
                SQL.AppendLine("O.level7, O.Remark7, O.user7, O.ApproveDt7,");
                SQL.AppendLine("P.level8, P.Remark8, P.user8, P.ApproveDt8,");
                SQL.AppendLine("Q.level9, Q.Remark9, Q.user9, Q.ApproveDt9,");
                SQL.AppendLine("R.level10, R.Remark10, R.user10, R.ApproveDt10,");
                SQL.AppendLine("S.level11, S.Remark11, S.user11, S.ApproveDt11, ");
                SQL.AppendLine("I.SignPict1, J.SignPict2, K.SignPict3, L.SignPict4, M.SignPict5, N.SignPict6, O.SignPict7, P.SignPict8, Q.SignPict9, R.SignPict10, S.SignPict11 ");
                SQL.AppendLine("From TblApprovalSheetHdr A ");
                SQL.AppendLine("Inner Join tblItemCategory B On A.ItCtCode = B.ItCtCode ");
                SQL.AppendLine("Inner Join tblOption C On A.ReqType = C.OptCode And C.OptCat = 'ReqType' ");
                SQL.AppendLine("Inner Join TblDepartment D On A.DeptCode = D.DeptCode ");
                SQL.AppendLine("LEFT JOIN tbluser E ON A.PIC = E.UserCode ");
                SQL.AppendLine("INNER JOIN tblapprovalsheetdtl F ON A.Docno = F.DocNo ");
                SQL.AppendLine("INNER JOIN tblitem G ON F.ItCode = G.ItCode ");
                SQL.AppendLine("Left Join Tblitemcategory H ON A.ItCtCode = H.ItCtCode ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict1, B.`Level` AS LEVEL1, A.Remark AS Remark1, C.Username As User1, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt1 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '1' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")I ON A.DocNo = I.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict2, B.`Level` AS LEVEL2, A.Remark AS Remark2, C.Username As User2, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt2 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '2' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")J ON A.DocNo = J.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict3, B.`Level` AS LEVEL3, A.Remark AS Remark3, C.Username As User3, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt3 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '3' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")K ON A.DocNo = K.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict4, B.`Level` AS LEVEL4, A.Remark AS Remark4, C.Username As User4, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt4 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '4' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")L ON A.DocNo = L.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict5, B.`Level` AS LEVEL5, A.Remark AS Remark5, C.Username As User5, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt5 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '5' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")M ON A.DocNo = M.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict6, B.`Level` AS LEVEL6, A.Remark AS Remark6, C.Username As User6, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt6 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '6' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")N ON A.DocNo = N.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict7, B.`Level` AS LEVEL7, A.Remark AS Remark7, C.Username As User7, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt7 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '7' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")O ON A.DocNo = O.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict8, B.`Level` AS LEVEL8, A.Remark AS Remark8, C.Username As User8, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt8 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '8' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")P ON A.DocNo = P.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict9, B.`Level` AS LEVEL9, A.Remark AS Remark9, C.Username As User9, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt9 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '9' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")Q ON A.DocNo = Q.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict10, B.`Level` AS LEVEL10, A.Remark AS Remark10, C.Username As User10,DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt10 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '10' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")R ON A.DocNo = Q.DocNo ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.DocNo, Concat(IfNull(D.ParValue, ''), C.UserCode, '.JPG') As SignPict11, B.`Level` AS LEVEL11, A.Remark AS Remark11, C.Username As User11, DATE_FORMAT(Left(A.LastUpDt, 8),'%d %M %Y') As ApproveDt11 ");
                SQL.AppendLine("    FROM tbldocapproval A ");
                SQL.AppendLine("    INNER JOIN tbldocapprovalsetting B ON A.ApprovalDNo = B.DNo AND B.DocType = 'ApprovalSheet' ");
                SQL.AppendLine("    INNER JOIN tbluser C ON A.UserCode = C.UserCode  ");
                SQL.AppendLine("    LEFT JOIN tblparameter D ON D.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("    WHERE A.Doctype = 'ApprovalSheet' AND A.docno = @DocNo AND B.`Level` = '11' ");
                SQL.AppendLine("    LIMIT 1 ");
                SQL.AppendLine(")S ON A.DocNo = S.DocNo ");

                SQL.AppendLine("WHERE A.cancelInd = 'N' ");
                SQL.AppendLine("And A.DocNo=@DocNo ");
               
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyAddressCity",
                    "CompanyPhone",
                    "CompanyFax",

                    //6-10
                    "DocDt",
                    "DocNo",
                    "ReqType",
                    "DeptName",
                    "UserName",

                    //11-15
                    "DueDt",
                    "ItName",
                    "Specification",
                    "Qty",
                    "UoMCode",
                    //16-20
                    "CurCode",
                    "EstPrice",
                    "TotalPrice",
                    "RemarkHdr",
                    "ItCtName",
                    //21-25
                    "level1",
                    "remark1",
                    "level2",
                    "remark2",
                    "level3",
                    //26-30
                    "remark3",
                    "level4",
                    "remark4",
                    "level5",
                    "remark5",
                    //31-35
                    "level6",
                    "remark6",
                    "level7",
                    "remark7",
                    "level8",
                    //36-40
                    "remark8",
                    "level9",
                    "remark9",
                    "level10",
                    "remark10",
                    //41-45
                    "level11",
                    "remark11",
                    "User1",
                    "ApproveDt1",
                     "User2",
                     //46-50
                    "ApproveDt2",
                     "User3",
                    "ApproveDt3",
                     "User4",
                    "ApproveDt4",
                    //51-55
                     "User5",
                    "ApproveDt5",
                     "User6",
                    "ApproveDt6",
                     "User7",
                     //56-60
                    "ApproveDt7",
                     "User8",
                    "ApproveDt8",
                     "User9",
                    "ApproveDt9",
                    //61-65
                     "User10",
                    "ApproveDt10",
                     "User11",
                    "ApproveDt11",
                    "SignPict1",
                    //66-70
                    "SignPict2",
                    "SignPict3",
                    "SignPict4",
                    "SignPict5",
                    "SignPict6",
                    //71-75
                    "SignPict7",
                    "SignPict8",
                    "SignPict9",
                    "SignPict10",
                    "SignPict11",


                    });

                    if (dr.HasRows)
                    {
                        int numb = 0;
                        while (dr.Read())
                        {
                            numb = numb + 1;
                            l.Add(new APSheetHex()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressCity = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                DocDt = Sm.DrStr(dr, c[6]),
                                DocNo = Sm.DrStr(dr, c[7]),
                                ReqType = Sm.DrStr(dr, c[8]),
                                DeptName = Sm.DrStr(dr, c[9]),
                                UserName = Sm.DrStr(dr, c[10]),

                                DueDt = Sm.DrStr(dr, c[11]),
                                ItName = Sm.DrStr(dr, c[12]),
                                Specification = Sm.DrStr(dr, c[13]),
                                Qty = Sm.DrDec(dr, c[14]),
                                UoMCode = Sm.DrStr(dr, c[15]),

                                CurCode = Sm.DrStr(dr, c[16]),
                                EstPrice = Sm.DrDec(dr, c[17]),
                                TotalPrice = Sm.DrDec(dr, c[18]),
                                RemarkHdr = Sm.DrStr(dr, c[19]),
                                ItCtName = Sm.DrStr(dr, c[20]),

                                level1 = Sm.DrStr(dr, c[21]),
                                remark1 = Sm.DrStr(dr, c[22]),
                                level2 = Sm.DrStr(dr, c[23]),
                                remark2 = Sm.DrStr(dr, c[24]),
                                level3 = Sm.DrStr(dr, c[25]),

                                remark3 = Sm.DrStr(dr, c[26]),
                                level4 = Sm.DrStr(dr, c[27]),
                                remark4 = Sm.DrStr(dr, c[28]),
                                level5 = Sm.DrStr(dr, c[29]),
                                remark5 = Sm.DrStr(dr, c[30]),

                                level6 = Sm.DrStr(dr, c[31]),
                                remark6 = Sm.DrStr(dr, c[32]),
                                level7 = Sm.DrStr(dr, c[33]),
                                remark7 = Sm.DrStr(dr, c[34]),
                                level8 = Sm.DrStr(dr, c[35]),

                                remark8 = Sm.DrStr(dr, c[36]),
                                level9 = Sm.DrStr(dr, c[37]),
                                remark9 = Sm.DrStr(dr, c[38]),
                                level10 = Sm.DrStr(dr, c[39]),
                                remark10 = Sm.DrStr(dr, c[40]),

                                level11 = Sm.DrStr(dr, c[41]),
                                remark11 = Sm.DrStr(dr, c[42]),
                                User1 = Sm.DrStr(dr, c[43]),
                                ApproveDt1 = Sm.DrStr(dr, c[44]),
                                User2 = Sm.DrStr(dr, c[45]),
                                ApproveDt2 = Sm.DrStr(dr, c[46]),
                                User3 = Sm.DrStr(dr, c[47]),
                                ApproveDt3 = Sm.DrStr(dr, c[48]),
                                User4 = Sm.DrStr(dr, c[49]),
                                ApproveDt4 = Sm.DrStr(dr, c[50]),
                                User5 = Sm.DrStr(dr, c[51]),
                                ApproveDt5 = Sm.DrStr(dr, c[52]),
                                User6 = Sm.DrStr(dr, c[53]),
                                ApproveDt6 = Sm.DrStr(dr, c[54]),
                                User7 = Sm.DrStr(dr, c[55]),
                                ApproveDt7 = Sm.DrStr(dr, c[56]),
                                User8 = Sm.DrStr(dr, c[57]),
                                ApproveDt8 = Sm.DrStr(dr, c[58]),
                                User9 = Sm.DrStr(dr, c[59]),
                                ApproveDt9 = Sm.DrStr(dr, c[60]),
                                User10 = Sm.DrStr(dr, c[61]),
                                ApproveDt10 = Sm.DrStr(dr, c[62]),
                                User11 = Sm.DrStr(dr, c[63]),
                                ApproveDt11 = Sm.DrStr(dr, c[64]),
                                SignPict1 = Sm.DrStr(dr, c[65]),
                                SignPict2 = Sm.DrStr(dr, c[66]),
                                SignPict3 = Sm.DrStr(dr, c[67]),
                                SignPict4 = Sm.DrStr(dr, c[68]),
                                SignPict5 = Sm.DrStr(dr, c[69]),
                                SignPict6 = Sm.DrStr(dr, c[70]),
                                SignPict7 = Sm.DrStr(dr, c[71]),
                                SignPict8 = Sm.DrStr(dr, c[72]),
                                SignPict9 = Sm.DrStr(dr, c[73]),
                                SignPict10 = Sm.DrStr(dr, c[74]),
                                SignPict11 = Sm.DrStr(dr, c[75]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                // UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")


                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                Sm.PrintReport(mFormPrintOutApprovalSheet, myLists, TableName, false);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Event

        private void BtnFile1_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile1.Checked = true;
                OD.InitialDirectory = "c:";
                //OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile1.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ChkFile1_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile1.Checked == false)
            {
                TxtFile1.EditValue = string.Empty;
            }
        }

        private void BtnDownload1_Click(object sender, EventArgs e)
        {
            DownloadFile1(mHostAddrForFTPClient, mPortForFTPClient, TxtFile1.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile1.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile1, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void LueCurCode_Validated(object sender, EventArgs e)
        {
            LueCurCode.Visible = false;
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (LueCurCode.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LueCurCode).Trim();
                    Grd1.Cells[fCell.RowIndex, 9].Value = LueCurCode.GetColumnValue("Col2");
                }
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(SetLuePICCode));
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
        }

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(Sl.SetLueOption), "ReqType");
            //if (BtnSave.Enabled && Sm.GetLue(LueReqType).Length > 0)
            //    SetTabCertificate();
        }

        #endregion

    }

    #region Report Class
    class APSheetHex
    {
        public string CompanyLogo { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string DocDt { get; set; }
        public string DocNo { get; set; }
        public string ReqType { get; set; }
        public string DeptName { get; set; }
        public string UserName { get; set; }
        public string DueDt { get; set; }
        public string ItName { get; set; }
        public string Specification { get; set; }
        public decimal Qty { get; set; }
        public string UoMCode { get; set; }
        public string CurCode { get; set; }
        public decimal EstPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string PrintBy { get; set; }

        public string RemarkHdr { get; set; }
        public string ItCtName { get; set; }

        public string level1 { get; set; }
        public string remark1 { get; set; }
        public string level2 { get; set; }
        public string remark2 { get; set; }
        public string level3 { get; set; }
        public string remark3 { get; set; }
        public string level4 { get; set; }
        public string remark4 { get; set; }
        public string level5 { get; set; }
        public string remark5 { get; set; }
        public string level6 { get; set; }
        public string remark6 { get; set; }
        public string level7 { get; set; }
        public string remark7 { get; set; }
        public string level8 { get; set; }
        public string remark8 { get; set; }
        public string level9 { get; set; }
        public string remark9 { get; set; }
        public string level10 { get; set; }
        public string remark10 { get; set; }
        public string level11 { get; set; }
        public string remark11 { get; set; }

        public string User1 { get; set; }
        public string ApproveDt1 { get; set; }
        public string User2 { get; set; }
        public string ApproveDt2 { get; set; }
        public string User3 { get; set; }
        public string ApproveDt3 { get; set; }
        public string User4 { get; set; }
        public string ApproveDt4 { get; set; }
        public string User5 { get; set; }
        public string ApproveDt5 { get; set; }
        public string User6 { get; set; }
        public string ApproveDt6 { get; set; }
        public string User7 { get; set; }
        public string ApproveDt7 { get; set; }
        public string User8 { get; set; }
        public string ApproveDt8 { get; set; }
        public string User9 { get; set; }
        public string ApproveDt9 { get; set; }
        public string User10 { get; set; }
        public string ApproveDt10 { get; set; }
        public string User11 { get; set; }
        public string ApproveDt11 { get; set; }
        public string SignPict1 { get; set; }
        public string SignPict2 { get; set; }
        public string SignPict3 { get; set; }
        public string SignPict4 { get; set; }
        public string SignPict5 { get; set; }
        public string SignPict6 { get; set; }
        public string SignPict7 { get; set; }
        public string SignPict8 { get; set; }
        public string SignPict9 { get; set; }
        public string SignPict10 { get; set; }
        public string SignPict11 { get; set; }
    }

    #endregion
}
