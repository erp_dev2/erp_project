﻿namespace RunSystem
{
    partial class FrmOtFormula2Find
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkOTFormulaDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteOTFormulaDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteOTFormulaDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtOTFormulaCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkOTFormulaCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOTFormulaDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOTFormulaCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOTFormulaCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.ChkOTFormulaDt);
            this.panel2.Controls.Add(this.DteOTFormulaDt2);
            this.panel2.Controls.Add(this.DteOTFormulaDt1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtOTFormulaCode);
            this.panel2.Controls.Add(this.ChkOTFormulaCode);
            this.panel2.Size = new System.Drawing.Size(672, 54);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 419);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(212, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 14);
            this.label4.TabIndex = 41;
            this.label4.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(72, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Date";
            // 
            // ChkOTFormulaDt
            // 
            this.ChkOTFormulaDt.Location = new System.Drawing.Point(330, 27);
            this.ChkOTFormulaDt.Name = "ChkOTFormulaDt";
            this.ChkOTFormulaDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkOTFormulaDt.Properties.Appearance.Options.UseFont = true;
            this.ChkOTFormulaDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkOTFormulaDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkOTFormulaDt.Properties.Caption = " ";
            this.ChkOTFormulaDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkOTFormulaDt.Size = new System.Drawing.Size(20, 22);
            this.ChkOTFormulaDt.TabIndex = 15;
            this.ChkOTFormulaDt.ToolTip = "Remove filter";
            this.ChkOTFormulaDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkOTFormulaDt.ToolTipTitle = "Run System";
            this.ChkOTFormulaDt.EditValueChanged += new System.EventHandler(this.ChkOTFormulaDt_CheckedChanged);
            // 
            // DteOTFormulaDt2
            // 
            this.DteOTFormulaDt2.EditValue = null;
            this.DteOTFormulaDt2.EnterMoveNextControl = true;
            this.DteOTFormulaDt2.Location = new System.Drawing.Point(226, 27);
            this.DteOTFormulaDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteOTFormulaDt2.Name = "DteOTFormulaDt2";
            this.DteOTFormulaDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOTFormulaDt2.Properties.Appearance.Options.UseFont = true;
            this.DteOTFormulaDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOTFormulaDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteOTFormulaDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteOTFormulaDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteOTFormulaDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOTFormulaDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteOTFormulaDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOTFormulaDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteOTFormulaDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteOTFormulaDt2.Size = new System.Drawing.Size(101, 20);
            this.DteOTFormulaDt2.TabIndex = 14;
            this.DteOTFormulaDt2.EditValueChanged += new System.EventHandler(this.DteOTFormulaDt2_EditValueChanged);
            // 
            // DteOTFormulaDt1
            // 
            this.DteOTFormulaDt1.EditValue = null;
            this.DteOTFormulaDt1.EnterMoveNextControl = true;
            this.DteOTFormulaDt1.Location = new System.Drawing.Point(108, 27);
            this.DteOTFormulaDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteOTFormulaDt1.Name = "DteOTFormulaDt1";
            this.DteOTFormulaDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOTFormulaDt1.Properties.Appearance.Options.UseFont = true;
            this.DteOTFormulaDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOTFormulaDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteOTFormulaDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteOTFormulaDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteOTFormulaDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOTFormulaDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteOTFormulaDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOTFormulaDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteOTFormulaDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteOTFormulaDt1.Size = new System.Drawing.Size(101, 20);
            this.DteOTFormulaDt1.TabIndex = 13;
            this.DteOTFormulaDt1.EditValueChanged += new System.EventHandler(this.DteOTFormulaDt1_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "OT Formula Code";
            // 
            // TxtOTFormulaCode
            // 
            this.TxtOTFormulaCode.EnterMoveNextControl = true;
            this.TxtOTFormulaCode.Location = new System.Drawing.Point(108, 5);
            this.TxtOTFormulaCode.Name = "TxtOTFormulaCode";
            this.TxtOTFormulaCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOTFormulaCode.Properties.Appearance.Options.UseFont = true;
            this.TxtOTFormulaCode.Properties.MaxLength = 250;
            this.TxtOTFormulaCode.Size = new System.Drawing.Size(219, 20);
            this.TxtOTFormulaCode.TabIndex = 10;
            this.TxtOTFormulaCode.Validated += new System.EventHandler(this.TxtOTFormulaCode_Validated);
            // 
            // ChkOTFormulaCode
            // 
            this.ChkOTFormulaCode.Location = new System.Drawing.Point(330, 5);
            this.ChkOTFormulaCode.Name = "ChkOTFormulaCode";
            this.ChkOTFormulaCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkOTFormulaCode.Properties.Appearance.Options.UseFont = true;
            this.ChkOTFormulaCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkOTFormulaCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkOTFormulaCode.Properties.Caption = " ";
            this.ChkOTFormulaCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkOTFormulaCode.Size = new System.Drawing.Size(20, 22);
            this.ChkOTFormulaCode.TabIndex = 11;
            this.ChkOTFormulaCode.ToolTip = "Remove filter";
            this.ChkOTFormulaCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkOTFormulaCode.ToolTipTitle = "Run System";
            this.ChkOTFormulaCode.CheckedChanged += new System.EventHandler(this.ChkOTFormulaCode_CheckedChanged);
            // 
            // FrmOtFormula2Find
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmOtFormula2Find";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOTFormulaDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOTFormulaDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOTFormulaCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOTFormulaCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkOTFormulaDt;
        internal DevExpress.XtraEditors.DateEdit DteOTFormulaDt2;
        internal DevExpress.XtraEditors.DateEdit DteOTFormulaDt1;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtOTFormulaCode;
        private DevExpress.XtraEditors.CheckEdit ChkOTFormulaCode;

    }
}