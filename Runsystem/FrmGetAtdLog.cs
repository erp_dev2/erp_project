﻿#region Update
/*
    07/10/2019 [HAR/SRN] tarik data dari SQl server
    10/10/2019 [HAR/SRN] BUg waktu validasi koneksi return type nya masih true
    14/11/2019 [HAR/SRN] tambahan validasi jika employeeoldcodenya kosong
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmGetAtdLog : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        //internal Frm FrmFind;
        internal string ServerLocal = "";
       

        #endregion

        #region Constructor

        public FrmGetAtdLog(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Get Data Attendance Log";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false; 
                BtnEdit.Visible = false;
                BtnFind.Visible = false;
                SetFormControl(mState.View);
                SetLueFrom(ref LueFrom);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        # region Standart Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueYr, LueMth, LueFrom
                    }, true);
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueYr, LueMth, LueFrom
                    }, false);
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    
                    break;

            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueYr, LueMth, LueFrom
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {

        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
           
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            ConnectionStringType();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
      
            var lAbsensi = new List<Absensi>();
            var lEmp = new List<Emp>();

            Process1(ref lAbsensi);

            Process2(ref lAbsensi);

            lAbsensi.ForEach(r => {
                if (r.EmpCode.Length > 0)
                {
                    cml.Add(SaveAttendanceLog(ref r));
                }
            });

            Sm.ExecCommands(cml);
            Sm.StdMsg(mMsgType.Info, "Data Attendance from " + LueFrom.Text + " Already processed");


            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueFrom, "From") ||
                CekConn(ServerLocal);
        }

        private void getDataAtd()
        {
            #region old
            //string Yr = Sm.GetLue(LueYr);
            //string Mth = Sm.GetLue(LueMth);

            //string DateFilter = String.Concat(Yr, '-', Mth);
            ////RestClient restClient = new RestClient("http://localhost:8080");

            //////Creating request to get data from server
            ////RestRequest restRequest = new RestRequest("/absensi/search?Tanggal=" + DateFilter + "", Method.GET);

            ////// Executing request to server and checking server response to the it
            ////IRestResponse restResponse = restClient.Execute(restRequest);

            ////// Extracting output data from received response
            ////string response = restResponse.Content;

            ////MessageBox.Show(response);

            ////Define your baseUrl
            //string baseUrl = "http://localhost:8080/absensi/search?Tanggal=" + DateFilter + "";
            ////Have your using statements within a try/catch blokc that will catch any exceptions.
            //try
            //{
            //    //We will now define your HttpClient with your first using statement which will use a IDisposable.
            //    using (HttpClient client = new HttpClient())
            //    {

            //    }
            //}
            //catch (Exception exception)
            //{
            //    Console.WriteLine("Exception Hit------------");
            //    Console.WriteLine(exception);
            //}
            #endregion
           
        }


        private MySqlCommand SaveAttendanceLog(ref Absensi r)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblAttendancelog(EmpCode, Dt, Tm, Machine, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @Dt, @Tm, @Machine, @CreateBy, CurrentDateTime()) ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@Dt", String.Concat(r.yy, r.mm, r.dd));
            Sm.CmParam<String>(ref cm, "@Tm", String.Concat(r.hh, r.mi, r.ss));
            Sm.CmParam<String>(ref cm, "@Machine", r.Machine);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        private void Process1(ref List<Absensi> lAbsensi)
        {
            var SQL = new StringBuilder();
            var cm = new SqlCommand();

            string yr = Sm.GetLue(LueYr);
            string mth = Sm.GetLue(LueMth);

            SQL.AppendLine("Select *  ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("    Select PIN, ");
            SQL.AppendLine("    CAST(DATEPART(yy, masuk) AS VARCHAR) yy, ");
            SQL.AppendLine("    CAST(DATEPART(mm, masuk) AS VARCHAR) mm,  ");
            SQL.AppendLine("    CAST(DATEPART(dd, masuk) AS VARCHAR) as dd, ");
            SQL.AppendLine("    CAST(DATEPART(hour, masuk) AS VARCHAR) as hh, ");
            SQL.AppendLine("    CAST(DATEPART(mi, masuk) AS VARCHAR) as mi, ");
            SQL.AppendLine("    CAST(DATEPART(ss, masuk) AS VARCHAR) as ss,  ");
	        SQL.AppendLine("    Kodearea from absensi  ");
            SQL.AppendLine("    WHERE masuk IS NOT NULL AND  ");
	        SQL.AppendLine("     (DATEPART(yy, tanggal) = "+yr+" ");
		    SQL.AppendLine("        AND    DATEPART(mm, tanggal) = "+mth+") ");
            SQL.AppendLine("    Union All  ");
            SQL.AppendLine("     Select PIN, ");
            SQL.AppendLine("    CAST(DATEPART(yy, pulang) AS VARCHAR) yy, ");
            SQL.AppendLine("    CAST(DATEPART(mm, pulang) AS VARCHAR) mm,  ");
            SQL.AppendLine("    CAST(DATEPART(dd, pulang) AS VARCHAR) as dd, ");
            SQL.AppendLine("    CAST(DATEPART(hour, pulang) AS VARCHAR) as hh, ");
            SQL.AppendLine("    CAST(DATEPART(mi, pulang) AS VARCHAR) as mi, ");
            SQL.AppendLine("    CAST(DATEPART(ss, pulang) AS VARCHAR) as ss,  ");
	        SQL.AppendLine("    Kodearea from absensi  ");
            SQL.AppendLine("    WHERE pulang IS NOT NULL AND  ");
	        SQL.AppendLine("     (DATEPART(yy, tanggal) = "+yr+" ");
		    SQL.AppendLine("        AND    DATEPART(mm, tanggal) = "+mth+") ");
            SQL.AppendLine(")Z  ");
            SQL.AppendLine("Order By Z.PIN ");


            using (var cn = new SqlConnection(ServerLocal))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PIN", "yy", "mm", "dd", "hh", "mi", "ss", "kodearea" });
                
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {   
                        lAbsensi.Add(new Absensi()
                        {
                            EmpCode = string.Empty,
                            PIN = Sm.DrStr(dr, c[0]).Length>0 ? Sm.Right(String.Concat("0000000", Sm.DrStr(dr, c[0])), 7) : string.Empty,
                            yy = Sm.DrStr(dr, c[1]).ToString(),
                            mm = Sm.DrStr(dr, c[2]).ToString().Length > 0 ? Sm.Right(string.Concat("00", Sm.DrStr(dr, c[2])).ToString(), 2) : string.Empty,
                            dd = Sm.DrStr(dr, c[3]).ToString().Length > 0 ? Sm.Right(string.Concat("00", Sm.DrStr(dr, c[3])).ToString(), 2) : string.Empty,
                            hh = Sm.DrStr(dr, c[4]).ToString().Length > 0 ? Sm.Right(string.Concat("00", Sm.DrStr(dr, c[4])).ToString(), 2) : string.Empty,
                            mi = Sm.DrStr(dr, c[5]).ToString().Length > 0 ? Sm.Right(string.Concat("00", Sm.DrStr(dr, c[5])).ToString(), 2) : string.Empty,
                            ss = Sm.DrStr(dr, c[6]).ToString().Length > 0 ? Sm.Right(string.Concat("00", Sm.DrStr(dr, c[6])).ToString(), 2) : string.Empty,
                            Machine = Sm.DrStr(dr, c[7])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Absensi> lAbsensi)
        {
            var lEmp = new List<Emp>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select EmpCode, EmpName, EmpCodeOld ");
            SQL.AppendLine("From TblEmployee Where EmpCodeOld is not null ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "EmpName", "EmpCodeOld" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmp.Add(new Emp()
                        {
                            EmpCode = dr.GetString(0),
                            EmpName = dr.GetString(1),
                            EmpCodeOld = dr.GetString(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lEmp.Count > 0)
            {
                lEmp.OrderBy(x => x.EmpCode);
                int Temp = 0;
                for (var i = 0; i < lEmp.Count; i++)
                {
                    for (var j = Temp; j < lAbsensi.Count; j++)
                    {
                        if (lAbsensi[j].PIN.Length > 0)
                        {
                            var PINs = Sm.Right(string.Concat("0000000", lAbsensi[j].PIN), 7);
                            if (lEmp[i].EmpCodeOld == PINs)
                            {
                                lAbsensi[j].EmpCode = lEmp[i].EmpCode;
                                lAbsensi[j].yy = lAbsensi[j].yy;
                                lAbsensi[j].mm = lAbsensi[j].mm;
                                lAbsensi[j].dd = lAbsensi[j].dd;
                                lAbsensi[j].hh = lAbsensi[j].hh;
                                lAbsensi[j].mi = lAbsensi[j].mi;
                                lAbsensi[j].ss = lAbsensi[j].ss;
                                lAbsensi[j].Machine = lAbsensi[j].Machine;
                            }
                        }
                    }
                }
            }
            lEmp.Clear();
        }


        #endregion

   
        #endregion

  
        #region Additional Method

        private void ConnectionStringType()
        {

            string DocTitle = Sm.GetParameter("DocTitle");
            if (DocTitle == "SRN")
            {
                if (Sm.GetLue(LueFrom) == "1")
                {
                    ServerLocal = @"Server=192.168.70.66;Database=biov6;Uid=sdmsarinah;Password=Sarinah123;Connection Timeout=10;";                
                }
 
            }
        }

        private static bool CekConn(string ServerLocal)
        {
            bool Result = true;
            try
            {
                SqlConnection conn = new SqlConnection(ServerLocal);
                conn.Open();
                Result = false;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Invalid Connection String");
            }
            return Result;
        }

        private void SetLueFrom(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select '1' As Col1, 'Kantor Pusat' As Col2 ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

     
        #endregion 

        #endregion

        #region Event

        #region Misc Control Event

        private void LueForm_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFrom, new Sm.RefreshLue1(SetLueFrom));
        }

        #endregion

        #endregion

    }

    internal class Emp
    {
        public string EmpCode { set; get; }
        public string EmpName { set; get; }
        public string EmpCodeOld { set; get; }
    }

    internal class Absensi
    {
        public string EmpCode { get; set; }
        public string PIN { get; set; }
        public string Tanggal { get; set; }
        public string Tm { get; set; }
        public string yy { get; set; } 
        public string mm { get; set; } 
        public string dd { get; set; } 
        public string hh { get; set; }
        public string mi { get; set; }
        public string ss { get; set; }
        public string Machine { get; set; }

    }
}
