﻿#region Update
    // 22/08/2017 [TKG] 1 Allowance bisa dibagi menjadi lbh dari  1 site utk masing2 employee.
    // 25/08/2017 [TKG] Utk Allowance ditambah end date.
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSalaryDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpSalary mFrmParent;
        private string mADType = string.Empty;
        private iGrid mGrd;

        #endregion

        #region Constructor

        public FrmEmpSalaryDlg2(FrmEmpSalary FrmParent, string ADType, iGrid Grd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mADType = ADType;
            mGrd = Grd;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();

                this.Text = mADType == "A" ? "List of Allowance" : "List of Deduction";
                LblADCode.Text = mADType == "A" ? "Allowance" : "Deduction";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] { "No", "", "Code", "Name" },
                new int[] { 50, 20, 100, 250 }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                if (!
                    (mADType=="A" && 
                    (mFrmParent.mIsAllowancePerSite || mFrmParent.mIsAllowanceMultiplePeriod))
                    )
                    Filter = " And Locate(Concat('##', ADCode, '##'), @SelectedADCode)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ADType", mADType);
                Sm.CmParam<String>(ref cm, "@SelectedADCode",
                    mFrmParent.GetSelectedADCode(mADType == "A" ? mFrmParent.Grd2 : mFrmParent.Grd3));
                Sm.FilterStr(ref Filter, ref cm, TxtADCode.Text, new string[] { "ADCode", "ADName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select ADCode, ADName From TblAllowanceDeduction " +
                        "Where ADType=@ADType " + Filter + " Order By ADName;",
                        new string[] { "ADCode", "ADName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsADCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mGrd.Rows.Count - 1;
                        Row2 = Row;
                        if (mADType == "A")
                        {
                            Sm.CopyGrdValue(mGrd, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mGrd, Row1, 3, Grd1, Row2, 3);

                            mGrd.Rows.Add();

                            Sm.SetGrdNumValueZero(mGrd, mGrd.Rows.Count - 1, new int[] { 4 });
                        }
                        else
                        {
                            Sm.CopyGrdValue(mGrd, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mGrd, Row1, 3, Grd1, Row2, 3);

                            mGrd.Rows.Add();

                            Sm.SetGrdNumValueZero(mGrd, mGrd.Rows.Count - 1, new int[] { 4 });
                        }

                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 " + (mADType == "A" ? "allowance" : "deduction") + ".");
        }

        private bool IsADCodeAlreadyChosen(int Row)
        {
            if (mFrmParent.mIsAllowancePerSite || mFrmParent.mIsAllowanceMultiplePeriod) 
                return false;

            string ADCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mGrd.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mGrd, r, 2), ADCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtADCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkADCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, mADType == "A" ? "Allowance" : "Deduction");
        }

        #endregion

        #region Form Event

        private void FrmEmployeeSalaryDlg2_Activated(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion
    }
}
