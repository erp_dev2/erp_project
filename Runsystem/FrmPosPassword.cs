﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Sm = RunSystem.StdMtd;

namespace RunSystem
{
    public partial class FrmPosPassword : Form
    {
        private string MyPassword;

        public FrmPosPassword(string Password, string Title)
        {
            InitializeComponent();
            MyPassword = Password;
            Text = Title;
        }

        private void TxtPwd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) BtnOK_Click(sender, e);
            if (e.KeyChar == 27) BtnCancel_Click(sender, e);
            
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if (MyPassword == TxtPwd.Text)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                Sm.StdMsg(mMsgType.Warning, "Incorrect Password"); 
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }

        private void FrmPosPassword_Load(object sender, EventArgs e)
        {
            TxtPwd.Focus();
        }
    }
}
