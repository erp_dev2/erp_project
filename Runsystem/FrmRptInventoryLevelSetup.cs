﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptInventoryLevelSetup : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptInventoryLevelSetup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetGrd();
                SetSQL();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            #region SQL Stock Summary
            SQL.AppendLine("Select 'Stock Summary' As kode, '0' As Avg,  ifnull(if(01>Substring(CurrentDateTime(), 5, 2),0, Mth01), 0) Mth01, ifnull(if(02>Substring(CurrentDateTime(), 5, 2),0, Mth02), 0) Mth02, ifnull(if(03>Substring(CurrentDateTime(), 5, 2),0, Mth03), 0) Mth03, ifnull(if(04>Substring(CurrentDateTime(), 5, 2),0, Mth04), 0) Mth04, ");
            SQL.AppendLine("ifnull(if(05>Substring(CurrentDateTime(), 5, 2),0, Mth05), 0) Mth05, ifnull(if(06>Substring(CurrentDateTime(), 5, 2),0, Mth06), 0) Mth06, ifnull(if(07>Substring(CurrentDateTime(), 5, 2),0, Mth07), 0) Mth07, ");
            SQL.AppendLine("ifnull(if(08>Substring(CurrentDateTime(), 5, 2),0, Mth08), 0) Mth08, ifnull(if(09>Substring(CurrentDateTime(), 5, 2),0, Mth09), 0) Mth09, ifnull(if(10>Substring(CurrentDateTime(), 5, 2),0, Mth10), 0) Mth10, ");
            SQL.AppendLine("ifnull(if(11>Substring(CurrentDateTime(), 5, 2),0, Mth11), 0) Mth11, ifnull(if(12>Substring(CurrentDateTime(), 5, 2),0, Mth12), 0) Mth12 From ( ");
            SQL.AppendLine("Select Sum(Qty01) Mth01, (SUM(Qty01)+Sum(Qty02)) Mth02, (SUM(Qty01)+Sum(Qty02)+Sum(Qty03)) Mth03, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)) Mth04, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)) Mth05,");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)) Mth06, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)+Sum(Qty07)) Mth07, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)+Sum(Qty07)+Sum(Qty08)) Mth08, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)+Sum(Qty07)+Sum(Qty08)+Sum(Qty09)) Mth09, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)+Sum(Qty07)+Sum(Qty08)+Sum(Qty09)+Sum(Qty10)) Mth10, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)+Sum(Qty07)+Sum(Qty08)+Sum(Qty09)+Sum(Qty10)+Sum(Qty11)) Mth11, ");
            SQL.AppendLine("(SUM(Qty01)+Sum(Qty02)+Sum(Qty03)+Sum(Qty04)+Sum(Qty05)+Sum(Qty06)+Sum(Qty07)+Sum(Qty08)+Sum(Qty09)+Sum(Qty10)+Sum(Qty11)+Sum(Qty12)) Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select ");
            SQL.AppendLine("Case Mth When '01' Then Qty Else 0.0000 End As Qty01, ");
            SQL.AppendLine("Case Mth When '02' Then Qty Else 0.0000 End As Qty02, ");
            SQL.AppendLine("Case Mth When '03' Then Qty Else 0.0000 End As Qty03, ");
            SQL.AppendLine("Case Mth When '04' Then Qty Else 0.0000 End As Qty04, ");
            SQL.AppendLine("Case Mth When '05' Then Qty Else 0.0000 End As Qty05, ");
            SQL.AppendLine("Case Mth When '06' Then Qty Else 0.0000 End As Qty06, ");
            SQL.AppendLine("Case Mth When '07' Then Qty Else 0.0000 End As Qty07, ");
            SQL.AppendLine("Case Mth When '08' Then Qty Else 0.0000 End As Qty08, ");
            SQL.AppendLine("Case Mth When '09' Then Qty Else 0.0000 End As Qty09, ");
            SQL.AppendLine("Case Mth When '10' Then Qty Else 0.0000 End As Qty10, ");
            SQL.AppendLine("Case Mth When '11' Then Qty Else 0.0000 End As Qty11, ");
            SQL.AppendLine("Case Mth When '12' Then Qty Else 0.0000 End As Qty12 ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("   Select T1.Mth, SUM(T1.Qty) As Qty From ( ");
	        SQL.AppendLine("    Select T1.Mth, (T2.Qty*T3.Uprice*T3.ExcRate) As Qty ");
	        SQL.AppendLine("    From ( ");
		    SQL.AppendLine("        Select convert('01' using latin1) As Mth Union All ");
    	    SQL.AppendLine("        Select convert('02' using latin1) Union All ");
    	    SQL.AppendLine("        Select convert('03' using latin1) Union All ");
    	    SQL.AppendLine("       Select convert('04' using latin1) Union All ");
    	    SQL.AppendLine("        Select convert('05' using latin1) Union All ");
    	    SQL.AppendLine("        Select convert('06' using latin1) Union All ");
		    SQL.AppendLine("        Select convert('07' using latin1) Union All ");
		    SQL.AppendLine("        Select convert('08' using latin1) Union All ");
		    SQL.AppendLine("        Select convert('09' using latin1) Union All ");
		    SQL.AppendLine("        Select convert('10' using latin1) Union All ");
		    SQL.AppendLine("       Select convert('11' using latin1) Union All ");
    	    SQL.AppendLine("        Select convert('12' using latin1)  ");
	        SQL.AppendLine("    ) T1 ");
	        SQL.AppendLine("    Inner Join ( ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty  ");
		    SQL.AppendLine("        From TblStockMovement A ");
		    SQL.AppendLine("        Inner Join (Select convert('01' using latin1) As Mth) B On 0=0 ");
 		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0201'))And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source 	");	    
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A ");
		    SQL.AppendLine("        Inner Join (Select convert('02' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0301')) And A.DocDt > (Select Concat(@year,'0201')) ");
		    SQL.AppendLine("        And A.Qty<>0  "); 
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A ");
		    SQL.AppendLine("        Inner Join (Select convert('03' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0401')) And A.DocDt > (Select Concat(@year,'0301')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A ");
		    SQL.AppendLine("        Inner Join (Select convert('04' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0501')) And A.DocDt > (Select Concat(@year,'0401')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source  ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement A ");
		    SQL.AppendLine("        Inner Join (Select convert('05' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0601')) And A.DocDt > (Select Concat(@year,'0501')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement A ");
		    SQL.AppendLine("        Inner Join (Select convert('06' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0701')) And A.DocDt > (Select Concat(@year,'0601')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        Select B.Mth,A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A ");
		    SQL.AppendLine("        Inner Join (Select convert('07' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0801')) And A.DocDt > (Select Concat(@year,'0701')) ");
		    SQL.AppendLine("        And A.Qty<>0");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth,A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A ");
		    SQL.AppendLine("        Inner Join (Select convert('08' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'0901')) And A.DocDt > (Select Concat(@year,'0801')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth,A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A "); 
		    SQL.AppendLine("        Inner Join (Select convert('09' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'1001')) And A.DocDt > (Select Concat(@year,'0901')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth,A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement A ");
		    SQL.AppendLine("        Inner Join (Select convert('10' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'1101')) And A.DocDt > (Select Concat(@year,'1001')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All");
		    SQL.AppendLine("        select B.Mth,A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement A ");
		    SQL.AppendLine("        Inner Join (Select convert('11' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year,'1201')) And A.DocDt > (Select Concat(@year,'1101')) ");
		    SQL.AppendLine("        And A.Qty<>0 ");
		    SQL.AppendLine("       Group By A.ItCode, A.BatchNo, A.Source ");
		    SQL.AppendLine("        Union All ");
		    SQL.AppendLine("        select B.Mth, A.ItCode, A.BatchNo, A.Source, Sum(A.Qty) As Qty ");
		    SQL.AppendLine("        From TblStockMovement  A ");
		    SQL.AppendLine("       Inner Join (Select convert('12' using latin1) As Mth) B On 0=0 ");
		    SQL.AppendLine("        Where A.DocDt<(Select Concat(@year+1,'0101')) And A.DocDt > (Select Concat(@year,'1201')) ");
		    SQL.AppendLine("        And Qty<>0");
		    SQL.AppendLine("        Group By A.ItCode, A.BatchNo, A.Source");
	        SQL.AppendLine("    ) T2 On T1.Mth=T2.Mth");
	        SQL.AppendLine("    Inner Join TblStockPrice T3 On T2.ItCode = T3.ItCode And T2.BatchNo=T3.BatchNo And T2.Source=T3.Source ");
	        SQL.AppendLine("    )T1  ");
            SQL.AppendLine(" Group By T1.Mth ");
            SQL.AppendLine(" )T ");
            SQL.AppendLine(" )Tbl ");
            SQL.AppendLine(" )Tbl2 ");
            #endregion

            #region Purchase Order
            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("Select ifnull(Kode, 'Purchase Order'), 0 As Avg, ifnull(SUM(MthPO01), 0) As Mth01, ifnull(SUM(MthPO02), 0) As Mth02, ifnull(SUM(MthPO03), 0) As Mth03, ifnull(SUM(MthPO04), 0) As Mth04, ");
            SQL.AppendLine("ifnull(SUM(MthPO05), 0) As Mth05, ifnull(SUM(MthPO06), 0) As Mth06, ifnull(SUM(MthPO07), 0) As Mth07, ifnull(SUM(MthPO08), 0) As Mth08, ifnull(SUM(MthPO09), 0) As Mth09, ");
            SQL.AppendLine("ifnull(SUM(MthPO10), 0) As Mth10, ifnull(SUM(MthPO11), 0) As Mth11, ifnull(SUM(MthPO12), 0) As Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Distinct 'Purchase Order' As Kode,  ");
            SQL.AppendLine("Case Mth When '01' Then POValue Else 0.0000 End As MthPO01, ");
            SQL.AppendLine("Case Mth When '02' Then POValue Else 0.0000 End As MthPO02, ");
            SQL.AppendLine("Case Mth When '03' Then POValue Else 0.0000 End As MthPO03, ");
            SQL.AppendLine("Case Mth When '04' Then POValue Else 0.0000 End As MthPO04, ");
            SQL.AppendLine("Case Mth When '05' Then POValue Else 0.0000 End As MthPO05, ");
            SQL.AppendLine("Case Mth When '06' Then POValue Else 0.0000 End As MthPO06, ");
            SQL.AppendLine("Case Mth When '07' Then POValue Else 0.0000 End As MthPO07, ");
            SQL.AppendLine("Case Mth When '08' Then POValue Else 0.0000 End As MthPO08, ");
            SQL.AppendLine("Case Mth When '09' Then POValue Else 0.0000 End As MthPO09, ");
            SQL.AppendLine("Case Mth When '10' Then POValue Else 0.0000 End As MthPO10, ");
            SQL.AppendLine("Case Mth When '11' Then POValue Else 0.0000 End As MthPO11, ");
            SQL.AppendLine("Case Mth When '12' Then POValue Else 0.0000 End As MthPO12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select T2.Mth, ifnull(T.AmountTotal, 0) As POValue ");
            SQL.AppendLine("From "); 
            SQL.AppendLine("( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All ");
            SQL.AppendLine("   Select convert('06' using latin1) Union All ");
            SQL.AppendLine("   Select convert('07' using latin1) Union All ");
            SQL.AppendLine("   Select convert('08' using latin1) Union All ");
            SQL.AppendLine("   Select convert('09' using latin1) Union All ");
            SQL.AppendLine("   Select convert('10' using latin1) Union All ");
            SQL.AppendLine("   Select convert('11' using latin1) Union All ");
            SQL.AppendLine("   Select convert('12' using latin1)  ");
            SQL.AppendLine(") T2");
            SQL.AppendLine("Inner Join (");
	        SQL.AppendLine("    Select X.Mth, SUM((X.Qty*X.UPrice*ifnull(X.Amt, 1))) As AmountTotal");
	        SQL.AppendLine("    From ");
		    SQL.AppendLine("        (Select Substring(A.DocDt, 5,2) As mth, A.DocNo, H.CurCode, A.DocDt, F.ItCode, B.Qty, I.UPrice, ");
			SQL.AppendLine("            (Select Amt From TblCurrencyrate Where rateDt<= A.DocDt And CurCode1=H.CurCode order by rateDt Desc limit 1) As Amt ");
			SQL.AppendLine("            From TblPOHdr A  ");
			SQL.AppendLine("            Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
			SQL.AppendLine("            Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo  ");
			SQL.AppendLine("            Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo  ");
			SQL.AppendLine("            Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
			SQL.AppendLine("            Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
			SQL.AppendLine("            Inner Join TblItem G On F.ItCode=G.ItCode ");
			SQL.AppendLine("            Inner Join TblQtHdr H On D.QtDocNo=H.DocNo ");
			SQL.AppendLine("            Inner Join TblQtDtl I On D.QtDocNo=I.DocNo And D.QtDNo=I.DNo ");
			SQL.AppendLine("            Inner Join TblVendor J On H.VdCode=J.VdCode ");
			SQL.AppendLine("            Inner Join TblDepartment K On E.DeptCode=K.DeptCode");
            SQL.AppendLine("        Where Left(A.DocDt, 4)=@Year and G.InventoryItemInd = 'Y' ");
		    SQL.AppendLine("        ) X");
	        SQL.AppendLine("    Group By X.Mth ");
            SQL.AppendLine(" )T On convert(T.Mth using latin1) = T2.Mth ");
            SQL.AppendLine(" Order By T2.Mth ");
            SQL.AppendLine(" )Y  Group By kode, mth ");
            SQL.AppendLine(" ) Y1 ");
            #endregion

            #region Minimum Stock
            SQL.AppendLine("Union All");

            SQL.AppendLine("Select ifnull(Kode, 'Minimum Stock'), 0 As Avg, ifnull(SUM(MthMV01), 0) As Mth01, ifnull(SUM(MthMV02), 0) As Mth02, ifnull(SUM(MthMV03), 0) As Mth03, ifnull(SUM(MthMV04), 0) As Mth04, ");
            SQL.AppendLine("ifnull(SUM(MthMV05), 0) As Mth05, ifnull(SUM(MthMV06), 0) As Mth06, ifnull(SUM(MthMV07), 0) As Mth07, ifnull(SUM(MthMV08), 0) As Mth08, ifnull(SUM(MthMV09), 0) As Mth09, ");
            SQL.AppendLine("ifnull(SUM(MthMV10), 0) As Mth10, ifnull(SUM(MthMV11), 0) As Mth11, ifnull(SUM(MthMV12), 0) As Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Distinct 'Minimum Stock' As Kode, "); 
            SQL.AppendLine("Case Mth When '01' Then MinValue Else 0.0000 End As MthMV01, ");
            SQL.AppendLine("Case Mth When '02' Then MinValue Else 0.0000 End As MthMV02, ");
            SQL.AppendLine("Case Mth When '03' Then MinValue Else 0.0000 End As MthMV03, ");
            SQL.AppendLine("Case Mth When '04' Then MinValue Else 0.0000 End As MthMV04, ");
            SQL.AppendLine("Case Mth When '05' Then MinValue Else 0.0000 End As MthMV05, ");
            SQL.AppendLine("Case Mth When '06' Then MinValue Else 0.0000 End As MthMV06, ");
            SQL.AppendLine("Case Mth When '07' Then MinValue Else 0.0000 End As MthMV07, ");
            SQL.AppendLine("Case Mth When '08' Then MinValue Else 0.0000 End As MthMV08, ");
            SQL.AppendLine("Case Mth When '09' Then MinValue Else 0.0000 End As MthMV09, ");
            SQL.AppendLine("Case Mth When '10' Then MinValue Else 0.0000 End As MthMV10, ");
            SQL.AppendLine("Case Mth When '11' Then MinValue Else 0.0000 End As MthMV11, ");
            SQL.AppendLine("Case Mth When '12' Then MinValue Else 0.0000 End As MthMV12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select T2.Mth, ifnull(T3.MinValue, 0) As MinValue ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All ");
            SQL.AppendLine("   Select convert('06' using latin1) Union All ");
            SQL.AppendLine("   Select convert('07' using latin1) Union All ");
            SQL.AppendLine("   Select convert('08' using latin1) Union All ");
            SQL.AppendLine("   Select convert('09' using latin1) Union All ");
            SQL.AppendLine("   Select convert('10' using latin1) Union All "); 
            SQL.AppendLine("   Select convert('11' using latin1) Union All ");
            SQL.AppendLine("   Select convert('12' using latin1)  ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select T.Mth, SUM(MinValue) As Minvalue, SUM(RopValue) As Ropvalue ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select B.DocNo, Substring(A.DocDt, 5, 2) As Mth, B.DNo, B.ItCode, (B.MinStock*B.Uprice) As MinValue, ");
		    SQL.AppendLine("        (B.ReOrderPoint*B.UPrice) As RopValue ");
	        SQL.AppendLine("    From TblreorderPointDtl B ");
	        SQL.AppendLine("    Inner Join Tblreorderpointhdr A On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    Where B.DocNo In ( ");
		    SQL.AppendLine("        Select A.DocNo From TblReorderPointHdr A ");
		    SQL.AppendLine("        Where Concat(Concat(A.DOcDt, Left(A.DocNo, 4))) In ( ");
			SQL.AppendLine("            Select MAX(Concat(A.DOcDt, Left(A.DocNo, 4))) From TblReorderPointHdr A)) ");
	        SQL.AppendLine("    And Left(A.DocDt, 4) = @Year ");
	        SQL.AppendLine("   Group By B.DocNo, B.DNo, B.ItCode ");
	        SQL.AppendLine("    )T ");
            SQL.AppendLine(")T3 On convert(T3.Mth using latin1) = T2.Mth ");
            SQL.AppendLine(" Order By T2.Mth ");
            SQL.AppendLine(" )Y  Group By kode, mth ");
            SQL.AppendLine(" ) Y1 ");

            #endregion

            #region ROP
            SQL.AppendLine("Union All");

            SQL.AppendLine("Select ifnull(Kode, 'Reorder Point'), 0 As Avg, ifnull(SUM(MthROP01), 0) As Mth01, ifnull(SUM(MthROP02), 0) As Mth02, ifnull(SUM(MthROP03), 0) As Mth03, ifnull(SUM(MthROP04), 0) As Mth04, ");
            SQL.AppendLine("ifnull(SUM(MthROP05), 0) As Mth05, ifnull(SUM(MthROP06), 0) As Mth06, ifnull(SUM(MthROP07), 0) As Mth07, ifnull(SUM(MthROP08), 0) As Mth08, ifnull(SUM(MthROP09), 0) As Mth09, ");
            SQL.AppendLine("ifnull(SUM(MthROP10), 0) As Mth10, ifnull(SUM(MthROP11), 0) As Mth11, ifnull(SUM(MthROP12), 0) As Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Distinct 'Reorder Point' As Kode, ");
            SQL.AppendLine("Case Mth When '01' Then RopValue Else 0.0000 End As MthROP01, ");
            SQL.AppendLine("Case Mth When '02' Then RopValue Else 0.0000 End As MthROP02, ");
            SQL.AppendLine("Case Mth When '03' Then RopValue Else 0.0000 End As MthROP03, ");
            SQL.AppendLine("Case Mth When '04' Then RopValue Else 0.0000 End As MthROP04, ");
            SQL.AppendLine("Case Mth When '05' Then RopValue Else 0.0000 End As MthROP05, ");
            SQL.AppendLine("Case Mth When '06' Then RopValue Else 0.0000 End As MthROP06, ");
            SQL.AppendLine("Case Mth When '07' Then RopValue Else 0.0000 End As MthROP07, ");
            SQL.AppendLine("Case Mth When '08' Then RopValue Else 0.0000 End As MthROP08, ");
            SQL.AppendLine("Case Mth When '09' Then RopValue Else 0.0000 End As MthROP09, ");
            SQL.AppendLine("Case Mth When '10' Then RopValue Else 0.0000 End As MthROP10, ");
            SQL.AppendLine("Case Mth When '11' Then RopValue Else 0.0000 End As MthROP11, ");
            SQL.AppendLine("Case Mth When '12' Then RopValue Else 0.0000 End As MthROP12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select T2.Mth, ifnull(T3.RopValue, 0) As RopValue ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All ");
            SQL.AppendLine("   Select convert('06' using latin1) Union All ");
            SQL.AppendLine("   Select convert('07' using latin1) Union All ");
            SQL.AppendLine("   Select convert('08' using latin1) Union All ");
            SQL.AppendLine("   Select convert('09' using latin1) Union All ");
            SQL.AppendLine("   Select convert('10' using latin1) Union All ");
            SQL.AppendLine("   Select convert('11' using latin1) Union All ");
            SQL.AppendLine("   Select convert('12' using latin1)  ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select T.Mth, SUM(MinValue) As Minvalue, SUM(RopValue) As Ropvalue ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select B.DocNo, Substring(A.DocDt, 5, 2) As Mth, B.DNo, B.ItCode, (B.MinStock*B.Uprice) As MinValue, ");
            SQL.AppendLine("        (B.ReOrderPoint*B.UPrice) As RopValue ");
            SQL.AppendLine("    From TblreorderPointDtl B ");
            SQL.AppendLine("    Inner Join Tblreorderpointhdr A On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where B.DocNo In ( ");
            SQL.AppendLine("        Select A.DocNo From TblReorderPointHdr A ");
            SQL.AppendLine("        Where Concat(Concat(A.DOcDt, Left(A.DocNo, 4))) In ( ");
            SQL.AppendLine("            Select MAX(Concat(A.DOcDt, Left(A.DocNo, 4))) From TblReorderPointHdr A)) ");
            SQL.AppendLine("    And Left(A.DocDt, 4) = @Year ");
            SQL.AppendLine("   Group By B.DocNo, B.DNo, B.ItCode ");
            SQL.AppendLine("    )T ");
            SQL.AppendLine(")T3 On convert(T3.Mth using latin1) = T2.Mth ");
            SQL.AppendLine(" Order By T2.Mth ");
            SQL.AppendLine(" )Y  Group By kode, mth ");
            SQL.AppendLine(" ) Y1 ");

            #endregion

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Option",

                        //1-5
                        "Average "+Sm.GetLue(LueYr)+"",
                        "January",
                        "February",
                        "March",
                        "April",

                        //6-10
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",

                        //11-13
                        "October",
                        "November",
                        "December"
                    },
                    new int[] 
                    {
                        //0
                        120, 

                        //1-5
                        150, 120, 120, 120, 120,  

                        //6-10
                        120, 120, 120, 120, 120, 
                        
                        //11-13
                        120, 120, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13}, false);
            Sm.GrdColInvisible(Grd1, new int[] {  });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);

            if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = string.Empty;

                    var cm = new MySqlCommand();

                    Sm.CmParam<String>(ref cm, "@Year", Year);
                   
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter,
                            new string[]
                            { 
                                //0
                                "Kode",  

                                //1-5
                                "Avg",  "Mth01", "Mth02", "Mth03", "Mth04",  

                                "Mth05", "Mth06", "Mth07", "Mth08", "Mth09", 
                                
                                "Mth10", "Mth11", "Mth12", 
                            },

                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);

                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 9);

                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 10);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 11);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 12);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 13);

                            }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    ComputeShowAvg();
                    if (Grd1.Rows.Count > 1)
                        Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 5 : 3);
                    else
                        Sm.FocusGrd(Grd1, 0, 0);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

       
     
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
        }

        #endregion

        #region Additional Method

        private decimal ComputeShowAvg()
        {
            decimal Avg = 0m; 
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                decimal Count = 0m;
                for (int Col = 2; Col <= 13; Col++)
                {
                    if (Sm.GetGrdDec(Grd1, Row, Col) != 0)
                        Count = Count + 1;
                }
                if (Count == 0)
                {
                    Count = 1;
                }
                Avg = (Sm.GetGrdDec(Grd1, Row, 2)+ Sm.GetGrdDec(Grd1, Row, 3)+
                    Sm.GetGrdDec(Grd1, Row, 4)+Sm.GetGrdDec(Grd1, Row, 5)+Sm.GetGrdDec(Grd1, Row, 6)+
                    Sm.GetGrdDec(Grd1, Row, 7)+Sm.GetGrdDec(Grd1, Row, 8)+Sm.GetGrdDec(Grd1, Row, 9)+
                    Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 11) + Sm.GetGrdDec(Grd1, Row, 12) + Sm.GetGrdDec(Grd1, Row, 13)) / Count;
                Grd1.Cells[Row, 1].Value = Sm.FormatNum(Avg, 0);
            }
            return Avg;
        }
        #endregion

       
        #endregion

        #endregion

        #region Event
        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            SetGrd();
            SetSQL();
        }

        #endregion
    }
}
