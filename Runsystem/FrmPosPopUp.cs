﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RunSystem
{
    public partial class FrmPosPopUp : Form
    {
        private string mTitle;
        private string mDecimalFormat;
        private string mAmountLabel;
        private string mRemarkLabel = "";
        private bool mBtnOKVisible = false;
        private bool mBtnCancelVisible = false;

        public FrmPosPopUp(string Title, string AmountLabel, string DecimalFormat, bool BtnOKVisible, bool BtnCancelVisible, string RemarkLabel)
        {

            InitializeComponent();
            mTitle = Title;
            mAmountLabel = AmountLabel;
            mDecimalFormat = DecimalFormat;
            mDecimalFormat = mDecimalFormat.Replace("{0:", "");
            mDecimalFormat = mDecimalFormat.Replace("}", "");
            mBtnOKVisible = BtnOKVisible;
            mBtnCancelVisible = BtnCancelVisible;
            mRemarkLabel = RemarkLabel;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            if (PnlAmount.Visible)
                try
                {
                    TxtAmount.Text = (decimal.Parse(TxtAmount.Text)).ToString();
                }
                catch
                {
                    TxtAmount.Text = "0";
                }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void TxtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                if (PnlRemark.Visible == false)
                    BtnOK_Click(sender, e);
                else
                    rTxtRemark.Focus();
            if (e.KeyChar == 27) BtnCancel_Click(sender, e);
        }

        private void FrmPosGetAmount_Load(object sender, EventArgs e)
        {
            Text = mTitle;
            if (mAmountLabel == "")
                PnlAmount.Visible = false;
            else
                LblAmount.Text = mAmountLabel;

            if (mRemarkLabel == "")
                PnlRemark.Visible = false;
            else
                LblRemark.Text = mRemarkLabel;

            if (PnlAmount.Visible && PnlRemark.Visible)
                Height = 215;
            else
                Height = 130;

            BtnOK.Visible = mBtnOKVisible;
            BtnCancel.Visible = mBtnCancelVisible;
            if (mBtnOKVisible && !mBtnCancelVisible)
                BtnOK.Left  = 100;
            else
                BtnOK.Left = 26;

            //TxtAmount.Properties.EditFormat.FormatString = mDecimalFormat;
            //TxtAmount.Properties.DisplayFormat.FormatString = mDecimalFormat;
            TxtAmount.Properties.Mask.EditMask = mDecimalFormat;

        }

        private void TxtAmount_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void TxtAmount_EditValueChanged(object sender, EventArgs e)
        {
           TxtAmount.Text = String.Format("{0:" + mDecimalFormat + "}", TxtAmount.Text);
        }

        private void MeeRemark_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void rTxtRemark_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) BtnOK_Click(sender, e);
            if (e.KeyChar == 27) BtnCancel_Click(sender, e);

        }


    }
}
