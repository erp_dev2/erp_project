﻿namespace RunSystem
{
    partial class FrmNews
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkPriorityInd = new DevExpress.XtraEditors.CheckEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtNewsTitle = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtNewsCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPhoto = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnPhoto = new DevExpress.XtraEditors.SimpleButton();
            this.MeeNewsLink = new DevExpress.XtraEditors.MemoExEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LblExpDt = new System.Windows.Forms.Label();
            this.DteExpiredDt = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DtePostingDt = new DevExpress.XtraEditors.DateEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.MeeNewsLead = new DevExpress.XtraEditors.MemoExEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPriorityInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNewsLink.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePostingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePostingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNewsLead.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 202);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeNewsLead);
            this.panel2.Controls.Add(this.PbUpload);
            this.panel2.Controls.Add(this.LblExpDt);
            this.panel2.Controls.Add(this.DteExpiredDt);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.DtePostingDt);
            this.panel2.Controls.Add(this.MeeNewsLink);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.BtnPhoto);
            this.panel2.Controls.Add(this.TxtPhoto);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkPriorityInd);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtNewsTitle);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtNewsCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 202);
            // 
            // ChkPriorityInd
            // 
            this.ChkPriorityInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPriorityInd.Location = new System.Drawing.Point(347, 13);
            this.ChkPriorityInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPriorityInd.Name = "ChkPriorityInd";
            this.ChkPriorityInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPriorityInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPriorityInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPriorityInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPriorityInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPriorityInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPriorityInd.Properties.Caption = "Priority";
            this.ChkPriorityInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPriorityInd.Size = new System.Drawing.Size(76, 22);
            this.ChkPriorityInd.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(19, 59);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 14);
            this.label15.TabIndex = 14;
            this.label15.Text = "News Lead";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNewsTitle
            // 
            this.TxtNewsTitle.EnterMoveNextControl = true;
            this.TxtNewsTitle.Location = new System.Drawing.Point(104, 35);
            this.TxtNewsTitle.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNewsTitle.Name = "TxtNewsTitle";
            this.TxtNewsTitle.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNewsTitle.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNewsTitle.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNewsTitle.Properties.Appearance.Options.UseFont = true;
            this.TxtNewsTitle.Properties.MaxLength = 40;
            this.TxtNewsTitle.Size = new System.Drawing.Size(319, 20);
            this.TxtNewsTitle.TabIndex = 13;
            this.TxtNewsTitle.Validated += new System.EventHandler(this.TxtNewsTitle_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(21, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "News Title";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNewsCode
            // 
            this.TxtNewsCode.EnterMoveNextControl = true;
            this.TxtNewsCode.Location = new System.Drawing.Point(104, 14);
            this.TxtNewsCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNewsCode.Name = "TxtNewsCode";
            this.TxtNewsCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNewsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNewsCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNewsCode.Properties.Appearance.Options.UseFont = true;
            this.TxtNewsCode.Properties.MaxLength = 16;
            this.TxtNewsCode.Size = new System.Drawing.Size(154, 20);
            this.TxtNewsCode.TabIndex = 10;
            this.TxtNewsCode.Validated += new System.EventHandler(this.TxtNewsCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "News Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhoto
            // 
            this.TxtPhoto.EnterMoveNextControl = true;
            this.TxtPhoto.Location = new System.Drawing.Point(104, 79);
            this.TxtPhoto.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhoto.Name = "TxtPhoto";
            this.TxtPhoto.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhoto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhoto.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhoto.Properties.Appearance.Options.UseFont = true;
            this.TxtPhoto.Properties.MaxLength = 40;
            this.TxtPhoto.Size = new System.Drawing.Size(280, 20);
            this.TxtPhoto.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(46, 82);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Photo";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPhoto
            // 
            this.BtnPhoto.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPhoto.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPhoto.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPhoto.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPhoto.Appearance.Options.UseBackColor = true;
            this.BtnPhoto.Appearance.Options.UseFont = true;
            this.BtnPhoto.Appearance.Options.UseForeColor = true;
            this.BtnPhoto.Appearance.Options.UseTextOptions = true;
            this.BtnPhoto.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPhoto.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnPhoto.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnPhoto.Location = new System.Drawing.Point(392, 79);
            this.BtnPhoto.Name = "BtnPhoto";
            this.BtnPhoto.Size = new System.Drawing.Size(31, 20);
            this.BtnPhoto.TabIndex = 18;
            this.BtnPhoto.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPhoto.ToolTipTitle = "Latest vendor code";
            this.BtnPhoto.Click += new System.EventHandler(this.BtnPhoto_Click);
            // 
            // MeeNewsLink
            // 
            this.MeeNewsLink.EnterMoveNextControl = true;
            this.MeeNewsLink.Location = new System.Drawing.Point(104, 124);
            this.MeeNewsLink.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNewsLink.Name = "MeeNewsLink";
            this.MeeNewsLink.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLink.Properties.Appearance.Options.UseFont = true;
            this.MeeNewsLink.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLink.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNewsLink.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLink.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNewsLink.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLink.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNewsLink.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLink.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNewsLink.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNewsLink.Properties.MaxLength = 400;
            this.MeeNewsLink.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNewsLink.Properties.ShowIcon = false;
            this.MeeNewsLink.Size = new System.Drawing.Size(319, 20);
            this.MeeNewsLink.TabIndex = 20;
            this.MeeNewsLink.ToolTip = "F4 : Show/hide text";
            this.MeeNewsLink.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNewsLink.ToolTipTitle = "Run System";
            this.MeeNewsLink.Validated += new System.EventHandler(this.MeeNewsLink_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(24, 127);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Link News";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblExpDt
            // 
            this.LblExpDt.AutoSize = true;
            this.LblExpDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExpDt.ForeColor = System.Drawing.Color.Red;
            this.LblExpDt.Location = new System.Drawing.Point(9, 169);
            this.LblExpDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblExpDt.Name = "LblExpDt";
            this.LblExpDt.Size = new System.Drawing.Size(77, 14);
            this.LblExpDt.TabIndex = 23;
            this.LblExpDt.Text = "Expired Date";
            this.LblExpDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteExpiredDt
            // 
            this.DteExpiredDt.EditValue = null;
            this.DteExpiredDt.EnterMoveNextControl = true;
            this.DteExpiredDt.Location = new System.Drawing.Point(104, 166);
            this.DteExpiredDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteExpiredDt.Name = "DteExpiredDt";
            this.DteExpiredDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpiredDt.Properties.Appearance.Options.UseFont = true;
            this.DteExpiredDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpiredDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteExpiredDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteExpiredDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteExpiredDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpiredDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteExpiredDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpiredDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteExpiredDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteExpiredDt.Size = new System.Drawing.Size(109, 20);
            this.DteExpiredDt.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(9, 148);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Posting Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DtePostingDt
            // 
            this.DtePostingDt.EditValue = null;
            this.DtePostingDt.EnterMoveNextControl = true;
            this.DtePostingDt.Location = new System.Drawing.Point(104, 145);
            this.DtePostingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePostingDt.Name = "DtePostingDt";
            this.DtePostingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePostingDt.Properties.Appearance.Options.UseFont = true;
            this.DtePostingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePostingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePostingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePostingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePostingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePostingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePostingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePostingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePostingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePostingDt.Size = new System.Drawing.Size(109, 20);
            this.DtePostingDt.TabIndex = 22;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(104, 102);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(319, 21);
            this.PbUpload.TabIndex = 25;
            // 
            // MeeNewsLead
            // 
            this.MeeNewsLead.EnterMoveNextControl = true;
            this.MeeNewsLead.Location = new System.Drawing.Point(104, 56);
            this.MeeNewsLead.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNewsLead.Name = "MeeNewsLead";
            this.MeeNewsLead.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLead.Properties.Appearance.Options.UseFont = true;
            this.MeeNewsLead.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLead.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNewsLead.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLead.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNewsLead.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLead.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNewsLead.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNewsLead.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNewsLead.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNewsLead.Properties.MaxLength = 400;
            this.MeeNewsLead.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNewsLead.Properties.ShowIcon = false;
            this.MeeNewsLead.Size = new System.Drawing.Size(319, 20);
            this.MeeNewsLead.TabIndex = 15;
            this.MeeNewsLead.ToolTip = "F4 : Show/hide text";
            this.MeeNewsLead.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNewsLead.ToolTipTitle = "Run System";
            this.MeeNewsLead.Validated += new System.EventHandler(this.MeeNewsLead_Validated);
            // 
            // FrmNews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 202);
            this.Name = "FrmNews";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPriorityInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNewsLink.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpiredDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePostingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePostingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNewsLead.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkPriorityInd;
        private System.Windows.Forms.Label label15;
        protected internal DevExpress.XtraEditors.TextEdit TxtNewsTitle;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtNewsCode;
        private System.Windows.Forms.Label label1;
        protected internal DevExpress.XtraEditors.TextEdit TxtPhoto;
        private System.Windows.Forms.Label label3;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeNewsLink;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnPhoto;
        private System.Windows.Forms.Label LblExpDt;
        internal DevExpress.XtraEditors.DateEdit DteExpiredDt;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DtePostingDt;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.ProgressBar PbUpload;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeNewsLead;
    }
}