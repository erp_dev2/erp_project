﻿#region Update
/*
 *  26/12/2022 [TYO/MNET] new app based on ProjectImplementation
 *  10/02/2023 [DITA/MNET] rombak find
 *  03/03/2023 [MYA/MNET] Penambahan warning berupa validasi perbedaan warna untuk mengakomodir ketika task pada project sudah mendekati atau melewati plan end date di Menu Project Implementation
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementation3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmProjectImplementation3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementation3Find(FrmProjectImplementation3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetLueCtCode(ref LueCtCode);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Project Code",
                        "Date",
                        "Cancel",
                        "Status",

                        //6-10
                        "Process",
                        "SO Contract#",
                        "BOQ#",
                        "LOP#",
                        "Customer",

                        //11-15
                        "Project Name",
                        "PIC Sales",
                        "Contract Amount",
                        "Total Resource",
                        "Total Settled Revenue",
                        
                        //16-20
                        "Total Estimated",
                        "Achievement%",
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        
                        //21-23
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Plan End Date"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 80, 60, 100,
                        
                        //6-10
                        100, 150, 150, 150, 200,
                        
                        //11-15
                         200, 150, 150, 130, 130,

                        //16-20
                        130, 130, 130, 130, 130,

                         //21-24
                         130, 130, 130, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] {3, 19, 22, 24 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22 , 23, 24}, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16, 17 }, 2);
           

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23, 24}, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, "A.SOContractDocNo", false);

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SOContractDocNo, D.CtName, E.ProjectName, A.Remark,  ");
                SQL.AppendLine("B.ProjectCode2, G.SiteName, ");
                SQL.AppendLine("Case A.Status  ");
                SQL.AppendLine("	When 'A' Then 'Approved'  ");
                SQL.AppendLine("	When 'C' Then 'Cancelled'  ");
                SQL.AppendLine("	When 'O' Then 'Outstanding'  ");
                SQL.AppendLine("End As StatusDesc, B.BOQDocNo, C.LOPDocNo, H.UserCode PICSales,  ");
                SQL.AppendLine("F.OptDesc ProcessInd, B.Amt As ContractAmt, A.TotalResource, A.TotalRev, A.TotalEstimatedCost, A.TotalAchievement,  ");
                SQL.AppendLine("I.PlanEndDt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");
                SQL.AppendLine("From TblProjectImplementationHdr A  ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo And A.Doctype = '1'  ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo  ");
                SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo  ");
                SQL.AppendLine("Inner Join TblCustomer D On C.CtCode = D.CtCode  ");
                SQL.AppendLine("Inner Join TblLopHdr E On E.DocNo = C.LOPDocNo  ");
                SQL.AppendLine("Inner Join TblOption F ON F.OptCat ='ProjectImplementationProcessInd' AND F.OptCode = A.ProcessInd ");
                SQL.AppendLine("Left Join TblSite G ON E.SiteCode = G.SiteCode ");
                if (mFrmParent.mIsFilterBySite)
                {
                    SQL.AppendLine("And (E.SiteCode Is Null Or ( ");
                    SQL.AppendLine("    E.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(")) ");
                }
                SQL.AppendLine("Left Join TblUser H ON E.PICCode = H.UserCode ");
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("SELECT DocNo, MIN(PlanEndDt) AS PlanEndDt, SettledInd ");
                SQL.AppendLine("FROM TblProjectImplementationDtl ");
                SQL.AppendLine("WHERE SettledInd = 'N' ");
                SQL.AppendLine("GROUP BY DocNo ");
                SQL.AppendLine(")I ON A.DocNo = I.DocNo ");
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString() + Filter + " Group By A.DocNo Order By A.DocDt Desc, A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                           "ProjectCode2", "DocDt", "CancelInd", "StatusDesc", "ProcessInd", 
                            
                           //6-10
                            "SOContractDocNo", "BOQDocNo", "LOPDocNo", "CtName", "ProjectName", 

                            //11-15
                            "PICSales","ContractAmt", "TotalResource", "TotalRev", "TotalEstimatedCost", 
                            
                            //16-20
                            "TotalAchievement", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                            //21
                            "PlanEndDt"
                         
                             
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, false
                    );
                CheckDueDate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode As Col1, B.CtName As Col2 ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Order By B.CtName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        internal void CheckDueDate()
        {
            int Date1 = 0;
            int Date2 = 0;
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 5) == "Approved" && Sm.GetGrdStr(Grd1, row, 6) == "Final" && Sm.GetGrdStr(Grd1, row, 24).Length > 0)
                    {
                        Date1 = Convert.ToInt32(Sm.GetGrdDate(Grd1, row, 24).Substring(0, 8));
                        Date2 = Convert.ToInt32(Sm.GetValue("Select Date_Format(CURDATE(),'%Y%m%d')"));
                        if ((Date1 - Date2) < 0)
                        {
                            Grd1.Rows[row].BackColor = Color.Red;
                        }
                    }
                }
            }

        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
