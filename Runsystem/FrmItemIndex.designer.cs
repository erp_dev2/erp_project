﻿namespace RunSystem
{
    partial class FrmItemIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmItemIndex));
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtItIndex = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueItPlanningGrp = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnItCode = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItPlanningGrp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.BtnItCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueItPlanningGrp);
            this.panel2.Controls.Add(this.TxtItIndex);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(133, 55);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(441, 20);
            this.TxtItName.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(62, 58);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Item Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(65, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Item Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItIndex
            // 
            this.TxtItIndex.EnterMoveNextControl = true;
            this.TxtItIndex.Location = new System.Drawing.Point(133, 76);
            this.TxtItIndex.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItIndex.Name = "TxtItIndex";
            this.TxtItIndex.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItIndex.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItIndex.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItIndex.Properties.Appearance.Options.UseFont = true;
            this.TxtItIndex.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtItIndex.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtItIndex.Size = new System.Drawing.Size(96, 20);
            this.TxtItIndex.TabIndex = 15;
            this.TxtItIndex.Validated += new System.EventHandler(this.TxtItIndex_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(92, 80);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 14);
            this.label15.TabIndex = 14;
            this.label15.Text = "Index";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItPlanningGrp
            // 
            this.LueItPlanningGrp.EnterMoveNextControl = true;
            this.LueItPlanningGrp.Location = new System.Drawing.Point(133, 97);
            this.LueItPlanningGrp.Margin = new System.Windows.Forms.Padding(5);
            this.LueItPlanningGrp.Name = "LueItPlanningGrp";
            this.LueItPlanningGrp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItPlanningGrp.Properties.Appearance.Options.UseFont = true;
            this.LueItPlanningGrp.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItPlanningGrp.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItPlanningGrp.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItPlanningGrp.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItPlanningGrp.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItPlanningGrp.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItPlanningGrp.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItPlanningGrp.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItPlanningGrp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItPlanningGrp.Properties.DropDownRows = 12;
            this.LueItPlanningGrp.Properties.NullText = "[Empty]";
            this.LueItPlanningGrp.Properties.PopupWidth = 500;
            this.LueItPlanningGrp.Size = new System.Drawing.Size(217, 20);
            this.LueItPlanningGrp.TabIndex = 17;
            this.LueItPlanningGrp.ToolTip = "F4 : Show/hide list";
            this.LueItPlanningGrp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItPlanningGrp.EditValueChanged += new System.EventHandler(this.LueItPlanningGrp_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(7, 100);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Planning Group Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(133, 34);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Size = new System.Drawing.Size(217, 20);
            this.TxtItCode.TabIndex = 10;
            // 
            // BtnItCode
            // 
            this.BtnItCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode.Appearance.Options.UseBackColor = true;
            this.BtnItCode.Appearance.Options.UseFont = true;
            this.BtnItCode.Appearance.Options.UseForeColor = true;
            this.BtnItCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode.Image")));
            this.BtnItCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode.Location = new System.Drawing.Point(352, 33);
            this.BtnItCode.Name = "BtnItCode";
            this.BtnItCode.Size = new System.Drawing.Size(24, 21);
            this.BtnItCode.TabIndex = 11;
            this.BtnItCode.ToolTip = "Find Planning Document";
            this.BtnItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode.ToolTipTitle = "Run System";
            this.BtnItCode.Click += new System.EventHandler(this.BtnItCode_Click);
            // 
            // FrmItemIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmItemIndex";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItPlanningGrp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtItIndex;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueItPlanningGrp;
        public DevExpress.XtraEditors.SimpleButton BtnItCode;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        protected internal DevExpress.XtraEditors.TextEdit TxtItName;
    }
}