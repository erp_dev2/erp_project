﻿#region Namespace

using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.Media;


#endregion

namespace RunSystem
{
    public partial class FrmAttendance : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private string EmployeeCode = "";
        private bool NewCode = true;
        string EmpName = "";
        string DeptName = "";

        #endregion

        #region Constructor

        public FrmAttendance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        //public FrmAttendance()
        //{
        //    InitializeComponent();
        //}

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            panel1.Visible = false;

            timer1.Enabled = true;
            ShowImage();
            //ShowInfo();

            string Comp = Sm.GetParameter("ReportTitle1");
            string Company = Sm.GetParameter("ReportTitle2");
            string Company2 = Sm.GetParameter("ReportTitle3");
            LblComp.Text = Comp;
            LblCompany.Text = Company;
            LblCompany2.Text = Company2;
            LblPhone.Text = Sm.GetParameter("ReportTitle4");
            SetFormControl(mState.Insert);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode, TxtDeptName, TxtEmpName
                    }, true);
                    TxtEmpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode,
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDeptName, TxtEmpName
                    }, true);
                    TxtEmpCode.Focus();
                    break;
                case mState.Edit:

                    break;
                default:
                    break;
            }
        }

        #region Button Method

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            SetFormControl(mState.View);
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            try
            {

                if (Sm.GetValue("Select EmpCode From TblEmployee Where BarCodeCode = '" + TxtEmpCode.Text + "'").Length != 0)
                {
                    try
                    {
                        PlaySound(1);
                    }
                    catch
                    {

                    }

                    Cursor.Current = Cursors.WaitCursor;
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Insert Into TblAttendanceLog(EmpCode, Dt, Tm, Machine, ProcessInd, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@EmpCode, Left(CurrentDateTime(), 8), Concat(Right(CurrentDateTime(), 4), Right(substr(replace(curtime(), ':', ''), 1, 6), 2)),  @Machine, 'N',  @CreateBy, CurrentDateTime()); ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text.Substring(0, (TxtEmpCode.Text.Length - 2)));
                    Sm.CmParam<String>(ref cm, "@Machine", System.Environment.MachineName);
                    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

                    Sm.ExecCommand(cm);
                }
                else
                {
                    try
                    {
                        PlaySound(2);
                    }
                    catch
                    {

                    }
                }

            }
            catch (Exception Exc)
            {

            }
            finally
            {
                Cursor.Current = Cursors.Default;

            }
        }

        #endregion

        #endregion

        #endregion

        #region Additional method

        private void ShowImage()
        {
            try
            {
                PicItemLogo.Image = 
                    Image.FromFile(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) +
                    @"\Logo2.png");
            }
            catch (FileNotFoundException)
            {

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowEmp(string EmpCode)
        {
            EmpName = Sm.GetValue("Select EmpName From TblEmployee Where BarcodeCode = '" + EmpCode + "'");
            DeptName = Sm.GetValue("Select B.DeptName From TblEmployee A Inner Join TblDepartment B On A.DeptCode = B.DeptCode Where BarCodeCode = '" + EmpCode + "'");
            TxtEmpName.EditValue = EmpName;
            TxtDeptName.EditValue = DeptName;
        }

        private void PlaySound(int CodeSound)
        {
            string fileSound = Sm.GetParameter("FileSound");

            if (fileSound.Length != 0)
            {
                int caseSwitch = CodeSound;
                switch (caseSwitch)
                {
                    case 1:
                        SoundPlayer pl = new SoundPlayer(@"" + fileSound + "sukses.wav");
                        pl.Play();
                        break;
                    case 2:
                        SoundPlayer pl2 = new SoundPlayer(@"" + fileSound + "gagal.wav");
                        pl2.Play();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                
            }
        }

        private void ShowInfo()
        {
            string HRinfo = Sm.GetValue("select remark From TblHRI " +
                                    "Where docno=(select Max(docno) from tblhri where activeind = 'Y')");
            RtbInfo.Text = HRinfo;
        }

        #endregion

        #region Event

        private void timer1_Tick(object sender, EventArgs e)
        {
            LblHH.Text = String.Format("{0:H:mm:ss}", DateTime.Now);
            LblDay.Text = DateTime.Now.DayOfWeek.ToString() + ", " + String.Format("{0:dd-MMM-yyyy}", DateTime.Now);

        }

        private void FrmAttendance2_Activated(object sender, EventArgs e)
        {
            TxtEmpCode.Focus();
        }

        private void TxtEmpCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                //1. cari ke database, tampilkan

                NewCode = true;
                EmployeeCode = "";
                TxtEmpCode.Focus();
                InsertData();
            }
            else
            {
                if (NewCode)
                {
                    EmployeeCode = "";
                    TxtEmpCode.Text = "";
                }
                EmployeeCode = EmployeeCode + e.KeyChar;
                NewCode = false;
                ShowEmp(EmployeeCode);
                TxtEmpCode.Focus();
            }

        }

        private void TxtEmpCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                e.SuppressKeyPress = true;
            }
        }

        #endregion
    }
}
