﻿#region Update
/*
    31/12/2019 [WED/WMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIMMPackingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmIMMPacking mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMPackingFind(FrmIMMPacking FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueWhsCode(ref LueWhsCode);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Sequence#",
                    "Date",
                    "Cancel",
                    "Warehouse",

                    //6-10
                    "Bin",
                    "Source",
                    "Product's"+Environment.NewLine+"Code",
                    "Product's"+Environment.NewLine+"Description",
                    "Seller",
                    
                    //11-15
                    "Created By",
                    "Created Date",
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date",

                    //16
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 100, 120, 80, 200, 
                    
                    //6-10
                    100, 120, 120, 200, 150,   
                    
                    //11-15
                    100, 100, 100, 100, 100, 

                    //16
                    100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocSeqNo, A.DocDt, A.CancelInd, E.WhsName, D.Bin, ");
            SQL.AppendLine("B.Source, D.ProdCode, F.ProdDesc, G.SellerName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM TblIMMPackingHdr A ");
            SQL.AppendLine("INNER JOIN TblIMMPackingDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("INNER JOIN TblIMMPickingHdr C ON B.PickingDocNo = C.DocNo ");
            SQL.AppendLine("    AND B.PickingDocSeqNo = C.DocSeqNo ");
            SQL.AppendLine("INNER JOIN TblIMMStockSummary D ON A.WhsCode = D.WhsCode ");
            SQL.AppendLine("    AND C.Bin = D.Bin ");
            SQL.AppendLine("    AND B.Source = D.Source ");
            SQL.AppendLine("INNER JOIN TblWarehouse E ON A.WhsCode = E.WhsCode ");
            SQL.AppendLine("INNER JOIN TblIMMProduct F ON D.ProdCode = F.ProdCode ");
            SQL.AppendLine("INNER JOIN TblIMMSeller G ON D.SellerCode = G.SellerCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "D.ProdCode", "F.ProdDesc" });
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "B.Source", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSellerCode.Text, new string[] { "D.SellerCode", "G.SellerName" });
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "A.WhsCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc, A.DocNo;",
                    new string[]
                    {
                        //0
                        "DocNo", 
                            
                        //1-5
                        "DocSeqNo", "DocDt", "CancelInd", "WhsName", "Bin",
                        
                        //6-10
                        "Source", "ProdCode", "ProdDesc", "SellerName", "CreateBy",  
                        
                        //11-13
                        "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        private void TxtSellerCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSellerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Seller");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #endregion
    }
}
