﻿#region Update
/*
   
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentSekuritasFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmInvestmentSekuritas mFrmParent;

        #endregion

        #region Constructor

        public FrmInvestmentSekuritasFind(FrmInvestmentSekuritas FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Institution"+Environment.NewLine+"Code",
                        "Institution"+Environment.NewLine+"Name",
                        "Moving Average",
                        "Category",
                        "Cost Center",
                        
                        //6-10
                        "Cost Category",
                        "COA"+Environment.NewLine+"Account#",
                        "COA"+Environment.NewLine+"Description",
                        "Site",   
                        "Created By",
                        
                        //11-15
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.FilterStr(ref Filter, ref cm, TxtWhsName.Text, new string[] { "A.SekuritasCode", "A.SekuritasName" });

                SQL.AppendLine("Select A.SekuritasCode, A.SekuritasName, ");
                SQL.AppendLine("'N' As MovingAvgInd, ");
                SQL.AppendLine("null as WhsCtName, null as CCName, null as CCtName, A.AcNo, D.AcDesc, null as SiteName, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblInvestmentSekuritas A ");
                SQL.AppendLine("Left Join TblCOA D On A.AcNo=D.AcNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By SekuritasName");              

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(), 
                        new string[]
                        {
                            //0
                            "SekuritasCode", 
                                
                            //1-5
                            "SekuritasName", "MovingAvgInd", "WhsCtName", "CCName", "CCtName", 
                            
                            //6-10
                            "AcNo",  "AcDesc", "SiteName", "CreateBy", "CreateDt", 
                            
                            //11-12
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWhsName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Institution");
        }


        #endregion

        #endregion
    }
}
