﻿namespace RunSystem
{
    partial class FrmUpdatePurchaseInvoiceDueDt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdatePurchaseInvoiceDueDt));
            this.BtnPurchaseInvoice = new DevExpress.XtraEditors.SimpleButton();
            this.DteDueDtNew = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDueDtOld = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnPurchaseInvoice2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOCDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtNew.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtOld.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(422, 0);
            this.panel1.Size = new System.Drawing.Size(70, 123);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(70, 33);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnPurchaseInvoice);
            this.panel2.Controls.Add(this.DteDueDtNew);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDueDtOld);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.BtnPurchaseInvoice2);
            this.panel2.Controls.Add(this.BtnSOCDocNo);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(422, 123);
            // 
            // BtnPurchaseInvoice
            // 
            this.BtnPurchaseInvoice.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPurchaseInvoice.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPurchaseInvoice.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPurchaseInvoice.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPurchaseInvoice.Appearance.Options.UseBackColor = true;
            this.BtnPurchaseInvoice.Appearance.Options.UseFont = true;
            this.BtnPurchaseInvoice.Appearance.Options.UseForeColor = true;
            this.BtnPurchaseInvoice.Appearance.Options.UseTextOptions = true;
            this.BtnPurchaseInvoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPurchaseInvoice.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPurchaseInvoice.Image = ((System.Drawing.Image)(resources.GetObject("BtnPurchaseInvoice.Image")));
            this.BtnPurchaseInvoice.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPurchaseInvoice.Location = new System.Drawing.Point(371, 12);
            this.BtnPurchaseInvoice.Name = "BtnPurchaseInvoice";
            this.BtnPurchaseInvoice.Size = new System.Drawing.Size(24, 19);
            this.BtnPurchaseInvoice.TabIndex = 15;
            this.BtnPurchaseInvoice.ToolTip = "Browse File";
            this.BtnPurchaseInvoice.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPurchaseInvoice.ToolTipTitle = "Run System";
            this.BtnPurchaseInvoice.Click += new System.EventHandler(this.BtnPurchaseInvoice_Click);
            // 
            // DteDueDtNew
            // 
            this.DteDueDtNew.EditValue = null;
            this.DteDueDtNew.EnterMoveNextControl = true;
            this.DteDueDtNew.Location = new System.Drawing.Point(108, 57);
            this.DteDueDtNew.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDtNew.Name = "DteDueDtNew";
            this.DteDueDtNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDtNew.Properties.Appearance.Options.UseFont = true;
            this.DteDueDtNew.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDtNew.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDtNew.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDtNew.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDtNew.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDtNew.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDtNew.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDtNew.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDtNew.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDtNew.Size = new System.Drawing.Size(124, 20);
            this.DteDueDtNew.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(17, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 14);
            this.label1.TabIndex = 19;
            this.label1.Text = "Due Date New";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDtOld
            // 
            this.DteDueDtOld.EditValue = null;
            this.DteDueDtOld.EnterMoveNextControl = true;
            this.DteDueDtOld.Location = new System.Drawing.Point(108, 36);
            this.DteDueDtOld.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDtOld.Name = "DteDueDtOld";
            this.DteDueDtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteDueDtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDtOld.Properties.Appearance.Options.UseBackColor = true;
            this.DteDueDtOld.Properties.Appearance.Options.UseFont = true;
            this.DteDueDtOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDtOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDtOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDtOld.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDtOld.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDtOld.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDtOld.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDtOld.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDtOld.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDtOld.Size = new System.Drawing.Size(124, 20);
            this.DteDueDtOld.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(24, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Due Date Old";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPurchaseInvoice2
            // 
            this.BtnPurchaseInvoice2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPurchaseInvoice2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPurchaseInvoice2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPurchaseInvoice2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPurchaseInvoice2.Appearance.Options.UseBackColor = true;
            this.BtnPurchaseInvoice2.Appearance.Options.UseFont = true;
            this.BtnPurchaseInvoice2.Appearance.Options.UseForeColor = true;
            this.BtnPurchaseInvoice2.Appearance.Options.UseTextOptions = true;
            this.BtnPurchaseInvoice2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPurchaseInvoice2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPurchaseInvoice2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPurchaseInvoice2.Image")));
            this.BtnPurchaseInvoice2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPurchaseInvoice2.Location = new System.Drawing.Point(390, 12);
            this.BtnPurchaseInvoice2.Name = "BtnPurchaseInvoice2";
            this.BtnPurchaseInvoice2.Size = new System.Drawing.Size(24, 19);
            this.BtnPurchaseInvoice2.TabIndex = 16;
            this.BtnPurchaseInvoice2.ToolTip = "Browse File";
            this.BtnPurchaseInvoice2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPurchaseInvoice2.ToolTipTitle = "Run System";
            this.BtnPurchaseInvoice2.Click += new System.EventHandler(this.BtnPurchaseInvoice2_Click);
            // 
            // BtnSOCDocNo
            // 
            this.BtnSOCDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOCDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOCDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOCDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOCDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseFont = true;
            this.BtnSOCDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOCDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOCDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOCDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOCDocNo.Location = new System.Drawing.Point(371, 12);
            this.BtnSOCDocNo.Name = "BtnSOCDocNo";
            this.BtnSOCDocNo.Size = new System.Drawing.Size(24, 19);
            this.BtnSOCDocNo.TabIndex = 21;
            this.BtnSOCDocNo.ToolTip = "Browse File";
            this.BtnSOCDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOCDocNo.ToolTipTitle = "Run System";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(108, 14);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 25;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtDocNo.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(32, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Document#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmUpdatePurchaseInvoiceDueDt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 123);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmUpdatePurchaseInvoiceDueDt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtNew.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtOld.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnPurchaseInvoice;
        internal DevExpress.XtraEditors.DateEdit DteDueDtNew;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDueDtOld;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnPurchaseInvoice2;
        public DevExpress.XtraEditors.SimpleButton BtnSOCDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label4;
    }
}