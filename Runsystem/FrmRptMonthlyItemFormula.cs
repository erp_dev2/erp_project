﻿#region Update
/*
    13/06/2019 [MEY] Reporting baru KSM
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyItemFormula : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string mMonthlyItemFormulaRtPemWhsCode = string.Empty,
            mMonthlyItemFormulaSizingWhsCode = string.Empty, mMonthlyItemFormulaItCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMonthlyItemFormula(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
    
            mMonthlyItemFormulaRtPemWhsCode = Sm.GetParameter("MonthlyItemFormulaRtPemWhsCode");
            mMonthlyItemFormulaSizingWhsCode = Sm.GetParameter("MonthlyItemFormulaSizingWhsCode");
            mMonthlyItemFormulaItCtCode = Sm.GetParameter("MonthlyItemFormulaItCtCode");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();
 
            SQL.AppendLine("Select Tbl.ItCode, Tbl2.ItName, ");
            SQL.AppendLine("IfNull(Sum(Tbl.Awal), 0.00) Awal, IfNull(Sum(Tbl.Awal2), 0.00) Awal2, IfNull(Sum(Tbl.Awal3), 0.00) Awal3, ");
            SQL.AppendLine("IfNull(Sum(Tbl.Pembelian), 0.00) Pembelian, IfNull(Sum(Tbl.Pembelian2), 0.00)  Pembelian2, IfNull(Sum(Tbl.Pembelian3), 0.00)  Pembelian3, ");
            SQL.AppendLine("IfNull(Sum(Tbl.RtSz), 0.00) RtSz, IfNull(Sum(Tbl.RtSz2), 0.00) RtSz2, IfNull(Sum(Tbl.RtSz3), 0.00) RtSz3,  ");
            SQL.AppendLine("IfNull(Sum(Tbl.Sizing), 0.00) Sizing, IfNull(Sum(Tbl.Sizing2), 0.00) Sizing2, IfNull(Sum(Tbl.Sizing3), 0.00) Sizing3, ");
            SQL.AppendLine("IfNull(Sum(Tbl.RtPem), 0.00) RtPem, IfNull(Sum(Tbl.RtPem2), 0.00) RtPem2, IfNull(Sum(Tbl.RtPem3), 0.00) RtPem3, ");
            SQL.AppendLine("IfNull(Sum(Tbl.Fisik), 0.00) Fisik, IfNull(Sum(Tbl.Fisik2), 0.00) Fisik2, IfNull(Sum(Tbl.Fisik3), 0.00) Fisik3, ");
            //SQL.AppendLine("IfNull(Sum(Tbl.PersenJimpitan), 0.00) PersenJimpitan, IfNull(Sum(Tbl.PersenJimpitan2), 0.00) PersenJimpitan2, IfNull(Sum(Tbl.PersenJimpitan3), 0.00) PersenJimpitan3, ");
            SQL.AppendLine(" ((IfNull(Sum(Tbl.Awal), 0.00) / IfNull(Sum(Tbl.Sizing), 1)) * 100) As PersenJimpitan, ");
            SQL.AppendLine(" ((IfNull(Sum(Tbl.Awal2), 0.00) / IfNull(Sum(Tbl.Sizing2), 1)) * 100) As PersenJimpitan2, ");
            SQL.AppendLine(" ((IfNull(Sum(Tbl.Awal3), 0.00) / IfNull(Sum(Tbl.Sizing3), 1)) * 100) As PersenJimpitan3, ");
            SQL.AppendLine("0.00 As Selisih, 0.00 As Selisih2, 0.00 As Selisih3, ");
            SQL.AppendLine("0.00 As StokLapangan, 0.00 As StokLapangan2, 0.00 As StokLapangan3, ");
            SQL.AppendLine("0.00 As Total, 0.00 As Total2, 0.00 As Total3 ");

            SQL.AppendLine("From ( ");

            SQL.AppendLine("Select A.ItCode, B.Init As Awal, B.Init2 As Awal2, B.Init3 As Awal3, ");
            SQL.AppendLine("C.Qty As Pembelian, C.Qty2 As Pembelian2, C.Qty3 As Pembelian3,  ");
            SQL.AppendLine("D.Qty As RtSz, D.Qty2 As RtSz2, D.Qty3 As RtSz3, ");
            SQL.AppendLine("E.Qty As Sizing, E.Qty2 As Sizing2, E.Qty3 As Sizing3, ");
            SQL.AppendLine("F.Qty As RtPem, F.Qty2 As RtPem2, F.Qty3 As RtPem3, ");
            SQL.AppendLine("((IfNull(B.Init,0.00) + IfNull(C.Qty,0.00)+ IfNull(D.Qty,0.00)) - (IfNull(E.Qty,0.00) + IfNull(F.Qty,0.00)))  As Fisik, ");
            SQL.AppendLine("((IfNull(B.Init2,0.00) + IfNull(C.Qty2,0.00)+ IfNull(D.Qty2,0.00)) - (IfNull(E.Qty2,0.00) + IfNull(F.Qty2,0.00))) As Fisik2, ");
            SQL.AppendLine("((IfNull(B.Init3,0.00) + IfNull(C.Qty3,0.00)+ IfNull(D.Qty3,0.00)) - (IfNull(E.Qty3,0.00) + IfNull(F.Qty3,0.00))) As Fisik3 ");
            //SQL.AppendLine(", ((B.Init/IfNull(E.Qty,1)) * 100 ) As PersenJimpitan, ");
            //SQL.AppendLine("((B.Init2/IfNull(E.Qty2,1)) * 100 ) As PersenJimpitan2, ");
            //SQL.AppendLine("((B.Init3/IfNull(E.Qty3,1)) * 100 ) As PersenJimpitan3 ");

            SQL.AppendLine("From  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select Distinct ItCode From TblStockMovement  ");
            SQL.AppendLine("Where Left(DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("And WhsCode=@WhsCode  ");
            SQL.AppendLine(") A ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("           Select Tbl.ItCode, IfNull(Sum(Tbl.Qty), 0.00) As Init, IfNull(Sum(Tbl.Qty2), 0.00) As Init2, IfNull(Sum(Tbl.Qty3), 0.00) As Init3, Sum(Tbl.Total) As TInitPrice ");
            SQL.AppendLine("          From ( ");
            SQL.AppendLine("                Select T1.ItCode, T1.Qty, T1.Qty2, T1.Qty3, (T1.Qty * T2.ExcRate * T2.UPrice) As Total ");
            SQL.AppendLine("               From ( ");
            SQL.AppendLine("                    Select A.ItCode, A.Source, ");
            SQL.AppendLine("                   Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            SQL.AppendLine("                   From TblStockMovement A ");
            SQL.AppendLine("                  Where Left(A.DocDt, 6)<Concat(@Yr, @Mth)  ");       
            SQL.AppendLine("                   And A.WhsCode= @WhsCode ");
            SQL.AppendLine("                   Group By A.ItCode, A.Source  ");
            SQL.AppendLine("                  Having Sum(A.Qty)<>0.00 ");
            SQL.AppendLine("              ) T1 ");
            SQL.AppendLine("               Inner Join TblStockPrice T2 On T1.Source = T2.Source  ");
            SQL.AppendLine("            ) Tbl ");
            SQL.AppendLine("            Group By Tbl.ItCode   ");        
            SQL.AppendLine("       ) B On A.ItCode = B.ItCode  ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.ItCode, T3.Qty, T3.Qty2, T3.Qty3 From TblRecvVdHdr T2 ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T3.DocNo=T2.DocNo And T3.CancelInd = 'N' And T3.Status = 'A' ");
            SQL.AppendLine("    Where Left(T2.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("    And T2.WhsCode=@WhsCode  ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.ItCode, T4.Qty, T4.Qty2, T4.Qty3 From TblRecvWhs2Dtl T4 ");
            SQL.AppendLine("    Inner Join TblRecvWhs2Hdr T5 On T4.DocNo=T5.DocNo And T4.CancelInd = 'N' And T4.Status = 'A' ");
            SQL.AppendLine("    Where Left(T5.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("    And T5.WhsCode=@WhsCode ");
            SQL.AppendLine(") D On A.ItCode=D.ItCode  ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T6.ItCode, T6.Qty, T6.Qty2, T6.Qty3 From TblStockMovement T6 ");
            SQL.AppendLine("    Inner Join TblItem T7 On T6.ItCode = T7.ItCode And T7.ItCtCode = '(" + mMonthlyItemFormulaItCtCode + ")' ");
            SQL.AppendLine("    And Left(T6.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("    And T6.WhsCode= '(" + mMonthlyItemFormulaSizingWhsCode + ")' ");
            SQL.AppendLine(") E On A.ItCode=E.ItCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T8.ItCode, T8.Qty, T8.Qty2, T8.Qty3 From TblDOVdDtl T8 ");
            SQL.AppendLine("    Inner Join TblDOVdHdr T9 On T8.DocNo = T9.DocNo And T8.CancelInd = 'N' ");
            SQL.AppendLine("    Where Left(T9.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("      And T9.WhsCode= '(" + mMonthlyItemFormulaRtPemWhsCode + ")' ");
            SQL.AppendLine(") F On A.ItCode=F.ItCode ");

            SQL.AppendLine(") Tbl Inner Join TblItem Tbl2 On Tbl.ItCode = Tbl2.ItCode ");
            SQL.AppendLine("Group By Tbl.ItCode, Tbl2.ItName ");
            SQL.AppendLine("Order By Tbl2.ItName" );

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 33;
            //Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 100;
            Grd1.Header.Cells[0, 1].Value = "Item Code";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Jenis Formula";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[1, 3].Value = "Awal";
            Grd1.Header.Cells[1, 3].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 3].SpanCols = 3;
            Grd1.Header.Cells[0, 3].Value = "Quantity 1";
            Grd1.Header.Cells[0, 4].Value = "Quantity 2";
            Grd1.Header.Cells[0, 5].Value = "Quantity 3";
            Grd1.Cols[3].Width = 100;
            Grd1.Cols[4].Width = 100;
            Grd1.Cols[5].Width = 100;

            Grd1.Header.Cells[1, 6].Value = "Pembelian";
            Grd1.Header.Cells[1, 6].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 6].SpanCols = 3;
            Grd1.Header.Cells[0, 6].Value = "Quantity 1";
            Grd1.Header.Cells[0, 7].Value = "Quantity 2";
            Grd1.Header.Cells[0, 8].Value = "Quantity 3";
            Grd1.Cols[6].Width = 100;
            Grd1.Cols[7].Width = 100;
            Grd1.Cols[8].Width = 100;

            Grd1.Header.Cells[1, 9].Value = "RtSz";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 3;
            Grd1.Header.Cells[0, 9].Value = "Quantity 1";
            Grd1.Header.Cells[0, 10].Value = "Quantity 2";
            Grd1.Header.Cells[0, 11].Value = "Quantity 3";
            Grd1.Cols[9].Width = 100;
            Grd1.Cols[10].Width = 100;
            Grd1.Cols[11].Width = 100;

            Grd1.Header.Cells[1, 12].Value = "Sizing";
            Grd1.Header.Cells[1, 12].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 12].SpanCols = 3;
            Grd1.Header.Cells[0, 12].Value = "Quantity 1";
            Grd1.Header.Cells[0, 13].Value = "Quantity 2";
            Grd1.Header.Cells[0, 14].Value = "Quantity 3";
            Grd1.Cols[12].Width = 100;
            Grd1.Cols[13].Width = 100;
            Grd1.Cols[14].Width = 100;

            Grd1.Header.Cells[1, 15].Value = "RtPem";
            Grd1.Header.Cells[1, 15].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 15].SpanCols = 3;
            Grd1.Header.Cells[0, 15].Value = "Quantity 1";
            Grd1.Header.Cells[0, 16].Value = "Quantity 2";
            Grd1.Header.Cells[0, 17].Value = "Quantity 3";
            Grd1.Cols[15].Width = 100;
            Grd1.Cols[16].Width = 100;
            Grd1.Cols[17].Width = 100;

            Grd1.Header.Cells[1, 18].Value = "Fisik";
            Grd1.Header.Cells[1, 18].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 18].SpanCols = 3;
            Grd1.Header.Cells[0, 18].Value = "Quantity 1";
            Grd1.Header.Cells[0, 19].Value = "Quantity 2";
            Grd1.Header.Cells[0, 20].Value = "Quantity 3";
            Grd1.Cols[18].Width = 100;
            Grd1.Cols[19].Width = 100;
            Grd1.Cols[20].Width = 100;

            Grd1.Header.Cells[1, 21].Value = "Selisih";
            Grd1.Header.Cells[1, 21].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 21].SpanCols = 3;
            Grd1.Header.Cells[0, 21].Value = "Quantity 1";
            Grd1.Header.Cells[0, 22].Value = "Quantity 2";
            Grd1.Header.Cells[0, 23].Value = "Quantity 3";
            Grd1.Cols[21].Width = 100;
            Grd1.Cols[22].Width = 100;
            Grd1.Cols[23].Width = 100;

            Grd1.Header.Cells[1, 24].Value = "% Jimpitan";
            Grd1.Header.Cells[1, 24].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 24].SpanCols = 3;
            Grd1.Header.Cells[0, 24].Value = "Quantity 1";
            Grd1.Header.Cells[0, 25].Value = "Quantity 2";
            Grd1.Header.Cells[0, 26].Value = "Quantity 3";
            Grd1.Cols[24].Width = 100;
            Grd1.Cols[25].Width = 100;
            Grd1.Cols[26].Width = 100;

            Grd1.Header.Cells[1, 27].Value = "Stok Lapangan";
            Grd1.Header.Cells[1, 27].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 27].SpanCols = 3;
            Grd1.Header.Cells[0, 27].Value = "Quantity 1";
            Grd1.Header.Cells[0, 28].Value = "Quantity 2";
            Grd1.Header.Cells[0, 29].Value = "Quantity 3";
            Grd1.Cols[27].Width = 100;
            Grd1.Cols[28].Width = 100;
            Grd1.Cols[29].Width = 100;

            Grd1.Header.Cells[1, 30].Value = "Total";
            Grd1.Header.Cells[1, 30].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 30].SpanCols = 3;
            Grd1.Header.Cells[0, 30].Value = "Quantity 1";
            Grd1.Header.Cells[0, 31].Value = "Quantity 2";
            Grd1.Header.Cells[0, 32].Value = "Quantity 3";
            Grd1.Cols[30].Width = 100;
            Grd1.Cols[31].Width = 100;
            Grd1.Cols[32].Width = 100;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32  }, 0);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;

            Sm.GrdColInvisible(Grd1, new int[] { 1, 21, 22, 23, 27, 28, 29 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }
        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                //string Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(), //+ Filter + " Group By A.WhsCode, D.WhsName, A.ItCode, C.ItName, B.DocDt, B.RemarkQC, B.Lot, B.VdCode, E.VdName, B.ExSupplier, B.PropCode; ",
                        new string[]
                        { 
                            //0
                            "ItCode", 
                            
                            //1-5
                            "ItName", "Awal", "Awal2", "Awal3", "Pembelian", 
                            
                            //6-10
                            "Pembelian2", "Pembelian3",  "RtSz",  "RtSz2", "RtSz3", 
                            
                            //11-15
                            "Sizing", "Sizing2", "Sizing3", "RtPem", "RtPem2",

                            //16-20
                            "RtPem3", "Fisik", "Fisik2", "Fisik3", "Selisih",

                            //21-25
                            "Selisih2", "Selisih3","PersenJimpitan","PersenJimpitan2", "PersenJimpitan3", 

                            //25-30
                            "StokLapangan", "StokLapangan2", "StokLapangan3", "Total", "Total2",

                            //31
                            "Total3"
                        },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 19);
                          //   Grd.Cells[Row, 21].Value = Sm.GetGrdDec(Grd, Row, 12) + Sm.GetGrdDec(Grd, Row, 15) + Sm.GetGrdDec(Grd, Row, 18);
                          //  Grd.Cells[Row, 22].Value = Sm.GetGrdDec(Grd, Row, 13) + Sm.GetGrdDec(Grd, Row, 16) + Sm.GetGrdDec(Grd, Row, 19);
                          //  Grd.Cells[Row, 23].Value = Sm.GetGrdDec(Grd, Row, 14) + Sm.GetGrdDec(Grd, Row, 17) + Sm.GetGrdDec(Grd, Row, 20);
                             }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                   3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
                });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetLueWhsCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

                SQL.AppendLine("Select WhsCode As Col1, WhsName As Col2 From TblWarehouse  ");
                SQL.AppendLine("Where Find_In_Set(IfNull(WhsCode, ''), ");
                SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParCode='MonthlyItemFormulaWhsCode'), '')) ");
                SQL.AppendLine("Order By WhsName; ");

                Sm.SetLue2(
                        ref Lue,
                        SQL.ToString(),
                        0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        //override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        //{
        //    int c = e.ColIndex;
        //    if (Sm.IsGrdColSelected(new int[] { 8, 9, 11, 13 }, c))
        //    {
        //        decimal Total = 0m;
        //        for (int r = 0; r < Grd1.Rows.Count; r++)
        //            if (Sm.GetGrdStr(Grd1, r, c).Length != 0) Total += Sm.GetGrdDec(Grd1, r, c);
        //        Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
        //    }
        //}

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode));
        }
        #endregion

    }
}
