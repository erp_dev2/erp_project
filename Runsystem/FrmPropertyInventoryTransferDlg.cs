﻿#region Update
/*
    27/12/2022 [SET/BBT] Menu Dlg baru
    11/01/2023 [SET/BBT] penyesuaian source Property Inventory indikator Complete & Active = Y
    12/01/2022 [MAU/BBT] Tambah validasi show data
    13/01/2022 [MAU/BBT] Tambah validasi show data (Remstockvalue)
    14/01/2022 [MAU/BBT] Tambah validasi show data (Remstockvalue)
    17/01/2023 [SET/BBT] tidak menampilkan pop up ketika klik bagian property code
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryTransferDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPropertyInventoryTransfer mFrmParent;
        private string 
            mSQL = string.Empty, 
            mCCCode = string.Empty,
            mLocCode = string.Empty;
        private int mCurRow;

        #endregion

        #region Constructor

        public FrmPropertyInventoryTransferDlg(FrmPropertyInventoryTransfer FrmParent, int CurRow)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sl.SetLueCCCode(ref LueCCCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.RegistrationDt, A.PropertyCode, A.Propertyname, C.Sitename, A.InventoryQty, A.UoMCode, A.UPrice, A.PropertyInventoryValue, ");
            SQL.AppendLine("B.PropertyCategoryName, B.AcNo1 AcNo, D.AcDesc COA, A.PropertyCategoryCode, A.SiteCode, A.CCCode  ");
            SQL.AppendLine("From TblPropertyInventoryHdr A  ");
            SQL.AppendLine("Inner JOIN tblpropertyinventorycategory B ON A.PropertyCategoryCode = B.PropertyCategoryCode And A.CancelInd = 'N' AND A.ActInd = 'Y' And A.Status = 'A' AND A.RemStockQty > 0  ");
            SQL.AppendLine("Inner JOIN tblsite C ON A.SiteCode = C.SiteCode ");
            SQL.AppendLine("Inner JOIN tblcoa D ON B.AcNo1 = D.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' And A.CancelInd = 'N' And CompleteInd = 'Y' ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Date of Registration",
                        "Property Code", 
                        "", 
                        "Property Name",

                        //6-10
                        "Site",
                        "Inventory Quantity",
                        "UoM",
                        "Unit Price",
                        "Property Inventory" + Environment.NewLine + "Value",

                        //11-15
                        "Property Category",
                        "COA#",
                        "COA Description",
                        "Property Category Code",
                        "Site Code",

                        //16
                        "Cost Center Code"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 150, 20, 250,

                        //6-10
                        150, 100, 100, 200, 200,

                        //11-12
                        150, 150, 200, 100, 100, 

                        //16
                        100, 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            //Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And A.PropertyCode Not In ("+mFrmParent.GetSelectedItem()+")";

                var cm = new MySqlCommand();

                //Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                //Sm.CmParam<String>(ref cm, "@LocCode", mLocCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPropertyCode.Text, new string[] { "A.PropertyCode", "A.PropertyName", "A.DisplayName" });

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By A.PropertyName;",
                        new string[] 
                        { 
                            //0
                            "RegistrationDt",
 
                            //1-5
                            "PropertyCode", "PropertyName", "SiteName", "InventoryQty", "UomCode", 

                            //6-10
                            "UPrice", "PropertyInventoryValue", "PropertyCategoryName", "AcNo", "COA", 
                            
                            //11-13
                            "PropertyCategoryCode", "SiteCode", "CCCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;
                        mFrmParent.SetLuePropertyCtCode(ref mFrmParent.LuePropertyCtFrom, Sm.GetGrdStr(Grd1, Row, 14));
                        mFrmParent.SetLuePropertyCtCode(ref mFrmParent.LuePropertyCtTo, Sm.GetGrdStr(Grd1, Row, 14));
                        Sm.SetLue(mFrmParent.LueSiteCodeFrom, Sm.GetGrdStr(Grd1, Row, 15));
                        Sm.SetLue(mFrmParent.LueSiteCodeTo, Sm.GetGrdStr(Grd1, Row, 15));
                        Sm.SetLue(mFrmParent.LueCCCodeFrom, Sm.GetGrdStr(Grd1, Row, 16));
                        Sm.SetLue(mFrmParent.LueCCCodeTo, Sm.GetGrdStr(Grd1, Row, 16));
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        mFrmParent.LuePropertyCtTo, mFrmParent.LueCCCodeTo, mFrmParent.LueSiteCodeTo
                    }, false);

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row, 10);
                        mFrmParent.Grd1.Rows.Add();
                        Close();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 Property.");
        }


        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 4)) 
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmPropertyInventory(mFrmParent.mMenuCode);
            //    f.Tag = mFrmParent.mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mPropertyCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
            //    f.ShowDialog();
            //}
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmPropertyInventory(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPropertyCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }
        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void LueCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void LueSite_Validated(object sender, EventArgs e)
        {

            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

    }
}
