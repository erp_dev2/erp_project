﻿#region Update
/*
    19/02/2018 [TKG] tambah group user
    10/05/2022 [BRI/PRODUCT] tambah remark berdasarkan param IsLoginHistoryRecordFailedLogin
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmRptLog : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsLoginHistoryRecordFailedLogin = false;

        #endregion

        #region Constructor

        public FrmRptLog(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteLog1, ref DteLog2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.UserCode, B.UserName, C.GrpName, left(A.Login,12) as Login, Logout, A.Ip, A.Machine, A.SysVer ");
            if (mIsLoginHistoryRecordFailedLogin)
                SQL.AppendLine(", A.Remark ");
            else
                SQL.AppendLine(", NULL as Remark ");
            SQL.AppendLine("From TblLog A ");
            SQL.AppendLine("Inner Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("Left Join TblGroup C On B.GrpCode=C.GrpCode ");
            SQL.AppendLine("Where Left(A.Login, 8) Between @DocDt1 And @DocDt2 ");
            if (mIsLoginHistoryRecordFailedLogin)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.UserCode, B.UserName, C.GrpName, left(A.Login,12) as Login, Logout, A.Ip, A.Machine, A.SysVer ");
                SQL.AppendLine(", A.Remark ");
                SQL.AppendLine("From TblLogError A ");
                SQL.AppendLine("Inner Join TblUser B On A.UserCode=B.UserCode ");
                SQL.AppendLine("Left Join TblGroup C On B.GrpCode=C.GrpCode ");
                SQL.AppendLine("Where Left(A.Login, 8) Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("Order By Login; ");
            }
            else
                SQL.AppendLine("Order By Login; ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "User Code", 
                        "User Name",
                        "Group",
                        "Login Date",
                        "Login"+Environment.NewLine+"Time",
                        
                        //6-10
                        "Logout Date",
                        "Logout"+Environment.NewLine+"Time",
                        "IP",
                        "Machine",
                        "Version",

                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 200, 100, 80, 
                        
                        //6-10
                        100, 80, 100, 100, 80,

                        //11
                        200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 6 });
            Sm.GrdFormatTime(Grd1, new int[] { 5, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (!mIsLoginHistoryRecordFailedLogin)
                Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteLog1, "Start date") ||
                Sm.IsDteEmpty(DteLog2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteLog1, ref DteLog2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteLog1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteLog2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "UserCode", 

                            //1-5
                            "UserName", "GrpName", "Login", "Logout", "Ip", 
                            
                            //6-8
                            "Machine", "SysVer", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void GetParameter()
        {
            mIsLoginHistoryRecordFailedLogin = Sm.GetParameterBoo("IsLoginHistoryRecordFailedLogin");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteLog1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteLog2).Length == 0) DteLog2.EditValue = DteLog1.EditValue;
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
        
        #endregion
    }
}
