﻿#region Update
/* 
    04/04/2017 [TKG] Untuk wasted result, harganya 0.
    04/08/2017 [HAR] ubah query untuk mendapatkan formulasi BOM paling MAX berdasarkan date pembuatan formulasi
    20/06/2017 [TKG] Update remark di journal
    15/02/2017 [ARI] tambah printot workcenter cucuk
    26/03/2017 [TKG] SFC nomor dokumen 8 karakter
    08/08/2017 [TKG] merubah proses generate journal# spy lbh cepat.
    13/02/2019 [MEY] Menambahkan filter Machine
    08/07/2019 [DITA] Filter Machine di SHOP FLOOR CONTROL diganti berdasarkan Display Name Master Assetnya.
    09/07/2019 [WED] Short Code di Asset ditambahkan di Batch Number Item Result, berdasarkan parameter IsAssetShortCodeAddedToSFCResultBatchNo
    16/07/2019 [TKG] bug, saat input item result secara manual, maka wasted item akan diset berdasarkan item category.
    16/12/2019 [TKG/IMS] journal untuk moving average
    09/01/2020 [WED/MMM] tambah parameter IsSFCUseOptionBatchNoFormula, agar batchno nya menyesuaikan rule yg ada, tapi bersifat default
    09/01/2020 [WED/MMM] tambah untuk BatchNo Sacheting, berdasarkan parameter IsSFCUseBatchCategoryCode
    25/08/2020 [DITA/MGI] menambah lue group kerja pada menu transaksi
    26/08/2020 [TKG/IOK] saat tekan enter di batch#, cursor langsung ke kolom di bawahnya
    12/10/2020 [ICA/MGI] generate otomatis batch# production result based on parameter IsUseProductionWorkGroup
    13/08/2021 [TKG/ALL] tambah validasi setting journal
    26/01/2022 [TKG/IOK] ubah GetParameter() dan proses Save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControl2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mWhsCode2 = string.Empty, mDocNo = string.Empty, //if this application is called from other application;
            mSFCDefaultBatchNo = string.Empty, mBatchNo = string.Empty;
        internal FrmShopFloorControl2Find FrmFind;
        private int mNumberOfPlanningUomCode = 1;
        internal int mNumberOfInventoryUomCode = 1;
        private decimal mSFCDefaultDocDt = 0m;
        private string 
            mDocType = "24", //SFC
            mDocType2 = "23", // Receiving From Production
            mDocDt = string.Empty, 
            mProductionShiftCode = "", 
            mWhsCode = string.Empty, 
            mPPDocNo = string.Empty,
            mDocNoSeqNumberType = "1",
            mRuleForShowingAssetName = "",
            mAssetShortCode = string.Empty;
        internal bool 
            mIsSFCAutoCopyData = false, 
            mIsSFCShowLeaveRemark = false,
            mIsSFCShowRecvWhsWithTRRemark = false,
            mIsAssetShortCodeAddedToSFCResultBatchNo = false,
            mIsSFCUseOptionBatchNoFormula = false,
            mIsUseProductionWorkGroup = false;
        private bool 
            mIsSFCAutoRecvProduction = false,
            mIsAutoJournalActived = false, 
            mIsMovingAvgEnabled = false,
            mIsSFCUseBatchCategoryCode = false,
            mIsJournalValidationShopFloorControl2Enabled = false;
        private List<Stock> ls = null;
        private List<BatchNo> lb = null;
        internal bool mIsCodeForSFCStandard = false;
        //private iGCopyPasteManager fCopyPasteManager;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmShopFloorControl2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Shop Floor Control";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtPPDocNo, MeePPRemark, TxtWorkCenterDocNo, TxtWorkCenterDocName, 
                        TxtWhsCode2
                    }, true);
                
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();

                ls = new List<Stock>();
                lb = new List<BatchNo>();
                LueBatchNo.Visible = false;
                LueItCode.Visible = false;
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                SetLueItCode(ref LueItCode);
                Sl.SetLueProductionShiftCode(ref LueProductionShiftCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                if (!mIsSFCUseBatchCategoryCode)
                {
                    LblBatchCategoryCode.Visible = LueBatchCategoryCode.Visible = false;
                }
                Sl.SetLueOption(ref LueBatchCategoryCode, "SFCBatchCategoryCode");
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                if (!mIsUseProductionWorkGroup)
                {
                    LueProductionWorkGroup.Visible = LblProductionWorkGroup.Visible = false;

                }
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Batch#",
                        
                        //6-10
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Price", 
                        
                        //11
                        "Wasted"
                    },
                     new int[] 
                    {
                        //0
                        40, 

                        //1-5
                        20, 120, 20, 250, 250,  
                        
                        //6-10
                        100, 80, 100, 80, 0,

                        //11
                        60
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 11 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 8, 9, 10 }, false);

            ShowPlanningUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Code",
                        "Direct Labor",
                        "Old Code",
                        "Position",
                        
                        //6-9
                        "Department",
                        "Coordinator",
                        "Quantity",
                        "Leave"
                    },
                     new int[] 
                    {
                        //0
                        40, 
                        
                        //1-5
                        20, 80, 300, 80, 150,  
                        
                        //6-9
                        150, 80, 80, 80
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 7 });
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 6, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 9 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 23;
            Grd3.FrozenArea.ColCount = 6;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Local Code",
                        "Item's Name",
                        
                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",   
                     
                        //11-15
                        "Bin",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock 2",

                        //16-20
                        "Quantity 2",
                        "Uom 2",
                        "Stock 3",
                        "Quantity 3",
                        "Uom 3",

                        //21-22
                        "Remark",
                        "Price"
                    },
                     new int[] 
                    {
                        //0
                        40,
 
                        //1-5
                        20, 80, 20, 80, 250,
                        
                        //6-10
                        0, 80, 200, 180, 60,
                        
                        //11-15
                        60, 100, 100, 80, 100, 
                        
                        //16-20
                        100, 80, 100, 100, 80,  
                        
                        //21-22
                        300, 0
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 12, 13, 15, 16, 18, 19, 22 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 1, 3 });
            Sm.GrdColInvisible(Grd3, new int[] { 2, 3, 4, 6, 7, 9, 10, 15, 16, 17, 18, 19, 20, 21, 22 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17, 18, 20 });
            ShowInventoryUomCode();
            //fCopyPasteManager = new iGCopyPasteManager(Grd3);

            #endregion
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 }, true);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd3, new int[] { 15, 16, 17 }, true);
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd3, new int[] { 15, 16, 17, 18, 19, 20 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { ChkCancelInd, DteDocDt, LueProductionShiftCode, LueWhsCode, LueMachineCode, MeeRemark, LueBatchCategoryCode, LueProductionWorkGroup }, true);
                   
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11  });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 8, 13, 16, 19, 21, 22 });
                    BtnPPDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueProductionShiftCode, LueWhsCode, LueMachineCode, MeeRemark, LueBatchCategoryCode, LueProductionWorkGroup }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 4, 6, 8});
                    if (!mIsUseProductionWorkGroup) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 7, 8 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1, 8, 13, 16, 19, 21 });
                    BtnPPDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ls.Clear();
            lb.Clear();
            mBatchNo = string.Empty;
            mWhsCode2 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueProductionShiftCode, TxtPPDocNo, MeePPRemark, 
                TxtWorkCenterDocNo, TxtWorkCenterDocName, TxtWhsCode2, LueWhsCode, LueMachineCode, MeeRemark, LueBatchCategoryCode, 
                LueProductionWorkGroup
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
            mAssetShortCode = string.Empty;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 8, 10 });
            SetDNo(Grd1);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdBoolValueFalse(ref Grd2, 0, new int[] { 7 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8 });
            SetDNo(Grd2);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 12, 13, 15, 16, 18, 19, 22 });
            SetDNo(Grd3);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmShopFloorControl2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            BtnInsertClick();
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            
            if (mIsSFCUseBatchCategoryCode && Sm.GetLue(LueBatchCategoryCode).Length == 0)
            {
                if (Sm.StdMsgYN("Question", "Batch is empty. Do you want to proceed ?") == DialogResult.No) 
                { 
                    LueBatchCategoryCode.Focus(); return; 
                }
            }

            Cursor.Current = Cursors.WaitCursor;

            ProcessComputeResultPrice();

            if (mIsUseProductionWorkGroup)
            {
                if (Grd1.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        GenerateBatchNo(i);
                    }
                }
            }

            string DocNo = string.Empty;
            
            if (mDocNoSeqNumberType=="2")
                DocNo = Sm.GenerateDocNo2(Sm.GetDte(DteDocDt), "ShopFloorControl", "TblShopFloorControlHdr");
            else
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ShopFloorControl", "TblShopFloorControlHdr");

            string RecvProductionDocNo = string.Empty;

            if (mIsSFCAutoRecvProduction)
            {
                if (mDocNoSeqNumberType == "2")
                    RecvProductionDocNo = Sm.GenerateDocNo2(Sm.GetDte(DteDocDt), "RecvProduction", "TblRecvProductionHdr");
                else
                    RecvProductionDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvProduction", "TblRecvProductionHdr");
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveShopFloorControlHdr(DocNo));

            if (Grd1.Rows.Count > 1)
            {
                cml.Add(SaveShopFloorControlDtl(DocNo));

                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
                //        cml.Add(SaveShopFloorControlDtl(DocNo, Row));

                if (mIsSFCAutoRecvProduction)
                {
                    cml.Add(SaveRecvProductionHdr(RecvProductionDocNo));
                    cml.Add(SaveRecvProductionDtl(DocNo, RecvProductionDocNo));
                    cml.Add(SaveStock(RecvProductionDocNo));
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                cml.Add(SaveShopFloorControl2Dtl(DocNo));
                //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveShopFloorControl2Dtl(DocNo, Row));
            }

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveShopFloorControl3Dtl(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                //        cml.Add(SaveShopFloorControl3Dtl(DocNo, Row));

                cml.Add(SaveStockMovement(DocNo, "N"));

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                        cml.Add(SaveStockSummary(1, Row));
            }

            if (mIsAutoJournalActived)
            {
                cml.Add(SaveJournal(DocNo));
                if (mIsSFCAutoRecvProduction)
                    cml.Add(SaveJournal2(RecvProductionDocNo));
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private void BtnInsertClick()
        {
            try
            {
                if (mIsSFCShowLeaveRemark) Sm.GrdColInvisible(Grd2, new int[] { 9 }, true);
                ClearData();
                SetFormControl(mState.Insert);
                SetDocDtDefault();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProductionShiftCode, "Shift") ||
                Sm.IsTxtEmpty(TxtPPDocNo, "Production planning#", false) ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsPPAlreadyCancelled() ||
                IsPPAlreadyFulfilled() ||
                //IsPPAlreadyProcessedToSFC() ||
                IsGrdExceedMaxRecords() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrd2ValueNotValid() ||
                IsBatchNoNotValid() ||
                IsGrd3ValueNotValid() ||
                IsJournalSettingInvalid();
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationShopFloorControl2Enabled) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select Distinct ItCtName From ( ");

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And (B.AcNo Is Null Or B.AcNo2 Is Null) ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd3, r, 2);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_1_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_1_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") ");

            if (mIsSFCAutoRecvProduction)
            {
                IsFirst = true;

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
                SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And (B.AcNo Is Null Or B.AcNo3 Is Null) ");
                SQL.AppendLine("And A.ItCode In (");
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (ItCode.Length > 0)
                    {
                        if (IsFirst)
                            IsFirst = false;
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine("@ItCode_2_" + r.ToString());
                        Sm.CmParam<String>(ref cm, "@ItCode_2_" + r.ToString(), ItCode);
                    }
                }
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsBatchNoNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 9).Length!=0)
                    for (int Row2 = 0; Row2 < Grd3.Rows.Count; Row2++)
                    {
                        if (Row != Row2 && Sm.CompareGrdStr(Grd3, Row, 9, Grd3, Row2, 9))
                        {
                            Sm.StdMsg(mMsgType.Warning, 
                                "Item's Code : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine +
                                "Item's Name : " + Sm.GetGrdStr(Grd3, Row, 5) + Environment.NewLine +
                                "Property : " + Sm.GetGrdStr(Grd3, Row, 7) + Environment.NewLine +
                                "Batch# : " + Sm.GetGrdStr(Grd3, Row, 8) + Environment.NewLine +
                                "Lot : " + Sm.GetGrdStr(Grd3, Row, 10) + Environment.NewLine +
                                "Bin : " + Sm.GetGrdStr(Grd3, Row, 11) + Environment.NewLine + Environment.NewLine +
                                "Double input."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsPPAlreadyFulfilled()
        {
            return IsDataExists(
                    "Select DocNo From TblPPHdr Where ProcessInd='F' And DocNo=@Param;",
                    TxtPPDocNo.Text,
                    "Production Planning# : " + TxtPPDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This Production Planning# already fulfilled.");
        }

        private bool IsPPAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblPPHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPPDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production Planning# : " + TxtPPDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This Production Planning# already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 production result.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production result data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Direct labor data entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;
                Msg =
                    "Production Result" + Environment.NewLine + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine;
                    
                if (Sm.GetGrdDec(Grd1, Row, 6) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[8].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 8) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrd2ValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Direct labor is empty.") ||
                    (!Sm.GetGrdBool(Grd2, Row, 7) && Sm.IsGrdValueEmpty(Grd2, Row, 8, true, "Quantity should be greater than 0.")) 
                    ) return true;
            }

            return false;
        }

        private bool IsGrd3ValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeStock();

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Item is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine +
                    "Item's Local Code : " + Sm.GetGrdStr(Grd3, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd3, Row, 5) + Environment.NewLine +
                    "Property : " + Sm.GetGrdStr(Grd3, Row, 7) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd3, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd3, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd3, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd3, Row, 11) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd3, Row, 13) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd3, Row, 13) > Sm.GetGrdDec(Grd3, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd3.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd3, Row, 16) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd3, Row, 16) > Sm.GetGrdDec(Grd3, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd3.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd3, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd3, Row, 19) > Sm.GetGrdDec(Grd3, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveShopFloorControlHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblShopFloorControlHdr(DocNo, DocDt, ProcessInd, CancelInd, ProductionShiftCode, PPDocNo, WorkCenterDocNo, MachineCode, WhsCode, WhsCode2, TemplateInd, BatchCategoryCode, ProductionWorkGroup, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @ProductionShiftCode, @PPDocNo, @WorkCenterDocNo, @MachineCode, @WhsCode, @WhsCode2, @TemplateInd, @BatchCategoryCode, @ProductionWorkGroup, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetLue(LueProductionShiftCode));
            Sm.CmParam<String>(ref cm, "@PPDocNo", TxtPPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", TxtWorkCenterDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MachineCode", Sm.GetLue(LueMachineCode));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", mWhsCode2);
            Sm.CmParam<String>(ref cm, "@TemplateInd", mIsCodeForSFCStandard ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@BatchCategoryCode", Sm.GetLue(LueBatchCategoryCode));
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveShopFloorControlDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* SFC - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblShopFloorControlDtl(DocNo, DNo, ProcessInd, ItCode, BatchNo, Qty, Qty2, UPrice, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ProcessInd, @ItCode_" + r.ToString() +
                        ", IfNull(@BatchNo_" + r.ToString() +
                        ", '-'), @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_"+r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsSFCAutoRecvProduction ? "F" : "O");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveShopFloorControl2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* SFC - Dtl2 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblShopFloorControl2Dtl(DocNo, DNo, BomCode, BomType, CoordinatorInd, Qty, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@DocNo, @DNo_" + r.ToString() +
                        ", @BomCode_" + r.ToString() +
                        ", '3', @CoordinatorInd_" + r.ToString() +
                        ", @Qty_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@BomCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<String>(ref cm, "@CoordinatorInd_" + r.ToString(), Sm.GetGrdBool(Grd2, r, 7) ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 8));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveShopFloorControl3Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* SFC - Dtl3 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblShopFloorControl3Dtl(DocNo, DNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @PropCode_" + r.ToString() +
                        ", @BatchNo_" + r.ToString() +
                        ", @Source_" + r.ToString() +
                        ", @Lot_" + r.ToString() +
                        ", @Bin_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 2));
                    Sm.CmParam<String>(ref cm, "@PropCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 6));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 8));
                    Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 10));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 16));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 19));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 21));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveShopFloorControlDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "/* SFC */ " +
        //            "Insert Into TblShopFloorControlDtl(DocNo, DNo, ProcessInd, ItCode, BatchNo, Qty, Qty2, UPrice, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @ProcessInd, @ItCode, IfNull(@BatchNo, '-'), @Qty, @Qty2, @UPrice, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", mIsSFCAutoRecvProduction?"F":"O");
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveShopFloorControl2Dtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "/* SFC */ " +
        //            "Insert Into TblShopFloorControl2Dtl(DocNo, DNo, BomCode, BomType, CoordinatorInd, Qty, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @BomCode, '3', @CoordinatorInd, @Qty, @CreateBy, CurrentDateTime());"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@BomCode", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@CoordinatorInd", Sm.GetGrdBool(Grd2, Row, 7)?"Y":"N");
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveShopFloorControl3Dtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* SFC */ ");
        //    SQL.AppendLine("Insert Into TblShopFloorControl3Dtl(DocNo, DNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd3, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd3, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd3, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd3, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd3, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 13));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd3, Row, 16));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd3, Row, 19));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 21));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            SQL.AppendLine("Update TblShopFloorControlHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Shop Floor Control : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblShopFloorControlHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo2 As AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblShopFloorControl3Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblShopFloorControl3Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblShopFloorControlHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SFC */ ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, ");
            SQL.AppendLine("A.WhsCode2, B.Lot, B.Bin, ");
            SQL.AppendLine("B.ItCode, IfNull(B.PropCode, '-'), B.BatchNo, B.Source, ");
            if (CancelInd == "N")
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            else
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControl3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(Byte Type, int Row)
        {
            //Type=1 -> Insert
            //Type=2 -> Edit

            var SQL = new StringBuilder();

            SQL.AppendLine("/* SFC Stock Summary */ ");

            SQL.AppendLine("Update TblStockSummary Set ");
            if (Type == 1)
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            else
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2);
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd3, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd3, Row, 11));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRecvProductionHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "/* SFC : Recv From Production */ " +
                    "Insert Into TblRecvProductionHdr(DocNo, DocDt, WhsCode, WhsCode2, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @WhsCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvProductionDtl(string SFCDocNo, string RecvProductionDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SFC : Recv From Production */ ");
            SQL.AppendLine("Insert Into TblRecvProductionDtl(DocNo, DNo, CancelInd, ShopFloorControlDocNo, ShopFloorControlDNo, ");
            SQL.AppendLine("ItCode, PropCode, BatchNo, Source, Lot, Bin, ");
            SQL.AppendLine("QtyPlanning, QtyPlanning2, Qty, Qty2, Qty3, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @RecvProductionDocNo, DNo, 'N', DocNo, DNo, ");
            SQL.AppendLine("ItCode, '-', BatchNo, Concat(@DocType2, '*', @RecvProductionDocNo, '*', DNo), '-', '-', ");
            SQL.AppendLine("Qty, Qty2, Qty, Qty2, Qty2, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblShopFloorControlDtl Where DocNo=@SFCDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SFCDocNo", SFCDocNo);
            Sm.CmParam<String>(ref cm, "@RecvProductionDocNo", RecvProductionDocNo);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SFC : Recv From Production */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("/* SFC : Recv From Production */ ");
            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo,  B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("/* SFC : Recv From Production */ ");
            SQL.AppendLine("Insert Into TblStockPrice(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select Distinct B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("(Select ParValue from TblParameter Where ParCode='MainCurCode'), C.UPrice, 1.00, Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblRecvProductionHdr A ");
            SQL.AppendLine("Inner Join TblRecvProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl C On B.ShopFloorControlDocNo=C.DocNo And B.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblRecvProductionHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 2));
            SQL.AppendLine(" Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Production : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvProductionHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, ");
            SQL.AppendLine("        B.UPrice*A.Qty As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblRecvProductionDtl A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDocNo=B.DocNo And A.ShopFloorControlDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.AcNo3 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, B.UPrice*A.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvProductionDtl A ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDocNo=B.DocNo And A.ShopFloorControlDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo3 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblRecvProductionHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveStock(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode2, B.Lot, B.Bin, B.ItCode, IfNull(B.PropCode, '-'), B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
        //    SQL.AppendLine("From TblShopFloorControlHdr A ");
        //    SQL.AppendLine("Inner Join TblShopFloorControl3Dtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo; ");

        //    SQL.AppendLine("Update TblStockSummary As T1 ");
        //    SQL.AppendLine("Inner Join ( ");
        //    SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, ");
        //    SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
        //    SQL.AppendLine("    From TblStockMovement A ");
        //    SQL.AppendLine("    Inner Join ( ");
        //    SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source ");
        //    SQL.AppendLine("        From TblStockMovement ");
        //    SQL.AppendLine("        Where DocType=@DocType ");
        //    SQL.AppendLine("        And DocNo=@DocNo ");
        //    SQL.AppendLine("        And CancelInd='N' ");
        //    SQL.AppendLine("    ) B ");
        //    SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
        //    SQL.AppendLine("        And A.Lot=B.Lot ");
        //    SQL.AppendLine("        And A.Bin=B.Bin ");
        //    SQL.AppendLine("        And A.ItCode=B.ItCode ");
        //    SQL.AppendLine("        And A.PropCode=B.PropCode ");
        //    SQL.AppendLine("        And A.BatchNo=B.BatchNo ");
        //    SQL.AppendLine("        And A.Source=B.Source ");
        //    SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
        //    SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source ");
        //    SQL.AppendLine(") T2 ");
        //    SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
        //    SQL.AppendLine("    And T1.Lot=T2.Lot ");
        //    SQL.AppendLine("    And T1.Bin=T2.Bin ");
        //    SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
        //    SQL.AppendLine("    And T1.PropCode=T2.PropCode ");
        //    SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
        //    SQL.AppendLine("    And T1.Source=T2.Source ");
        //    SQL.AppendLine("Set ");
        //    SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
        //    SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
        //    SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
        //    SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
        //    SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdatePP()
        //{
            //var SQL = new StringBuilder();

            //SQL.AppendLine("Update TblPPDtl2 T1 ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select B.ItCode, Sum(Qty) As Qty2 ");
            //SQL.AppendLine("    From TblShopFloorControlHdr A ");
            //SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    Where A.CancelInd='N' ");
            //SQL.AppendLine("    And A.PPDocNo=@PPDocNo ");
            //SQL.AppendLine("    And A.WorkCenterDocNo=@WorkCenterDocNo ");
            //SQL.AppendLine("    Group By B.ItCode ");
            //SQL.AppendLine("    ) T2 On T1.ItCode=T2.ItCode ");
            //SQL.AppendLine("Set T1.Qty2=IfNull((T2.Qty2), 0) ");	
            //SQL.AppendLine("Where T1.DocNo=@PPDocNo And T1.WorkCenterDocNo=@WorkCenterDocNo; ");

            //SQL.AppendLine("Update TblPPHdr Set ProcessInd='O' ");
            //SQL.AppendLine("Where DocNo=@PPDocNo ");
            //SQL.AppendLine("And Exists( ");
            //SQL.AppendLine("    Select DocNo From TblPPDtl2 ");
            //SQL.AppendLine("    Where DocNo=@PPDocNo And Qty2<Qty1 Limit 1 ");
            //SQL.AppendLine("); ");

            //SQL.AppendLine("Update TblPPHdr Set ProcessInd='F' ");
            //SQL.AppendLine("Where DocNo=@PPDocNo ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblPPDtl2 ");
            //SQL.AppendLine("    Where DocNo=@PPDocNo And Qty2<Qty1 Limit 1 ");
            //SQL.AppendLine("); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@PPDocNo", TxtPPDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", TxtWorkCenterDocNo.Text);

        //    return cm;
        //}

        #endregion

        #endregion

        #region Edit data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelShopFloorControlHdr());

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveStockMovement(TxtDocNo.Text, "Y"));

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                        cml.Add(SaveStockSummary(2, Row));
            }

            //cml.Add(UpdatePP());

            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataCancelledAlready() ||
                IsSFCAlreadyProcessToPWages() ||
                IsSFCAlreadyProcessToPPenalty() ||
                IsProdResultAlreadyTransferredToInventory()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            return IsDataExists(
                "Select 1 From TblShopFloorControlHdr " +
                "Where CancelInd='Y' And DocNo=@Param; ",
                TxtDocNo.Text, "This document already cancelled."
                );
        }

        private bool IsSFCAlreadyProcessToPWages()
        {
            return IsDataExists(
                "Select 1 From TblShopFloorControlHdr " +
                "Where ProcessInd<>'O' And DocNo=@Param; ",
                TxtDocNo.Text, "This document already processed to production wages."
                );
        }

        private bool IsSFCAlreadyProcessToPPenalty()
        {
            return IsDataExists(
                "Select 1 From TblShopFloorControlHdr " +
                "Where ProcessInd2<>'O' And DocNo=@Param; ",
                TxtDocNo.Text, "This document already processed to production penalty."
                );
        }

        private bool IsProdResultAlreadyTransferredToInventory()
        {
            return IsDataExists(
                "Select 1 From TblRecvProductionDtl " +
                "Where CancelInd='N' And ShopFloorControlDocNo=@Param Limit 1; ",
                TxtDocNo.Text, "Production results already transferred to inventory."
                );
        }

        private MySqlCommand CancelShopFloorControlHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblShopFloorControlHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            #region Old Code

            //SQL.AppendLine("Insert Into TblStockMovement ");
            //SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            //SQL.AppendLine("Qty, Qty2, Qty3, ");
            //SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            //SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            //SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblStockMovement ");
            //SQL.AppendLine("Where DocNo=@DocNo ");
            //SQL.AppendLine("And DocType=@DocType ");
            //SQL.AppendLine("And CancelInd='N'; ");

            //SQL.AppendLine("Update TblStockSummary As T1 ");
            //SQL.AppendLine("Inner Join ( ");
            //SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, ");
            //SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            //SQL.AppendLine("    From TblStockMovement A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source ");
            //SQL.AppendLine("        From TblStockMovement ");
            //SQL.AppendLine("        Where DocType=@DocType ");
            //SQL.AppendLine("        And DocNo=@DocNo ");
            //SQL.AppendLine("        And CancelInd='Y' ");
            //SQL.AppendLine("    ) B ");
            //SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            //SQL.AppendLine("        And A.Lot=B.Lot ");
            //SQL.AppendLine("        And A.Bin=B.Bin ");
            //SQL.AppendLine("        And A.ItCode=B.ItCode ");
            //SQL.AppendLine("        And A.PropCode=B.PropCode ");
            //SQL.AppendLine("        And A.BatchNo=B.BatchNo ");
            //SQL.AppendLine("        And A.Source=B.Source ");
            //SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
            //SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source ");
            //SQL.AppendLine(") T2 ");
            //SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            //SQL.AppendLine("    And T1.Lot=T2.Lot ");
            //SQL.AppendLine("    And T1.Bin=T2.Bin ");
            //SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
            //SQL.AppendLine("    And T1.PropCode=T2.PropCode ");
            //SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
            //SQL.AppendLine("    And T1.Source=T2.Source ");
            //SQL.AppendLine("Set ");
            //SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            //SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            //SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            //SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            //SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            //Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            
            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
            
            SQL.AppendLine("Update TblShopFloorControlHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Shop Floor Control : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblShopFloorControlHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblShopFloorControlHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (mIsSFCShowLeaveRemark) Sm.GrdColInvisible(Grd2, new int[] { 9 }, false);

                ClearData();
                ShowShopFloorControlHdr(DocNo);
                ShowShopFloorControlDtl(DocNo);
                ShowShopFloorControl2Dtl(DocNo);
                ShowShopFloorControl3Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowShopFloorControlHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ProductionShiftCode, ");
            SQL.AppendLine("A.PPDocNo, D.Remark As PPRemark, A.WorkCenterDocNo, B.DocName As WorkCenterDocName, A.MachineCode, ");
            SQL.AppendLine("A.WhsCode, A.WhsCode2, C.WhsName As WhsName2, A.BatchCategoryCode, A.ProductionWorkGroup, A.Remark ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Left Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblWarehouse C On A.WhsCode2=C.WhsCode ");
            SQL.AppendLine("Left Join TblPPHdr D On A.PPDocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "ProductionShiftCode", "PPDocNo", "PPRemark", 
                        
                        //6-10
                        "WorkCenterDocNo", "WorkCenterDocName", "MachineCode", "WhsCode", "WhsCode2", 
                        
                        //11-14
                        "WhsName2", "Remark", "BatchCategoryCode", "ProductionWorkGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueProductionShiftCode, Sm.DrStr(dr, c[3]));
                        TxtPPDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        MeePPRemark.EditValue = Sm.DrStr(dr, c[5]);
                        TxtWorkCenterDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtWorkCenterDocName.EditValue = Sm.DrStr(dr, c[7]);
                        SetLueMachineCode(ref LueMachineCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueMachineCode, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[9]));
                        mWhsCode2 = Sm.DrStr(dr, c[10]);
                        TxtWhsCode2.EditValue = Sm.DrStr(dr, c[11]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        Sm.SetLue(LueBatchCategoryCode, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[14]));
                    }, true
                );
        }

        private void ShowShopFloorControlDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, A.BatchNo, ");
            SQL.AppendLine("A.Qty, B.PlanningUomCode, A.Qty2, B.PlanningUomCode2, A.UPrice, C.WastedInd ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "BatchNo", "Qty", "PlanningUomCode",
 
                    //6-9
                    "Qty2", "PlanningUomCode2", "UPrice", "WastedInd" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8, 10 });
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowShopFloorControl2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BomCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, A.CoordinatorInd, A.Qty  ");
            SQL.AppendLine("From TblShopFloorControl2Dtl A ");
            SQL.AppendLine("Left Join TblEmployee B On A.BomCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "BomCode", "EmpName", "EmpCodeOld", "PosName", "DeptName", 

                   //6-7
                   "CoordinatorInd", "Qty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd2, Grd2.Rows.Count - 1, new int[] { 7 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowShopFloorControl3Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.ItCode, C.ItCodeInternal, C.ItName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(E.Qty, 0) + Case When A.CancelInd='N' Then B.Qty Else 0 End As Stock, B.Qty, C.InventoryUomCode, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) + Case When A.CancelInd='N' Then B.Qty2 Else 0 End As Stock2, B.Qty2, C.InventoryUomCode2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) + Case When A.CancelInd='N' Then B.Qty3 Else 0 End As Stock3, B.Qty3, C.InventoryUomCode3, B.Remark ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControl3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode2=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItCodeInternal", "ItName", "PropCode", "PropName",    
                    
                    //6-10
                    "BatchNo", "Source", "Lot", "Bin", "Stock",    
                    
                    //11-15
                    "Qty", "InventoryUomCode", "Stock2", "Qty2", "InventoryUomCode2", 
                    
                    //16-19
                    "Stock3", "Qty3", "InventoryUomCode3", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Grd.Cells[Row, 22].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        internal void ShowEmployeeInfo(string PPDocNo, string WorkCenterDocNo)
        {
            if (Sm.GetDte(DteDocDt).Length == 0)
            {
                Sm.ClearGrd(Grd2, true);
                return;
            }

            var SQL = new StringBuilder();

            if (mIsSFCShowLeaveRemark)
            {
                SQL.AppendLine("Select A.DocCode, A.EmpName, A.EmpCodeOld, A.PosName, A.DeptName, A.Qty, ");
                SQL.AppendLine("Case When IfNull(B.NumberOfDay, 0)>0 Then  Concat(Convert(Format(B.NumberOfDay, 2) Using utf8), ' Day') Else ");
                SQL.AppendLine("    Case When IfNull(B.NumberOfHour, 0)>0 Then  Concat(Convert(Format(B.NumberOfHour, 2) Using utf8), 'Hour') Else Null End ");
                SQL.AppendLine("End As LeaveRemark ");
                SQL.AppendLine("From (");
                SQL.AppendLine("    Select T5.DocCode, T6.EmpName, T6.EmpCodeOld, T7.PosName, T8.DeptName, Sum(T5.Qty) As Qty ");
                SQL.AppendLine("    From TblPPDtl T1 ");
                SQL.AppendLine("    Inner Join TblProductionOrderHdr T2 On T1.ProductionOrderDocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblProductionOrderDtl T3 On T2.DocNo=T3.DocNo ");
                SQL.AppendLine("    Inner Join TblProductionRoutingDtl T4 ");
                SQL.AppendLine("        On T2.ProductionRoutingDocNo=T4.DocNo ");
                SQL.AppendLine("        And T3.ProductionRoutingDNo=T4.DNo ");
                SQL.AppendLine("        And T4.WorkCenterDocNo=@WorkCenterDocNo ");
                if (!mIsCodeForSFCStandard)
                {
                    SQL.AppendLine("    Inner Join TblBOMDtl T5 On T3.BomDocNo=T5.DocNo And T5.DocType In ('3') ");
                }
                else
                {
                    SQL.AppendLine("    Inner Join  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("            SELECT A.WorkCenterCode, A.BOMFormulationCode ");
                    SQL.AppendLine("            FROM tblworkcenterbomformulation AS a ");
                    SQL.AppendLine("            WHERE date in ( ");
                    SQL.AppendLine("                SELECT MAX(date) ");
                    SQL.AppendLine("                FROM tblworkcenterbomformulation AS b ");
                    SQL.AppendLine("                WHERE a.workcenterCode = b.workcenterCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("    )T4a On T4.WorkcenterDocno = T4a.WorkcenterCode And T4.WorkcenterDocno = @WorkCenterDocNo ");
                    SQL.AppendLine("    Inner Join TblBOMDtl T5 On T4a.BomFormulationCode=T5.DocNo And T5.DocType In ('3')  ");
                }
                SQL.AppendLine("    Inner Join TblEmployee T6 On T5.DocCode=T6.EmpCode ");
                SQL.AppendLine("    And (T6.ResignDt Is Null Or (T6.ResignDt Is Not Null And T6.ResignDt>Left(@DocDt, 8))) ");
                SQL.AppendLine("    Left Join TblPosition T7 On T6.PosCode=T7.PosCode ");
                SQL.AppendLine("    Left Join TblDepartment T8 On T6.DeptCode=T8.DeptCode ");
                SQL.AppendLine("    Where T1.DocNo=@PPDocNo ");
                SQL.AppendLine("    Group By T5.DocCode, T6.EmpName, T6.EmpCodeOld, T7.PosName, T8.DeptName ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("   Select EmpCode, Sum(NumberOfDay) NumberOfDay, Sum(NumberOfHour) NumberOfHour ");
                SQL.AppendLine("   From ( ");
                SQL.AppendLine("       Select T.EmpCode, ");
                SQL.AppendLine("       Case When T.LeaveType='F' Then 1 Else 0 End As NumberOfDay, ");
                SQL.AppendLine("       Case When T.LeaveType<>'F' Then T.DurationHour Else 0 End As NumberOfHour ");
                SQL.AppendLine("       From TblEmpLeaveHdr T ");
                SQL.AppendLine("       Where T.CancelInd='N' ");
                SQL.AppendLine("       And IfNull(T.Status, 'O')<>'C' ");
                SQL.AppendLine("       And Exists( ");
                SQL.AppendLine("           Select Distinct DocNo From TblEmpLeaveDtl ");
                SQL.AppendLine("           Where DocNo=T.DocNo And LeaveDt=@DocDt ");
                SQL.AppendLine("       ) ");
                SQL.AppendLine("   Union All ");
                SQL.AppendLine("       Select T2.EmpCode, ");
                SQL.AppendLine("       Case When T1.LeaveType<>'H' Then ");
                SQL.AppendLine("            Case When T1.LeaveType='F' Then 1 Else 0.5 End ");
                SQL.AppendLine("       Else 0 End As NumberOfDay, ");
                SQL.AppendLine("       Case When T1.LeaveType='H' Then T1.DurationHour Else 0 End As NumberOfHour ");
                SQL.AppendLine("       From TblEmpLeave2Hdr T1 ");
                SQL.AppendLine("       Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("       Where T1.CancelInd='N' ");
                SQL.AppendLine("       And IfNull(T1.Status, 'O')<>'C' ");
                SQL.AppendLine("       And Exists( ");
                SQL.AppendLine("           Select Distinct DocNo From TblEmpLeave2Dtl2 ");
                SQL.AppendLine("           Where DocNo=T1.DocNo And LeaveDt=@DocDt ");
                SQL.AppendLine("       ) ");
                SQL.AppendLine("   ) Tbl Group By EmpCode ");
                SQL.AppendLine(") B On A.DocCode=B.EmpCode ");
                SQL.AppendLine("Order By A.EmpName; ");
            }
            else
            {
                SQL.AppendLine("Select T5.DocCode, T6.EmpName, T6.EmpCodeOld, T7.PosName, T8.DeptName, Sum(T5.Qty) As Qty, Null As LeaveRemark ");
                SQL.AppendLine("From TblPPDtl T1 ");
                SQL.AppendLine("Inner Join TblProductionOrderHdr T2 On T1.ProductionOrderDocNo=T2.DocNo ");
                SQL.AppendLine("Inner Join TblProductionOrderDtl T3 On T2.DocNo=T3.DocNo ");
                SQL.AppendLine("Inner Join TblProductionRoutingDtl T4 ");
                SQL.AppendLine("    On T2.ProductionRoutingDocNo=T4.DocNo ");
                SQL.AppendLine("    And T3.ProductionRoutingDNo=T4.DNo ");
                SQL.AppendLine("    And T4.WorkCenterDocNo=@WorkCenterDocNo ");
                if (!mIsCodeForSFCStandard)
                {
                    SQL.AppendLine("    Inner Join TblBOMDtl T5 On T3.BomDocNo=T5.DocNo And T5.DocType In ('3') ");
                }
                else
                {
                    SQL.AppendLine("    Inner Join  ");
                    SQL.AppendLine("    ( ");
	 		        SQL.AppendLine("            SELECT A.WorkCenterCode, A.BOMFormulationCode ");
			        SQL.AppendLine("            FROM tblworkcenterbomformulation AS a ");
			        SQL.AppendLine("            WHERE date in ( ");
				    SQL.AppendLine("                SELECT MAX(date) ");
				    SQL.AppendLine("                FROM tblworkcenterbomformulation AS b ");
				    SQL.AppendLine("                WHERE a.workcenterCode = b.workcenterCode ");
			        SQL.AppendLine("            ) ");
                    SQL.AppendLine("    )T4a On T4.WorkcenterDocno = T4a.WorkcenterCode And T4.WorkcenterDocno = @WorkCenterDocNo ");
                    SQL.AppendLine("    Inner Join TblBOMDtl T5 On T4a.BomFormulationCode=T5.DocNo And T5.DocType In ('3')  ");
                }
                SQL.AppendLine("Inner Join TblEmployee T6 On T5.DocCode=T6.EmpCode ");
                SQL.AppendLine("And (T6.ResignDt Is Null Or (T6.ResignDt Is Not Null And T6.ResignDt>Left(@DocDt, 8))) ");
                SQL.AppendLine("Left Join TblPosition T7 On T6.PosCode=T7.PosCode ");
                SQL.AppendLine("Left Join TblDepartment T8 On T6.DeptCode=T8.DeptCode ");
                SQL.AppendLine("Where T1.DocNo=@PPDocNo ");
                SQL.AppendLine("Group By T5.DocCode, T6.EmpName, T6.EmpCodeOld, T7.PosName, T8.DeptName ");
                SQL.AppendLine("Order By T6.EmpName; ");
            }
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@PPDocNo", PPDocNo);
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", WorkCenterDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DocCode",

                           //1-5
                           "EmpName", "EmpCodeOld", "PosName", "DeptName", "Qty",
 
                           //6
                           "LeaveRemark"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdBoolValueFalse(Grd, Row, new int[] { 7 });
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd2, Grd2.Rows.Count - 1, new int[] { 7 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8 });
            SetDNo(Grd2);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal void GenerateBatchNo(int Row)
        {
            var dt = Sm.GetDte(DteDocDt);
            if (dt.Length > 8) dt = Sm.Left(dt, 8);

            if (mIsSFCUseOptionBatchNoFormula)
            {
                var SQL = new StringBuilder();
                var SQL1 = new StringBuilder();
                var cmx = new MySqlCommand() { CommandText = "Set Session group_concat_max_len = 1000000;" };
                
                string mSource = string.Empty;
                string
                    // Mixing
                    mMaterialCategoryCode = string.Empty, mMaterialCode = string.Empty,
                    mBatchMaterialCode = string.Empty,

                    //Sacheting
                    mMixingDt = string.Empty, mMixingMth = string.Empty, mShiftSacheting = string.Empty,
                    mMixingYr = string.Empty, mLineSacheting = string.Empty, mMixingBatchCategoryCode = string.Empty,
                    mBalanceDay = string.Empty,

                    // BatchNo
                    mBatchNo = string.Empty;

                string mItCode = Sm.GetGrdStr(Grd1, Row, 2);
                bool mIsSacheting = false;

                SQL1.AppendLine("Select Group_Concat(Distinct OptCode) ");
                SQL1.AppendLine("From TblOption ");
                SQL1.AppendLine("Where OptCat = 'SFCSachetingBatchNo'; ");

                Sm.ExecCommand(cmx);

                string mSFCSachetingDocNo = Sm.GetValue(SQL1.ToString());

                if (mSFCSachetingDocNo.Length > 0)
                {
                    string[] s = mSFCSachetingDocNo.Split(',');
                    foreach (string d in s)
                    {
                        if (d == TxtWorkCenterDocNo.Text)
                        {
                            mIsSacheting = true;
                            break;
                        }
                    }
                }

                if (!mIsSacheting)
                {
                    mMaterialCode = Sm.GetValue("Select ItGrpCode From TblItem Where ItCode = @Param Limit 1; ", mItCode);
                    if (DteDocDt.Text.Length > 0) mBatchMaterialCode = GetDteFormat(DteDocDt.DateTime);

                    SQL.AppendLine("Select B.OptDesc ");
                    SQL.AppendLine("From TblItem A ");
                    SQL.AppendLine("Inner Join TblOption B On A.ItCtCode = B.OptCode ");
                    SQL.AppendLine("    And B.OptCat = 'RecvVdBatchNoFormula' ");
                    SQL.AppendLine("    And A.ItCode = @Param ");
                    SQL.AppendLine("Limit 1; ");

                    mMaterialCategoryCode = Sm.GetValue(SQL.ToString(), mItCode);

                    mBatchNo = string.Concat(mMaterialCategoryCode, mMaterialCode, "-", mBatchMaterialCode);
                }
                else
                {
                    string mSFCDocNo = string.Empty, mSFCDocDt = string.Empty, mSFCMixingBatchCategoryCode = string.Empty;
                    
                    //dapetin source untuk SFC
                    for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                    {
                        if (mSource.Length > 0) mSource += ",";
                        mSource += Sm.GetGrdStr(Grd3, i, 9);
                    }

                    if (mSource.Length > 0)
                    {
                        var SQL2 = new StringBuilder();
                        var cm = new MySqlCommand();

                        // cari SFC dengan DocDt tertua
                        SQL2.AppendLine("Select A.DocNo, A.DocDt, A.BatchCategoryCode ");
                        SQL2.AppendLine("From TblShopFloorControlHdr A ");
                        SQL2.AppendLine("Inner Join TblStockMovement B ON A.DocNo = B.DocNo ");
                        SQL2.AppendLine("    And Find_In_Set(B.Source, @Param) ");
                        SQL2.AppendLine("Order By A.DocDt Desc, A.CreateDt Desc ");
                        SQL2.AppendLine("Limit 1; ");

                        using (var cn = new MySqlConnection(Gv.ConnectionString))
                        {
                            cn.Open();
                            cm.Connection = cn;
                            cm.CommandText = SQL2.ToString();
                            Sm.CmParam<String>(ref cm, "@Param", mSource);
                            var dr = cm.ExecuteReader();
                            var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DocDt", "BatchCategoryCode" });
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    mSFCDocNo = Sm.DrStr(dr, c[0]);
                                    mSFCDocDt = Sm.DrStr(dr, c[1]);
                                    mSFCMixingBatchCategoryCode = Sm.DrStr(dr, c[2]);
                                }
                            }
                            dr.Close();
                        }
                    }

                    // isi tanggal, bulan, tahun mixing, berdasarkan SFC DocDt tertua
                    if (mSFCDocDt.Length > 0)
                    {
                        mMixingDt = mSFCDocDt.Substring(6, 2);
                        mMixingMth = mSFCDocDt.Substring(4, 2);
                        mMixingYr = mSFCDocDt.Substring(2, 2);
                    }

                    // isi shift
                    if (Sm.GetLue(LueProductionShiftCode).Length > 0)
                        mShiftSacheting = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'SFCBatchShiftCode' And OptCode = @Param Limit 1; ", Sm.GetLue(LueProductionShiftCode));

                    // isi line sacheting
                    if (Sm.GetLue(LueMachineCode).Length > 0)
                    {
                        var SQL3 = new StringBuilder();

                        SQL3.AppendLine("SELECT B.ShortCode ");
                        SQL3.AppendLine("FROM TblWorkCenterDtlAssetMachine A ");
                        SQL3.AppendLine("INNER JOIN TblAsset B ON A.AssetCode = B.AssetCode ");
                        SQL3.AppendLine("    AND A.DocNo = @Param1 ");
                        SQL3.AppendLine("    AND A.AssetCode = @Param2 ");
                        SQL3.AppendLine("Limit 1; ");

                        mLineSacheting = Sm.GetValue(SQL3.ToString(), TxtWorkCenterDocNo.Text, Sm.GetLue(LueMachineCode), string.Empty);
                    }

                    // isi BatchCategoryCode dari SFC DocDt tertua
                    if(mIsSFCUseBatchCategoryCode)
                    {
                        mMixingBatchCategoryCode = mSFCMixingBatchCategoryCode;
                    }

                    // hitung selisih hari
                    if (DteDocDt.Text.Length > 0)
                    {
                        string mDocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
                        mBalanceDay = Sm.GetValue("SELECT If(DATEDIFF(@Param1, @Param2) > 0, DATEDIFF(@Param1, @Param2), 0) AS Diff;", mDocDt, mSFCDocDt, string.Empty);
                    }

                    mBatchNo = string.Concat(mMixingDt, mMixingMth, mShiftSacheting, mMixingYr, mLineSacheting, mMixingBatchCategoryCode, mBalanceDay);
                }

                Grd1.Cells[Row, 5].Value = mBatchNo;
            }

            if (mIsUseProductionWorkGroup)
            {
                string mBatchNo = string.Empty,
                    DocDt = dt, //Sm.Left(Sm.GetDte(DteDocDt), 8),
                    WorkCenter = TxtWorkCenterDocName.Text.Length == 0 ? "XXX" : TxtWorkCenterDocName.Text,
                    Shift = LueProductionShiftCode.Text,
                    WorkGroup = LueProductionWorkGroup.Text;

                mBatchNo = DocDt + "/" + WorkCenter + "/" + Shift + "/" + WorkGroup;
                Grd1.Cells[Row, 5].Value = mBatchNo;
            }
        }

        private string GetDteFormat(DateTime Value)
        {
            return
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                Sm.Right(Value.Year.ToString(), 2);
        }

        internal void RefractorBatchNo()
        {
            if (mIsAssetShortCodeAddedToSFCResultBatchNo)
            {
                if (Grd1.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                        {
                            if(mAssetShortCode.Length > 0)
                                Grd1.Cells[i, 5].Value = Sm.GetGrdStr(Grd1, i, 5).Replace(mAssetShortCode, string.Empty);
                        }
                    }
                }

                mAssetShortCode = string.Empty;
                if (Sm.GetLue(LueMachineCode).Length > 0) mAssetShortCode = Sm.GetValue("Select IfNull(ShortCode, '') ShortCode From TblAsset Where AssetCode = @Param Limit 1; ", Sm.GetLue(LueMachineCode));

                if (Grd1.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                        {
                            Grd1.Cells[i, 5].Value = string.Concat(Sm.GetGrdStr(Grd1, i, 5), mAssetShortCode);
                        }
                    }
                }
            }
        }

        internal void SetLueBatchNo(ref DXE.LookUpEdit Lue)
        {
            
            try
            {
                //var SQL = new StringBuilder();

                //SQL.AppendLine("Select Distinct BatchNo As Col1, BatchNo As Col2 From TblStockSummary ");
                //SQL.AppendLine("Where WhsCode=@WhsCode And Qty>0 ");
                //SQL.AppendLine("Order By BatchNo;");

                //var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                //Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2.Length==0?"XXX":mWhsCode2);

                //Sm.SetLue2(
                //    ref Lue,
                //    ref cm,
                //    0, 50, false, true, "Code", "Name", "Col2", "Col1");

                var ListOfObject = new List<Lue2>()
                {
                    new Lue2{Col1 = null, Col2 = null},
                    new Lue2{Col1 = "<Refresh>", Col2 = "<Refresh>"},
                };

                if (lb.Count>0)
                    lb.ForEach(b => 
                        {
                            ListOfObject.Add(new Lue2 { Col1 = b.BN, Col2 = b.BN });
                        }
                    );
                Sl.SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = "Code";
                Lue.Properties.Columns["Col2"].Caption = "Name";
                Lue.Properties.Columns["Col1"].Width = 0;
                Lue.Properties.Columns["Col2"].Width = 50;
                Lue.Properties.Columns["Col1"].Visible = false;
                Lue.Properties.Columns["Col2"].Visible = true;
                Lue.Properties.DisplayMember = "Col2";
                Lue.Properties.ValueMember = "Col1";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueItCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText = 
                        "Select ItCode Col1, ItName Col2 " +
                        "From TblItem Where PlanningItemInd='Y' And ActInd='Y' Order By ItName;" 
                };
                Sm.SetLue2(
                    ref Lue,
                    ref cm,
                    0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueMachineCode(ref DXE.LookUpEdit Lue, string WCDocNo)
        {
            var sql = new StringBuilder();
            sql.AppendLine("Select A.AssetCode As Col1, ");
            if (mRuleForShowingAssetName == "2")
                sql.AppendLine("B.DisplayName As Col2 ");
            else
                sql.AppendLine("B.AssetName As Col2 ");
            sql.AppendLine("From TblWorkCenterDtlAssetMachine A  Inner Join  TblAsset B  On A.AssetCode = B.AssetCode Where A.DocNo = @WCDocNo Order By B.AssetName ;");
            var cm = new MySqlCommand()
            {
                CommandText = sql.ToString()
            };
            Sm.CmParam<String>(ref cm, "@WCDocNo", WCDocNo);
            Sm.SetLue2(
                ref Lue,
                ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DXE.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 8).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 8));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void LueRequestEdit2(iGrid Grd, DXE.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 4).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 2));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }                 

        private void ReComputeStock()
        {
            if (Grd3.Rows.Count <= 1) return;
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2);
                if (Grd3.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 9).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd3, Row, 9, 10, 11);
                            No += 1;
                        }
                    }
                }
                cm.CommandText = 
                    SQL.ToString() + 
                    (Filter.Length!=0?" And (" + Filter + ") ":string.Empty);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd3.ProcessTab = true;
                    Grd3.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 9), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 10), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 11), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 12, 3);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 15, 4);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 18, 5);
                                break;
                            }
                        }
                    }
                    Grd3.EndUpdate();
                }
                dr.Close();
            }
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd3, Row, 2) +
                            Sm.GetGrdStr(Grd3, Row, 6) +
                            Sm.GetGrdStr(Grd3, Row, 8) +
                            Sm.GetGrdStr(Grd3, Row, 9) +
                            Sm.GetGrdStr(Grd3, Row, 10) +
                            Sm.GetGrdStr(Grd3, Row, 11) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 9).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd3, Row, 9) +
                            Sm.GetGrdStr(Grd3, Row, 10) +
                            Sm.GetGrdStr(Grd3, Row, 11) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsJournalValidationShopFloorControl2Enabled', 'IsUseProductionWorkGroup', 'IsSFCUseBatchCategoryCode', 'IsSFCUseOptionBatchNoFormula', 'IsMovingAvgEnabled', ");
            SQL.AppendLine("'NumberOfPlanningUomCode', 'NumberOfInventoryUomCode', 'SFCDefaultBatchNo', 'DocNoSeqNumberType', 'RuleForShowingAssetName', ");
            SQL.AppendLine("'IsAssetShortCodeAddedToSFCResultBatchNo', 'IsSFCAutoCopyData', 'IsSFCShowLeaveRemark', 'IsSFCAutoRecvProduction', 'IsSFCShowRecvWhsWithTRRemark', ");
            SQL.AppendLine("'IsAutoJournalActived', 'MenuCodeForSFCStandard', 'SFCDefaultDocDt' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsJournalValidationShopFloorControl2Enabled": mIsJournalValidationShopFloorControl2Enabled = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsSFCUseBatchCategoryCode": mIsSFCUseBatchCategoryCode = ParValue == "Y"; break;
                            case "IsSFCUseOptionBatchNoFormula": mIsSFCUseOptionBatchNoFormula = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsAssetShortCodeAddedToSFCResultBatchNo": mIsAssetShortCodeAddedToSFCResultBatchNo = ParValue == "Y"; break;
                            case "IsSFCAutoCopyData": mIsSFCAutoCopyData = ParValue == "Y"; break;
                            case "IsSFCShowLeaveRemark": mIsSFCShowLeaveRemark = ParValue == "Y"; break;
                            case "IsSFCAutoRecvProduction": mIsSFCAutoRecvProduction = ParValue == "Y"; break;
                            case "IsSFCShowRecvWhsWithTRRemark": mIsSFCShowRecvWhsWithTRRemark = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                                                
                            //string
                            case "SFCDefaultBatchNo": mSFCDefaultBatchNo = ParValue; break;
                            case "DocNoSeqNumberType": mDocNoSeqNumberType = ParValue; break;
                            case "RuleForShowingAssetName": mRuleForShowingAssetName = ParValue; break;
                            case "MenuCodeForSFCStandard": 
                                mIsCodeForSFCStandard = !Sm.CompareStr(mMenuCode, ParValue); 
                                break;
                            
                            //Integer
                            case "NumberOfPlanningUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfPlanningUomCode = 1;
                                else
                                    mNumberOfPlanningUomCode = int.Parse(ParValue);
                                break;
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;

                            //decimal
                            case "SFCDefaultDocDt":
                                if (ParValue.Length > 0) mSFCDefaultDocDt = decimal.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetDocDtDefault()
        {
            if (mSFCDefaultDocDt==0)
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            else
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime()).AddDays((double)(mSFCDefaultDocDt));
        }

        internal void GetSFCDefaultBatchNo()
        {
            //Customized for IOK

            string WC = "X/X", Dt = "ddMMyy", Value = string.Empty;

            mBatchNo = string.Empty;

            if (TxtWorkCenterDocNo.Text.Length != 0)
            {
                Value = GetWorkCenterInfo();
                if (Value.Length != 0) WC = Value;
            }

            Value = Sm.GetDte(DteDocDt);
            if (Value.Length >= 8)
                Dt = Value.Substring(6, 2) + Value.Substring(4, 2) + Value.Substring(2, 2);

            mBatchNo = WC + "/" + Dt + "/";
        }

        private string GetWorkCenterInfo()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =
                    "Select Concat(IfNull(B.ShortCode, 'X'), '/', IfNull(A.ShortCode, 'X')) As ShortCode " +
                    "From TblWorkCenterHdr A " +
                    "Left Join TblDepartment B On A.DeptCode=B.DeptCode " +
                    "Where A.DocNo=@DocNo Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtWorkCenterDocNo.Text);
            return Sm.GetValue(cm);
        }

        private void ShowBatchNoInfo(int Row)
        {
            var BatchNo = Sm.GetLue(LueBatchNo);
            foreach (var s in ls.Where(s =>s.BatchNo==BatchNo))
            {
                Grd3.BeginUpdate();
                Grd3.Cells[Row, 2].Value = s.ItCode;
                Grd3.Cells[Row, 4].Value = s.ItCodeInternal;
                Grd3.Cells[Row, 5].Value = s.ItName;
                Grd3.Cells[Row, 6].Value = s.PropCode;
                Grd3.Cells[Row, 7].Value = s.PropName;
                Grd3.Cells[Row, 8].Value = s.BatchNo;
                Grd3.Cells[Row, 9].Value = s.Source;
                Grd3.Cells[Row, 10].Value = s.Lot;
                Grd3.Cells[Row, 11].Value = s.Bin;
                Grd3.Cells[Row, 12].Value = s.Qty;
                Grd3.Cells[Row, 13].Value = s.Qty;
                Grd3.Cells[Row, 14].Value = s.InventoryUomCode;
                Grd3.Cells[Row, 15].Value = s.Qty2;
                Grd3.Cells[Row, 16].Value = s.Qty2;
                Grd3.Cells[Row, 17].Value = s.InventoryUomCode2;
                Grd3.Cells[Row, 18].Value = s.Qty3;
                Grd3.Cells[Row, 19].Value = s.Qty3;
                Grd3.Cells[Row, 20].Value = s.InventoryUomCode3;
                Grd3.Cells[Row, 22].Value = 0m;
                Grd3.EndUpdate();
                return;
            }


            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            //SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3 ");
            //SQL.AppendLine("From TblStockSummary A ");
            //SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            //SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            //SQL.AppendLine("And A.BatchNo=@BatchNo ");
            //SQL.AppendLine("And (A.Qty>0) ");
            //SQL.AppendLine("Order By A.Source Asc Limit 1;");

            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    var cm = new MySqlCommand()
            //    {
            //        Connection = cn,
            //        CommandText = SQL.ToString()
            //    };
            //    Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2);
            //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetLue(LueBatchNo));
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] 
            //        { 
            //            //0
            //            "ItCode",
 
            //            //1-5
            //            "ItCodeInternal", "ItName", "PropCode", "PropName", "BatchNo", 
                        
            //            //6-10
            //            "Source", "Lot", "Bin", "Qty", "InventoryUomCode", 
                        
            //            //11-14
            //            "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3"
            //        });

            //    if (dr.HasRows)
            //    {
            //        Grd3.ProcessTab = true;
            //        Grd3.BeginUpdate();
            //        while (dr.Read())
            //        {
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 0);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 1);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 2);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 6, 3);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 7, 4);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 8, 5);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 9, 6);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 10, 7);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 11, 8);
            //            Sm.SetGrdValue("N", Grd3, dr, c, Row, 12, 9);
            //            Sm.SetGrdValue("N", Grd3, dr, c, Row, 13, 9);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 14, 10);
            //            Sm.SetGrdValue("N", Grd3, dr, c, Row, 15, 11);
            //            Sm.SetGrdValue("N", Grd3, dr, c, Row, 16, 11);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 17, 12);
            //            Sm.SetGrdValue("N", Grd3, dr, c, Row, 18, 13);
            //            Sm.SetGrdValue("N", Grd3, dr, c, Row, 19, 13);
            //            Sm.SetGrdValue("S", Grd3, dr, c, Row, 20, 14);
            //        }
            //        Grd3.EndUpdate();
            //    }
            //    dr.Close();
            //}
        }

        private void ShowItCodeInfo(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.PlanningUomCode, A.PlanningUomCode2, B.WastedInd ");
            SQL.AppendLine("From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("And A.ItCode=@ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetLue(LueItCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode",
 
                        //1-4
                        "ItName", "PlanningUomCode", "PlanningUomCode2", "WastedInd"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                        Grd1.Cells[Row, 5].Value = mBatchNo;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 3);
                        Grd1.Cells[Row, 10].Value = 0m;
                        Sm.SetGrdValue("B", Grd1, dr, c, Row, 11, 4);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void SetDNo(iGrid Grd)
        {
            if (Grd.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd.Rows.Count; Row++)
                    Grd.Cells[Row, 0].Value = Sm.Right("00"+(Row+1), 3).ToString();
            }
        }

        internal void ProcessStock()
        {
            ls.Clear();
            lb.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3 ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And A.Qty>0 Order By A.BatchNo; ");

            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode2);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "PropCode", "PropName", "BatchNo", 
                    
                    //6-10
                    "Source", "Lot", "Bin", "Qty", "InventoryUomCode", 
                    
                    //11-14
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ls.Add(new Stock()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItCodeInternal = Sm.DrStr(dr, c[1]),
                            ItName = Sm.DrStr(dr, c[2]),
                            PropCode = Sm.DrStr(dr, c[3]),
                            PropName = Sm.DrStr(dr, c[4]),
                            BatchNo = Sm.DrStr(dr, c[5]),
                            Source = Sm.DrStr(dr, c[6]),
                            Lot = Sm.DrStr(dr, c[7]),
                            Bin = Sm.DrStr(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            InventoryUomCode = Sm.DrStr(dr, c[10]),
                            Qty2 = Sm.DrDec(dr, c[11]),
                            InventoryUomCode2 = Sm.DrStr(dr, c[12]),
                            Qty3 = Sm.DrDec(dr, c[13]),
                            InventoryUomCode3 = Sm.DrStr(dr, c[14]),
                        });
                    }
                }
                dr.Close();
            }

            if (ls.Count > 0)
            {
                var l = ls.Select(s => s.BatchNo).Distinct().ToList();
                foreach (var b in l)
                    lb.Add(new BatchNo(){ BN = b });
            }
        }

        private void ProcessComputeResultPrice()
        {
            ProcessComputeResultPrice1();
            ProcessComputeResultPrice2();
        }

        private void ProcessComputeResultPrice1()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Source = string.Empty, Filter = string.Empty;

            if (Grd3.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                {
                    Source = Sm.GetGrdStr(Grd3, Row, 9);
                    if (Source.Length != 0)
                    {
                        Grd3.Cells[Row, 22].Value = 0m;
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Source=@Source" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@Source" + Row.ToString(), Source);
                    }
                }
            
                if (Filter.Length!=0)
                {
                    Source = string.Empty;
                    decimal Price = 0m;

                    SQL.AppendLine("Select Source, UPrice*ExcRate As Price ");
                    SQL.AppendLine("From TblStockPrice ");
                    SQL.AppendLine("Where (" + Filter + ");");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]{ "Source", "Price" });
                        if (dr.HasRows)
                        {
                            Grd3.BeginUpdate();
                            while (dr.Read())
                            {
                                Source = Sm.DrStr(dr, 0);
                                Price = Sm.DrDec(dr, 1);
                                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                                    if (Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 9), Source))
                                        Grd3.Cells[Row, 22].Value = Price;
                            }
                            Grd3.EndUpdate();
                        }
                        dr.Close();
                    }
                }
            }
        }

        private void ProcessComputeResultPrice2()
        {
            decimal Value = 0m, Qty = 0m, Price = 0m;
            if (Grd3.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 22).Length > 0)
                        Value += (Sm.GetGrdDec(Grd3, Row, 13) * Sm.GetGrdDec(Grd3, Row, 22));
            }
            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    Grd1.Cells[Row, 10].Value = 0m;
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !Sm.GetGrdBool(Grd1, Row, 11))
                        Qty += Sm.GetGrdDec(Grd1, Row, 6);
                }
            }
            if (Qty!=0) Price = Value / Qty;
            if (Price != 0 && Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 11))
                            Grd1.Cells[Row, 10].Value = 0m;
                        else
                            Grd1.Cells[Row, 10].Value = Price;
                    }
                }
            }
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<SFC>();
            var l2 = new List<Company>();

            string[] TableName = { "SFC", "Company" };
            List<IList> myLists = new List<IList>();

            #region Cucuk

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo, Date_Format(A.DocDt, '%d %M %Y')As DocDt, SUBSTRING_INDEX(B.BatchNo, ' ', 1) As Batch1, ");
            SQL.AppendLine("SUBSTRING_INDEX(SUBSTRING_INDEX(B.BatchNo, ' ', 2),' ',-1) As Batch2, SUBSTRING_INDEX(B.BatchNo,' ',-1) As Batch3, A.Remark, B.Qty, B.Qty2, A.ProductionShiftCode ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner join TblShopFloorControlDtl B On A.DocNo=B.DocNo");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On A.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And C.DocName='CUCUK' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "DocNo",

                         "DocDt",
                         "Batch1",
                         "Batch2",
                         "Batch3",
                         "Remark",

                         "Qty",
                         "Qty2",
                         "ProductionShiftCode"
                        });
                if (dr.HasRows)
                {
                    int nomor = 0;
                    while (dr.Read())
                    {
                        nomor = nomor + 1;
                        l.Add(new SFC()
                        {
                            nomor = nomor,
                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            Batch1 = Sm.DrStr(dr, c[2]),
                            Batch2 = Sm.DrStr(dr, c[3]),
                            Batch3 = Sm.DrStr(dr, c[4]),
                            Remark = Sm.DrStr(dr, c[5]),

                            Qty = Sm.DrDec(dr, c[6]),
                            Qty2 = Sm.DrDec(dr, c[7]),
                            ShiftCode = Sm.DrStr(dr, c[8]),
                            
                      });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion 

            #region Company

            var cm2 = new MySqlCommand();

            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1
                         "CompanyName",
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Company()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),
                            CompanyName = Sm.DrStr(dr2, c2[1]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion 

            Sm.PrintReport("SFC", myLists, TableName, false);

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 2, 3, 4, 6, 7, 9, 10, 21 }, !ChkHideInfoInGrd.Checked);
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
                Grd3.Font = TheFont;
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd2, true);
                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsSFCUseOptionBatchNoFormula)
                    {
                        for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                        {
                            GenerateBatchNo(i);
                        }
                    }
                }
            }
        }

        private void LueProductionShiftCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProductionShiftCode, new Sm.RefreshLue1(Sl.SetLueProductionShiftCode));
                
                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsSFCUseOptionBatchNoFormula)
                    {
                        for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                        {
                            GenerateBatchNo(i);
                        }
                    }
                }
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueBatchNo_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueBatchNo) == "<Refresh>")
            {
                ProcessStock();
                SetLueBatchNo(ref LueBatchNo);
            }

            if (LueBatchNo.Visible && fAccept && fCell.ColIndex == 8)
            {
                int row = fCell.RowIndex, col = fCell.ColIndex;
                Sm.SetGrdValueNull(ref Grd3, row, new int[] { 2, 4, 5, 6, 7, 8, 9, 10, 11, 14, 17, 20 });
                Sm.SetGrdNumValueZero(ref Grd3, row, new int[] { 12, 13, 15, 16, 18, 19, 22 });
                if (Sm.GetLue(LueBatchNo).Length != 0) ShowBatchNoInfo(row);
            }
        }

        private void LueBatchNo_Enter(object sender, EventArgs e)
        {
            if (fCell.ColIndex == 8 && Sm.GetGrdStr(Grd3, fCell.RowIndex, fCell.ColIndex).Length == 0)
                SendKeys.Send(" ");
        }

        private void LueBatchNo_Leave(object sender, EventArgs e)
        {
            if (LueBatchNo.Visible && fAccept && fCell.ColIndex == 8)
                LueBatchNo.Visible = false;
        }

        private void LueBatchNo_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);    
        }

        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue1(SetLueItCode));
            if (LueItCode.Visible && fAccept && fCell.ColIndex == 4)
            {
                int row = fCell.RowIndex, col = fCell.ColIndex;
                Sm.SetGrdValueNull(ref Grd1, row, new int[] { 2, 4, 5, 7, 9 });
                Sm.SetGrdNumValueZero(ref Grd1, row, new int[] { 6, 8, 10 });
                Sm.SetGrdBoolValueFalse(ref Grd1, row, new int[] { 11 });
                if (Sm.GetLue(LueItCode).Length != 0) ShowItCodeInfo(row);
            }
        }

        private void LueItCode_Enter(object sender, EventArgs e)
        {
            if (fCell.ColIndex== 4 && Sm.GetGrdStr(Grd1, fCell.RowIndex, fCell.ColIndex).Length == 0)
                SendKeys.Send(" ");
        }

        private void LueItCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e); 
        }

        private void LueItCode_Leave(object sender, EventArgs e)
        {
            if (LueItCode.Visible && fAccept && fCell.ColIndex == 4)
                LueItCode.Visible = false;
        }

        private void LueMachineCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueMachineCode, new Sm.RefreshLue2(SetLueMachineCode), TxtWorkCenterDocNo.Text);
                if (mIsAssetShortCodeAddedToSFCResultBatchNo)
                {
                    RefractorBatchNo();
                }

                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsSFCUseOptionBatchNoFormula)
                    {
                        for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                        {
                            GenerateBatchNo(i);
                        }
                    }
                }
            }
        }

        private void LueBatchCategoryCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBatchCategoryCode, new Sm.RefreshLue2(Sl.SetLueOption), "SFCBatchCategoryCode");
                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsSFCUseOptionBatchNoFormula)
                    {
                        for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                        {
                            GenerateBatchNo(i);
                        }
                    }
                }
            }
        }

        #endregion

        #region Button Event

        private void BtnPPDocNo_Click(object sender, EventArgs e)
        {
            if (mPPDocNo.Length!=0)
            {
                if (TxtPPDocNo.Text.Length!=0 && TxtWorkCenterDocNo.Text.Length!=0)
                    mPPDocNo = TxtPPDocNo.Text;
            }
            Sm.FormShowDialog(new FrmShopFloorControl2Dlg(this, mPPDocNo));
        }

        private void BtnPPDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPPDocNo, "Production planning#", false))
            {
                var f1 = new FrmPP(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtPPDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void BtnWorkCenterDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWorkCenterDocNo, "Work center#", false))
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtWorkCenterDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 4, 5 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6, 8 }, e);

            if (e.ColIndex == 6)
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 6, 8, 7, 9);

            if (e.ColIndex == 8)
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 8, 6, 9, 7);

            if (e.ColIndex == 6 && Sm.CompareGrdStr(Grd1, e.RowIndex, 7, Grd1, e.RowIndex, 9))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 8, Grd1, e.RowIndex, 6);

        }

        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (
                        e.KeyChar == Char.Parse(" ") &&
                        !Sm.IsTxtEmpty(TxtPPDocNo, "Planning#", false) &&
                        !Sm.IsTxtEmpty(TxtWorkCenterDocNo, "Work center", false)
                        )
                        Sm.FormShowDialog(
                            new FrmShopFloorControl2Dlg2(
                                this,
                                TxtPPDocNo.Text,
                                TxtWorkCenterDocNo.Text,
                                mAssetShortCode
                                ));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 4, 5, 6, 8 }, e.ColIndex))
                {
                    if (e.ColIndex == 4) LueRequestEdit2(Grd1, LueItCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8, 10 });
                    Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11 });
                    SetDNo(Grd1);
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                SetDNo(Grd1);
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);

            if (BtnSave.Enabled && Grd1.CurCell != null && e.KeyCode == Keys.Enter)
            {
                int row = Grd1.CurCell.RowIndex - 1, col = Grd1.CurCell.ColIndex;
                if (col == 4)
                {
                    Sm.FocusGrd(Grd1, row, 5);
                }

                if (col == 5)
                {
                    Sm.FocusGrd(Grd1, row, 6);
                }

                if (col == 6)
                {
                    if (Grd1.Cols[8].Visible)
                        Sm.FocusGrd(Grd1, row, 8);
                    else
                    {
                        Sm.FocusGrd(Grd1, row + 1, 4);
                        if (Sm.GetGrdStr(Grd1, row + 1, 4).Length == 0) SendKeys.Send(" ");
                        if (Grd1.Rows.Count == row + 2)
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
                            SetDNo(Grd1);
                        }
                    }
                }

                if (col == 8)
                {    
                    Sm.FocusGrd(Grd1, row + 1, 4);
                    if (Sm.GetGrdStr(Grd1, row + 1, 4).Length == 0) SendKeys.Send(" ");
                    if (Grd1.Rows.Count == row + 2)
                    {
                        Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
                        SetDNo(Grd1);
                    }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (
                BtnSave.Enabled && 
                e.ColIndex == 1 &&
                !Sm.IsTxtEmpty(TxtPPDocNo, "Planning#", false) &&
                !Sm.IsTxtEmpty(TxtWorkCenterDocNo, "Work center", false)
                )
                Sm.FormShowDialog(
                    new FrmShopFloorControl2Dlg2(
                            this,
                            TxtPPDocNo.Text,
                            TxtWorkCenterDocNo.Text,
                            mAssetShortCode)
                            );

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 6, 8 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0)
                        Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Grid 2

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 8 }, e);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && TxtPPDocNo.Text.Length != 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ") && !Sm.IsDteEmpty(DteDocDt, "Date"))
                        Sm.FormShowDialog(new FrmShopFloorControl2Dlg3(this, Sm.GetDte(DteDocDt)));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 7, 8 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    SetDNo(Grd2);
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtPPDocNo.Text.Length != 0 && !Sm.IsDteEmpty(DteDocDt, "Date"))
                Sm.FormShowDialog(new FrmShopFloorControl2Dlg3(this, Sm.GetDte(DteDocDt)));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                SetDNo(Grd2);
            }
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length==0 && Grd2.Rows.Count > 0)
            {
                decimal Qty = 0m;
                bool IsFirst = true;
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length>0 && 
                        !Sm.GetGrdBool(Grd2, Row, 7))
                    {
                        if (IsFirst)
                        {
                            Qty = Sm.GetGrdDec(Grd2, Row, 8);
                            IsFirst = false;
                        }
                        Grd2.Cells[Row, 8].Value = Qty;
                    }
                }
            }
        }

        private void Grd2_BeforeContentsSorted(object sender, EventArgs e)
        {
            if (Grd2.Rows.Count > 1)
                Grd2.Rows.RemoveAt(Grd2.Rows.Count - 1);
        }

        private void Grd2_AfterContentsSorted(object sender, EventArgs e)
        {
            Grd2.Rows.Add();
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0) SetDNo(Grd2);
            Sm.SetGrdBoolValueFalse(Grd2, Grd2.Rows.Count - 1, new int[] { 7 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8 });
        }

        #endregion

        #region Grid 3

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                SetDNo(Grd3);
            }
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);

            if (BtnSave.Enabled && Grd3.CurCell != null && e.KeyCode == Keys.Enter)
            {
                int row = Grd3.CurCell.RowIndex-1, col = Grd3.CurCell.ColIndex;
                if (col == 8)
                {
                    Sm.FocusGrd(Grd3, row+1, 8);
                    if (Sm.GetGrdStr(Grd3, row+1, 8).Length == 0) SendKeys.Send(" ");
                    if (Grd3.Rows.Count == row + 2)
                    {
                        Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                        SetDNo(Grd3);
                    }
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 &&
                        mWhsCode2.Length!=0 &&
                        !Sm.IsTxtEmpty(TxtPPDocNo, "Planning#", false) &&
                        !Sm.IsTxtEmpty(TxtWorkCenterDocNo, "Work center", false)
                        )
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) 
                            Sm.FormShowDialog(new FrmShopFloorControl2Dlg4(this,
                                mWhsCode2,
                                TxtPPDocNo.Text,
                                TxtWorkCenterDocNo.Text
                                ));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 8, 13, 16, 19, 21 }, e.ColIndex))
                    {
                        if (e.ColIndex == 8) LueRequestEdit(Grd3, LueBatchNo, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd3, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                        SetDNo(Grd3);
                    }
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && 
                BtnSave.Enabled && 
                TxtDocNo.Text.Length == 0 &&
                !Sm.IsTxtEmpty(TxtPPDocNo, "Planning#", false) &&
                !Sm.IsTxtEmpty(TxtWorkCenterDocNo, "Work center", false) &&
                mWhsCode2.Length != 0 
                )
                Sm.FormShowDialog(new FrmShopFloorControl2Dlg4(this,
                    mWhsCode2, 
                    TxtPPDocNo.Text, 
                    TxtWorkCenterDocNo.Text));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 13, 16, 19 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 21 }, e);

                if (e.ColIndex == 13)
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd3, e.RowIndex, 2, 13, 16, 19, 14, 17, 20);

                if (e.ColIndex == 16)
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd3, e.RowIndex, 2, 16, 13, 19, 17, 14, 20);

                if (e.ColIndex == 13 && Sm.CompareGrdStr(Grd3, e.RowIndex, 14, Grd3, e.RowIndex, 17))
                    Sm.CopyGrdValue(Grd3, e.RowIndex, 16, Grd3, e.RowIndex, 13);

                if (e.ColIndex == 13 && Sm.CompareGrdStr(Grd3, e.RowIndex, 14, Grd3, e.RowIndex, 20))
                    Sm.CopyGrdValue(Grd3, e.RowIndex, 19, Grd3, e.RowIndex, 13);

                if (e.ColIndex == 16 && Sm.CompareGrdStr(Grd3, e.RowIndex, 17, Grd3, e.RowIndex, 20))
                    Sm.CopyGrdValue(Grd3, e.RowIndex, 19, Grd3, e.RowIndex, 16);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void Grd3_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 13, 16, 19 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, e.ColIndex).Length != 0) 
                        Total += Sm.GetGrdDec(Grd3, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #endregion

        #endregion

        #region Class

        private class Stock
        {
            public string ItCode { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string PropCode { get; set; }
            public string PropName { get; set; }
            public string BatchNo { get; set; }            
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
        }

        private class BatchNo
        {
            public string BN { get; set; }
        }

       private class SFC
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Batch1 { get; set; }
            public string Batch2 { get; set; }
            public string Batch3 { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public string Remark { get; set; }
            public string ShiftCode { get; set; }
           
        }

       private class Company
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { set; get; }

        }

        #endregion
    }
}
