﻿#region Update
/*
    06/12/2020 [WED/IMS] new apps
    07/12/2020 [WED/IMS] tambah filter Periode, Group. tambah kolom Group dari COst category. kolom Cost category di hide
    25/10/2021 [VIN/IMS] yang SOContractDOcNo nya keiisi tidak ditarik 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOperationalBudgetMonitoring : RunSystem.FrmBase6
    {
        #region Field

        private string mSQL = string.Empty;
        private bool mIsFilterByDeptHR = false, mIsBudget2YearlyFormat = false;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptOperationalBudgetMonitoring(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueOption(ref LueCCGrpCode, "CostCenterGroup");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Yr, A.Mth, A.DeptCode, C.DeptName, A.BCCode, B.BCName, B.CCtCode, D.CCtName, E.OptDesc As CCGrpName, D.AcNo, A.Amt ");
            if (mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.Yr, '00' As Mth, T1.DeptCode, T1.BCCode, Sum(T1.Amt2) Amt ");
                SQL.AppendLine("    From TblBudgetSummary T1 ");
                SQL.AppendLine("    Where T1.Yr = @Yr ");
                SQL.AppendLine("    Group By T1.Yr, T1.DeptCode, T1.BCCode ");
                SQL.AppendLine(") A ");
            }
            else
            {
                SQL.AppendLine("From TblBudgetSummary A ");
            }
            SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Inner Join TblCostCategory D On B.CCtCode = D.CCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("Left Join TblOption E On D.CCGrpCode = E.OptCode And E.OptCat = 'CostCenterGroup' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Year",
                    "Month",
                    "Department Code", 
                    "Department",
                    "Budget Category Code", 
                    
                    //6-10
                    "Budget Category",
                    "Cost Category Code",
                    "Cost Category",
                    "Group",
                    "Budget Amount",

                    //11-15
                    "",
                    "JournalDocNo",
                    "AcNo",
                    "Usage Amount",
                    "Balance Amount"
                },
                new int[] 
                {
                    //0
                    60, 

                    //1-5
                    0, 0, 120, 200, 100,  
                    
                    //6-10
                    200, 120, 200, 200, 150, 

                    //11-15
                    20, 0, 0, 150, 150
                }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15 });
            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 7, 8, 12, 13 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where A.Yr = @Yr ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCGrpCode), "D.CCGrpCode", true);
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By C.DeptName, B.BCName, D.CCtName; ",
                    new string[]
                    {
                        //0
                        "Yr",  
                        //1-5
                        "Mth", "DeptCode", "DeptName", "BCCode", "BCName", 
                        //6-9
                        "CCtCode", "CCtName", "CCGrpName", "Amt", "AcNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Grd.Cells[Row, 14].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 9);
                    }, true, false, false, false
                );

                ProcessJournalUsage();

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 14, 15 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                {
                    Sm.FormShowDialog(new FrmRptOperationalBudgetMonitoringDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 13)));
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
        }

        private void ProcessJournalUsage()
        {
            var l = new List<OperationalJournal>();

            GetJournalData(ref l);
            if (l.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    foreach (var x in l.Where(w => w.AcNo == Sm.GetGrdStr(Grd1, i, 13) && w.DeptCode == Sm.GetGrdStr(Grd1, i, 3)))
                    {
                        Grd1.Cells[i, 12].Value = x.JournalDocNo;
                        Grd1.Cells[i, 14].Value = Sm.FormatNum(x.UsageAmt, 0);
                        Grd1.Cells[i, 15].Value = Sm.GetGrdDec(Grd1, i, 10) - Sm.GetGrdDec(Grd1, i, 14);
                    }
                }
            }

            l.Clear();
        }

        private void GetJournalData(ref List<OperationalJournal> l)
        {
            string DeptCode = string.Empty;
            string AcNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (DeptCode.Length > 0) DeptCode += ",";
                    if (AcNo.Length > 0) AcNo += ",";
                    DeptCode += Sm.GetGrdStr(Grd1, i, 3);
                    AcNo += Sm.GetGrdStr(Grd1, i, 13);
                }
            }

            if (DeptCode.Length > 0 && AcNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select T.DeptCode, T.AcNo, Group_COncat(Distinct T.DocNo) JournalDocNo, Sum(T.Amt) Amt From ( ");
                SQL.AppendLine("Select C.DeptCode, B.AcNo, A.DocNo, ");
                SQL.AppendLine("Case D.AcType When 'D' Then B.DAmt - B.CAmt Else B.CAmt - B.DAmt End As Amt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.CCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
                SQL.AppendLine("    And B.SOContractDocNo Is Null ");
                SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCode ");
                SQL.AppendLine("    And Find_In_Set(C.DeptCode, @DeptCode) ");
                SQL.AppendLine("Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                SQL.AppendLine("    And D.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.DeptCode, T.AcNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DeptCode", "AcNo", "JournalDocNo", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new OperationalJournal()
                            {
                                DeptCode= Sm.DrStr(dr, c[0]),
                                AcNo = Sm.DrStr(dr, c[1]),
                                JournalDocNo = Sm.DrStr(dr, c[2]),
                                UsageAmt= Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCCGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCGrpCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group");
        }

        #endregion

        #endregion

        #region Class

        private class OperationalBudget
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public string CCtCode { get; set; }
            public string CCtName { get; set; }
            public string JournalDocNoAcNo { get; set; }
            public decimal BudgetAmt { get; set; }
            public decimal UsageAmt { get; set; }
            public decimal BalanceAmt { get; set; }
        }

        private class OperationalJournal
        {
            public string DeptCode { get; set; }
            public string AcNo { get; set; }
            public string JournalDocNo { get; set; }
            public decimal UsageAmt { get; set; }
        }

        #endregion

    }
}
