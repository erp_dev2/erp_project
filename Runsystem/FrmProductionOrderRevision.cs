﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionOrderRevision : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmProductionOrderRevisionFind FrmFind;

        #endregion

        #region Constructor

        public FrmProductionOrderRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Production Order Revision";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, TxtProductionOrderDocNo, 
                        TxtItCode, TxtItName, TxtQtyOld, TxtPlanningUomCode, TxtQty, MeeRemark
                    }, true);
                    BtnProductionOrderDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtQty, MeeRemark
                    }, false);
                    TxtStatus.Text = "Outstanding";
                    BtnProductionOrderDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtProductionOrderDocNo, 
                TxtItCode, TxtItName, TxtPlanningUomCode, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQtyOld }, 0);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionOrderRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionOrderRevision", "TblProductionOrderRevision");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProductionOrderRevision(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtProductionOrderDocNo, "Production Order#", false) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity (New)", true) ||
                IsProductionOrderStillOutstanding() ||
                IsQtyNotValid()
                ;
        }

        private bool IsProductionOrderStillOutstanding()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT b.DocNo ");
            SQL.AppendLine("FROM TblProductionOrderRevision a ");
            SQL.AppendLine("INNER JOIN TblProductionOrderHdr b ");
            SQL.AppendLine("ON a.ProductionOrderDocNo = b.DocNo ");
            SQL.AppendLine("WHERE a.ProductionOrderDocNo = @ProductionOrderDocNo AND a.`Status` = 'O';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Production Order ( " + TxtProductionOrderDocNo.Text + " ) is stored yet still outstanding.");
                return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            decimal 
                Qty = 0m,
                OtherProductionOrderQty1 = 0m,
                OtherProductionOrderQty2 = 0m, 
                SFCQty = 0m;

            if (TxtQty.Text.Length > 0) Qty = Decimal.Parse(TxtQty.Text);
            OtherProductionOrderQty1 = GetOtherProductionOrderQty1();
            OtherProductionOrderQty2 = GetOtherProductionOrderQty2();
            SFCQty = GetSFCQty();

            if (Qty + OtherProductionOrderQty1 + OtherProductionOrderQty2 - SFCQty < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "SFC's quantity is bigger than production orders's quantity.");
                return true;
            }
            return false;
        }

        private decimal GetOtherProductionOrderQty1()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Sum(T.Qty) As Qty ");
            SQL.AppendLine("From TblProductionOrderHdr T ");
            SQL.AppendLine("Where T.ItCode=@ItCode ");
            SQL.AppendLine("And T.DocNo<>@ProductionOrderDocNo ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine("And T.DocNo In ( ");
            SQL.AppendLine("    Select B.ProductionOrderDocNo ");
            SQL.AppendLine("    From TblPPHdr A ");
            SQL.AppendLine("    Inner Join TblPPDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocNo In ( ");
            SQL.AppendLine("        Select T1.DocNo ");
            SQL.AppendLine("        From TblPPHdr T1 ");
            SQL.AppendLine("        Inner Join TblPPDtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.ProductionOrderDocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Not Exists(");
            SQL.AppendLine("    Select DocNo From TblProductionOrderRevision ");
            SQL.AppendLine("    Where Status='O' ");
            SQL.AppendLine("    And ProductionOrderDocNo=T.DocNo ");
            SQL.AppendLine(");");

            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            cm.CommandText = SQL.ToString();
            var Qty = Sm.GetValue(cm);
            if (Qty.Length > 0) return decimal.Parse(Qty);
            return 0;
        }

        private decimal GetOtherProductionOrderQty2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Sum(X.Qty) As Qty ");
            SQL.AppendLine("From TblProductionOrderRevision X ");
            SQL.AppendLine("Where X.Status='O' ");
            SQL.AppendLine("And X.ProductionOrderDocNo In (");
            SQL.AppendLine("    Select T.DocNo ");
            SQL.AppendLine("    From TblProductionOrderHdr T ");
            SQL.AppendLine("    Where T.ItCode=@ItCode ");
            SQL.AppendLine("    And T.CancelInd='N' ");
            SQL.AppendLine("    And T.DocNo In ( ");
            SQL.AppendLine("        Select B.ProductionOrderDocNo ");
            SQL.AppendLine("        From TblPPHdr A ");
            SQL.AppendLine("        Inner Join TblPPDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.DocNo In ( ");
            SQL.AppendLine("            Select T1.DocNo ");
            SQL.AppendLine("            From TblPPHdr T1 ");
            SQL.AppendLine("            Inner Join TblPPDtl T2 ");
            SQL.AppendLine("                On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.ProductionOrderDocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And Exists(");
            SQL.AppendLine("        Select DocNo From TblProductionOrderRevision ");
            SQL.AppendLine("        Where Status='O' ");
            SQL.AppendLine("        And ProductionOrderDocNo=T.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(");");

            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            cm.CommandText = SQL.ToString();
            var Qty = Sm.GetValue(cm);
            if (Qty.Length > 0) return decimal.Parse(Qty);
            return 0;
        }

        private decimal GetSFCQty()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Sum(B.Qty) As Qty ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.PPDocNo In ( ");
            SQL.AppendLine("    Select T1.DocNo ");
            SQL.AppendLine("    From TblPPHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.ProductionOrderDocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine(");");

            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            cm.CommandText = SQL.ToString();
            var Qty = Sm.GetValue(cm);
            if (Qty.Length > 0) return decimal.Parse(Qty);
            return 0;
        }

        private MySqlCommand SaveProductionOrderRevision(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblProductionOrderRevision(DocNo, DocDt, `Status`, ProductionOrderDocNo, QtyOld, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@DocNo, @DocDt, 'O', @ProductionOrderDocNo, @QtyOld, @Qty, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ProductionOrderRevision';");

            SQL.AppendLine("Update TblProductionOrderRevision Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ProductionOrderRevision' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblProductionOrderHdr Set Qty=@Qty ");
            SQL.AppendLine("Where DocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ProductionOrderRevision' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@QtyOld", decimal.Parse(TxtQtyOld.Text));
            Sm.CmParam<Decimal>(ref cm, "@Qty", decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowProductionOrderRevision(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProductionOrderRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT a.DocNo, a.DocDt, ");
            SQL.AppendLine("CASE a.`Status` ");
            SQL.AppendLine("  WHEN 'O' THEN 'Outstanding' ");
            SQL.AppendLine("  WHEN 'A' THEN 'Approved' ");
            SQL.AppendLine("  WHEN 'C' THEN 'Cancelled' ");
            SQL.AppendLine("END AS `Status`, ");
            SQL.AppendLine("a.ProductionOrderDocNo, b.ItCode, c.ItName, a.QtyOld, c.PlanningUomCode, a.Qty, a.Remark ");
            SQL.AppendLine("FROM TblProductionOrderRevision a ");
            SQL.AppendLine("INNER JOIN TblProductionOrderHdr b ON a.ProductionOrderDocNo = b.DocNo ");
            SQL.AppendLine("INNER JOIN TblItem c ON b.ItCode = c.ItCode ");
            SQL.AppendLine("WHERE a.DocNo = @DocNo; ");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Status", "ProductionOrderDocNo", "ItCode", "ItName",    
                        
                        //6-9
                        "QtyOld", "PlanningUomCode", "Qty", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        TxtProductionOrderDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtQtyOld.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[6]), 0);
                        TxtPlanningUomCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);                        
                    }, true
                );
        }       

        #endregion

        #endregion

        #region Events

        #region Button Event

        private void BtnProductionOrderDocNo_Click(object sender, EventArgs e)
            {
                Sm.FormShowDialog(new FrmProductionOrderRevisionDlg(this));
            }

            #endregion

            #region Misc Control Event

            private void TxtQty_Validated(object sender, EventArgs e)
            {
                Sm.FormatNumTxt(TxtQty, 0);
            }

            #endregion

        #endregion
    }
}
