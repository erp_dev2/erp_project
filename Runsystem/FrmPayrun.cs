﻿#region Update
/* 
    06/05/2017 [TKG] Menghapus tunjangan cuti besar apabila payrun dicancel untuk (KMI)
    10/10/2017 [TKG] 
        menggunakan parameter IsFilterBySiteHR untuk filter site berdasarkan group
        menggunakan parameter IsFilterByDeptHR untuk filter department berdasarkan group
    19/10/2017 [TKG] tambah indikator end of year tax process
    20/12/2017 [TKG] bila payrun dicancel maka data EOYTax table tsb akan dihapus.
    13/04/2018 [TKG] Payrun code berdasarkan start date atau end date menggunakan parameter PayrunCodeFormatType
    12/07/2018 [TKG] tambah ss year dan ss month
    04/05/2019 [TKG] tambah validasi untuk outstanding OT amount (VIR)
    21/01/2020 [TKG/IMS] Berdasarkan parameter IsPayrunSiteEnabled, aplikasi payrun menggunakan site atau tidak.
    02/12/2020 [TKG/PHT] delete data payrollprocessssprogram ketika payrun dicancel.
    28/01/2021 [TKG/PHT] panjang payrun name menjadi 250 karakter
    30/03/2021 [TKG/PHT] edit menggunakan list command (spy apabila koneksi terputus, proses akan gagal semua)
    10/01/2022 [TKG/PHT] Payrun name otomatis diisi dengan department dan payroll group ketika disave
    12/01/2022 [TKG/PHT] ubah GetParameter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrun : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mPayrunCode = string.Empty;
        internal FrmPayrunFind FrmFind;
        private bool
            IsInserted = false, mIsPayrunNameAutoUseDeptPG = false;
        internal bool 
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsPayrunSSYrMthEnabled = false,
            mIsSiteMandatory = false,
            mIsPayrunSiteEnabled = false;
        private string
            mPayrunCodeFormatType = "1",
            mSalaryInd = "2", 
            mADCodeEmploymentPeriod = string.Empty;

        #endregion

        #region Constructor

        public FrmPayrun(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Payrun";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtPayrunCode }, true);
                SetFormControl(mState.View);
                GetParameter();
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLueAccountPeriodStatus(ref LueStatus);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                Sl.SetLueYr(LueEOYTaxYr, string.Empty);
                Sm.SetLue(LueEOYTaxYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                if (mIsFilterBySiteHR && mIsPayrunSiteEnabled) LblSiteCode.ForeColor = Color.Red;

                if (mIsPayrunSSYrMthEnabled)
                {
                    LblSSYr.Visible = true;
                    LblSSMth.Visible = true;
                    LueSSYr.Visible = true;
                    LueSSMth.Visible = true;
                    Sl.SetLueYr(LueSSYr, string.Empty);
                    Sl.SetLueMth(LueSSMth);
                }

                //if this application is called from other application
                if (mPayrunCode.Length != 0)
                {
                    ShowData(mPayrunCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPayrunName, ChkCancelInd, LueDeptCode, LueSystemType, LuePayrunPeriod, 
                        LuePGCode, LueSiteCode, DteStartDt, DteEndDt, LueStatus, 
                        LueEOYTaxYr, LueSSYr, LueSSMth, MeeRemark
                    }, true);
                    TxtPayrunCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPayrunName, ChkCancelInd, LueDeptCode, LueSystemType, LuePayrunPeriod, 
                        LuePGCode, LueSiteCode, DteStartDt, DteEndDt, LueEOYTaxYr, 
                        MeeRemark
                    }, false);
                    if (mIsPayrunSSYrMthEnabled)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueSSYr, LueSSMth }, false);
                    TxtPayrunName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtPayrunName, ChkCancelInd, MeeRemark }, false);
                    TxtPayrunName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            IsInserted = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPayrunCode, TxtPayrunName, LueDeptCode, LueSystemType, LuePayrunPeriod, 
                LuePGCode, LueSiteCode, DteStartDt, DteEndDt, LueStatus, 
                LueEOYTaxYr, LueSSYr, LueSSMth, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ChkEOYTaxInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPayrunFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetLue(LueStatus, "O");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y": "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                if (mIsPayrunSSYrMthEnabled)
                {
                    var YrMth = Sm.Left(Sm.ServerCurrentDateTime(), 6);
                    var Yr = Sm.Left(YrMth, 4);
                    var Mth = Sm.Right(YrMth, 2);

                    if (Mth=="01")
                    {
                        Yr = (decimal.Parse(Yr)-1).ToString();
                        Mth = "12";
                    }
                    else
                        Mth = Sm.Right(string.Concat("0", (decimal.Parse(Mth)-1).ToString()), 2);
                    Sm.SetLue(LueSSYr, Yr);
                    Sm.SetLue(LueSSMth, Mth);
                }
                IsInserted = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPayrunCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPayrunCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPayrun Where PayrunCode=@PayrunCode" };
                Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (IsInserted)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PayrunCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select PayrunCode, PayrunName, CancelInd, DeptCode, SystemType, " +
                        "PayrunPeriod, PGCode, SiteCode, StartDt, EndDt, Status, EOYTaxYr, EOYTaxInd, Remark, SSYr, SSMth " +
                        "From TblPayrun Where PayrunCode=@PayrunCode;",
                        new string[] 
                        {
                            //0
                            "PayrunCode", 
                            
                            //1-5
                            "PayrunName", "CancelInd", "DeptCode", "SystemType", "PayrunPeriod", 
                            
                            //6-10
                            "PGCode", "SiteCode", "StartDt", "EndDt", "Status", 
                            
                            //11-15
                            "EOYTaxYr", "EOYTaxInd", "Remark", "SSYr", "SSMth"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPayrunCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPayrunName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                            Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[3]), string.Empty);
                            Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[5]));
                            Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[6]));
                            Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[7]), string.Empty);
                            Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[8]));
                            Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[9]));
                            Sm.SetLue(LueStatus, Sm.DrStr(dr, c[10]));
                            Sm.SetLue(LueEOYTaxYr, Sm.DrStr(dr, c[11]));
                            ChkEOYTaxInd.Checked = Sm.DrStr(dr, c[12]) == "Y";
                            MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LueSSYr, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(LueSSMth, Sm.DrStr(dr, c[15]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPayrunName, "Payrun name", false) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueSystemType, "System type") ||
                Sm.IsLueEmpty(LuePayrunPeriod, "Payrun period") ||
                Sm.IsLueEmpty(LuePGCode, "Payroll group") ||
                (mIsFilterBySiteHR && mIsPayrunSiteEnabled && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                (mIsPayrunSSYrMthEnabled && Sm.IsLueEmpty(LueSSYr, "Social security's year")) ||
                (mIsPayrunSSYrMthEnabled && Sm.IsLueEmpty(LueSSMth, "Social security's month"))
                ;
        }

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            TxtPayrunCode.EditValue = GeneratePayrunCode();

            if (mIsPayrunNameAutoUseDeptPG)
            {
                TxtPayrunName.Text = string.Concat(
                    TxtPayrunName.Text, 
                    " ", LueDeptCode.GetColumnValue("Col2"),
                    " ", LuePGCode.GetColumnValue("Col2")); 
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPayrun(PayrunCode, PayrunName, CancelInd, EOYTaxYr, EOYTaxInd, DeptCode, SystemType, PayrunPeriod, PGCode, SiteCode, StartDt, EndDt, Status, SSYr, SSMth, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PayrunCode, @PayrunName, @CancelInd, @EOYTaxYr, @EOYTaxInd, @DeptCode, @SystemType, @PayrunPeriod, @PGCode, @SiteCode, @StartDt, @EndDt, @Status, @SSYr, @SSMth, @Remark, @UserCode, CurrentDateTime());");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            Sm.CmParam<String>(ref cm, "@PayrunName", TxtPayrunName.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@EOYTaxYr", Sm.GetLue(LueEOYTaxYr));
            Sm.CmParam<String>(ref cm, "@EOYTaxInd", ChkEOYTaxInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@SSYr", Sm.GetLue(LueSSYr));
            Sm.CmParam<String>(ref cm, "@SSMth", Sm.GetLue(LueSSMth));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            Sm.StdMsg(mMsgType.Info,
                "New payrun code : " + TxtPayrunCode.Text + Environment.NewLine +
                "Process is completed.");

            string 
                PayrunPeriod = Sm.GetLue(LuePayrunPeriod), 
                StartDt = Sm.GetDte(DteStartDt), 
                EndDt = Sm.GetDte(DteEndDt);

            BtnInsertClick(sender, e);

            Sm.SetLue(LuePayrunPeriod, PayrunPeriod);
            Sm.SetDte(DteStartDt, StartDt);
            Sm.SetDte(DteEndDt, EndDt);
        }

        private string GeneratePayrunCode()
        {
            string YrMth = string.Empty;
            
            if (mPayrunCodeFormatType=="2")
                YrMth = Sm.GetDte(DteEndDt).Substring(0, 6);
            else
                YrMth = Sm.GetDte(DteStartDt).Substring(0, 6);

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("   Select Right(Concat('0000', Convert(PayrunCode+1, Char)), 4) From ( ");
            SQL.Append("       Select Convert(Right(PayrunCode, 4), Decimal) As PayrunCode From TblPayrun ");
            SQL.Append("       Where Left(PayrunCode, 6)='" + YrMth + "' ");
            SQL.Append("       Order By Right(PayrunCode, 4) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '0001') As PayrunCode");

            return YrMth + "-" + Sm.GetValue(SQL.ToString());
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPayrunName, "Payrun name", false) ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessedToVR() ||
                IsPayrollProcessOutstandingOTAmtNotValid()
                ;
        }

        private bool IsPayrollProcessOutstandingOTAmtNotValid()
        {
            if (!ChkCancelInd.Checked) return false;
            return
               Sm.IsDataExist(
                   "Select 1 From TblPayrollProcessOutstandingOTAmt Where PayrunCode=@Param And ProcessBy Is Not Null;",
                   TxtPayrunCode.Text,
                   "This Payrun's outstanding OT amount already processed by another payruns." + Environment.NewLine +
                   "You need to cancel those payruns."
               );
        }

        private bool IsDataAlreadyProcessedToVR()
        {
            return Sm.IsDataExist(
               "Select PayrunCode From TblPayrun " +
               "Where Payruncode=@Param And CancelInd='N' And VoucherRequestPayrollInd='F';",
               TxtPayrunCode.Text,
               "This data already processed to voucher request."
               );
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select PayrunCode From TblPayrun Where Payruncode=@Param And CancelInd='Y';",
                TxtPayrunCode.Text,
                "This data already cancelled."
                );
        }

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPayrun Set ");
            SQL.AppendLine("    PayrunName=@PayrunName, CancelInd=@CancelInd, Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode; ");

            if (ChkCancelInd.Checked)
            {
                SQL.AppendLine("Update TblPayrollProcessOutstandingOTAmt Set ProcessBy=Null ");
                SQL.AppendLine("Where ProcessBy Is Not Null And ProcessBy=@PayrunCode;");

                SQL.AppendLine("Update TblEmpSSListDtl Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblProductionIncentiveDtl Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustmentHdr Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustment2Dtl Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblPNTDtl4 Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblPNT2Dtl5 Set PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                if (mSalaryInd == "2" && mADCodeEmploymentPeriod.Length > 0)
                {
                    SQL.AppendLine("Delete From TblEmpAD ");
                    SQL.AppendLine("Where PayrunCode Is Not Null ");
                    SQL.AppendLine("And PayrunCode=@PayrunCode ");
                    SQL.AppendLine("And ADCode=@ADCodeEmploymentPeriod;");
                }

                SQL.AppendLine("Update TblEmpSCIDtl Set ");
                SQL.AppendLine("    PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Update TblAnnualLeaveAllowanceDtl Set ");
                SQL.AppendLine("    PayrunCode=Null ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode;");

                SQL.AppendLine("Delete From TblEOYTax ");
                SQL.AppendLine("Where PayrunCode Is Not Null And PayrunCode=@PayrunCode; ");

                SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode; ");

                SQL.AppendLine("Delete From TblPayrollProcessAD ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode; ");

                SQL.AppendLine("Delete From TblPayrollProcessADOT ");
                SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode; ");

                SQL.AppendLine("Delete From TblPayrollProcessSSProgram ");
                SQL.AppendLine("Where PayrunCode=@PayrunCode; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            Sm.CmParam<String>(ref cm, "@PayrunName", TxtPayrunName.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);

            if (mSalaryInd == "2" && mADCodeEmploymentPeriod.Length > 0)
                Sm.CmParam<String>(ref cm, "@ADCodeEmploymentPeriod", mADCodeEmploymentPeriod);
            
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);

            ShowData(TxtPayrunCode.Text);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsPayrunSSYrMthEnabled = Sm.GetParameterBoo("IsPayrunSSYrMthEnabled");
            mIsPayrunSiteEnabled = Sm.GetParameterBoo("IsPayrunSiteEnabled");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterByDeptHR', 'IsFilterBySiteHR', 'IsSiteMandatory', 'SalaryInd', 'ADCodeEmploymentPeriod', ");
            SQL.AppendLine("'IsPayrunNameAutoUseDeptPG', 'IsPayrunSSYrMthEnabled', 'IsPayrunSiteEnabled', 'PayrunCodeFormatType' ");            
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();

                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsPayrunSSYrMthEnabled": mIsPayrunSSYrMthEnabled = ParValue == "Y"; break;
                            case "IsPayrunSiteEnabled": mIsPayrunSiteEnabled = ParValue == "Y"; break;
                            case "IsPayrunNameAutoUseDeptPG": mIsPayrunNameAutoUseDeptPG = ParValue == "Y"; break;

                            //string
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            case "PayrunCodeFormatType": mPayrunCodeFormatType = ParValue; break;
                            case "ADCodeEmploymentPeriod": mADCodeEmploymentPeriod = ParValue; break;
                                


                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR?"Y":"N");
        }

        private void LueSystemType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSystemType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpSystemType");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        private void TxtPayrunCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPayrunCode);
        }

        private void TxtPayrunName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPayrunName);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(Sl.SetLueAccountPeriodStatus));
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty,
                (mIsFilterBySiteHR)? "Y" : "N");
        }

        private void LueEOYTaxYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ChkEOYTaxInd.Checked = Sm.GetLue(LueEOYTaxYr).Length > 0;
        }

        #endregion

        #endregion
    }
}
