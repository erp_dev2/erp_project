﻿#region Update
/* 
    15/08/2017 [TKG] tambah delivery date.
    23/12/2017 [TKG] tambah filter item.
    23/02/2021 [WED/SIER] tambah informasi Customer Category berdasarkan parameter IsCustomerComboBasedOnCategory
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSO2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmSO2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSO2Find(FrmSO2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("A.LocalDocNo, ");
            SQL.AppendLine("Case Status When 'O' Then 'Outstanding' When 'P' Then 'Partial Fullfiled' When 'M' Then 'Manual Fullfiled' When 'F' Then 'FullFilled' End As Status, CancelInd, ");
            SQL.AppendLine("K.CtCtName, A.SAAddress, B.CtName, A.CtPONo, A.CtQtDocNo, A.Amt, ");
            SQL.AppendLine("F.ItCode, H.ItCodeInternal, H.ItName, H.Specification, J.CtItCode, J.CtItName, C.DeliveryDt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join tblCustomer B on A.CtCode = B.CtCode ");
            SQL.AppendLine("Inner Join TblSODtl C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl D On A.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItemPriceHdr E On D.ItemPriceDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl F On D.ItemPriceDocNo=F.DocNo And D.ItemPriceDNo=F.DNo ");
            SQL.AppendLine("Left Join TblSOQuotPromoItem G On A.SOQuotPromoDocNo=G.DocNo And F.ItCode=G.ItCode  ");
            SQL.AppendLine("Inner Join TblItem H On F.ItCode=H.ItCode ");
            SQL.AppendLine("Left Join TblAgent I On C.AgtCode=I.AgtCode ");
            SQL.AppendLine("Left Join TblCustomerItem J On F.ItCode=J.ItCode And A.CtCode=J.CtCode ");
            SQL.AppendLine("Left Join TblCustomerCategory K On B.CtCtCode = K.CtCtCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
         
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Status",
                    "Cancel",
                    "Local#",

                    //6-10
                    "Customer's"+Environment.NewLine+"Category",
                    "Customer",
                    "Customer's"+Environment.NewLine+"PO#",
                    "Customer's"+Environment.NewLine+"Quotation",
                    "Shipping"+Environment.NewLine+"Address",

                    //11-15
                    "Amount",
                    "Item's Code",
                    "Local Code",
                    "Item's Name",
                    "Customer's"+Environment.NewLine+"Item Code",

                    //16-20
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Specification",
                    "Delivery Date",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 

                    //21-24
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 80, 60, 130, 

                    //6-10
                    180, 200, 130, 130, 200, 

                    //11-15
                    120, 100, 100, 250, 100, 

                    //16-20
                    200, 250, 100, 100, 100, 

                    //21-24
                    100, 100, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 19, 20, 21, 22, 23, 24 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 });
            if (!mFrmParent.mIsCustomerComboBasedOnCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "H.ItCode", "H.ItName", "H.ItCodeInternal" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {

                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Status", "CancelInd", "LocalDocNo", "CtCtName", 
                        
                        //6-10
                        "CtName", "CtPONo", "CtQtDocNo", "SAAddress", "Amt", 
                        
                        //11-15
                        "ItCode", "ItCodeInternal", "ItName", "CtItCode", "CtItName", 

                        //16-20
                        "Specification", "DeliveryDt", "CreateBy", "CreateDt", "LastUpBy", 

                        //21
                        "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
