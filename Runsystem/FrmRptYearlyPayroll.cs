﻿#region Update
    // 04/10/2019 [HAR] tambah infromasi boonus, thr, dan leave allowance
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptYearlyPayroll : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsNotFilterByAuthorization = false;

        #endregion

        #region Constructor

        public FrmRptYearlyPayroll(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( "); 
            SQL.AppendLine("Select  Z.PayrunCode, Z.Sequence, Z.EmpCode, Z.EmpName, Z.DeptCode, Z.Deptname, Z.PosName,  Z.Sitename, Z.Yr, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When Z.Mth = '01' then 'January' ");
            SQL.AppendLine("When Z.Mth = '02' then 'February' ");
            SQL.AppendLine("When Z.Mth = '03' then 'March' ");
            SQL.AppendLine("When Z.Mth = '04' then 'April' ");
            SQL.AppendLine("When Z.Mth = '05' then 'May' ");
            SQL.AppendLine("When Z.Mth = '06' then 'June' ");
            SQL.AppendLine("When Z.Mth = '07' then 'July' ");
            SQL.AppendLine("When Z.Mth = '08' then 'August' ");
            SQL.AppendLine("When Z.Mth = '09' then 'September' ");
            SQL.AppendLine("When Z.Mth = '10' then 'October' ");
            SQL.AppendLine("When Z.Mth = '11' then 'November' ");
            SQL.AppendLine("When Z.Mth = '12' then 'December' ");
            SQL.AppendLine("End As Mth, 'Payroll' As Source, ");
            SQL.AppendLine("ifnull(Z.Tax, 0) As Tax, ifnull(Z.SSEmployeehealth, 0) As SShealth, ifnull(Z.SSTnk, 0) As SSTnk, ");
            SQL.AppendLine("IfNull(Z.JHT, 0) As SSJHT, Z.Amt, Z.SiteCode, Z.ADleave ");
            SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, C.EmpName, C.DeptCode, D.Deptname, E.PosName, F.Sitename, Substring(B.EndDt, 5,2) As Mth, ");
            //SQL.AppendLine("    A.Tax, A.SSEmployeehealth, A.SSEmployeeEmployment, (A.SSEmployeeEmployment-G.EmployeeAmt) As SSTnk, G.EmployeeAmt As JHT, A.Amt, B.SiteCode ");
            //SQL.AppendLine("    From TblPayrollProcess1 A ");
            //SQL.AppendLine("    Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            //SQL.AppendLine("    Inner Join TblDepartment D On B.DeptCode= D.DeptCode ");
            //SQL.AppendLine("    Left Join TblPosition E On C.PosCode = E.PosCode ");
            //SQL.AppendLine("    Left Join TblSite F On B.SiteCode = F.SiteCode ");
            //SQL.AppendLine("    Left Join TblEmpSSlistDtl G On B.PayRunCode=G.PayrunCode And A.EmpCode=G.EmpCode And G.SSCode = 'JHT' ");
            //SQL.AppendLine("    Left Join TblEmpSSlisthdr H On G.DocNo = H.DocNo ");

            SQL.AppendLine("    Select '1' As Sequence, A.PayrunCode, A.EmpCode, C.EmpName, C.DeptCode, D.Deptname, E.PosName, F.Sitename, Left(B.EndDt, 4) As yr,  Substring(B.EndDt, 5,2) As Mth,  ");
	        SQL.AppendLine("    A.Tax, A.SSEmployeehealth, A.SSEmployeeEmployment, (A.SSEmployeeEmployment-G.EmployeeAmt) As SSTnk, G.EmployeeAmt As JHT, A.Amt, B.SiteCode, A.AdLeave  ");
	        SQL.AppendLine("    From TblPayrollProcess1 A  ");
	        SQL.AppendLine("    Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N'  ");
	        SQL.AppendLine("    Inner Join TblEmployee C On A.EmpCode=C.EmpCode  ");
	        SQL.AppendLine("    Inner Join TblDepartment D On B.DeptCode= D.DeptCode  ");
	        SQL.AppendLine("    Left Join TblPosition E On C.PosCode = E.PosCode  ");
	        SQL.AppendLine("    Left Join TblSite F On B.SiteCode = F.SiteCode  ");
	        SQL.AppendLine("    Left Join TblEmpSSlistDtl G On B.PayRunCode=G.PayrunCode And A.EmpCode=G.EmpCode And G.SSCode = 'JHT'  ");
	        SQL.AppendLine("    Left Join TblEmpSSlisthdr H On G.DocNo = H.DocNo  ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Z ");
            SQL.AppendLine("union All ");
            SQL.AppendLine("Select A.DocNO, '2' As Sequence, B.EmpCode, C.EmpName,  C.DeptCode, D.Deptname, E.PosName,  ");
            SQL.AppendLine("F.Sitename, Left(A.DocDt, 4) As yr,   ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '01' then 'January'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '02' then 'February'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '03' then 'March'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '04' then 'April'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '05' then 'May'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '06' then 'June'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '07' then 'July'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '08' then 'August'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '09' then 'September'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '10' then 'October'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '11' then 'November'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '12' then 'December'  ");
            SQL.AppendLine("End As Mth, 'Bonus',   ");
            SQL.AppendLine("B.tax, null, null,  null, B.Amt, C.SiteCode, 0 ");
            SQL.AppendLine("From tblbonushdr A ");
            SQL.AppendLine("Inner Join tblbonusDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join tblEmployee C On B.EmpCode = C.EmpCode  ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode= D.DeptCode  ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode = E.PosCode  ");
            SQL.AppendLine("Left Join TblSite F On C.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.`Status` = 'A'  ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select A.DocNo, '3' As Sequence,  B.EmpCode, C.EmpName,  C.DeptCode, D.Deptname, E.PosName,  ");
            SQL.AppendLine("F.Sitename, Left(A.DocDt, 4) As yr,   ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '01' then 'January'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '02' then 'February'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '03' then 'March'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '04' then 'April'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '05' then 'May'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '06' then 'June'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '07' then 'July'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '08' then 'August'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '09' then 'September'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '10' then 'October'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '11' then 'November'  ");
            SQL.AppendLine("When Substring(A.DocDt, 5,2) = '12' then 'December'  ");
            SQL.AppendLine("End As Mth, 'THR', ");
            SQL.AppendLine("B.tax, null, null,  null,  B.Amt, C.SiteCode, 0 ");
            SQL.AppendLine("From tblRHAhdr A ");
            SQL.AppendLine("Inner Join tblRHADtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join tblEmployee C On B.EmpCode = C.EmpCode  ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode= D.DeptCode  ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode = E.PosCode  ");
            SQL.AppendLine("Left Join TblSite F On C.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.`Status` = 'A' ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(")T1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5    
                        "Kode Payrun",
                        "Kode Karyawan",
                        "Karyawan",
                        "Departemen",
                        "Jabatan",

                        //6-10
                        "Nama Site",
                        "Sumber",
                        "Tahun",
                        "Bulan",
                        "THP",

                        //11
                        "Pajak",
                        "BPJS Kes",
                        "BPJS TNK",
                        "BPJS JHT",
                        "Leave Allowance"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 200, 150, 150,  
                        
                        //6-10
                        150, 100,  100, 100, 100,  

                        //11
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] {  10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4,  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = "";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), new string[] { "T1.DeptCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), new string[] { "T1.SiteCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T1.EmpCode", "T1.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "Order by T1.EmpCode, T1.Sequence ",
                        new string[]
                        {
                            //0
                            "PayrunCode", 
                            //1-5
                            "EmpCode", "EmpName", "Deptname", "PosName", "Sitename", 
                            //6-10
                            "Source", "Yr", "Mth", "Amt", "Tax",  
                            //11-13
                            "SShealth", "SSTnk", "SSJHT", "ADleave"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
