﻿namespace RunSystem
{
    partial class FrmSOContractDownpayment2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSOContractDownpayment2));
            this.panel3 = new System.Windows.Forms.Panel();
            this.MeeQualityOfGoods = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAmtBeforeTax = new DevExpress.XtraEditors.TextEdit();
            this.MeeOriginCountry = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.MeeSKBDN = new DevExpress.XtraEditors.MemoExEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtRate = new DevExpress.XtraEditors.TextEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtOthersAmt = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCurrentAmt = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblBankAcCode = new System.Windows.Forms.Label();
            this.TxtBalanceAmt = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.TxtSOContractAmtAfterTax = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TxtSOContractAmt = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnSOContractDownpaymentDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtOutstandingAmt = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtSOContractDownpaymentAmt = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtSOContractDownpaymentDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.MeeSOContractRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtPtName = new DevExpress.XtraEditors.TextEdit();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPONo = new DevExpress.XtraEditors.TextEdit();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt3 = new DevExpress.XtraEditors.DateEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtTaxAmt3 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueTaxCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt4 = new DevExpress.XtraEditors.DateEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtTaxAmt4 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.LueTaxCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo4 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt = new DevExpress.XtraEditors.DateEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtTaxAmt = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueTaxCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.LueOption = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeQualityOfGoods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBeforeTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOriginCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSKBDN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOthersAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurrentAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalanceAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractAmtAfterTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDownpaymentAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDownpaymentDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSOContractRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 649);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 649);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.MeeQualityOfGoods);
            this.panel3.Controls.Add(this.TxtAmtBeforeTax);
            this.panel3.Controls.Add(this.MeeOriginCountry);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.MeeSKBDN);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.label45);
            this.panel3.Controls.Add(this.label38);
            this.panel3.Controls.Add(this.label40);
            this.panel3.Controls.Add(this.label39);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.label42);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtAmt);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.DteDueDt);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.label34);
            this.panel3.Controls.Add(this.TxtRate);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.TxtOthersAmt);
            this.panel3.Controls.Add(this.label43);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.TxtLocalDocNo);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtCurrentAmt);
            this.panel3.Controls.Add(this.label41);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.LblBankAcCode);
            this.panel3.Controls.Add(this.TxtBalanceAmt);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.LueCurCode);
            this.panel3.Controls.Add(this.LueBankAcCode);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.TxtCtCode);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 333);
            this.panel3.TabIndex = 8;
            // 
            // MeeQualityOfGoods
            // 
            this.MeeQualityOfGoods.EnterMoveNextControl = true;
            this.MeeQualityOfGoods.Location = new System.Drawing.Point(472, 282);
            this.MeeQualityOfGoods.Margin = new System.Windows.Forms.Padding(5);
            this.MeeQualityOfGoods.Name = "MeeQualityOfGoods";
            this.MeeQualityOfGoods.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQualityOfGoods.Properties.Appearance.Options.UseFont = true;
            this.MeeQualityOfGoods.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQualityOfGoods.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeQualityOfGoods.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQualityOfGoods.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeQualityOfGoods.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQualityOfGoods.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeQualityOfGoods.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQualityOfGoods.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeQualityOfGoods.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeQualityOfGoods.Properties.MaxLength = 5000;
            this.MeeQualityOfGoods.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeQualityOfGoods.Properties.ShowIcon = false;
            this.MeeQualityOfGoods.Size = new System.Drawing.Size(289, 20);
            this.MeeQualityOfGoods.TabIndex = 45;
            this.MeeQualityOfGoods.ToolTip = "F4 : Show/hide text";
            this.MeeQualityOfGoods.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeQualityOfGoods.ToolTipTitle = "Run System";
            // 
            // TxtAmtBeforeTax
            // 
            this.TxtAmtBeforeTax.EnterMoveNextControl = true;
            this.TxtAmtBeforeTax.Location = new System.Drawing.Point(165, 240);
            this.TxtAmtBeforeTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtBeforeTax.Name = "TxtAmtBeforeTax";
            this.TxtAmtBeforeTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmtBeforeTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBeforeTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtBeforeTax.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtBeforeTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtBeforeTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtBeforeTax.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBeforeTax.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtAmtBeforeTax.Properties.ReadOnly = true;
            this.TxtAmtBeforeTax.Size = new System.Drawing.Size(201, 20);
            this.TxtAmtBeforeTax.TabIndex = 33;
            // 
            // MeeOriginCountry
            // 
            this.MeeOriginCountry.EnterMoveNextControl = true;
            this.MeeOriginCountry.Location = new System.Drawing.Point(472, 261);
            this.MeeOriginCountry.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOriginCountry.Name = "MeeOriginCountry";
            this.MeeOriginCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOriginCountry.Properties.Appearance.Options.UseFont = true;
            this.MeeOriginCountry.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOriginCountry.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOriginCountry.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOriginCountry.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOriginCountry.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOriginCountry.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOriginCountry.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOriginCountry.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOriginCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOriginCountry.Properties.MaxLength = 5000;
            this.MeeOriginCountry.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeOriginCountry.Properties.ShowIcon = false;
            this.MeeOriginCountry.Size = new System.Drawing.Size(289, 20);
            this.MeeOriginCountry.TabIndex = 43;
            this.MeeOriginCountry.ToolTip = "F4 : Show/hide text";
            this.MeeOriginCountry.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOriginCountry.ToolTipTitle = "Run System";
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(165, 49);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(201, 20);
            this.TxtStatus.TabIndex = 14;
            // 
            // MeeSKBDN
            // 
            this.MeeSKBDN.EnterMoveNextControl = true;
            this.MeeSKBDN.Location = new System.Drawing.Point(472, 240);
            this.MeeSKBDN.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSKBDN.Name = "MeeSKBDN";
            this.MeeSKBDN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSKBDN.Properties.Appearance.Options.UseFont = true;
            this.MeeSKBDN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSKBDN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSKBDN.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSKBDN.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSKBDN.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSKBDN.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSKBDN.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSKBDN.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSKBDN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSKBDN.Properties.MaxLength = 5000;
            this.MeeSKBDN.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeSKBDN.Properties.ShowIcon = false;
            this.MeeSKBDN.Size = new System.Drawing.Size(289, 20);
            this.MeeSKBDN.TabIndex = 41;
            this.MeeSKBDN.ToolTip = "F4 : Show/hide text";
            this.MeeSKBDN.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSKBDN.ToolTipTitle = "Run System";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(371, 285);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 14);
            this.label37.TabIndex = 44;
            this.label37.Text = "Quality of Goods";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(49, 243);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(115, 14);
            this.label45.TabIndex = 32;
            this.label45.Text = "Amount Before Tax";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(383, 264);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 14);
            this.label38.TabIndex = 42;
            this.label38.Text = "Origin Country";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(117, 51);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(42, 14);
            this.label40.TabIndex = 13;
            this.label40.Text = "Status";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(424, 243);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 14);
            this.label39.TabIndex = 40;
            this.label39.Text = "SKBDN";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(113, 222);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 14);
            this.label21.TabIndex = 30;
            this.label21.Text = "Amount";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(472, 303);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(289, 20);
            this.MeeRemark.TabIndex = 47;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(103, 116);
            this.label42.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(59, 14);
            this.label42.TabIndex = 20;
            this.label42.Text = "Due Date";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(421, 306);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 46;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(165, 219);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(201, 20);
            this.TxtAmt.TabIndex = 31;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(24, 72);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(135, 14);
            this.label20.TabIndex = 15;
            this.label20.Text = "Reason For Cancellation";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(165, 113);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(114, 20);
            this.DteDueDt.TabIndex = 21;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(165, 70);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(373, 20);
            this.MeeCancelReason.TabIndex = 16;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(15, 264);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(149, 14);
            this.label34.TabIndex = 34;
            this.label34.Text = "Invoice for Project Others";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRate
            // 
            this.TxtRate.EnterMoveNextControl = true;
            this.TxtRate.Location = new System.Drawing.Point(165, 198);
            this.TxtRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRate.Name = "TxtRate";
            this.TxtRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRate.Properties.Appearance.Options.UseFont = true;
            this.TxtRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRate.Size = new System.Drawing.Size(201, 20);
            this.TxtRate.TabIndex = 29;
            this.TxtRate.Validated += new System.EventHandler(this.TxtRate_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(542, 69);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 17;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtOthersAmt
            // 
            this.TxtOthersAmt.EnterMoveNextControl = true;
            this.TxtOthersAmt.Location = new System.Drawing.Point(165, 261);
            this.TxtOthersAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOthersAmt.Name = "TxtOthersAmt";
            this.TxtOthersAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOthersAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOthersAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOthersAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtOthersAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOthersAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOthersAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOthersAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtOthersAmt.Properties.ReadOnly = true;
            this.TxtOthersAmt.Size = new System.Drawing.Size(201, 20);
            this.TxtOthersAmt.TabIndex = 35;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(131, 201);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(32, 14);
            this.label43.TabIndex = 28;
            this.label43.Text = "Rate";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(165, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(114, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(11, 285);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(153, 14);
            this.label35.TabIndex = 36;
            this.label35.Text = "Invoice for Project Current";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(165, 92);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Properties.ReadOnly = true;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(443, 20);
            this.TxtLocalDocNo.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(126, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurrentAmt
            // 
            this.TxtCurrentAmt.EnterMoveNextControl = true;
            this.TxtCurrentAmt.Location = new System.Drawing.Point(165, 282);
            this.TxtCurrentAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurrentAmt.Name = "TxtCurrentAmt";
            this.TxtCurrentAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurrentAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurrentAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurrentAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCurrentAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCurrentAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCurrentAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurrentAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtCurrentAmt.Properties.ReadOnly = true;
            this.TxtCurrentAmt.Size = new System.Drawing.Size(201, 20);
            this.TxtCurrentAmt.TabIndex = 37;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(58, 95);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(104, 14);
            this.label41.TabIndex = 18;
            this.label41.Text = "Local Document#";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(11, 306);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(153, 14);
            this.label36.TabIndex = 38;
            this.label36.Text = "Invoice for Project Balance";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(165, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(373, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // LblBankAcCode
            // 
            this.LblBankAcCode.AutoSize = true;
            this.LblBankAcCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode.ForeColor = System.Drawing.Color.Red;
            this.LblBankAcCode.Location = new System.Drawing.Point(79, 158);
            this.LblBankAcCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode.Name = "LblBankAcCode";
            this.LblBankAcCode.Size = new System.Drawing.Size(83, 14);
            this.LblBankAcCode.TabIndex = 24;
            this.LblBankAcCode.Text = "Bank Account";
            this.LblBankAcCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBalanceAmt
            // 
            this.TxtBalanceAmt.EnterMoveNextControl = true;
            this.TxtBalanceAmt.Location = new System.Drawing.Point(165, 303);
            this.TxtBalanceAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalanceAmt.Name = "TxtBalanceAmt";
            this.TxtBalanceAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalanceAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalanceAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalanceAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtBalanceAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalanceAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalanceAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalanceAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtBalanceAmt.Properties.ReadOnly = true;
            this.TxtBalanceAmt.Size = new System.Drawing.Size(201, 20);
            this.TxtBalanceAmt.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(86, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(165, 177);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 250;
            this.LueCurCode.Properties.ReadOnly = true;
            this.LueCurCode.Size = new System.Drawing.Size(201, 20);
            this.LueCurCode.TabIndex = 27;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(165, 155);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 25;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 650;
            this.LueBankAcCode.Size = new System.Drawing.Size(443, 20);
            this.LueBankAcCode.TabIndex = 25;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(103, 137);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 14);
            this.label22.TabIndex = 22;
            this.label22.Text = "Customer";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(108, 180);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 14);
            this.label23.TabIndex = 26;
            this.label23.Text = "Currency";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(165, 134);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 250;
            this.TxtCtCode.Properties.ReadOnly = true;
            this.TxtCtCode.Size = new System.Drawing.Size(443, 20);
            this.TxtCtCode.TabIndex = 23;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 333);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp4;
            this.Tc1.Size = new System.Drawing.Size(772, 316);
            this.Tc1.TabIndex = 46;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp4,
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.TxtSOContractAmtAfterTax);
            this.Tp4.Controls.Add(this.label44);
            this.Tp4.Controls.Add(this.TxtSOContractAmt);
            this.Tp4.Controls.Add(this.label3);
            this.Tp4.Controls.Add(this.BtnSOContractDownpaymentDocNo);
            this.Tp4.Controls.Add(this.TxtOutstandingAmt);
            this.Tp4.Controls.Add(this.label28);
            this.Tp4.Controls.Add(this.TxtSOContractDownpaymentAmt);
            this.Tp4.Controls.Add(this.label33);
            this.Tp4.Controls.Add(this.TxtSOContractDownpaymentDocNo);
            this.Tp4.Controls.Add(this.label24);
            this.Tp4.Controls.Add(this.MeeSOContractRemark);
            this.Tp4.Controls.Add(this.label26);
            this.Tp4.Controls.Add(this.label25);
            this.Tp4.Controls.Add(this.TxtPtName);
            this.Tp4.Controls.Add(this.TxtSOContractDocNo);
            this.Tp4.Controls.Add(this.label17);
            this.Tp4.Controls.Add(this.BtnSOContractDocNo);
            this.Tp4.Controls.Add(this.label5);
            this.Tp4.Controls.Add(this.TxtPONo);
            this.Tp4.Controls.Add(this.BtnSOContractDocNo2);
            this.Tp4.Controls.Add(this.label6);
            this.Tp4.Controls.Add(this.TxtProjectName);
            this.Tp4.Controls.Add(this.label4);
            this.Tp4.Controls.Add(this.TxtProjectCode);
            this.Tp4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tp4.Name = "Tp4";
            this.Tp4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Tp4.Size = new System.Drawing.Size(766, 288);
            this.Tp4.Text = "SO Contract Information";
            // 
            // TxtSOContractAmtAfterTax
            // 
            this.TxtSOContractAmtAfterTax.EnterMoveNextControl = true;
            this.TxtSOContractAmtAfterTax.Location = new System.Drawing.Point(203, 81);
            this.TxtSOContractAmtAfterTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractAmtAfterTax.Name = "TxtSOContractAmtAfterTax";
            this.TxtSOContractAmtAfterTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractAmtAfterTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractAmtAfterTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractAmtAfterTax.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractAmtAfterTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSOContractAmtAfterTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSOContractAmtAfterTax.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractAmtAfterTax.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtSOContractAmtAfterTax.Properties.ReadOnly = true;
            this.TxtSOContractAmtAfterTax.Size = new System.Drawing.Size(242, 20);
            this.TxtSOContractAmtAfterTax.TabIndex = 57;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(21, 84);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(176, 14);
            this.label44.TabIndex = 56;
            this.label44.Text = "SO Contract Amount after Tax";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractAmt
            // 
            this.TxtSOContractAmt.EnterMoveNextControl = true;
            this.TxtSOContractAmt.Location = new System.Drawing.Point(203, 60);
            this.TxtSOContractAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractAmt.Name = "TxtSOContractAmt";
            this.TxtSOContractAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSOContractAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSOContractAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtSOContractAmt.Properties.ReadOnly = true;
            this.TxtSOContractAmt.Size = new System.Drawing.Size(242, 20);
            this.TxtSOContractAmt.TabIndex = 55;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(75, 63);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 14);
            this.label3.TabIndex = 54;
            this.label3.Text = "SO Contract Amount";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDownpaymentDocNo
            // 
            this.BtnSOContractDownpaymentDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDownpaymentDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDownpaymentDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDownpaymentDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDownpaymentDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDownpaymentDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDownpaymentDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDownpaymentDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDownpaymentDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDownpaymentDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDownpaymentDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDownpaymentDocNo.Image")));
            this.BtnSOContractDownpaymentDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDownpaymentDocNo.Location = new System.Drawing.Point(452, 40);
            this.BtnSOContractDownpaymentDocNo.Name = "BtnSOContractDownpaymentDocNo";
            this.BtnSOContractDownpaymentDocNo.Size = new System.Drawing.Size(17, 17);
            this.BtnSOContractDownpaymentDocNo.TabIndex = 53;
            this.BtnSOContractDownpaymentDocNo.ToolTip = "Show ARDP for Project Information";
            this.BtnSOContractDownpaymentDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDownpaymentDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDownpaymentDocNo.Click += new System.EventHandler(this.BtnSOContractDownpaymentDocNo_Click);
            // 
            // TxtOutstandingAmt
            // 
            this.TxtOutstandingAmt.EnterMoveNextControl = true;
            this.TxtOutstandingAmt.Location = new System.Drawing.Point(203, 123);
            this.TxtOutstandingAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstandingAmt.Name = "TxtOutstandingAmt";
            this.TxtOutstandingAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOutstandingAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstandingAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstandingAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstandingAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstandingAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstandingAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstandingAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtOutstandingAmt.Properties.ReadOnly = true;
            this.TxtOutstandingAmt.Size = new System.Drawing.Size(242, 20);
            this.TxtOutstandingAmt.TabIndex = 61;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(123, 126);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(74, 14);
            this.label28.TabIndex = 60;
            this.label28.Text = "Outstanding";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDownpaymentAmt
            // 
            this.TxtSOContractDownpaymentAmt.EnterMoveNextControl = true;
            this.TxtSOContractDownpaymentAmt.Location = new System.Drawing.Point(203, 102);
            this.TxtSOContractDownpaymentAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDownpaymentAmt.Name = "TxtSOContractDownpaymentAmt";
            this.TxtSOContractDownpaymentAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDownpaymentAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDownpaymentAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDownpaymentAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDownpaymentAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSOContractDownpaymentAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSOContractDownpaymentAmt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDownpaymentAmt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtSOContractDownpaymentAmt.Properties.ReadOnly = true;
            this.TxtSOContractDownpaymentAmt.Size = new System.Drawing.Size(242, 20);
            this.TxtSOContractDownpaymentAmt.TabIndex = 59;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(98, 105);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(99, 14);
            this.label33.TabIndex = 58;
            this.label33.Text = "ARDP for Project";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDownpaymentDocNo
            // 
            this.TxtSOContractDownpaymentDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDownpaymentDocNo.Location = new System.Drawing.Point(203, 39);
            this.TxtSOContractDownpaymentDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDownpaymentDocNo.Name = "TxtSOContractDownpaymentDocNo";
            this.TxtSOContractDownpaymentDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDownpaymentDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDownpaymentDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDownpaymentDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDownpaymentDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDownpaymentDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDownpaymentDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtSOContractDownpaymentDocNo.TabIndex = 52;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(98, 43);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 14);
            this.label24.TabIndex = 51;
            this.label24.Tag = "";
            this.label24.Text = "ARDP for Project";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeSOContractRemark
            // 
            this.MeeSOContractRemark.EnterMoveNextControl = true;
            this.MeeSOContractRemark.Location = new System.Drawing.Point(203, 228);
            this.MeeSOContractRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSOContractRemark.Name = "MeeSOContractRemark";
            this.MeeSOContractRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeSOContractRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSOContractRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeSOContractRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeSOContractRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSOContractRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSOContractRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSOContractRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSOContractRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSOContractRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSOContractRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSOContractRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSOContractRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSOContractRemark.Properties.MaxLength = 5000;
            this.MeeSOContractRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeSOContractRemark.Properties.ReadOnly = true;
            this.MeeSOContractRemark.Properties.ShowIcon = false;
            this.MeeSOContractRemark.Size = new System.Drawing.Size(443, 20);
            this.MeeSOContractRemark.TabIndex = 71;
            this.MeeSOContractRemark.ToolTip = "F4 : Show/hide text";
            this.MeeSOContractRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSOContractRemark.ToolTipTitle = "Run System";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(150, 231);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 14);
            this.label26.TabIndex = 70;
            this.label26.Text = "Remark";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(94, 210);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(103, 14);
            this.label25.TabIndex = 68;
            this.label25.Text = "Term of Payment";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPtName
            // 
            this.TxtPtName.EnterMoveNextControl = true;
            this.TxtPtName.Location = new System.Drawing.Point(203, 207);
            this.TxtPtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPtName.Name = "TxtPtName";
            this.TxtPtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPtName.Properties.Appearance.Options.UseFont = true;
            this.TxtPtName.Properties.MaxLength = 250;
            this.TxtPtName.Properties.ReadOnly = true;
            this.TxtPtName.Size = new System.Drawing.Size(443, 20);
            this.TxtPtName.TabIndex = 69;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(203, 18);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtSOContractDocNo.TabIndex = 48;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(114, 22);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 14);
            this.label17.TabIndex = 47;
            this.label17.Tag = "";
            this.label17.Text = "SO Contract#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(452, 19);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(17, 17);
            this.BtnSOContractDocNo.TabIndex = 49;
            this.BtnSOContractDocNo.ToolTip = "Find DO To Customer Based On Delivery Request Document";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(108, 168);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 14);
            this.label5.TabIndex = 64;
            this.label5.Text = "Project\'s Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPONo
            // 
            this.TxtPONo.EnterMoveNextControl = true;
            this.TxtPONo.Location = new System.Drawing.Point(203, 186);
            this.TxtPONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPONo.Name = "TxtPONo";
            this.TxtPONo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPONo.Properties.Appearance.Options.UseFont = true;
            this.TxtPONo.Properties.MaxLength = 250;
            this.TxtPONo.Properties.ReadOnly = true;
            this.TxtPONo.Size = new System.Drawing.Size(242, 20);
            this.TxtPONo.TabIndex = 67;
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(475, 19);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(17, 17);
            this.BtnSOContractDocNo2.TabIndex = 50;
            this.BtnSOContractDocNo2.ToolTip = "Show DO To Customer Based On Delivery Request Information";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(101, 189);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 14);
            this.label6.TabIndex = 66;
            this.label6.Text = "Customer\'s PO#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(203, 165);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(443, 20);
            this.TxtProjectName.TabIndex = 65;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(111, 147);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 14);
            this.label4.TabIndex = 62;
            this.label4.Text = "Project\'s Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(203, 144);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 250;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(242, 20);
            this.TxtProjectCode.TabIndex = 63;
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.Grd1);
            this.Tp1.Controls.Add(this.panel4);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 0);
            this.Tp1.Text = "Service";
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 94);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 0);
            this.Grd1.TabIndex = 57;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.DteTaxInvoiceDt2);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtTaxAmt2);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.LueTaxCode2);
            this.panel4.Controls.Add(this.TxtTaxInvoiceNo2);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 94);
            this.panel4.TabIndex = 27;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.Controls.Add(this.label14);
            this.panel9.Controls.Add(this.DteTaxInvoiceDt3);
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.TxtTaxAmt3);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.LueTaxCode3);
            this.panel9.Controls.Add(this.TxtTaxInvoiceNo3);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Controls.Add(this.simpleButton1);
            this.panel9.Controls.Add(this.simpleButton2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(395, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(371, 94);
            this.panel9.TabIndex = 57;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(47, 71);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 14);
            this.label14.TabIndex = 63;
            this.label14.Text = "Amount";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt3
            // 
            this.DteTaxInvoiceDt3.EditValue = null;
            this.DteTaxInvoiceDt3.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt3.Location = new System.Drawing.Point(103, 48);
            this.DteTaxInvoiceDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt3.Name = "DteTaxInvoiceDt3";
            this.DteTaxInvoiceDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt3.Size = new System.Drawing.Size(130, 20);
            this.DteTaxInvoiceDt3.TabIndex = 62;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(65, 51);
            this.label15.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 14);
            this.label15.TabIndex = 61;
            this.label15.Text = "Date";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt3
            // 
            this.TxtTaxAmt3.EnterMoveNextControl = true;
            this.TxtTaxAmt3.Location = new System.Drawing.Point(103, 69);
            this.TxtTaxAmt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt3.Name = "TxtTaxAmt3";
            this.TxtTaxAmt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt3.Properties.ReadOnly = true;
            this.TxtTaxAmt3.Size = new System.Drawing.Size(182, 20);
            this.TxtTaxAmt3.TabIndex = 64;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(69, 30);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 14);
            this.label16.TabIndex = 59;
            this.label16.Text = "Tax";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode3
            // 
            this.LueTaxCode3.EnterMoveNextControl = true;
            this.LueTaxCode3.Location = new System.Drawing.Point(103, 27);
            this.LueTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode3.Name = "LueTaxCode3";
            this.LueTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode3.Properties.DropDownRows = 25;
            this.LueTaxCode3.Properties.NullText = "[Empty]";
            this.LueTaxCode3.Properties.PopupWidth = 200;
            this.LueTaxCode3.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode3.TabIndex = 60;
            this.LueTaxCode3.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode3.EditValueChanged += new System.EventHandler(this.LueTaxCode3_EditValueChanged);
            // 
            // TxtTaxInvoiceNo3
            // 
            this.TxtTaxInvoiceNo3.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo3.Location = new System.Drawing.Point(103, 6);
            this.TxtTaxInvoiceNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo3.Name = "TxtTaxInvoiceNo3";
            this.TxtTaxInvoiceNo3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo3.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo3.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo3.TabIndex = 58;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(17, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 14);
            this.label19.TabIndex = 57;
            this.label19.Text = "Tax Invoice#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Appearance.Options.UseTextOptions = true;
            this.simpleButton1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(388, 27);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(24, 18);
            this.simpleButton1.TabIndex = 37;
            this.simpleButton1.ToolTip = "Show LOP";
            this.simpleButton1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.simpleButton1.ToolTipTitle = "Run System";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Appearance.Options.UseForeColor = true;
            this.simpleButton2.Appearance.Options.UseTextOptions = true;
            this.simpleButton2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(388, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(24, 18);
            this.simpleButton2.TabIndex = 34;
            this.simpleButton2.ToolTip = "Show BOQ";
            this.simpleButton2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.simpleButton2.ToolTipTitle = "Run System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(40, 71);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 14);
            this.label7.TabIndex = 51;
            this.label7.Text = "Amount";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt2
            // 
            this.DteTaxInvoiceDt2.EditValue = null;
            this.DteTaxInvoiceDt2.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt2.Location = new System.Drawing.Point(96, 48);
            this.DteTaxInvoiceDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt2.Name = "DteTaxInvoiceDt2";
            this.DteTaxInvoiceDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt2.Size = new System.Drawing.Size(130, 20);
            this.DteTaxInvoiceDt2.TabIndex = 50;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(58, 51);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 14);
            this.label9.TabIndex = 49;
            this.label9.Text = "Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt2
            // 
            this.TxtTaxAmt2.EnterMoveNextControl = true;
            this.TxtTaxAmt2.Location = new System.Drawing.Point(96, 69);
            this.TxtTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt2.Name = "TxtTaxAmt2";
            this.TxtTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt2.Properties.ReadOnly = true;
            this.TxtTaxAmt2.Size = new System.Drawing.Size(182, 20);
            this.TxtTaxAmt2.TabIndex = 52;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(62, 30);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 14);
            this.label10.TabIndex = 47;
            this.label10.Text = "Tax";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(96, 27);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 25;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 200;
            this.LueTaxCode2.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode2.TabIndex = 48;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            // 
            // TxtTaxInvoiceNo2
            // 
            this.TxtTaxInvoiceNo2.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo2.Location = new System.Drawing.Point(96, 6);
            this.TxtTaxInvoiceNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo2.Name = "TxtTaxInvoiceNo2";
            this.TxtTaxInvoiceNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo2.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo2.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo2.TabIndex = 46;
            this.TxtTaxInvoiceNo2.Validated += new System.EventHandler(this.TxtTaxInvoiceNo2_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(10, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 14);
            this.label13.TabIndex = 45;
            this.label13.Text = "Tax Invoice#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd2);
            this.Tp2.Controls.Add(this.panel5);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 0);
            this.Tp2.Text = "Inventory";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 94);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 0);
            this.Grd2.TabIndex = 61;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.DteTaxInvoiceDt);
            this.panel5.Controls.Add(this.label27);
            this.panel5.Controls.Add(this.TxtTaxAmt);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.LueTaxCode);
            this.panel5.Controls.Add(this.TxtTaxInvoiceNo);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 94);
            this.panel5.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.DteTaxInvoiceDt4);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.TxtTaxAmt4);
            this.panel6.Controls.Add(this.label31);
            this.panel6.Controls.Add(this.LueTaxCode4);
            this.panel6.Controls.Add(this.TxtTaxInvoiceNo4);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Controls.Add(this.simpleButton3);
            this.panel6.Controls.Add(this.simpleButton4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(387, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(379, 94);
            this.panel6.TabIndex = 53;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(50, 71);
            this.label29.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(51, 14);
            this.label29.TabIndex = 59;
            this.label29.Text = "Amount";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt4
            // 
            this.DteTaxInvoiceDt4.EditValue = null;
            this.DteTaxInvoiceDt4.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt4.Location = new System.Drawing.Point(106, 48);
            this.DteTaxInvoiceDt4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt4.Name = "DteTaxInvoiceDt4";
            this.DteTaxInvoiceDt4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt4.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt4.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt4.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt4.Size = new System.Drawing.Size(130, 20);
            this.DteTaxInvoiceDt4.TabIndex = 58;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(68, 51);
            this.label30.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 14);
            this.label30.TabIndex = 57;
            this.label30.Text = "Date";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt4
            // 
            this.TxtTaxAmt4.EnterMoveNextControl = true;
            this.TxtTaxAmt4.Location = new System.Drawing.Point(106, 69);
            this.TxtTaxAmt4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt4.Name = "TxtTaxAmt4";
            this.TxtTaxAmt4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt4.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt4.Properties.ReadOnly = true;
            this.TxtTaxAmt4.Size = new System.Drawing.Size(182, 20);
            this.TxtTaxAmt4.TabIndex = 60;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(74, 30);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(27, 14);
            this.label31.TabIndex = 55;
            this.label31.Text = "Tax";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode4
            // 
            this.LueTaxCode4.EnterMoveNextControl = true;
            this.LueTaxCode4.Location = new System.Drawing.Point(106, 27);
            this.LueTaxCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode4.Name = "LueTaxCode4";
            this.LueTaxCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode4.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode4.Properties.DropDownRows = 25;
            this.LueTaxCode4.Properties.NullText = "[Empty]";
            this.LueTaxCode4.Properties.PopupWidth = 200;
            this.LueTaxCode4.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode4.TabIndex = 56;
            this.LueTaxCode4.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode4.EditValueChanged += new System.EventHandler(this.LueTaxCode4_EditValueChanged);
            // 
            // TxtTaxInvoiceNo4
            // 
            this.TxtTaxInvoiceNo4.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo4.Location = new System.Drawing.Point(106, 6);
            this.TxtTaxInvoiceNo4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo4.Name = "TxtTaxInvoiceNo4";
            this.TxtTaxInvoiceNo4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo4.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo4.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo4.TabIndex = 54;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(22, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(79, 14);
            this.label32.TabIndex = 53;
            this.label32.Text = "Tax Invoice#";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Appearance.Options.UseForeColor = true;
            this.simpleButton3.Appearance.Options.UseTextOptions = true;
            this.simpleButton3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton3.Location = new System.Drawing.Point(388, 27);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(24, 18);
            this.simpleButton3.TabIndex = 37;
            this.simpleButton3.ToolTip = "Show LOP";
            this.simpleButton3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.simpleButton3.ToolTipTitle = "Run System";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Appearance.Options.UseForeColor = true;
            this.simpleButton4.Appearance.Options.UseTextOptions = true;
            this.simpleButton4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Location = new System.Drawing.Point(388, 6);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(24, 18);
            this.simpleButton4.TabIndex = 34;
            this.simpleButton4.ToolTip = "Show BOQ";
            this.simpleButton4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.simpleButton4.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(36, 70);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 14);
            this.label11.TabIndex = 51;
            this.label11.Text = "Amount";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt
            // 
            this.DteTaxInvoiceDt.EditValue = null;
            this.DteTaxInvoiceDt.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt.Location = new System.Drawing.Point(92, 47);
            this.DteTaxInvoiceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt.Name = "DteTaxInvoiceDt";
            this.DteTaxInvoiceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt.Size = new System.Drawing.Size(130, 20);
            this.DteTaxInvoiceDt.TabIndex = 50;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(54, 50);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 14);
            this.label27.TabIndex = 49;
            this.label27.Text = "Date";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt
            // 
            this.TxtTaxAmt.EnterMoveNextControl = true;
            this.TxtTaxAmt.Location = new System.Drawing.Point(92, 68);
            this.TxtTaxAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt.Name = "TxtTaxAmt";
            this.TxtTaxAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt.Properties.ReadOnly = true;
            this.TxtTaxAmt.Size = new System.Drawing.Size(182, 20);
            this.TxtTaxAmt.TabIndex = 52;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(60, 29);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 14);
            this.label12.TabIndex = 47;
            this.label12.Text = "Tax";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode
            // 
            this.LueTaxCode.EnterMoveNextControl = true;
            this.LueTaxCode.Location = new System.Drawing.Point(92, 26);
            this.LueTaxCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode.Name = "LueTaxCode";
            this.LueTaxCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode.Properties.DropDownRows = 25;
            this.LueTaxCode.Properties.NullText = "[Empty]";
            this.LueTaxCode.Properties.PopupWidth = 200;
            this.LueTaxCode.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode.TabIndex = 48;
            this.LueTaxCode.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode.EditValueChanged += new System.EventHandler(this.LueTaxCode_EditValueChanged);
            // 
            // TxtTaxInvoiceNo
            // 
            this.TxtTaxInvoiceNo.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo.Location = new System.Drawing.Point(92, 5);
            this.TxtTaxInvoiceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo.Name = "TxtTaxInvoiceNo";
            this.TxtTaxInvoiceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo.TabIndex = 46;
            this.TxtTaxInvoiceNo.Validated += new System.EventHandler(this.TxtTaxInvoiceNo_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(8, 8);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 14);
            this.label18.TabIndex = 45;
            this.label18.Text = "Tax Invoice#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.LueOption);
            this.Tp3.Controls.Add(this.Grd3);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 0);
            this.Tp3.Text = "Discount, Additional Cost, Etc";
            // 
            // LueOption
            // 
            this.LueOption.EnterMoveNextControl = true;
            this.LueOption.Location = new System.Drawing.Point(185, 26);
            this.LueOption.Margin = new System.Windows.Forms.Padding(5);
            this.LueOption.Name = "LueOption";
            this.LueOption.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.Appearance.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOption.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOption.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOption.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOption.Properties.DropDownRows = 30;
            this.LueOption.Properties.NullText = "[Empty]";
            this.LueOption.Properties.PopupWidth = 300;
            this.LueOption.Size = new System.Drawing.Size(202, 20);
            this.LueOption.TabIndex = 46;
            this.LueOption.ToolTip = "F4 : Show/hide list";
            this.LueOption.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOption.EditValueChanged += new System.EventHandler(this.LueOption_EditValueChanged);
            this.LueOption.Leave += new System.EventHandler(this.LueOption_Leave);
            this.LueOption.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueOption_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 0);
            this.Grd3.TabIndex = 45;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // FrmSOContractDownpayment2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 649);
            this.Name = "FrmSOContractDownpayment2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeQualityOfGoods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBeforeTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOriginCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSKBDN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOthersAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurrentAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalanceAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp4.ResumeLayout(false);
            this.Tp4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractAmtAfterTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDownpaymentAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDownpaymentDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSOContractRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            this.Tp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueOption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.LookUpEdit LueTaxCode;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt2;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt2;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo2;
        private System.Windows.Forms.Label label13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label21;
        protected internal DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.LookUpEdit LueOption;
        private System.Windows.Forms.Label LblBankAcCode;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        internal DevExpress.XtraEditors.MemoExEdit MeeSOContractRemark;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtPtName;
        protected internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtPONo;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDownpaymentAmt;
        private System.Windows.Forms.Label label33;
        protected internal DevExpress.XtraEditors.TextEdit TxtSOContractDownpaymentDocNo;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtOutstandingAmt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDownpaymentDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractAmt;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtBalanceAmt;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtCurrentAmt;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtOthersAmt;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.MemoExEdit MeeQualityOfGoods;
        private DevExpress.XtraEditors.MemoExEdit MeeOriginCountry;
        private DevExpress.XtraEditors.MemoExEdit MeeSKBDN;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        protected System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt3;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt3;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo3;
        private System.Windows.Forms.Label label19;
        public DevExpress.XtraEditors.SimpleButton simpleButton1;
        public DevExpress.XtraEditors.SimpleButton simpleButton2;
        protected System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt4;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt4;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode4;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo4;
        private System.Windows.Forms.Label label32;
        public DevExpress.XtraEditors.SimpleButton simpleButton3;
        public DevExpress.XtraEditors.SimpleButton simpleButton4;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.TextEdit TxtRate;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractAmtAfterTax;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtAmtBeforeTax;
        private System.Windows.Forms.Label label45;
    }
}