﻿#region Update
/*
    16/06/2021 [MYA/KSM] New Apps Monthly Outstanding DO to Customer - Sisa DO
    23/06/2021 [BRI/KSM] tambah filter item category & grid contract date
    24/06/2021 [BRI/KSM] tambah fitur print
    06/08/2021 [RDA/KSM] penyesuaian fitur print
    06/09/2021 [TKG/KSM] tambah item category, ubah query
    02/09/2021 [SET/KSM] Minta ada tambahan kolom TOP dan CBD pada print out
    20/09/2021 [SET/KSM] Menu Monthly Outstanding Do to Customer (Sisa DO) dan Menu Monthly Outstanding DO to Customer - Rekap Sisa Order muncul warning saat akan print
    10/11/2021 [DEV/KSM] Pembenahan di kolom Received Order terbalik dengan Rest Order
    19/11/2021 [MYA/KSM] Menambahkan kolom harga yang Include PPN bukan DPP
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyOutstandingDOCt2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMonthlyOutstandingDOCt2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter1, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select F.CtName, F.CtCode, G.ItName, D.DocDt, D.LocalDocNo, A.Qty, ");
            SQL.AppendLine("I.PtName, L.CBDInd, ");
            SQL.AppendLine("IfNull(B.Qty, 0.00) As Qty2, IfNull(C.Qty, 0.00) As Qty3, ");
            SQL.AppendLine("A.Qty-IfNull(B.Qty, 0.00)-IfNull(C.Qty, 0.00) As Balance, A.UPrice, A.UPriceAfterTax, H.ItCtName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.SalesMemoDocNo, T3.ItCode, T3.UPrice, T3.UPriceAfterTax, Sum(T3.Qty) As Qty ");
            SQL.AppendLine("    From TblSalesContract T1 ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr T2 ON T1.SalesMemoDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            SQL.AppendLine(Filter2.Replace("X.", "T2."));
            SQL.AppendLine("    Inner Join TblSalesMemoDtl T3 ON T2.DocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItem T4 ON T3.ItCode=T4.ItCode ");
            SQL.AppendLine(Filter3.Replace("X.", "T4."));
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter1.Replace("X.", "T1."));
            SQL.AppendLine("    Group By T1.DocNo, T1.SalesMemoDocNo, T3.ItCode, T3.UPrice ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.SCDocNo, T2.SODocNo, T4.ItCode, Sum(T4.Qty) As Qty ");
            SQL.AppendLine("    From TblDRHdr T1 ");
            SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 ");
            SQL.AppendLine("        On T2.DocNo = T3.DRDocNo ");
            SQL.AppendLine("        And T3.DocDt < @DocDt1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 ON T3.DocNo = T4.DocNo And T4.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblItem T5 ON T4.ItCode=T5.ItCode ");
            SQL.AppendLine(Filter3.Replace("X.", "T5."));
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T2.SCDocNo, T2.SODocNo, T4.ItCode ");
            SQL.AppendLine(") B On A.DocNo=B.SCDocNo And A.SalesMemoDocNo=B.SODocNo And A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.SCDocNo, T2.SODocNo, T4.ItCode, Sum(T4.Qty) As Qty ");
            SQL.AppendLine("    From TblDRHdr T1 ");
            SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 On T2.DocNo=T3.DRDocNo And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 On T3.DocNo = T4.DocNo And T4.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblItem T5 ON T4.ItCode=T5.ItCode ");
            SQL.AppendLine(Filter3.Replace("X.", "T5."));
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T2.SCDocNo, T2.SODocNo, T4.ItCode  ");
            SQL.AppendLine(") C On A.DocNo=C.SCDocNo And A.SalesMemoDocNo=C.SODocNo And A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblSalesContract D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblSalesMemoHdr E ON A.SalesMemoDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer F On E.CtCode=F.CtCode ");
            SQL.AppendLine("Inner Join TblItem G On A.ItCode=G.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory H On G.ItCtCode=H.ItCtCode ");
            SQL.AppendLine("Inner Join TblPaymentterm I On D.PtCode = I.PtCode ");
            SQL.AppendLine("Inner Join TblSalesMemoDtl J ON E.DocNo = J.DocNo ");
            SQL.AppendLine("Left Join TblDRDtl K On D.DocNo = K.SCDocNo and J.DocNo = K.SODocNo ");
            SQL.AppendLine("Left join TblDRHdr L on K.DocNo = L.DocNo ");
            SQL.AppendLine("Order By D.DocDt, D.LocalDocNo; ");

            //SQL.AppendLine("Select F.CtName, F.CtCode, G.ItName, A.DocDt, A.LocalDocNo, IfNull(SUM(C.Qty),0) As Qty, ");
            //SQL.AppendLine("IfNull(D.Qty,0) As LastMonthDOQty, IfNull(E.Qty,0) DOQty, ");
            //SQL.AppendLine("IfNull(SUM(C.Qty),0) - IfNull(D.Qty,0) - IfNull(E.Qty,0) RemainingDOQty, C.UPrice ");
            //SQL.AppendLine("From TblSalesContract A ");
            //SQL.AppendLine("Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo = B.DocNo ");
            //SQL.AppendLine("    And B.CancelInd = 'N' ");
            //SQL.AppendLine("    And B.Status In ('O', 'A') ");
            //SQL.AppendLine("Inner Join TblSalesMemoDtl C ON B.DocNo=C.DocNo ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T2.SCDocNo, T2.SODocNo, T4.ItCode, Sum(T4.Qty) As Qty ");
            //SQL.AppendLine("    From TblDRHdr T1 ");
            //SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 ");
            //SQL.AppendLine("        On T2.DocNo = T3.DRDocNo ");
            //SQL.AppendLine("        And T3.DocDt < @DocDt1 ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 ON T3.DocNo = T4.DocNo And T4.CancelInd = 'N' ");
            //SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            //SQL.AppendLine("    Group By T2.SCDocNo, T2.SODocNo, T4.ItCode ");
            //SQL.AppendLine(") D ON A.DocNo=D.SCDocNo And C.DocNo = D.SODocNo And C.ItCode=D.ItCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T2.SCDocNo, T2.SODocNo, T4.ItCode, Sum(T4.Qty) As Qty ");
            //SQL.AppendLine("    From TblDRHdr T1 ");
            //SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 On T2.DocNo=T3.DRDocNo And T3.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 On T3.DocNo = T4.DocNo And T4.CancelInd = 'N' ");
            //SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            //SQL.AppendLine("    Group By T2.SCDocNo, T2.SODocNo, T4.ItCode  ");
            //SQL.AppendLine(") E ON A.DocNo = E.SCDocNo And C.DocNo = E.SODocNo And C.ItCode=E.ItCode  ");
            //SQL.AppendLine("Inner Join TblCustomer F On B.CtCode = F.CtCode ");
            //SQL.AppendLine("Inner Join TblItem G On C.ItCode=G.ItCode ");
            //SQL.AppendLine("Where A.CancelInd = 'N' ");
            //SQL.AppendLine("And A.Status In ('O', 'A') ");
            //SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Customer's" + Environment.NewLine + "Name",
                    "Customer's" + Environment.NewLine + "Code",
                    "Construction",
                    "Item's Category",
                    "Contract Date",
                    
                    //6-10
                    "Contract#",
                    "Price",
                    "Price After Tax",
                    "Rest Order",
                    "Order Sent" + Environment.NewLine + "Last Periode",
                    

                    //11-14
                    "Order Sent" + Environment.NewLine + "In Periode",
                    "Received" + Environment.NewLine + "Order",
                    "PaymentType",
                    "CBD"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    250, 120, 200, 200, 100, 

                    //6-10
                    200, 120, 120, 120, 120, 

                    //11-14
                    120, 120, 170, 50
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14 });
            Sm.GrdColCheck(Grd1, new int[] { 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (Grd1.Rows.Count > 0) Grd1.Rows[0].BackColor = Color.White;
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter1 = " ", Filter2 = " ", Filter3 = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter1, ref cm, TxtLocalDocNo.Text, new string[] { "X.LocalDocNo" } );
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueCtCode), "X.CtCode", true);
                Sm.FilterStr(ref Filter3, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName" }); 
                Sm.FilterStr(ref Filter3, ref cm, Sm.GetLue(LueItCtCode), "X.ItCtCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL(Filter1, Filter2, Filter3),
                new string[]
                    {
                        //0
                        "CtName",

                        //1-5
                        "CtCode",
                        "ItName",
                        "ItCtName",
                        "DocDt",
                        "LocalDocNo",
                        
                        //6-10
                        "UPrice",
                        "UPriceAfterTax",
                        "Balance",                        
                        "Qty2",
                        "Qty3",
                        

                        //11-12
                        "Qty",
                        "PtName",
                        "CBDInd"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 13);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }
                iGSubtotalManager.ForeColor = Color.Black;

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Additional Method

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<Header>();
            var ldtl = new List<Detail>();

            string[] TableName = { "Header", "Detail" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            l.Add(new Header()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
            });
            myLists.Add(l);

            #endregion

            #region Detail

            int nomor = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                nomor = nomor + 1;
                ldtl.Add(new Detail()
                {
                    No = nomor,
                    CtName = Sm.GetGrdStr(Grd1, i, 1),
                    CtCode = Sm.GetGrdStr(Grd1, i, 2),
                    Construction = Sm.GetGrdStr(Grd1, i, 3),
                    ContractDt = Sm.GetGrdText(Grd1, i, 5),
                    NoContract = Sm.GetGrdStr(Grd1, i, 6),
                    RestOrder = Sm.GetGrdDec(Grd1, i, 9),
                    LastPeriode = Sm.GetGrdDec(Grd1, i, 10),
                    InPeriode = Sm.GetGrdDec(Grd1, i, 11),                    
                    ReceiveOrder = Sm.GetGrdDec(Grd1, i, 12),
                    Price = Sm.GetGrdDec(Grd1, i, 7),
                    PtName = Sm.GetGrdStr(Grd1, i, 13),
                    CBDInd = Sm.GetGrdBool(Grd1, i, 14)
                });

            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("OutstandingSCKSM", myLists, TableName, false);

        }

        #endregion

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Document#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeActive), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        #endregion

        #region Class

        private class Header
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
        }

        private class Detail
        {
            public int No { get; set; }
            public string CtName { get; set; }
            public string CtCode { get; set; }
            public string Construction { get; set; }
            public string ContractDt { get; set; }
            public string NoContract { get; set; }
            public decimal RestOrder { get; set; }            
            public decimal LastPeriode { get; set; }
            public decimal InPeriode { get; set; }
            public decimal ReceiveOrder { get; set; }
            public decimal Price { get; set; }
            public string PtName { get; set; }
            public bool CBDInd { get; set; }
        }

        #endregion

    }
}
