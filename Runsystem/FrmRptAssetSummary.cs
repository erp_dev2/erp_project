﻿#region Update
/*
    28/10/2021 [TYO/PHT] Menambah kolom Site di Asset summary
    29/10/2021 [VIN/ALL] BUG: Site berdasarkan kolom mIsAssetShowAdditionalInformation di left join 
    09/11/2021 [ICA/ALL] menghapus Environment.NewLine di kolom cost center name agar ketika group by CCName, CCName muncul
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAssetSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool 
            mIsShowForeignName = false, 
            mIsFilterByItCt = false, 
            mIsAssetShowAdditionalInformation = false;

        #endregion

        #region Constructor

        public FrmRptAssetSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();
                Sl.SetLueCCCode(ref LueCCCode);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsAssetShowAdditionalInformation = Sm.GetParameterBoo("IsAssetShowAdditionalInformation");
        }

        override protected void SetSQL()
        {
            
            var SQL = new StringBuilder();

            SQL.AppendLine(" Select A.AssetCode, B.AssetName, B.DisplayName, B.ItCode, D.ItName, A.CCCode, C.CCName, D.ForeignName, E.SiteName ");
            SQL.AppendLine(" From TblAssetSummary A ");
            SQL.AppendLine(" Left Join TblAsset B On A.AssetCode = B.AssetCode And B.ActiveInd = 'Y' ");
            SQL.AppendLine(" Left Join TblCostCenter C On A.CCCode = C.CCCOde  ");
            SQL.AppendLine(" Inner Join TblItem D On B.ItCode = D.ItCode  ");
            if (mIsAssetShowAdditionalInformation)
                SQL.AppendLine(" Inner Join TblSite E On B.SiteCode = E.SiteCode ");
            else
                SQL.AppendLine(" Left Join TblSite E On B.SiteCode = E.SiteCode ");

            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Asset Code",
                        "Asset Name",
                        "Display Name",
                        "Item's Code",
                        "Item's Name",
                       

                        //6-9
                        "Foreign Name",
                        "Site",
                        "Cost Center Code",
                        "Cost Center Name",
                        
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 200, 200, 100, 150, 
                        
                        //6-9
                        100, 200, 100, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9 }, false);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 8 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6, 8}, false);
            }

            Sm.GrdColInvisible(Grd1, new int[] { 7 }, mIsAssetShowAdditionalInformation);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "B.AssetName", "B.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "AssetCode",   

                            //1-5
                            "AssetName", "DisplayName", "ItCode", "ItName", "ForeignName",  
                            //6-8
                            "SiteName", "CCCode", "CCName"             
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "CostCenter ");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        
    }
}
