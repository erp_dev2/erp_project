﻿#region Update
/* 
09/02/2018 [HAR] inputan di grid diganti kayak remark 
27/03/2018 [HAR] jumlah tab jadi 20, position dihapus ganti combo name  ambil dri master competence,  competency detail default 1 double klik untuk all, bug Level 
               competence detail yang tampil adalah competence yang yang name nya sesuai di pilih 
25/02/2020 [HAR/SIER] tambah length untuk assesment name
11/03/2020 [DITA/SIER] fitur import data
15/07/2022 [RIS/PRODUCT] Menambah checkbox ActInd
 * */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using FastReport;
using FastReport.Data;
using DevExpress.XtraEditors.Repository;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmAssesment2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mSectionNo = string.Empty;
        internal FrmAssesment2Find FrmFind;
        internal int BtnCode = 0,   Row = 0;
        internal string mAsmCode = "";
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmAssesment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Assesment";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);

                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetLueCompetenceLevelType(ref LueCompetenceLevelType);
                SetLueCompetenceLevelName(ref LueCompLevelName);


                SetTabControls("2", ref LueComp2);
                SetTabControls("3", ref LueComp3);
                SetTabControls("4", ref LueComp4);
                SetTabControls("5", ref LueComp5);
                SetTabControls("6", ref LueComp6);
                SetTabControls("7", ref LueComp7);
                SetTabControls("8", ref LueComp8);
                SetTabControls("9", ref LueComp9);
                SetTabControls("10", ref LueComp10);
                SetTabControls("11", ref LueComp11);
                SetTabControls("12", ref LueComp12);
                SetTabControls("13", ref LueComp13);
                SetTabControls("14", ref LueComp14);
                SetTabControls("15", ref LueComp15);
                SetTabControls("16", ref LueComp16);
                SetTabControls("17", ref LueComp17);
                SetTabControls("18", ref LueComp18);
                SetTabControls("19", ref LueComp19);
                SetTabControls("20", ref LueComp20);
                SetTabControls("1", ref LueComp1);

                MeeCompGrd1.Visible = MeeCompGrd2.Visible = MeeCompGrd3.Visible = MeeCompGrd4.Visible = MeeCompGrd5.Visible = 
                MeeCompGrd6.Visible = MeeCompGrd7.Visible = MeeCompGrd8.Visible = MeeCompGrd9.Visible = MeeCompGrd10.Visible = 
                MeeCompGrd11.Visible = MeeCompGrd12.Visible = MeeCompGrd13.Visible = MeeCompGrd14.Visible = MeeCompGrd15.Visible =
                MeeCompGrd16.Visible = MeeCompGrd17.Visible = MeeCompGrd18.Visible = MeeCompGrd19.Visible = MeeCompGrd20.Visible = 
                false;

                MeeIndGrd1.Visible = MeeIndGrd2.Visible = MeeIndGrd3.Visible = MeeIndGrd4.Visible = MeeIndGrd5.Visible =
                MeeIndGrd6.Visible = MeeIndGrd7.Visible = MeeIndGrd8.Visible = MeeIndGrd9.Visible = MeeIndGrd10.Visible =
                MeeIndGrd11.Visible = MeeIndGrd12.Visible = MeeIndGrd13.Visible = MeeIndGrd14.Visible = MeeIndGrd15.Visible =
                MeeIndGrd16.Visible = MeeIndGrd17.Visible = MeeIndGrd18.Visible = MeeIndGrd19.Visible = MeeIndGrd20.Visible =
                false;

                MeeEviGrd1.Visible = MeeEviGrd2.Visible = MeeEviGrd3.Visible = MeeEviGrd4.Visible = MeeEviGrd5.Visible =
                MeeEviGrd6.Visible = MeeEviGrd7.Visible = MeeEviGrd8.Visible = MeeEviGrd9.Visible = MeeEviGrd10.Visible =
                MeeEviGrd11.Visible = MeeEviGrd12.Visible = MeeEviGrd13.Visible = MeeEviGrd14.Visible = MeeEviGrd15.Visible =
                MeeEviGrd16.Visible = MeeEviGrd17.Visible = MeeEviGrd18.Visible = MeeEviGrd19.Visible = MeeEviGrd20.Visible =
                false;

                MeeRemarkGrd1.Visible = MeeRemarkGrd2.Visible = MeeRemarkGrd3.Visible = MeeRemarkGrd4.Visible = MeeRemarkGrd5.Visible =
                MeeRemarkGrd6.Visible = MeeRemarkGrd7.Visible = MeeRemarkGrd8.Visible = MeeRemarkGrd9.Visible = MeeRemarkGrd10.Visible =
                MeeRemarkGrd11.Visible = MeeRemarkGrd12.Visible = MeeRemarkGrd13.Visible = MeeRemarkGrd14.Visible = MeeRemarkGrd15.Visible =
                MeeRemarkGrd16.Visible = MeeRemarkGrd17.Visible = MeeRemarkGrd18.Visible = MeeRemarkGrd19.Visible = MeeRemarkGrd20.Visible =
                false;
                
                SetFormControl(mState.View);

                if (mAsmCode.Length != 0)
                {
                    this.Text = "Assesment";
                    ShowData(mAsmCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetTabControls(string TabPage, ref DXE.LookUpEdit LueComp)
        {
            tabControl1.SelectTab("tabPage" + TabPage);
            SetLueCompetenceLevelforDetail(ref LueComp, Sm.GetLue(LueCompetenceLevelType), "0");
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAsmCode, TxtAsmName, DteDocDt, ChkActInd,  
                        LueComp1, LueComp2, LueComp3, LueComp4, LueComp5, 
                        LueComp6, LueComp7, LueComp8, LueComp9, LueComp10,
                        LueComp11, LueComp12, LueComp13, LueComp14, LueComp15, 
                        LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                        MeeRemark, LueCompLevelName, LueCompetenceLevelType,
                        LueLevel_1, LueLevel_2, LueLevel_3, LueLevel_4, LueLevel_5, 
                        LueLevel_6, LueLevel_7, LueLevel_8, LueLevel_9, LueLevel_10, 
                        LueLevel_11, LueLevel_12, LueLevel_13, LueLevel_14, LueLevel_15, 
                        LueLevel_16, LueLevel_17, LueLevel_18, LueLevel_19, LueLevel_20,
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd9, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd10, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 2, 3 });
                    BtnImport.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtAsmCode, TxtAsmName, ChkActInd,
                        LueComp1, LueComp2, LueComp3, LueComp4, LueComp5, 
                        LueComp6, LueComp7, LueComp8, LueComp9, LueComp10,
                        LueComp11, LueComp12, LueComp13, LueComp14, LueComp15, 
                        LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                        MeeRemark, LueCompLevelName, LueCompetenceLevelType,
                        LueLevel_1, LueLevel_2, LueLevel_3, LueLevel_4, LueLevel_5, 
                        LueLevel_6, LueLevel_7, LueLevel_8, LueLevel_9, LueLevel_10, 
                        LueLevel_11, LueLevel_12, LueLevel_13, LueLevel_14, LueLevel_15,
                        LueLevel_16, LueLevel_17, LueLevel_18, LueLevel_19, LueLevel_20, 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd8, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd9, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd10, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3 });
                    BtnImport.Enabled = true;
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAsmName, MeeRemark, ChkActInd,
                        LueComp1, LueComp2, LueComp3, LueComp4, LueComp5, 
                        LueComp6, LueComp7, LueComp8, LueComp9, LueComp10,
                        LueComp11, LueComp12, LueComp13, LueComp14, LueComp15,
                        LueLevel_1, LueLevel_2, LueLevel_3, LueLevel_4, LueLevel_5, 
                        LueLevel_6, LueLevel_7, LueLevel_8, LueLevel_9, LueLevel_10, 
                        LueLevel_11, LueLevel_12, LueLevel_13, LueLevel_14, LueLevel_15,
                        LueLevel_16, LueLevel_17, LueLevel_18, LueLevel_19, LueLevel_20, 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd8, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd9, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd10, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3 });
                    BtnImport.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void SetGrd()
        {
            SetGrdCriteria(new List<iGrid> 
                { 
                    Grd1, Grd2, Grd3, Grd4, Grd5,
                    Grd6, Grd7, Grd8, Grd9, Grd10,
                    Grd11, Grd12, Grd13, Grd14, Grd15,
                    Grd16, Grd17, Grd18, Grd19, Grd20,
                });

        }

        private void SetGrdCriteria(List<iGrid> ListofGrd)
        {
            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 4;
                Grd.ReadOnly = false;
                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[] 
                        {
                            //0
                            "Competence Detail",
                            //1-3
                            "Indicator Behaviour",
                            "Evidence Behaviour",
                            "Remark"
                        },
                        new int[] 
                        { 
                            250, 
                            250, 250, 250
                        }
                    );
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssesment2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                ChkActInd.Checked = true;
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                BtnCode = 0;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAsmCode, "", false)) return;
            SetFormControl(mState.Edit);
            BtnCode = 1;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                 InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAsmCode, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            PrintData(TxtAsmCode.Text);
        }

        private void PrintData(string DocNo)
        {           

        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAsmCode, TxtAsmName, DteDocDt, MeeRemark,LueCompLevelName, LueCompetenceLevelType,
                LueComp1, LueComp2, LueComp3, LueComp4, LueComp5,
                LueComp6, LueComp7, LueComp8, LueComp9, LueComp10,
                LueComp11, LueComp12, LueComp13, LueComp14, LueComp15,
                LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                LueLevel_1, LueLevel_2, LueLevel_3, LueLevel_4, LueLevel_5, 
                LueLevel_6, LueLevel_7, LueLevel_8, LueLevel_9, LueLevel_10, 
                LueLevel_11, LueLevel_12, LueLevel_13, LueLevel_14, LueLevel_15,
                LueLevel_16, LueLevel_17, LueLevel_18, LueLevel_19, LueLevel_20,
            });

            ClearGrd(new List<iGrid>(){
                Grd1, Grd2, Grd3, Grd4, Grd5,
                Grd6, Grd7, Grd8, Grd9, Grd10,
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
            });

            ChkActInd.Checked = false;
        }


        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueComp1, LueComp2, LueComp3, LueComp4, LueComp5,
                LueComp6, LueComp7, LueComp8, LueComp9, LueComp10,
                LueComp11, LueComp12, LueComp13, LueComp14, LueComp15,
                LueComp16, LueComp17, LueComp18, LueComp19, LueComp20,
                LueLevel_1, LueLevel_2, LueLevel_3, LueLevel_4, LueLevel_5, 
                LueLevel_6, LueLevel_7, LueLevel_8, LueLevel_9, LueLevel_10, 
                LueLevel_11, LueLevel_12, LueLevel_13, LueLevel_14, LueLevel_15,
                LueLevel_16, LueLevel_17, LueLevel_18, LueLevel_19, LueLevel_20,
            });

            ClearGrd(new List<iGrid>(){
                Grd1, Grd2, Grd3, Grd4, Grd5,
                Grd6, Grd7, Grd8, Grd9, Grd10,
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
            });
        }

      

        private void ClearGrd(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.FocusGrd(Grd, 0, 0);
            });
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string AsmCode = TxtAsmCode.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAssesmentHdr(AsmCode));

            int DNo = 0;

            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "1", Grd1);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "2", Grd2);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "3", Grd3);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "4", Grd4);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "5", Grd5);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "6", Grd6);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "7", Grd7);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "8", Grd8);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "9", Grd9);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "10", Grd10);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "11", Grd11);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "12", Grd12);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "13", Grd13);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "14", Grd14);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "15", Grd15);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "16", Grd16);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "17", Grd17);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "18", Grd18);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "19", Grd19);
            SaveAssesmentDtlX(ref cml, AsmCode, ref DNo, "20", Grd20);

            Sm.ExecCommands(cml);

            ShowData(AsmCode);
        }

        private void SaveAssesmentDtlX(ref List<MySqlCommand> cml, string AsmCode, ref int DNo, string SectionNo, iGrid Grd)
        {
            for (int Row = 0; Row < Grd.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd, Row, 0).Length > 0)
                    cml.Add(SaveAssesmentDtl(AsmCode, ref DNo, SectionNo, Grd, Row));
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAsmCode, "Assesment Code", false)||
                Sm.IsTxtEmpty(TxtAsmName, "Assesment Name", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords()||
                IsAsmCodeExisted() ||
                IsAssessmentProcess()
                ;
        }

        private bool IsAsmCodeExisted()
        {
            if (BtnCode == 0 &&
                Sm.IsDataExist("Select AsmCode From TblAssesmentHdr Where AsmCode='" + TxtAsmCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Assesment Code ( " + TxtAsmCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsAssessmentProcess()
        {
            if (BtnCode == 1 &&
                Sm.IsDataExist("Select AsmCode From TblAssesmentProcessHdr Where AsmCode='" + TxtAsmCode.Text + "' And CancelInd = 'N' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is used on Assessment Process");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 competence.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (IsGrdExceedMaxRecords(Grd1, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd2, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd3, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd4, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd5, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd6, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd7, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd8, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd9, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd10, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd11, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd12, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd13, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd14, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd15, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd16, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd17, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd18, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd19, "Competence Detail") ||
                IsGrdExceedMaxRecords(Grd20, "Competence Detail") 
                ) return true;

            return false;
        }

        private bool IsGrdExceedMaxRecords(iGrid Grd, string Msg)
        {
            if (Grd.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning,
                    Msg + " data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveAssesmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssesmentHdr ");
            SQL.AppendLine("(AsmCode, AsmName, DocDt, ActInd, CompetenceLevel, CompetencelevelType, ");
            SQL.AppendLine("CompetenceCode1, CompetenceCode2, CompetenceCode3, CompetenceCode4, CompetenceCode5, ");
            SQL.AppendLine("CompetenceCode6, CompetenceCode7, CompetenceCode8, CompetenceCode9, CompetenceCode10, ");
            SQL.AppendLine("CompetenceCode11, CompetenceCode12, CompetenceCode13, CompetenceCode14, CompetenceCode15, ");
            SQL.AppendLine("CompetenceCode16, CompetenceCode17, CompetenceCode18, CompetenceCode19, CompetenceCode20, ");
            SQL.AppendLine("Level1, Level2, Level3, Level4, Level5, ");
            SQL.AppendLine("Level6, Level7, Level8, Level9, Level10, ");
            SQL.AppendLine("Level11, Level12, Level13, Level14, Level15, ");
            SQL.AppendLine("Level16, Level17, Level18, Level19, Level20, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AsmCode, @AsmName, @DocDt, @ActInd, @CompetenceLevel, @CompetencelevelType, ");
            SQL.AppendLine("@CompetenceCode1, @CompetenceCode2, @CompetenceCode3, @CompetenceCode4, @CompetenceCode5, ");
            SQL.AppendLine("@CompetenceCode6, @CompetenceCode7, @CompetenceCode8, @CompetenceCode9, @CompetenceCode10, ");
            SQL.AppendLine("@CompetenceCode11, @CompetenceCode12, @CompetenceCode13, @CompetenceCode14, @CompetenceCode15,");
            SQL.AppendLine("@CompetenceCode16, @CompetenceCode17, @CompetenceCode18, @CompetenceCode19, @CompetenceCode20, ");
            SQL.AppendLine("@Level1, @Level2, @Level3, @Level4, @Level5, ");
            SQL.AppendLine("@Level6, @Level7, @Level8, @Level9, @Level10, ");
            SQL.AppendLine("@Level11, @Level12, @Level13, @Level14, @Level15, ");
            SQL.AppendLine("@Level16, @Level17, @Level18, @Level19, @Level20, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update AsmName=@AsmName, ActInd=@ActInd, ");
            SQL.AppendLine("    CompetenceCode1=@CompetenceCode1, CompetenceCode2=@CompetenceCode2, CompetenceCode3=@CompetenceCode3, CompetenceCode4=@CompetenceCode4, CompetenceCode5=@CompetenceCode5, ");
            SQL.AppendLine("    CompetenceCode6=@CompetenceCode6, CompetenceCode7=@CompetenceCode7, CompetenceCode8=@CompetenceCode8, CompetenceCode9=@CompetenceCode9, CompetenceCode10=@CompetenceCode10, ");
            SQL.AppendLine("    CompetenceCode11=@CompetenceCode11, CompetenceCode12=@CompetenceCode12, CompetenceCode13=@CompetenceCode13, CompetenceCode14=@CompetenceCode14, CompetenceCode15=@CompetenceCode15, ");
            SQL.AppendLine("    CompetenceCode16=@CompetenceCode16, CompetenceCode17=@CompetenceCode17, CompetenceCode18=@CompetenceCode18, CompetenceCode19=@CompetenceCode19, CompetenceCode20=@CompetenceCode20, ");
            SQL.AppendLine("    Level1=@Level1, Level2=@Level2, Level3=@Level3, Level4=@Level4, Level5=@Level5, ");
            SQL.AppendLine("    Level6=@Level6, Level7=@Level7, Level8=@Level8, Level9=@Level9, Level10=@Level10, ");
            SQL.AppendLine("    Level11=@Level11, Level12=@Level12, Level13=@Level13, Level14=@Level14, Level15=@Level15, ");
            SQL.AppendLine("    Level16=@Level16, Level17=@Level17, Level18=@Level18, Level19=@Level19, Level20=@Level20, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            
            SQL.AppendLine("Delete From TblAssesmentDtl Where AsmCode=@AsmCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AsmCode", TxtAsmCode.Text);
            Sm.CmParam<String>(ref cm, "@AsmName", TxtAsmName.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CompetenceLevel", Sm.GetLue(LueCompLevelName));
            Sm.CmParam<String>(ref cm, "@CompetenceLevelType", Sm.GetLue(LueCompetenceLevelType));
            Sm.CmParam<String>(ref cm, "@CompetenceCode1", Sm.GetLue(LueComp1));
            Sm.CmParam<String>(ref cm, "@CompetenceCode2", Sm.GetLue(LueComp2));
            Sm.CmParam<String>(ref cm, "@CompetenceCode3", Sm.GetLue(LueComp3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode4", Sm.GetLue(LueComp4));
            Sm.CmParam<String>(ref cm, "@CompetenceCode5", Sm.GetLue(LueComp5));
            Sm.CmParam<String>(ref cm, "@CompetenceCode6", Sm.GetLue(LueComp6));
            Sm.CmParam<String>(ref cm, "@CompetenceCode7", Sm.GetLue(LueComp7));
            Sm.CmParam<String>(ref cm, "@CompetenceCode8", Sm.GetLue(LueComp8));
            Sm.CmParam<String>(ref cm, "@CompetenceCode9", Sm.GetLue(LueComp9));
            Sm.CmParam<String>(ref cm, "@CompetenceCode10", Sm.GetLue(LueComp10));
            Sm.CmParam<String>(ref cm, "@CompetenceCode11", Sm.GetLue(LueComp11));
            Sm.CmParam<String>(ref cm, "@CompetenceCode12", Sm.GetLue(LueComp12));
            Sm.CmParam<String>(ref cm, "@CompetenceCode13", Sm.GetLue(LueComp13));
            Sm.CmParam<String>(ref cm, "@CompetenceCode14", Sm.GetLue(LueComp14));
            Sm.CmParam<String>(ref cm, "@CompetenceCode15", Sm.GetLue(LueComp15));
            Sm.CmParam<String>(ref cm, "@CompetenceCode16", Sm.GetLue(LueComp16));
            Sm.CmParam<String>(ref cm, "@CompetenceCode17", Sm.GetLue(LueComp17));
            Sm.CmParam<String>(ref cm, "@CompetenceCode18", Sm.GetLue(LueComp18));
            Sm.CmParam<String>(ref cm, "@CompetenceCode19", Sm.GetLue(LueComp19));
            Sm.CmParam<String>(ref cm, "@CompetenceCode20", Sm.GetLue(LueComp20));
            Sm.CmParam<String>(ref cm, "@Level1", Sm.GetLue(LueLevel_1));
            Sm.CmParam<String>(ref cm, "@Level2", Sm.GetLue(LueLevel_2));
            Sm.CmParam<String>(ref cm, "@Level3", Sm.GetLue(LueLevel_3));
            Sm.CmParam<String>(ref cm, "@Level4", Sm.GetLue(LueLevel_4));
            Sm.CmParam<String>(ref cm, "@Level5", Sm.GetLue(LueLevel_5));
            Sm.CmParam<String>(ref cm, "@Level6", Sm.GetLue(LueLevel_6));
            Sm.CmParam<String>(ref cm, "@Level7", Sm.GetLue(LueLevel_7));
            Sm.CmParam<String>(ref cm, "@Level8", Sm.GetLue(LueLevel_8));
            Sm.CmParam<String>(ref cm, "@Level9", Sm.GetLue(LueLevel_9));
            Sm.CmParam<String>(ref cm, "@Level10", Sm.GetLue(LueLevel_10));
            Sm.CmParam<String>(ref cm, "@Level11", Sm.GetLue(LueLevel_11));
            Sm.CmParam<String>(ref cm, "@Level12", Sm.GetLue(LueLevel_12));
            Sm.CmParam<String>(ref cm, "@Level13", Sm.GetLue(LueLevel_13));
            Sm.CmParam<String>(ref cm, "@Level14", Sm.GetLue(LueLevel_14));
            Sm.CmParam<String>(ref cm, "@Level15", Sm.GetLue(LueLevel_15));
            Sm.CmParam<String>(ref cm, "@Level16", Sm.GetLue(LueLevel_16));
            Sm.CmParam<String>(ref cm, "@Level17", Sm.GetLue(LueLevel_17));
            Sm.CmParam<String>(ref cm, "@Level18", Sm.GetLue(LueLevel_18));
            Sm.CmParam<String>(ref cm, "@Level19", Sm.GetLue(LueLevel_19));
            Sm.CmParam<String>(ref cm, "@Level20", Sm.GetLue(LueLevel_20));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAssesmentDtl(string DocNo, ref int DNo, string SectionNo, iGrid Grd, int Row)
        {
            var SQL = new StringBuilder();

            DNo += 1;

            SQL.AppendLine("Insert Into TblAssesmentDtl ");
            SQL.AppendLine("(AsmCode, DNo, SectionNo, Description, Indicator, Evidence, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AsmCode, @DNo, @SectionNo, @Description, @Indicator, @Evidence, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AsmCode", TxtAsmCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@Indicator", Sm.GetGrdStr(Grd, Row, 1));
            Sm.CmParam<String>(ref cm, "@Evidence", Sm.GetGrdStr(Grd, Row, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data
        public void ShowData(string AsmCode)
        {
            try
            {
                ClearData();
                ShowAssesmentHdr(AsmCode);
                ShowAssesmentDtl(AsmCode, "1", ref Grd1);
                ShowAssesmentDtl(AsmCode, "2", ref Grd2);
                ShowAssesmentDtl(AsmCode, "3", ref Grd3);
                ShowAssesmentDtl(AsmCode, "4", ref Grd4);
                ShowAssesmentDtl(AsmCode, "5", ref Grd5);
                ShowAssesmentDtl(AsmCode, "6", ref Grd6);
                ShowAssesmentDtl(AsmCode, "7", ref Grd7);
                ShowAssesmentDtl(AsmCode, "8", ref Grd8);
                ShowAssesmentDtl(AsmCode, "9", ref Grd9);
                ShowAssesmentDtl(AsmCode, "10", ref Grd10);
                ShowAssesmentDtl(AsmCode, "11", ref Grd11);
                ShowAssesmentDtl(AsmCode, "12", ref Grd12);
                ShowAssesmentDtl(AsmCode, "13", ref Grd13);
                ShowAssesmentDtl(AsmCode, "14", ref Grd14);
                ShowAssesmentDtl(AsmCode, "15", ref Grd15);
                ShowAssesmentDtl(AsmCode, "16", ref Grd16);
                ShowAssesmentDtl(AsmCode, "17", ref Grd17);
                ShowAssesmentDtl(AsmCode, "18", ref Grd18);
                ShowAssesmentDtl(AsmCode, "19", ref Grd19);
                ShowAssesmentDtl(AsmCode, "20", ref Grd20);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssesmentHdr(string AsmCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AsmCode, AsmName, DocDt, ActInd, CompetenceLevel, CompetencelevelType, ");
            SQL.AppendLine("CompetenceCode1, CompetenceCode2, CompetenceCode3, CompetenceCode4, CompetenceCode5, ");
            SQL.AppendLine("CompetenceCode6, CompetenceCode7, CompetenceCode8, CompetenceCode9, CompetenceCode10, ");
            SQL.AppendLine("CompetenceCode11, CompetenceCode12, CompetenceCode13, CompetenceCode14, CompetenceCode15, ");
            SQL.AppendLine("CompetenceCode16, CompetenceCode17, CompetenceCode18, CompetenceCode19, CompetenceCode20, ");
            SQL.AppendLine("Level1, Level2, Level3, Level4, Level5, ");
            SQL.AppendLine("Level6, Level7, Level8, Level9, Level10, ");
            SQL.AppendLine("Level11, Level12, Level13, Level14, Level15, ");
            SQL.AppendLine("Level16, Level17, Level18, Level19, Level20, ");
            SQL.AppendLine("Remark ");
            SQL.AppendLine("From TblAssesmentHdr ");
            SQL.AppendLine("Where AsmCode=@AsmCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AsmCode", AsmCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "AsmCode", 
                        //1-5
                        "AsmName", "DocDt", "CompetenceLevel", "CompetencelevelType", "CompetenceCode1", 
                        //6-10
                        "CompetenceCode2", "CompetenceCode3", "CompetenceCode4", "CompetenceCode5", "CompetenceCode6", 
                        //11-15
                        "CompetenceCode7", "CompetenceCode8", "CompetenceCode9", "CompetenceCode10", "CompetenceCode11",  
                        //16-20
                        "CompetenceCode12", "CompetenceCode13", "CompetenceCode14", "CompetenceCode15",  "CompetenceCode16",   
                        //21-25
                        "CompetenceCode17", "CompetenceCode18", "CompetenceCode19", "CompetenceCode20", "Level1",   
                        //26-30
                        "Level2", "Level3", "Level4", "Level5", "Level6", 
                        //31-35
                        "Level7",  "Level8", "Level9", "Level10", "Level11",  
                        //36-40
                        "Level12", "Level13", "Level14", "Level15", "Level16",  
                        //41-45
                        "Level17", "Level18", "Level19", "Level20", "Remark",
                        //46 
                        "ActInd",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAsmCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAsmName.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        SetLueCompetenceLevelName(ref LueCompLevelName);
                        Sm.SetLue(LueCompLevelName, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCompetenceLevelType, Sm.DrStr(dr, c[4]));
                        SetLueCompetenceLevelforDetail(ref LueComp1, Sm.DrStr(dr, c[5]), "1");
                        Sm.SetLue(LueComp1, Sm.DrStr(dr, c[5]));
                        SetLueCompetenceLevelforDetail(ref LueComp2, Sm.DrStr(dr, c[6]), "1");
                        Sm.SetLue(LueComp2, Sm.DrStr(dr, c[6]));
                        SetLueCompetenceLevelforDetail(ref LueComp3, Sm.DrStr(dr, c[7]), "1");
                        Sm.SetLue(LueComp3, Sm.DrStr(dr, c[7]));
                        SetLueCompetenceLevelforDetail(ref LueComp4, Sm.DrStr(dr, c[8]), "1");
                        Sm.SetLue(LueComp4, Sm.DrStr(dr, c[8]));
                        SetLueCompetenceLevelforDetail(ref LueComp5, Sm.DrStr(dr, c[9]), "1");
                        Sm.SetLue(LueComp5, Sm.DrStr(dr, c[9]));
                        SetLueCompetenceLevelforDetail(ref LueComp6, Sm.DrStr(dr, c[10]), "1");
                        Sm.SetLue(LueComp6, Sm.DrStr(dr, c[10]));
                        SetLueCompetenceLevelforDetail(ref LueComp7, Sm.DrStr(dr, c[11]), "1");
                        Sm.SetLue(LueComp7, Sm.DrStr(dr, c[11]));
                        SetLueCompetenceLevelforDetail(ref LueComp8, Sm.DrStr(dr, c[12]), "1");
                        Sm.SetLue(LueComp8, Sm.DrStr(dr, c[12]));
                        SetLueCompetenceLevelforDetail(ref LueComp9, Sm.DrStr(dr, c[13]), "1");
                        Sm.SetLue(LueComp9, Sm.DrStr(dr, c[13]));
                        SetLueCompetenceLevelforDetail(ref LueComp10, Sm.DrStr(dr, c[14]), "1");
                        Sm.SetLue(LueComp10, Sm.DrStr(dr, c[14]));
                        SetLueCompetenceLevelforDetail(ref LueComp11, Sm.DrStr(dr, c[15]), "1");
                        Sm.SetLue(LueComp11, Sm.DrStr(dr, c[15]));
                        SetLueCompetenceLevelforDetail(ref LueComp12, Sm.DrStr(dr, c[16]), "1");
                        Sm.SetLue(LueComp12, Sm.DrStr(dr, c[16]));
                        SetLueCompetenceLevelforDetail(ref LueComp13, Sm.DrStr(dr, c[17]), "1");
                        Sm.SetLue(LueComp13, Sm.DrStr(dr, c[17]));
                        SetLueCompetenceLevelforDetail(ref LueComp14, Sm.DrStr(dr, c[18]), "1");
                        Sm.SetLue(LueComp14, Sm.DrStr(dr, c[18]));
                        SetLueCompetenceLevelforDetail(ref LueComp15, Sm.DrStr(dr, c[19]), "1");
                        Sm.SetLue(LueComp15, Sm.DrStr(dr, c[19]));
                        SetLueCompetenceLevelforDetail(ref LueComp16, Sm.DrStr(dr, c[20]), "1");
                        Sm.SetLue(LueComp16, Sm.DrStr(dr, c[20]));
                        SetLueCompetenceLevelforDetail(ref LueComp17, Sm.DrStr(dr, c[21]), "1");
                        Sm.SetLue(LueComp17, Sm.DrStr(dr, c[21]));
                        SetLueCompetenceLevelforDetail(ref LueComp18, Sm.DrStr(dr, c[22]), "1");
                        Sm.SetLue(LueComp18, Sm.DrStr(dr, c[22]));
                        SetLueCompetenceLevelforDetail(ref LueComp19, Sm.DrStr(dr, c[23]), "1");
                        Sm.SetLue(LueComp19, Sm.DrStr(dr, c[23]));
                        SetLueCompetenceLevelforDetail(ref LueComp20, Sm.DrStr(dr, c[24]), "1");
                        Sm.SetLue(LueComp20, Sm.DrStr(dr, c[24]));
                        Sm.SetLue(LueLevel_1, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueLevel_2, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueLevel_3, Sm.DrStr(dr, c[27]));
                        Sm.SetLue(LueLevel_4, Sm.DrStr(dr, c[28]));
                        Sm.SetLue(LueLevel_5, Sm.DrStr(dr, c[29]));
                        Sm.SetLue(LueLevel_6, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueLevel_7, Sm.DrStr(dr, c[31]));
                        Sm.SetLue(LueLevel_8, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueLevel_9, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueLevel_10, Sm.DrStr(dr, c[34]));
                        Sm.SetLue(LueLevel_11, Sm.DrStr(dr, c[35]));
                        Sm.SetLue(LueLevel_12, Sm.DrStr(dr, c[36]));
                        Sm.SetLue(LueLevel_13, Sm.DrStr(dr, c[37]));
                        Sm.SetLue(LueLevel_14, Sm.DrStr(dr, c[38]));
                        Sm.SetLue(LueLevel_15, Sm.DrStr(dr, c[39]));
                        Sm.SetLue(LueLevel_16, Sm.DrStr(dr, c[40]));
                        Sm.SetLue(LueLevel_17, Sm.DrStr(dr, c[41]));
                        Sm.SetLue(LueLevel_18, Sm.DrStr(dr, c[42]));
                        Sm.SetLue(LueLevel_19, Sm.DrStr(dr, c[43]));
                        Sm.SetLue(LueLevel_20, Sm.DrStr(dr, c[44]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[45]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[46]), "Y");
                    }, true
                );
        }

        private void ShowAssesmentDtl(string AsmCode, string SectionNo, ref iGrid GrdXX)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AsmCode", AsmCode);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Description, Indicator, Evidence, Remark ");
            SQL.AppendLine("From TblAssesmentDtl ");
            SQL.AppendLine("Where AsmCode=@AsmCode ");
            SQL.AppendLine("And SectionNo=@SectionNo ");
            SQL.AppendLine("Order By DNo ; ");

            Sm.ShowDataInGrid(
                ref GrdXX, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Description", 
                    "Indicator", "Evidence", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(GrdXX, 0, 0);
        }

        private void ShowDataBehaviourDtl(LookUpEdit LueCompetenceCode, LookUpEdit LueCompetenceLevel, iGrid GrdXX)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetLue(LueCompetenceCode));
            Sm.CmParam<String>(ref cm, "@CompetenceLevel", Sm.GetLue(LueCompetenceLevel));

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Indicator, B.Evidence, B.Remark ");
            SQL.AppendLine("From tblBehaviourIndHdr A ");
            SQL.AppendLine("Inner Join TblbehaviourIndDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.ActiveInd = 'Y' And A.CompetenceCode=@CompetenceCode And A.CompetenceLevel = @CompetenceLevel ");
            SQL.AppendLine("Order By B.DNo ; ");

            Sm.ShowDataInGrid(
                ref GrdXX, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Indicator",  
                    //1
                    "Evidence", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    GrdXX.Cells[Row, 0].Value = "1";
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(GrdXX, 0, 0);
        }


        #endregion

        #region Additional Method

        private void SetLueCompetenceLevelName(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct A.CompetenceLevel As Col1, B.Levelname As Col2  ");
            SQL.AppendLine("From TblCompetence A ");
            SQL.AppendLine("Inner Join TblLevelHdr B On A.Competencelevel = B.LevelCode ");
            if (BtnSave.Enabled)
            {
                SQL.AppendLine("Where ActiveInd = 'Y'");
            }
            SQL.AppendLine("Order By Col2 ");

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCompetenceLevelforDetail(ref LookUpEdit Lue, string CompetenceType, string type)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select CompetenceCode As Col1, CompetenceName As Col2 From TblCompetence  ");
            if (type == "0")
            {
                SQL.AppendLine("Where CompetenceLevelType = '" + CompetenceType + "' ");
                SQL.AppendLine("And ActiveInd = 'Y'");
            }
            else
            {
                SQL.AppendLine("Where CompetenceCode = '" + CompetenceType + "' ");
            }
            SQL.AppendLine("Order By Col2 ");

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueCompetenceLevelType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'CompetenceLevelType' Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }


        private void MeeRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.MemoExEdit Mee,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Mee.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Mee.EditValue = null;
            else
                Mee.EditValue = Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex);

            Mee.Visible = true;
            Mee.Focus();

            fAccept = true;
        }


        public void SetLueLevelCode(ref LookUpEdit Lue, string CompetenceCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (CompetenceCode.Length > 0)
            {
                int LevelFrom = Convert.ToInt32(Sm.GetValue("Select LevelFrom From TblCompetence Where CompetenceCode = '" + CompetenceCode + "' "));
                int LevelTo = Convert.ToInt32(Sm.GetValue("Select LevelTo From TblCompetence Where CompetenceCode = '" + CompetenceCode + "' "));

                for (int j = LevelFrom; j <= LevelTo; j++)
                {
                    SQL.AppendLine("Select Distinct '" + j + "' As Col1, '" + j + "' As Col2  From TblCompetence ");
                    if (j != LevelTo)
                    {
                        SQL.AppendLine("Union All ");
                    }
                }
            }
            else
            {
                SQL.AppendLine("Select '' As Col1, '' As Col2 ");
            }

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ProcessAssesment()
        {
            bool IsFirst = true;
            List<IList> myLists = new List<IList>();
            var l = new List<Assesment>();
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits[1].Length == 0 && splits[2].Length == 0 )
                    {
                        Sm.StdMsg(mMsgType.Warning, "Data is invalid.");
                        return;
                    }
                    else
                    {
                        if (IsFirst)
                            IsFirst = false;
                        else
                        {
                            var arr = splits.ToArray();
                            if (arr[0].Trim().Length > 0)
                            {
                                l.Add(new Assesment()
                                {
                                    SectionNo = splits[0].Trim(),
                                    Description = splits[1].Trim(),
                                    Indicator = splits[2].Trim(),
                                    Evidence = splits[3].Trim(),
                                    Remark = splits[4].Trim(),
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.NoData, "");
                                return;
                            }
                        }
                    }

                }
                l.OrderBy(o => o.SectionNo);
                if (l.Count > 0)
                {
                    foreach(var x in l.OrderBy(o => o.SectionNo))
                    {
                        if (x.SectionNo == "1")
                            ProcessAssesment2(x, Grd1);
                        else if (x.SectionNo == "2")
                            ProcessAssesment2(x, Grd2);
                        else if (x.SectionNo == "3")
                            ProcessAssesment2(x, Grd3);
                        else if (x.SectionNo == "4")
                            ProcessAssesment2(x, Grd4);
                        else if (x.SectionNo == "5")
                            ProcessAssesment2(x, Grd5);
                        else if (x.SectionNo == "6")
                            ProcessAssesment2(x, Grd6);
                        else if (x.SectionNo == "7")
                            ProcessAssesment2(x, Grd7);
                        else if (x.SectionNo == "8")
                            ProcessAssesment2(x, Grd8);
                        else if (x.SectionNo == "9")
                            ProcessAssesment2(x, Grd9);
                        else if (x.SectionNo == "10")
                            ProcessAssesment2(x, Grd10);
                        else if (x.SectionNo == "11")
                            ProcessAssesment2(x, Grd11);
                        else if (x.SectionNo == "12")
                            ProcessAssesment2(x, Grd12);
                        else if (x.SectionNo == "13")
                            ProcessAssesment2(x, Grd13);
                        else if (x.SectionNo == "14")
                            ProcessAssesment2(x, Grd14);
                        else if (x.SectionNo == "15")
                            ProcessAssesment2(x, Grd15);
                        else if (x.SectionNo == "16")
                            ProcessAssesment2(x, Grd16);
                        else if (x.SectionNo == "17")
                            ProcessAssesment2(x, Grd17);
                        else if (x.SectionNo == "18")
                            ProcessAssesment2(x, Grd18);
                        else if (x.SectionNo == "19")
                            ProcessAssesment2(x, Grd19);
                        else if (x.SectionNo == "20")
                            ProcessAssesment2(x, Grd20);
                        else
                        {
                            Sm.StdMsg(mMsgType.Warning, "Section No invalid.");
                            return;
                        }
                    }
                   
                    

                }
                l.Clear();
            }
        }

        private void ProcessAssesment2(Assesment l, iGrid Grd)
        {
            Grd.Rows.Add();
            Grd.Cells[Grd.Rows.Count - 2, 0].Value = l.Description;
            Grd.Cells[Grd.Rows.Count - 2, 1].Value = l.Indicator;
            Grd.Cells[Grd.Rows.Count - 2, 2].Value = l.Evidence;
            Grd.Cells[Grd.Rows.Count - 2, 3].Value = l.Remark;
        }


        #endregion

        #endregion

        #region Event

        #region Luecomp Edit value

        private void LueComp1_EditValueChanged(object sender, EventArgs e)
        {
            // Sm.RefreshLookUpEdit(LueComp1, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd1, true);
                Sm.RefreshLookUpEdit(LueComp1, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp1).Length > 0)
                    SetLueLevelCode(ref LueLevel_1, Sm.GetLue(LueComp1));
        }

        private void LueComp2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd2, true);
                //Sm.RefreshLookUpEdit(LueComp2, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp2, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp2).Length > 0)
                    SetLueLevelCode(ref LueLevel_2, Sm.GetLue(LueComp2));
        }

        private void LueComp3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd3, true);
                //Sm.RefreshLookUpEdit(LueComp3, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp3, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp3).Length > 0)
                    SetLueLevelCode(ref LueLevel_3, Sm.GetLue(LueComp3));
        }

        private void LueComp4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd4, true);
                //Sm.RefreshLookUpEdit(LueComp4, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp4, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp4).Length > 0)
                    SetLueLevelCode(ref LueLevel_4, Sm.GetLue(LueComp4));
        }

        private void LueComp5_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd5, true);
                //Sm.RefreshLookUpEdit(LueComp5, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp5, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp5).Length > 0)
                    SetLueLevelCode(ref LueLevel_5, Sm.GetLue(LueComp5));
        }

        private void LueComp6_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd6, true);
                //Sm.RefreshLookUpEdit(LueComp6, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp6, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
                if (Sm.GetLue(LueComp6).Length > 0)
                    SetLueLevelCode(ref LueLevel_6, Sm.GetLue(LueComp6));
        }

        private void LueComp7_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd7, true);
                //Sm.RefreshLookUpEdit(LueComp7, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp7, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp7).Length > 0)
                    SetLueLevelCode(ref LueLevel_7, Sm.GetLue(LueComp7));
        }

        private void LueComp8_EditValueChanged(object sender, EventArgs e)
        {
            {
                Sm.ClearGrd(Grd8, true);
                //Sm.RefreshLookUpEdit(LueComp8, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp8, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp8).Length > 0)
                    SetLueLevelCode(ref LueLevel_8, Sm.GetLue(LueComp8));
        }

        private void LueComp9_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd9, true);
                //Sm.RefreshLookUpEdit(LueComp9, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp9, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp9).Length > 0)
                    SetLueLevelCode(ref LueLevel_9, Sm.GetLue(LueComp9));
            
        }

        private void LueComp10_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd10, true);
                //Sm.RefreshLookUpEdit(LueComp10, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp10, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp10).Length > 0)
                    SetLueLevelCode(ref LueLevel_10, Sm.GetLue(LueComp10));
        }

        private void LueComp11_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd11, true);
                //Sm.RefreshLookUpEdit(LueComp11, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp11, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp11).Length > 0)
                    SetLueLevelCode(ref LueLevel_11, Sm.GetLue(LueComp11));
        }

        private void LueComp12_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd12, true);
                //Sm.RefreshLookUpEdit(LueComp12, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp12, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp12).Length > 0)
                    SetLueLevelCode(ref LueLevel_12, Sm.GetLue(LueComp12));
        }

        private void LueComp13_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd13, true);
                //Sm.RefreshLookUpEdit(LueComp13, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp13, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp13).Length > 0)
                    SetLueLevelCode(ref LueLevel_13, Sm.GetLue(LueComp13));
        }

        private void LueComp14_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd14, true);
                //Sm.RefreshLookUpEdit(LueComp14, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp14, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp14).Length > 0)
                    SetLueLevelCode(ref LueLevel_14, Sm.GetLue(LueComp14));
        }

        private void LueComp15_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd15, true);
                //Sm.RefreshLookUpEdit(LueComp15, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp15, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }
            if (Sm.GetLue(LueComp15).Length > 0)
                    SetLueLevelCode(ref LueLevel_15, Sm.GetLue(LueComp15));
        }

        private void LueComp16_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd16, true);
                //Sm.RefreshLookUpEdit(LueComp16, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp16, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }   
            if (Sm.GetLue(LueComp16).Length > 0)
                    SetLueLevelCode(ref LueLevel_16, Sm.GetLue(LueComp16));
        }

        private void LueComp17_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd17, true);
                //Sm.RefreshLookUpEdit(LueComp17, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp17, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }   
            if (Sm.GetLue(LueComp17).Length > 0)
                    SetLueLevelCode(ref LueLevel_17, Sm.GetLue(LueComp17));
        }

        private void LueComp18_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd18, true);
                //Sm.RefreshLookUpEdit(LueComp18, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp18, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }  if (Sm.GetLue(LueComp18).Length > 0)
                    SetLueLevelCode(ref LueLevel_18, Sm.GetLue(LueComp18));
        }

        private void LueComp19_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd19, true);
                //Sm.RefreshLookUpEdit(LueComp19, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp19, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }  if (Sm.GetLue(LueComp19).Length > 0)
                    SetLueLevelCode(ref LueLevel_19, Sm.GetLue(LueComp19));
        }

        private void LueComp20_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.ClearGrd(Grd20, true);
                //Sm.RefreshLookUpEdit(LueComp20, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
                Sm.RefreshLookUpEdit(LueComp20, new Sm.RefreshLue3(SetLueCompetenceLevelforDetail), Sm.GetLue(LueCompetenceLevelType), "0");
            }  
            if (Sm.GetLue(LueComp20).Length > 0)
                    SetLueLevelCode(ref LueLevel_20, Sm.GetLue(LueComp20));
        }


        #endregion

        #region level edit value changed

        private void LueLevel1_EditValueChanged(object sender, EventArgs e)
        {

            if (Sm.GetLue(LueComp1).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_1, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp1));
                if (Sm.GetLue(LueLevel_1).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp1, LueLevel_1, Grd1);
                }
            }
            else
            {
                Sm.RefreshLookUpEdit(LueLevel_1, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);
            }
        }

        private void LueLevel2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueComp2).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_2, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp2));
                if (Sm.GetLue(LueLevel_2).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp2, LueLevel_2, Grd2);
                }
            }
            else
            {
                Sm.RefreshLookUpEdit(LueLevel_2, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);
            }
        }

        private void LueLevel3_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueComp3).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_3, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp3));
                if (Sm.GetLue(LueLevel_3).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp3, LueLevel_3, Grd3);
                }
            }
            else
            {
                Sm.RefreshLookUpEdit(LueLevel_3, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);
            }
        }

        private void LueLevel4_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueComp4).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_4, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp4));
                if (Sm.GetLue(LueLevel_4).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp4, LueLevel_4, Grd4);
                }
            }
            else
            {
                Sm.RefreshLookUpEdit(LueLevel_4, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);
            }
        }

        private void LueLevel5_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueComp5).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_5, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp5));
                if (Sm.GetLue(LueLevel_5).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp5, LueLevel_5, Grd5);
                }
            }
            else
            {
                Sm.RefreshLookUpEdit(LueLevel_5, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);
            }
        }

        private void LueLevel6_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueComp6).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_6, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp6));
                if (Sm.GetLue(LueLevel_6).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp6, LueLevel_6, Grd6);
                }
            }
            else
            {
                Sm.RefreshLookUpEdit(LueLevel_6, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);
            }
        }

        private void LueLevel7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_7, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp7).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_7, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp7));
                if (Sm.GetLue(LueLevel_7).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp7, LueLevel_7, Grd7);
                }
            }
        }

        private void LueLevel8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_8, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp8).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_8, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp8));
                if (Sm.GetLue(LueLevel_8).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp8, LueLevel_8, Grd8);
                }
            }
        }

        private void LueLevel9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_9, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp9).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_9, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp9));
                if (Sm.GetLue(LueLevel_9).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp9, LueLevel_9, Grd9);
                }
            }
        }

        private void LueLevel10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_10, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp10).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_10, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp10));
                if (Sm.GetLue(LueLevel_10).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp10, LueLevel_10, Grd10);
                }
            }
        }

        private void LueLevel11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_11, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp11).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_11, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp11));
                if (Sm.GetLue(LueLevel_11).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp11, LueLevel_11, Grd11);
                }
            }
        }

        private void LueLevel12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_12, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp12).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_12, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp12));
                if (Sm.GetLue(LueLevel_12).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp12, LueLevel_12, Grd12);
                }
            }
        }

        private void LueLevel13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_13, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp13).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_13, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp13));
                if (Sm.GetLue(LueLevel_13).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp13, LueLevel_13, Grd13);
                }
            }
        }

        private void LueLevel14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_14, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp14).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_14, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp14));
                if (Sm.GetLue(LueLevel_14).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp14, LueLevel_14, Grd14);
                }
            }
        }

        private void LueLevel15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_15, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp15).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_15, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp15));
                if (Sm.GetLue(LueLevel_15).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp15, LueLevel_15, Grd15);
                }
            }
        }

        private void LueLevel16_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_16, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp16).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_16, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp16));
                if (Sm.GetLue(LueLevel_16).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp15, LueLevel_16, Grd16);
                }
            }
        }

        private void LueLevel17_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_17, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp17).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_17, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp17));
                if (Sm.GetLue(LueLevel_17).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp17, LueLevel_17, Grd17);
                }
            }
        }

        private void LueLevel18_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_18, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp18).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_18, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp18));
                if (Sm.GetLue(LueLevel_18).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp18, LueLevel_18, Grd18);
                }
            }
        }

        private void LueLevel19_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_19, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp19).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_19, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp19));
                if (Sm.GetLue(LueLevel_19).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp19, LueLevel_19, Grd19);
                }
            }
        }

        private void LueLevel20_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel_20, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (Sm.GetLue(LueComp20).Length > 0 && TxtAsmCode.Text.Length != 0)
            {
                Sm.RefreshLookUpEdit(LueLevel_20, new Sm.RefreshLue2(SetLueLevelCode), Sm.GetLue(LueComp20));
                if (Sm.GetLue(LueLevel_20).Length > 0)
                {
                    ShowDataBehaviourDtl(LueComp20, LueLevel_20, Grd20);
                }
            }
        }

        #endregion

        #region Mee Keydown
        private void MeeCompGrd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeIndGrd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeEviGrd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeRemarkGrd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeCompGrd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void MeeIndGrd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void MeeEviGrd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void MeeRemarkGrd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void MeeCompGrd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void MeeIndGrd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void MeeEviGrd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void MeeRemarkGrd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void MeeCompGrd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeIndGrd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeEviGrd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeRemarkGrd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeCompGrd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void MeeIndGrd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void MeeEviGrd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void MeeRemarkGrd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void MeeCompGrd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeIndGrd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeEviGrd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeRemarkGrd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeCompGrd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void MeeIndGrd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void MeeEviGrd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void MeeRemarkGrd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void MeeCompGrd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void MeeIndGrd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void MeeEviGrd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void MeeRemarkGrd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd8, ref fAccept, e);
        }

        private void MeeCompGrd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void MeeIndGrd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void MeeEviGrd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void MeeRemarkGrd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void MeeCompGrd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void MeeIndGrd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void MeeEviGrd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void MeeRemarkGrd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void MeeCompGrd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void MeeIndGrd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void MeeEviGrd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void MeeRemarkGrd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void MeeCompGrd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void MeeIndGrd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void MeeEviGrd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void MeeRemarkGrd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void MeeCompGrd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void MeeIndGrd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void MeeEviGrd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void MeeRemarkGrd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void MeeCompGrd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void MeeIndGrd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void MeeEviGrd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void MeeRemarkGrd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void MeeCompGrd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void MeeIndGrd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void MeeEviGrd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void MeeRemarkGrd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void MeeCompGrd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void MeeIndGrd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void MeeEviGrd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void MeeRemarkGrd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void MeeCompGrd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void MeeIndGrd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void MeeEviGrd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void MeeRemarkGrd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void MeeCompGrd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void MeeIndGrd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void MeeEviGrd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void MeeRemarkGrd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void MeeCompGrd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void MeeIndGrd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void MeeEviGrd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void MeeRemarkGrd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void MeeCompGrd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void MeeIndGrd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void MeeEviGrd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void MeeRemarkGrd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        #endregion

        #region Mee Leave
        private void MeeCompGrd1_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd1.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd1.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = MeeCompGrd1.Text;
                }
                MeeCompGrd1.Visible = false;
            }
        }

        private void MeeIndGrd1_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd1.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd1.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = MeeIndGrd1.Text;
                }
                MeeIndGrd1.Visible = false;
            }
        }

        private void MeeEviGrd1_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd1.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd1.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = MeeEviGrd1.Text;
                }
                MeeEviGrd1.Visible = false;
            }
        }

        private void MeeRemarkGrd1_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd1.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd1.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd1.Text;
                }
                MeeRemarkGrd1.Visible = false;
            }
        }

        private void MeeCompGrd2_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd2.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd2.Text.Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = MeeCompGrd2.Text;
                }
                MeeCompGrd2.Visible = false;
            }
        }

        private void MeeIndGrd2_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd2.Text.Length == 0)
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 1].Value = MeeIndGrd2.Text;
                }
                MeeIndGrd2.Visible = false;
            }
        }

        private void MeeEviGrd2_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd2.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd2.Text.Length == 0)
                    Grd2.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 2].Value = MeeEviGrd2.Text;
                }
                MeeEviGrd2.Visible = false;
            }
        }

        private void MeeRemarkGrd2_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd2.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd2.Text.Length == 0)
                    Grd2.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd2.Text;
                }
                MeeRemarkGrd2.Visible = false;
            }
        }

        private void MeeCompGrd3_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd3.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd3.Text.Length == 0)
                    Grd3.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 0].Value = MeeCompGrd3.Text;
                }
                MeeCompGrd3.Visible = false;
            }
        }

        private void MeeIndGrd3_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd3.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd3.Text.Length == 0)
                    Grd3.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 1].Value = MeeIndGrd3.Text;
                }
                MeeIndGrd3.Visible = false;
            }
        }

        private void MeeEviGrd3_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd3.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd3.Text.Length == 0)
                    Grd3.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 2].Value = MeeEviGrd3.Text;
                }
                MeeEviGrd3.Visible = false;
            }
        }

        private void MeeRemarkGrd3_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd3.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd3.Text.Length == 0)
                    Grd3.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd3.Text;
                }
                MeeRemarkGrd3.Visible = false;
            }
        }

        private void MeeCompGrd4_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd4.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd4.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 0].Value = MeeCompGrd4.Text;
                }
                MeeCompGrd4.Visible = false;
            }
        }

        private void MeeIndGrd4_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd4.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd4.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value = MeeIndGrd4.Text;
                }
                MeeIndGrd4.Visible = false;
            }
        }

        private void MeeEviGrd4_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd4.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd4.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 2].Value = MeeEviGrd4.Text;
                }
                MeeEviGrd4.Visible = false;
            }
        }

        private void MeeRemarkGrd4_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd4.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd4.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd4.Text;
                }
                MeeRemarkGrd4.Visible = false;
            }
        }

        private void MeeCompGrd5_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd5.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd5.Text.Length == 0)
                    Grd5.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 0].Value = MeeCompGrd5.Text;
                }
                MeeCompGrd5.Visible = false;
            }
        }

        private void MeeIndGrd5_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd5.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd5.Text.Length == 0)
                    Grd5.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 1].Value = MeeIndGrd5.Text;
                }
                MeeIndGrd5.Visible = false;
            }
        }

        private void MeeEviGrd5_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd5.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd5.Text.Length == 0)
                    Grd5.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 2].Value = MeeEviGrd5.Text;
                }
                MeeEviGrd5.Visible = false;
            }
        }

        private void MeeRemarkGrd5_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd5.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd5.Text.Length == 0)
                    Grd5.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd5.Text;
                }
                MeeRemarkGrd5.Visible = false;
            }
        }

        private void MeeCompGrd6_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd6.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd6.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 0].Value = MeeCompGrd6.Text;
                }
                MeeCompGrd6.Visible = false;
            }
        }

        private void MeeIndGrd6_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd6.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd6.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 1].Value = MeeIndGrd6.Text;
                }
                MeeIndGrd6.Visible = false;
            }
        }

        private void MeeEviGrd6_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd6.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd6.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 2].Value = MeeEviGrd6.Text;
                }
                MeeEviGrd6.Visible = false;
            }
        }

        private void MeeRemarkGrd6_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd6.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd6.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd6.Text;
                }
                MeeRemarkGrd6.Visible = false;
            }
        }

        private void MeeCompGrd7_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd7.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd7.Text.Length == 0)
                    Grd7.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 0].Value = MeeCompGrd7.Text;
                }
                MeeCompGrd7.Visible = false;
            }
        }

        private void MeeIndGrd7_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd7.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd7.Text.Length == 0)
                    Grd7.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 1].Value = MeeIndGrd7.Text;
                }
                MeeIndGrd7.Visible = false;
            }
        }

        private void MeeEviGrd7_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd7.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd7.Text.Length == 0)
                    Grd7.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 2].Value = MeeEviGrd7.Text;
                }
                MeeEviGrd7.Visible = false;
            }
        }

        private void MeeRemarkGrd7_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd7.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd7.Text.Length == 0)
                    Grd7.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd7.Text;
                }
                MeeRemarkGrd7.Visible = false;
            }
        }

        private void MeeCompGrd8_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd8.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd8.Text.Length == 0)
                    Grd8.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 0].Value = MeeCompGrd8.Text;
                }
                MeeCompGrd8.Visible = false;
            }
        }

        private void MeeIndGrd8_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd8.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd8.Text.Length == 0)
                    Grd8.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 1].Value = MeeIndGrd8.Text;
                }
                MeeIndGrd8.Visible = false;
            }
        }

        private void MeeEviGrd8_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd8.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd8.Text.Length == 0)
                    Grd8.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 2].Value = MeeEviGrd8.Text;
                }
                MeeEviGrd8.Visible = false;
            }
        }

        private void MeeRemarkGrd8_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd8.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd8.Text.Length == 0)
                    Grd8.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd8.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd8.Text;
                }
                MeeRemarkGrd8.Visible = false;
            }
        }

        private void MeeCompGrd9_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd9.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd9.Text.Length == 0)
                    Grd9.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 0].Value = MeeCompGrd9.Text;
                }
                MeeCompGrd9.Visible = false;
            }
        }

        private void MeeIndGrd9_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd9.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd9.Text.Length == 0)
                    Grd9.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 1].Value = MeeIndGrd9.Text;
                }
                MeeIndGrd9.Visible = false;
            }
        }

        private void MeeEviGrd9_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd9.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd9.Text.Length == 0)
                    Grd9.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 2].Value = MeeEviGrd9.Text;
                }
                MeeEviGrd9.Visible = false;
            }
        }

        private void MeeRemarkGrd9_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd9.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd9.Text.Length == 0)
                    Grd9.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd9.Text;
                }
                MeeRemarkGrd9.Visible = false;
            }
        }

        private void MeeCompGrd10_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd10.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd10.Text.Length == 0)
                    Grd10.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 0].Value = MeeCompGrd10.Text;
                }
                MeeCompGrd10.Visible = false;
            }
        }

        private void MeeIndGrd10_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd10.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd10.Text.Length == 0)
                    Grd10.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 1].Value = MeeIndGrd10.Text;
                }
                MeeIndGrd10.Visible = false;
            }
        }

        private void MeeEviGrd10_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd10.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd10.Text.Length == 0)
                    Grd10.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 2].Value = MeeEviGrd10.Text;
                }
                MeeEviGrd10.Visible = false;
            }
        }

        private void MeeRemarkGrd10_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd10.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd10.Text.Length == 0)
                    Grd10.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd10.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd10.Text;
                }
                MeeRemarkGrd10.Visible = false;
            }
        }

        private void MeeCompGrd11_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd11.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd11.Text.Length == 0)
                    Grd11.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 0].Value = MeeCompGrd11.Text;
                }
                MeeCompGrd11.Visible = false;
            }
        }

        private void MeeIndGrd11_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd11.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd11.Text.Length == 0)
                    Grd11.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 1].Value = MeeIndGrd11.Text;
                }
                MeeIndGrd11.Visible = false;
            }
        }

        private void MeeEviGrd11_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd11.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd11.Text.Length == 0)
                    Grd11.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 2].Value = MeeEviGrd11.Text;
                }
                MeeEviGrd11.Visible = false;
            }
        }

        private void MeeRemarkGrd11_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd11.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd11.Text.Length == 0)
                    Grd11.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd11.Text;
                }
                MeeRemarkGrd11.Visible = false;
            }
        }

        private void MeeCompGrd12_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd12.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd12.Text.Length == 0)
                    Grd12.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 0].Value = MeeCompGrd12.Text;
                }
                MeeCompGrd12.Visible = false;
            }
        }

        private void MeeIndGrd12_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd12.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd12.Text.Length == 0)
                    Grd12.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 1].Value = MeeIndGrd12.Text;
                }
                MeeIndGrd12.Visible = false;
            }
        }

        private void MeeEviGrd12_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd12.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd12.Text.Length == 0)
                    Grd12.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 2].Value = MeeEviGrd12.Text;
                }
                MeeEviGrd12.Visible = false;
            }
        }

        private void MeeRemarkGrd12_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd12.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd12.Text.Length == 0)
                    Grd12.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd12.Text;
                }
                MeeRemarkGrd12.Visible = false;
            }
        }

        private void MeeCompGrd13_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd13.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd13.Text.Length == 0)
                    Grd13.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 0].Value = MeeCompGrd13.Text;
                }
                MeeCompGrd13.Visible = false;
            }
        }

        private void MeeIndGrd13_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd13.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd13.Text.Length == 0)
                    Grd13.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 1].Value = MeeIndGrd13.Text;
                }
                MeeIndGrd13.Visible = false;
            }
        }

        private void MeeEviGrd13_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd13.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd13.Text.Length == 0)
                    Grd13.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 2].Value = MeeEviGrd13.Text;
                }
                MeeEviGrd13.Visible = false;
            }
        }

        private void MeeRemarkGrd13_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd13.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd13.Text.Length == 0)
                    Grd13.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd13.Text;
                }
                MeeRemarkGrd13.Visible = false;
            }
        }

        private void MeeCompGrd14_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd14.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd14.Text.Length == 0)
                    Grd14.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 0].Value = MeeCompGrd14.Text;
                }
                MeeCompGrd14.Visible = false;
            }
        }

        private void MeeIndGrd14_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd14.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd14.Text.Length == 0)
                    Grd14.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 1].Value = MeeIndGrd14.Text;
                }
                MeeIndGrd14.Visible = false;
            }
        }

        private void MeeEviGrd14_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd14.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd14.Text.Length == 0)
                    Grd14.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 2].Value = MeeEviGrd14.Text;
                }
                MeeEviGrd14.Visible = false;
            }
        }

        private void MeeRemarkGrd14_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd14.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd14.Text.Length == 0)
                    Grd14.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd14.Text;
                }
                MeeRemarkGrd14.Visible = false;
            }
        }

        private void MeeCompGrd15_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd15.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd15.Text.Length == 0)
                    Grd15.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 0].Value = MeeCompGrd15.Text;
                }
                MeeCompGrd15.Visible = false;
            }
        }

        private void MeeIndGrd15_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd15.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd15.Text.Length == 0)
                    Grd15.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 1].Value = MeeIndGrd15.Text;
                }
                MeeIndGrd15.Visible = false;
            }
        }

        private void MeeEviGrd15_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd15.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd15.Text.Length == 0)
                    Grd15.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 2].Value = MeeEviGrd15.Text;
                }
                MeeEviGrd15.Visible = false;
            }
        }

        private void MeeRemarkGrd15_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd15.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd15.Text.Length == 0)
                    Grd15.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd15.Text;
                }
                MeeRemarkGrd15.Visible = false;
            }
        }

        private void MeeCompGrd16_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd16.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd16.Text.Length == 0)
                    Grd16.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 0].Value = MeeCompGrd16.Text;
                }
                MeeCompGrd16.Visible = false;
            }
        }

        private void MeeIndGrd16_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd16.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd16.Text.Length == 0)
                    Grd16.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 1].Value = MeeIndGrd16.Text;
                }
                MeeIndGrd16.Visible = false;
            }
        }

        private void MeeEviGrd16_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd16.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd16.Text.Length == 0)
                    Grd16.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 2].Value = MeeEviGrd16.Text;
                }
                MeeEviGrd16.Visible = false;
            }
        }

        private void MeeRemarkGrd16_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd16.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd16.Text.Length == 0)
                    Grd16.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd16.Text;
                }
                MeeRemarkGrd16.Visible = false;
            }
        }

        private void MeeCompGrd17_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd17.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd17.Text.Length == 0)
                    Grd17.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 0].Value = MeeCompGrd17.Text;
                }
                MeeCompGrd17.Visible = false;
            }
        }

        private void MeeIndGrd17_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd17.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd17.Text.Length == 0)
                    Grd17.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 1].Value = MeeIndGrd17.Text;
                }
                MeeIndGrd17.Visible = false;
            }
        }

        private void MeeEviGrd17_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd17.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd17.Text.Length == 0)
                    Grd17.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 2].Value = MeeEviGrd17.Text;
                }
                MeeEviGrd17.Visible = false;
            }
        }

        private void MeeRemarkGrd17_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd17.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd17.Text.Length == 0)
                    Grd17.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd17.Text;
                }
                MeeRemarkGrd17.Visible = false;
            }
        }

        private void MeeCompGrd18_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd18.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd18.Text.Length == 0)
                    Grd18.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 0].Value = MeeCompGrd18.Text;
                }
                MeeCompGrd18.Visible = false;
            }
        }

        private void MeeIndGrd18_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd18.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd18.Text.Length == 0)
                    Grd18.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 1].Value = MeeIndGrd18.Text;
                }
                MeeIndGrd18.Visible = false;
            }
        }

        private void MeeEviGrd18_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd18.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd18.Text.Length == 0)
                    Grd18.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 2].Value = MeeEviGrd18.Text;
                }
                MeeEviGrd18.Visible = false;
            }
        }

        private void MeeRemarkGrd18_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd18.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd18.Text.Length == 0)
                    Grd18.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd18.Text;
                }
                MeeRemarkGrd18.Visible = false;
            }
        }

        private void MeeCompGrd19_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd19.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd19.Text.Length == 0)
                    Grd19.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 0].Value = MeeCompGrd19.Text;
                }
                MeeCompGrd19.Visible = false;
            }
        }

        private void MeeIndGrd19_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd19.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd19.Text.Length == 0)
                    Grd19.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 1].Value = MeeIndGrd19.Text;
                }
                MeeIndGrd19.Visible = false;
            }
        }

        private void MeeEviGrd19_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd19.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd19.Text.Length == 0)
                    Grd19.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 2].Value = MeeEviGrd19.Text;
                }
                MeeEviGrd19.Visible = false;
            }
        }

        private void MeeRemarkGrd19_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd19.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd19.Text.Length == 0)
                    Grd19.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd19.Text;
                }
                MeeRemarkGrd19.Visible = false;
            }
        }

        private void MeeCompGrd20_Leave(object sender, EventArgs e)
        {
            if (MeeCompGrd20.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeCompGrd20.Text.Length == 0)
                    Grd20.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 0].Value = MeeCompGrd20.Text;
                }
                MeeCompGrd20.Visible = false;
            }
        }

        private void MeeIndGrd20_Leave(object sender, EventArgs e)
        {
            if (MeeIndGrd20.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeIndGrd20.Text.Length == 0)
                    Grd20.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 1].Value = MeeIndGrd20.Text;
                }
                MeeIndGrd20.Visible = false;
            }
        }

        private void MeeEviGrd20_Leave(object sender, EventArgs e)
        {
            if (MeeEviGrd20.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeEviGrd20.Text.Length == 0)
                    Grd20.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 2].Value = MeeEviGrd20.Text;
                }
                MeeEviGrd20.Visible = false;
            }
        }

        private void MeeRemarkGrd20_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd20.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeRemarkGrd20.Text.Length == 0)
                    Grd20.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 3].Value = MeeRemarkGrd20.Text;
                }
                MeeRemarkGrd20.Visible = false;
            }
        }

        #endregion

        #region Request Edit
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeCompGrd1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeIndGrd1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeEviGrd1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeRemarkGrd1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd2, MeeCompGrd2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd2, MeeIndGrd2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd2, MeeEviGrd2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd2, MeeRemarkGrd2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd3, MeeCompGrd3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd3, MeeIndGrd3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd3, MeeEviGrd3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd3, MeeRemarkGrd3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeCompGrd4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeIndGrd4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeEviGrd4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeRemarkGrd4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd5, MeeCompGrd5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd5, MeeIndGrd5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd5, MeeEviGrd5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd5, MeeRemarkGrd5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd6, MeeCompGrd6, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd6, MeeIndGrd6, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd6, MeeEviGrd6, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd6, MeeRemarkGrd6, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd7, MeeCompGrd7, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd7, MeeIndGrd7, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd7, MeeEviGrd7, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd7, MeeRemarkGrd7, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd8, MeeCompGrd8, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd8, MeeIndGrd8, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd8, MeeEviGrd8, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd8, MeeRemarkGrd8, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }
        }

        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd9, MeeCompGrd9, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd9, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd9, MeeIndGrd9, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd9, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd9, MeeEviGrd9, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd9, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd9, MeeRemarkGrd9, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd9, e.RowIndex);
            }
        }

        private void Grd10_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd10, MeeCompGrd10, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd10, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd10, MeeIndGrd10, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd10, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd10, MeeEviGrd10, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd10, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd10, MeeRemarkGrd10, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd10, e.RowIndex);
            }
        }

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd11, MeeCompGrd11, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd11, MeeIndGrd11, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd11, MeeEviGrd11, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd11, MeeRemarkGrd11, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
            }
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd12, MeeCompGrd12, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd12, MeeIndGrd12, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd12, MeeEviGrd12, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd12, MeeRemarkGrd12, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
            }
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd13, MeeCompGrd13, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd13, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd13, MeeIndGrd13, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd13, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd13, MeeEviGrd13, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd13, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd13, MeeRemarkGrd13, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd13, e.RowIndex);
            }
        }

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd14, MeeCompGrd14, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd14, MeeIndGrd14, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd14, MeeEviGrd14, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd14, MeeRemarkGrd14, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
            }
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd15, MeeCompGrd15, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd15, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd15, MeeIndGrd15, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd15, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd15, MeeEviGrd15, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd15, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd15, MeeRemarkGrd15, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd15, e.RowIndex);
            }
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd16, MeeCompGrd16, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd16, MeeIndGrd16, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd16, MeeEviGrd16, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd16, MeeRemarkGrd16, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
            }
        }

        private void Grd17_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd17, MeeCompGrd17, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd17, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd17, MeeIndGrd17, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd17, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd17, MeeEviGrd17, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd17, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd17, MeeRemarkGrd17, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd17, e.RowIndex);
            }
        }

        private void Grd18_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd18, MeeCompGrd18, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd18, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd18, MeeIndGrd18, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd18, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd18, MeeEviGrd18, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd18, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd18, MeeRemarkGrd18, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd18, e.RowIndex);
            }
        }

        private void Grd19_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd19, MeeCompGrd19, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd19, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd19, MeeIndGrd19, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd19, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd19, MeeEviGrd19, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd19, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd19, MeeRemarkGrd19, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd19, e.RowIndex);
            }
        }

        private void Grd20_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd20, MeeCompGrd20, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd20, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd20, MeeIndGrd20, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd20, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd20, MeeEviGrd20, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd20, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                MeeRequestEdit(Grd20, MeeRemarkGrd20, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd20, e.RowIndex);
            }
        }


        #endregion

        #region Key down
        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        private void Grd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd15, e, BtnSave);
        }

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
        }

        private void Grd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd13, e, BtnSave);
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
        }

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
        }

        private void Grd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd10, e, BtnSave);
        }

        private void Grd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd9, e, BtnSave);
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
        }

        private void Grd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd17, e, BtnSave);
        }

        private void Grd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd18, e, BtnSave);
        }

        private void Grd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd19, e, BtnSave);
        }

        private void Grd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd20, e, BtnSave);
        }
        #endregion

        #region header double klik

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd1, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd1, 0, 0);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        Grd1.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd2, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd2, 0, 0);
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        Grd2.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd3_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd3, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd3, 0, 0);
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                        Grd3.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd4_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd4, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd4, 0, 0);
                    for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                        Grd4.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd5_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd5, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd5, 0, 0);
                    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                        Grd5.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd6_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd6, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd6, 0, 0);
                    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                        Grd6.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd7_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd7, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd7, 0, 0);
                    for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                        Grd7.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd8_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd8, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd8, 0, 0);
                    for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
                        Grd8.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd9_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd9, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd9, 0, 0);
                    for (int Row = 0; Row < Grd9.Rows.Count - 1; Row++)
                        Grd9.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd10_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd10, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd10, 0, 0);
                    for (int Row = 0; Row < Grd10.Rows.Count - 1; Row++)
                        Grd10.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd11_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd11, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd11, 0, 0);
                    for (int Row = 0; Row < Grd11.Rows.Count - 1; Row++)
                        Grd11.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd12_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd12, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd12, 0, 0);
                    for (int Row = 0; Row < Grd12.Rows.Count - 1; Row++)
                        Grd12.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd13_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd13, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd13, 0, 0);
                    for (int Row = 0; Row < Grd13.Rows.Count - 1; Row++)
                        Grd13.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd14_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd14, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd14, 0, 0);
                    for (int Row = 0; Row < Grd14.Rows.Count - 1; Row++)
                        Grd14.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd15_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd15, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd15, 0, 0);
                    for (int Row = 0; Row < Grd15.Rows.Count - 1; Row++)
                        Grd15.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd16_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd16, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd16, 0, 0);
                    for (int Row = 0; Row < Grd16.Rows.Count - 1; Row++)
                        Grd16.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd17_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd17, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd17, 0, 0);
                    for (int Row = 0; Row < Grd17.Rows.Count - 1; Row++)
                        Grd17.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd18_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd18, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd18, 0, 0);
                    for (int Row = 0; Row < Grd18.Rows.Count - 1; Row++)
                        Grd18.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd19_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd19, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd19, 0, 0);
                    for (int Row = 0; Row < Grd19.Rows.Count - 1; Row++)
                        Grd19.Cells[Row, 0].Value = Data;
                }
            }
        }

        private void Grd20_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd20, 0, 0).Length != 0)
                {
                    var Data = Sm.GetGrdStr(Grd20, 0, 0);
                    for (int Row = 0; Row < Grd20.Rows.Count - 1; Row++)
                        Grd20.Cells[Row, 0].Value = Data;
                }
            }
        }

        #endregion

        private void LueCompLevelName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData2();
                Sm.RefreshLookUpEdit(LueCompLevelName, new Sm.RefreshLue1(SetLueCompetenceLevelName));
            }
        }

        private void LueCompetenceLevelType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceLevelType, new Sm.RefreshLue1(SetLueCompetenceLevelType));
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<Assesment>();

                ClearGrd(new List<iGrid>(){
                Grd1, Grd2, Grd3, Grd4, Grd5,
                Grd6, Grd7, Grd8, Grd9, Grd10,
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
            });
                try
                {
                    ProcessAssesment();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #endregion

        #region Class

        public class Assesment
        {
            public string SectionNo { get; set; }
            public string Description { get; set; }
            public string Indicator { get; set; }
            public string Evidence { get; set;}
            public string Remark { get; set; }
        }
        #endregion 

       

    }
}
