﻿#region Update
/*
    13/10/2022 [RDA/PRODUCT] Purchase Return Invoice menampilkan informasi Receiving, PI, dan Voucher berdasarkan param PurchaseReturnInvoiceShowSource & Update PaidInd
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseReturnInvoiceDlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseReturnInvoice mFrmParent;
        string mSQL = string.Empty;
        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseReturnInvoiceDlg4(FrmPurchaseReturnInvoice FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List of Voucher";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = false;
            SetGrd();
            SetSQL();
            ShowData();
        }
        #endregion

        #region Standard Method

        //SetGrd
        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "No",

                        //1-2
                        "Voucher#", ""

                    }, new int[]
                    {
                        50, 300, 20
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.SetGrdProperty(Grd1, true);

        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT H.DocNo VoucherDocNo ");
            SQL.AppendLine("FROM tblpurchasereturninvoicehdr A  ");
            SQL.AppendLine("INNER JOIN tblpurchasereturninvoicedtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("LEFT JOIN TblDOVdDtl C On B.DOVdDocNo=C.DocNo And B.DOVdDNo=C.DNo ");
            SQL.AppendLine("LEFT JOIN tblrecvvddtl D ON C.RecvVdDocNo=D.DocNo AND C.RecvVdDNo=D.DNo ");
            SQL.AppendLine("LEFT JOIN tblpurchaseinvoicedtl E ON D.DocNo=E.RecvVdDocNo AND D.DNo=E.RecvVdDNo ");
            SQL.AppendLine("LEFT JOIN tbloutgoingpaymentdtl F ON E.DocNo=F.InvoiceDocNo ");
            SQL.AppendLine("LEFT JOIN tbloutgoingpaymenthdr G ON F.DocNo=G.DocNo ");
            SQL.AppendLine("LEFT JOIN tblvoucherhdr H ON G.VoucherRequestDocNo=H.VoucherRequestDocNo ");
            SQL.AppendLine("WHERE A.DocNo=@DocNo ");

            mSQL = SQL.ToString();
        }
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtVoucher.Text, new string[] { "H.DocNo" });
                Sm.CmParam(ref cm, "@DocNo", mFrmParent.TxtDocNo.Text);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + "GROUP BY A.DocNo, D.DocNo, E.DocNo, G.DocNo ",
                        new string[]
                        { 
                            //0
                            "VoucherDocNo",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Grid Method
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        #endregion

        #region Event

        #region Button Event

        #endregion

        #region Misc Control Event
        private void TxtVoucher_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVoucher_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }
        #endregion

        #endregion
    }
}
