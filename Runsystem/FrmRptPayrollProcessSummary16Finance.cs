﻿#region Update
/*
    03/12/2020 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary16Finance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mSalaryInd = "1", mRptPayrollProcessSummaryVersion = "1";
        private bool
             mIsNotFilterByAuthorization = false,
             mIsFilterBySiteHR = false,
             mIsFilterByDeptHR = false,
             mIsPayrunSiteEnabled = false;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary16Finance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueOption(ref LueCostOfGroup, "EmpCostGroup");
                Sl.SetLuePayrollGrpCode(ref LuePayrollGrpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mRptPayrollProcessSummaryVersion = Sm.GetParameter("RptPayrollProcessSummaryVersion");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsPayrunSiteEnabled = Sm.GetParameterBoo("IsPayrunSiteEnabled");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, D.DeptName, ");
            SQL.AppendLine("H.PGName, J.SiteName, ");
            SQL.AppendLine("Sum(A.Salary) Salary, Sum(A.PLAmt) PLAmt, Sum(A.ProcessPLAmt) ProcessPLAmt, ");
            SQL.AppendLine("Sum(A.UPLAmt) UPLAmt, Sum(A.ProcessUPLAmt) ProcessUPLAmt, Sum(A.OT1Amt) OT1Amt, Sum(A.OT2Amt) OT2Amt, ");
            SQL.AppendLine("Sum(A.OTHolidayAmt) OTHolidayAmt, Sum(A.TaxableFixAllowance) TaxableFixAllowance, Sum(A.NonTaxableFixAllowance) NonTaxableFixAllowance, Sum(A.FixAllowance) FixAllowance, Sum(A.PerformanceValue) PerformanceValue, ");
            SQL.AppendLine("Sum(A.ADOT) ADOT, Sum(A.Meal) Meal, Sum(A.Transport) Transport, Sum(A.TaxAllowance) TaxAllowance, Sum(A.SSEmployerPension) SSEmployerPension, ");
            SQL.AppendLine("Sum(A.SSEmployerHealth) SSEmployerHealth, Sum(A.SSEmployeeHealth) SSEmployeeHealth, Sum(A.SSEmployerEmployment) SSEmployerEmployment, Sum(A.SSEmployeeEmployment) SSEmployeeEmployment, Sum(A.SSErPension) SSErPension, Sum(A.SSEePension) SSEePension, ");
            SQL.AppendLine("Sum(A.SSEmployeePension) SSEmployeePension, Sum(A.NonTaxableFixDeduction) NonTaxableFixDeduction, Sum(A.TaxableFixDeduction) TaxableFixDeduction, Sum(A.FixDeduction) FixDeduction, Sum(A.DedEmployee) DedEmployee, ");
            SQL.AppendLine("Sum(A.DedProduction) DedProduction, Sum(A.DedProdLeave) DedProdLeave, Sum(A.EmpAdvancePayment) EmpAdvancePayment, Sum(A.SalaryAdjustment) SalaryAdjustment, Sum((A.Amt+A.Tax-A.TaxAllowance)) As Brutto, ");
            SQL.AppendLine("Sum(A.Tax) Tax, Sum(A.EOYTax) EOYTax, Sum(A.Amt) Amt, L.OptDesc As CostGroupDesc ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR && mIsPayrunSiteEnabled)
            {
                SQL.AppendLine("    And (B.SiteCode Is Null Or ");
                SQL.AppendLine("    (B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X.EmpCode, X.payruncode, X.Amt001, X.Amt004  ");
            SQL.AppendLine("    from  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.EmpCode, A.PayrunCode, A.Amt As Amt001, ifnull(B.Amt004, 0) Amt004  ");
            SQL.AppendLine("        From TblPayrollProcessAd A ");
            SQL.AppendLine("        Left Join  ");
            SQL.AppendLine("        (  ");
            SQL.AppendLine("            Select A.EmpCode, A.PayrunCode, A.Amt As Amt004 ");
            SQL.AppendLine("            From TblPayrollProcessAd A ");
            SQL.AppendLine("            Where A.AdCode ='065' ");
            SQL.AppendLine("        )B On A.PayrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
            SQL.AppendLine("        Where A.AdCode ='001' ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(") K On A.payruncode = K.PayrunCode And  A.EmpCode = K.EmpCode ");
            SQL.AppendLine("Left Join TblOption L On L.OptCat='EmpCostGroup' And C.CostGroup=L.OptCode ");
            SQL.AppendLine("Where 1=1 ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 43;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Payrun Code",
                    "Payrun Name",
                    "Department",
                    "Cost's Group",

                    //6-10
                    "Payroll's Group",
                    "Site",
                    "Salary", 
                    "Paid Leave"+Environment.NewLine+"(Amount)", 
                    "Processed Paid"+Environment.NewLine+"Leave (Amount)", 

                    //11-15
                    "Unpaid Leave"+Environment.NewLine+"(Amount)", 
                    "Processed Unpaid"+Environment.NewLine+"Leave (Amount)", 
                    "OT 1"+Environment.NewLine+"(Amount)", 
                    "OT 2"+Environment.NewLine+"(Amount)", 
                    "OT Holiday"+Environment.NewLine+"(Amount)", 

                    //16-20
                    "Taxable Fixed"+Environment.NewLine+"Allowance", 
                    "Non Taxable Fixed"+Environment.NewLine+"Allowance",
                    "Fixed"+Environment.NewLine+"Allowance", 
                    "Performance"+Environment.NewLine+"Allowance", 
                    "",

                    //21-25
                    "",
                    "Meal", 
                    "Transport", 
                    "Tax"+Environment.NewLine+"Allowance",
                    "SS Employer"+Environment.NewLine+"Health", 

                    //26-30
                    "SS Employee"+Environment.NewLine+"Health", 
                    "SS Employer"+Environment.NewLine+"Employment",
                    "SS Employee"+Environment.NewLine+"Employment", 
                    "SS Employer"+Environment.NewLine+"Pension",
                    "SS Employee"+Environment.NewLine+"Pension",

                    //31-35
                    "SS Employer"+Environment.NewLine+"Jiwasraya",
                    "SS Employee"+Environment.NewLine+"Jiwasraya",
                    "Non Taxable Employee's"+Environment.NewLine+"Deduction",
                    "Taxable Employee's"+Environment.NewLine+"Deduction", 
                    "Fixed"+Environment.NewLine+"Deduction", 

                    //36-40
                    "Employee's"+Environment.NewLine+"Advance Payment",
                    "Salary"+Environment.NewLine+"Adjustment",
                    "Salary Before"+Environment.NewLine+"Adjustment",
                    "Brutto",
                    "Tax", 

                    //41-42
                    "End of Year"+Environment.NewLine+"Tax", 
                    "Take Home Pay", 
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 120, 200, 130, 200, 
                    
                    //6-10
                    180, 150, 150, 100, 100, 
                    
                    //11-15
                    100, 100, 100, 150, 130, 
                    
                    //16-20
                    100, 100, 100, 100, 20,  
                    
                    //21-25
                    20, 100, 100, 100, 100, 
                    
                    //26-30
                    100, 100, 100, 100, 100, 

                    //31-35
                    100, 100, 100, 100, 100, 
                    
                    //36-40
                    100, 100, 100, 100, 100, 
                    
                    //41-42
                    100, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 1, 20, 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
                34, 35, 36, 37, 38, 39, 40, 41, 42
            }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] 
            { 
                0, 
                2, 3, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 
                22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  
                41, 42
            });
            Sm.GrdColInvisible(Grd1, new int[] 
            { 
                1, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 
                34, 35, 36, 37, 38, 40, 41
            });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] 
            {
                9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 
                34, 35, 36, 37, 38, 40, 41
            }, !ChkHideInfoInGrd.Checked);
        }

        override protected void  ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";
                string GroupBy = " Group By A.PayrunCode, B.PayrunName, D.DeptName, H.PGName, J.SiteName, L.OptDesc ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCostOfGroup), "C.CostGroup", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrollGrpCode), "B.PGCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + GroupBy + " Order By A.PayrunCode;",
                        new string[]
                        {
                            //0
                            "PayrunCode",
                        
                            //1-5
                            "PayrunName",
                            "DeptName",
                            "CostGroupDesc", 
                            "PGName",
                            "SiteName",

                            //6-10
                            "Salary", 
                            "PLAmt", 
                            "ProcessPLAmt", 
                            "UPLAmt", 
                            "ProcessUPLAmt",

                            //11-15
                            "OT1Amt", 
                            "OT2Amt", 
                            "OTHolidayAmt",                            
                            "TaxableFixAllowance",
                            "NonTaxableFixAllowance",

                            //16-20
                            "FixAllowance", 
                            "PerformanceValue", 
                            "Meal",
                            "Transport",
                            "TaxAllowance",

                            //21-25
                            "SSEmployerHealth", 
                            "SSEmployeeHealth", 
                            "SSEmployerEmployment", 
                            "SSEmployeeEmployment",
                            "SSErPension", 

                            //26-30
                            "SSEePension",
                            "SSEmployerPension", 
                            "SSEmployeePension", 
                            "NonTaxableFixDeduction",
                            "TaxableFixDeduction",

                            //31-35
                            "FixDeduction", 
                            "EmpAdvancePayment",
                            "SalaryAdjustment", 
                            "Brutto",
                            "Tax", 

                            //36-37
                            "EOYTax",
                            "Amt",
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 34);
                            if (Sm.GetGrdDec(Grd1, Row, 37) < 0)
                            {
                                Grd1.Cells[Row, 38].Value = Sm.GetGrdDec(Grd1, Row, 37) + Sm.GetGrdDec(Grd1, Row, 39);
                            }
                            else if (Sm.GetGrdDec(Grd1, Row, 37) > 0)
                            {
                                Grd1.Cells[Row, 38].Value = Sm.GetGrdDec(Grd1, Row, 39) - Sm.GetGrdDec(Grd1, Row, 37);
                            }
                            else
                            {
                                Grd1.Cells[Row, 38].Value = 0m;
                            }
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 37);
                        }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            Grd1.BeginUpdate();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                { 
                    8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                    22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
                    34, 35, 36, 37, 38, 39, 40, 41, 42
                });
            Grd1.EndUpdate();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            //if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            //{
            //    e.DoDefault = false;
            //    if (e.KeyChar == Char.Parse(" "))
            //    {
            //        Sm.FormShowDialog(
            //            new FrmRptPayrollProcessSummary16FinanceDlg(
            //                this,
            //                Sm.GetGrdStr(Grd1, r, 2)
            //                ));
            //    }
            //}
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 18) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2));
                }
            }
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 35) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2));
                }
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            //if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            //{
            //    Sm.FormShowDialog(
            //        new FrmRptPayrollProcessSummary16FinanceDlg(
            //            this,
            //            Sm.GetGrdStr(Grd1, r, 2)
            //            ));
            //}
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 18) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2));
            }
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 35) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }

        override protected void PrintData()
        {
        }

        private void ShowPayrollProcessAD(string ADType, string PayrunCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, Sum(A.Amt) Amt ");
            SQL.AppendLine("From TblPayrollProcessAD A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType=@ADType And B.AmtType='1' ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.Amt<>0.00 ");
            SQL.AppendLine("Group By B.ADName ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };

                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParam<String>(ref cm, "@ADType", ADType);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtPayCod_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void LueCostOfGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostOfGroup, new Sm.RefreshLue2(Sl.SetLueOption), "EmpCostGroup");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCostOfGroup_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost of Group");
        }

        private void LuePayrollGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrollGrpCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrollGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payroll Group");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
