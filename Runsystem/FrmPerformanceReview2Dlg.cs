﻿#region Update
/*
    18/05/2022 [MYA/HIN] New Apps New Performance Review
    23/11/2022 [HPH/HIN] Penyesuaian Printout
    07/12/2022 [WED/HIN] rubah label text form + hanya goals process yg approved saja yang bisa ditarik
    08/12/2022 [WED/HIN] rubah value di kolom Period
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPerformanceReview2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPerformanceReview2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPerformanceReview2Dlg(FrmPerformanceReview2 FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of Performance Review";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno, A.DocDt, B.ActInd, B.GoalsName, B.PICCode, C.EmpName, B.Remark, ");
            SQL.AppendLine("B.EvaluatorCode, E.OptDesc Directorate, D.EmpName EvaluatorName, A.Yr, A.Period, F.OptDesc As PeriodDesc, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblGoalsProcessHdr A ");
            SQL.AppendLine("inner join TblGoalsSettingHdr B on A.GoalsDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EvaluatorCode = D.EmpCode ");
            SQL.AppendLine("Inner Join TblOption E On E.OptCat = 'GoalsSettingDirectorate' And E.OptCode = B.DirectorateCode ");
            SQL.AppendLine("Inner Join TblOption F On F.OptCat = 'GoalsProcessPeriode' And F.OptCode = A.Period ");
            SQL.AppendLine("Where B.ActInd = 'Y' And B.Status = 'A' And A.Status = 'A' And A.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Active",
                        "Goals"+Environment.NewLine+"Name",
                        "PIC Code",
                        
                        //6-10
                        "PIC Name",
                        "Directorate",
                        "Evaluator Code",
                        "Evaluator",
                        "Remark",
                        
                        //11-15
                        "Year",
                        "PeriodCode",
                        "Period",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",

                        //16-19
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 200, 80, 
                        
                        //6-10
                        150, 150, 150, 200, 200,
                        
                        //11-15
                        100, 0, 100, 100, 100, 

                        //16-19
                        100, 100, 100, 100

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            //if (IsShowDataNotValid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtGoalsName.Text, new string[] { "A.GoalsName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "DocDt", "ActInd", "GoalsName", "PICCode", "EmpName",   
                            //6-10
                            "Directorate", "EvaluatorCode", "EvaluatorName", "Remark", "Yr",
                            //11-15
                            "Period", "PeriodDesc", "CreateBy", "CreateDt", "LastUpBy", 
                            //16
                            "LastUpDt"

                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtGoalsProcessDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                //mFrmParent.TxtGoalsName.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtPICCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtPICName.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtEvaluator.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.LueYr.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                Sm.SetLue(mFrmParent.LuePeriod, Sm.GetGrdStr(Grd1, Row, 12));
                mFrmParent.ShowGoalsProcessDtl(Sm.GetGrdStr(Grd1, Row, 1));
                mFrmParent.ShowGoalsProcessDtl2(Sm.GetGrdStr(Grd1, Row, 1));
                mFrmParent.ShowGoalsProcessDtl3(Sm.GetGrdStr(Grd1, Row, 1));
                mFrmParent.ShowGoalsProcessDtl4(Sm.GetGrdStr(Grd1, Row, 1));
                mFrmParent.ComputeTotalScore();
                mFrmParent.ComputeTotalWeight();
                mFrmParent.ComputeTotalScoreDtl();
                this.Close();
            }
        }

        #endregion

        #region Additional Method

        private bool IsShowDataNotValid()
        {
            return
                IsDateNotValid();
        }

        private bool IsDateNotValid()
        {

            if (!Sm.CompareStr(Sm.GetLue(mFrmParent.LueYr), Sm.GetDte(DteDocDt1).Substring(0, 4)))
            {
                Sm.StdMsg(mMsgType.Warning,
                            "Date should be in range with year(" + Sm.GetLue(mFrmParent.LueYr) + ").");
                DteDocDt1.Focus();
                return true;
            }

            if (!Sm.CompareStr(Sm.GetLue(mFrmParent.LueYr), Sm.GetDte(DteDocDt2).Substring(0, 4)))
            {
                Sm.StdMsg(mMsgType.Warning,
                            "Date should be in range with year.");
                DteDocDt2.Focus();
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkGoalsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "KPI Name");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtGoalsName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
