﻿#region Update
/*
    09/02/2021 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOProjectJournalDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOProjectJournal mFrmParent;
        private byte mDocType = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOProjectJournalDlg(FrmDOProjectJournal FrmParent, byte DocType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");

            if (mDocType == 1)
            {
                SQL.AppendLine("Select A.DocNo, A.DocDt, C.SOCDocNo SOContractDocNo, G.CtName, D.PONo, H.ProjectCode, H.ProjectName ");
                SQL.AppendLine("From TblDOProjectHdr A ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr E On D.BOQDocNO = E.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo = F.DocNo ");
                SQL.AppendLine("Inner JOin TblCustomer G On F.CtCode = G.CtCode ");
                SQL.AppendLine("Left Join TblProjectGroup H On F.PGCode = H.PGCode ");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, B.SOContractDocNo, G.CtName, C.PONo, H.ProjectCode, H.ProjectName ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    And A.MenuCode In (Select MenuCode From TblMenu Where Param In ('FrmJournal', 'FrmVoucherJournal')) ");
                SQL.AppendLine("    And B.AcNo Like '5%' ");
                SQL.AppendLine("    And B.SOContractDocNo Is Not Null ");
                SQL.AppendLine("    And B.SOContractDocNo = @SOContractDocNo ");
                SQL.AppendLine("    And A.DocNo Not In ( ");
                SQL.AppendLine("        Select JournalDocNo ");
                SQL.AppendLine("        From TblDOProjectJournal ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOContractDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr E On C.BOQDocNO = E.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo = F.DocNo ");
                SQL.AppendLine("Inner JOin TblCustomer G On F.CtCode = G.CtCode ");
                SQL.AppendLine("Left Join TblProjectGroup H On F.PGCode = H.PGCode ");
            }

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();

            return mSQL;
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "SO Contract#",
                    "Customer",

                    //6-8
                    "Customer PO#",
                    "Project Code",
                    "Project Name"
                },
                new int[]
                {
                    50,
                    150, 20, 80, 180, 200,
                    120, 120, 280
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                if (mDocType == 1)
                {
                    var f = new FrmDOProject("***");
                    f.Tag = "***";
                    f.Text = Sm.GetMenuDesc("FrmDOProject");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmJournal("***");
                    f.Tag = "***";
                    f.Text = Sm.GetMenuDesc("FrmJournal");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (mDocType == 1)
                {
                    var f = new FrmDOProject("***");
                    f.Tag = "***";
                    f.Text = Sm.GetMenuDesc("FrmDOProject");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmJournal("***");
                    f.Tag = "***";
                    f.Text = Sm.GetMenuDesc("FrmJournal");
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if (mDocType != 1) Sm.CmParam<String>(ref cm, "@SOContractDocNo", GetSOContractDocNo());

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSONo.Text, "T.SOContractDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By T.DocDt Desc, T.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "SOContractDocNo", "CtName", "PONo", "ProjectCode",

                        //6
                        "ProjectName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if(mDocType == 1)
                    mFrmParent.TxtDODocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                else
                    mFrmParent.TxtJournalDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);

                this.Hide();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private string GetSOContractDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.SOCDocNo ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner JOin TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @Param ");
            SQL.AppendLine("Inner Join TblSOCOntractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), mFrmParent.TxtDODocNo.Text);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtSONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        #endregion

        #endregion

    }
}
