﻿#region Update
/*
    22/06/2017 [Ari] tambah DO Request
    26/07/2017 [TKG] Kalau menggunakan budget, kalau item tsb tidak memiliki estimated price berdasarkan quotation maka warna item name menjadi merah.
    26/07/2017 [TKG] Kalau menggunakan budget, kalau item tsb tidak memiliki estimated price berdasarkan quotation maka warna item name menjadi merah.
    01/08/2017 [WED] item dari DO Request yg masuk ke MR itu yg RequestedQty > All Stock berdasarkan Whs DORequest
    10/08/2017 [WED] Tambah approval berdasarkan Department, DocType = MaterialRequestWO
    22/08/2017 [TKG] menggunakan budget department
    19/01/2018 [WED] ShowDOInformation disamain kayak di MR (nggak lihat stock)
    19/01/2018 [WED] tambah validasi WO# saat klik button DORequest#
    19/03/2018 [TKG] setelah simpan langsung print data
    22/03/2018 [HAR] bug informasi approval gak muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequestWO : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,IsProcFormat = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmMaterialRequestWOFind FrmFind;
        private bool
            mIsRemarkForApprovalMandatory = false,
            IsAutoGeneratePurchaseLocalDocNo = false,
            mIsApprovalBySiteMandatory = false,
            mIsDocNoWithDeptShortCode = false,
            mIsDORequestNeedStockValidation = false;
        internal bool 
            mIsSiteMandatory = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsFilterByItCt = false,
            mIsBudgetActive = false,
            mIsShowForeignName = false;
        private string
            mBudgetBasedOn = "1",
            mReqTypeForNonBudget = string.Empty,
            mReqType = string.Empty,
            mSQL = string.Empty;

        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmMaterialRequestWO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Material Request Based On Work Order";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (!mIsDORequestNeedStockValidation) LblDORe.ForeColor = Color.Red; 
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtDocNo, TxtWODocNo, TxtRemainingBudget }, true);
                SetGrd();
                SetFormControl(mState.View);
                SetLueDeptCode(ref LueDeptCode);
                
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            mIsRemarkForApprovalMandatory = Sm.GetParameter("IsMaterialRequestRemarkForApprovalMandatory") == "Y";
            IsAutoGeneratePurchaseLocalDocNo = Sm.GetParameter("IsAutoGeneratePurchaseLocalDocNo") == "Y";
            mIsFilterByItCt = Sm.GetParameter("IsFilterByItCt") == "Y";
            mIsFilterByDept = Sm.GetParameter("IsFilterByDept") == "Y";
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
            mIsApprovalBySiteMandatory = Sm.GetParameter("IsApprovalBySiteMandatory") == "Y";
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsBudgetActive = Sm.GetParameter("IsBudgetActive") == "Y";
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsDocNoWithDeptShortCode = Sm.GetParameter("IsDocNoWithDeptShortCode") == "Y";
            mIsDORequestNeedStockValidation = Sm.GetParameter("IsDORequestNeedStockValidation") == "Y";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Cancel Reason",
                        "Status ",
                        "Cheked by",
                        
                        //6-10
                        "",
                        "Item's Code",
                        "Local Code",
                        "",
                        "Item's Name",

                        //11-15
                        "Foreign Name",
                        "Subcategory code",
                        "Subcategory",
                        "Minimum Stock",
                        "Reoder Point",

                        //16-20
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Quotation#",
                        "Quotation DNo",

                        //21-25
                        "Quotation Date",
                        "Estimated Unit Price",
                        "Total",
                        "Remark",
                        "DORequestDocNo",

                        //26
                        "DORequestDNo"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 200, 80, 100,
                        
                        //6-10
                        20, 100, 150, 20, 230,  
                        
                        //11-15
                        200, 80, 100, 100, 100, 
                        
                        //16-20
                        120, 100, 100, 150, 80, 
                        
                        //21-25
                        100, 150, 150, 400, 0,

                        //26
                        0
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 16, 22, 23 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 6, 9 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 18, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 25, 26 });
            if (!mIsDORequestNeedStockValidation)
                Sm.GrdColInvisible(Grd1, new int[] { 6 });
            if (mIsShowForeignName)
            {
                if (IsProcFormat == "1")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 7, 8, 9, 19, 20, 21, 22, 23, 12 }, false);
                }
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 7, 8, 9, 19, 20, 21, 22, 23, 12, 13 }, false);
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 7, 8, 9, 11, 12, 19, 20, 21, 22, 23 }, false);
                }
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 7, 8, 9, 11, 12, 13, 19, 20, 21, 22, 23 }, false);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 9, 21}, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, LueSiteCode, LueBCCode, MeeRemark
                    }, true);
                    BtnWODocNo.Enabled = false;
                    BtnDORDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 10, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 12, 13, 11, 8, 25, 26 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueReqType, LueDeptCode, LueSiteCode, MeeRemark }, false);                 
                    if (!IsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLocalDocNo }, false);
                    BtnWODocNo.Enabled = true;
                    if (!mIsDORequestNeedStockValidation){ BtnDORDocNo.Enabled = true; }
                    Sm.GrdColInvisible(Grd1, new int[]{ 3 }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6, 16, 18, 24 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, 
                LueSiteCode, TxtWODocNo, LueBCCode, MeeRemark, TxtDORDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtRemainingBudget }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 16, 22, 23 });
            if (mIsBudgetActive) Grd1.Cells[0, 10].ForeColor = Color.Black;
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialRequestWOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            InsertData(string.Empty);
        }

        private void InsertData(string DeptCode)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueReqType(ref LueReqType, string.Empty);
                if (mReqTypeForNonBudget.Length > 0)
                    Sm.SetLue(LueReqType, mReqTypeForNonBudget);
                if (DeptCode.Length>0) Sm.SetLue(LueDeptCode, DeptCode);
                SetLueSiteCode(ref LueSiteCode, string.Empty);
                Sm.FormShowDialog(new FrmMaterialRequestWODlg2(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
                {
                    if (TxtDocNo.Text.Length == 0)
                    {
                        if (e.ColIndex == 6 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" "))
                                Sm.FormShowDialog(new FrmMaterialRequestWODlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
                        }

                        if (Sm.IsGrdColSelected(new int[] { 16, 18, 24 }, e.ColIndex))
                        {
                            if (e.ColIndex == 18) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
                            if (mIsDORequestNeedStockValidation) Sm.GrdRequestEdit(Grd1, e.RowIndex);
                            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 16, 22, 23 });
                        }
                    }
                 else
                    {
                        if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length == 0))
                            e.DoDefault = false;
                    }
                }

                if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                    f.ShowDialog();
                }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (mIsDORequestNeedStockValidation)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeRemainingBudget();
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequestWODlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 16 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 24 }, e);
            if (e.ColIndex == 16) ComputeTotal(e.RowIndex);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 18)
            {
                if (Sm.GetGrdDate(Grd1, 0, 18).Length != 0)
                {
                    var UsageDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 18));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0) Grd1.Cells[Row, 18].Value = UsageDt;
                }
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 18)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            string LocalDocNo = string.Empty;
            if (Sm.StdMsgYN("Question",
                    "Department : " + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Do you want to save this data ?"
                    ) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DeptCode = Sm.GetLue(LueDeptCode);
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 12);
            string DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequestWO", "TblMaterialRequestHdr", SubCategory);
            if (IsAutoGeneratePurchaseLocalDocNo)
                LocalDocNo = GenerateLocalDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequestWO", "TblMaterialRequestHdr", SubCategory);
            else
                LocalDocNo = TxtLocalDocNo.Text;
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveMaterialRequestWOHdr(DocNo, LocalDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    cml.Add(SaveMaterialRequestWODtl(
                        DocNo,
                        Row,
                        IsDocApprovalSettingNotExisted()
                        ));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
            if (Sm.StdMsgYN("Print", "") == DialogResult.Yes) ParPrint(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsTxtEmpty(TxtWODocNo, "Work Order#", false) ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueReqType, "Request type") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsRemainingBudgetNotValid()||
                IsSubcategoryDifferent()||
                IsSubCategoryXXX() ||
                IsWOPDocNoNotValid()||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsWOAlreadyCancelled() ||
                IsWOAlreadySettled();
        }

        private bool IsWOAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where CancelInd='Y' And DocNo=@Param;", 
                TxtWODocNo.Text,
                "WO# : " + TxtWODocNo.Text + Environment.NewLine +
                "This WO already cancelled."
                );
        }

        private bool IsWOAlreadySettled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where SettleInd='Y' And DocNo=@Param;",
                TxtWODocNo.Text,
                "WO# : " + TxtWODocNo.Text + Environment.NewLine +
                "This WO already settled."
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 16, true, "Quantity is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 18, false, "Usage date is empty.") ||
                    IsUsageDtNotValid(DocDt, Row) ||
                    (Sm.GetLue(LueReqType)=="1" && Sm.IsGrdValueEmpty(Grd1, Row, 22, true, "Estimated unit Price is 0.")) ||
                    (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 24, false, "Remark is empty."))
                    ) return true;
            }
            return false;
        }

        private bool IsUsageDtNotValid(string DocDt, int Row)
        {
            var UsageDt = Sm.GetGrdDate(Grd1, Row, 18);
            if (Sm.CompareDtTm(UsageDt, DocDt) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Document Date : " + Sm.FormatDate2("dd/MMM/yyyy", DocDt) + Environment.NewLine +
                    "Usage Date : " + Sm.FormatDate2("dd/MMM/yyyy", UsageDt) + Environment.NewLine + Environment.NewLine +
                    "Usage date is earlier than document date.");
                Sm.FocusGrd(Grd1, Row, 18);
                return true;
            }
            return false;
        }

        private bool IsRemainingBudgetNotValid()
        {
            if (TxtRemainingBudget.Text.Length != 0)
            {
                if (decimal.Parse(TxtRemainingBudget.Text)<0m)
                {
                    Sm.StdMsg(mMsgType.Warning, "Not enough available budget.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            if (!Sm.IsDataExist(
                    "Select DocType From TblDocApprovalSetting " +
                    "Where UserCode Is not Null "+
                    "And DocType='MaterialRequestWO' " +
                    "And DeptCode='" + Sm.GetLue(LueDeptCode) + "' Limit 1"
                ))
                //Sm.StdMsg(mMsgType.Warning, "Nobody will approve this request.");
                return true;
            else
                return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 12).Length == 0)
                {
                    Grd1.Cells[Row, 12].Value = Grd1.Cells[Row, 13].Value = "XXX";
                }
            }
        }

        private bool IsSubCategoryXXX()
        {
            if (IsProcFormat == "1")
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13) == "XXX")
                    {
                        Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine;

                        Sm.StdMsg(mMsgType.Warning, Msg + "doesn't have Sub-Category.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 12);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsWOPDocNoNotValid()
        {
            if (TxtWODocNo.Text.Length == 0) return false;

            var cm = new MySqlCommand() 
            { 
                CommandText = 
                "Select DocNo From TblWOHdr " +
                "Where DocNo=@Param And CancelInd='N'; "
            };
            Sm.CmParam<String>(ref cm, "@Param", TxtWODocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Work Order Process# is invalid.");
                return true;
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveMaterialRequestWOHdr(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, DORequestDocNo, CancelInd, Status, LocalDocNo, WODocNo, DocDt, SiteCode, DeptCode, ReqType, SeqNo, ItScCode, Mth, Yr, Revision, BCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DORequestDocNo, 'N', 'A', @LocalDocNo, @WODocNo, @DocDt, @SiteCode, @DeptCode, @ReqType, @SeqNo, @ItScCode, @Mth, @Yr, @Revision, @BCCode, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@WODocNo", TxtWODocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            if (IsAutoGeneratePurchaseLocalDocNo)
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", Sm.Left(LocalDocNo, 6));
                Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 12));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteDocDt), 4));
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", "");
                Sm.CmParam<String>(ref cm, "@ItScCode", "");
                Sm.CmParam<String>(ref cm, "@Mth", "");
                Sm.CmParam<String>(ref cm, "@Yr", "");
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMaterialRequestWODtl(string DocNo, int Row, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, DoRequestDocNo, DORequestDNo, CancelInd, CancelReason, Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @DoRequestDocNo, @DORequestDNo, 'N', @CancelReason, @Status, @ItCode, @Qty, @UsageDt, @QtDocNo, @QtDNo, @UPrice, @Remark,  @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DeptCode=@DeptCode ");
                if (mIsApprovalBySiteMandatory)
                    SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                SQL.AppendLine("And T.DocType='MaterialRequestWO'; ");
            }
        
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@DORequestDNo", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval?"A":"O");
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMaterialRequestWODtl(TxtDocNo.Text, DNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    cml.Add(CancelMaterialRequestWODtl2(TxtDocNo.Text, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblMaterialRequestDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo) ||
                IsCancelledItemCheckedAlready(DNo)||
                IsCancelReasonEmpty() ;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemCheckedAlready(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPORequestDtl ");
            SQL.AppendLine("Where MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("And (CancelInd='N' And IfNull(Status, 'O')<>'C') ");
            SQL.AppendLine("And MaterialRequestDNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelMaterialRequestWODtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");

            if (!mIsDORequestNeedStockValidation)
            {
                SQL.AppendLine("Update TblMaterialRequestDtl Set ");
                SQL.AppendLine("  DORequestDocNo = null, ");
                SQL.AppendLine("  DORequestDNo = null ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And DORequestDocNo Is Not Null ");
                SQL.AppendLine("And DORequestDNo Is Not Null ");
                SQL.AppendLine("And DNo In (" + DNo + "); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand CancelMaterialRequestWODtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelReason=@CancelReason ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@Dno ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowMaterialRequestWOHdr(DocNo);
                ShowMaterialRequestWODtl(DocNo);
                ShowWO(DocNo);
                ComputeRemainingBudget();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMaterialRequestWOHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DoRequestDocNo, LocalDocNo, DocDt, SiteCode, DeptCode, ReqType, BCCode, Remark From TblMaterialRequestHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "LocalDocNo", "DocDt", "SiteCode", "DeptCode", "ReqType", 
                        "BCCode", "Remark", "DoRequestDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[4]));
                        SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[5]));
                        Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[6]), string.Empty);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        TxtDORDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowMaterialRequestWODtl(string DocNo)
        {
                
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason, A.DORequestDocNo, A.DORequestDNo, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select Group_Concat(distinct T2.UserName separator ', ') As UserName From TblDocApproval T1, TblUser T2 ");
            SQL.AppendLine("    Where T1.DocType='MaterialRequestWO' And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null  ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName,  B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, ");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, A.Remark  ");
            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    "ItCodeInternal", "ItName", "ForeignName", "ItScCode", "ItScName",  
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    "Remark", "DORequestDocNo", "DORequestDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);

                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 15);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 23, 20);

                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 23);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowWO(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQLWOP = new StringBuilder();
            SQLWOP.AppendLine("SELECT WODocNo FROM TblMaterialRequestHdr ");
            SQLWOP.AppendLine("WHERE DocNo = @DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm, SQLWOP.ToString(),
                new string[] 
                { 
                    "WODocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtWODocNo.EditValue = Sm.DrStr(dr, c[0]);
                }, true
            );
        }

        #endregion

        #region Additional Method

        private void SetBudgetCategory()
        { 
            LueBCCode.EditValue = null;
            Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueBCCode }, true);
            
            var ReqType = Sm.GetLue(LueReqType);
            var DeptCode = Sm.GetLue(LueDeptCode);
            
            if (
                mReqTypeForNonBudget.Length == 0 ||
                ReqType.Length==0 ||
                Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                DeptCode.Length==0
                ) return;

            Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
        }

        public static string GetNumber(string Dno)
        {
            string number = string.Empty;
            for (int ind = 0; ind < Dno.Length; ind++)
            {
                if (Char.IsNumber(Dno[ind]) == true)
                {
                    number = number + Dno[ind];
                }

            }
            return number;
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                ShortCode = string.Empty,
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            if (mIsDocNoWithDeptShortCode)
                ShortCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode='" + Sm.GetLue(LueDeptCode) + "';");
            
            var SQL = new StringBuilder();

            if (IsProcFormat == "1")
            {
                SQL.Append("Select Concat('"+SubCategory+"', '/', ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("         Select Convert(Substring(DocNo, locate('"+DocTitle+"', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', ");
                if (mIsDocNoWithDeptShortCode && ShortCode.Length>0) 
                    SQL.Append("'" + ShortCode + "', '/', ");
                SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateLocalDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(0, 4),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DeptCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode = '"+Sm.GetLue(LueDeptCode)+"' ");


            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000000', Convert(LocalDocNo+1, Char)), 6) From ( ");
            SQL.Append("       Select Convert(ifnull(Max(LocalDocNo), 0), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Order By Convert(ifnull(Max(LocalDocNo), 0), Decimal) Desc Limit 1");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '000001'), ");
            SQL.Append(" '/', '" + DocAbbr + "', '/', '"+DeptCode+"', '/', '"+SubCategory+"', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As LocalDocNo");

            return Sm.GetValue(SQL.ToString());
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 7) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 16).Length!=0) Qty = Sm.GetGrdDec(Grd1, Row, 16);
            if (Sm.GetGrdStr(Grd1, Row, 22).Length!=0) UPrice = Sm.GetGrdDec(Grd1, Row, 22);

            Grd1.Cells[Row, 23].Value = Qty*UPrice;

            ComputeRemainingBudget();
        }

        //private decimal ComputeAvailableBudget()
        //{
        //    var Amt = 0m;
        //    var DocDt = Sm.GetDte(DteDocDt);
        //    var DeptCode = Sm.GetLue(LueDeptCode);
        //    var BCCode = Sm.GetLue(LueBCCode);

        //    if (DocDt.Length>0 && DeptCode.Length>0 && BCCode.Length>0)
        //    {
        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select A.Amt2-IfNull(B.Amt, 0) As Amt ");
        //        SQL.AppendLine("From TblBudgetSummary A ");
        //        SQL.AppendLine("Left Join (");
        //        SQL.AppendLine("        Select Sum(T2.Qty*T2.UPrice) As Amt ");
        //        SQL.AppendLine("        From TblMaterialRequestHdr T1, TblMaterialRequestDtl T2 ");
        //        SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
        //        SQL.AppendLine("        And Left(T1.DocDt, 4)=@Yr ");
        //        SQL.AppendLine("        And Substring(T1.DocDt, 5, 2)=@Mth ");
        //        SQL.AppendLine("        And T1.DeptCode=@DeptCode ");
        //        SQL.AppendLine("        And T1.BCCode=@BCCode ");
        //        SQL.AppendLine("        And T1.ReqType<>@ReqType ");
        //        SQL.AppendLine("        And T1.CancelInd='N' ");
        //        SQL.AppendLine("        And T1.Status In ('O', 'A') ");
        //        SQL.AppendLine("        And T2.CancelInd='N' ");
        //        SQL.AppendLine("        And T2.Status In ('O', 'A') ");
        //        SQL.AppendLine("        And T1.DocNo<>@DocNo ");
        //        SQL.AppendLine(") B On 0=0 ");
        //        SQL.AppendLine("Where A.Yr=@Yr ");
        //        SQL.AppendLine("And A.Mth=@Mth ");
        //        SQL.AppendLine("And A.DeptCode=@DeptCode ");
        //        SQL.AppendLine("And A.BCCode=@BCCode;");

        //        var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

        //        Sm.CmParam<String>(ref cm, "@ReqType", mReqTypeForNonBudget);
        //        Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
        //        Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
        //        Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length!=0)?TxtDocNo.Text:"XXX");
        //        Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(DocDt, 4));
        //        Sm.CmParam<String>(ref cm, "@Mth", DocDt.Substring(4, 2));
                
        //        var Value = Sm.GetValue(cm);
        //        if (Value.Length != 0) Amt = decimal.Parse(Value);
        //    }
            
        //    return Amt;
        //}

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (Sm.GetLue(LueDeptCode).Length != 0 && Sm.GetDte(DteDocDt).Length != 0 && Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblBudget ");
                SQL.AppendLine("        Where DeptCode=@DeptCode ");
                SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                SQL.AppendLine("        And UserCode Is Not Null ");
                SQL.AppendLine("    ), 0)- ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                SQL.AppendLine("        And A.ReqType='1' ");
                SQL.AppendLine("        And B.CancelInd='N' ");
                SQL.AppendLine("        And B.Status<>'C' ");
                SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                SQL.AppendLine("        And Substring(DocDt, 5, 2)=Substring(@DocDt, 5, 2) ");
                SQL.AppendLine("       And A.DocNo<>@DocNo ");
                SQL.AppendLine("    ),0) ");
                SQL.AppendLine("As RemainingBudget");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mBudgetBasedOn == "1")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueSiteCode));

                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length != 0) ? TxtDocNo.Text : "XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

                AvailableBudget = Sm.GetValue(cm);
            }

            return decimal.Parse(AvailableBudget);
        }

        private void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                var ReqType = Sm.GetLue(LueReqType);
                if (ReqType.Length>0 && !Sm.CompareStr(ReqType, mReqTypeForNonBudget))
                {
                    AvailableBudget = ComputeAvailableBudget();
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 23).Length!=0) 
                            RequestedBudget += Sm.GetGrdDec(Grd1, Row, 23);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget-RequestedBudget, 0);
        }

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<MatReq>();
            var l1 = new List<MatReq1>();
            var l2 = new List<MatReq2>();
            var ldtl = new List<MatReqDtl>();
            var ldtl2 = new List<MatReqDtl2>();

            string[] TableName = { "MatReq", "MatReqDtl", "MatReqDtl2", "MatReq1", "MatReq2" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            var SQL = new StringBuilder();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, F.CompanyName, F.CompanyPhone, F.CompanyFax, F.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            }
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, E.SiteName, B.DeptName, C.OptDesc, A.Remark, D.UserName As CreateBy, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='isfilterbysite') As SiteInd ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.ReqType = C.OptCode ");
            SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblMaterialRequesthdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(")F On A.DocNo = F.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo And C.OptCat = 'ReqType' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblMaterialRequesthdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "DocNo",
                         //6-10
                         "DocDt",
                         "SiteName",
                         "DeptName",
                         "OptDesc",
                         "Remark",
                         //11-12
                         "CreateBy",
                         "SiteInd",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MatReq()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            SiteName = Sm.DrStr(dr, c[7]),
                            DeptName = Sm.DrStr(dr, c[8]),
                            OptDesc = Sm.DrStr(dr, c[9]),
                            HRemark = Sm.DrStr(dr, c[10]),
                            CreateBy = Sm.DrStr(dr, c[11]),
                            SiteInd = Sm.DrStr(dr, c[12]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, B.PurchaseUomCode, ");
                SQLDtl.AppendLine("DATE_FORMAT(A.UsageDt,'%d/%m/%Y') As UsageDt, ifnull(C.Qty01, 0) As Mth01, ");
                SQLDtl.AppendLine("ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, A.Remark,  B.ForeignName ");
                SQLDtl.AppendLine("From TblMaterialRequestDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine(" Left Join ( ");
                SQLDtl.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQLDtl.AppendLine("        From ( ");
                SQLDtl.AppendLine("        select Z1.itCode, ");
                SQLDtl.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQLDtl.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQLDtl.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQLDtl.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQLDtl.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQLDtl.AppendLine("            From ");
                SQLDtl.AppendLine("            ( ");
                SQLDtl.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQLDtl.AppendLine("                From ");
                SQLDtl.AppendLine("                ( ");
                SQLDtl.AppendLine("                    Select convert('01' using latin1) As Mth Union All ");
                SQLDtl.AppendLine("                    Select convert('03' using latin1) As Mth Union All ");
                SQLDtl.AppendLine("                    Select convert('06' using latin1) As Mth Union All ");
                SQLDtl.AppendLine("                    Select convert('09' using latin1) As Mth Union All ");
                SQLDtl.AppendLine("                    Select convert('12' using latin1) As Mth  ");
                SQLDtl.AppendLine("                )T1 ");
                SQLDtl.AppendLine("                Inner Join ");
                SQLDtl.AppendLine("                ( ");
                SQLDtl.AppendLine("                    Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt And last_day(@MthDocDt) ");
                SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode  ");
                SQLDtl.AppendLine("                    Union ALL ");
                SQLDtl.AppendLine("                    Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt2 And last_day(@MthDocDt3) ");
                SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    Union All ");
                SQLDtl.AppendLine("                    Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt4 And last_day(@MthDocDt3) ");
                SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    Union All ");
                SQLDtl.AppendLine("                    Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt5 And last_day(@MthDocDt3) ");
                SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    Union All ");
                SQLDtl.AppendLine("                    Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt6 And last_day(@MthDocDt3) ");
                SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                SQLDtl.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQLDtl.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQLDtl.AppendLine("        )Z1 ");
                SQLDtl.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQLDtl.AppendLine(" )C On C.ItCode = A.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' Order By A.ItCode");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                Sm.CmParamDt(ref cmDtl, "@MthDocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "Qty",
                     "PurchaseUomCode",
                     "UsageDt",
                     "Mth01",
                     //6-10
                     "Mth03",
                     "Mth06",
                     "Mth09",
                     "Mth12",
                     "Remark",

                     "ForeignName",
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new MatReqDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            UsageDt = Sm.DrStr(drDtl, cDtl[4]),
                            Mth01 = Sm.DrDec(drDtl, cDtl[5]),
                            Mth03 = Sm.DrDec(drDtl, cDtl[6]),
                            Mth06 = Sm.DrDec(drDtl, cDtl[7]),
                            Mth09 = Sm.DrDec(drDtl, cDtl[8]),
                            Mth12 = Sm.DrDec(drDtl, cDtl[9]),
                            DRemark = Sm.DrStr(drDtl, cDtl[10]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[11]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd ");
                SQLDtl2.AppendLine("From TblMaterialRequestHdr A ");
                SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-1
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "SignInd"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new MatReqDtl2()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                            SignInd = Sm.DrStr(drDtl2, cDtl2[3]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Approve1
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new MatReq1()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                            SignInd = Sm.DrStr(dr1, c1[4]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Approve2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict,  '" + Doctitle + "' As SignInd ");
            SQL2.AppendLine("from TblDocApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL2.AppendLine("And DocNo =@DocNo ");
            SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd"

                        
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new MatReq2()
                        {
                            ApprovalDno = Sm.DrStr(dr2, c2[0]),
                            UserCode = Sm.DrStr(dr2, c2[1]),
                            UserName = Sm.DrStr(dr2, c2[2]),
                            EmpPict = Sm.DrStr(dr2, c2[3]),
                            SignInd = Sm.DrStr(dr2, c2[4]),
                        });
                    }
                }

                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            if (Sm.GetParameter("IsUseItemConsumption") == "N")
                Sm.PrintReport("MaterialRequest", myLists, TableName, false);
            else
                Sm.PrintReport("MaterialRequest2", myLists, TableName, false);
        }

        internal void ShowWOInfo(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblWOHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        TxtWODocNo.EditValue = Sm.DrStr(dr, 0);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void ShowDORInfo(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblDORequestDeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        TxtDORDocNo.EditValue = Sm.DrStr(dr, 0);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private string ItemSelection()
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(Sm.GetLue(LueReqType), "2"))
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");

            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            return SQL.ToString();
        }

        internal void ShowDORDtlInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DORequestDocNo, B.DNo As DORequestDNo, B.ItCode, (IfNull(B.Qty, 0) - IfNull(D.Qty, 0)) As Qty, B.Remark, ");
            SQL.AppendLine("X.ItCtName, X.PurchaseUomCode, X.DocNo, X.DNo, X.DocDt, X.UPrice, X.MinStock, X.ReorderStock, ");
            SQL.AppendLine("X.ItName, X.ForeignName, ");
            SQL.AppendLine("X.Mth01, X.Mth03, X.Mth06, X.Mth09, X.Mth12, ");
            SQL.AppendLine("X.ItScCode, X.ItScName, X.ItCodeInternal, X.ItGrpCode, X.ItGrpName ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine(ItemSelection());

            SQL.AppendLine(")X On B.ItCode = X.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("  From TblStockSummary ");
            SQL.AppendLine("  Where WhsCode = @WhsCode ");
            SQL.AppendLine("  Group By ItCode ");
            SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            //SQL.AppendLine("And ( B.Qty > D.Qty Or B.Qty2 > D.Qty2 Or B.Qty3 > D.Qty3 ) ");
            SQL.AppendLine("And IfNull(B.Qty, 0.00)>IfNull(D.Qty, 0.00) ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetValue("Select WhsCode From TblDORequestDeptHdr Where DocNo = '" +DocNo+ "'; "));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                Sm.ShowDataInGrid(
                       ref Grd1, ref cm,
                       SQL.ToString() + " ",
                       new string[] 
                        { 
                            //0
                            "ItCode",

                            //1-5
                            "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                            
                            //6-7
                            "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                            //11-15
                            "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   

                            //16-20
                            "Mth03", "Mth06", "Mth09", "Mth12", "DORequestDocNo",
                            
                            //21-23
                            "DORequestDNo", "Qty", "Remark"
                         },
                       (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                       {
                           Grd.Cells[Row, 0].Value = Row + 1;
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 0);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 1);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 3);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 4);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 6);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 7);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 8);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 9);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 10);
                           Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 11);
                           Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 12);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                           Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 22);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);

                           if (mIsBudgetActive)
                               Grd1.Cells[Row, 10].ForeColor = Sm.GetGrdStr(Grd1, Row, 19).Length > 0 ? Color.Black : Color.Red;

                       }, true, false, false, false
                   );
                
            }
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();
                
                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (SiteCode.Length != 0)
                    Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                if (mIsFilterBySite)
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueReqType(ref DXE.LookUpEdit Lue, string ReqType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ReqType' ");
                if (ReqType.Length > 0)
                    SQL.AppendLine("And OptCode=@ReqType ");
                else
                {
                    if (mIsBudgetActive && mReqTypeForNonBudget.Length > 0)
                        SQL.AppendLine("And OptCode<>@ReqTypeForNonBudget ");
                }
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@ReqType", ReqType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                //SetBudgetCategory();
                ClearGrd();
                ComputeRemainingBudget();
            }
        }

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(SetLueReqType), string.Empty);
                //SetBudgetCategory();
                ClearGrd();
                ComputeRemainingBudget();
            }
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            ComputeRemainingBudget();
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(SetLueSiteCode), string.Empty);
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
        }

        #endregion

        #region Button  Event

        private void BtnWODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequestWODlg2(this));
        }

        private void BtnDORDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWODocNo, "Work Order#", false) && !Sm.IsLueEmpty(LueReqType, "Request Type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequestWODlg3(this, Sm.GetLue(LueDeptCode), TxtWODocNo.Text));
        }

        private void BtnWODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWODocNo, "Work Order#", false))
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtWODocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDORDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORDocNo, "DO Request#", false))
            {
                var f = new FrmDORequestDept(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDORDocNo.Text;
                f.ShowDialog();
            }

        }

        #endregion

        #endregion

        #region Report Class

        private class MatReq
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string OptDesc { get; set; }
            public string HRemark { get; set; }
            public string CreateBy { get; set; }
            public string PrintBy { get; set; }
            public string SiteInd { get; set; }
        }

        class MatReq1
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
        }
        private class MatReq2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
        }

        private class MatReqDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string UsageDt { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth12 { get; set; }
            public string DRemark { get; set; }
            public string ForeignName { get; set; }
        }

        private class MatReqDtl2
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }

        }

        #endregion

    }
}
