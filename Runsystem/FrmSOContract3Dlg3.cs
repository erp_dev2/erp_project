﻿#region Update
/*
    24/01/2023 [DITA/MNET] New Apps -> based on FrmSOContract
    21/02/2023 [BRI/MNET] tambah validasi, kolom, filter tab revenue item SO Contract
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract3Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContract3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract3Dlg3(FrmSOContract3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            this.Text = "List of Item";
            base.FrmLoad(sender, e);
            Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mFrmParent.mIsFilterByItCt ? "Y" : "N");
            SetGrd();
            SetSQL();
            ShowData();
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItCtCode, A.ItName, D.ItCtName, A.Specification, B.CtItCode, B.CtItName, A.InventoryUomCode ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Left Join TblCustomerItem B On A.ItCode=B.ItCode And B.CtCode=@CtCode ");
            SQL.AppendLine("Left Join tblitempackagingunit C On A.ItCode = C.ItCode And A.SalesUomCode = C.UomCode  ");
            SQL.AppendLine("Left Join TblItemCategory D On A.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("Where A.SalesitemInd = 'Y' ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Item's Code", 
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name",

                        //6-10
                        "Item's Category",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Customer's"+Environment.NewLine+"Item Name",
                        "UOM",

                        //11
                        "ItCtCode"
                    },
                    new int[]
                    {
                        //0
                        50,
                        
                        //1-5
                        20,80, 20, 80, 300, 

                        //6-10
                        150,130, 130, 250, 0,

                        //11
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8, 10 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            "ItCode",
                            "ItCodeInternal", "ItName", "Specification", "CtItCode", "CtItName",
                            "InventoryUomCode", "ItCtCode", "ItCtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 10, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 24, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 25, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 26, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 36, Grd1, Row2, 11);
                        mFrmParent.ComputeItem(Row1);

                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 27, 28, 31, 34, 35 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
          
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 4), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }


        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mFrmParent.mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        #endregion
    }
}
