﻿#region Update
/*
    20/01/2021 [VIN/KSM] New apps - based FrmSalesInvoice3Find
    23/02/2021 [VIN/KSM] Customer Category ditampilkan berdasarkan parameter IsCustomerComboShowCategory
    04/05/2021 [ICA/KSM] hanya menampilkan sales invoice non CBD
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice8Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesInvoice8 mFrmParent;
        private string mSQL = string.Empty;
        private string mDocTitle = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesInvoice8Find(FrmSalesInvoice8 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mDocTitle = Sm.GetParameter("DocTitle");
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mFrmParent.mIsFilterByCtCt?"Y":"N");
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, A.DocDt,  A.LocalDocNo, A.CancelInd, C.DOCtDocNo, ");
            SQL.AppendLine("B.CtName, J.CtCtName, B.NPWP, B.Address, A.CurCode, ");
            SQL.AppendLine("A.TotalAmt, A.TotalTax, ");
            SQL.AppendLine("G.TaxName As TaxName1, IfNull(G.TaxRate, 0.00)*0.01*A.TotalAmt As TaxAmt1, ");
            SQL.AppendLine("H.TaxName As TaxName2, IfNull(H.TaxRate, 0.00)*0.01*A.TotalAmt As TaxAmt2, ");
            SQL.AppendLine("I.TaxName As TaxName3, IfNull(I.TaxRate, 0.00)*0.01*A.TotalAmt As TaxAmt3, ");
            SQL.AppendLine("A.Downpayment, A.Amt, A.Remark, ");
            SQL.AppendLine("C1.ItName, Null As SAName, Null As SAAddress, Null As  SACityName, D.Remark As RemarkDO, ");
            SQL.AppendLine("C.CreateBy, C.CreateDt, C.LastUpBy, C.LastUpDt ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=B.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo=C.DocNo And C.DocType='3' ");
            SQL.AppendLine("Inner Join TblItem C1 On C.ItCode = C1.ItCode ");
            SQL.AppendLine("Left Join TblDoct2Hdr D On C.DOCtDocNo= D.DocNo ");
            //SQL.AppendLine("Left Join TblCity F On D.SACityCode = F.CityCode ");
            SQL.AppendLine("Left Join TblTax G On A.TaxCode1=G.TaxCode ");
            SQL.AppendLine("Left Join TblTax H On A.TaxCode2=H.TaxCode ");
            SQL.AppendLine("Left Join TblTax I On A.TaxCode3=I.TaxCode ");
            SQL.AppendLine("Left Join TblCustomerCategory J On B.CtCtCode=J.CtCtCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 And A.DocNo Like '%SLIDO%' And A.CBDInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Local"+Environment.NewLine+"Document#",
                        "Cancel", 
                        "DO#",
                        
                        //6-10
                        "Customer",
                        "Customer's Category",
                        "NPWP",
                        "Address",
                        "Currency", 

                        //11-15
                        mDocTitle=="KIM"?"DPP":"Amount"+Environment.NewLine+"Before Tax",
                        mDocTitle=="KIM"?"PPN":"Total Tax",
                        "Tax Type",
                        "Tax Amount",
                        "Tax Type",

                        //16-20
                        "Tax Amount",
                        "Tax Type",
                        "Tax Amount",
                        "Downpayment",
                        "Invoice Amount",

                        //21-25
                        "Item",
                        "Shipping",
                        "Address",
                        "City",
                        "DO's Remark",

                        //26-30
                        "Remark",
                        "Created By",
                        "Created Date",
                        "Created Time", 
                        "Last Updated By", 

                        //31-32
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 120, 80, 150, 
                        
                        //6-10
                        200, 200, 150, 200, 100, 

                        //11-15
                        130, 130, 130, 130, 130,

                        //16-20
                        130, 130, 130, 130, 130, 
                        
                        //21-25
                        200, 200, 200, 200, 200, 
                        
                        //26-30
                        200, 130, 130, 130, 130, 
                        
                        //31-32
                        130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 14, 16, 18, 19, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 28, 31 });
            Sm.GrdFormatTime(Grd1, new int[] { 29, 32 });
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 27, 28, 29, 30, 31, 32 }, false);
            if(!mFrmParent.mIsCustomerComboShowCategory) Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 27, 28, 29, 30, 31, 32 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And A.CancelInd='N' ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDO.Text, new string[] { "C.DOCtDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtCode), "B.CtCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "LocalDocNo", "CancelInd", "DOCtDocNo", "CtName", 

                            //6-10
                            "CtCtName", "NPWP", "Address", "CurCode", "TotalAmt", 
                            
                            //11-15
                            "TotalTax", "TaxName1", "TaxAmt1", "TaxName2", "TaxAmt2", 
                            
                            //16-20
                            "TaxName3", "TaxAmt3", "Downpayment", "Amt", "ItName", 
                            
                            //21-25
                            "SAName", "SAAddress", "SACityName", "RemarkDO", "Remark", 
                            
                            //26-29
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 14].Value = decimal.Truncate(dr.GetDecimal(c[13]));
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 16].Value = decimal.Truncate(dr.GetDecimal(c[15]));
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            if (mFrmParent.mIsDOCtAmtRounded)
                                Grd.Cells[Row, 18].Value = decimal.Truncate(dr.GetDecimal(c[17]));
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 29);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDO_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDO_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mFrmParent.mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mFrmParent.mIsFilterByCtCt?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's category");
        }

        #endregion

        #endregion
    }
}
