﻿#region Update
/*
    04/12/2020 [WED/IMS] new apps
    07/12/2020 [WED/IMS] masih salah logic
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt9Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt9 mFrmParent;
        string mSQL = string.Empty, mWhsCode = string.Empty; 
        
        #endregion

        #region Constructor

        public FrmDOCt9Dlg2(FrmDOCt9 FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            //SQL.AppendLine("    Select Null As SOContractDocNo, Null As SOContractDNo, A.ItCode, B.ItCodeInternal, B.ItName, A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            //SQL.AppendLine("    A.Qty, A.Qty2, A.Qty3, ");
            //SQL.AppendLine("    B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, B.ItGrpCode ");
            //SQL.AppendLine("    From TblStockSummary A ");
            //SQL.AppendLine("    Inner Join TblItem B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("    Left Join TblProperty C On A.PropCode=C.PropCode ");
            //SQL.AppendLine("    Where A.WhsCode=@WhsCode ");
            //SQL.AppendLine("    And A.Qty>0 ");
            //SQL.AppendLine("    And Locate(Concat('##', A.ItCode, '##'), @SelectedItem2)>0 ");

            SQL.AppendLine("    Select A.DocNo As SOContractDocNo, A.DNo As SOContractDNo, A.No, A.ItCode, B.ItCodeInternal, B.ItName, H.PropCode, I.PropName, H.BatchNo, H.Source, H.Lot, H.Bin, ");
            SQL.AppendLine("    If(G.Qty < H.Qty, G.Qty, H.Qty) As Qty, If(G.Qty < H.Qty, G.Qty, H.Qty) As Qty2, If(G.Qty < H.Qty, G.Qty, H.Qty) As Qty3, ");
            SQL.AppendLine("    B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, B.ItGrpCode ");
            SQL.AppendLine("    From TblSOContractDtl A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("        And B.ServiceItemInd = 'N' ");
            SQL.AppendLine("        And Find_In_Set(A.DocNo, @SOContractDocNo) ");
            SQL.AppendLine("    Inner Join TblSOContractHdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr D On C.BOQDocNO = D.DocNo ");
            SQL.AppendLine("    Inner JOin TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("    Inner Join TblProjectGroup F On E.PGCode = F.PGCode ");
            SQL.AppendLine("    Inner Join TblDRDtl G On A.DocNo = G.SODocNo And A.DNo = G.SODNo ");
            SQL.AppendLine("        And G.DocNo = @DRDocNo ");
            SQL.AppendLine("    Inner Join TblStockSummary H On A.ItCode = H.ItCode ");
            SQL.AppendLine("        And H.BatchNo = F.ProjectCode ");
            SQL.AppendLine("        And H.WhsCode = @WhsCode ");
            SQL.AppendLine("        And H.Qty > 0.00 "); // kalau stock nya 0, gak dimunculin
            SQL.AppendLine("    Left Join TblProperty I On H.PropCode = I.PropCode ");
            SQL.AppendLine("    Where Not Find_In_Set(Concat(A.DocNo, A.DNo), @SelectedItem2) ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select A.DocNo As SOContractDocNo, A.DNo As SOContractDNo, A.No, A.ItCode, B.ItCodeInternal, B.ItName, Null As PropCode, Null As PropName, F.ProjectCode As Batch, Null As Source, Null As Lot, Null As Bin, ");
            SQL.AppendLine("    G.Qty, G.Qty As Qty2, G.Qty As Qty3, ");
            SQL.AppendLine("    B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, B.ItGrpCode ");
            SQL.AppendLine("    From TblSOContractDtl A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("        And B.ServiceItemInd = 'Y' ");
            SQL.AppendLine("        And A.ItCode Not In (Select Distinct ItCode From TblStockSummary Where WhsCode = @WhsCode) ");
            SQL.AppendLine("        And Find_In_Set(A.DocNo, @SOContractDocNo) ");
            SQL.AppendLine("    Inner Join TblSOContractHdr C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr D On C.BOQDocNO = D.DocNo ");
            SQL.AppendLine("    Inner JOin TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("    Left Join TblProjectGroup F On E.PGCode = F.PGCode ");
            SQL.AppendLine("    Inner Join TblDRDtl G On A.DocNo = G.SODocNo And A.DNo = G.SODNo And G.DocNo = @DRDocNo ");
            SQL.AppendLine("    Where Not Find_In_Set(Concat(A.DocNo, A.DNo), @SelectedItem2) ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Where 0 = 0 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Code",
                        "Item's Name", 
                        
                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",

                        //11-15
                        "Bin", 
                        "Stock",
                        "UoM",
                        "Stock 2",
                        "Uom 2",

                        //16-20
                        "Stock 3",
                        "Uom 3",
                        "Group",
                        "SOContractDocNo",
                        "SOContractDNo",

                        //21
                        "No"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 100, 250, 
                        
                        //6-10
                        0, 0, 200, 150, 60, 
                        
                        //11-15
                        80, 80, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 100, 0, 0,

                        //21
                        60
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 9, 10, 14, 15, 16, 17, 18, 19, 20 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[18].Visible = true;
                Grd1.Cols[18].Move(5);
            }
            Grd1.Cols[21].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter2 = string.Empty;

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "T", ref mFrmParent.Grd1, 11, 12, 13);
                var Filter = (Filter2.Length > 0) ? " And (" + Filter2 + ") " : " And 0=0 ";

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem2", mFrmParent.GetSelectedItem2());
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", mFrmParent.GetSOContractDocNo());
                Sm.CmParam<String>(ref cm, "@DRDocNo", mFrmParent.TxtDRDocNo.Text);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItCodeInternal", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPropCode.Text, new string[] { "T.PropCode", "T.PropName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, new string[] { "T.BatchNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, new string[] { "T.Lot" });
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, new string[] { "T.Bin" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By Right(Concat('0000000000', IfNull(T.No, '0')), 10), T.ItName, T.BatchNo;",
                    new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-5
                        "ItCodeInternal", "ItName", "PropCode", "PropName", "BatchNo",  
                        
                        //6-10
                        "Source", "Lot", "Bin", "Qty", "InventoryUomCode", 
                        
                        //11-15
                        "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "ItGrpCode", 

                        //16-18
                        "SOContractDocNo", "SOContractDNo", "No"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 21);

                        mFrmParent.Grd1.Rows.Add();

                        mFrmParent.ComputeItemDOQty(Sm.GetGrdStr(mFrmParent.Grd1, Row1, 4));
                        
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), Sm.GetGrdStr(Grd1, Row, 6)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 10), Sm.GetGrdStr(Grd1, Row, 8)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 11), Sm.GetGrdStr(Grd1, Row, 9)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 12), Sm.GetGrdStr(Grd1, Row, 10)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 13), Sm.GetGrdStr(Grd1, Row, 11)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 27), Sm.GetGrdStr(Grd1, Row, 19)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 28), Sm.GetGrdStr(Grd1, Row, 20))
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtPropCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
