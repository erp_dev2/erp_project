﻿#region
/*
    20/12/2017 [HAR] tambah gold rate, info invoice
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    11/08/2021 [VIN/ALL] GenerateVoucherRequestDocNo: Bug SeqNo
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpReward : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo="" ;
        internal bool mIsFilterByDeptHR = true;
        internal FrmEmpRewardFind FrmFind;
        private string mMainCurCode = string.Empty;
        internal decimal mRewardSubmissionValue15, mRewardSubmissionValue25 = 0;

        #endregion

        #region Constructor

        public FrmEmpReward(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {

            if (this.Text.Length == 0) this.Text = "Employee's Reward";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetFormControl(mState.View);
            SetLuePeriodCode(ref LuePeriodCode);
            
            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtStatus, 
                        TxtEmpCode, TxtEmpName, TxtDeptCode, TxtPosCode, TxtYearsService, 
                        LuePeriodCode, TxtAmt, TxtVRDocNo, TxtVCDocNo,  TxtRate, MeeRemark 
                    }, true);
                    BtnEmpCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LuePeriodCode, TxtRate, MeeRemark 
                    }, false);
                    BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                        MeeCancelReason 
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, TxtEmpCode, TxtVCDocNo, TxtYearsService, 
                TxtEmpName, TxtDeptCode, TxtPosCode, LuePeriodCode, TxtVRDocNo, MeeRemark 
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtRate }, 0);
        }

        private void ClearData2()
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtRate }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpRewardFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLuePeriodCode(ref LuePeriodCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpReward", "TblEmpReward");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpReward(DocNo, VoucherRequestDocNo));

            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee's Code", false) ||
                Sm.IsLueEmpty(LuePeriodCode, "Periode") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark")||
                IsDataAlreadyExist()
                ;
        }


        private bool IsDataAlreadyExist()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblEmpReward Where CancelInd='N' And EmpCode=@Param And PeriodCode='"+Sm.GetLue(LuePeriodCode)+"' ;",
                    TxtEmpCode.Text,
                    "employee already get reward for this period.");
        }

        private MySqlCommand SaveEmpReward(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmpReward ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, PeriodCode, EmpCode, Rate, Amt, VoucherRequestDocNo, Remark, CreateBy, CreateDt )");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @PeriodCode, @EmpCode, @Rate, @Amt, @VoucherRequestDocNo, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PeriodCode", Sm.GetLue(LuePeriodCode));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Rate", decimal.Parse(TxtRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");

            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //        type = string.Empty;

        //    var SQL = new StringBuilder();

        //    if (type == string.Empty)
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        //    }
        //    else
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
        //    }
        //    return Sm.GetValue(SQL.ToString());
        //}

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string EmpRewardDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, CurCode, Amt, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='EmpRewardDeptCode'), ");
            SQL.AppendLine("'50', 'C', 'C', @CurCode, @Amt, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @EmpRewardDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='EmpReward' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select Amt From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("), 0.00)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpReward' ");
            SQL.AppendLine("    And DocNo=@EmpRewardDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblEmpReward Set Status='A' ");
            SQL.AppendLine("Where DocNo=@EmpRewardDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpReward' ");
            SQL.AppendLine("    And DocNo=@EmpRewardDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@EmpRewardDocNo", EmpRewardDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, '001', @Description, @Amt, @Remark, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Description", String.Concat(TxtEmpName.Text, "'s ", "Reward."));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpReward());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessedToVoucher();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblEmpReward Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessedToVoucher()
        {
            return Sm.IsDataExist(
                    "Select 1 " +
                    "From TblEmpReward A " +
                    "Inner Join TblVoucherHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo And B.CancelInd='N' " +
                    "Where A.DocNo=@Param;",
                    TxtDocNo.Text,
                    "This data already process to voucher request.");
        }

        private MySqlCommand EditEmpReward()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpReward Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And DocNo In (Select VoucherRequestDocNo From TblEmpReward Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpReward(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpReward(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, C.DeptName, D.PosName, ");
            SQL.AppendLine("A.PeriodCode, A.Rate, A.Amt, A.VoucherRequestDocNo, E.VoucherDocNo, TimeStampdiff(YEAR, B.JoinDt, A.DocDt) YS, A.Remark ");
            SQL.AppendLine("From TblEmpReward A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("left Join tblvoucherrequesthdr E On A.VoucherRequestDocNo = E.DocNo  ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "StatusDesc", "EmpCode",

                        //6-10
                        "EmpName", "DeptName", "PosName", "PeriodCode", "Rate",  
                        
                        //11-15
                        "Amt", "VoucherRequestDocNo", "VoucherDocNo", "YS", "Remark",
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                    TxtPosCode.EditValue = Sm.DrStr(dr, c[8]);
                    SetLuePeriodCode(ref LuePeriodCode);
                    Sm.SetLue(LuePeriodCode, Sm.DrStr(dr, c[9]));
                    TxtRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtVRDocNo.EditValue = Sm.DrStr(dr, c[12]);
                    TxtVCDocNo.EditValue = Sm.DrStr(dr, c[13]);
                    TxtYearsService.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[14]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                }, true
        );

        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameter("IsFilterByDeptHR") == "Y";
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mRewardSubmissionValue15 = Sm.GetParameterDec("RewardSubmissionValue15");
            mRewardSubmissionValue25 = Sm.GetParameterDec("RewardSubmissionValue25");
        }

   
        private static void SetLuePeriodCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='RewardPeriodYr' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ComputeReceive()
        {
            decimal amt = 0;
            decimal rate = Decimal.Parse(TxtRate.Text);
            if (rate > 0)
            {
                if (Sm.GetLue(LuePeriodCode) == "15")
                {
                    amt = mRewardSubmissionValue15 * rate;
                }
                else if(Sm.GetLue(LuePeriodCode) == "25")
                {
                    amt = mRewardSubmissionValue25 * rate;
                }
            }
            TxtAmt.EditValue = Sm.FormatNum(amt, 0);
        }

        #endregion

     
        #endregion

        #region Event
        private void TxtRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRate, 0);
            ComputeReceive();
        }

        private void LuePeriodCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData2();
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePeriodCode, new Sm.RefreshLue1(SetLuePeriodCode));
            }
        }

        private void TxtYearsService_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtYearsService, 0);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpRewardDlg(this, Sm.GetDte(DteDocDt).Substring(0, 8)));
        }
        #endregion    

        

    }
}
